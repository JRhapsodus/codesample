#ifndef INCLUDE_MANAGERS_H_
#define INCLUDE_MANAGERS_H_

#include "SIEngine/Managers/SystemSettings.h"
#include "SIEngine/Managers/UsersManager.h"
#include "SIEngine/Managers/TimerManager.h"
#include "SIEngine/Managers/ConnectionManager.h"
#include "SIEngine/Managers/ContentManager.h"
#include "SIEngine/Managers/StringsManager.h"
using namespace SIManagers;

#endif

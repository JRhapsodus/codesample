#ifndef INCLUDE_SISCENES_H_
#define INCLUDE_SISCENES_H_

#include "Glasgow/Scenes/SceneManager.h"
#include "Glasgow/Scenes/Child/ChildScene.h"
#include "Glasgow/Scenes/Parent/ParentScene.h"
#include "Glasgow/Scenes/OutOfBox/OutOfBoxScene.h"
#include "Glasgow/Scenes/Flappy/FlappyScene.h"
#include "Glasgow/Scenes/VideoWidget/VideoWidgetScene.h"
#include "Glasgow/Scenes/ErrorWidget/ErrorScene.h"

using namespace Glasgow;

#endif

#ifndef SIDESERIALIZE_H_
#define SIDESERIALIZE_H_

#include "SIEngine/Core/Scene.h"
#include "SIEngine/Core/Exception.h"
#include "SIEngine/Core/SerializedObjectInfo.h"

using namespace SICore;
using namespace SIGeometry;

#endif /* SIDESERIALIZE_H_ */

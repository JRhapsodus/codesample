#ifndef INCLUDE_SIMATERIALS_H_
#define INCLUDE_SIMATERIALS_H_

#include "SIEngine/Rendering/Materials/BlurredMaterial.h"
#include "SIEngine/Rendering/Materials/TextMaterial.h"
#include "SIEngine/Rendering/Materials/TransparentSpriteMaterial.h"
#include "SIEngine/Rendering/Materials/VertexLitMaterial.h"
#include "SIEngine/Rendering/Materials/WireFrameMaterial.h"
#include "SIEngine/Rendering/Materials/TintMaterial.h"

using namespace SIRendering;

#endif

#ifndef SIINPUT_H_
#define SIINPUT_H_

#include "SIEngine/Core/Event.h"
#include "SIEngine/Input/ButtonEvent.h"
#include "SIEngine/Input/AccelerationEvent.h"
#include "SIEngine/Input/AnalogStickEvent.h"
#include "SIEngine/Input/WandEvent.h"

using namespace SICore;
using namespace SIInput;

#endif

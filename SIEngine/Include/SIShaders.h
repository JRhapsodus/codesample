#ifndef INCLUDE_SISHADERS_H_
#define INCLUDE_SISHADERS_H_

#include "SIEngine/Rendering/Shaders/BlurredShader.h"
#include "SIEngine/Rendering/Shaders/ColorShader.h"
#include "SIEngine/Rendering/Shaders/LitTextShader.h"
#include "SIEngine/Rendering/Shaders/TextShader.h"
#include "SIEngine/Rendering/Shaders/TexturedAlphaShader.h"
#include "SIEngine/Rendering/Shaders/TexturedShader.h"
#include "SIEngine/Rendering/Shaders/TransformedColorShader.h"
#include "SIEngine/Rendering/Shaders/TintShader.h"

using namespace SIRendering;

#endif

#ifndef INCLUDE_SICORE_H_
#define INCLUDE_SICORE_H_


#include "SIEngine/Core/StringClass.h"
#include "SIEngine/Core/Exception.h"
#include "SIEngine/Core/User.h"
#include "SIEngine/Core/Event.h"
#include "SIEngine/Core/Scene.h"
#include "SIEngine/Core/LaunchAppState.h"
#include "SIEngine/Core/LaunchParameters.h"
using namespace SICore;
#endif

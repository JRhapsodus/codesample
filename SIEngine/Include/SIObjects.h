#ifndef SIOBJECTS_H_
#define SIOBJECTS_H_

#include "SIEngine/Objects/DialogObject.h"
#include "SIEngine/Objects/CameraObject.h"
#include "SIEngine/Objects/MeshObject.h"

using namespace SICore;

#endif /* SIOBJECTS_H_ */

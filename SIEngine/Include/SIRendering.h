#ifndef SIRENDERING_H_
#define SIRENDERING_H_

#include "SIEngine/Core/BaseRenderer.h"
#include "SIEngine/Core/QtRenderer.h"
#include "SIEngine/Core/OpenGlRenderer.h"
#include "SIEngine/Rendering/TextureManager.h"

using namespace SIRendering;
using namespace SICore;


#endif /* SIRENDERING_H_ */

#ifndef INCLUDE_SIGEOMETRY_H_
#define INCLUDE_SIGEOMETRY_H_

#include "SIEngine/Geometry/Transform.h"
#include "SIEngine/Geometry/Box3f.h"
#include "SIEngine/Geometry/Line3f.h"
#include "SIEngine/Geometry/Matrix.h"
#include "SIEngine/Geometry/Quaternion.h"
#include "SIEngine/Geometry/Triangle3f.h"
#include "SIEngine/Geometry/Frustum.h"
#include "SIEngine/Geometry/VertexFormats.h"
#include "SIEngine/Geometry/Mathematics.h"

/**
 * @namespace SIGeometry
 * Geometry namespace to deal with the core mathematics of the engine.
 */
using namespace SIGeometry;

#endif



#ifndef INCLUDE_SIACTIONS_H_
#define INCLUDE_SIACTIONS_H_

#include "SIEngine/Actions/BaseAction.h"
#include "SIEngine/Actions/MoveByAction.h"
#include "SIEngine/Actions/MoveToAction.h"
#include "SIEngine/Actions/RotateByAction.h"
#include "SIEngine/Actions/RotateToAction.h"
#include "SIEngine/Actions/ScaleByAction.h"
#include "SIEngine/Actions/ScaleToAction.h"
#include "SIEngine/Actions/IntervalAction.h"
#include "SIEngine/Actions/MultiAction.h"
#include "SIEngine/Actions/SpriteAnimationAction.h"
#include "SIEngine/Actions/FadeToAction.h"
#include "SIEngine/Actions/PlaySoundAction.h"
#include "SIEngine/Actions/VisibilityAction.h"
#include "SIEngine/Actions/ColorToAction.h"
#include "SIEngine/Actions/KillAction.h"
#include "SIEngine/Actions/CallBackAction.h"
#include "SIEngine/Actions/AnimationAction.h"
#include "SIEngine/Actions/ButtonStateAction.h"
#include "SIEngine/Actions/ChangeLabelAction.h"

/**
 * @namespace SIActions
 * Actions to deal with scripting objects in a sequence style syntax
 */
using namespace SIActions;

#endif

#ifndef WAND_EVENT
#define WAND_EVENT

#include "SIEngine/Core/Event.h"
#include "SIEngine/Objects/GameObject.h"

namespace SIInput
{

typedef enum WandAction
{
	WandEventEnter,
	WandEventLeave
} WandAction;

class WandEvent: public Event
{
	CLASSEXTENDS(WandEvent, Event);
public:
	WandEvent();
	virtual ~WandEvent();

	WandEvent(int a, GameObject* object);

	int 			action;
	GameObject* 	sceneObject;

};

}
#endif /* BUTTONEVENT_H_ */

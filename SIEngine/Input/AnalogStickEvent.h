#ifndef ANALOGSTICKEVENT_H_
#define ANALOGSTICKEVENT_H_

#include "SIEngine/Core/Event.h"
#include "SIEngine/Include/SIGeometry.h"

namespace SIInput
{

class AnalogStickEvent: public Event
{
	CLASSEXTENDS(AnalogStickEvent, Event);
	ADD_TO_CLASS_MAP;
public:
	AnalogStickEvent();
	virtual ~AnalogStickEvent();
	Vec2f 	analogStick;
};

}

#endif /* ANALOGSTICKEVENT_H_ */

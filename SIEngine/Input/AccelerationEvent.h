#ifndef ACCELERATIONEVENT_H_
#define ACCELERATIONEVENT_H_

#include "SIEngine/Core/Event.h"
#include "SIEngine/Include/SIGeometry.h"

namespace SIInput
{

class AccelerationEvent: public Event
{
	CLASSEXTENDS(AccelerationEvent, Event);
	ADD_TO_CLASS_MAP;
public:
	AccelerationEvent(Vec3f accel);
	virtual ~AccelerationEvent();

	Vec3f 	acceleration;
};

}
#endif /* ACCELERATIONEVENT_H_ */

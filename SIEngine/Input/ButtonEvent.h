#ifndef BUTTONEVENT_H_
#define BUTTONEVENT_H_

#include "SIEngine/Core/Event.h"
#include "SIEngine/Include/SIGeometry.h"

namespace SIInput
{

typedef enum ButtonActions {
	ButtonActionNone,
	ButtonActionPress,
	ButtonActionRelease
} ButtonAction;

class ButtonEvent: public Event {
	CLASSEXTENDS(ButtonEvent, Event);
	ADD_TO_CLASS_MAP;
public:
	ButtonEvent();
	virtual ~ButtonEvent();

	ButtonEvent(int b, ButtonAction a ) {buttons = b; action = a;}

	int 			buttons;
	ButtonAction	action;
};

}
#endif /* BUTTONEVENT_H_ */

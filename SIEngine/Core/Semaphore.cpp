#include "Semaphore.h"
#include "SIEngine/Core/Exception.h"

namespace SICore
{

Semaphore* createSemaphore(bool manualReset, bool initialState)
{
	Semaphore* event = new Semaphore();
	int result = pthread_cond_init(&event->variable, 0);

	if( result == 0 )
	{ ThrowException::exception("System error obtaining semaphore"); }

	result = pthread_mutex_init(&event->mutex, 0);

	if( result == 0 )
	{ ThrowException::exception("System error obtaining semaphore"); }

	event->state = false;
	event->autoReset = !manualReset;

	if(initialState)
	{
		result = setSemaphore(event);
		if( result == 0 )
		{ ThrowException::exception("System error obtaining semaphore"); }
	}

	return event;
}

int destroySemaphore(Semaphore* event)
{
	int result = pthread_cond_destroy(&event->variable);

	if( result == 0 )
	{ ThrowException::exception("System error destroying semaphore"); }

	result = pthread_mutex_destroy(&event->mutex);

	if( result == 0 )
	{ ThrowException::exception("System error destroying semaphore"); }

	delete event;
	return 0;
}

int unlockedWaitForSemaphore(Semaphore* event, uint64_t milliseconds)
{
	int result = 0;
	if(!event->state)
	{
		//Zero-timeout event state check optimization
		if(milliseconds == 0)
		{  return -1; }

		timespec ts;
		if(milliseconds != (uint64_t) -1)
		{
			timeval tv;
			gettimeofday(&tv, NULL);
			uint64_t nanoseconds = ((uint64_t) tv.tv_sec) * 1000 * 1000 * 1000 + milliseconds * 1000 * 1000 + ((uint64_t) tv.tv_usec) * 1000;
			ts.tv_sec = nanoseconds / 1000 / 1000 / 1000;
			ts.tv_nsec = (nanoseconds - ((uint64_t) ts.tv_sec) * 1000 * 1000 * 1000);
		}

		do
		{
			//Regardless of whether it's an auto-reset or manual-reset event:
			//wait to obtain the event, then lock anyone else out
			if(milliseconds != (uint64_t) -1)
			{ result = pthread_cond_timedwait(&event->variable, &event->mutex, &ts); }
			else
			{ result = pthread_cond_wait(&event->variable, &event->mutex); }
		}
		while(result == 0 && !event->state);

		//We've only accquired the event if the wait succeeded
		if(result == 0 && event->autoReset)
		{  event->state = false;  }
	}
	else if(event->autoReset)
	{
		//It's an auto-reset event that's currently available;
		//we need to stop anyone else from using it
		result = 0;
		event->state = false;
	}
	//Else we're trying to obtain a manual reset event with a signaled state;  don't do anything

	return result;
}


int waitForSemaphore(Semaphore* event, uint64_t milliseconds)
{
	int tempResult;
	if(milliseconds == 0)
	{
		tempResult = pthread_mutex_trylock(&event->mutex);
		if(tempResult == EBUSY)
		{  return ETIMEDOUT;  }
	}
	else
	{  tempResult = pthread_mutex_lock(&event->mutex);  }

	int result = unlockedWaitForSemaphore(event, milliseconds);
	tempResult = pthread_mutex_unlock(&event->mutex);

	 return result;
}

int setSemaphore(Semaphore* event)
{
	int result = pthread_mutex_lock(&event->mutex);

	if( result == 0 )
	{ ThrowException::exception("System error setting semaphore"); }

	event->state = true;

	//Depending on the event type, we either trigger everyone or only one
	if(event->autoReset)
	{
		if(event->state)
		{
			result = pthread_mutex_unlock(&event->mutex);
			result = pthread_cond_signal(&event->variable);

			if( result == 0 )
			{ ThrowException::exception("System error setting semaphore"); }

			return 0;
		}
	}
	else
	{
		result = pthread_mutex_unlock(&event->mutex);

		if( result == 0 )
		{ ThrowException::exception("System error setting semaphore"); }

		result = pthread_cond_broadcast(&event->variable);

		if( result == 0 )
		{ ThrowException::exception("System error setting semaphore"); }
	}

	return 0;
}

int resetSemaphore(Semaphore* event)
{
	int result = pthread_mutex_lock(&event->mutex);

	if( result == 0 )
	{ ThrowException::exception("System error resetting semaphore"); }

	event->state = false;

	result = pthread_mutex_unlock(&event->mutex);

	if( result == 0 )
	{ ThrowException::exception("System error resetting semaphore"); }

	return 0;
}


}

#ifndef INCLUDE_OPENGLRENDERER_H_
#define INCLUDE_OPENGLRENDERER_H_

#include "SIEngine/Core/BaseRenderer.h"

namespace SICore
{
#define MAX_CAMERAS 5
class OpenGlRenderer : public BaseRenderer
{
	CLASSEXTENDS(OpenGlRenderer, BaseRenderer);
public:
	OpenGlRenderer();
	virtual ~OpenGlRenderer();

	/// <summary> 
	/// Rendering functions
	/// </summary> 
	virtual void render(GameObject* renderable);

	static void renderWireFrame(GameObject* renderable);
	static void renderWorldBox( Box3f box );
	static void renderScreenBox( Box3f box );
	static void renderLine( Line3f box, Color c);
	static void renderTextToScreen(const char *text, float startScreenX, float startScreenY, float scalar, Color color);
	static void checkGLError();

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void render(Scene *scene);

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void suspend();

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void resume();

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void clearBuffers();

	/// <summary>
	/// Gets rid of the cached 'zsorted' scene graph
	/// </summary>
	virtual void markSceneGraphDirty();


	/// <summary> 
	/// Statics
	/// </summary> 
	static OpenGlRenderer* getInstance();

	virtual bool isLeftHanded();
	void swapBackBuffer();

	/// <summary> 
	/// Debugging features
	/// </summary> 
	bool showLocalAxis;
	bool showBounds;
	bool wireFrameMode;
	bool showWorldPositions;

	/// <summary> 
	/// Camera being used to current render with: weak reference
	/// </summary> 
	CameraObject* mainCamera;

private:
	ObjectArray* flatGraph[MAX_CAMERAS];
	FontData *debugFont;
	BrioOpenGLConfig* openGlConfig;
	GLuint vertexBuffer;
	GLuint colorBuffer;
	GLuint textureBuffer;
	GLuint indexBuffer;
};

}
#endif

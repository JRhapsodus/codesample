#ifndef LAUNCHAPPSTATE_H_
#define LAUNCHAPPSTATE_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Utilities/Constants.h"
#include "LaunchParameters.h"

using namespace SIUtils;

namespace SICore
{

class LaunchAppState : public BaseObject
{
	CLASSEXTENDS(LaunchAppState, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	/// <summary>
	/// Struct filled out to go between applications
	/// </summary>
	LaunchAppState();
	LaunchAppState(ExitReason reason, AppState currentAppState, String data1, String data2, LaunchParameters params);
	virtual ~LaunchAppState();

	/// <summary>
	/// Debug Log
	/// </summary>
	void debugLog();

	LaunchParameters launchParameters;
	ExitReason exitReason;
	AppState stateAtLastExit;
	String data1;
	String data2;
};

}
#endif

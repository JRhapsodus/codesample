#ifndef SCENE_H_
#define SCENE_H_

#include <App.h>
#include <AppInterface.h>
#include <AppManager.h>
#include <LTM.h>
#include <CSystemData.h>
#include <KernelMPI.h>
#include <ButtonMPI.h>
#include <DisplayMPI.h>
#include <iostream>
#include <stdio.h>
#include <ButtonEventQueue.h>
#include <ButtonTypes.h>
#include <BrioOpenGLConfig.h>
#include <EventListener.h>

#include <EventMPI.h>
#include <ButtonEventQueue.h>
#include <TouchEventQueue.h>
#include <Hardware/HWAnalogStickMPI.h>
#include <AccelerometerMPI.h>

#include <sys/time.h>
#include <GLES2/gl2.h>

#include <Hardware/HWController.h>
#include <Hardware/HWControllerMPI.h>
#include <Vision/VNVisionMPI.h>
#include <Vision/VNWand.h>
#include <Vision/VNWandTracker.h>

#include "SIEngine/Objects/DialogObject.h"
#include "SIEngine/Objects/QtSpriteObject.h"
#include "SIEngine/Include/SIInput.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/GlasgowData.h"
#include "Glasgow/Renderables/WandObject.h"
#include "Glasgow/Data/SoundInfo.h"

#include <AudioMPI.h>

using namespace SIUtils;
using namespace Glasgow;
LF_USING_BRIO_NAMESPACE();
using namespace LF::Hardware;

namespace SICore
{
class DialogObject;

/// <summary> 
/// Scene
/// </summary> 
class Scene : public BaseObject
{
	CLASSEXTENDS(Scene, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	Scene();
	Scene(String path,String stringTable );
	~Scene();

	bool			loaded;

	/// <summary> 
	/// Settors
	/// </summary> 
	void 			setWorldRoot(GameObject *rootGameObject);
	void 			printSceneGraph();
	static void 	printSceneGraph( GameObject* node, int level );
	/// <summary> 
	/// Information queries
	/// </summary> 
	GameObject*		getWorldRoot();
	GameObject*		findObject(String name);
	CameraObject* 	getCamera(int id);
	CameraObject* 	getCamera( GameObject* forObject );
	ObjectArray* 	getCameras();
	GameObject*   	getHighlightedItem();
	ObjectArray* 	getHotSpots();

	/// <summary> 
	/// Creation for parts of the scene
	/// </summary> 
	void addCamera( CameraObject* camera );
	void createWand();
	void playSound( String soundName );
	void playVo( String soundName );
	static void instancePlaySound( String soundName );
    static void instancePlayVo( String voName );
	void playMusic( String soundName );
	static void instancePlayMusic( String soundName );


	/// <summary> 
	/// BaseScene features
	/// </summary> 
	virtual void render();
	virtual void update(float dt);
	virtual void postRender();


	LaunchAppState getLastExitState();


	/// <summary> 
	/// Cannot override, all scenes will process this the same way, and call
	/// the appropriate post processed function for convenience
	/// </summary> 
	void onButtonEvent(ButtonEvent *event);

	/// <summary>
	/// Sometimes a scene will want to check for any analog event as opposed to being
	/// told about the post processed candidacy
	/// </summary>
	virtual bool onAnalogStickEvent(AnalogStickEvent *event);

	/// <summary>
	/// Input from accelerometer
	/// </summary>
	void onAccelerationEvent(AccelerationEvent *event);

	/// <summary>
	/// Input from wand
	/// </summary>
	void onWandEvent(WandEvent * event);

	/// <summary> 
	/// Post processed information
	/// </summary>
	virtual void onSyncPushed();
	virtual void onButtonHint();
	virtual void onButtonHome();
	virtual void onButtonPressAction();
	virtual void onButtonPressBack();
	virtual void onWandEnter( GameObject* sceneObject );
	virtual void onWandLeave( GameObject* sceneObject );
	virtual void onAnalogChange( GameObject* current, GameObject* candidate );
	virtual void onAccelerationChange(Vec3f acceleration);
	virtual void onSelectionChange( GameObject* oldSelection, GameObject* newSelection );
	virtual void onDialogHandled( DialogType type, DialogButtonOption option );
	virtual void onDialogCanceled( DialogType type );
	virtual void onTimerExpired( TimerEvent* timer );
	virtual void onNetworkConnected( String networkName );
	virtual void onDonwloadFinished( String packageID );
	virtual void onDonwloadStarted( String packageID );
	virtual void onDonwloadProgressed( String packageID, int downloadProgress );
	virtual void onNetworkError(String networkName, String networkError );
	virtual void onEthernetPluggedIn();
	virtual void onEthernetUnplugged();
	virtual void onReturnToApp();
	virtual void onControllerConnected(HWController* controller);
	virtual void onControllerDisconnected(HWController* controller);
	virtual void onControllerSynced(HWController* controller, bool success);
	virtual void onWandTurnedOn(HWController* controller);
	virtual void onWandTurnedOff(HWController* controller);
	virtual void onCartridgeInserted();
	virtual void onCartridgeReady();
	virtual void onCartridgeRemoved();
	virtual void onCameraPluggedIn();
	virtual void onCameraUnplugged();
	virtual void onDownloadError();
	virtual void onPendingDownloadsChanged();
	virtual void onPackageDownloadProgressed( PackageInfo* info, int percent);
	virtual void onPackageChanged( tPackageStatus newStatus, PackageInfo* info);
	virtual void onPackageDeleted( PackageInfo* info );
	bool hasASyncedController();

	/// <summary>
	/// Launches widget
	/// </summary>
	void launchErrorWidget(ErrorID errorCode, bool autoCorrect, int autoClose, String titleOverRide, String descOverRide, String cutomGraphic);

	/// <summary>
	/// Sets all the appropriate lock outs
	/// </summary>
	virtual void lockOutInput( float wait );

	/// <summary> 
	/// Hotspot management
	/// </summary> 
	virtual void setupHotSpots();
	virtual GameObject* getDefaultHighLight();

	/// <summary> 
	/// Custom creation and parsing of advanced objects from Unity
	/// </summary> 
	static GameObject* instantiate( String prefabName );
	static GameObject* createObject( FILE* fp, String objectType );
	static GameObject* deSerializeNext( FILE* fp );

	void 	stopAllVo();

	void	stopAllMusic();


	void updateAudio(float dt );


	void			keyBoardOverride(GameObject* highlight);

	void			setCurrentHighlight(GameObject *highlight);

	bool 			hasAConnectedController();
	/// <summary> 
	/// Getters/Setters for tracking feature
	/// </summary> 
	int			getNumControllers();
	bool 		isWandEnabled();
	GameObject* getCurrentHighlight();
	void		setTrackedObjects(ObjectArray *objects);
	void		addTrackedObjects(ObjectArray *objects);
	void 		removeTrackedObjects(ObjectArray *objects);
	void 		showModalDialog( DialogType dialogType, DialogOptions options);
	void		cancelCurrentDialog();
	bool		isShowingModalDialog();
	void 		debugLogTrackedObjects();
	void 		removeAllTrackedObjects();
	void 		removeSelection();

protected:

	LaunchAppState lastStateOnExit;
	LaunchParameters requestedState;


	bool 			wandEnabled;
	ObjectArray* 	cameras;
	ObjectArray* 	trackedObjects;
	WandObject* 	wand;
	GameObject* 	worldRoot;
	GameObject* 	currentHighlight;
	DialogObject*	currentModal;
	GameObject*		preDialogSelection;
	CAudioMPI 		audio_mpi;
	float 			clickLockOut;
	float 			selectionLockOut;
	bool			analogTrackOffScreen;
	float 			analogProjectionWeight;
	float			analogMaxDistance;
	std::vector<HWController*> controllers;
	float 			launchTimer;
	ObjectArray		playingSounds;

	void reloadLastState();
private:
	ObjectArray*    	preDialogTrackedObjects;

	void privateInit();
};

}

#endif /* SCENE_H_ */


#ifndef BASERENDERER_H_
#define BASERENDERER_H_

#include "SIEngine/Objects/BaseObject.h"
#include "SIEngine/Objects/CameraObject.h"

#include <BrioOpenGLConfig.h>
#include <GLES2/gl2ext.h>
#include <GLES2/gl2.h>
#include <EGL/egl.h>

#include "SIEngine/Core/Scene.h"
#include "SIEngine/Resources/FontData.h"

using LeapFrog::Brio::BrioOpenGLConfig;

namespace SICore
{

class BaseRenderer : public BaseObject
{
	CLASSEXTENDS(BaseRenderer, BaseObject);
public:
	BaseRenderer();
	virtual ~BaseRenderer();

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void render(Scene *scene) = 0;

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void render(GameObject* renderable) = 0;

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void suspend() = 0;

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void resume() = 0;

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void clearBuffers() = 0;

	/// <summary>
	/// Gets rid of the cached 'zsorted' scene graph
	/// </summary>
	virtual void markSceneGraphDirty() = 0;

	/// <summary>
	/// Represents the sort order of the main camera
	/// </summary>
	virtual bool isLeftHanded();

	static int renderCount;
};

}
#endif

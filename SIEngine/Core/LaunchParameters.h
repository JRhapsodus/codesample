#ifndef LAUNCHPARAMETERS_H_
#define LAUNCHPARAMETERS_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Utilities/Constants.h"

using namespace SIUtils;

namespace SICore
{

class LaunchParameters : public BaseObject
{
	CLASSEXTENDS(LaunchParameters, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	LaunchParameters();
	LaunchParameters(AppID fromApp, AppID toApp, AppState requested);
	virtual ~LaunchParameters();

	void debugLog();
	AppID from;
	AppID to;
	AppState loadToState;
};

}
#endif

#include "SerializedObjectInfo.h"
#include "SIEngine/Core/Exception.h"
#include "SIEngine/Include/SIObjects.h"

namespace SICore
{



//****************************************************************************************
// Reads initial meta data of struct
//****************************************************************************************
SerializedObjectInfo* SerializedObjectInfo::readInfo( FILE* fp )
{
	char name[500];
	char sceneGraphName[500];
	char type[250];
	int culling;
	int tracked;
	int level;

	if( ( fscanf(fp, "%d", &level) == 1) && (fscanf(fp, "%499s %d %499s %d", name, &culling, sceneGraphName, &tracked) == 4))
	{
		SerializedObjectInfo* info = new SerializedObjectInfo();
		String sName = name;
		sName.replace("\"", "");
		info->itemName = sName;
		String sgName = sceneGraphName;
		sgName.replace("\"","");
		info->itemSceneGraphName = sgName;
		info->cullingLayer = culling;
		info->trackObject = tracked;
		info->sceneGraphLevel = level;

		if( fscanf(fp, "%f %f %f %f %f %f %f %f %f %249s",
				&info->position.x, &info->position.y, &info->position.z,
				&info->scale.x, &info->scale.y, &info->scale.z,
				&info->rotation.x, &info->rotation.y, &info->rotation.z, type) == 10 )
		{
			info->itemType = type;
//			info->debugLog();
			return info;
		}

		ThrowException::deserializeException();
	}

	return NULL;
}

//****************************************************************************************
// Fills 9 floats for transform data
//****************************************************************************************
void SerializedObjectInfo::fillObject( SerializedObjectInfo* info, GameObject* newObject )
{
	newObject->name = info->itemName;
	newObject->sceneGraphName = info->itemSceneGraphName;
	newObject->setCullingLayerRecursive(info->cullingLayer);
	newObject->setPosition(info->position);
	newObject->setScale(info->scale);
	newObject->setRotation(info->rotation);
	newObject->originalScale = info->scale;

	newObject->originalState.originalTransform.setPosition(newObject->getPosition());
	newObject->originalState.originalTransform.setScale(newObject->getScale());
	newObject->originalState.originalTransform.setRotation(newObject->getRotation());
}

void SerializedObjectInfo::debugLog()
{
	printf("SerializedObjectInfo::printInfo\n");
	printf("--------            name : %s\n", itemName.str());
	printf("--------  sceneGraphName : %s\n", itemSceneGraphName.str());
	printf("--------   trackedObject : %d\n", trackObject);
	printf("--------    cullingLayer : %d\n", cullingLayer);
	printf("--------        position : {%f,%f,%f}\n", position.x, position.y, position.z);
	printf("--------           scale : {%f,%f,%f}\n", scale.x, scale.y, scale.z);
	printf("--------        rotation : {%f,%f,%f}\n", rotation.x, rotation.y, rotation.z);
	printf("--------          typeOf : {%s}\n", itemType.str());
}

}

#ifndef QTRENDERER_H_
#define QTRENDERER_H_

#include "SIEngine/Core/BaseRenderer.h"
#include <BlitBuffer.h>
#include <FontMPI.h>
#include "Glasgow/Data/VideoInfo.h"
#include "SIEngine/Objects/QtSpriteObject.h"
#include "SIEngine/Objects/QtTextObject.h"
#include "Glasgow/Renderables/QtRectObject.h"



namespace SICore
{

class QtRenderer : public BaseRenderer
{
	CLASSEXTENDS( QtRenderer, BaseRenderer);
public:
	QtRenderer();
	virtual ~QtRenderer();


	static QtRenderer* getInstance();
	void blitPng(CBlitBuffer &blitObject, int left, int right, int top, int bottom, int x, int y);

	/// <summary>
	/// Rendering functions
	/// </summary>
	virtual void render(GameObject* renderable);

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void render(Scene *scene);

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void suspend();

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void resume();

	/// <summary>
	/// Base renderer features
	/// </summary>
	virtual void clearBuffers();

	/// <summary>
	/// Gets rid of the cached 'zsorted' scene graph
	/// </summary>
	virtual void markSceneGraphDirty();


	void cleanupVideoSurface(VideoInfo* info);

	/// <summary>
	/// Creates a layer to render video onto, and passed back through the VideoInfo
	/// </summary>
	void createVideoHandle( VideoInfo* videoInfo );

	/// <summary>
	/// Presents back buffer to screen
	/// </summary>
	void swapBuffers();

	/// <summary>
	/// Helper function which determines the real screen size to use when renering videos
	/// </summary>
	Size getVideoDimensions(Size sourceSize, Size targetSize);

	/// <summary>
	/// Blit sprite
	/// </summary>
	void blitSprite(QtSpriteObject* sprite);
	void blitRectObject(QtRectObject* object);
	void blitText(QtTextObject* object);

	/// <summary>
	/// Helper function to get string bounds
	/// </summary>
	Rect getStringBounds( String string );

	/// <summary>
	/// Draws string to screen
	/// </summary>
	void drawString( String string, float x, float y, U32 color, bool underline );

	/// <summary>
	/// Get video rect
	/// </summary>
	Rect getVideoRect();

private:
	/// <summary>
	/// Video rect
	/// </summary>
	Rect videoRect;

	tFontHndl mainFont;
	int iCurrentBuffer;
	/// <summary>
	/// MPI to render text
	/// </summary>
	CFontMPI 			fontManager;

	/// <summary>
	/// MPI to create textures for rendering
	/// </summary>
	CDisplayMPI 		displayManager;

	/// <summary>
	/// Scene graph
	/// </summary>
	ObjectArray* 		flatGraph;

	/// <summary>
	/// Screen buffer to render onto
	/// </summary>
	CBlitBuffer 		mScreenBuffers[2];

	/// <summary>
	/// Display handles to rendering layers
	/// </summary>
	tDisplayHandle		uiDisplayHandles[2];
};

}
#endif

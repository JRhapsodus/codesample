#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include "SIEngine/Core/StringClass.h"

namespace SICore
{

/// <summary> 
/// Generic exception
/// </summary> 
class Exception : public BaseObject
{
	CLASSEXTENDS(Exception, BaseObject);
	ADD_TO_CLASS_MAP;
private:
	String m_str;

public:
	Exception( const String& str );
	Exception( LPCTSTR str );
	~Exception();
	virtual String toString();
};

/// <summary> 
/// Thrown by scene when parsing unity file import
/// </summary> 
class DeserializeException : public Exception
{
	CLASSEXTENDS(DeserializeException, Exception);
public:
	DeserializeException(String className) : super("Could not deserialize object: " + className) {}
	DeserializeException() : super("Could not deserialize object") {}
	~DeserializeException(){}
};

/// <summary> 
/// Thrown when IShader fails to compile
/// </summary> 
class ShaderNotCompiledException : public Exception
{
	CLASSEXTENDS(ShaderNotCompiledException, Exception);
public:
	ShaderNotCompiledException(String shaderName) : super(String("Shader could not compile ") + shaderName ) {}
	~ShaderNotCompiledException(){}
};

/// <summary> 
/// Throw when access to fopen returns null
/// </summary> 
class FileNotFoundException : public Exception
{
	CLASSEXTENDS(FileNotFoundException, Exception);
public:
	FileNotFoundException(String fileName) : super(String("File not found ") + fileName ) {}
	~FileNotFoundException(){}
};

/// <summary> 
/// Throw when an opengl error occurs
/// </summary> 
class OpenGlException : public Exception
{
	CLASSEXTENDS(OpenGlException, Exception);
public:
	OpenGlException(int errorCode) : super(String("OpenGL Error ") + errorCode ) {}
	~OpenGlException(){}
};

/// <summary>
/// Throw when a function or setting is not yet supported
/// </summary>
class UnsupportedException : public Exception
{
	CLASSEXTENDS(UnsupportedException, Exception);
public:
	UnsupportedException(String objName) : super(String("Not yet supported ") + objName ) {}
	~UnsupportedException(){}
};

/// <summary>
/// Throw when a parameter is required to abide a strict contract
/// </summary>
class InvalidApiException : public Exception
{
	CLASSEXTENDS(InvalidApiException, Exception);
public:
	InvalidApiException() : super("Invalid Use of Api") {}
	~InvalidApiException(){}
};


/// <summary>
/// Throw when a parameter is required to abide a strict contract
/// </summary>
class InvalidParameterException : public Exception
{
	CLASSEXTENDS(SceneGraphException, Exception);
public:
	InvalidParameterException(String parameterName) : super(String("Invalid Parameter: ") + parameterName ) {}
	~InvalidParameterException(){}
};

/// <summary> 
/// Throw when an object gets added to the scene graph and already has a parent
/// </summary> 
class SceneGraphException : public Exception
{
	CLASSEXTENDS(SceneGraphException, Exception);
public:
	SceneGraphException(String objName) : super(String("Error modifying sceneGraph for ") + objName ) {}
	~SceneGraphException(){}
};

class ThrowException
{
public:
	static void deserializeException(void)
	{
		DeserializeException exc();
	}

	static void deserializeException(String className)
	{
		DeserializeException exc(className);
	}

	static void shaderNotCompiledException(String shaderName)
	{
		ShaderNotCompiledException exc(shaderName);
	}

	static void fileNotFoundException(String fileName)
	{
		FileNotFoundException exc(fileName);
	}

	static void openGlException( int errorCode)
	{
		OpenGlException exc(errorCode);
	}


	static void invalidParameterException(String objName)
	{
		InvalidParameterException exc(objName);
	}

	static void unsupportedException(String parameterName)
	{
		UnsupportedException exc(parameterName);
	}

	static void sceneGraphException(String objName)
	{
		SceneGraphException exc(objName);
	}

	static void invalidApiException()
	{
		InvalidApiException esc();
	}

	static void exception( String str )
	{
		Exception esc(str);
	}
};

}

#endif /* EXCEPTION_H_ */

#ifndef SEMAPHORE_H_
#define SEMAPHORE_H_

#include <pthread.h>
#include <stdint.h>
#include <errno.h>

namespace SICore
{

class Semaphore
{
public:
	bool autoReset;
	pthread_cond_t variable;
	pthread_mutex_t mutex;
	bool state;
};

Semaphore* createSemaphore(bool manualReset = false, bool initialState = false);
int destroySemaphore(Semaphore* event);
int waitForSemaphore(Semaphore* event, uint64_t milliseconds = -1);
int unlockedWaitForSemaphore(Semaphore* event, uint64_t milliseconds);
int setSemaphore(Semaphore* event);
int resetSemaphore(Semaphore* event);

}
#endif

#include "SIEngine/Core/User.h"
#include <ProfileDscFile.h>
#include "SIEngine/Managers/UsersManager.h"
#include "SIEngine/Utilities/AssetLibrary.h"
#include "SIEngine/Core/Exception.h"
#include "SIEngine/Include/SIManagers.h"

namespace SICore
{
/// <summary>
/// Construct from loading
/// </summary>
User::User(tSlotID slotID) : PlayerInfo(slotID)
{
	if ( slotID > 6 && slotID != kSlotIDGuest)
	{ ThrowException::exception("Invalid SLot ID");	}
	printf("User::User(slotID)\n");
	if ( isDummyProfile() )
	{
		CPath cTheme = "UnderWater";
		CPath cProfilePic = "AvatarItemNotChosen";
		CPath cGender = "female";
		CPath cBirthDate = "01-01-1994";
		String displayName_ = "TEST PROFILE";
		SetName(displayName_.str());
		SetGender(cGender);
		SetTheme(cTheme);
		SetProfilePicture(cProfilePic);
		SetGrade(254);
		SetBirthdate(cBirthDate);
	}

	slotID_ = slotID;


	String theme = GetTheme().c_str();
	if( theme.isEmpty() )
	{
		CPath cTheme = "UnderWater";
		CPath cProfilePic = "AvatarItemNotChosen";
		SetTheme(cTheme);
		SetProfilePicture(cProfilePic);
	}
}

/// <summary>
/// Construct for new user
/// </summary>
User::User(String displayName_, tSlotID slotID) : PlayerInfo(slotID)
{
	printf("User::User name, slotID%d\n", slotID);
	if ( slotID > 6 && slotID != kSlotIDGuest)
	{ ThrowException::exception("Invalid SLot ID");	}

	//Default profile settings
	CPath cTheme = "UnderWater";
	CPath cProfilePic = "AvatarItemNotChosen";
	CPath cGender = "female";
	CPath cBirthDate = "01-01-1994";
	slotID_ = slotID;

	//this call doesn't work on constructor, and Leap refuses to fix
	//so we're going to bypass them
	//SetParentSneakPeekShow(true);
	if(SystemSettings::getSystemLocale() == SystemLocaleUnitedStates)
		SetUInt(Constants::Parameters::keySneakPeeks.str(), 1);
	else
		SetUInt(Constants::Parameters::keySneakPeeks.str(), 0);

	SetName(displayName_.str());
	SetGender(cGender);
	SetTheme(cTheme);
	SetProfilePicture(cProfilePic);
	SetGrade(254);
	SetBirthdate(cBirthDate);
}

User::~User()
{
	printf("User::~User()\n");
}


bool User::needsTutorial()
{
	String avatarName = getAvatar();
	return avatarName.equalsIgnoreCase("AVATARITEMNOTCHOSEN");
}

/// <summary>
/// </summary>
tSlotID User::getPlayerSLot()
{
	return slotID_;
}

/// <summary>
///  Checks to see if the user is a guest
/// </summary>
bool User::isGuest()
{
	if( slotID_ == kSlotIDGuest )
		return true;
	if( getDisplayName().equalsIgnoreCase(Constants::UI::guestProfileName) )
		return true;

	return false;
}


/// <summary>
/// Creates a user guest
/// </summary>
User* User::guest()
{ return new User(Constants::UI::guestProfileName, kSlotIDGuest); }

/// <summary>
/// Api call to load user even though constructor loads, the nomenclature
/// is still useful to distinguish
/// </summary>
User* User::loadUser( tSlotID slotID)
{ return new User(slotID); }

/// <summary>
/// Static call here as UsersManager has a call to add a User based on reference
/// and UI will create an instance to fill the initial values with. This is done
/// as opposed to a call with 9+ parameters in UsersManager::addUser(...)
/// </summary>
User* User::newUser(String userName, tSlotID slotID)
{ return new User(userName, slotID); }

/// <summary>
/// wrapper call into id, not necessarily the slot id,
/// not sure what this is for in Leap's system
/// </summary>
int User::getID()
{ return GetId(); }

/// <summary>
/// Outputs all fields to console
/// </summary>
void User::debugLog()
{

	printf("User named (%s)\n", GetName().c_str());
	printf("\t GetId %d\n", GetId());
	printf("\t GetGrade %d\n", GetGrade());
	printf("\t GetBadges %d\n", GetBadges());
	printf("\t GetTokens %d\n", GetTokens());
	printf("\t GetGender %d\n", GetGender());
	printf("\t GetBirthDate %s\n", GetBirthDate().c_str());
	printf("\t GetBirthMonth %d\n", GetBirthMonth());
	printf("\t GetBirthDay %d\n", GetBirthDay());
	printf("\t GetBirthYear %d\n", GetBirthYear());
	printf("\t GetProfilePicture %s\n", GetProfilePicture().c_str());
	printf("\t GetTheme %s\n", GetTheme().c_str());
	printf("\t GetSneakPeeksEnabled %s\n", GetParentSneakPeekShow() ? "true" : "false");

}

/// <summary>
///  Check to determine wether the profile file has consumer data
/// </summary>
bool User::isDummyProfile()
{
	CString name = GetName();

	if ( name.size() < 1 )
	{ return true; }

	return false;
}

/// <summary>
/// Wrapper function for protected PlayerInfo calls
/// </summary>
bool User::save()
{
	if( !isGuest() )
	{
		if ( !Save() )
		{ ThrowException::exception("Problem saving user profile\n"); }
		else
		{
			printf("User::save() for user %s\n", getDisplayName().str() );
			//Call required by LeapFrog for web sync: GLUI-183
			pthread_t threadID;
			pthread_create(&threadID, null, &User::syncProfilesToWebWorker, null);

			return true;
		}
	}

	return false;
}

/// <summary>
/// For packagemanager to sync the user profiles when they change
/// </summary>
void* User::syncProfilesToWebWorker(void* arg)
{
	RioPkgManager* packageManager = PackageManager::Instance();
	packageManager->UpdateProfiles(true);
}

/// <summary>
/// Returns the whether the avatar is set
/// </summary>
bool User::userHasChosenAvatar()
{ return (getAvatar() == "AvatarItemNotChosen"); }

/// <summary>
/// Returns the display name field
/// </summary>
String User::getDisplayName()
{ return GetName().c_str(); }

String User::getBirthdate()
{ return GetBirthDate().c_str();}

int	User::getBirthDay()
{ return (int)GetBirthDay();}

int	User::getBirthMonth()
{ return (int)GetBirthMonth();}

int	User::getBirthYear()
{ return (int)GetBirthYear();}

String User::getGender()
{ return ((int)GetGender() == Constants::System::genderMaleEnum) ? Constants::System::genderMaleKey : Constants::System::genderFemaleKey;}

/// <summary>
/// Returns the first name field
/// </summary>
String User::getFirstName()
{ return GetName().c_str(); }

/// <summary>
/// Returns if sneak peaks are enabled for this user
/// </summary>
bool User::getSneakPeeksEnabled()
{
	if( isGuest() )
		return false;

	return GetParentSneakPeekShow();
}

/// <summary>
/// Returns the grade int
/// </summary>
int User::getGrade()
{ return GetGrade(); }

/// <summary>
/// Returns the avatar prefab name
/// </summary>
String User::getAvatar()
{ return GetProfilePicture().c_str(); }

/// <summary>
/// Returns the theme name
/// </summary>
String User::getTheme()
{ return GetTheme().c_str(); }

/// <summary>
/// Sets the field for display name
/// </summary>
void User::setDisplayName(String displayName_)
{ SetName(displayName_.str()); }

bool User::removeProfile()
{
	printf("User::removeProfile\n");
	return RemoveProfile();
}

/// <summary>
/// Sets whether sneak peaks are enabled
/// </summary>
void User::setSneakPeeksEnabled(bool enabled)
{ SetParentSneakPeekShow(enabled);}

/// <summary>
/// Sets gender
/// </summary>
void User::setGender(String gender)
{
	if(gender != Constants::System::genderMaleKey && gender != Constants::System::genderFemaleKey)
		ThrowException::unsupportedException(gender);

	int genderEnum = (gender == Constants::System::genderMaleKey) ? Constants::System::genderMaleEnum : Constants::System::genderFemaleEnum;

	SetGender(genderEnum);
}

/// <summary>
/// Sets the fields for setting birthdate
/// </summary>
void User::setBirthDate(String day, String month, String year)
{
	String date = day+month+year;
	SetBirthdate(date.str());
}

/// <summary>
/// Sets the birth day
/// </summary>
void User::setBirthDay(int day)
{ SetBirthDay(day);}

/// <summary>
/// Sets the birth month
/// </summary>
void User::setBirthMonth(int month)
{ SetBirthMonth(month);}

/// <summary>
/// Sets the birth year
/// </summary>
void User::setBirthYear(int year)
{ SetBirthYear(year);}

/// <summary>
/// Sets the field for grade
/// </summary>
void User::setGrade(int grade_)
{ SetGrade(grade_); }

/// <summary>
/// Sets the field for avatar usage
/// </summary>
void User::setAvatar(String avatarName_)
{
	printf("Setting new avatar %s for %s\n", avatarName_.str(), getAvatar().str());
	CPath cProfilePic = avatarName_.str();
	SetProfilePicture(cProfilePic);
}

/// <summary>
/// Sets the field for theme
/// </summary>
void User::setTheme(String userTheme)
{
	printf("Setting new theme %s for %s\n", userTheme.str(), getFirstName().str());
	CPath cPathTheme = userTheme.str();
	SetTheme(cPathTheme);
}

}

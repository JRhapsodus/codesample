#ifndef SERIALIZEDOBJECTINFO_H_
#define SERIALIZEDOBJECTINFO_H_

#include "SIEngine/Objects/GameObject.h"
#include "SIEngine/Geometry/Vec3f.h"
using namespace SIGeometry;

namespace SICore
{

/// <summary>
/// This represents the base GameObject's data as read in from the Unity exporter.
/// It will have the cullingLayer, name, custom class name, whether it is auto tracked
/// into the wand / analog stick targets, and the 9 floats representing its transform
/// </summary>
class SerializedObjectInfo : public BaseObject
{
	CLASSEXTENDS(SerializedObjectInfo, BaseObject);

public:
	SerializedObjectInfo() : super()
	{
		cullingLayer = -1;
		trackObject = 0;
		itemName = "";
		itemSceneGraphName = "";
		itemType = "GameObject";
		sceneGraphLevel = 0;
	}
	virtual ~SerializedObjectInfo() { }

	/// <summary> 
	/// Parsing operations
	/// </summary> 
	static SerializedObjectInfo* readInfo( FILE* fp );

	/// <summary>
	/// Sets all corresponding variables in the game object with the info struct data
	/// </summary>
	static void fillObject( SerializedObjectInfo* info, GameObject* newObject );


	void debugLog();

	/// <summary> 
	/// Culling layer, tells rendering system which camera this object can be seen by
	/// </summary> 
	int cullingLayer;

	/// <summary>
	/// Normally a bool, but our file parser sees int more conveniently:
	/// Set to 1 if this object is added to the Wand / Analog tracking list on Scene load
	/// </summary>
	int trackObject;

	/// <summary>
	/// How deep into the scene graph this is. Used to rebuild the Unity scene graph via a
	/// custom post order tree search that is alphabetized sorted
	/// </summary>
	int sceneGraphLevel;

	/// <summary>
	/// Name of object
	/// </summary>
	String itemName;

	/// <summary>
	/// Scene graph name of object
	/// </summary>
	String itemSceneGraphName;

	/// <summary>
	/// Class name of object for the custom deserializing
	/// </summary>
	String itemType;

	/// <summary>
	/// 3 floats representing position
	/// </summary>
	Vec3f position;

	/// <summary>
	/// 3 floats representing scale
	/// </summary>
	Vec3f scale;

	/// <summary>
	/// 3 floats representing euler based rotation
	/// </summary>
	Vec3f rotation;
};

}

#endif /* SERIALIZEDOBJECTINFO_H_ */


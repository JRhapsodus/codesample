#include "SIEngine/Core/Scene.h"
#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Objects/MeshObject.h"
#include "SIEngine/Core/SerializedObjectInfo.h"
#include "SIEngine/Rendering/TextureManager.h"
#include "SIEngine/Core/OpenGlRenderer.h"
#include "SIEngine/Objects/QtButtonObject.h"
#include "SIEngine/Objects/QtTextObject.h"
#include "SIEngine/Include/GlasgowControls.h"
#include "Glasgow/Scenes/SceneManager.h"
#include "Glasgow/GlasgowApp.h"
#include <GLES2/gl2.h>
#include <unistd.h>
#include <sys/resource.h>
#include <iostream>
#include <fstream>


using namespace SICore;
using namespace SIUtils;
using namespace SIRendering;
using namespace Glasgow;

namespace SICore
{

/**
 * Returns the peak (maximum so far) resident set size (physical
 * memory use) measured in bytes, or zero if the value cannot be
 * determined on this OS.
 */
size_t getPeakRSS( )
{
    /* BSD, Linux, and OSX -------------------------------------- */
    struct rusage rusage;
    getrusage( RUSAGE_SELF, &rusage );
    return (size_t)(rusage.ru_maxrss * 1024L);
}

/**
 * Out the resident set size of the process
 */
void logMemoryFootPrint()
{
	printf("Log Memory foot print\n");
	int tSize = 0, resident = 0, share = 0;
	std::ifstream buffer("/proc/self/statm");
	buffer >> tSize >> resident >> share;
	buffer.close();

	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024;
	double rss = resident * page_size_kb;
	printf("\t\tRSS - %.0f kB\n", rss);

	double shared_mem = share * page_size_kb;
	printf("\t\tShared Memory - %.0f kB\n", shared_mem);
	printf("\t\tPrivate Memory - %.0f kB\n", (rss - shared_mem));
	printf("\t\tBaseObject Count - %d\n", BaseObject::gObjectCount);
	printf("\t\tGlobalRef Count - %d\n", BaseObject::gRefCount);


	for ( std::map<std::string, int>::iterator iter = BaseObject::gRefCountMap.begin();	iter != BaseObject::gRefCountMap.end(); iter++ )
	{
		std::string key(iter->first);
		printf("[%d] \t\t\t [%s]\n", iter->second, key.c_str());
	}
}

/**
 * Returns the current resident set size (physical memory use) measured
 * in bytes, or zero if the value cannot be determined on this OS.
 */
size_t getCurrentRSS( )
{
    /* Linux ---------------------------------------------------- */
    long rss = 0L;
    FILE* fp = NULL;
    if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL )
        return (size_t)0L;      /* Can't open? */
    if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
    {
        fclose( fp );
        return (size_t)0L;      /* Can't read? */
    }
    fclose( fp );
    return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE);
}


//********************************************************************************
// Really this should be deprecated as most scene should be loaded from Unity layout
// but for the few widgets and simple scenes, I will keep this for legacy
//********************************************************************************
Scene::Scene() : super()
{
	loaded = false;
	privateInit();
	addCamera( TRelease<CameraObject>(CameraObject::orthographic(true)));
	setWorldRoot(TRelease<GameObject>(new GameObject("WorldRoot")));
	SystemSettings::setTheme("UnderWater");
	getCamera(0)->name = "Camera";
	getCamera(0)->clearOptions = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
	getCamera(0)->clearColor = Color(0,0,0,1);
	worldRoot->addChild(getCamera(0));
	launchTimer = 0;
	TextureManager::getInstance()->loadPng("cursor");
}

void Scene::privateInit()
{
	cameras = new ObjectArray();
	trackedObjects = new ObjectArray();
	preDialogTrackedObjects = new ObjectArray();
	worldRoot = NULL;
	clickLockOut = 0;
	selectionLockOut = 0;
	currentHighlight = NULL;
	analogProjectionWeight = 1.15f;
	analogMaxDistance = 1.0f;
	analogTrackOffScreen = false;

	//hard wired for now due to cut feature
	//JLF: I still want this architecture in place for a future release
	wandEnabled = false;
	currentModal = NULL;

	lastStateOnExit = SystemSettings::loadLaunchState();
	requestedState = lastStateOnExit.launchParameters;
	lastStateOnExit.debugLog();

	LaunchAppState freshLoad = LaunchAppState();
	SystemSettings::saveLaunchState(freshLoad);
}

Scene::~Scene()
{
	size_t currentSize = getCurrentRSS( );
	size_t peakSize    = getPeakRSS( );
	printf("RSS: %d ==> PEAK: %d\n", currentSize, peakSize);


	printf("Scene::~Scene()\n");
	preDialogTrackedObjects->removeAllElements();
	SAFE_RELEASE(preDialogTrackedObjects);
	trackedObjects->removeAllElements();
	SAFE_RELEASE(trackedObjects);
	cameras->removeAllElements();
	SAFE_RELEASE(cameras);
	SAFE_RELEASE(worldRoot);
	printf("Scene::~Scene() Final\n");
}

void Scene::onDownloadError()
{



	onPendingDownloadsChanged();
}

void Scene::onPackageDownloadProgressed( PackageInfo* info, int percent)
{
	//base
}

void Scene::onPackageDeleted( PackageInfo* info )
{


}


void Scene::onPackageChanged( tPackageStatus newStatus, PackageInfo* info)
{
	//base
}

void Scene::onPendingDownloadsChanged()
{
	//base
}

//************************************************************************
// Load a scene by data file
//************************************************************************
Scene::Scene(String path, String stringTable ) : super()
{
	StringsManager::getInstance()->loadStrings(stringTable);

	printf("Scene::Scene\n");
	privateInit();
	SystemSettings::setTheme("UnderWater");

	//************************************************************************
	// Get file handle for parsing
	//************************************************************************
	printf("Loading scene file: %s\n", path.str());
	FILE* fp = AssetLibrary::getFileHandle( path, "r" );

	//************************************************************************
	// Prepare scene graph stack, for relative Post Order tree traversal
	//************************************************************************
	ObjectArray* sceneGraphStack = new ObjectArray();
	int sceneGraphLevel = 0;
	int oldSceneGraphLevel = -1;
	GameObject* previousObject = NULL;
	SerializedObjectInfo* objInfo = NULL;

	//************************************************************************
	//Read first entry
	//************************************************************************
	objInfo = SerializedObjectInfo::readInfo(fp);
	printf("Loading Scene\n");
	while ( objInfo != null )
	{
		GameObject* newObject = Scene::createObject( fp, objInfo->itemType );
		SerializedObjectInfo::fillObject(objInfo, newObject);
		newObject->onRefreshLocalization();

		//************************************************************************
		// Features of the scene are automatic tracking and cameras
		//************************************************************************
		if( newObject->typeOf(CameraObject::type()))
		{ addCamera((CameraObject*)newObject); }
		else if ( newObject->typeOf( WandObject::type() ) )
		{ wand = (WandObject*)newObject; }

		if( objInfo->trackObject > 0 )
		{ trackedObjects->addElement(newObject); }

		//************************************************************************
		// Fill in the game object now
		//************************************************************************
		sceneGraphLevel = objInfo->sceneGraphLevel;

		//************************************************************************
		// Now that object has been created attach to scene graph
		//************************************************************************
		if( getWorldRoot() == NULL )
		{ this->setWorldRoot(newObject); }
		else
		{
			if( sceneGraphLevel > oldSceneGraphLevel )
			{ sceneGraphStack->addElement(previousObject); }
			else if ( sceneGraphLevel < oldSceneGraphLevel )
			{
				for( int i =0;i<(oldSceneGraphLevel-sceneGraphLevel); i++ )
				{ sceneGraphStack->removeElementAt(sceneGraphStack->getSize()-1); }
			}

			((GameObject*)sceneGraphStack->lastElement())->addChild(newObject);
		}

		newObject->release();
		previousObject = newObject;
		oldSceneGraphLevel = sceneGraphLevel;

		//************************************************************************
		// Lets read next line
		//************************************************************************
		SAFE_RELEASE(objInfo);
		objInfo = SerializedObjectInfo::readInfo(fp);
	}

	printf("Done loading Scene\n");
	fclose(fp);
	sceneGraphStack->release();
}

void Scene::reloadLastState()
{
	lastStateOnExit = SystemSettings::loadLaunchState();
	requestedState = lastStateOnExit.launchParameters;
	lastStateOnExit.debugLog();
	LaunchAppState freshLoad = LaunchAppState();
	SystemSettings::saveLaunchState(freshLoad);
}
//************************************************************************
// Prints the name of all tracked objects
//************************************************************************
void Scene::debugLogTrackedObjects()
{
	for( int i = 0; i<trackedObjects->getSize(); i++ )
	{
		GameObject* trackedObject = (GameObject*)trackedObjects->elementAt(i);
		if( trackedObject != NULL )
		{
			printf("Tracked Object: %s is visible %d\n", trackedObject->name.str(), trackedObject->isVisibleHierarchy());
		}
	}
}

//************************************************************************
// Instantiates modal dialog with options
//************************************************************************
LaunchAppState Scene::getLastExitState()
{ return lastStateOnExit; }

//************************************************************************
// Instantiates modal dialog with options
//************************************************************************
void Scene::showModalDialog(DialogType dialogType, DialogOptions options)
{
	options.debugLog();
	lockOutInput(0.7f);
	preDialogSelection = currentHighlight;

	if( currentHighlight != null )
	{ printf("Setting postSelection to %s \n", preDialogSelection->name.str()); }

	trackedObjects->removeAllElements();

	// Cancel the current modal
	if(currentModal != null)
	{ currentModal->cancel(); }

	// create dialog
	currentModal = (DialogObject*)instantiate("Dialog");
	currentModal->setCullingLayerRecursive(getCamera(0)->getCullingMask());
	currentModal->dialogType = dialogType;
	currentModal->setup(dialogType, options);

	if( options.hasTwoButtons() )
	{
		trackedObjects->addElement(currentModal->button1);
		trackedObjects->addElement(currentModal->button2);
	}
	else
	{ trackedObjects->addElement(currentModal->button1); }

	//animate dialog
	currentModal->animateIn();
	worldRoot->addChild(currentModal);
	currentModal->release();

	if ( preDialogSelection != null && preDialogSelection->typeOf(ButtonObject::type()) )
	{ ((ButtonObject*)preDialogSelection)->setState(ButtonStateNormal); }

	GlasgowApp::getRenderer()->markSceneGraphDirty();
	currentHighlight = currentModal->button1;
	((ButtonObject*)currentHighlight)->setState(ButtonStateActive);

	printf("Scene::showModalDialog\n");
}

void Scene::cancelCurrentDialog()
{
	if(currentModal != null)
	{
		currentModal->cancel();
	}
}

bool Scene::isShowingModalDialog()
{
	return currentModal != null;
}

//************************************************************************
// Turns off currenthighlight
//************************************************************************
void Scene::removeSelection()
{
	onSelectionChange( currentHighlight, NULL );
	currentHighlight = NULL;
}

//***************************************************************************
// Deserializes the next object in the file
//***************************************************************************
GameObject* Scene::deSerializeNext( FILE* fp )
{
	GameObject* nextObject = NULL;
	SerializedObjectInfo* info = SerializedObjectInfo::readInfo(fp);
	nextObject = Scene::createObject(fp, info->itemType );
	SerializedObjectInfo::fillObject(info, nextObject);
	nextObject->onRefreshLocalization();
	SAFE_RELEASE(info);
	return nextObject;
}

/// <summary>
/// Scene::onCartridgeInserted()
/// </summary>
void Scene::onCartridgeReady()
{ }

/// <summary>
/// Scene::onCartridgeInserted()
/// </summary>
void Scene::onCartridgeInserted()
{ }

/// <summary>
/// Scene::onCameraPluggedIn()
/// </summary>
void Scene::onCameraPluggedIn()
{
	printf("Scene::onCameraPluggedIn()\n");
}

/// <summary>
/// Scene::onCartridgeInserted()
/// </summary>
void Scene::onCameraUnplugged()
{
	printf("Scene::onCameraUnplugged()\n");
}

void Scene::onDonwloadFinished( String packageID )
{


}

void Scene::onDonwloadStarted( String packageID )
{


}

/// <summary>
/// On download progressed
/// </summary>
void Scene::onDonwloadProgressed( String packageID, int downloadProgress )
{


}

/// <summary>
/// Scene::onCartridgeRemoved()
/// </summary>
void Scene::onCartridgeRemoved()
{ }

void Scene::onControllerConnected(HWController* controller)
{
	GlasgowApp::mainState->getAllControllers(controllers);
	printf("Scene::onControllerConnected\n");

	for( int i = 0; i<(int)controllers.size(); i++ )
	{
		HWController* controller = controllers[i];
		printf("Scene::onControllerConnected - Controller ID[%d] exists\n", controller->GetID());
	}
}

bool Scene::hasASyncedController()
{
	GlasgowApp::mainState->getAllControllers(controllers);
	return (controllers.size() != 0);
}

bool Scene::hasAConnectedController()
{
	GlasgowApp::mainState->getAllControllers(controllers);

	if( controllers.size() == 0 )
	{ return false; }

	for( int i = 0; i<(int)controllers.size(); i++ )
	{
		HWController* controller = controllers[i];
		if( controller->IsConnected() )
			return true;
	}

	return false;

}

void Scene::onControllerDisconnected(HWController* controller)
{
	printf(" Scene::onControllerDisconnected\n");
	GlasgowApp::mainState->getAllControllers(controllers);

}

void Scene::onControllerSynced(HWController* controller, bool success)
{
	printf("Scene::onControllerSynced\n");

	GlasgowApp::mainState->getAllControllers(controllers);
	if( controllers.size() == 0 )
	{ printf("Scene::onControllerSynced -- NO CONTROLLERS ! \n"); }

	if(success)
	{
		printf("Scene::onControllerSynced -- Controller Sync Success\n");
		for( int i = 0; i<(int)controllers.size(); i++ )
		{
			HWController* controller = controllers[i];
			printf("Scene::onControllerSynced -- Controller ID[%d] exists\n", controller->GetID());
		}
	}
	else
	{
		printf("Scene::onControllerSynced -- Controller Synce Failure\n");
	}
}


//************************************************************************
// Base scene on wand turned on notification
//************************************************************************
void Scene::onWandTurnedOn(HWController* controller)
{
	return;

	printf("Scene::onWandTurnedOn for controller ID %d\n", (int)controller->GetID());

	wandEnabled = true;

	if( wandEnabled )
	{
		removeSelection();
		wand->setPosition( Vec3f(0, 0, 0 ) );
	}
}

//************************************************************************
// Base scene on wand turned off notification
//************************************************************************
void Scene::onWandTurnedOff(HWController* controller)
{
	return;
	//printf("Scene::onWandTurnedOff for controller ID %d\n", (int)controller->GetID());
	wandEnabled = false;
}

//************************************************************************
//
//************************************************************************
void Scene::onNetworkError(String networkName, String networkError)
{
	//base class implementation
}


//************************************************************************
//
//************************************************************************
void Scene::onNetworkConnected( String networkName )
{

	//base class implementation
}

//************************************************************************
// Virtual override for creation of custom class objects from Unity
//************************************************************************
GameObject* Scene::createObject( FILE* fp, String itemType )
{
	GameObject* newObject = NULL;

	//************************************************************************
	//Now if we have a specific type we know what to create, or we can use a class
	//factory system which would be far superior to me
	//************************************************************************
	if( itemType == GameObject::type() )
	{  newObject = GameObject::deSerialize(fp); }
	else if ( itemType == SpriteObject::type() )
	{ newObject = SpriteObject::deSerialize(fp); }
	else if ( itemType == TextLabel::type() )
	{ newObject = TextLabel::deSerialize(fp); }
	else if ( itemType == CameraObject::type() )
	{ newObject = CameraObject::deSerialize(fp); }
	else if( itemType == MeshObject::type())
	{ newObject = MeshObject::deSerialize(fp); }
	else if ( itemType == TextButton::type())
	{ newObject = TextButton::deSerialize(fp); }
	else if ( itemType == RollOverButton::type())
	{ newObject = RollOverButton::deSerialize(fp); }
	else if ( itemType == WifiButton::type())
	{ newObject = WifiButton::deSerialize(fp); }
	else if ( itemType == HilightButton::type())
	{ newObject = HilightButton::deSerialize(fp); }
	else if( itemType == ContentItem::type())
	{ newObject = ContentItem::deSerialize(fp); }
	else if( itemType == DownloadItem::type())
	{ newObject = DownloadItem::deSerialize(fp); }
	else if( itemType == InstalledItem::type())
	{ newObject = InstalledItem::deSerialize(fp); }
	else if( itemType == ButtonObject::type())
	{ newObject = ButtonObject::deSerialize(fp); }
	else if( itemType == LoginButton::type())
	{ newObject = LoginButton::deSerialize(fp); }
	else if( itemType == DialogObject::type())
	{ newObject = DialogObject::deSerialize(fp); }
	else if( itemType == ProgressBarObject::type())
	{ newObject = ProgressBarObject::deSerialize(fp); }
	else if( itemType == GateLockObject::type())
	{ newObject = GateLockObject::deSerialize(fp); }
	else if( itemType == ThemeItem::type())
	{ newObject = ThemeItem::deSerialize(fp); }
	else if( itemType == AvatarItem::type())
	{ newObject = AvatarItem::deSerialize(fp); }
	else if( itemType == PurchasedItem::type())
	{ newObject = PurchasedItem::deSerialize(fp); }
	else if( itemType == CheckButton::type())
	{ newObject = CheckButton::deSerialize(fp); }
	else if( itemType == WandObject::type())
	{ newObject = WandObject::deSerialize(fp); }
	else if( itemType == AudioItem::type())
	{ newObject = AudioItem::deSerialize(fp); }
	else if( itemType == GalleryItem::type())
	{ newObject = GalleryItem::deSerialize(fp); }
	else if( itemType == SliderObject::type())
	{ newObject = SliderObject::deSerialize(fp); }
	else if( itemType == VerticalSlider::type())
	{ newObject = VerticalSlider::deSerialize(fp); }
	else if( itemType == QtSpriteObject::type())
	{ newObject = QtSpriteObject::deSerialize(fp); }
	else if( itemType == QtButtonObject::type())
	{ newObject = QtButtonObject::deSerialize(fp); }
	else if( itemType == QtTextObject::type())
	{ newObject = QtTextObject::deSerialize(fp); }
	else if( itemType == NetErrorObject::type())
	{ newObject = NetErrorObject::deSerialize(fp); }
	else if( itemType == FirmwareProgressBar::type())
	{ newObject = FirmwareProgressBar::deSerialize(fp); }
	else
	{ ThrowException::deserializeException(itemType); }

	return newObject;
}

//*********************************************************************************
// Timer expired
//*********************************************************************************
void Scene::onTimerExpired( TimerEvent* timer )
{
	//base implementation
}

//********************************************************************************
// Get camera for object
//********************************************************************************
CameraObject* Scene::getCamera( GameObject* forObject )
{
	if (forObject == NULL) return NULL;

	for( int i = 0; i<cameras->getSize(); i++ )
	{
		CameraObject* camera = getCamera(i);
		if( forObject->getCullingLayer() == camera->getCullingMask() )
			return camera;
	}

	return NULL;
}

//********************************************************************************
// Handle dialog messages, really meant to override to handle flows or options
//********************************************************************************
void Scene::onDialogHandled( DialogType type, DialogButtonOption option )
{
	printf("Scene::onDialogHandled\n");

	bool ignore = false;
	if( currentModal != null )
	{
		ignore = currentModal->options.ignoreOldTrackedObjects;
	}
	else
	{ ThrowException::exception("onDialogHandled called with currentModal set to NULL\n"); }

	if( !ignore )
	{
		printf("Setting selection back to preselection \n");

		currentHighlight = preDialogSelection;
		if ( currentHighlight != null && currentHighlight->typeOf(ButtonObject::type()) )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
	}
	else
	{
		printf("Ignoring old selection\n");
		preDialogSelection = null;
	}

	currentModal = null;
	setupHotSpots();
}

void Scene::onDialogCanceled( DialogType type )
{
	printf("Scene::onDialogCancelled\n");

	bool ignore = false;
	if( currentModal != null )
	{
		currentModal->options.debugLog();
		ignore = currentModal->options.ignoreOldTrackedObjects;
	}
	else
	{ ThrowException::exception("onDialogCancelled called with currentModal set to NULL\n"); }

	if( !ignore )
	{
		printf("Setting selection back to preselection \n");

		currentHighlight = preDialogSelection;
		if ( currentHighlight != null && currentHighlight->typeOf(ButtonObject::type()) )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
	}
	else
	{
		printf("Ignoring old selection\n");
		preDialogSelection = null;
	}

	currentModal = null;
	setupHotSpots();
}

void Scene::onEthernetPluggedIn()
{
	printf("Scene::onEthernetPluggedIn()\n");
	onNetworkConnected("Wired");
}

void Scene::onReturnToApp()
{
	reloadLastState();
}

void Scene::onEthernetUnplugged()
{
	printf("Scene::onEthernetUnplugged()\n");
	onNetworkError("Wired", "Disconnected");
}

//********************************************************************************
// Convenience function for accessing a specific camera by id index
//********************************************************************************
CameraObject* Scene::getCamera(int id)
{ return (CameraObject*)cameras->elementAt(id); }

//********************************************************************************
// Get highlighted item
//********************************************************************************
GameObject* Scene::getHighlightedItem()
{ return currentHighlight; }


//*************************************************************************
// Adds a scene camera
//*************************************************************************
void Scene::addCamera( CameraObject* camera )
{
	cameras->addElement( camera );

	if( cameras->getSize() > 1 )
		cameras->quickSort(&CameraObject::compareByRank);
}

//*************************************************************************
// Set's world root  transform object
//*************************************************************************
void Scene::setWorldRoot(GameObject *rootGameObject)
{
	SAFE_RELEASE(worldRoot);
	worldRoot = rootGameObject;
	worldRoot->addRef();
}

//*************************************************************************
// Print scene graph, debug utility
//*************************************************************************
void Scene::printSceneGraph()
{ printSceneGraph( worldRoot, 0 ); }

//*************************************************************************
// Debug helper which outputs the scene graph a. la Unity's Hierarchy tab
//*************************************************************************
void Scene::printSceneGraph( GameObject* node, int level )
{
	if (node == NULL)
	{ printf("Null node!!!\n"); }

	for( int i = 0; i<level; i++ )
	{ printf("\t"); }

	printf("-- (%s %d) %s '%s'\n", node->classname(), node->getRefCount(), node->name.str(), node->toString().str());

	for ( int i = 0; i<node->numChildren(); i++)
	{
		GameObject* child = node->childAtIndex(i);
		printSceneGraph(child, level+1);
	}
}

/// <summary>
/// Input from wand
/// </summary>
void Scene::onWandEvent(WandEvent *event)
{
	if (!wandEnabled)
	{ return; }

	//base implementation
	if( event->action == WandEventEnter )
	{ onWandEnter( event->sceneObject ); }
	else if ( event->action == WandEventLeave )
	{ onWandLeave( event->sceneObject ); }
}

//******************************************************************
// Public query for things like wand to ask the scene what is tracked
//******************************************************************
ObjectArray* Scene::getHotSpots()
{
	return trackedObjects;
}

//******************************************************************
// An override for each scene to determine its own hotspots to
// be tracked by the wand or the analog stick
//******************************************************************
void Scene::setupHotSpots()
{
	//base implementation, this is meant to override
}



//******************************************************************
// Occurs when Hint button is pressed
//******************************************************************
void Scene::onButtonHint()
{
}

//******************************************************************
// Occurs when home button is pressed
//******************************************************************
void Scene::onButtonHome()
{
	if ( wand != null )
	{ wand->getWorldPosition().debugLog(); }
}

//******************************************************************
// Occurs when A button is pressed
//******************************************************************
void Scene::onButtonPressAction()
{
	//base implementation meant to override for handling
	if( currentModal != NULL )
	{ currentModal->onActionPress(); }
}

//******************************************************************
// Occurs when B button is pressed
//******************************************************************
void Scene::onButtonPressBack()
{


}

//******************************************************************
// This is really for convenience if a derived scene really doesn't
// need to track each input device indipendently
//******************************************************************
void Scene::onSelectionChange(GameObject* oldObject, GameObject* newObject )
{
	//base implementation
}

//******************************************************************
// Occurs when wand enters object's screen bounds
//******************************************************************
void Scene::onWandEnter( GameObject* sceneObject )
{
	GameObject* oldHighlight = currentHighlight;
	currentHighlight = sceneObject;
	onSelectionChange(oldHighlight, currentHighlight);
}

//******************************************************************
// Occurs when wand leaves object's screen bounds
//******************************************************************
void Scene::onWandLeave( GameObject* sceneObject )
{
	GameObject* oldHighlight = currentHighlight;
	currentHighlight = NULL;
	onSelectionChange(oldHighlight, currentHighlight);
}

//******************************************************************
// Occurs when the analog stick triggers a change in highlighted
//******************************************************************
void Scene::onAnalogChange( GameObject* focused, GameObject* candidate )
{
	GameObject* oldHighlight = currentHighlight;

	if( candidate != NULL )
	{
		currentHighlight = candidate;
		onSelectionChange(oldHighlight, currentHighlight);
	}


}

//******************************************************************
// Occurs when the acceleration changes
//******************************************************************
void Scene::onAccelerationChange(Vec3f acceleration)
{
	//base implementation meant to override for handling
}

void Scene::stopAllMusic()
{
	for ( int i = 0; i<playingSounds.getSize(); i++ )
	{
		SoundInfo* info = (SoundInfo*)playingSounds[i];
		if( info->soundType == SoundTypeMusic )
		{
			audio_mpi.StopAudio(info->audioID, false);
			playingSounds.removeElementAt(i);
			i--;
		}
	}
}

//******************************************************************
// Loops music
//******************************************************************
void Scene::playMusic( String soundName )
{
	audio_mpi.StopAllAudio();
	tAudioID id = audio_mpi.StartAudio( AssetLibrary::getSoundPath(soundName).str(), 1000, kAudioOptionsLooped );
	TRelease<SoundInfo> soundInfo( new SoundInfo(id, soundName, SoundTypeMusic));
	playingSounds.addElement(soundInfo);
}

void Scene::instancePlayMusic( String soundName)
{
	Scene* scene = SceneManager::getInstance()->getScene();
	if( scene != null ){ scene->playMusic(soundName); }
}

//******************************************************************
// Plays a one time sound effect
//******************************************************************
void Scene::playSound( String soundName )
{

	//full path over ride, no need to ask assetlibrary for the relative path
	if( soundName.startsWith("/") )
	{
		tAudioID id = audio_mpi.StartAudio( soundName.str(), 0, kAudioOptionsNoDoneMsg );
		TRelease<SoundInfo> soundInfo( new SoundInfo(id, soundName, SoundTypeSFX));
		playingSounds.addElement(soundInfo);
	}
	else
	{
		tAudioID id = audio_mpi.StartAudio( AssetLibrary::getSoundPath(soundName).str(), 0, kAudioOptionsNoDoneMsg );
		TRelease<SoundInfo> soundInfo( new SoundInfo(id, soundName, SoundTypeSFX));
		playingSounds.addElement(soundInfo);
	}
}

void Scene::stopAllVo()
{
	for ( int i = 0; i<playingSounds.getSize(); i++ )
	{
		SoundInfo* info = (SoundInfo*)playingSounds[i];
		if( info->soundType == SoundTypeVo )
		{
			audio_mpi.StopAudio(info->audioID, false);
			playingSounds.removeElementAt(i);
			i--;
		}
	}
}

void Scene::playVo( String soundName )
{
	stopAllVo();

	//full path over ride, no need to ask assetlibrary for the relative path
	if( soundName.startsWith("/") )
	{
		tAudioID id = audio_mpi.StartAudio( soundName.str(), 0, kAudioOptionsNoDoneMsg );
		TRelease<SoundInfo> soundInfo( new SoundInfo(id, soundName, SoundTypeVo));
		playingSounds.addElement(soundInfo);
	}
	else
	{
		tAudioID id = audio_mpi.StartAudio( AssetLibrary::getSoundPath(soundName).str(), 0, kAudioOptionsNoDoneMsg );
		TRelease<SoundInfo> soundInfo( new SoundInfo(id, soundName, SoundTypeVo));
		playingSounds.addElement(soundInfo);
	}


}

void Scene::instancePlaySound( String soundName )
{
	Scene* scene = SceneManager::getInstance()->getScene();
	if( scene != null ){ scene->playSound(soundName); }
}
    
void Scene::instancePlayVo( String voName )
{
    Scene* scene = SceneManager::getInstance()->getScene();
    if( scene != null ){ scene->playVo(voName); }
}

//******************************************************************
// Sync pushed
//******************************************************************
void Scene::onSyncPushed()
{
	printf("Scene::onSyncPushed");
}

//static ObjectArray* stringArray = new ObjectArray();

//******************************************************************
// Base Scene responders
//******************************************************************
void Scene::onButtonEvent(ButtonEvent *event)
{
	if( event->action == ButtonActionPress )
	{
		if ( BIT_IS_MASKED(event->buttons, kButtonA) )
		{
			if (clickLockOut > 0 )
			{ return; }
			clickLockOut = 1.0f/30.0f;
			onButtonPressAction();
		}
		else if ( BIT_IS_MASKED(event->buttons, kButtonHint) )
		{
			printf("Hint button pressed \n");
			if (clickLockOut > 0 )
			{ return; }
			onButtonHint();
		}
		else if ( BIT_IS_MASKED(event->buttons, kButtonMenu) )
		{
			printf("Home button pressed \n");
			if (clickLockOut > 0 )
			{ return; }
			onButtonHome();
		}
		else if ( BIT_IS_MASKED(event->buttons, kButtonB) )
		{
			if (clickLockOut > 0 )
			{ return; }
			clickLockOut = 1.0f/30.0f;
			onButtonPressBack();
		}
	}
}

/// <summary>
/// Input from accelerometer
/// </summary>
void Scene::onAccelerationEvent(AccelerationEvent *event)
{ onAccelerationChange(event->acceleration); }

//***************************************************************************************************
// Loads prefab by name
//***************************************************************************************************
GameObject* Scene::instantiate( String prefabName )
{
	FILE* prefabFile = AssetLibrary::getPrefab(prefabName);
	SerializedObjectInfo* objInfo = SerializedObjectInfo::readInfo(prefabFile);
	if ( objInfo != null )
	{
		GameObject* newObject = Scene::createObject( prefabFile, objInfo->itemType );
		SerializedObjectInfo::fillObject(objInfo, newObject);
		newObject->onRefreshLocalization();
		objInfo->release();
		fclose(prefabFile);
		return newObject;
	}
	else
	{
		ThrowException::deserializeException(prefabName);
	}
}

//***************************************************************************************************
// In case base scenes decide to set specific objects as a target for selection
//***************************************************************************************************
GameObject* Scene::getDefaultHighLight()
{
	for( int i = 0; i<trackedObjects->getSize(); i++ )
	{
		GameObject* candidateTest = (GameObject*)trackedObjects->elementAt(i);
		if( candidateTest->isVisibleHierarchy() )
		{
			return candidateTest;
			break;
		}
	}

	return null;
}

/// <summary>
/// Sometimes a scene will want to check for any analog event as opposed to being
/// told about the post processed candidacy
/// </summary>
bool Scene::onAnalogStickEvent(AnalogStickEvent *event)
{
	if( selectionLockOut > 0 )
		return false;

	float directionX = event->analogStick.x;
	float directionY = event->analogStick.y;

	//***************************************************************************************************
	// There is no current highlight to base our decision on, this means that any jiggling of analog
	// stick should probably just find the object closest to the center, or have the scenes define it
	// For speed in testing, lets just pick the first object in the tracked array
	//***************************************************************************************************
	if( currentHighlight == NULL)
	{
		if( currentModal == NULL )
		{
			GameObject* candidate = getDefaultHighLight();
			if( candidate != NULL )
			{
				onAnalogChange( currentHighlight, candidate);
				return true;
			}
		}
		else
		{
			GameObject* candidate = currentModal->button1;
			if( candidate != NULL )
			{
				onAnalogChange( currentHighlight, candidate);
				return true;
			}
		}
	}
	//***************************************************************************************************
	// Work out which objects are in the direction we're pointing and select the appropriate ones
	//***************************************************************************************************
	else if (directionX != 0 || directionY != 0)
	{
		GameObject *candidate = NULL;
		float strongestScore = 0;
		CameraObject* contentCamera = (CameraObject*)cameras->lastElement();
		Vec3f thisVec = currentHighlight->getScreenBoundingBox(contentCamera).getCenter();

		//For debugging
		//printf("Scanning candidates for %s in <%f, %f> direction\n", currentHighlight->name.str(), directionX, directionY);
		for (int i = 0; i < trackedObjects->getSize(); i++)
		{
			//*******************************************************************************************
			// Skip over the current highlighted obejct, distance = 0, strength = 1
			//*******************************************************************************************
			GameObject *other = (GameObject*)trackedObjects->elementAt(i);
			if (other == NULL || other == currentHighlight || !other->isVisibleHierarchy())
			{ continue; }

			Vec3f otherVec = other->getScreenBoundingBox(contentCamera).getCenter();
			thisVec.z = 0; otherVec.z = 0;

			//*******************************************************************************************
			// Get distance and projected strength in direction of looking direction
			//*******************************************************************************************
			float distanceToTarget = thisVec.distanceTo(otherVec);
			Vec3f targetDirection = (otherVec-thisVec).normalized();
			Vec3f lookingDirection = Vec3f(directionX, directionY, 0).normalized();

			if( contentCamera == NULL )
			{ lookingDirection.y *= -1; }

			float strength = targetDirection.dot(lookingDirection);

			// This is the tricky part
			float score =  strength*analogProjectionWeight + (1-distanceToTarget);
			if( contentCamera == NULL )
			{ score =  strength*analogProjectionWeight + (1-(distanceToTarget/720)); }

			//For debugging
			//printf("Candidate strength %f with distance %f and score %f for %s \n", strength, distanceToTarget, score, other->name.str() );

			//*******************************************************************************************
			// Possible matches must meet the min str, highest scored object wins overall
			//*******************************************************************************************
			bool onScreen = true;
			if( !analogTrackOffScreen )
			{
				if( otherVec.x < -1.0f || otherVec.x > 1.0f || otherVec.y < -1.0f || otherVec.y > 1.0f )
					onScreen = false;
			}

			if( onScreen && strength > 0.4f && score > strongestScore && distanceToTarget <= analogMaxDistance)
			{
				candidate = other;
				strongestScore = score;
			}
		}

		if (candidate != NULL)
		{
			if ( candidate->typeOf(ButtonObject::type()) )
			{
				if ( ((ButtonObject*)candidate)->getState() != ButtonStateDisabled )
				{
					onAnalogChange( currentHighlight, candidate);
					return true;
				}
			}
			else
			{
				onAnalogChange( currentHighlight, candidate);
				return true;
			}
		}
		else
		{
			printf("No likely candidate\n");
		}
	}

	return false;
}

/// <summary>
/// Sets all the appropriate lock outs
/// </summary>
void Scene::lockOutInput( float wait )
{
	selectionLockOut = wait;
	clickLockOut = wait;
}

//******************************************************************
// Get world root
//******************************************************************
GameObject *Scene::getWorldRoot()
{ return worldRoot; }

//******************************************************************
// Finds object
//******************************************************************
GameObject *Scene::findObject(String name)
{
	GameObject* returnObject = NULL;
	TRelease<ObjectArray> nodes(name.split("/"));

	for( int i= 0; i<nodes->getSize(); i++ )
	{
		String name = *(String*)nodes->elementAt(i);
		if( !returnObject )
		{ returnObject = worldRoot->find(name); }
		else
		{ returnObject = returnObject->find(name); }
	}

	return returnObject;
}

//******************************************************************
// Returns cameras
//******************************************************************
ObjectArray* Scene::getCameras()
{ return cameras; }

//******************************************************************
// Game loop plug
//******************************************************************
void Scene::render()
{
	if (worldRoot != NULL)
	{ worldRoot->render(); }
}

//******************************************************************
// Post render
//******************************************************************
void Scene::postRender()
{
 //base implementation

}

void Scene::updateAudio(float dt )
{
	for( int i = 0 ; i<playingSounds.getSize(); i++ )
	{
		SoundInfo* info = (SoundInfo*)playingSounds[i];
		if( !audio_mpi.IsAudioPlaying(info->audioID) )
		{
			playingSounds.removeElementAt(i);
			i--;
		}
	}
}


//******************************************************************
// Game loop plug
//******************************************************************
void Scene::update(float dt)
{


	clickLockOut -= dt;
	selectionLockOut -= dt;

	updateAudio(dt);

	if (worldRoot != NULL)
	{ worldRoot->update(dt); }
}

//******************************************************************
// return connected controller count
//******************************************************************
int Scene::getNumControllers()
{
	return controllers.size();
}

//******************************************************************
// Wand mode
//******************************************************************
bool Scene::isWandEnabled()
{ return wandEnabled; }

//******************************************************************
// Get current highlight
//******************************************************************
GameObject *Scene::getCurrentHighlight()
{ return currentHighlight; }

void Scene::keyBoardOverride(GameObject* highlight)
{
	currentHighlight = highlight;

}


//******************************************************************
// Sets the current object as the highlighted selection
//******************************************************************
void Scene::setCurrentHighlight(GameObject *highlight)
{
	if( highlight != null )
	{
		onSelectionChange(currentHighlight, highlight);
		currentHighlight = highlight;
	}
}

//******************************************************************
// Add to the trackedobjects
//******************************************************************
void Scene::addTrackedObjects(ObjectArray *objects)
{
	trackedObjects->addElementsFromArray(objects);
}

//******************************************************************
// Removes a list of targets
//******************************************************************
void Scene::removeTrackedObjects(ObjectArray *objects)
{
	trackedObjects->removeElementsFromArray(objects);
}

//******************************************************************
// Sets all the targets
//******************************************************************
void Scene::setTrackedObjects(ObjectArray *objects)
{
	trackedObjects->removeAllElements();
	trackedObjects->addElementsFromArray(objects);
}

void Scene::removeAllTrackedObjects()
{
	trackedObjects->removeAllElements();
	preDialogTrackedObjects->removeAllElements();
}

}

#include "StringClass.h"
#include "SIEngine/Utilities/Integer.h"
#include <stdio.h>
#include <stdlib.h>
#include "SIEngine/Core/Lock.h"

using namespace SIUtils;

namespace SICore
{

String String::kWordDelimiters( S( " \n" ) );
// An arbitrary object that we can make locks on to prevent a String from being modified by two
// threads simultaneously. Having a single object, rather than using SYNCHRONIZED, means that
// we don't have to create a critical section for every String. This is a performance issue,
// since there can be lots of temporary strings. The actual likelihood of contention is small,
// so it shouldn't matter than this approach prevents two threads from manipulating different
// Strings at the same time. One drawback is that you need to avoid other static constructors
// which might rely on manipulating Strings.
BaseObject String::gMutex;

String::String()
{
	m_pData   = null;
	m_nLength = 0;
	m_nCapacity = 0;
}

String::String(const String& str)
{
	m_nLength = str.length();
	m_nCapacity = ROUNDUP16(m_nLength+1);
	m_pData = new TCHAR[m_nCapacity];

	strcpy(m_pData, str);
}

String::String(LPCTSTR pstr, bool bConstant)
{
	if (pstr == null)
	{
		m_nLength = 0;
		m_nCapacity = 0;
		m_pData = null;
		return;
	}
	m_nLength = strlen(pstr);
	if (bConstant)
	{
		m_nCapacity = 0;
		m_pData = const_cast<LPTSTR>(pstr);
	}
	else
	{
		m_nCapacity = ROUNDUP16(m_nLength+1);
		m_pData = new TCHAR[m_nCapacity];
		strcpy(m_pData, pstr);
	}
}

String::String(LPCTSTR pstr,int nBuffer)
{
	m_nLength = strlen(pstr);
	m_nCapacity = ROUNDUP16(m_nLength+1);
	if (nBuffer > m_nCapacity) m_nCapacity = ROUNDUP16(nBuffer);
	m_pData = new TCHAR[m_nCapacity];
	strcpy(m_pData, pstr);
}

String::String(const String* str,int nBuffer)
{
	if( str != NULL )
	{
		m_nLength = str->length();
		m_nCapacity = ROUNDUP16(m_nLength+1);
		if (nBuffer > m_nCapacity) m_nCapacity = ROUNDUP16(nBuffer);
		m_pData = new TCHAR[m_nCapacity];
		strcpy(m_pData, *str);
	}
}


String::String(const String& str,int nBuffer)
{
	m_nLength = str.length();
	m_nCapacity = ROUNDUP16(m_nLength+1);
	if (nBuffer > m_nCapacity)
	{ m_nCapacity = ROUNDUP16(nBuffer); }
	m_pData = new TCHAR[m_nCapacity];
	strcpy(m_pData, str);
}

String::String(LPCTSTR pstr, int iFirstChar, int iLastChar)
{
	m_nLength = iLastChar - iFirstChar;
	m_nCapacity = ROUNDUP16(m_nLength+1);
	m_pData = new TCHAR[m_nCapacity];
	memcpy(m_pData, pstr+iFirstChar, m_nLength*sizeof(TCHAR));
	m_pData[m_nLength] = '\0';
}

String::String( TCHAR ch, int nBuffer)
{
	m_nLength   = 1;
	m_nCapacity = ROUNDUP16(2);
	if (nBuffer > m_nCapacity) m_nCapacity = ROUNDUP16(nBuffer);
	m_pData = new TCHAR[m_nCapacity];
	*m_pData     = ch;
	*(m_pData+1) = '\0';
}

String::String( double d )
{
	char sz[12];
	gcvt( d, 9, sz );

	m_pData = NULL;
	m_nCapacity = 0;
	m_nLength = 0;
	*this = sz;
}

String::String( int i )
{
	m_pData = NULL;
	m_nCapacity = 0;
	m_nLength = 0;
	*this = i;
}

String::String( unsigned int i )
{
	m_pData = NULL;
	m_nCapacity = 0;
	m_nLength = 0;
	*this = i;
}

String::~String()
{
	//********************************************************************
	// if m_nCapacity is 0, it indicates that we
	// are using a memory buffer that another process provided
	// and we should not try to delete it.
	//********************************************************************
	if (m_nCapacity > 0)
	{ delete [] m_pData; }
}

//********************************************************************
// Returns a split string via the splitter
//********************************************************************
ObjectArray* String::split(String splitter)
{
	String currentString = *this;
	ObjectArray* results = new ObjectArray();

	int scannedIndex = currentString.indexOf(splitter, 0);
	while( scannedIndex != -1 )
	{
		TRelease<String> currentSplit(new String(currentString.substring(0, scannedIndex)));//ref 1
		currentString = currentString.substring(scannedIndex+splitter.length(), currentString.length());
		results->addElement(currentSplit); //ref 2
		scannedIndex = currentString.indexOf(splitter, 0);
	} //trelease ref 1, owned by array

	if( currentString.length()>0)
	{
		TRelease<String> currentSplit( new String(currentString));
		results->addElement(currentSplit);
	}

	return results;
}

String& String::operator =( unsigned int i )
{
	TCHAR sz[14];
	TCHAR sz2[14];
	unsigned int nLen=0;

	while( i != 0)
	{
		sz[nLen] = '0' + (i%10);
		i = i / 10;
		nLen++;
	}
	if (nLen == 0)
	{
		sz[nLen++] = '0';
	}

	for(i=0; i < nLen; i++)
	{ sz2[i] = sz[nLen-i-1]; }

	sz2[nLen] = '\0';
	m_nLength = nLen;

	if (m_pData == NULL || m_nCapacity < m_nLength+1)
	{
		SYNCHRONIZEOBJ(gMutex);
		if (m_nCapacity > 0)
		{ delete [] m_pData; }
		m_nCapacity = ROUNDUP16(m_nLength+1);
		m_pData = new TCHAR[m_nCapacity];
	}

	strcpy(m_pData, sz2);
	return *this;
}


String& String::operator =( int i )
{
	TCHAR sz[14];
	TCHAR sz2[14];
	int nLen=0;
	bool bSigned = false;

	if (i < 0)
	{
		bSigned = true;
		i *= -1;
	}

	while( i > 0)
	{
		sz[nLen] = '0' + (i%10);
		i = i / 10;
		nLen++;
	}
	if (nLen == 0)
	{
		sz[nLen++] = '0';
	}

	if (bSigned)
		sz[nLen++] = '-';

	for(i=0; i < nLen; i++)
		sz2[i] = sz[nLen-i-1];
	sz2[nLen] = '\0';
	m_nLength = nLen;

	if (m_pData == NULL || m_nCapacity < m_nLength+1)
	{
		SYNCHRONIZEOBJ(gMutex);
		if (m_nCapacity > 0) delete [] m_pData;
		m_nCapacity = ROUNDUP16(m_nLength+1);
		m_pData = new TCHAR[m_nCapacity];
	}

	strcpy(m_pData, sz2);
	return *this;
}

void String::resize( int nLength )
{
	SYNCHRONIZEOBJ(gMutex);
	nLength = ROUNDUP16(nLength);
	LPTSTR psz = new TCHAR[nLength];
	if (m_pData != NULL)
	{
		memcpy(psz, m_pData, (m_nLength+1)*sizeof(TCHAR));
		if (m_nCapacity > 0) delete [] m_pData;
	}
	else
	{
		*psz = '\0';
	}
	m_pData = psz;
	m_nCapacity = nLength;
}

void String::insert( int pos, LPCTSTR lpsz )
{
	int nNewLength = length() + strlen(lpsz);
	if (m_nCapacity <= nNewLength)
	{
		resize(nNewLength+1);
	}
	//************************************************************
	// Copy to end
	//************************************************************
	int i = nNewLength;
	int j = length();

	while (j >= pos)
	{
		m_pData[i] = m_pData[j];
		i--;
		j--;
	}

	j = pos;
	i = 0;

	while( lpsz[i] != '\0' )
	{
		m_pData[j] = lpsz[i];
		j++;
		i++;
	}

	m_nLength = nNewLength;
}

void String::append(TCHAR ch)
{
	int nNewLength = length() + 1;
	if (m_nCapacity <= nNewLength)
	{
		resize(nNewLength+1);
	}
	m_pData[m_nLength] = ch;
	m_pData[m_nLength+1] = '\0';
	m_nLength = nNewLength;
}


void String::append(const String& str2)
{
	if (str2.length() == 0) return;

	int nNewLength = length() + str2.length();
	if (m_nCapacity <= nNewLength)
	{
		resize(nNewLength+1);
	}
	memcpy(m_pData+m_nLength, str2.m_pData, (str2.m_nLength+1)*sizeof(TCHAR));
	m_nLength = nNewLength;
}

void String::append(LPCTSTR lpsz)
{
	if (lpsz==null) return;
	int nNewLength = length() + strlen(lpsz);
	if (m_nCapacity <= nNewLength)
	{ resize(nNewLength+1); }

	strcpy(m_pData+m_nLength, lpsz);
	m_nLength = nNewLength;
}

void String::setCharAt(const int index, const char aChar)
{
	if (index >= 0 && index < m_nLength)
	{ m_pData[index] = aChar; }
}

void String::truncate( int nLen )
{
	if (nLen>=0 && nLen < m_nLength)
	{
		m_nLength = nLen;
		m_pData[nLen] = '\0';
	}
}

void String::formatNumber (int nNumber)
{
	TCHAR sz[20];
	int  nCount=0;
	int  index=(sizeof(sz)/sizeof(TCHAR))-1;
	TCHAR ch[2];

	ch[0] = ',';
	ch[1] = '\0';

	sz[index--] = '\0';

	if (nNumber == 0)
	{
		//********************************************************
		// Special-case for 0, since while loop below excludes it
		//********************************************************
		sz[index--] = '0';
	}
	else
	{
		while (nNumber > 0)
		{
			if (nCount > 0 && (nCount%3)==0)
			{
				sz[index--] = ch[0];
			}
			sz[index--] = '0' + (nNumber%10);
			nNumber = nNumber / 10;
			nCount++;
		}
	}

	*this = sz+index+1;
}

String String::substring( int nStart ) const
{
	if (nStart > m_nLength)
		nStart = m_nLength;

	String str( ((LPCTSTR)*this)+nStart );
	return str;
}

String String::substring( int nStart, int nEnd ) const
{
	String str( ((LPCTSTR)*this), nStart, nEnd );
	return str;
}

float String::toFloat() const
{
	return strtof(m_pData, NULL);
}

int	String::toInt() const
{
	return Integer::parseInt(m_pData);
}

String String::toLowerCase() const
{
	String str( this );
	for( int i = 0; i<m_nLength; i++)
		str.m_pData[i] = tolower( str.m_pData[i] );
	return str;
}

String String::toUpperCase() const
{
	String str( this );
	for( int i = 0; i<m_nLength; i++)
		str.m_pData[i] = toupper( str.m_pData[i] );
	return str;
}

String String::trim() const
{
	int start, end;

	start=0;
	if (m_pData == NULL) return String("");

	while( start+1<m_nLength &&
		 m_pData[start] <= ' ')
	{
		start++;
	}

	end=m_nLength-1;
	while (end >= start &&
		m_pData[end]<=' ')
	{
		end--;
	}

	return String(m_pData, start, end+1);
}

String::String( const CString& cstring )
{
	m_nLength = cstring.length();
	m_nCapacity = ROUNDUP16(m_nLength+1);
	m_pData = new TCHAR[m_nCapacity];

	strcpy(m_pData, cstring.c_str());
}

String& String::operator =(const CString& cstring)
{
	m_nLength = cstring.size();
	if (m_pData == NULL || m_nCapacity < m_nLength+1)
	{
		SYNCHRONIZEOBJ(gMutex);
		if (m_nCapacity > 0)
		{ delete [] m_pData; }
		m_nCapacity = ROUNDUP16(m_nLength+1);
		m_pData = new TCHAR[m_nCapacity];
	}

	strcpy(m_pData, cstring.c_str());
	return *this;
}

bool String::operator !=(LPCTSTR pstr)
{
	String t(pstr);
	return !this->equals(t);
}


bool String::operator ==(LPCTSTR pstr)
{
	String t(pstr);
	return this->equals(t);
}

String& String::operator =(const String& str)
{
	m_nLength = str.length();
	if (m_pData == NULL || m_nCapacity < m_nLength+1)
	{
		SYNCHRONIZEOBJ(gMutex);
		if (m_nCapacity > 0) delete [] m_pData;
		m_nCapacity = ROUNDUP16(m_nLength+1);
		m_pData = new TCHAR[m_nCapacity];
	}

	strcpy(m_pData, (LPCTSTR)str);
	return *this;
}

String& String::operator =(LPCTSTR pstr)
{
	if (pstr==null)
	{
		truncate(0);
		return *this;
	}
	m_nLength = strlen(pstr);
	if (m_pData == NULL || m_nCapacity < m_nLength+1)
	{
		SYNCHRONIZEOBJ(gMutex);
		if (m_nCapacity > 0) delete [] m_pData;
		m_nCapacity = ROUNDUP16(m_nLength+1);
		m_pData = new TCHAR[m_nCapacity];
	}

	strcpy(m_pData, pstr);
	return *this;
}

String& String::operator =(TCHAR ch)
{
	TCHAR sz[2];
	sz[0] = ch;
	sz[1] = '\0';
	*this = sz;
	return *this;
}

String& String::operator +=(LPCTSTR lpsz)
{
	append(lpsz);
	return *this;
}

String& String::operator +=(const String& str2)
{
	append(str2);
	return *this;
}

String& String::operator +=(float f)
{
	String str(f);
	append(str);
	return *this;

}

String& String::operator +=(int i)
{
	String str(i);
	append(str);
	return *this;
}

String& String::operator +=(unsigned int i)
{
	String str(i);
	append(str);
	return *this;
}


String& String::operator +=(TCHAR ch)
{
	append(ch);
	return *this;
}

bool String::startsWith( const String& str ) const
{
	return startsWith( (LPCTSTR)str );
}

bool String::startsWith( LPCTSTR lpsz ) const
{
	LPCTSTR lpsz2 = m_pData;
	if ( m_pData == null )
		return (lpsz==null || *lpsz=='\0');

	while(*lpsz!='\0' && *lpsz2!='\0' && *lpsz==*lpsz2)
	{
		lpsz++;
		lpsz2++;
	}
	return (*lpsz=='\0');
}

bool String::endsWith( const String& str ) const
{
	return endsWith( (LPCTSTR)str );
}

bool String::endsWith( LPCTSTR lpsz ) const
{
	int nLen = strlen(lpsz);
	if (nLen > m_nLength) return false;

	LPCTSTR lpsz2 = m_pData+m_nLength-nLen;
	while(*lpsz!='\0' && *lpsz2!='\0' && *lpsz==*lpsz2)
	{
		lpsz++;
		lpsz2++;
	}
	return (*lpsz=='\0');
}

int String::hashValue() const
{
	int nHash = 0;
	int i=0;
	int nShift=0;
	while (i < length())
	{
		nHash = nHash | (m_pData[i] << nShift);
		nShift += 8;
		if (nShift > 24)
		{ nShift = 0; }

		i++;
	}

	return nHash;
}

bool String::equals(BaseObject& str) const
{
	if (str.typeOf( String::type() ))
	{
		return equals(*(String *)&str);
	}
	return false;
}


String String::stripString(String characters)
{
	String newString = this;
	for ( int i = 0 ; i<characters.length(); i++ )
	{
		char charToStrip = characters.charAt(i);
		newString.replace(charToStrip, "" );
	}

	return newString;
}

bool String::equals(const String& str) const
{
	return strcmp((LPCTSTR)*this, (LPCTSTR)str) == 0;
}

bool String::equals(LPCTSTR lpsz) const
{
	return strcmp((LPCTSTR)*this, lpsz) == 0;
}

bool String::equalsIgnoreCase( const String& str) const
{
	return strcasecmp((LPCTSTR)*this, (LPCTSTR)str) == 0;
}

bool String::equalsIgnoreCase(LPCTSTR lpsz) const
{
	return strcasecmp((LPCTSTR)*this, lpsz) == 0;
}

int String::indexOf( TCHAR ch, int nStart ) const
{
	for( int i=nStart; i < m_nLength; i++ )
	{
		if (m_pData[i] == ch)
			return i;
	}
	return -1;
}

int String::indexOfChar( const String& charList, int nStart ) const
{
	for( int i=nStart; i < m_nLength; i++ )
	{
		for( int j=0; j < charList.length(); j++ )
		{
			if (m_pData[i] == charList.charAt(j))
				return i;
		}
	}
	return -1;
}

int String::indexOf( const String& str, int nStart ) const
{ return indexOf( (LPCTSTR)str, nStart ); }

int String::indexOf( LPCTSTR lpsz, int nStart  ) const
{
	LPCTSTR lpsz2,lpsz3;
	int nLen = strlen(lpsz);
	if (nLen == 0) return 0;

	while (nStart <= m_nLength-nLen)
	{
		lpsz2 = lpsz;
		lpsz3 = m_pData+nStart;
		while (*lpsz2 == *lpsz3 && *lpsz2!='\0')
		{
			lpsz2++;
			lpsz3++;
		}

		//***************************************
		// Check for a match.
		//***************************************
		if ( *lpsz2 == '\0' )
		{ return nStart; }

		nStart++;
	}

	return -1;
}

bool String::isEmpty() const
{
	return (m_pData == null || m_nLength == 0);
}

int String::lastIndexOf( TCHAR ch ) const
{
	int index=-1;
	for( int i=0; i < m_nLength; i++ )
	{
		if (m_pData[i] == ch)
			index=i;
	}
	return index;
}

int String::lastIndexOf( TCHAR ch, int fromIndex ) const
{
	int index=-1;
	for( int i=0; i < m_nLength && i <= fromIndex; i++ )
	{
		if (m_pData[i] == ch)
			index=i;
	}
	return index;
}

int String::compareTo( const String& str ) const
{
	return strcasecmp( (LPCTSTR)(*this), (LPCTSTR)str );
}

void String::replace(const String& str1, const String& str2)
{
	int i = 0;
	int iLast = 0;
	String	strb("",length());

	if (str1.length() == 0) return;
	
	while ( (i = indexOf(str1,iLast)) >= 0 )
	{
		strb.append( substring(iLast,i) );
		strb.append( str2 );
		iLast    =  i + str1.length();
	}

	if (iLast == 0)
		return;

	strb.append( substring(iLast) );
	*this = strb;
}

String String::removeLastChar()
{
	if( m_nLength == 0 )
		return this;

	return this->substring(0, m_nLength-1);
}

String	String::right(int aLength) const
{
	if (aLength > this->length())
	{
		return this->substring(0);
	}
	else
	{
		return this->substring(this->length() - aLength);
	}
}

}

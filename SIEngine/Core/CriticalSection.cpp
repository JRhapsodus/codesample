#include "CriticalSection.h"

namespace SICore
{

CriticalSection::CriticalSection()
{
	cs_mutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
}

CriticalSection::~CriticalSection()
{
	pthread_mutex_destroy(&cs_mutex);
}

void CriticalSection::enter()
{
	pthread_mutex_lock( &cs_mutex );
}

void CriticalSection::leave()
{
	pthread_mutex_unlock( &cs_mutex );
}


void CriticalSection::initialize()
{
	pthread_mutexattr_init(&mattr);
	pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_ERRORCHECK);
	pthread_mutex_init(&cs_mutex, &mattr);
}


}

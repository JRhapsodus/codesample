#ifndef INCLUDE_USER_H_
#define INCLUDE_USER_H_

#include "SIEngine/Include/LFIncludes.h"
#include "SIEngine/Include/SIBase.h"
#include <MultiProfile/PlayerInfo.h>
#include <MultiProfile/PlayerRewards.h>
#include <MultiProfile/PlayerMilestones.h>

using namespace LTM;
using namespace LeapFrog::MultiProfile;

namespace SICore
{

/// <summary>
/// User is a subclass of MultiProfile for protected access
/// </summary>
class User : public LeapFrog::MultiProfile::PlayerInfo, public BaseObject
{
	CLASSEXTENDS(User, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	~User();

	/// <summary>
	/// Creates a user guest
	/// </summary>
	static User *guest();

	/// <summary>
	/// Api call to load user even though constructor loads, the nomenclature
	/// is still useful to distinguish
	/// </summary>
	static User* loadUser(tSlotID slotID);


	/// <summary>
	/// </summary>
	tSlotID getPlayerSLot();


	/// <summary>
	/// Static call here as UsersManager has a call to add a User based on reference
	/// and UI will create an instance to fill the initial values with. This is done
	/// as opposed to a call with 9+ parameters in UsersManager::addUser(...)
	/// </summary>
	static User* newUser(String userName, tSlotID slotID);

	/// <summary>
	///  Check to determine wether the profile file has consumer data
	/// </summary>
	bool 				isDummyProfile();

	/// <summary>
	///  Checks to see if the user is a guest
	/// </summary>
	bool 				isGuest();

	/// <summary>
	/// Outputs all fields to console
	/// </summary>
	void 				debugLog();

	/// <summary>
	/// Wrapper function for protected PlayerInfo calls
	/// </summary>
	bool 				save();

	/// <summary>
	/// Wrapper function for protected PlayerInfo calls
	/// </summary>
	bool 				removeProfile();

	/// <summary> 
	/// Returns the display name field
	/// </summary> 
	String 				getDisplayName();

	/// <summary>
	/// Returns the first name field
	/// </summary>
	String 				getFirstName();

	/// <summary>
	/// Returns the avatar prefab name
	/// </summary>
	String 				getAvatar();

	/// <summary>
	/// Returns the theme name
	/// </summary>
	String				getTheme();

	/// <summary>
	/// Returns the formatted birthdate as set by Leapfrog systems
	/// DO NOT recommend using this per consistency, use getBirthDay, getBirthMonth, getBirthYear instead
	/// </summary>
	String				getBirthdate();

	/// <summary>
	/// Returns the Birth Day
	/// </summary>
	int					getBirthDay();

	/// <summary>
	/// Returns the Birth Month
	/// </summary>
	int					getBirthMonth();

	/// <summary>
	/// Returns the Birth Year
	/// </summary>
	int					getBirthYear();


	/// <summary>
	/// Returns the theme name
	/// </summary>
	String				getGender();

	/// <summary>
	/// Returns if sneak peaks are enabled for this user
	/// </summary>
	bool 				getSneakPeeksEnabled();

	/// <summary>
	/// Returns the grade int
	/// </summary>
	int  				getGrade();

	/// <summary>
	/// Returns the whether the avatar is set
	/// </summary>
	bool 				userHasChosenAvatar();

	/// <summary>
	/// wrapper call into id, not necessarily the slot id,
	/// not sure what this is for in Leap's system
	/// </summary>
	int					getID();

	/// <summary> 
	/// Sets gender
	/// </summary> 
	void 				setGender(String gender);

	/// <summary>
	/// Sets the field for display name
	/// </summary>
	void				setDisplayName(String displayName);

	/// <summary>
	/// Sets whether sneak peaks are enabled
	/// </summary>
	void				setSneakPeeksEnabled(bool enabled);

	/// <summary>
	/// Sets the field for grade
	/// </summary>
	void				setGrade(int grade);///enum

	/// <summary>
	/// Sets the field for avatar usage
	/// </summary>
	void				setAvatar(String avatarName);

	/// <summary>
	/// Sets the field for theme
	/// </summary>
	void				setTheme(String userTheme);

	/// <summary>
	/// Sets the fields for setting birthdate
	/// </summary>
	void 				setBirthDate(String day, String month, String year);

	/// <summary>
	/// Sets the birth day
	/// </summary>
	void				setBirthDay(int day);

	/// <summary>
	/// Sets the birth month
	/// </summary>
	void				setBirthMonth(int month);

	/// <summary>
	/// Sets the birth year
	/// </summary>
	void				setBirthYear(int year);

	bool				needsTutorial();

private:

	/// <summary>
	/// For packagemanager to sync the user profiles when they change
	/// </summary>
	static void* syncProfilesToWebWorker(void* arg);

	/// <summary>
	/// Construct for new user, do not allow outside users, class factory usage
	/// </summary>
	User(String displayName, tSlotID slotID);

	/// <summary>
	/// Construct from loading, do not allow outside users, class factory usage
	/// </summary>
	User(tSlotID slotID);

	/// <summary>
	/// Primary key, users are identified in Leap's system by slot id
	/// </summary>
	int					slotID_;
};

}
#endif

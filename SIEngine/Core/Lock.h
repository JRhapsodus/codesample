#ifndef LOCK_H_
#define LOCK_H_

#include "SIEngine/Objects/BaseObject.h"

namespace SICore
{

///<summary>
/// Helper class for providing thread safety using critical section techniques.
/// The obj passed in is the locking object for the CS, and thus the SYNCRONIZED()
/// macro primarily passes in a this pointer. SYNCHRONIZEOBJ() for a generic lock
/// on a particular object. Locks do not derive from BaseObject as they are not
/// meant to be ICloneables or newed willy nilly
///</summary>
class Lock
{
	BaseObject& m_obj;
	bool m_bNoRef;
public:
	Lock( BaseObject& obj );
	Lock( BaseObject* obj );
	Lock( BaseObject& obj, bool bNoRef );
	~Lock();

private:
	Lock&	operator=(const Lock& another);
};

}
#endif

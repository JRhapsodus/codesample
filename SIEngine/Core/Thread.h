#ifndef THREAD_H_
#define THREAD_H_

#include "SIEngine/Objects/BaseObject.h"
#include "SIEngine/Core/Semaphore.h"
#include "SIEngine/Core/Lock.h"

namespace SICore
{

class Runnable : public BaseObject
{
	CLASSEXTENDS(Runnable, BaseObject);
public:
	Runnable() {}
	virtual ~Runnable() {}
	virtual void run() = 0;
};

class Thread : public Runnable
{
	CLASSEXTENDS(Thread, Runnable);
public:
	Thread();
	Thread(Runnable* runnable);
	~Thread();

	void join( long ms=0 );
	void start();
	void stop();
	bool isActive();
	virtual void run();

	static void sleep( long ms );

private:
    ///<summary>
    /// Worker function to do threaded calls into connman wrapper
    ///</summary>
    static void* workerFunction(void* arg);

	void internalRun();
	pthread_t 	pInvokable;
	Runnable*	m_runnable;
	Semaphore*	m_hThread;
	bool		m_bRunning;

};

}
#endif

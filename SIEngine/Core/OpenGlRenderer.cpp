#include <DisplayMPI.h>
#include "OpenGlRenderer.h"

#include "SIEngine/Rendering/ShaderManager.h"
#include "SIEngine/Rendering/TextureManager.h"
#include "SIEngine/Objects/CameraObject.h"
#include "SIEngine/Core/Exception.h"
#include "SIEngine/Resources/FontGlyph.h"
#include "SIEngine/Include/SIGeometry.h"

#include <stdio.h>
#include <stdlib.h>

namespace SICore
{

#define BUFFER_OFFSET(bytes) ((GLubyte*) NULL + (bytes))

OpenGlRenderer::OpenGlRenderer() :
		super()
{
	showLocalAxis = false;
	showWorldPositions = false;
	wireFrameMode = false;
	showBounds = false;
	mainCamera = NULL;
	openGlConfig = new BrioOpenGLConfig(kBrioOpenGL20);

	for ( int i = 0; i < MAX_CAMERAS; i++ )
	{
		flatGraph[i] = NULL;
	}

	//***********************************************************************************************
	// Buffer references on card to use. 1 for geomtery, 1 for fragment colors
	//***********************************************************************************************
	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &colorBuffer);
	glGenBuffers(1, &textureBuffer);
	glGenBuffers(1, &indexBuffer);
	OpenGlRenderer::checkGLError();

	//***********************************************************************************************
	// Make sure that vsync is enabled if supported by the device
	//***********************************************************************************************
	eglSwapInterval(openGlConfig->eglDisplay, 1);
	debugFont = null;

	OpenGlRenderer::checkGLError();
}

OpenGlRenderer::~OpenGlRenderer()
{
	printf("OpenGlRenderer::~OpenGlRenderer\n");

	for ( int i = 0; i < MAX_CAMERAS; i++ )
	{ SAFE_RELEASE(flatGraph[i]); }

	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &colorBuffer);
	glDeleteBuffers(1, &textureBuffer);
	glDeleteBuffers(1, &indexBuffer);

	delete openGlConfig;
}

OpenGlRenderer* globalOpenGlRenderer = null;
OpenGlRenderer* OpenGlRenderer::getInstance()
{
	if ( !globalOpenGlRenderer )
	{
		globalOpenGlRenderer = new OpenGlRenderer();
	}
	return globalOpenGlRenderer;
}

//***********************************************************************************************
// Returns the type of coordinate system the camera is using
//***********************************************************************************************
bool OpenGlRenderer::isLeftHanded()
{
	if ( mainCamera == NULL )
		return false;
	return mainCamera->leftHanded;
}

void OpenGlRenderer::clearBuffers()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0, 0, 0, 1);
	getInstance()->swapBackBuffer();
}

void OpenGlRenderer::markSceneGraphDirty()
{
	for ( int i = 0; i < MAX_CAMERAS; i++ )
	{
		if ( flatGraph[i] != NULL )
		{
			flatGraph[i]->removeAllElements();
			SAFE_RELEASE(flatGraph[i]);
		}
	}
}

void OpenGlRenderer::render( Scene *scene )
{
	if( scene == null )
		return;

	//***********************************************************************************************
	// Override for wireframe rendering since some cameras rely on UV textures to fill the backbuffer
	//***********************************************************************************************
	if ( wireFrameMode )
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1, 1, 1, 1);
	}

	//***********************************************************************************************
	// Render out scene graph
	//***********************************************************************************************
	ObjectArray* cameras = scene->getCameras();
	for ( int cameraID = 0; cameraID < cameras->getSize(); cameraID++ )
	{
		//***********************************************************************************************
		// Setup scene graph and camera for the scene pass
		//***********************************************************************************************
		mainCamera = (CameraObject*) cameras->elementAt(cameraID);

		if ( flatGraph[cameraID] == NULL )
		{ flatGraph[cameraID] = (ObjectArray*) scene->getWorldRoot()->toPointerArray(true); }

		flatGraph[cameraID]->quickSort(&GameObject::zSortCompare);

		//***********************************************************************************************
		// Clear & preprocessing for camera settings
		//***********************************************************************************************
		glClear(mainCamera->clearOptions);
		if ( mainCamera->clearColor != Color::Clear )
		{
			glClearColor(mainCamera->clearColor.r, mainCamera->clearColor.g, mainCamera->clearColor.b,
					mainCamera->clearColor.a);
		}

		//************************************************************************************
		//Painters algorithm back to front using culled layers via camera
		//Really we should use a proper mask and |= the bits, but for our use this will work.
		//************************************************************************************
		for ( int i = 0; flatGraph[cameraID]!=null && i < flatGraph[cameraID]->getSize(); i++ )
		{
			BaseObjectPointer* pointer = (BaseObjectPointer*) flatGraph[cameraID]->elementAt(i);
			GameObject* object = (GameObject*) pointer->object;
			if ( object->getCullingLayer() == mainCamera->getCullingMask() )
			{
				object->render();
			}
		}

		BaseRenderer::renderCount++;
	}

	//***********************************************************************************************
	// Commit back buffer to window surface
	//***********************************************************************************************
	getInstance()->swapBackBuffer();

}

void OpenGlRenderer::swapBackBuffer()
{
	eglSwapBuffers(openGlConfig->eglDisplay, openGlConfig->eglSurface);
}

void OpenGlRenderer::renderTextToScreen( const char *text, float startScreenX, float startScreenY, float scalar, Color color )
{
	IShader* shader = ShaderManager::getInstance().getShader(ShaderNameText);
	Texture* texture = TextureManager::getInstance()->getTexture(getInstance()->debugFont->getTextureName());

	//************************************************************************************************
	// Send the texture to gpu
	//************************************************************************************************
	glUseProgram(shader->getShaderAttributeID());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->getTextureID());
	glUniform1i(shader->getUniformID("u_s2dTexture"), 0);

	//************************************************************************************************
	// Render a quad for each char in the string
	//************************************************************************************************
	FontGlyph* lastGlyph = NULL;
	float additivex = 0;
	for ( const char* p = text; *p; p++ )
	{
		int ic = (int) p[0]; //advancing pointer
		FontGlyph* glyph = getInstance()->debugFont->getGlyph(ic);

		//************************************************************************************************
		// Space character
		//************************************************************************************************
		if ( ic == 32 )
		{
			additivex += glyph->xadvance;
			lastGlyph = glyph;
			continue;
		}

		//************************************************************************************************
		// Glyph dimensions
		//************************************************************************************************
		float x2 = glyph->x;
		float y2 = glyph->y;
		float w = glyph->width;
		float h = glyph->height;
		float tw = texture->getSize().width_;
		float th = texture->getSize().height_;

		//************************************************************************************************
		// UVs
		//************************************************************************************************
		float topLeftu = x2 / tw;
		float topLeftv = y2 / th;
		float topRightu = (x2 + w) / tw;
		float topRightv = topLeftv;
		float bottomRightu = topRightu;
		float bottomRightv = (y2 + h) / th;
		float bottomLeftu = topLeftu;
		float bottomLeftv = bottomRightv;

		//************************************************************************************************
		// Screenposition
		//************************************************************************************************
		float screenx = startScreenX + additivex + scalar * (glyph->width / 2.0f) + scalar * glyph->xoffset;
		float screeny = startScreenY - scalar * (glyph->height / 2.0f) - scalar * glyph->yoffset;

		//adjust fo rkerning
		if ( lastGlyph != NULL )
		{
			BaseObject* krnBase = glyph->kerningMap[ic];
			FontGlyphKerning *krn = (FontGlyphKerning*) krnBase;
			if ( krn != NULL )
			{
				screenx += scalar * krn->amount;
			}
		}

		additivex += (scalar * glyph->xadvance);
		lastGlyph = glyph;

		w *= scalar;
		h *= scalar;

		//************************************************************************************************
		// Screen to clipping gl space
		//************************************************************************************************
		Vec3f ul = Vec3f((-1 + 2 * ((screenx - w / 2) / 1280.0f)), (-1 + 2 * ((screeny + h / 2) / 720.0f)), 0);
		Vec3f ur = Vec3f((-1 + 2 * (((screenx - w / 2) + w) / 1280.0f)), (-1 + 2 * ((screeny + h / 2) / 720.0f)), 0);
		Vec3f lr = Vec3f((-1 + 2 * (((screenx - w / 2) + w) / 1280.0f)), (-1 + 2 * (((screeny + h / 2) - h) / 720.0f)),
				0);
		Vec3f ll = Vec3f((-1 + 2 * (((screenx - w / 2)) / 1280.0f)), (-1 + 2 * (((screeny + h / 2) - h) / 720.0f)), 0);

		//************************************************************************************************
		// Opengl data
		//************************************************************************************************
		GLfloat verts[] =
		{ ul.x, ul.y, ur.x, ur.y, lr.x, lr.y, ll.x, ll.y };
		GLfloat uvs[] =
		{ topLeftu, topLeftv, topRightu, topRightv, bottomRightu, bottomRightv, bottomLeftu, bottomLeftv };
		GLfloat colors[] =
		{ color.r, color.g, color.b, color.a, color.r, color.g, color.b, color.a, color.r, color.g, color.b, color.a, color.r, color.g, color.b, color.a };

		//************************************************************************************************
		// Verts
		//************************************************************************************************
		if ( shader->getVertexBufferAttributeID() != -1 )
		{
			glBindBuffer(GL_ARRAY_BUFFER, getInstance()->vertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
			glEnableVertexAttribArray(shader->getVertexBufferAttributeID());
			glVertexAttribPointer(shader->getVertexBufferAttributeID(), 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);
		}

		//************************************************************************************************
		// Colors
		//************************************************************************************************
		if ( shader->getColorBufferAttributeID() != -1 )
		{
			glBindBuffer(GL_ARRAY_BUFFER, getInstance()->colorBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
			glEnableVertexAttribArray(shader->getColorBufferAttributeID());
			glVertexAttribPointer(shader->getColorBufferAttributeID(), 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
		}

		//************************************************************************************************
		// UVS
		//************************************************************************************************
		if ( shader->getUVBufferAttributeID() != -1 )
		{
			glBindBuffer(GL_ARRAY_BUFFER, getInstance()->textureBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);
			glEnableVertexAttribArray(shader->getUVBufferAttributeID());
			glVertexAttribPointer(shader->getUVBufferAttributeID(), 2, GL_FLOAT, GL_FALSE, 0, (void*) 0);
		}

		//************************************************************************************************
		// Draw
		//************************************************************************************************
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	}
}

//***********************************************************************************************
// Renders a box in clip space
//***********************************************************************************************
void OpenGlRenderer::renderScreenBox( Box3f screenBox )
{
	IShader* shader = ShaderManager::getInstance().getShader(ShaderNameColorOnly);
	glUseProgram(shader->getShaderAttributeID());

	Vec3f ul = Vec3f(screenBox.getLeft(), screenBox.getTop(), screenBox.getFront());
	Vec3f ur = Vec3f(screenBox.getRight(), screenBox.getTop(), screenBox.getFront());
	Vec3f br = Vec3f(screenBox.getRight(), screenBox.getBottom(), screenBox.getFront());
	Vec3f ll = Vec3f(screenBox.getLeft(), screenBox.getBottom(), screenBox.getFront());

	GLfloat verts[] =
	{ ul.x, ul.y, ul.z, ur.x, ur.y, ur.z, ur.x, ur.y, ur.z, br.x, br.y, br.z, br.x, br.y, br.z, ll.x, ll.y, ll.z, ll.x, ll.y, ll.z, ul.x, ul.y, ul.z };

	//************************************************************************************************
	// Vert data
	//************************************************************************************************
	if ( shader->getVertexBufferAttributeID() != -1 )
	{
		glBindBuffer(GL_ARRAY_BUFFER, getInstance()->vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
		glEnableVertexAttribArray(shader->getVertexBufferAttributeID());
		glVertexAttribPointer(shader->getVertexBufferAttributeID(), 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	}

	//************************************************************************************************
	// Draw
	//************************************************************************************************
	glLineWidth(1.0);
	glDrawArrays(GL_LINES, 0, 16);

	OpenGlRenderer::checkGLError();
}

//************************************************************************************************
// Renders a line using worldSpace (projected into clip space
//************************************************************************************************
void OpenGlRenderer::renderLine( Line3f worldLine, Color c )
{
	CameraObject* camera = OpenGlRenderer::getInstance()->mainCamera;
	if ( camera )
	{
		//************************************************************************************************
		// Attach shader
		//************************************************************************************************
		IShader* shader = ShaderManager::getInstance().getShader(ShaderNameColorOnly);
		glUseProgram(shader->getShaderAttributeID());

		Vec3f worldStart = Matrix::project(worldLine.Start, camera->getViewMatrix(), camera->projection);
		Vec3f worldEnd = Matrix::project(worldLine.End, camera->getViewMatrix(), camera->projection);

		GLfloat verts[] = { worldStart.x, worldStart.y, worldStart.z, worldEnd.x, worldEnd.y, worldEnd.z };
		float colors[] = { c.r, c.g, c.b, c.a, c.r, c.g, c.b, c.a };

		//************************************************************************************************
		// Color data
		//************************************************************************************************
		if ( shader->getColorBufferAttributeID() != -1 )
		{
			glBindBuffer(GL_ARRAY_BUFFER, getInstance()->colorBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
			glEnableVertexAttribArray(shader->getColorBufferAttributeID());
			glVertexAttribPointer(shader->getColorBufferAttributeID(), 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
		}

		//************************************************************************************************
		// Vert data
		//************************************************************************************************
		if ( shader->getVertexBufferAttributeID() != -1 )
		{
			glBindBuffer(GL_ARRAY_BUFFER, getInstance()->vertexBuffer);
			glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
			glEnableVertexAttribArray(shader->getVertexBufferAttributeID());
			glVertexAttribPointer(shader->getVertexBufferAttributeID(), 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
		}

		//************************************************************************************************
		// Draw
		//************************************************************************************************
		glLineWidth(3.0);
		glDrawArrays(GL_LINES, 0, 2);
	}

	OpenGlRenderer::checkGLError();
}

//***********************************************************************************************
// Render the bounding box in the world
//***********************************************************************************************
void OpenGlRenderer::renderWorldBox( Box3f worldBox )
{
	//************************************************************************************************
	// Attach shader
	//************************************************************************************************
	IShader* shader = ShaderManager::getInstance().getShader(ShaderNameColorOnly);
	glUseProgram(shader->getShaderAttributeID());

	//************************************************************************************************
	// Build geometry
	//************************************************************************************************
	CameraObject* camera = OpenGlRenderer::getInstance()->mainCamera;
	Box3f screenBox = worldBox.project(camera->getViewMatrix(), camera->projection);

	Vec3f ul = Vec3f(screenBox.getLeft(), screenBox.getTop(), screenBox.getFront());
	Vec3f ur = Vec3f(screenBox.getRight(), screenBox.getTop(), screenBox.getFront());
	Vec3f br = Vec3f(screenBox.getRight(), screenBox.getBottom(), screenBox.getFront());
	Vec3f ll = Vec3f(screenBox.getLeft(), screenBox.getBottom(), screenBox.getFront());

	GLfloat verts[] =
	{ ul.x, ul.y, ul.z, ur.x, ur.y, ur.z, ur.x, ur.y, ur.z, br.x, br.y, br.z, br.x, br.y, br.z, ll.x, ll.y, ll.z, ll.x, ll.y, ll.z, ul.x, ul.y, ul.z };

	float colors[] =
	{ 0.1f, 1, 0.4f, 1, 0.1f, 1, 0.4f, 1, 0.1f, 1, 0.4f, 1, 0.1f, 1, 0.4f, 1, 0.1f, 1, 0.4f, 1, 0.1f, 1, 0.4f, 1, 0.1f, 1, 0.4f, 1, 0.1f, 1, 0.4f, 1 };

	//************************************************************************************************
	// Color data
	//************************************************************************************************
	if ( shader->getColorBufferAttributeID() != -1 )
	{
		glBindBuffer(GL_ARRAY_BUFFER, getInstance()->colorBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
		glEnableVertexAttribArray(shader->getColorBufferAttributeID());
		glVertexAttribPointer(shader->getColorBufferAttributeID(), 4, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	}

	//************************************************************************************************
	// Vert data
	//************************************************************************************************
	if ( shader->getVertexBufferAttributeID() != -1 )
	{
		glBindBuffer(GL_ARRAY_BUFFER, getInstance()->vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
		glEnableVertexAttribArray(shader->getVertexBufferAttributeID());
		glVertexAttribPointer(shader->getVertexBufferAttributeID(), 3, GL_FLOAT, GL_FALSE, 0, (void*) 0);
	}

	//************************************************************************************************
	// Draw
	//************************************************************************************************
	glLineWidth(1.0);
	glDrawArrays(GL_LINES, 0, 8);
}

//***********************************************************************************************
// Renders wireframe view
//***********************************************************************************************
void OpenGlRenderer::renderWireFrame( GameObject* renderable )
{
	//***********************************************************************************************
	// Get render info
	//***********************************************************************************************
	IMaterial* material = renderable->getMaterial();
	if ( material == NULL )
	{ return; }

	IShader* shader = ShaderManager::getInstance().getShader(ShaderNameTransformedColor);
	IDrawMode* drawMode = material->getDrawMode();
	VertexFormat* vertFormat = drawMode->VertFormat;

	//***********************************************************************************************
	// Begin pass on shader
	//***********************************************************************************************
	if ( shader == NULL )
	{ return; }

	glUseProgram(shader->getShaderAttributeID());

	//***********************************************************************************************
	// Get pointers to object's data
	//***********************************************************************************************
	float* vertexData = renderable->getVertexBuffer();

	//***********************************************************************************************
	// Shader info
	//***********************************************************************************************
	GLint positionAttributeID = shader->getVertexBufferAttributeID();
	GLint colorAttributeID = shader->getColorBufferAttributeID();
	GLint vertexCount = renderable->getVertexCount();

	//***********************************************************************************************
	// Send information regarding position
	//***********************************************************************************************
	if ( positionAttributeID != -1 && vertFormat->usesPosition() )
	{
		glEnableVertexAttribArray(positionAttributeID);
		glVertexAttribPointer(positionAttributeID, 3, GL_FLOAT, GL_FALSE, vertFormat->strideSize(), (void*) (vertFormat->positionOffset()));
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Send information regarding the color attribute
	//***********************************************************************************************
	if ( colorAttributeID != -1 && vertFormat->usesColor() )
	{
		glEnableVertexAttribArray(colorAttributeID);
		glVertexAttribPointer(colorAttributeID, 4, GL_FLOAT, GL_FALSE, vertFormat->strideSize(), (void*) (vertFormat->colorOffset()));
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Now send the combined localToScreen matrix to shader to transform verts
	//***********************************************************************************************
	if ( shader->getModelViewProjectionUniformID() != -1 )
	{
		Matrix projection = getInstance()->mainCamera->projection;
		Matrix view = getInstance()->mainCamera->getViewMatrix();
		Matrix model = renderable->transform.getLocalToWorld();
		Matrix projectionViewModel = projection * view * model;
		glUniformMatrix4fv(shader->getModelViewProjectionUniformID(), 1, GL_FALSE, projectionViewModel.getAsArray());
	}

	glLineWidth(1);
	glDrawArrays(GL_LINE_LOOP, 0, vertexCount);

}

//************************************************************************************************
// Throws an exception if any opengl Error has occured : note this is the last gl Error, if this
// function isn't called regularly the error could have occured in the 'past'
//************************************************************************************************
void OpenGlRenderer::checkGLError()
{
	GLenum errorCode = glGetError();
	if ( errorCode != 0 )
	{
		printf("OpenGL Error Code was found : %d\n", errorCode);
		ThrowException::openGlException(errorCode);
	}
}

//***********************************************************************************************
// Render a single object
//***********************************************************************************************
void OpenGlRenderer::render( GameObject* renderable )
{
	//***********************************************************************************************
	// Get render info
	//***********************************************************************************************
	IMaterial* material = renderable->getMaterial();
	if ( material == NULL )
	{
		return;
	}

	IShader* shader = material->getShader();
	IDrawMode* drawMode = material->getDrawMode();
	VertexFormat* vertFormat = drawMode->VertFormat;

	//***********************************************************************************************
	// This is frustum culling and will not render objects whose bounding boxes appear outside
	// the viewing space of the camera. This is not occlusion culling!!! Also I have not been able
	// to fine tooth debug the Frustum geometry math, so I wouldn't trust this code to be infallible
	//***********************************************************************************************
	/*
	 Frustum viewingBounds = getInstance()->mainCamera->GetFrustum();
	 Box3f worldBounds = renderable->WorldBoundingBox();
	 if ( !viewingBounds.BoxInFrustum(worldBounds) )
	 return;
	 */

	//***********************************************************************************************
	// Begin pass on shader
	//***********************************************************************************************
	if ( shader == NULL || drawMode == NULL )
		return;
	glUseProgram(shader->getShaderAttributeID());
	OpenGlRenderer::checkGLError();

	//***********************************************************************************************
	// Setup render state & blend mode, this should be a BLEND_MODE struct on each object
	//***********************************************************************************************
	glEnable(GL_BLEND);
	glBlendFunc(drawMode->SrcBlendMode, drawMode->DstBlendMode);
	OpenGlRenderer::checkGLError();

	//***********************************************************************************************
	// Get pointers to object's data
	//***********************************************************************************************
	float* vertexData = renderable->getVertexBuffer();
	int * indexData = renderable->getIndexBuffer();

	//***********************************************************************************************
	// Shader info
	//***********************************************************************************************
	GLint positionAttributeID = shader->getVertexBufferAttributeID();
	GLint colorAttributeID = shader->getColorBufferAttributeID();
	GLint uvAttributeID = shader->getUVBufferAttributeID();
	GLint vertexCount = renderable->getVertexCount();

	//***********************************************************************************************
	// Pass custom variables to the shader via the material proxy
	//***********************************************************************************************
	material->doShaderPass();

	//***********************************************************************************************
	// Honor draw modes scissor / stenciling if it exists
	//***********************************************************************************************
	Rect scissor = drawMode->scissor;
	if ( !scissor.isEmpty() )
	{
		glEnable(GL_SCISSOR_TEST);
		glScissor(scissor.origin_.x, scissor.origin_.y, scissor.size_.width_, scissor.size_.height_);
	}
	else
	{
		glDisable(GL_SCISSOR_TEST);
	}
	OpenGlRenderer::checkGLError();

	//***********************************************************************************************
	// Upload our vertices. We could optimize this as pointed out via:
	//		https://bugs.randomhouse.com/browse/LPGU-288
	//
	// By sending the gpu a bound sub buffer for colors and UV's, things unlikely to change. A better
	// optimization would be collecting all triangles and batch drawing per texture. Though for time's
	// sake and due to time constraints, we can at least make sure we are using VBOs correctly. I don't
	// like how OpenGL always uses NULL+int arithmetic to calculate VBO offsets, and I believe is actually
	// wrong casting a NULL to int == 0. Lets make use of our vertex formats and tell the gpu the offset in
	// our buffer to where our varying elements belong vrs. a separate buffer for each.
	// Indices are truly another buffer, but this project hasn't made use of indexed meshes yet.
	//***********************************************************************************************
	if ( vertexData != NULL )
	{
		glBindBuffer(GL_ARRAY_BUFFER, getInstance()->vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * vertFormat->strideSize(), vertexData, GL_STATIC_DRAW);
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Send information regarding our position
	//***********************************************************************************************
	if ( positionAttributeID != -1 && vertFormat->usesPosition() )
	{
		glEnableVertexAttribArray(positionAttributeID);
		glVertexAttribPointer(positionAttributeID, 3, GL_FLOAT, GL_FALSE, vertFormat->strideSize(), (void*)(vertFormat->positionOffset()));
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Send info regarding colors
	//***********************************************************************************************
	if ( colorAttributeID != -1 && vertFormat->usesColor() )
	{
		glEnableVertexAttribArray(colorAttributeID);
		glVertexAttribPointer(colorAttributeID, 4, GL_FLOAT, GL_FALSE, vertFormat->strideSize(), (void*)(vertFormat->colorOffset()));
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Send information regarding UVs
	//***********************************************************************************************
	if ( uvAttributeID != -1 && vertFormat->usesUVs() )
	{
		glEnableVertexAttribArray(uvAttributeID);
		glVertexAttribPointer(uvAttributeID, 2, GL_FLOAT, GL_FALSE, vertFormat->strideSize(), (void*)vertFormat->uvOffset());
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Upload index data
	//***********************************************************************************************
	if ( indexData != NULL )
	{
		glBindBuffer(GL_ARRAY_BUFFER, getInstance()->indexBuffer);
		glBufferData(GL_ARRAY_BUFFER, renderable->getIndexCount() * sizeof(GL_INT), indexData, GL_STATIC_DRAW);
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Now send the combined localToScreen matrix to shader to transform verts
	//***********************************************************************************************
	if ( shader->getModelViewProjectionUniformID() != -1 )
	{
		Matrix projection = getInstance()->mainCamera->projection;
		Matrix view = getInstance()->mainCamera->getViewMatrix();
		Matrix model = renderable->transform.getLocalToWorld();
		Matrix projectionViewModel = projection * view * model;
		glUniformMatrix4fv(shader->getModelViewProjectionUniformID(), 1, GL_FALSE, projectionViewModel.getAsArray());
		OpenGlRenderer::checkGLError();
	}

	//***********************************************************************************************
	// Do the actual draw
	//***********************************************************************************************
	if ( drawMode->DrawTechnique == Indexed )
	{
		glDrawElements(drawMode->TriangleMode, renderable->getIndexCount(), GL_UNSIGNED_SHORT, indexData);
	}
	else if ( drawMode->DrawTechnique == Triangles )
	{
		glDrawArrays(drawMode->TriangleMode, 0, vertexCount);
	}
}

void OpenGlRenderer::suspend()
{
	printf("OpenGlRenderer::suspend()\n");
	TextureManager::getInstance()->unloadAllTextures();

	//***********************************************************************************************
	// Shutdown OpenGL
	//***********************************************************************************************
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &colorBuffer);
	glDeleteBuffers(1, &textureBuffer);
	glDeleteBuffers(1, &indexBuffer);

	OpenGlRenderer::checkGLError();


	delete openGlConfig;
}

void OpenGlRenderer::resume()
{
	//***********************************************************************************************
	// Re-initialise OpenGL
	//***********************************************************************************************
	openGlConfig = new LeapFrog::Brio::BrioOpenGLConfig(kBrioOpenGL20);

	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &colorBuffer);
	glGenBuffers(1, &textureBuffer);
	glGenBuffers(1, &indexBuffer);

	OpenGlRenderer::checkGLError();
}

}

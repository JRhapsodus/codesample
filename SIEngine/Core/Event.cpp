#include "Event.h"

namespace SICore
{
Event::Event() {
	handled = false;
}

Event::~Event()
{ }

bool Event::wasHandled() {
	return handled;
}

void Event::eventHandled() {
	handled = true;
}

}

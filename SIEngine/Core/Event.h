#ifndef EVENT_H_
#define EVENT_H_

#include "SIEngine/Core/StringClass.h"

namespace SICore
{

class Event : public BaseObject
{
	CLASSEXTENDS(Event, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	Event();
	virtual ~Event();
	bool wasHandled();
	void eventHandled();

private:
	bool handled;
};

}

#endif /* EVENT_H_ */

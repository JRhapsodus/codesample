#include "QtRenderer.h"
#include "Glasgow/Scenes/SceneManager.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"
using namespace Glasgow;

namespace SICore
{

QtRenderer::QtRenderer()
{
	//CAppManager::Instance()->EnableInactivity(false);
	iCurrentBuffer = 0;
	//Create two screen buffers for page double buffered page flipping
	uiDisplayHandles[0] = displayManager.CreateHandle(720, 1280, kPixelFormatARGB8888, NULL);
	uiDisplayHandles[1] = displayManager.CreateHandle(720, 1280, kPixelFormatARGB8888, NULL);
	displayManager.Register(uiDisplayHandles[0], 0, 0, kDisplayOnOverlay, 0);
	displayManager.Register(uiDisplayHandles[1], 0, 0, kDisplayOnOverlay, 0);
	mScreenBuffers[0].setFromDisplayHandle(uiDisplayHandles[0]);
	mScreenBuffers[1].setFromDisplayHandle(uiDisplayHandles[1]);
	displayManager.SetAlpha(uiDisplayHandles[0], 0, true);
	displayManager.SetAlpha(uiDisplayHandles[1], 0, true);
	String fontFile = AssetLibrary::getInstallDir() + "Shared/Fonts/Montserrat-Regular.ttf";
	mainFont = fontManager.LoadFont(fontFile.str(), 22);
	fontManager.SelectFont(mainFont);
	clearBuffers();
}

QtRenderer::~QtRenderer()
{
	printf("QtRenderer::~QtRenderer()\n");

	//TODO switch to faster clear
	//Access pixel array directly:
	//mScreenBuffers[0].surface.buffer
	tRect rect = {0, 1280, 0, 720};
	mScreenBuffers[0].blitRect(rect, 0, 0, 0, 0, true);
	mScreenBuffers[1].blitRect(rect, 0, 0, 0, 0, true);

	displayManager.UnRegister(uiDisplayHandles[0], kDisplayScreenAllScreens);
	displayManager.UnRegister(uiDisplayHandles[1], kDisplayScreenAllScreens);

	displayManager.DestroyHandle(uiDisplayHandles[0],true);
	displayManager.DestroyHandle(uiDisplayHandles[1],true);
}

/// <summary>
/// Singleton pattern
/// </summary>
QtRenderer* globalQtRenderer = null;
QtRenderer* QtRenderer::getInstance()
{
	if ( !globalQtRenderer )
	{
		globalQtRenderer = new QtRenderer();
	}
	return globalQtRenderer;

}

/// <summary>
/// Major lifting draws sprite to screen
/// </summary>
void QtRenderer::blitRectObject(QtRectObject* renderable)
{
	Box3f bounds =  renderable->getScreenBoundingBox(NULL);
	Color color  =   renderable->getRectColor();
	tRect rect = {bounds.getLeft(), bounds.getRight(), bounds.getTop(), bounds.getBottom()};

	if ( rect.bottom < 0 )
		return;
	if( rect.right < 0 )
		return;
	if( rect.top > 720 )
		return;

	mScreenBuffers[iCurrentBuffer].blitRect(rect, color.r*255, color.g*255, color.b*255, color.a*255, false);
}


void QtRenderer::blitText(QtTextObject* object)
{
	String textToDraw = object->getLabelText();
	if ( !textToDraw.isEmpty() )
	{
		Rect stringBounds = getStringBounds(textToDraw);
		QtTextAlignment alignment = object->getAlignment();
		if( alignment == QtAlignmentCenter )
		{
			float x = 640 + object->getWorldPosition().x -stringBounds.size_.width_/2;
			float y = 360 -object->getWorldPosition().y - stringBounds.size_.height_/2;
			drawString( textToDraw, x, y, object->getQtColor(), object->isUnderlined );
		}
		else
		{
			float x = 640 + object->getWorldPosition().x;
			float y = 360 -object->getWorldPosition().y;
			drawString( textToDraw, x, y, object->getQtColor(), object->isUnderlined );
		}
	}
}

/// <summary>
/// Major lifting draws sprite to screen
/// </summary>
void QtRenderer::blitSprite(QtSpriteObject* sprite)
{
	Box3f screenBounds = sprite->getScreenBoundingBox(null);
	Box3f localBounds = sprite->getLocalBoundingBox();
	CBlitBuffer* buffer = sprite->getBlitBuffer();

	float x = screenBounds.getLeft();
	float y = screenBounds.getTop();

	float top = localBounds.getTop();
	float height = localBounds.getHeight();
	float bottom  = y+height;

	//offscreen
	if( bottom < 0 )
		return;

	if( y < 0 && bottom > 0 )
	{
		float diff = -y;
		height += diff;
		top += diff;
		y = 0;
	}

	blitPng( *buffer, localBounds.getLeft(), localBounds.getWidth(), top, height, x, y );
}

/// <summary>
/// Major liftingm draws the png to screen
/// </summary>
void QtRenderer::blitPng(CBlitBuffer &blitObject, int left, int right, int top, int bottom, int x, int y)
{

	tRect sourceRect;
	sourceRect.left = left;
	sourceRect.right = right;
	sourceRect.top = top;
	sourceRect.bottom = bottom;

	mScreenBuffers[iCurrentBuffer].blitFromBuffer(blitObject, sourceRect, x, y, true);
}

/// <summary>
/// Helper function which determines the real screen size to use when renering videos
/// </summary>
Size QtRenderer::getVideoDimensions(Size sourceSize, Size targetSize)
{
	float width = 0;
	float height = 0;
	float viewWidth = 1280;
	float viewHeight = 720;
	float targetWidth = targetSize.width_;
	float targetHeight = targetSize.height_;
	float sourceWidth = sourceSize.width_;
	float sourceHeight = sourceSize.height_;

	if( targetWidth <= 0 && targetWidth <= 0 )
	{ return Size( 1280, 720 ); }
	else if( targetWidth <= viewWidth && targetHeight <= viewHeight )
	{
		return Size(targetWidth, targetHeight);
	}
	else
	{
		float scale;
		scale = targetHeight/targetWidth;
		if(scale <= 1280/720)
		{
			width = viewWidth;
			height = viewWidth*scale;
		}
		else
		{
			height = viewHeight;
			width = viewHeight/scale;
		}
	}

	return Size(width, height);
}


void QtRenderer::cleanupVideoSurface(VideoInfo* info)
{
	printf("QtRenderer::cleanupVideoSurface\n");
	displayManager.DestroyHandle(info->videoHandle, true);
	displayManager.UnRegister(info->videoHandle, 0);
	info->videoHandle = 0;
}


/// <summary>
/// Creates a layer to render video onto
/// </summary>
void QtRenderer::createVideoHandle( VideoInfo* videoInfo )
{
	// Create display surface for video renderingdisplayHandle
	float sourceWidth = videoInfo->sourceWidth;
	float sourceHeight = videoInfo->sourceHeight;
	float targetWidth = videoInfo->targetWidth;
	float targetHeight = videoInfo->targetHeight;
	videoInfo->debugLog();

	Size viewSize = getVideoDimensions( Size( sourceWidth, sourceHeight), Size(targetWidth, targetHeight) );
	printf("ViewSize => [%f x %f]\n", viewSize.width_, viewSize.height_ );

	tDisplayHandle videoDisplayHandle = displayManager.CreateHandle(viewSize.height_, viewSize.width_, kPixelFormatYUV420, NULL);
	unsigned char* buffer = displayManager.GetBuffer(videoDisplayHandle);
	int pitch = displayManager.GetPitch(videoDisplayHandle);

	if (kNoErr != displayManager.Register(videoDisplayHandle, 0, 0, kDisplayOnTop, kDisplayScreenAllScreens))
	{ ThrowException::exception("Display Manager failed to create video display buffers"); }

	videoRect.origin_ = Vec2f(640 - viewSize.width_/2, 360 - viewSize.height_/2);
	videoRect.size_   = Size( viewSize.width_, viewSize.height_);

	printf("VideoRect => (%f,%f) at [%f x %f]\n", videoRect.origin_.x, videoRect.origin_.y, videoRect.size_.width_, videoRect.size_.height_);
	displayManager.SetVideoScaler(videoDisplayHandle, viewSize.width_, viewSize.height_, true);
	displayManager.SetWindowPosition(videoDisplayHandle, videoRect.origin_.x, videoRect.origin_.y, videoRect.size_.width_, videoRect.size_.height_, true);
	displayManager.SetAlpha(videoDisplayHandle, 255, false);

	tVideoSurf videoSurface = { sourceWidth, sourceHeight, pitch, buffer, kPixelFormatYUV420 };
	videoInfo->videoHandle = videoDisplayHandle;
	videoInfo->videoSurface = videoSurface;
}


/// <summary>
/// Rendering functions
/// </summary>
void QtRenderer::render(GameObject* renderable)
{
	if( renderable->typeOf(QtSpriteObject::type()) )
	{ blitSprite((QtSpriteObject*)renderable); }
	else if ( renderable->typeOf(QtRectObject::type()) )
	{ blitRectObject( (QtRectObject*)renderable ); }
	else if ( renderable->typeOf(QtTextObject::type()) )
	{ blitText((QtTextObject*)renderable ); }
}

/// <summary>
/// Helper function to get string bounds
/// </summary>
Rect QtRenderer::getStringBounds( String string )
{
	CString textToDraw = string.str();
	tRect* trect = fontManager.GetStringRect( textToDraw );

	Rect returnRect = Rect( Vec2f( trect->left, trect->top ), Size( trect->right - trect->left, trect->bottom-trect->top) );
	delete trect;
	return returnRect;
}

void QtRenderer::drawString( String string, float x, float y, U32 color, bool underline )
{
	fontManager.SetFontColor(color);
	fontManager.SetFontUnderlining(underline);

	CString textToDraw = string.str();
	fontManager.DrawString(&textToDraw, x, y, &mScreenBuffers[iCurrentBuffer].surface);
}

/// <summary>
/// Base renderer features
/// </summary>
void QtRenderer::render(Scene *scene)
{
	++iCurrentBuffer%=2;
	flatGraph = (ObjectArray*) scene->getWorldRoot()->toPointerArray(true);
	flatGraph->quickSort(&GameObject::zSortCompare);

	//************************************************************************************
	//Painters algorithm back to front using culled layers via camera
	//Really we should use a proper mask and |= the bits, but for our use this will work.
	//************************************************************************************
	BaseRenderer::renderCount++;
	for ( int i = 0; i < flatGraph->getSize(); i++ )
	{
		BaseObjectPointer* pointer = (BaseObjectPointer*)flatGraph->elementAt(i);
		GameObject* object = (GameObject*)pointer->object;
		object->render();
	}
}

/// <summary>
/// Presents back buffer to screen
/// </summary>
void QtRenderer::swapBuffers()
{
	displayManager.SwapBuffers(uiDisplayHandles[iCurrentBuffer], true);
}


/// <summary>
/// Base renderer features
/// </summary>
void QtRenderer::suspend()
{




}

/// <summary>
/// Base renderer features
/// </summary>
void QtRenderer::resume()
{




}

/// <summary>
/// Base renderer features
/// </summary>
void QtRenderer::clearBuffers()
{
	tRect rect = {0, 1280, 0, 720};
	mScreenBuffers[iCurrentBuffer].blitRect(rect, 0, 0, 0, 200, true);
}

/// <summary>
/// Get video rect
/// </summary>
Rect QtRenderer::getVideoRect()
{ return videoRect; }


/// <summary>
/// Gets rid of the cached 'zsorted' scene graph
/// </summary>
void QtRenderer::markSceneGraphDirty()
{
	//The proper thing to do is handle dirty rect management, to reduce the blitting size
	//There is no time for that but if it comes up in the future this is where it would be done.
}




}

#include "Exception.h"

namespace SICore
{

//******************************************************************************
// Flush stdout and print exception
//******************************************************************************
Exception::Exception( const String& str ) : m_str(str)
{
	printf("====================> EXCEPTION THROWN: %s\n", m_str.str());
	fflush(stdout);
	throw;
}

//******************************************************************************
// Flush stdout and print exception
//******************************************************************************
Exception::Exception( LPCTSTR str ) : m_str(str)
{
	printf("====================> EXCEPTION THROWN: %s\n", m_str.str());
	fflush(stdout);
	throw;
}

Exception::~Exception() { }
String Exception::toString() { return m_str; }



}

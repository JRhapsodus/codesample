#include "LaunchAppState.h"

namespace SICore
{

LaunchAppState::LaunchAppState()
{
	exitReason  		= ExitReasonNone;
	stateAtLastExit 	= AppStateNone;
}

LaunchAppState::LaunchAppState(ExitReason reason, AppState currentAppState_,
		String data1_, String data2_, LaunchParameters params ) : super()
{
	exitReason =  reason;
	launchParameters = params;
	stateAtLastExit = currentAppState_;
	data1= data1_;
	data2 = data2_;
}

/// <summary>
/// Debug Log
/// </summary>
void LaunchAppState::debugLog()
{
	printf("Reason for our last exit: %d\n", exitReason);
	printf("Last AppState %d\n", stateAtLastExit);
	printf("Last Data1 %s\n", data1.str());
	printf("Last Data2 %s\n", data2.str());
	printf("Requested Launch Params:\n");
	launchParameters.debugLog();
}

LaunchAppState::~LaunchAppState()
{

}

}

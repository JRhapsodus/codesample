#include "Thread.h"

namespace SICore
{

Thread::Thread()
{
	pInvokable = 0;
	m_hThread  = null;
	m_runnable = this;
	m_bRunning = false;
}

Thread::Thread(Runnable* runnable)
{
	pInvokable = 0;
	m_hThread  = null;
	m_runnable = runnable;
	m_bRunning = false;
	runnable->addRef();
}

Thread::~Thread()
{
	printf("Thread::~Thread()\n");
	if (m_runnable != this)
		m_runnable->release();
	if( m_hThread != null )
		destroySemaphore(m_hThread);
}

void Thread::join( long ms )
{
	int *ptr;
	pthread_join(pInvokable, (void**)&(ptr));
}

void Thread::start()
{
	printf("Thread::start()\n");
	if (m_bRunning)
	{ return; }

	m_bRunning = true;
	pthread_create(&pInvokable, NULL, &Thread::workerFunction, this);
}

void Thread::internalRun()
{
	printf("Thread::internalRun()\n");
	if (m_runnable!=null)
		m_runnable->run();
	m_bRunning = false;
}

void Thread::stop()
{
	//TODO:

}

bool Thread::isActive()
{ return m_bRunning; }

void Thread::run()
{
	//base
}

void Thread::sleep( long ms )
{
	//Todo?
	//std::sleep(ms);
}

///<summary>
/// Worker function to do threaded calls into connman wrapper
///</summary>
/*static*/ void* Thread::workerFunction(void* arg)
{
	Thread* pRunnable = (Thread*)arg;
	pRunnable->addRef();
	pRunnable->internalRun();
	//pRunnable->release();
	return 0;
}


}

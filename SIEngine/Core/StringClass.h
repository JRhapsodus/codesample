#ifndef __STRING__H_
#define __STRING__H_
#include "SIEngine/Collections/ObjectArray.h"

#include <stdlib.h>
#include <cstring>

LF_USING_BRIO_NAMESPACE()
using namespace SICollections;
namespace SICollections
{ class ObjectArray; }

namespace SICore
{

class String : public BaseObject
{
	CLASSEXTENDS(String, BaseObject)
	ADD_TO_CLASS_MAP;
private:
	LPTSTR		m_pData;
	int			m_nCapacity;
	int			m_nLength;

	void resize( int nLength );

public:
	static BaseObject gMutex;
	/// <summary> 
	/// Construction
	/// </summary> 
	String();
	String(const String& str);
	String(LPCTSTR pstr, int nBuffer);
	String(LPCTSTR pstr, bool bConstant=false);
	String(const String* str, int nBuffer=-1);
	String(const String& str, int nBuffer);
	String(LPCTSTR pstr, int iFirstChar, int iLastChar);
	String( TCHAR ch, int nBuffer=-1 );
	String( double d );
	String( int i );
	String( unsigned int i );
	String( const CString& cstring );
	~String();

	/// <summary> 
	/// do not add const to Object& version of equals because
	/// this is an override of the BaseObject::equals() and
	/// hashtables will not work if this is changes
	/// </summary> 
	virtual bool equals(BaseObject& str) const;
	virtual int hashValue() const;

	/// <summary> 
	/// Information
	/// </summary> 
	int compareTo( const String& str ) const;
	inline int	length() const { return m_nLength; }
	inline TCHAR charAt( int index ) const	{ return m_pData[index]; }
	void	setCharAt(const int index, const char aChar);
	bool isEmpty() const;

	/// <summary> 
	/// cast function
	/// </summary> 
	operator LPCTSTR() const { return m_pData==NULL ? _T("") : const_cast<LPCTSTR>(m_pData); }
	LPCSTR str() { return m_pData==NULL ? _T("") : const_cast<LPCSTR>(m_pData); }

	/// <summary> 
	/// Modifiers
	/// </summary> 
	String trim() const;
	String substring( int nStart ) const;
	String right(int aLength) const;
	String substring( int nStart, int nEnd ) const;
	String toLowerCase() const;
	String toUpperCase() const;

	void append(const String& str2);
	void append(LPCTSTR lpsz);
	void append(TCHAR ch);
	void insert( int i, LPCTSTR lpsz );
	void replace( const String& str1, const String& str2 );
	void truncate( int nLen );
	String removeLastChar();

	String stripString(String characters);

	ObjectArray* split(String splitter);

	/// <summary> 
	/// Formatting
	/// </summary> 
	void formatNumber( int nNumber );
	int	toInt() const;
	float toFloat() const;

	/// <summary> 
	/// Operators
	/// </summary> 
	bool operator ==(LPCTSTR pstr);
	bool operator !=(LPCTSTR pstr);

	String& operator =(const String& str);
	String& operator =(LPCTSTR pstr);
	String& operator =(TCHAR ch);
	String& operator =(int i);
	String& operator =(unsigned int i);
	String& operator =(const CString& cstring);
	String& operator +=(LPCTSTR lpsz);
	String& operator +=(const String& str2);
	String& operator +=(int i);
	String& operator +=(unsigned int i);
	String& operator +=(float f);
	String& operator +=(TCHAR ch);

	/// <summary> 
	/// Searching
	/// </summary> 
	bool startsWith( const String& str ) const;
	bool startsWith( LPCTSTR lpsz ) const;
	bool endsWith( const String& str ) const;
	bool endsWith( LPCTSTR lpsz ) const;
	bool equals(const String& str) const;
	bool equals(LPCTSTR lpsz) const;
	bool equalsIgnoreCase(LPCTSTR lpsz) const;
	bool equalsIgnoreCase(const String& str) const;
	int indexOf( TCHAR ch, int nStart=0 ) const;
	int indexOf( LPCTSTR lpsz, int nStart=0 ) const;
	int indexOf( const String& str, int nStart=0 ) const;
	int indexOfChar( const String& charList, int nStart=0 ) const;
	int lastIndexOf( TCHAR ch ) const;
	int lastIndexOf( TCHAR ch, int fromIndex ) const;

	/// <summary> 
	/// Friend ops
	/// </summary> 
	friend String operator +(const String& str1, const String& str2)
	{
		String str(str1, str1.length()+str2.length()+1);
		str += str2;
		return str;
	}

	friend String operator +(const String& str1, float f)
	{
		String str(str1,str1.length()+10);
		str += f;
		return str;
	}

	friend String operator +(const String& str1, int i)
	{
		String str(str1,str1.length()+10);
		str += i;
		return str;
	}

	friend String operator +(const String& str1, TCHAR ch)
	{
		String str(str1, str1.length()+2);
		str += ch;
		return str;
	}

	friend String operator +(TCHAR ch, const String& str1)
	{
		String str(ch, str1.length()+2);
		str += str1;
		return str;
	}

	friend String operator +(int i, const String& str1)
	{
		String str(i,str1.length()+10);
		str += str1;
		return str;
	}

	friend String operator +(const String& str1, LPCTSTR lpsz)
	{
		String str(str1, str1.length()+strlen(lpsz)+1);
		str += lpsz;
		return str;
	}


	static String kWordDelimiters;
};

/// use this macro to create CONSTANT string, for example:
/// S("test")
/// That will save memory and CPU time by using the original
/// storage for the string and not making a copy.
#define S(s) (String(s,(bool)true))

}

#endif

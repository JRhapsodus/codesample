#include "LaunchParameters.h"

namespace SICore
{

LaunchParameters::LaunchParameters(AppID fromApp, AppID toApp, AppState requested)
{
	from = fromApp;
	to = toApp;
	loadToState= requested;
}

LaunchParameters::LaunchParameters()
{
	from = AppIDNone;
	to = AppIDNone;
	loadToState = AppStateNone;
}

void LaunchParameters::debugLog()
{
	printf("\tFrom[%d]->To[%d] Requesting[%d]\n", from, to, loadToState);
}

LaunchParameters::~LaunchParameters()
{ }

}

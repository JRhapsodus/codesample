#ifndef CRITICALSECTION_H_
#define CRITICALSECTION_H_

#include "pthread.h"

namespace SICore
{

class CriticalSection
{
public:
	CriticalSection();
	virtual ~CriticalSection();

	void enter();
	void leave();
	void initialize();
private:
	pthread_mutex_t cs_mutex;
	pthread_mutexattr_t mattr;
};

}
#endif

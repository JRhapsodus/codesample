#include "Lock.h"

namespace SICore
{

Lock::Lock( BaseObject& obj ) :
  m_obj(obj)
{
	m_bNoRef=false;
	m_obj.addRef();
	m_obj.sync();
}

Lock::Lock( BaseObject* obj ) :
  m_obj(*obj)
{
	m_bNoRef=false;
	m_obj.addRef();
	m_obj.sync();
}

Lock::Lock( BaseObject& obj, bool bNoRef ) :
  m_obj(obj)
{
	m_bNoRef = bNoRef;
	if ( !bNoRef )
	{ obj.addRef(); }

	obj.sync();
}

Lock::~Lock()
{
	m_obj.unsync();
	if ( !m_bNoRef )
	{ m_obj.release(); }
}


}

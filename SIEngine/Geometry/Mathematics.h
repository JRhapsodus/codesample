#ifndef MATHEMATICS_H_
#define MATHEMATICS_H_
#include <stdlib.h>
#include <math.h>
#include "SIEngine/Geometry/Primitives.h"

namespace SIGeometry
{

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif /* M_PI */

#ifndef RAND_MAX
#define RAND_MAX 65534
#endif

inline float distanceBetweenPoints(const Vec2f& point1, const Vec2f& point2) {
	return hypotf((point2.x - point1.x), (point2.y - point1.y));

	/// Usually slower on ARM
	///return sqrtf((point2.x - point1.x) * (point2.x - point1.x)
	///			+ (point2.y - point1.y) * (point2.y - point1.y));
}

inline float angleBetweenPoints(const Vec2f& point1, const Vec2f& point2) {
	float a = point1.x - point2.x;
	float o = point1.y - point2.y;

	return atan2(a, o);
}

inline float signum(float f) {
	if (f > 0.0f) {
		return 1.0f;
	}

	if (f < 0.0f) {
		return -1.0f;
	}

	return 0.0f;
}

inline float uniformRandomNumber() {
	return rand() / float(RAND_MAX);
}

inline float degreesToRadians(float degrees) {
	return (float)M_PI * degrees / 180.0f;
}

inline float radiansToDegrees(float radians) {
	return radians * (180/(float)M_PI);
}

inline float randomFloat(float min, float max) {
	long int r = random();
	float rf = (float)r / (float)RAND_MAX;
	float rt = ((max - min) * rf) + min;
	return rt;
}

}

#endif /* MATHEMATICS_H_ */

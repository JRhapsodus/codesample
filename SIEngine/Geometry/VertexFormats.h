#ifndef INCLUDE_VERTEXFORMATS_H_
#define INCLUDE_VERTEXFORMATS_H_

#include <stdlib.h>
#include <vector>
#include <math.h>

#include "SIEngine/Geometry/Primitives.h"
#include "SIEngine/Utilities/Color.h"
#include "SIEngine/Core/StringClass.h"

using namespace SIUtils;


namespace SIGeometry
{

/// <summary> 
/// Components of a vertex format
/// </summary> 
enum VertexFormatFlags
{
    VERTEXFORMAT_COORD      = 0x01,
    VERTEXFORMAT_COLOR      = 0x02,
    VERTEXFORMAT_NORMAL     = 0x04,
    VERTEXFORMAT_TEXCOORDS  = 0x08
};

/// <summary> 
/// Defines the features used in the vertex data
/// </summary> 
class VertexFormat
{
private:
	int stride;
	int formatFlags;

public:

	VertexFormat()
	{
		stride = 0;
		formatFlags = 0;
	}

	VertexFormat(int stridesize, bool useNormals, bool usePosition, bool useColor, bool useUV )
	{
		formatFlags = 0;

		if( useNormals ) { SET_BIT(formatFlags, VERTEXFORMAT_NORMAL); }
		if( usePosition ) { SET_BIT(formatFlags, VERTEXFORMAT_COORD); }
		if( useColor ) { SET_BIT(formatFlags, VERTEXFORMAT_COLOR); }
		if( useUV ) { SET_BIT(formatFlags, VERTEXFORMAT_TEXCOORDS); }

		stride = stridesize;
	}

	virtual ~VertexFormat() { }

	VertexFormat& operator=(const VertexFormat &another) {
		stride = another.stride;
		formatFlags = another.formatFlags;
		return *this;
	}

	virtual int positionOffset()
	{ return 0; }

	virtual int colorOffset()
	{
		int size = 0;
		if( usesPosition() )
			size += sizeof(Vec3f);
		return size;
	}

	virtual int normalOffset()
	{
		int size = 0;
		if( usesPosition() )
			size += sizeof(Vec3f);
		if( usesColor() )
			size += sizeof(Color);
		return size;
	}

	virtual int uvOffset()
	{
		int size = 0;
		if( usesPosition() )
			size += sizeof(Vec3f);
		if( usesColor() )
			size += sizeof(Color);
		if( usesNormals() )
			size += sizeof(Vec3f);
		return size;
	}




	int strideSize()
	{ return stride; }

	bool usesNormals()
	{ return BIT_IS_SET(formatFlags, VERTEXFORMAT_NORMAL); }

	bool usesPosition()
	{ return BIT_IS_SET(formatFlags, VERTEXFORMAT_COORD); }

	bool usesColor()
	{ return BIT_IS_SET(formatFlags, VERTEXFORMAT_COLOR); }

	bool usesUVs()
	{ return BIT_IS_SET(formatFlags, VERTEXFORMAT_TEXCOORDS); }
};


/// <summary> 
/// Base IVertex
/// </summary> 
class IVertex : public BaseObject
{
	CLASSEXTENDS(IVertex, BaseObject);
public:
	IVertex() { }
	virtual ~IVertex() { }
	virtual VertexFormat getVertexFormat() = 0;
};

/// <summary> 
/// Color data, Normal data, UV data, Position Data
/// </summary> 
class LitTexturedNormalPositionVertex : public IVertex
{
	CLASSEXTENDS(LitTexturedNormalPositionVertex, IVertex);
public:
	LitTexturedNormalPositionVertex() : IVertex()
	{
		normal = Vec3f(0,0,0);
		position = Vec3f(0,0,0);
		color = Color();
		uv = UV();
	}

	LitTexturedNormalPositionVertex(Vec3f pos, Vec3f norm, Color col, UV coord) : IVertex()
	{
		position = pos;
		normal = norm;
		color = col;
		uv = coord;
	}

	static VertexFormat* staticVertexFormat()
	{
		customVertexFormat = VertexFormat(sizeof(LitTexturedNormalPositionVertex), true, true, true, true );
		return &customVertexFormat;
	}

	virtual VertexFormat getVertexFormat()
	{ return *staticVertexFormat(); }

	Vec3f position;
	Color color;
	Vec3f normal;
	UV uv;

private:
	static VertexFormat customVertexFormat;
};

/// <summary> 
/// Position data, Normal data, UV data
/// </summary> 
class TexturedNormalPositionVertex : public IVertex
{
	CLASSEXTENDS(TexturedNormalPositionVertex, IVertex);
public:
	~TexturedNormalPositionVertex() { }

	TexturedNormalPositionVertex() : IVertex()
	{
		position = Vec3f(0,0,0);
		normal = Vec3f( 0,0 ,0);
		uv = UV();
	}

	TexturedNormalPositionVertex(Vec3f pos, Vec3f norm, UV coord) : IVertex()
	{
		normal = norm;
		position = pos;
		uv = coord;
	}

	static VertexFormat* staticVertexFormat()
	{
		customVertexFormat = VertexFormat( sizeof(TexturedNormalPositionVertex), true, true, false, true );
		return &customVertexFormat;
	}

	virtual VertexFormat getVertexFormat()
	{ return *staticVertexFormat(); }

	int positionStride()
	{ return 0; }

	int normalStride()
	{ return (int)(sizeof(Vec3f)); }

	int uvStride()
	{  return (int)(sizeof(Vec3f) + sizeof(Vec3f)); }

	Vec3f position;
	Vec3f normal;
	UV uv;

private:
	static VertexFormat customVertexFormat;
};

/// <summary> 
/// Position data, UV data
/// </summary> 
class TexturedPositionVertex : public IVertex
{
	CLASSEXTENDS(TexturedPositionVertex, IVertex);
public:
	~TexturedPositionVertex() { }

	TexturedPositionVertex() : IVertex()
	{
		position = Vec3f(0,0,0);
		uv = UV();
	}

	TexturedPositionVertex(Vec3f pos, UV coord) : IVertex()
	{
		position = pos;
		uv = coord;
	}

	static VertexFormat* staticVertexFormat()
	{
		customVertexFormat = VertexFormat(sizeof(TexturedPositionVertex), false, true, false, true );
		return &customVertexFormat;
	}

	virtual VertexFormat getVertexFormat()
	{ return *staticVertexFormat(); }

	int positionStride()
	{ return 0; }

	int uvStride()
	{  return (int)(sizeof(Vec3f)); }

	Vec3f position;
	UV uv;

private:
	static VertexFormat customVertexFormat;
};

/// <summary> 
/// Position data, Color data
/// </summary> 
class LitPositionVertex : public IVertex
{
	CLASSEXTENDS(LitPositionVertex, IVertex);
public:
	~LitPositionVertex() {}
	LitPositionVertex() : IVertex()
	{
		position = Vec3f(0,0,0);
		color = Color();
	}


	LitPositionVertex(Vec3f pos, Color col) : IVertex()
	{
		position = pos;
		color = col;
	}

	static VertexFormat* staticVertexFormat()
	{
		customVertexFormat = VertexFormat(sizeof(LitPositionVertex), false, true, true, false );
		return &customVertexFormat;
	}


	virtual VertexFormat getVertexFormat()
	{ return *staticVertexFormat(); }


	int positionStride()
	{ return 0; }

	int colorStride()
	{  return (int)(sizeof(Vec3f)); }

	Vec3f position;
	Color color;
private:
	static VertexFormat customVertexFormat;
};

/// <summary> 
/// Position data
/// </summary> 
class PositionVertex : public IVertex
{
	CLASSEXTENDS(PositionVertex, IVertex);
public:
	~PositionVertex() {}

	PositionVertex() : IVertex()
	{ position = Vec3f(0,0,0); }

	PositionVertex(Vec3f pos) : IVertex()
	{ position = pos; }

	static VertexFormat* staticVertexFormat()
	{
		customVertexFormat = VertexFormat(sizeof(PositionVertex), false, true, false, false );
		return &customVertexFormat;
	}

	virtual VertexFormat getVertexFormat()
	{ return *staticVertexFormat(); }

	int positionStride()
	{ return 0; }

	Vec3f position;

private:
	static VertexFormat customVertexFormat;
};




/// <summary> 
/// Color data, UV data, Position Data
/// </summary> 
class LitTexturedPositionVertex : public IVertex
{
	CLASSEXTENDS(LitTexturedPositionVertex, IVertex);
public:
	~LitTexturedPositionVertex() { }

	LitTexturedPositionVertex() : IVertex()
	{
		position = Vec3f(0,0,0);
		color = Color();
		uv = UV();
	}

	LitTexturedPositionVertex(Vec3f pos, Color col, UV coord) : IVertex()
	{
		position = pos;
		color = col;
		uv = coord;
	}

	static VertexFormat* staticVertexFormat()
	{
		customVertexFormat = VertexFormat(sizeof(LitTexturedPositionVertex), false, true, true, true );
		return &customVertexFormat;
	}

	virtual VertexFormat getVertexFormat()
	{ return *staticVertexFormat(); }

	int positionStride()
	{ return 0; }

	int colorStride()
	{  return (int)(sizeof(Vec3f)); }

	int uvStride()
	{  return (int)(sizeof(Vec3f) + sizeof(Color)); }

	Vec3f position;
	Color color;
	UV uv;

private:
	static VertexFormat customVertexFormat;
};



}
#endif

#include "Box3f.h"
#include <math.h>
#include <algorithm>

namespace SIGeometry
{

Box3f Box3f::Empty(0,0,0,0,0,0);

//******************************************************************************************
// Struct life cycle
//******************************************************************************************
Box3f::Box3f()
{
	_x = 0;
	_y = 0;
	_z = 0;
	_width = 0;
	_height = 0;
	_depth = 0;
}

Box3f::Box3f(float x, float y, float z, float width, float height, float depth)
{
	_x = x;
	_y = y;
	_z = z;
	_width = width;
	_height = height;
	_depth = depth;
}

Box3f::Box3f(Vec3f center, Vec3f size)
{
	_x = center.x - size.x/2;
	_y = center.y - size.y/2;
	_z = center.z - size.z/2;
	_width = size.x;
	_height = size.y;
	_depth = size.z;
}

Box3f::~Box3f()
{ }

//******************************************************************************************
// Operators
//******************************************************************************************
bool Box3f::operator ==(Box3f& b)
{ return equals(b); }

bool Box3f::operator !=(Box3f& b)
{ return !equals(b); }

//******************************************************************************************
// Returns union of two Box3fs
//******************************************************************************************
Box3f Box3f::unionWith(Box3f& a, Box3f& b)
{
	if (a.isEmpty())
		return b;

	if (b.isEmpty())
		return a;

	float x = std::min(a._x, b._x);
	float y = std::min(a._y, b._y);
	float z = std::min(a._z, b._z);

	float xb = std::max(a.getRight(), b.getRight());
	float yb = std::max(a.getBottom(), b.getBottom());
	float zb = std::max(a.getBack(), b.getBack());


	return Box3f(x, y, z, xb - x, yb - y, zb - z);
}

//******************************************************************************************
// Equals test
//******************************************************************************************
bool Box3f::equals(Box3f& b)
{
	return
			(b._x == _x &&
			b._y == _y &&
			b._z == _z &&
			b._depth == _depth &&
			b._height == _height &&
			b._width == _width);
}

//******************************************************************************************
// Returns a union of this and other Box3f
//******************************************************************************************
Box3f Box3f::unionWith(Box3f& b)
{
	float x = std::min(_x, b._x);
	float y = std::min(_y, b._y);
	float z = std::min(_z, b._z);
	float xb = std::max(getRight(), b.getRight());
	float yb = std::max(getBottom(), b.getBottom());
	float zb = std::max(getBack(), b.getBack());
	return Box3f(x, y, z, xb - x, yb - y, zb - z);
}

//******************************************************************************************
// Probably not needed, and should Probably take a Line3f& const
//******************************************************************************************
bool Box3f::lineIntersectsFace(float X1, float Y1, float Z1, float X2, float Y2, float Z2, float X3, float Y3, float Z3, Line3f line)
{
	//this one is probably not needed
	return false;
}

//******************************************************************************************
// Contains function for screen calculations
//******************************************************************************************
bool Box3f::containsIgnoreDepth( Vec3f &point)
{
	if(point.x >= getLeft() && point.x <= getRight() && point.y >= getTop() && point.y <= getBottom() )
	{ return true; }

	return false;
}

//******************************************************************************************
// Returns if point is within box
//******************************************************************************************
bool Box3f::contains( Vec3f &point)
{
	if(point.x >= getLeft() && point.x <= getRight() && point.y <= getTop() && point.y >= getBottom() && point.z <= getFront() && point.z >= getBack() )
	{ return true; }

	return false;
}

//******************************************************************************************
// Transforms box by matrix
//******************************************************************************************
Box3f Box3f::transform(Box3f& box, Matrix& mat)
{
	Box3f ret = Box3f(box.getX(), box.getY(), box.getZ(), box.getWidth(), box.getHeight(), box.getDepth());
	ret.transform(mat);
	return ret;
}

//******************************************************************************************
// Projects the box through the projection & View matrices
//******************************************************************************************
Box3f Box3f::project( Matrix view, Matrix projection )
{
	Vec3f points[8];
	points[0] = Vec3f(getLeft(), getTop(), getFront());
	points[1] = Vec3f(getLeft(), getTop(), getBack());
	points[2] = Vec3f(getLeft(), getBottom(), getFront());
	points[3] = Vec3f(getLeft(), getBottom(), getBack());
	points[4] = Vec3f(getRight(), getTop(), getFront());
	points[5] = Vec3f(getRight(), getTop(), getBack());
	points[6] = Vec3f(getRight(), getBottom(), getFront());
	points[7] = Vec3f(getRight(), getBottom(), getBack());

	float lowX = 999999999.0f;
	float lowY = 999999999.0f;
	float lowZ = 999999999.0f;
	float highX = -999999.0f;
	float highY = -999999.0f;
	float highZ = -999999.0f;

	for ( int i = 0;i<8; i++)
	{
		Vec3f vect = Matrix::project(points[i],view, projection);
		lowX = std::min(lowX, vect.x);
		lowY = std::min(lowY, vect.y);
		lowZ = std::min(lowZ, vect.z);
		highX = std::max(highX, vect.x);
		highY = std::max(highY, vect.y);
		highZ = std::max(highZ, vect.z);
	}

	return Box3f(lowX, lowY, lowZ, highX-lowX, highY-lowY, highZ-lowZ);
}

//******************************************************************************************
// Transforms box by matrix
//******************************************************************************************
void Box3f::transform(Matrix& mat)
{
	Vec3f points[8];
	points[0] = Vec3f(getLeft(), getTop(), getFront());
	points[1] = Vec3f(getLeft(), getTop(), getBack());
	points[2] = Vec3f(getLeft(), getBottom(), getFront());
	points[3] = Vec3f(getLeft(), getBottom(), getBack());
	points[4] = Vec3f(getRight(), getTop(), getFront());
	points[5] = Vec3f(getRight(), getTop(), getBack());
	points[6] = Vec3f(getRight(), getBottom(), getFront());
	points[7] = Vec3f(getRight(), getBottom(), getBack());

	float lowX = 999999999.0f;
	float lowY = 999999999.0f;
	float lowZ = 999999999.0f;
	float highX = -999999.0f;
	float highY = -999999.0f;
	float highZ = -999999.0f;

	for ( int i = 0;i<8; i++)
	{
		//Vec3f vect = points[i];
		Vec3f vect = mat.multiplyPoint(points[i]);

		lowX = std::min(lowX, vect.x);
		lowY = std::min(lowY, vect.y);
		lowZ = std::min(lowZ, vect.z);
		highX = std::max(highX, vect.x);
		highY = std::max(highY, vect.y);
		highZ = std::max(highZ, vect.z);
	}

	_x = lowX;
	_y = lowY;
	_z = lowZ;
	_width = highX - lowX;
	_height = highY - lowY;
	_depth = highZ - lowZ;
}

//******************************************************************************************
// Gettors
//******************************************************************************************
float Box3f::getX()
{
	return _x;
}
float Box3f::getY()
{
	return _y;
}
float Box3f::getZ()
{
	return _z;
}
float Box3f::getLeft()
{
	return _x;
}
float Box3f::getTop()
{
return _y;
}
float Box3f::getFront()
{
return _z;

}
float Box3f::getWidth()
{
	 return _width;
}
float Box3f::getHeight()
{
	 return _height;

}
float Box3f::getDepth()
{
	 return _depth;
}

Vec3f Box3f::getSize()
{
	 return Vec3f(_width, _height, _depth);
}
Vec3f Box3f::getCenter()
{
	return Vec3f(_x + _width / 2, _y + _height / 2, _z + _depth / 2);

}
float Box3f::getRight()
{
	return  _x + _width;

}

float Box3f::getBottom()
{
	 return _y + _height;
}
float Box3f::getBack()
{
	return _z + _depth;
}

bool Box3f::isEmpty()
{
	return _x == 0 && _y == 0 && _z == 0 && _width == 0 && _height == 0 && _depth == 0;
}

}

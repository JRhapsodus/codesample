#include "Vec3f.h"
#include <stdio.h>

namespace SIGeometry
{

Vec3f Vec3f::Zero = Vec3f(0,0,0);
Vec3f Vec3f::One = Vec3f(1,1,1);
Vec3f Vec3f::Left = Vec3f(-1,0,0);
Vec3f Vec3f::Up = Vec3f(0,1,0);
Vec3f Vec3f::Down = Vec3f(0,-1,0);
Vec3f Vec3f::Forward = Vec3f(0,0,1);
Vec3f Vec3f::Back = Vec3f(0,0,-1);
Vec3f Vec3f::Right = Vec3f(1,0,0);
Vec3f Vec3f::Max = Vec3f(MAXFLOAT,MAXFLOAT,MAXFLOAT);
Vec3f Vec3f::Min = Vec3f(MINFLOAT,MINFLOAT,MINFLOAT);

Vec3f::Vec3f ()
{
	x = 0;
	y = 0;
	z = 0;
}

float Vec3f::length()
{
	return sqrt(lengthSquared());
}

float Vec3f::lengthSquared()
{ return ( x*x + y*y + z*z ); }

void Vec3f::debugLog()
{ printf("<%f,%f,%f>\n", (double)x, (double)y, (double)z); }

Vec3f::Vec3f ( float x1, float y2, float z1 )
{
	x = x1; y = y2; z = z1;
}

float Vec3f::dot(Vec3f vect)
{
	return (x*vect.x + y*vect.y + z*vect.z);
}

Vec3f Vec3f::getUnitVector()
{
	float length = sqrt(x * x + y * y + z * z);
	return Vec3f(x / length, y / length, z / length);
}

float Vec3f::angleBetween(Vec3f vect)
{
	Vec3f v1 = getUnitVector();
	Vec3f v2 = vect.getUnitVector();

	float dot = v1.dot(v2);
	float rads = (float)acos(dot);
	return rads;
}

bool Vec3f::isPerpendicular(const Vec3f &other)
{
	return (float)fabs(dot(other)) < EPSILON;
}

float Vec3f::distanceTo(Vec3f& other)
{
	return sqrt((other.x-x)*(other.x-x) + (other.y-y)*(other.y-y) + (other.z-z)*(other.z-z) );
}

void Vec3f::normalize(void)
{
	float length = sqrt(x * x + y * y + z * z);

	x /= length;
	y /= length;
	z /= length;
}

Vec3f Vec3f::cross(const Vec3f& vector_a, const Vec3f& vector_b)
{
	Vec3f crossProduct;

	crossProduct.x = (vector_a.y * vector_b.z) - (vector_a.z * vector_b.y);
	crossProduct.y = (vector_a.z * vector_b.x) - (vector_a.x * vector_b.z);
	crossProduct.z = (vector_a.x * vector_b.y) - (vector_a.y * vector_b.x);

	return crossProduct;
}

Vec3f Vec3f::operator/(float scalar)
{
	return Vec3f(this->x / scalar, this->y / scalar, this->z / scalar);
}

Vec3f Vec3f::normalized()
{
        float len = sqrtf(x * x + y * y + z * z);
        return Vec3f(x / len, y / len, z / len);
}

Vec3f Vec3f::project( Vec3f& vector, Vec3f& onNormal)
{
	Vec3f theNormal = onNormal.normalized();
    return theNormal * vector.dot(theNormal) / theNormal.lengthSquared();
}

float Vec3f::dot(Vec3f& lhs, Vec3f& rhs)
{
    return (lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z);
}

Vec3f Vec3f::minVec(Vec3f& lhs, Vec3f& rhs)
{
	float X = (lhs.x < rhs.x)? lhs.x : rhs.x;
	float Y = (lhs.y < rhs.y)? lhs.y : rhs.y;
	float Z = (lhs.z < rhs.z)? lhs.z : rhs.z;
	return Vec3f(X, Y, Z);
}

Vec3f Vec3f::maxVec(Vec3f& lhs, Vec3f& rhs)
{
	float X = (lhs.x < rhs.x)? rhs.x : lhs.x;
	float Y = (lhs.y < rhs.y)? rhs.y : lhs.y;
	float Z = (lhs.z < rhs.z)? rhs.z : lhs.z;
	return Vec3f(X, Y, Z);
}

void Vec3f::orthoNormalize(Vec3f* normal, Vec3f* tangent)
{
    normal->normalize();
    Vec3f norm = *normal;
    Vec3f tan = tangent->normalized();

    *tangent = tan - (norm * Vec3f::dot(norm, tan));
    tangent->normalize();
}

Vec3f Vec3f::project(Vec3f onto)
{
	Vec3f a = Vec3f( x, y, z);
	Vec3f b = onto;
	b.normalize();

	return b * a.dot(b);
}

Vec3f Vec3f::operator*(float scalar)
{
	return Vec3f(x * scalar, y * scalar, z * scalar);
}

Vec3f Vec3f::operator-(const Vec3f& rhs)
{
	return Vec3f(x - rhs.x, y - rhs.y, z - rhs.z);
}

Vec3f Vec3f::operator+(const Vec3f& rhs)
{
	return Vec3f(x + rhs.x, y + rhs.y, z + rhs.z);
}

Vec3f& Vec3f::operator+=(const Vec3f& rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;
	return *this;
}

Vec3f& Vec3f::operator-=(const Vec3f& rhs)
{
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    return *this;
}

bool Vec3f::operator!=(const Vec3f& rhs)
{
	return (x != rhs.x || y != rhs.y || z != rhs.z);
}

bool Vec3f::operator==(const Vec3f& rhs)
{
	return (x == rhs.x && y == rhs.y && z == rhs.z);
}

Vec3f& Vec3f::operator=(const Vec3f& rhs)
{
	x= rhs.x;
	y= rhs.y;
	z = rhs.z;
	return *this;
}

Vec3f Vec3f::lerp(Vec3f start, Vec3f end, float t)
{
	return (start * (1.0f - t)) + (end*t);
}

}

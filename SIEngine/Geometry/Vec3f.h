#ifndef VEC3F_H_
#define VEC3F_H_

#include <vector>
#include <math.h>

namespace SIGeometry
{

#define MAXFLOAT 999999999.0f
#define MINFLOAT -9999999.0f
#define EPSILON 0.0001f

class Matrix;


class Vec3f
{
public:
	float x;
	float y;
	float z;

	Vec3f();
	Vec3f ( float x1, float y2, float z1 );

	static void orthoNormalize(Vec3f* normal, Vec3f* tangent);
	static Vec3f lerp(Vec3f start, Vec3f end, float t);
	static Vec3f cross(const Vec3f& vector_a, const Vec3f& vector_b);
	static float dot(Vec3f& lhs, Vec3f& rhs);
	static Vec3f project( Vec3f& vector, Vec3f& onNormal);
	static Vec3f minVec(Vec3f& lhs, Vec3f& rhs);
	static Vec3f maxVec(Vec3f& lhs, Vec3f& rhs);

	static Vec3f One;
	static Vec3f Zero;
	static Vec3f Left;
	static Vec3f Up;
	static Vec3f Down;
	static Vec3f Forward;
	static Vec3f Back;
	static Vec3f Right;
	static Vec3f Max;
	static Vec3f Min;

	float distanceTo(Vec3f& other);
	float dot(Vec3f vect);
	float angleBetween(Vec3f vect);
	void transform(Matrix& m);
	void normalize(void);
	Vec3f normalized();
	float length();
	float lengthSquared();
	Vec3f getUnitVector();
	void debugLog();

	bool isPerpendicular(const Vec3f &other);

	Vec3f project( Vec3f onto );
	Vec3f operator*(float scalar);
	Vec3f operator/(float scalar);
	Vec3f operator-(const Vec3f& rhs);
	Vec3f operator+(const Vec3f& rhs);
	Vec3f& operator=(const Vec3f& rhs);
	Vec3f& operator-=(const Vec3f& rhs);
	Vec3f& operator+=(const Vec3f& rhs);
	bool operator==(const Vec3f& rhs);
	bool operator!=(const Vec3f& rhs);

};

}

#endif /* VEC3F_H_ */

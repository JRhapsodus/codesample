#ifndef INCLUDE_PRIMITIVES_H_
#define INCLUDE_PRIMITIVES_H_

#include <vector>
#include <math.h>
#include "Vec3f.h"
#include <stdio.h>

namespace SIGeometry
{

/// <summary>
/// 2 component class for mathematical vectors
/// </summary>
class Vec2f
{
public:
	float x, y;
	Vec2f() {
		x = 0;
		y = 0;
	}
	~Vec2f(){}
	Vec2f(float x1, float y1 ) { x = x1; y = y1; }
};

/// <summary>
/// 4 component class for mathematical vectors
/// </summary>
class Vec4f
{
public:
	float x, y, z, w;

	Vec4f()
	{ x = 0; y = 0; z= 0; w= 0;}

	Vec4f( float a, float b, float c, float d )
	{ x= a; y = b; z= c; w= d; }

	void normalize(void)
	{
		float length = sqrt(x * x + y * y + z * z + w * w);

		x /= length;
		y /= length;
		z /= length;
		w /= length;
	}
};

/// <summary>
/// 2 components that generally represent the UV space for textures
/// UV space is typically normalized from 0,0 to 1,1 where 0,0 represents
/// the first pixel (generally Upper Left) and 1,1 represents the last pixel
/// </summary>
class UV
{
public:
	UV()
	{
		u = 0;
		v = 0;
	}

	UV(float u1, float v1 )
	{
		u = u1;
		v = v1;
	}

	~UV() {}
	float u;
	float v;
};

/// <summary>
/// Width and height container class. Useful for nomenclature
/// </summary>
struct Size
{
    float width_;
    float height_;
    Size(float width, float height) :  width_(width), height_(height) { }
    Size(const Size& size) : width_(size.width_), height_(size.height_) { }
    Size(void) : width_(0), height_(0) { }
};

/// <summary>
/// Rectangle class wrapping an origin and size
/// </summary>
struct Rect
{
    Vec2f origin_;
    Size size_;
    bool isEmpty()
    {
    	if( origin_.x == 0 && origin_.y == 0 && size_.width_ == 0 && size_.height_ == 0 )
    	{ return true; }

    	return false;
    }

    void debugLog()
    {
    	printf("Rect: [%f,%f] [%f x %f]\n", origin_.x, origin_.y, size_.width_, size_.height_);

    }
    Rect(int x, int y, int w, int h ) : origin_(x,y), size_(w,h) { }
    Rect(Vec2f origin, Size size) : origin_(origin), size_(size) { }
    Rect(const Rect& rect) : origin_(rect.origin_), size_(rect.size_) { }
    Rect(void) { }
};

}
#endif

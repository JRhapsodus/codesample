#ifndef ISPLINE_H_
#define ISPLINE_H_

#include "Vec3f.h"

namespace SIGeometry
{

class ISpline
{
public:
	ISpline() {}
	virtual ~ISpline() {}

	virtual Vec3f computeValue(float t) = 0;
	virtual Vec3f computeVelocity(float t) = 0;
	virtual Vec3f computeAcceleration(float t) = 0;
};

}

#endif /* ISPLINE_H_ */

#include "VertexFormats.h"

namespace SIGeometry
{

VertexFormat LitTexturedPositionVertex::customVertexFormat = *LitTexturedPositionVertex::staticVertexFormat();
VertexFormat LitTexturedNormalPositionVertex::customVertexFormat = *LitTexturedNormalPositionVertex::staticVertexFormat();
VertexFormat TexturedNormalPositionVertex::customVertexFormat = *TexturedNormalPositionVertex::staticVertexFormat();
VertexFormat TexturedPositionVertex::customVertexFormat = *TexturedPositionVertex::staticVertexFormat();
VertexFormat LitPositionVertex::customVertexFormat = *LitPositionVertex::staticVertexFormat();
VertexFormat PositionVertex::customVertexFormat = *PositionVertex::staticVertexFormat();

}

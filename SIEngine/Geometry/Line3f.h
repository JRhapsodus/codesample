#ifndef LINE3F_H_
#define LINE3F_H_

#include "Primitives.h"
#include "Plane3f.h"

namespace SIGeometry
{

class Line3f
{
public:
	Line3f(Vec3f s, Vec3f e);
	virtual ~Line3f();

	Vec3f Start;
	Vec3f End;

	Vec3f getValue(float t);
	bool intersect(Plane3f plane, float &t, bool &parallel);
	bool intersect(Plane3f plane, Vec3f& pos, bool &parallel);
	bool intersects(Line3f line, float& intersection);
	bool intersects(Line3f line, Vec3f& intersection);
};

}

#endif /* LINE3F_H_ */

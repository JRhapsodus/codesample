#include "CubicBezierCurve.h"

namespace SIGeometry
{

CubicBezierCurve::CubicBezierCurve() {

}

CubicBezierCurve::~CubicBezierCurve() {
}

//ISpline
Vec3f CubicBezierCurve::computeValue(float t)
{
	return Vec3f::Zero;
}

Vec3f CubicBezierCurve::computeVelocity(float t)
{
	return Vec3f::Zero;

}

Vec3f CubicBezierCurve::computeAcceleration(float t)
{
	return Vec3f::Zero;
}

}

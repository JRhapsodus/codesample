#ifndef QUATERNION_H_
#define QUATERNION_H_

#include "Vec3f.h"
#include "Matrix.h"

namespace SIGeometry
{

class Quaternion
{
public:
        Quaternion();
        Quaternion(float W, float X, float Y, float Z);
        Quaternion(const Quaternion& q);
        ~Quaternion();

        Quaternion& operator=(const Quaternion&);

        bool operator==(Quaternion& rhs);
        bool operator!=(Quaternion& rhs);

        Quaternion& operator*=(Quaternion& rhs);
        Quaternion& operator*=(const Quaternion& rhs);
        Quaternion operator*(Quaternion& rhs);
        const Quaternion operator*(const Quaternion& rhs);
        Quaternion operator*(float& rhs);
        const Quaternion operator*(const float& rhs);
        Quaternion operator+(Quaternion& rhs);
        const Quaternion operator+(const Quaternion& rhs);

        Quaternion operator- (const Quaternion& rkQ) const;
        Quaternion operator- () const;

        Vec3f operator*(Vec3f& rhs);
        const Vec3f operator*(const Vec3f& rhs);

        Vec3f toEuler();
        Quaternion normalized();
        void normalize();
        Quaternion conjugate(); /// Same as inverse

        void toAngleAxis(float* angle, Vec3f* axis);
        void setEuler(float X, float Y, float Z);
        float dot(Quaternion& b);
        void setLookRotation(Vec3f& lookAt);
        void setLookRotation(Vec3f& lookAt, Vec3f& upDirection);

        static Quaternion lookRotation(Vec3f& lookAt);
        static Quaternion lookRotation(Vec3f& lookAt, Vec3f& upDirection);
        static Quaternion slerp(Quaternion& from, Quaternion& to, float t, bool shortestPath);
        static Quaternion nlerp(Quaternion& rkP, Quaternion& rkQ, float fT, bool shortestPath);

        static Quaternion lerp(Quaternion& from, Quaternion& to, float t);
        static float angle(Quaternion& a, Quaternion& b);
        static float dot(Quaternion& a, Quaternion& b);
        static Quaternion angleAxis(float angle, Vec3f& axis);
        static Quaternion inverse(Quaternion& rotation);
        static Quaternion euler(float X, float Y, float Z);
        static Matrix toMatrix(Quaternion& q);

        /// <summary>
        /// Rotation Matrix is in Form: This is in row major order, so convert
        /// to colum major order prior to setting quaternion
        /// 1 - 2y^2 - 2z^2        2xy - 2wz            2xz + 2wy
        ///    2xy + 2wz        1 - 2x^2 - 2z^2         2yz - 2wx
        ///    2xz - 2wy           2yz + 2wx         1 - 2x^2 - 2y^2
        /// </summary>
        void setRotationMatrix(Matrix& m);
        static Quaternion fromMatrix(Matrix& m);

        static Quaternion identity;
        float w, x, y, z;
};

const Vec3f operator*(Vec3f& v, const Quaternion& m);
Vec3f operator*(Vec3f& v, Quaternion& m);
Quaternion operator*(float f, Quaternion& m);
const Quaternion operator*(float f, const Quaternion& m);

}

#endif /* QUATERNION_H_ */

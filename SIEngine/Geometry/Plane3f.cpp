#include "Plane3f.h"

namespace SIGeometry
{

//********************************************************************************
// Returns true if the point is contained on the plane
//********************************************************************************
bool Plane3f::Contains( Vec3f point )
{
	return ( SideOf( point ) == On );
}

//********************************************************************************
// Unit space
//********************************************************************************
void Plane3f::Normalize()
{
	normal.normalize();
	float mag = 1.0f/normal.length();
	normal.x *=  mag;
	normal.y *=  mag;
	normal.z *= mag;
	d *= mag;
}

//********************************************************************************
// What side of the plane does the point lie. Use distance formula to plane
//********************************************************************************
Side Plane3f::SideOf( Vec3f point )
{
	float val = point.x * normal.x + point.y * normal.y + point.z * normal.z + d;

	if( val == 0 )
		return On;

	if( val > 0 )
		return Front;

	return Back;
}

}

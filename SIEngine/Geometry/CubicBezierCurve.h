#ifndef CUBICBEZIERCURVE_H_
#define CUBICBEZIERCURVE_H_

#include "ISpline.h"

namespace SIGeometry
{

class CubicBezierCurve : public ISpline
{
public:
	CubicBezierCurve();
	virtual ~CubicBezierCurve();

	///ISpline
	virtual Vec3f computeValue(float t);
	virtual Vec3f computeVelocity(float t);
	virtual Vec3f computeAcceleration(float t);
};

}

#endif /* CUBICBEZIERCURVE_H_ */

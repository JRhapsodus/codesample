#include "Frustum.h"

namespace SIGeometry
{

//******************************************************************************************************
//probably should be const Matrix &, Constructs a frustum with the model & projection
//******************************************************************************************************
Frustum::Frustum( Matrix & model, Matrix & projection )
{
	float clip[16];
	float* proj = projection.getAsArray();
	float* modl = model.getAsArray();

	/* Combine the two matrices (multiply projection by modelview) */
	clip[0] = modl[0] * proj[0] + modl[1] * proj[4] + modl[2] * proj[8] + modl[3] * proj[12];
	clip[1] = modl[0] * proj[1] + modl[1] * proj[5] + modl[2] * proj[9] + modl[3] * proj[13];
	clip[2] = modl[0] * proj[2] + modl[1] * proj[6] + modl[2] * proj[10] + modl[3] * proj[14];
	clip[3] = modl[0] * proj[3] + modl[1] * proj[7] + modl[2] * proj[11] + modl[3] * proj[15];

	clip[ 4] = modl[4] * proj[0] + modl[5] * proj[4] + modl[6] * proj[8] + modl[7] * proj[12];
	clip[ 5] = modl[4] * proj[1] + modl[5] * proj[5] + modl[6] * proj[9] + modl[7] * proj[13];
	clip[ 6] = modl[4] * proj[2] + modl[5] * proj[6] + modl[6] * proj[10] + modl[7] * proj[14];
	clip[ 7] = modl[4] * proj[3] + modl[5] * proj[7] + modl[6] * proj[11] + modl[7] * proj[15];

	clip[ 8] = modl[8] * proj[0] + modl[9] * proj[4] + modl[10] * proj[8] + modl[11] * proj[12];
	clip[ 9] = modl[8] * proj[1] + modl[9] * proj[5] + modl[10] * proj[9] + modl[11] * proj[13];
	clip[10] = modl[8] * proj[2] + modl[9] * proj[6] + modl[10] * proj[10] + modl[11] * proj[14];
	clip[11] = modl[8] * proj[3] + modl[9] * proj[7] + modl[10] * proj[11] + modl[11] * proj[15];

	clip[12] = modl[12] * proj[0] + modl[13] * proj[4] + modl[14] * proj[8] + modl[15] * proj[12];
	clip[13] = modl[12] * proj[1] + modl[13] * proj[5] + modl[14] * proj[9] + modl[15] * proj[13];
	clip[14] = modl[12] * proj[2] + modl[13] * proj[6] + modl[14] * proj[10] + modl[15] * proj[14];
	clip[15] = modl[12] * proj[3] + modl[13] * proj[7] + modl[14] * proj[11] + modl[15] * proj[15];

	left = Plane3f( (clip[3] + clip[0]), (clip[7] + clip[4]), (clip[7] + clip[4]), (clip[15] + clip[12]) );
	right = Plane3f( (clip[3] - clip[0]), (clip[7] - clip[4]), (clip[7] - clip[4]), (clip[15] - clip[12]) );
	top = Plane3f( (clip[3] - clip[1]), (clip[7] - clip[5]), (clip[11] - clip[9]), (clip[15] - clip[13]) );
	bottom = Plane3f( (clip[3] + clip[1]), (clip[7] + clip[5]), (clip[11] + clip[9]), (clip[15] + clip[13]) );
	back = Plane3f( (clip[3] - clip[2]), (clip[7] - clip[6]), (clip[11] - clip[10]), (clip[15] - clip[14]) );
	front = Plane3f( (clip[3] + clip[2]), (clip[7] + clip[6]), (clip[11] + clip[10]), (clip[15] + clip[14]) );

	left.Normalize();
	right.Normalize();
	top.Normalize();
	bottom.Normalize();
	back.Normalize();
	front.Normalize();
}

//******************************************************************************************************
//This is going to assume the normals of the plane point outwards
//******************************************************************************************************
bool Frustum::pointInFrustum( Vec3f& point )
{

	if( 	top.SideOf(point) != Front && left.SideOf(point) != Front && right.SideOf(point) != Front &&
			bottom.SideOf(point) != Front && front.SideOf(point) != Front && back.SideOf(point) != Front )
	{ return true; }

	return false;
}

//******************************************************************************************************
// Returns true if box is inside frustrum
//******************************************************************************************************
bool Frustum::boxInFrustum( Box3f& box )
{
	//Looping convenience
	Plane3f* frustum[6];
	frustum[0] = &top;
	frustum[1] = &left;
	frustum[2] = &right;
	frustum[3] = &bottom;
	frustum[4] = &back;
	frustum[5] = &front;

	for(int p = 0; p < 6; p++ )
	{
		if( frustum[p]->a * ((float)box.getX() - box.getSize().x) + frustum[p]->b * (box.getY() - box.getSize().y) + frustum[p]->c * (box.getZ() - box.getSize().z) + frustum[p]->d > 0 )
		 continue;
		if( frustum[p]->a * (box.getX() + box.getSize().x) + frustum[p]->b * (box.getY() - box.getSize().y) + frustum[p]->c * (box.getZ() - box.getSize().z) + frustum[p]->d > 0 )
		 continue;
		if( frustum[p]->a * (box.getX() - box.getSize().x) + frustum[p]->b * (box.getY() + box.getSize().y) + frustum[p]->c * (box.getZ() - box.getSize().z) + frustum[p]->d > 0 )
		 continue;
		if( frustum[p]->a * (box.getX() + box.getSize().x) + frustum[p]->b * (box.getY() + box.getSize().y) + frustum[p]->c * (box.getZ() - box.getSize().z) + frustum[p]->d > 0 )
		 continue;
		if( frustum[p]->a * (box.getX() - box.getSize().x) + frustum[p]->b * (box.getY() - box.getSize().y) + frustum[p]->c * (box.getZ() + box.getSize().z) + frustum[p]->d > 0 )
		 continue;
		if( frustum[p]->a * (box.getX() + box.getSize().x) + frustum[p]->b * (box.getY() - box.getSize().y) + frustum[p]->c * (box.getZ() + box.getSize().z) + frustum[p]->d > 0 )
		 continue;
		if( frustum[p]->a * (box.getX() - box.getSize().x) + frustum[p]->b * (box.getY() + box.getSize().y) + frustum[p]->c * (box.getZ() + box.getSize().z) + frustum[p]->d > 0 )
		 continue;
		if( frustum[p]->a * (box.getX() + box.getSize().x) + frustum[p]->b * (box.getY() + box.getSize().y) + frustum[p]->c * (box.getZ() + box.getSize().z) + frustum[p]->d > 0 )
		 continue;

		return true;
	}

	return false;

}

}

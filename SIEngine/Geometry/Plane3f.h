#ifndef PLANE3F_H_
#define PLANE3F_H_

#include "../Geometry/Vec3f.h"

namespace SIGeometry
{

/// <summary> 
/// Sides of a plane
/// </summary> 
typedef enum Side
{
	Front,
	Back,
	On,
	Inside,
	Outside
} SideOf;

/// <summary> 
/// Plane using the formula ax + bx + cz + dw = 0
/// </summary> 
class Plane3f
{
public:
	Vec3f normal;
	float a,b,c,d;

	/// <summary>
	/// Default plane is considered empty: All 0s
	/// </summary>
	Plane3f()
	{ a = 0; b = 0; c = 0; d = 0; }

	/// <summary>
	/// Creates a plane based on all the coefficients of the plane formula
	/// </summary>
	Plane3f( float a1, float b1, float c1, float d1 )
	{ a=  a1; b= b1; c= c1; d= d1; }

	/// <summary>
	/// Creates a plane based on the point and normal
	/// </summary>
	Plane3f( Vec3f point, Vec3f normalToPlane )
	{
		normal = normalToPlane;
		normal.normalize();
		a = normal.x;
		b = normal.y;
		c = normal.z;
		d = -(normal.x * point.x + normal.y*point.y + normal.z*point.z );
	}

	/// <summary>
	/// Normalize, d and normal multiplied by magnitudeyup
	/// </summary>
	void Normalize();

	/// <summary>
	/// Returns whether the point lands within the plane
	/// </summary>
	bool Contains( Vec3f point );

	/// <summary>
	/// Returns the enum describing the relative position of the point to this plane
	/// </summary>
	Side SideOf( Vec3f point );

	/// <summary>
	/// Should be moved to a static object: A plane with all 0's
	/// </summary>
	static Plane3f Empty()
	{ return Plane3f( 0, 0, 0, 0); }

	virtual ~Plane3f() { }
};

}

#endif /* PLANE3F_H_ */

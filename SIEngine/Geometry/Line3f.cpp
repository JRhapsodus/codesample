#include "Line3f.h"

namespace SIGeometry
{

Line3f::Line3f(Vec3f s, Vec3f e)
{
	Start = s;
	End = e;

}

Line3f::~Line3f()
{

}

Vec3f Line3f::getValue(float t)
{
	// Pa = P1 + t ( P2 - P1 )
	Vec3f range = (End - Start)*t;
	return Start + range;
}

bool Line3f::intersect(Plane3f plane, float &t, bool &parallel)
{
	return false;
}

bool Line3f::intersect(Plane3f plane, Vec3f& pos, bool &parallel)
{
	return false;
}

bool Line3f::intersects(Line3f line, float& intersection)
{

	return false;

}

bool Line3f::intersects(Line3f line, Vec3f& intersection)
{
	return false;


}

}


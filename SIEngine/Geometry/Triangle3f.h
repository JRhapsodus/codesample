#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "SIEngine/Geometry/Vec3f.h"

namespace SIGeometry
{

class Triangle3f
{
public:
	Triangle3f();
	virtual ~Triangle3f();

	Vec3f a;
	Vec3f b;
	Vec3f c;
};

}

#endif /* TRIANGLE_H_ */

#ifndef FRUSTUM_H_
#define FRUSTUM_H_

#include "SIEngine/Geometry/Plane3f.h"
#include "SIEngine/Geometry/Box3f.h"

namespace SIGeometry
{

class Frustum
{
public:
	Plane3f right;
	Plane3f left;
	Plane3f top;
	Plane3f bottom;
	Plane3f front;
	Plane3f back;

	bool pointInFrustum( Vec3f& point );
	bool boxInFrustum( Box3f& box );

	Frustum()
	{ right = Plane3f::Empty(); left = Plane3f::Empty(); top = Plane3f::Empty(); bottom = Plane3f::Empty();  front = Plane3f::Empty(); back = Plane3f::Empty(); }

	Frustum( Plane3f r, Plane3f l, Plane3f t, Plane3f btm, Plane3f f, Plane3f bck )
	{ right = r; left = l; top = t; bottom = btm; front = f; back = bck; }

	Frustum( Matrix & model, Matrix & projection );

	virtual ~Frustum() { }
};

}

#endif /* FRUSTUM_H_ */

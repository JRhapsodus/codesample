#include <cstddef>
#include "Transform.h"

namespace SIGeometry
{

Transform::Transform()
{
	position 	= Vec3f(0,0,0);
	rotation 	= Quaternion::identity;
	scale 		= Vec3f(1,1,1);
	pivot		= Vec3f(0,0,0);
	parentTransform = NULL;
	isDirty = true;
}


Transform::~Transform()
{

}

Vec3f Transform::getWorldLeft()
{ return localToWorld*Vec3f::Left; }

Vec3f Transform::getWorldForward()
{ return localToWorld*Vec3f::Forward; }

Vec3f Transform::getWorldUp()
{ return localToWorld*Vec3f::Up; }

Vec3f Transform::getPosition()
{ return position; }

Quaternion Transform::getRotation()
{ return rotation; }

Vec3f Transform::getEulerRotation()
{ return rotation.toEuler(); }

Vec3f Transform::getScale()
{ return scale; }


void Transform::setPosition( Vec3f p )
{
	position = p;
	markDirty();
}

void Transform::setRotation(Quaternion q )
{
	rotation = q;
	markDirty();
}

void Transform::setEulerRotation( Vec3f r )
{
	rotation = Quaternion::euler(r.x, r.y, r.z);
	markDirty();
}
void Transform::setScale( Vec3f s )
{
	scale =s ;
	markDirty();
}

Matrix Transform::getLocalToParent()
{
	return  Matrix::createTranslation(pivot)*
			Matrix::createTranslation(position)*
			Quaternion::toMatrix(rotation)*
			Matrix::createScale(scale);
}

Matrix Transform::getLocalToWorld()
{
	if (isDirty)
		computeLocalToWorld();
	return localToWorld;
}

Matrix Transform:: getWorldToLocal()
{
	if (isDirty)
		computeLocalToWorld();
	return Matrix::matrixInvert( &localToWorld );
}

void Transform::computeLocalToWorld()
{
	if ( parentTransform == NULL )
	{
		//if no parent this is root therefore localtoparent equates to localtoworld
		localToWorld = getLocalToParent();
	}
	else
	{
		localToWorld = parentTransform->getLocalToWorld() * getLocalToParent();
	}

	isDirty = false;
}

Vec3f Transform::convertWorldToLocal( Vec3f point)
{
	return getWorldToLocal()*point;
}

void Transform::markDirty()
{
	isDirty = true;
}

}

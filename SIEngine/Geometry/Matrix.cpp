#include "SIEngine/Geometry/Matrix.h"
#include "SIEngine/Geometry/Mathematics.h"
#include <string.h>
#include <stdio.h>
#include <math.h>

namespace SIGeometry
{

const float Matrix::identityArray[16] = {
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f,
};

Matrix Matrix::identityMatrix = Matrix(identityArray);

Matrix::Matrix() {
	for (int i = 0; i < 15; i++) {
		elements[i] = 0;
	}
}

Matrix::Matrix(const float* array) {
	memcpy(elements, array, 16 * sizeof(float));
}

Matrix::~Matrix() {

}

float* Matrix::getAsArray() {
	return elements;
}


Matrix Matrix::inverse()
{
	return Matrix::matrixInvert(this);
}

// Get a float from elements at index
float& Matrix::operator[](unsigned index)
{
	return elements[index];
}

Matrix Matrix::operator*(Matrix right) {
	return multiply(this, &right);
}

Vec3f Matrix::operator*(Vec3f right) {
	Vec3f u;
	u.x = _00()*right.x + _10()*right.y + _20()*right.z;
	u.y = _01()*right.x + _11()*right.y + _21()*right.z;
	u.z = _02()*right.x + _12()*right.y + _22()*right.z;

	return u;
}

Matrix& Matrix::operator=(const Matrix &another) {
	if(this != &another) {
		memcpy(this->elements, another.elements, 16 * sizeof(float));
	}

	return *this;
}

Vec4f Matrix::vertexTransform(Vec4f *vertex, Matrix *matrix)
{
	Vec4f result;

	result.x = vertex->x * matrix->elements[0];
	result.x += vertex->y * matrix->elements[4];
	result.x += vertex->z * matrix->elements[8];
	result.x += vertex->w * matrix->elements[12];

	result.y = vertex->x * matrix->elements[1];
	result.y += vertex->y * matrix->elements[5];
	result.y += vertex->z * matrix->elements[9];
	result.y += vertex->w * matrix->elements[13];

	result.z = vertex->x * matrix->elements[2];
	result.z += vertex->y * matrix->elements[6];
	result.z += vertex->z * matrix->elements[10];
	result.z += vertex->w * matrix->elements[14];

	result.w = vertex->x * matrix->elements[3];
	result.w += vertex->y * matrix->elements[7];
	result.w += vertex->z * matrix->elements[11];
	result.w += vertex->w * matrix->elements[15];

	return result;
}

Vec3f Matrix::vertexTransform(Vec3f *vertex, Matrix *matrix)
{
	Vec3f result;
	Vec4f extendedVertex;

	extendedVertex.x = vertex->x;
	extendedVertex.y = vertex->y;
	extendedVertex.z = vertex->z;
	extendedVertex.w = 1.0f;

	result.x = extendedVertex.x * matrix->elements[0];
	result.x += extendedVertex.y * matrix->elements[4];
	result.x += extendedVertex.z * matrix->elements[8];
	result.x += extendedVertex.w * matrix->elements[12];

	result.y = extendedVertex.x * matrix->elements[1];
	result.y += extendedVertex.y * matrix->elements[5];
	result.y += extendedVertex.z * matrix->elements[9];
	result.y += extendedVertex.w * matrix->elements[13];

	result.z = extendedVertex.x * matrix->elements[2];
	result.z += extendedVertex.y * matrix->elements[6];
	result.z += extendedVertex.z * matrix->elements[10];
	result.z += extendedVertex.w * matrix->elements[14];

	return result;
}

void Matrix::matrixTranspose(Matrix *matrix) {
	float temp;

	temp = matrix->elements[1];
	matrix->elements[1] = matrix->elements[4];
	matrix->elements[4] = temp;

	temp = matrix->elements[2];
	matrix->elements[2] = matrix->elements[8];
	matrix->elements[8] = temp;

	temp = matrix->elements[3];
	matrix->elements[3] = matrix->elements[12];
	matrix->elements[12] = temp;

	temp = matrix->elements[6];
	matrix->elements[6] = matrix->elements[9];
	matrix->elements[9] = temp;

	temp = matrix->elements[7];
	matrix->elements[7] = matrix->elements[13];
	matrix->elements[13] = temp;

	temp = matrix->elements[11];
	matrix->elements[11] = matrix->elements[14];
	matrix->elements[14] = temp;
}

Matrix Matrix::createRotationX(float angle) {
	Matrix result = identityMatrix;

	result.elements[5] = cos(degreesToRadians(angle));
	result.elements[9] = -sin(degreesToRadians(angle));
	result.elements[6] = sin(degreesToRadians(angle));
	result.elements[10] = cos(degreesToRadians(angle));

	return result;
}

Matrix Matrix::createRotationY(float angle) {
	Matrix result = identityMatrix;

	result.elements[0] = cos(degreesToRadians(angle));
	result.elements[8] = sin(degreesToRadians(angle));
	result.elements[2] = -sin(degreesToRadians(angle));
	result.elements[10] = cos(degreesToRadians(angle));

	return result;
}

Matrix Matrix::createRotationZ(float angle) {
	Matrix result = identityMatrix;

	result.elements[0] = cos(degreesToRadians(angle));
	result.elements[4] = -sin(degreesToRadians(angle));
	result.elements[1] = sin(degreesToRadians(angle));
	result.elements[5] = cos(degreesToRadians(angle));

	return result;
}

Matrix Matrix::createTranslation(Vec3f translation) {
	return createTranslation( translation.x, translation.y, translation.z);
}

Matrix Matrix::rotationMatrix( Vec3f right, Vec3f up, Vec3f forward)
{
	Matrix m = Matrix::identityMatrix;
/*
#define m00 right.x
#define m10 right.y
#define m20 right.z
#define m01 up.x
#define m11 up.y
#define m21 up.z
#define m02 forward.x
#define m12 forward.y
#define m22 forward.z
 */
	m._00(right.x);
	m._10(right.y);
	m._20(right.z);

	m._01(up.x);
	m._11(up.y);
	m._21(right.z);

	m._02(forward.z);
	m._12(forward.y);
	m._22(forward.z);

	return m;
}

Matrix Matrix::createEulerRotation(Vec3f eulerAngles)
{
	return createRotationX(eulerAngles.x)*createRotationY(eulerAngles.y)*createRotationZ(eulerAngles.z);
}

Matrix Matrix::createScale(Vec3f scale)
{
	return  createScaling(scale.x, scale.y, scale.z );
}


Matrix Matrix::createTranslation(float x, float y, float z) {
	Matrix result = identityMatrix;

	result.elements[12] = x;
	result.elements[13] = y;
	result.elements[14] = z;

	return result;
}

Matrix Matrix::createScaling(float x, float y, float z) {
	Matrix result = identityMatrix;

	result.elements[0] = x;
	result.elements[5] = y;
	result.elements[10] = z;

	return result;
}

Matrix Matrix::matrixPerspectiveLH(float fov, float ratio, float znear, float zfar)
{
	Matrix result = identityMatrix;

	float top = znear * tanf(fov * (float)M_PI/360.0f);
	float right = top*ratio;
	float bottom = -top;
	float left   = -right;

	result.elements[0]  = 2*znear/(right-left);
	result.elements[1]  = 0;
	result.elements[2]  = 0;
	result.elements[3]  = 0;

	result.elements[4]  = 0;
	result.elements[5]  = 2*znear/(top-bottom);
	result.elements[6]  = 0;
	result.elements[7]  = 0;

	result.elements[8]  = (right+left)/(right-left);
	result.elements[9]  = (top+bottom)/(top-bottom);
	result.elements[10] = ((zfar+znear)/(zfar-znear));
	result.elements[11] = 1;

	result.elements[12] = 0;
	result.elements[13] = 0;
	result.elements[14] = -2 * (zfar * znear) / (zfar - znear);
	result.elements[15] = 0;
	return result;
}

Matrix Matrix::matrixPerspectiveRH(float fov, float ratio, float znear, float zfar)
{
	Matrix result = identityMatrix;

	float top = znear * tanf(fov * (float)M_PI/360.0f);
	float right = top*ratio;
	float bottom = -top;
	float left   = -right;

	result.elements[0]  = 2*znear/(right-left);
	result.elements[1]  = 0;
	result.elements[2]  = 0;
	result.elements[3]  = 0;

	result.elements[4]  = 0;
	result.elements[5]  = 2*znear/(top-bottom);
	result.elements[6]  = 0;
	result.elements[7]  = 0;

	result.elements[8]  = (right+left)/(right-left);
	result.elements[9]  = (top+bottom)/(top-bottom);
	result.elements[10] = -((zfar+znear)/(zfar-znear));
	result.elements[11] = -1;

	result.elements[12] = 0;
	result.elements[13] = 0;
	result.elements[14] = -2 * (zfar * znear) / (zfar - znear);
	result.elements[15] = 0;

	/*
	  Matrix given by unity
	  0.97428  0.00000  0.00000  0.00000
	  0.00000  1.73205  0.00000  0.00000
	  0.00000  0.00000  -1.00401  -20.04008
	  0.00000  0.00000  -1.00000  0.00000
	*/

	return result;
}

void Matrix::debugOutput()
{
	printf("%f %f %f %f \n", (double)elements[0], (double)elements[4], (double)elements[8], (double)elements[12]);
	printf("%f %f %f %f \n", (double)elements[1], (double)elements[5], (double)elements[9], (double)elements[13]);
	printf("%f %f %f %f \n", (double)elements[2], (double)elements[6], (double)elements[10], (double)elements[14]);
	printf("%f %f %f %f \n", (double)elements[3], (double)elements[7], (double)elements[11], (double)elements[15]);
}

Vec3f Matrix::multiplyPoint(Vec3f& vect)
{
	float w = 1.0f / (vect.x * elements[3] + vect.y * elements[7] + vect.z * elements[11] + 1 * elements[15]);

	return Vec3f
			(
					(vect.x * elements[0] + vect.y * elements[4] + vect.z * elements[8] + 1 * elements[12]) * w,
					(vect.x * elements[1] + vect.y * elements[5] + vect.z * elements[9] + 1 * elements[13]) * w,
					(vect.x * elements[2] + vect.y * elements[6] + vect.z * elements[10] + 1 * elements[14]) * w
			);
}
Matrix Matrix::matrixCameraLookAt(Vec3f eye, Vec3f center, Vec3f up) {
	Matrix result = identityMatrix;

	Vec3f cameraX, cameraY;

	Vec3f cameraZ = Vec3f( center.x - eye.x, center.y - eye.y, center.z - eye.z );
	cameraZ.normalize();

	cameraX = Vec3f::cross(cameraZ, up);
	cameraX.normalize();

	cameraY = Vec3f::cross(cameraX, cameraZ);

	/*
	 * The final cameraLookAt should look like:
	 *
	 * cameraLookAt[] = { cameraX.x,	cameraY.x,   -cameraZ.x,  0.0f,
	 *					  cameraX.y,	cameraY.y,   -cameraZ.y,  0.0f,
	 *					  cameraX.z,	cameraY.z,   -cameraZ.z,  0.0f,
	 *					 -eye.x,	   -eye.y,		 -eye.z,	  1.0f };
	 */

	result[0] = cameraX.x;
	result[1] = cameraY.x;
	result[2] = -cameraZ.x;

	result[4] = cameraX.y;
	result[5] = cameraY.y;
	result[6] = -cameraZ.y;

	result[8] = cameraX.z;
	result[9] = cameraY.z;
	result[10] = -cameraZ.z;

	result[12] = -eye.x;
	result[13] = -eye.y;
	result[14] = -eye.z;

	return result;
}


Matrix::Matrix(
	  float m11, float m12, float m13, float m14,
      float m21, float m22, float m23, float m24,
      float m31, float m32, float m33, float m34,
      float m41, float m42, float m43, float m44 )
{
	elements[0] = m11;
	elements[1] = m12;
	elements[2] = m13;
	elements[3] = m14;
	elements[4] = m21;
	elements[5] = m22;
	elements[6] = m23;
	elements[7] = m24;
	elements[8] = m31;
	elements[9] = m32;
	elements[10] = m33;
	elements[11] = m34;
	elements[12] = m41;
	elements[13] = m42;
	elements[14] = m43;
	elements[15] = m44;
}

//*******************************************************************************************
// Builds a matrix for left handed orthogonal projection
//*******************************************************************************************
Matrix Matrix::orthoLH(float width, float height, float znearPlane, float zfarPlane)
{
	return Matrix(
		2 / width, 0, 0, 0,
     0, 2 / height, 0, 0,
     0, 0, 1 / (zfarPlane - znearPlane), 0,
     0, 0, znearPlane / (znearPlane - zfarPlane), 1
	);
}

//*******************************************************************************************
// Builds a matrix for left handed orthogonal projection, with corner being origin
//*******************************************************************************************
Matrix Matrix::orthoOffCenterLH(float left, float right, float bottom, float top, float znearPlane, float zfarPlane)
{
	return Matrix(
			2 / (right - left), 0, 0, 0,
			0, 2 / (top - bottom), 0, 0,
			0, 0, 1 / (zfarPlane - znearPlane), 0,
			(left + right) / (left - right), (top + bottom) / (bottom - top), znearPlane / (znearPlane - zfarPlane), 1
	);
}

//*******************************************************************************************
// Builds a matrix for right handed orthogonal projection, with corner being origin
//*******************************************************************************************
Matrix Matrix::orthoOffCenterRH(float left, float right, float bottom, float top, float znearPlane, float zfarPlane)
{
	return Matrix(
			2 / (right - left), 0, 0, 0,
			0, 2 / (top - bottom), 0, 0,
			0, 0, 1 / (znearPlane - zfarPlane), 0,
			(left + right) / (left - right), (top + bottom) / (bottom - top), znearPlane / (znearPlane - zfarPlane), 1
	);
}

//*******************************************************************************************
// Builds a matrix for right handed orthogonal projection
//*******************************************************************************************
Matrix Matrix::orthoRH(float width, float height, float znearPlane, float zfarPlane)
{
	return Matrix(
			2 / width, 0, 0, 0,
    	0, 2 / height, 0, 0,
		0, 0, 1 / (znearPlane - zfarPlane), 0,
		0, 0, znearPlane / (znearPlane - zfarPlane), 1
	);
}




Matrix Matrix::matrixInvert(Matrix *matrix) {
	Matrix result;
	float matrix3x3[9];

	/* Find the cofactor of each element. */
	/* Element (i, j) (1, 1) */
	matrix3x3[0] = matrix->elements[5];
	matrix3x3[1] = matrix->elements[6];
	matrix3x3[2] = matrix->elements[7];
	matrix3x3[3] = matrix->elements[9];
	matrix3x3[4] = matrix->elements[10];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[13];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	result.elements[0] = matrixDeterminant(matrix3x3);

	/* Element (i, j) (1, 2) */
	matrix3x3[0] = matrix->elements[1];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[9];
	matrix3x3[4] = matrix->elements[10];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[13];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	result.elements[4] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (1, 3) */
	matrix3x3[0] = matrix->elements[1];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[5];
	matrix3x3[4] = matrix->elements[6];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[13];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	result.elements[8] = matrixDeterminant(matrix3x3);

	/* Element (i, j) (1, 4) */
	matrix3x3[0] = matrix->elements[1];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[5];
	matrix3x3[4] = matrix->elements[6];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[9];
	matrix3x3[7] = matrix->elements[10];
	matrix3x3[8] = matrix->elements[11];
	result.elements[12] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (2, 1) */
	matrix3x3[0] = matrix->elements[4];
	matrix3x3[1] = matrix->elements[6];
	matrix3x3[2] = matrix->elements[7];
	matrix3x3[3] = matrix->elements[8];
	matrix3x3[4] = matrix->elements[10];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	result.elements[1] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (2, 2) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[8];
	matrix3x3[4] = matrix->elements[10];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	result.elements[5] = matrixDeterminant(matrix3x3);

	/* Element (i, j) (2, 3) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[4];
	matrix3x3[4] = matrix->elements[6];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	result.elements[9] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (2, 4) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[4];
	matrix3x3[4] = matrix->elements[6];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[8];
	matrix3x3[7] = matrix->elements[10];
	matrix3x3[8] = matrix->elements[11];
	result.elements[13] = matrixDeterminant(matrix3x3);

	/* Element (i, j) (3, 1) */
	matrix3x3[0] = matrix->elements[4];
	matrix3x3[1] = matrix->elements[5];
	matrix3x3[2] = matrix->elements[7];
	matrix3x3[3] = matrix->elements[8];
	matrix3x3[4] = matrix->elements[9];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[13];
	matrix3x3[8] = matrix->elements[15];
	result.elements[2] = matrixDeterminant(matrix3x3);

	/* Element (i, j) (3, 2) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[1];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[8];
	matrix3x3[4] = matrix->elements[9];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[13];
	matrix3x3[8] = matrix->elements[15];
	result.elements[6] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (3, 3) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[1];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[4];
	matrix3x3[4] = matrix->elements[5];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[13];
	matrix3x3[8] = matrix->elements[15];
	result.elements[10] = matrixDeterminant(matrix3x3);

	/* Element (i, j) (3, 4) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[1];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[4];
	matrix3x3[4] = matrix->elements[5];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[8];
	matrix3x3[7] = matrix->elements[9];
	matrix3x3[8] = matrix->elements[11];
	result.elements[14] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (4, 1) */
	matrix3x3[0] = matrix->elements[4];
	matrix3x3[1] = matrix->elements[5];
	matrix3x3[2] = matrix->elements[6];
	matrix3x3[3] = matrix->elements[8];
	matrix3x3[4] = matrix->elements[9];
	matrix3x3[5] = matrix->elements[10];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[13];
	matrix3x3[8] = matrix->elements[14];
	result.elements[3] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (4, 2) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[1];
	matrix3x3[2] = matrix->elements[2];
	matrix3x3[3] = matrix->elements[8];
	matrix3x3[4] = matrix->elements[9];
	matrix3x3[5] = matrix->elements[10];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[13];
	matrix3x3[8] = matrix->elements[14];
	result.elements[7] = matrixDeterminant(matrix3x3);

	/* Element (i, j) (4, 3) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[1];
	matrix3x3[2] = matrix->elements[2];
	matrix3x3[3] = matrix->elements[4];
	matrix3x3[4] = matrix->elements[5];
	matrix3x3[5] = matrix->elements[6];
	matrix3x3[6] = matrix->elements[12];
	matrix3x3[7] = matrix->elements[13];
	matrix3x3[8] = matrix->elements[14];
	result.elements[11] = -matrixDeterminant(matrix3x3);

	/* Element (i, j) (4, 4) */
	matrix3x3[0] = matrix->elements[0];
	matrix3x3[1] = matrix->elements[1];
	matrix3x3[2] = matrix->elements[2];
	matrix3x3[3] = matrix->elements[4];
	matrix3x3[4] = matrix->elements[5];
	matrix3x3[5] = matrix->elements[6];
	matrix3x3[6] = matrix->elements[8];
	matrix3x3[7] = matrix->elements[9];
	matrix3x3[8] = matrix->elements[10];
	result.elements[15] = matrixDeterminant(matrix3x3);

	/* The adjoint is the transpose of the cofactor matrix. */
	matrixTranspose(&result);

	/* The inverse is the adjoint divided by the determinant. */
	result = matrixScale(&result, 1.0f / matrixDeterminant(matrix));

	return result;
}

float Matrix::matrixDeterminant(float *matrix) {
	float result = 0.0f;

	result = matrix[0] * (matrix[4] * matrix[8] - matrix[7] * matrix[5]);
	result -= matrix[3] * (matrix[1] * matrix[8] - matrix[7] * matrix[2]);
	result += matrix[6] * (matrix[1] * matrix[5] - matrix[4] * matrix[2]);

	return result;
}

float Matrix::matrixDeterminant(Matrix *matrix) {
	float matrix3x3[9];
	float determinant3x3 = 0.0f;
	float result = 0.0f;

	/* Remove (i, j) (1, 1) to form new 3x3 matrix. */
	matrix3x3[0] = matrix->elements[5];
	matrix3x3[1] = matrix->elements[6];
	matrix3x3[2] = matrix->elements[7];
	matrix3x3[3] = matrix->elements[9];
	matrix3x3[4] = matrix->elements[10];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[13];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	determinant3x3 = matrixDeterminant(matrix3x3);
	result += matrix->elements[0] * determinant3x3;

	/* Remove (i, j) (1, 2) to form new 3x3 matrix. */
	matrix3x3[0] = matrix->elements[1];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[9];
	matrix3x3[4] = matrix->elements[10];
	matrix3x3[5] = matrix->elements[11];
	matrix3x3[6] = matrix->elements[13];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	determinant3x3 = matrixDeterminant(matrix3x3);
	result -= matrix->elements[4] * determinant3x3;

	/* Remove (i, j) (1, 3) to form new 3x3 matrix. */
	matrix3x3[0] = matrix->elements[1];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[5];
	matrix3x3[4] = matrix->elements[6];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[13];
	matrix3x3[7] = matrix->elements[14];
	matrix3x3[8] = matrix->elements[15];
	determinant3x3 = matrixDeterminant(matrix3x3);
	result += matrix->elements[8] * determinant3x3;

	/* Remove (i, j) (1, 4) to form new 3x3 matrix. */
	matrix3x3[0] = matrix->elements[1];
	matrix3x3[1] = matrix->elements[2];
	matrix3x3[2] = matrix->elements[3];
	matrix3x3[3] = matrix->elements[5];
	matrix3x3[4] = matrix->elements[6];
	matrix3x3[5] = matrix->elements[7];
	matrix3x3[6] = matrix->elements[9];
	matrix3x3[7] = matrix->elements[10];
	matrix3x3[8] = matrix->elements[11];
	determinant3x3 = matrixDeterminant(matrix3x3);
	result -= matrix->elements[12] * determinant3x3;

	return result;
}


Vec3f Matrix::project( Vec3f worldLoc, Matrix view, Matrix projection)
{
	Vec4f in = Vec4f(worldLoc.x, worldLoc.y, worldLoc.z, 1);
	Vec4f out = Matrix::vertexTransform(&in, &view);
	Vec4f final = Matrix::vertexTransform(&out, &projection);

	if (final.w == 0.0f)
		return Vec3f(0,0,0);


	final = Vec4f( (final.x/final.w), (final.y/final.w), (final.z/final.w), final.w);

	return Vec3f( final.x, final.y, final.z);
}



Vec3f Matrix::unProject( Vec3f winLoc, Matrix view, Matrix projection, Rect viewPort )
{
    Vec4f in = Vec4f(winLoc.x, winLoc.y, winLoc.z, 1.0f);
    Matrix mvp = (projection*view).inverse();

    Vec4f out = Matrix::vertexTransform(&in, &mvp);

    if (out.w == 0.0f)
    { return Vec3f::Zero; }

    return Vec3f( out.x/out.w, out.y/out.w, out.z/out.w );
}


Matrix Matrix::matrixScale(Matrix *matrix, float scale)
{
	Matrix result;

	for (int allElements = 0; allElements < 16; allElements++) {
		result.elements[allElements] = matrix->elements[allElements] * scale;
	}

	return result;
}

Matrix Matrix::multiply(Matrix *left, Matrix *right)
{
	Matrix result;

	for(int row = 0; row < 4; row ++)
	{
		for(int column = 0; column < 4; column ++)
		{
			/*result.elements[row * 4 + column]  = left->elements[0 + row * 4] * right->elements[column + 0 * 4];
			result.elements[row * 4 + column] += left->elements[1 + row * 4] * right->elements[column + 1 * 4];
			result.elements[row * 4 + column] += left->elements[2 + row * 4] * right->elements[column + 2 * 4];
			result.elements[row * 4 + column] += left->elements[3 + row * 4] * right->elements[column + 3 * 4];*/
			float accumulator = 0.0f;
			for(int allElements = 0; allElements < 4; allElements ++)
			{
				accumulator += left->elements[allElements * 4 + row] * right->elements[column * 4 + allElements];
			}
			result.elements[column * 4 + row] = accumulator;
		}
	}

	return result;
}

bool Matrix::toEulerAnglesXYZ(float& yaw, float& pitch, float& roll) {
	// rot =  cy*cz          -cy*sz           sy
	//        cz*sx*sy+cx*sz  cx*cz-sx*sy*sz -cy*sx
	//       -cx*cz*sy+sx*sz  cz*sx+cx*sy*sz  cx*cy

	pitch = asin(elements[2]);
	if (pitch < (float)M_PI_2) {
		if (pitch > -(float)M_PI_2) {
			yaw = -atan2(-elements[6], elements[10]);
			roll = -atan2(-elements[1], elements[0]);
			pitch = -pitch;
			return true;
		} else {
			// WARNING.  Not a unique solution.
			float r = atan2(elements[4], elements[5]);
			roll = 0.0f;  // any angle works
			yaw = roll - r;
			pitch = -pitch;
			return false;
		}
	} else {
		// WARNING.  Not a unique solution.
		float r = atan2(elements[4], elements[5]);
		roll = 0.0f;  // any angle works
		yaw = r - roll;
		pitch = -pitch;
		return false;
	}
}

void Matrix::print()
{
	float reordered[16];
	printf("matrix dump ----------------\n");
	for(int row = 0; row < 4; row ++)
	{
		for(int column = 0; column < 4; column ++)
		{
			int src = (row * 4) + column;
			int dst = (column *4) + row;
			reordered[dst] = elements[src];
		}
	}

	for (int i = 0; i < 16; i++)
	{
		printf("%2.8f  ", (double)reordered[i]);
		if ((i+1) % 4 == 0)
		{ printf("\n"); }
	}
	printf("----------------------------\n");
}

}

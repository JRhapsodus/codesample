#ifndef BOX3F_H_
#define BOX3F_H_

#include <stdio.h>
#include "Vec3f.h"
#include "Line3f.h"
#include "Matrix.h"

namespace SIGeometry
{

class Box3f
{
public:
	static Box3f Empty;

	/// <summary> 
	/// Life cycle
	/// </summary> 
	Box3f();
	Box3f(float x, float y, float z, float width, float height, float depth);
	Box3f(Vec3f pos, Vec3f size);
	virtual ~Box3f();


	void debugOutput()
	{
		printf("<Left,Top,Right,Bottom> : <w x h>=== <%f,%f,%f,%f> : < %f x %f>\n",
				(double)getLeft(), (double)getTop(), (double)getRight(), (double)getBottom(), (double)getWidth(), (double)getHeight());
	}
	/// <summary> 
	/// Operators & logic
	/// </summary> 
	bool operator ==(Box3f& b);
	bool operator !=(Box3f& b);
	bool equals(Box3f& obj);
	bool contains( Vec3f &point);
	bool containsIgnoreDepth( Vec3f &point);

	/// <summary> 
	/// Static ops
	/// </summary> 
	static Box3f unionWith(Box3f& a, Box3f& b);
	static Box3f transform(Box3f& box, Matrix& mat);

	/// <summary> 
	/// Instance ops
	/// </summary> 

	Box3f project( Matrix view, Matrix projection );
	Box3f unionWith(Box3f& b);
	bool lineIntersectsFace(float X1, float Y1, float Z1, float X2, float Y2, float Z2, float X3, float Y3, float Z3, Line3f line);
	void transform(Matrix& mat);

	/// <summary> 
	/// Gettors
	/// </summary> 
	float getX();
	float getY();
	float getZ();
	float getLeft();
	float getTop();
	float getFront();
	float getWidth();
	float getHeight();
	float getDepth();
	Vec3f getSize();
	Vec3f getCenter();
	float getRight();
	float getBottom();
	float getBack();

	bool isEmpty();
	float _x, _y, _z;
	float _width, _height, _depth;
};

}

#endif /* BOX3F_H_ */

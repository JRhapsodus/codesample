#include "Quaternion.h"
#include <math.h>

namespace SIGeometry
{

Quaternion Quaternion::identity(1.0f, 0.0f, 0.0f, 0.0f);

Quaternion::Quaternion() : w(0.0f), x(0.0f), y(0.0f), z(0.0f) { }
Quaternion::Quaternion(float W, float X, float Y, float Z) : w(W), x(X), y(Y), z(Z) { }
Quaternion::~Quaternion() { }

Quaternion::Quaternion(const Quaternion& q)
{
	w = q.w;
	x = q.x;
	y = q.y;
	z = q.z;
}

Quaternion& Quaternion::operator=(const Quaternion& q)
{
	if (this == &q) return *this;
	w = q.w;
	x = q.x;
	y = q.y;
	z = q.z;
	return *this;
}

Quaternion Quaternion::fromMatrix(Matrix& m)
{
	Quaternion q;
	q.setRotationMatrix(m);
	return q;
}



//*********************************************************************
//Rotation Matrix is in Form: This is in row major order, so convert
//to colum major order prior to setting quaternion
// 1 - 2y^2 - 2z^2        2xy - 2wz            2xz + 2wy
//    2xy + 2wz        1 - 2x^2 - 2z^2         2yz - 2wx
//    2xz - 2wy           2yz + 2wx         1 - 2x^2 - 2y^2
//*********************************************************************
void Quaternion::setRotationMatrix(Matrix& m)
{
	Matrix::matrixTranspose(&m);
	float r = m._00() + m._11() + m._22();
	// r == 3 - 4(x^2+y^2+z^2) == 4(1-x^2-y^2-z^2) - 1 == 4*w^2 - 1.

	//********************************************************************************
	// Picking positive for w arbitrarily, though other choice is valid too
	//********************************************************************************
	if (r > 0) // In this case, |w| > 1/2.
	{
		w = sqrtf(r + 1.f) * 0.5f;
		float inv4w = 1.f / (4.f * w);
		x = (m._21() - m._12()) * inv4w;
		y = (m._02() - m._20()) * inv4w;
		z = (m._10() - m._01()) * inv4w;
	}
    else if (m._00() > m._11() && m._00() > m._22())
    {
    	x = sqrtf(1 + m._00() - m._11() - m._22()) * 0.5f;
    	const float x4 = 1.f / (4.f * x);
    	y = (m._01() + m._10()) * x4;
    	z = (m._02() + m._20()) * x4;
    	w = (m._21() - m._12()) * x4;
    }

    else if (m._11() > m._22())
    {
    	y = sqrtf(1 + m._11() - m._00() - m._22()) * 0.5f;
    	const float y4 = 1.f / (4.f * y);
    	x = (m._01() + m._10()) * y4;
    	z = (m._12() + m._21()) * y4;
    	w = (m._02() - m._20()) * y4;
    }
	else
	{
		z = sqrtf(1 + m._22() - m._00() - m._11()) * 0.5f;
		const float z4 = 1.f / (4.f * z);
		x = (m._02() + m._20()) * z4;
		y = (m._12() + m._21()) * z4;
		w = (m._10() - m._01()) * z4;
	}
}

//*********************************************************************
//Rotation Matrix is in Form:
// 1 - 2y^2 - 2z^2        2xy - 2wz            2xz + 2wy
//    2xy + 2wz        1 - 2x^2 - 2z^2         2yz - 2wx
//    2xz - 2wy           2yz + 2wx         1 - 2x^2 - 2y^2
//*********************************************************************
Matrix Quaternion::toMatrix(Quaternion& q)
{
	float qx = q.x;
	float qy = q.y;
	float qz = q.z;
	float qw = q.w;
	float matrix[] = {
			1.0f - 2.0f*qy*qy - 2.0f*qz*qz, 2.0f*qx*qy + 2.0f*qz*qw,        2.0f*qx*qz - 2.0f*qy*qw,        0.0f,
            2.0f*qx*qy - 2.0f*qz*qw,        1.0f - 2.0f*qx*qx - 2.0f*qz*qz, 2.0f*qy*qz + 2.0f*qx*qw,        0.0f,
            2.0f*qx*qz + 2.0f*qy*qw,        2.0f*qy*qz - 2.0f*qx*qw,        1.0f - 2.0f*qx*qx - 2.0f*qy*qy, 0.0f,
            0.0f,                           0.0f,                           0.0f,                           1.0f
	};


	Matrix mat(matrix);
    return mat;
}

bool Quaternion::operator==(Quaternion& rhs)
{ return (w == rhs.w && x == rhs.x && y == rhs.y && z == rhs.z); }

bool Quaternion::operator!=(Quaternion& rhs)
{ return !(w == rhs.w && x == rhs.x && y == rhs.y && z == rhs.z); }

Quaternion& Quaternion::operator*=(Quaternion& rhs)
{
	Quaternion q;

	q.w = w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z;
	q.x = w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y;
	q.y = w * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x;
	q.z = w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w;

	*this = q;
	return *this;
}

Quaternion& Quaternion::operator*=(const Quaternion& rhs)
{
	Quaternion q;

	q.w = w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z;
	q.x = w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y;
	q.y = w * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x;
	q.z = w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w;

	*this = q;
	return *this;
}

Quaternion Quaternion::operator*(Quaternion& rhs)
{
	Quaternion q;

	q.w = w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z;
	q.x = w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y;
	q.y = w * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x;
	q.z = w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w;

	return q;
}

const Quaternion Quaternion::operator*(const Quaternion& rhs)
{
	Quaternion q;

	q.w = w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z;
	q.x = w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y;
	q.y = w * rhs.y - x * rhs.z + y * rhs.w + z * rhs.x;
	q.z = w * rhs.z + x * rhs.y - y * rhs.x + z * rhs.w;

	return q;
}

Quaternion Quaternion::normalized()
{
        float mag = sqrtf(w * w + x * x + y * y + z * z);
        return Quaternion(w / mag, x / mag, y / mag, z / mag);
}

Quaternion Quaternion::conjugate()
{
        return Quaternion(w, -x, -y, -z);
}

void Quaternion::normalize()
{
        float mag = sqrtf(w * w + x * x + y * y + z * z);

        w /= mag;
        x /= mag;
        y /= mag;
        z /= mag;
}

Vec3f Quaternion::toEuler()
{
	float rotx = 57.2957795f * (float)atan2(2*((w * x) + (y * z)), 1 - (2 * ((x* x) + (y * y))));
	float roty = 57.2957795f * (float)asin(2 * ((w * y) - (z * x)));
	float rotz = 57.2957795f * (float)atan2(2 * ((w * z) + (x * y)), 1 - (2 * ((y * y) + (z * z))));
	return Vec3f(rotx, roty, rotz);
}

Vec3f Quaternion::operator*(Vec3f& rhs)
{ return Quaternion::toMatrix(*this) * rhs; }

const Vec3f operator*(Vec3f& v, const Quaternion& m)
{
	Quaternion nonConst = m;
	return Quaternion::toMatrix(nonConst) * v;
}

Vec3f operator*(Vec3f& v, Quaternion& m)
{ return Quaternion::toMatrix(m) * v; }

const Vec3f Quaternion::operator*(const Vec3f& rhs)
{ return Quaternion::toMatrix(*this) * rhs; }

Quaternion Quaternion::angleAxis(float angle, Vec3f& axis)
{
	Vec3f vn = axis.normalized();
	angle *=  0.0174532925f; // To radians!
	angle *= 0.5f;
	float sinAngle = sin(angle);

	return Quaternion(cos(angle), vn.x * sinAngle, vn.y * sinAngle, vn.z * sinAngle);
}

Quaternion Quaternion::euler(float X, float Y, float Z)
{
	X *= 0.0174532925f; // To radians!
	Y *= 0.0174532925f; // To radians!
	Z *= 0.0174532925f; // To radians!

	X *= 0.5f;
	Y *= 0.5f;
	Z *= 0.5f;

	float sinx = sinf(X);
	float siny = sinf(Y);
	float sinz = sinf(Z);
	float cosx = cosf(X);
	float cosy = cosf(Y);
	float cosz = cosf(Z);

	Quaternion q;

	q.w = cosx * cosy * cosz + sinx * siny * sinz;
	q.x = sinx * cosy * cosz + cosx * siny * sinz;
	q.y = cosx * siny * cosz - sinx * cosy * sinz;
	q.z = cosx * cosy * sinz - sinx * siny * cosz;

	return q;
}

void Quaternion::setEuler(float X, float Y, float Z)
{
	X *= 0.0174532925f; // To radians!
	Y *= 0.0174532925f; // To radians!
	Z *= 0.0174532925f; // To radians!

	X *= 0.5f;
	Y *= 0.5f;
	Z *= 0.5f;

	float sinx = sinf(X);
	float siny = sinf(Y);
	float sinz = sinf(Z);
	float cosx = cosf(X);
	float cosy = cosf(Y);
	float cosz = cosf(Z);

	w = cosx * cosy * cosz + sinx * siny * sinz;
	x = sinx * cosy * cosz + cosx * siny * sinz;
	y = cosx * siny * cosz - sinx * cosy * sinz;
	z = cosx * cosy * sinz - sinx * siny * cosz;
}

void Quaternion::toAngleAxis(float* angle, Vec3f* axis)
{
	*angle = acosf(w);
	float sinz = sinf(*angle);

	if (fabsf(sinz) > 1e-4f)
	{
		sinz = 1.0f / sinz;

		axis->x = x * sinz;
		axis->y = y * sinz;
		axis->z = z * sinz;

		*angle *= 2.0f * 57.2957795f;
		if (*angle > 180.0f)
		{ *angle = 360.0f - *angle; }
	}
	else
	{
		*angle = 0.0f;
		axis->x = 1.0f;
		axis->y = 0.0f;
		axis->z = 0.0f;
	}
}

Quaternion Quaternion::inverse(Quaternion& rotation)
{ return Quaternion(rotation.w, -1.0f * rotation.x, -1.0f * rotation.y, -1.0f * rotation.z); }

float Quaternion::dot(Quaternion& a, Quaternion& b)
{ return (a.w * b.w + a.x * b.x * a.y * b.y + a.z * b.z); }

float Quaternion::dot(Quaternion& b)
{ return (w * b.w + x * b.x * y * b.y + z * b.z); }

float Quaternion::angle(Quaternion& a, Quaternion& b)
{
	float degrees = acosf((b * Quaternion::inverse(a)).w) * 2.0f * 57.2957795f;
	if (degrees > 180.0f)
			return 360.0f - degrees;
	return degrees;
}

Quaternion operator*(float f, Quaternion& m)
{ return Quaternion(m.w * f, m.x * f, m.y * f, m.z * f); }

const Quaternion operator*(float f, const Quaternion& m)
{ return Quaternion(m.w * f, m.x * f, m.y * f, m.z * f); }

Quaternion Quaternion::operator*(float& f)
{ return Quaternion(w * f, x * f, y * f, z * f); }

const Quaternion Quaternion::operator*(const float& f)
{ return Quaternion(w * f, x * f, y * f, z * f); }

Quaternion Quaternion::operator+(Quaternion& rhs)
{ return Quaternion(w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.z); }

const Quaternion Quaternion::operator+(const Quaternion& rhs)
{ return Quaternion(w + rhs.w, x + rhs.x, y + rhs.y, z + rhs.z); }

Quaternion Quaternion::lerp(Quaternion& from, Quaternion& to, float t)
{
	Quaternion src = from * (1.0f - t);
	Quaternion dst = to * t;
	Quaternion q = src + dst;
	return q;
}

Quaternion Quaternion::operator- (const Quaternion& rkQ) const
{
    return Quaternion(w-rkQ.w,x-rkQ.x,y-rkQ.y,z-rkQ.z);
}

Quaternion Quaternion::operator- () const
{
    return Quaternion(-w,-x,-y,-z);
}

Quaternion Quaternion::nlerp(Quaternion& from, Quaternion& to, float t, bool shortestPath)
{
	Quaternion result;
    float fCos = from.dot(to);
	if (fCos < 0.0f && shortestPath)
	{ result = from + t * ((-to) - from); }
	else
	{ result = from + t * (to - from); }
    result.normalize();
    return result;
}


Quaternion Quaternion::slerp(Quaternion& from, Quaternion& to, float t, bool shortestPath)
{
	float cosTheta = from.dot(to);
	double startInterp, endInterp;

	// if "angle" between quaternions is less than 90 degrees
	if (cosTheta >= EPSILON)
	{
		// if angle is greater than zero
		if ((1.0f - cosTheta) > EPSILON)
		{
			// use standard slerp
			float theta = cosf(cosTheta);
			float recipSinTheta = 1.0f / sinf(theta);

			startInterp = sinf((1.0f - t) * theta) * recipSinTheta;
			endInterp = sinf(t * theta) * recipSinTheta;
		}
		// angle is close to zero
		else
		{
		// use linear interpolation
			startInterp = 1.0f - t;
			endInterp = t;
		}
	}
	// otherwise, take the shorter route
	else
	{
		// if angle is less than 180 degrees
		if ((1.0f + cosTheta) > EPSILON)
		{
			// use slerp w/negation of start quaternion
			float theta = cosf(-cosTheta);
			float recipSinTheta = 1.0f / sinf(theta);

			startInterp = sinf((t - 1.0f) * theta) * recipSinTheta;
			endInterp = sinf(t * theta) * recipSinTheta;
		}
		// angle is close to 180 degrees
		else
		{
			// use lerp w/negation of start quaternion
			startInterp = t - 1.0f;
			endInterp = t;
		}
	}

	Quaternion end = to*endInterp;
	Quaternion start = from*startInterp;
	return start + end;

}

#define m00 right.x
#define m01 up.x
#define m02 forward.x
#define m10 right.y
#define m11 up.y
#define m12 forward.y
#define m20 right.z
#define m21 up.z
#define m22 forward.z

Quaternion Quaternion::lookRotation(Vec3f& localForward, Vec3f& upDirection)
{
	Vec3f forward = localForward;
	Vec3f up = upDirection;
	Vec3f::orthoNormalize(&forward, &up);
	Vec3f right = Vec3f::cross(up, forward);

	Quaternion ret;

	ret.w = sqrtf(1.0f + m00 + m11 + m22) * 0.5f;
	if( ret.w == 0 )
	{
		ret.w = 0;
		ret.x = 0;
		ret.y = 0;
		ret.z = 0;
		return ret;
	}

	float w4_recip = 1.0f / (4.0f * ret.w);
	ret.x = (m21 - m12) * w4_recip;
	ret.y = (m02 - m20) * w4_recip;
	ret.z = (m10 - m01) * w4_recip;

	return ret;
}

Quaternion Quaternion::lookRotation(Vec3f& lookAt)
{
	Vec3f up = Vec3f::Up;
	Vec3f forward = lookAt;
	Vec3f::orthoNormalize(&forward, &up);
	Vec3f right = Vec3f::cross(up, forward);

    Quaternion ret;
	ret.w = sqrtf(1.0f + m00 + m11 + m22) * 0.5f;
	if( ret.w == 0 )
	{
		ret.w = 0;
		ret.x = 0;
		ret.y = 0;
		ret.z = 0;
		return ret;

	}

	float w4_recip = 1.0f / (4.0f * ret.w);
	ret.x = (m21 - m12) * w4_recip;
	ret.y = (m02 - m20) * w4_recip;
	ret.z = (m10 - m01) * w4_recip;

	return ret;
}

void Quaternion::setLookRotation(Vec3f& lookAt)
{
	Vec3f up = Vec3f::Up;
	Vec3f forward = lookAt;
	Vec3f::orthoNormalize(&forward, &up);
	Vec3f right = Vec3f::cross(up, forward);

	w = sqrtf(1.0f + m00 + m11 + m22) * 0.5f;
	if( w == 0 )
	{
		w = 0;
		x = 0;
		y = 0;
		z = 0;
	}
	else
	{
		float w4_recip = 1.0f / (4.0f * w);
		x = (m21 - m12) * w4_recip;
		y = (m02 - m20) * w4_recip;
		z = (m10 - m01) * w4_recip;
	}
}

void Quaternion::setLookRotation(Vec3f& lookAt, Vec3f& upDirection)
{
	Vec3f forward = lookAt;
	Vec3f up = upDirection;
	Vec3f::orthoNormalize(&forward, &up);
	Vec3f right = Vec3f::cross(up, forward);

	w = sqrtf(1.0f + m00 + m11 + m22) * 0.5f;
	if( w == 0 )
	{
		w = 0;
		x = 0;
		y = 0;
		z = 0;
	}
	else
	{
		float w4_recip = 1.0f / (4.0f * w);
		x = (m21 - m12) * w4_recip;
		y = (m02 - m20) * w4_recip;
		z = (m10 - m01) * w4_recip;
	}
}

#undef m00
#undef m01
#undef m02
#undef m10
#undef m11
#undef m12
#undef m20
#undef m21
#undef m22

}

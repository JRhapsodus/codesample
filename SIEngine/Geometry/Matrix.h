#ifndef INCLUDE_MATRIX_H_
#define INCLUDE_MATRIX_H_

#include "Vec3f.h"
#include "Primitives.h"

namespace SIGeometry
{

class Matrix
{
public:
	Matrix();
	Matrix(const float* array); /// Construct with elements array
	Matrix( float m11, float m12, float m13, float m14, float m21, float m22, float m23, float m24, float m31, float m32, float m33, float m34, float m41, float m42, float m43, float m44 );
	virtual ~Matrix();

	float* getAsArray();
	/// Get a float from elements at index
	float& operator[] (unsigned index);

	Matrix operator* (Matrix right);
	Vec3f operator* (Vec3f right);
	Matrix& operator=(const Matrix &another);
	Matrix operator+ (Matrix right);

	Matrix inverse();
	void debugOutput();


	static Matrix identityMatrix; /// Basic identity matrix

	static Vec4f vertexTransform(Vec4f *vector, Matrix *matrix);
	static Vec3f vertexTransform(Vec3f *vector, Matrix *matrix);
	static void matrixTranspose(Matrix *matrix);

	static Matrix createRotationX(float angle);
	static Matrix createRotationY(float angle);
	static Matrix createRotationZ(float angle);

	static Matrix createTranslation(float x, float y, float z);
	static Matrix createScaling(float x, float y, float z);

	static Matrix matrixPerspectiveLH(float FOV, float ratio, float zNear, float zFar);
	static Matrix matrixPerspectiveRH(float FOV, float ratio, float zNear, float zFar);
	static Matrix matrixCameraLookAt(Vec3f eye, Vec3f center, Vec3f up);


	static Matrix orthoLH(float width, float height, float znearPlane, float zfarPlane);
	static Matrix orthoOffCenterLH(float left, float right, float bottom, float top, float znearPlane, float zfarPlane);
	static Matrix orthoOffCenterRH(float left, float right, float bottom, float top, float znearPlane, float zfarPlane);
	static Matrix orthoRH(float width, float height, float znearPlane, float zfarPlane);

	static Matrix matrixInvert(Matrix *matrix);
	static Matrix createTranslation(Vec3f translation);
	static Matrix createEulerRotation(Vec3f eulerAngles);
	static Matrix createScale(Vec3f scale);

	static float matrixDeterminant(float *matrix);
	static float matrixDeterminant(Matrix *matrix);
	static Matrix matrixScale(Matrix *matrix, float scale);

	static Matrix rotationMatrix( Vec3f right, Vec3f up, Vec3f forward);

	Vec3f multiplyPoint(Vec3f& vec);
	static Vec3f unProject( Vec3f winLoc, Matrix view, Matrix projection, Rect viewPort );
	static Vec3f project( Vec3f worldLoc, Matrix view, Matrix projection);


	bool toEulerAnglesXYZ(float& yaw, float& pitch, float& roll);
	void print();
private:
	static const float identityArray[];
	static Matrix multiply(Matrix *left, Matrix *right);
	float elements[16];



public:

	void _00(float a) { elements[0] = a; }
	void _01(float a) { elements[1] = a; }
	void _02(float a) { elements[2] = a; }
	void _03(float a) { elements[3] = a; }

	void _10(float a) { elements[4] = a; }
	void _11(float a) { elements[5] = a; }
	void _12(float a) { elements[6] = a; }
	void _13(float a) { elements[7] = a; }

	void _20(float a) { elements[8] = a; }
	void _21(float a) { elements[9] = a; }
	void _22(float a) { elements[10] = a; }
	void _23(float a) { elements[11] = a; }

	void _30(float a) { elements[12] = a; }
	void _31(float a) { elements[13] = a; }
	void _32(float a) { elements[14] = a; }
	void _33(float a) { elements[15] = a; }

	float _00() { return elements[0]; }
	float _01() { return elements[1]; }
	float _02() { return elements[2]; }
	float _03() { return elements[3]; }

	float _10() { return elements[4]; }
	float _11() { return elements[5]; }
	float _12() { return elements[6]; }
	float _13() { return elements[7]; }

	float _20() { return elements[8]; }
	float _21() { return elements[9]; }
	float _22() { return elements[10]; }
	float _23() { return elements[11]; }

	float _30() { return elements[12]; }
	float _31() { return elements[13]; }
	float _32() { return elements[14]; }
	float _33() { return elements[15]; }

};

}

#endif


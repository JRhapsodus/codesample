#ifndef INCLUDE_TRANSFORM_H_
#define INCLUDE_TRANSFORM_H_

#include "Primitives.h"
#include "Matrix.h"
#include "Quaternion.h"

namespace SIGeometry
{

class Transform
{
public:
	Transform* parentTransform;

	/// <summary>
	/// Pivot * Position * Rotation * Scale
	/// </summary>
	Matrix getLocalToParent();

	/// <summary>
	/// Returns cached localToWorld
	/// </summary>
	Matrix getLocalToWorld();

	/// <summary>
	/// Inverse of localToWorld
	/// </summary>
	Matrix getWorldToLocal();

	/// <summary>
	/// worldToLocal * point
	/// </summary>
	Vec3f convertWorldToLocal( Vec3f point);

	/// <summary>
	/// localToWorld * Vec3f::Forward
	/// </summary>
	Vec3f getWorldForward();

	/// <summary>
	/// localToWorld * Vec3f::Up
	/// </summary>
	Vec3f getWorldUp();

	/// <summary>
	/// localToWorld * Vec3f::Left
	/// </summary>
	Vec3f getWorldLeft();

	/// <summary>
	/// Sets position, marks transform dirty
	/// </summary>
	void setPosition( Vec3f position );

	/// <summary>
	/// Sets rotation, marks transform dirty
	/// </summary>
	void setRotation( Quaternion rotation );

	/// <summary>
	/// Sets rotation based on an euler representation, marks transform dirty
	/// </summary>
	void setEulerRotation( Vec3f rotation );

	/// <summary>
	/// Sets scale, marks transform dirty
	/// </summary>
	void setScale( Vec3f scale );

	/// <summary>
	/// Gets Vec3f euler representation of rotation quaternion
	/// </summary>
	Vec3f getEulerRotation();

	/// <summary>
	/// Gets position
	/// </summary>
	Vec3f getPosition();

	/// <summary>
	/// Gets rotation quaternion
	/// </summary>
	Quaternion getRotation();

	/// <summary>
	/// Gets scale
	/// </summary>
	Vec3f getScale();

	/// <summary>
	/// Marks transform dirty, next request for localToWorld will cause
	/// a recomputation of concatented Matrices
	/// </summary>
	void markDirty();

	/// <summary>
	/// Create empty transform
	/// </summary>
	Transform();
	~Transform();

private:
	void computeLocalToWorld();

	/// <summary>
	/// MVP model for all gameobjects
	/// </summary>
	Matrix localToWorld;

	/// <summary>
	/// Whether current localToWorld matrix is invalid
	/// </summary>
	bool isDirty;

	/// <summary>
	/// Affine component: Position
	/// </summary>
	Vec3f position;

	/// <summary>
	/// Affine component: Rotation
	/// </summary>
	Quaternion rotation;

	/// <summary>
	/// Affine component: Scale
	/// </summary>
	Vec3f scale;

	/// <summary>
	/// Affine component: Pivot
	/// </summary>
	Vec3f pivot;

};

}

#endif

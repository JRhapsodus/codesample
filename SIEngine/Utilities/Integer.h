#ifndef INTEGER_H_
#define INTEGER_H_

#include "SIEngine/Include/SIBase.h"

namespace SIUtils
{

class Integer : public BaseObject
{
	CLASSEXTENDS(Integer,BaseObject);
	ADD_TO_CLASS_MAP;
private:
	long m_l;

public:
	enum
	{
		MAX_VALUE = 0x7fffffff
	};

	Integer( int i ) : m_l(i) {}
	Integer( LPCTSTR lpsz ) : m_l(parseInt(lpsz)) {}
	static int parseInt( LPCTSTR lpsz );

	long longValue();
	int intValue();

	virtual int hashValue() const;
	virtual bool equals( BaseObject& obj ) const;
};

}
#endif /* INTEGER_H_ */

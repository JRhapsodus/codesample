#ifndef CALENDER_H_
#define CALENDER_H_

#include "SIEngine/Core/StringClass.h"
#include "SIEngine/Include/SIManagers.h"

namespace SIUtils
{

class Calender: public BaseObject
{
	CLASSEXTENDS(Calender,BaseObject);
public:
	Calender();
	virtual ~Calender();

	int daysInMonthYear(String month,String year);
	int getMonthIntRepresentation(String month);
	int getDayIntRepresentation(String day);
	bool isLeapYear(String year);
	bool isValidBirthYear(String year);
	String dayStrForInt(int day);
	String monthStrForInt(int month);
	String formattedDayStrForInt(int day);
	String formattedMonthStrForInt(int month);
	String formattedDayStr(String day);
	String formattedMonthStr(String month);
};

} /* namespace Glasgow */

#endif /* CALENDER_H_ */

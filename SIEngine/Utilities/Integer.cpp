#include "Integer.h"
#include <stdio.h>

namespace SIUtils
{

int Integer::hashValue() const
{
	return (int)m_l;
}


bool Integer::equals( BaseObject& obj ) const
{
	if (obj.typeOf(Integer::type()))
	{
		// just check if we are the same object.
		return ((Integer*)&obj)->m_l == m_l;
	}

	return false;
}

int Integer::parseInt( LPCTSTR lpsz )
{
	char * pEnd;
	return strtol(lpsz, &pEnd, 10);
}

long Integer::longValue() { return m_l; }
int Integer::intValue() { return (int)m_l; }

}


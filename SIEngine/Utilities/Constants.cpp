#include "Constants.h"

namespace SIUtils
{
	///<summary>
	/// Constants relating to known content and packages
	///</summary>
	long 	Constants::Content::petPlayWorldProductID 		= 3211277;

	///<summary>
	/// All timer and time based constants
	///</summary>
	int		Constants::Time::activationTimeOut 				= 60*3;
	float	Constants::Time::registrationCodeTimeOut		= 60*2;
	float	Constants::Time::wifiInputTimer					= 60*2;
	double 	Constants::Time::kNanoSecondsInASecond   		= 1000000000;

	///<summary>
	/// All paths and names of flags defined in Leap's /flags/ folder
	///</summary>
	String  Constants::Paths::flagUiReady 					= "/tmp/ui_ready";
	String  Constants::Paths::flagUiLoaded 					= "/tmp/ui_loaded";
	String  Constants::Paths::flagFirmwareInstalled  		= "/flags/firmware-installed";
	String  Constants::Paths::flagFirmwareUpdateReady  		= "/flags/update-firmware";
	String  Constants::Paths::glasgowSettingsFile			= "/LF/Bulk/glasgowUISettings.cfg";
	String	Constants::Paths::firmwareVersion				= "/etc/version";
	String	Constants::Paths::macEthernetAddressFile		= "/sys/class/net/eth0/address";
	String  Constants::Paths::macWifiAddressFile			= "/sys/class/net/wlan0/address";
	String  Constants::Paths::LFBulk						= "/LF/Bulk/";
	String	Constants::Paths::guestProfileFile				= "/LF/Bulk/Data/Local/254/profile_private.dsc";
	String  Constants::Paths::ethernetStatusFile			= "/sys/class/net/eth0/carrier";

	///<summary>
	/// Parameters like names of keys and values
	///</summary>
	String 	Constants::Parameters::keySneakPeeks			= "Parent_SneakPeekShow";
	String	Constants::Parameters::keyIsActivated			= "isActivated";
	String	Constants::Parameters::keyLastUser				= "lastUser";
	String	Constants::Parameters::keyLastGame				= "lastGame";
	String	Constants::Parameters::keyLastTheme				= "lastUsedTheme";
	String  Constants::Parameters::keyTimeModifier			= "keyTimeModifier";
	String  Constants::Parameters::keyExitReason			= "keyExitReason";
	String  Constants::Parameters::keyExitAppState			= "keyExitAppState";
	String  Constants::Parameters::keyData1					= "keyData1";
	String  Constants::Parameters::keyData2					= "keyData2";
	String  Constants::Parameters::keyLaunchFrom			= "keyLaunchFrom";
	String  Constants::Parameters::keyLaunchTo				= "keyLaunchTo";
	String  Constants::Parameters::keyLaunchState			= "keyLaunchState";
	String	Constants::Parameters::keyWristStrapComplete	= "keyWristStrapComplete";

	///<summary>
	/// System constants
	///</summary>
	String	Constants::System::backDoorGateCode				= "196backspacebutton87";
	String  Constants::System::backDoorUnlockCode			= "092backspacebutton89";
	String	Constants::System::appName						= "";
	AppID 	Constants::System::appID						= AppIDNone;
	int		Constants::System::wifiPasswordMaximumCharLimit = 128;
	int 	Constants::System::userNameMaximumCharLimit		= 12;
	int		Constants::System::minBirthYear					= 1910;
	String 	Constants::System::genderMaleKey				= "male";
	String	Constants::System::genderFemaleKey				= "female";
	int		Constants::System::genderMaleEnum				= 0;
	int		Constants::System::genderFemaleEnum				= 1;
	int		Constants::System::maxNumUsers					= 5;

	///<summary>
	/// UI constants
	///</summary>
	int		Constants::UI::wifiPasswordDisplayLimit			= 21;
	String	Constants::UI::textTruncationEndPattern			= "...";
	String	Constants::UI::encryptionDisplayPattern			= "* ";
	int		Constants::UI::wifiNetworkNameMaxLines			= 1;
	int 	Constants::UI::contentItemBannerTextMaxLines	= 2;
	float	Constants::UI::addProfileButtonYOffset			= 65;
	String 	Constants::UI::guestProfileName					= "Guest";

	///<summary>
	/// Regular expressions, patterns
	///</summary>
	int Constants::Text::NPC_CarriageReturn 				= 10;
	int Constants::Text::NPC_Space 							= 32;

	///<summary>
	/// Common units for math
	///</summary>
	double Constants::Units::UnitGigaByte 					= 1024*1024*1024;
	double Constants::Units::UnitMegaByte					= 1024*1024;
	double Constants::Units::UnitKiloByte					= 1024;

Constants::Constants()
{ }

Constants::~Constants()
{ }

}

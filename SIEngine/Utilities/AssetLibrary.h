#ifndef ASSETLIBRARY_H_
#define ASSETLIBRARY_H_
#include <AppManager.h>
#include <memory.h>
#include <png.h>
#include <sstream>
#include "SIEngine/Include/SIBase.h"

namespace SIUtils
{

/// <summary> 
/// File parsing helpers
/// </summary> 
struct line_reader
{
public:
	FILE	*f;
	char	*buf;
	size_t	 siz;
};

/// <summary> 
/// Utility class for doing small batch work compiling paths, etc..
/// </summary> 
class AssetLibrary
{
public:
	/// <summary> 
	/// Path processing helpers
	/// </summary> 
	static String getCompletePath( String relativeProjectPath );

	/// <summary>
	/// Returns file location of the sound: eg "MOO.ogg" => /LF/Base/GlasgowUI/Shared/Sounds/MOO.ogg
	/// </summary>
	static String getSoundPath( String relativeProjectPath );

	/// <summary>
	/// Returns file location of the sound: eg "piece.png" => /LF/Base/GlasgowUI/Shared/Textures/Themes/Cave/piece.png
	/// </summary>
	static String getQtImagePath( String textureName );

	/// <summary>
	/// Returns the directory of where the app is installed
	/// </summary>
	static String getInstallDir();

	/// <summary>
	/// Full path to where the video widget is
	/// </summary>
	static String getVideoWidgetPath();

	/// <summary>
	/// Full path to where the error widget is
	/// </summary>
	static String getErrorWidgetPath();

	/// <summary>
	/// Parent settings path
	/// </summary>
	static String getParentSettingsPath();

	/// <summary>
	/// GlasgowUI Path
	/// </summary>
	static String getGlasgowUIPath();

	/// <summary> 
	/// Opens a file in binary mode for reading data, takes full path
	/// </summary> 
	static FILE* getBinaryFile(String fileName);

	/// <summary>
	/// Opens a file using the options flag.  r for ready, w for write, b for binary
	/// </summary>
	static FILE* getFileHandle(String fileName, String options);

	/// <summary>
	/// Opens a file using partial information about the file, .txt file in folder blah ?
	/// </summary>
	static FILE* getFile(String folderName, String fileName, String extension, String fileOptions );

	/// <summary>
	/// Opens a prefab file
	/// </summary>
	static FILE* getPrefab( String prefabName );

	/// <summary>
	/// Opens an animation file
	/// </summary>
	static FILE* getAnimationFile( String animName );

	/// <summary>
	/// Finds the place where the images are
	/// </summary>
	static FILE* getImageFile( String imageName );

	/// <summary>
	/// Returns a file for the csv string tables
	/// </summary>
	static FILE* getCSVFile(String fileName);

	/// <summary>
	/// Reads the entire file off hard drive into ram and returns it
	/// </summary>
	static String getFileContents( String fullFilePath );

	/// <summary>
	/// Removes the file at full path
	/// </summary>
	static bool removeFile( String fullFilePath );

	/// <summary>
	/// Determines if the fullFilePath points to an executable file
	/// </summary>
	static bool executableExists(String fullFilePath );

	/// <summary>
	/// Determines if the path has a file
	/// </summary>
	static bool fileExists(String fullFilePath );

	/// <summary>
	/// Writes the contents of insideFlag into a file at fullFilePath
	/// </summary>
	static void writeFile( String fullFilePath, String insideFlag );



	/// <summary> 
	/// JLF
	/// This following is donkey karl crap and it needs to go or be wrapped into a better API !!!
	/// </summary> 
	static void lr_free(struct line_reader *lr);
	static char* next_line(struct line_reader *lr, size_t *len);
	static void lr_init(struct line_reader *lr, FILE *f);
	static void installTheme(String themeArchive);
};

}
#endif

#include "AssetLibrary.h"
#include "Glasgow/Scenes/SceneManager.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "SIEngine/Core/Exception.h"

using namespace Glasgow;

namespace SIUtils
{

static String g_BasePath = "/LF/Base/GlasgowUI/";


String AssetLibrary::getInstallDir()
{ return g_BasePath; }

/// <summary>
/// Full path to where the video widget is
/// </summary>
String AssetLibrary::getVideoWidgetPath()
{ return "/LF/Base/VideoWidget/VideoWidget"; }

String AssetLibrary::getErrorWidgetPath()
{ return "/LF/Base/ErrorWidget/ErrorWidget"; }

String AssetLibrary::getParentSettingsPath()
{ return "/LF/Base/ParentSettings/ParentSettings"; }

String AssetLibrary::getGlasgowUIPath()
{ return "/LF/Base/GlasgowUI/GlasgowUI"; }

/// <summary>
/// Removes the file at full path
/// </summary>
bool AssetLibrary::removeFile( String fullFilePath )
{
	if( remove( fullFilePath.str() ) != 0 )
	{
		printf("ERROR Deleting file [%s]\n", fullFilePath.str());
		return false;
	}
	else
	{
		printf("Removed file [%s]\n", fullFilePath.str());
		return true;
	}
}

/// <summary>
/// Reads the entire file off hard drive into ram and returns it
/// </summary>
String AssetLibrary::getFileContents( String fullFilePath )
{
	String fileContents = "";
	FILE* fp = fopen(fullFilePath.str(), "r");
	if (fp == NULL)
	{ ThrowException::fileNotFoundException(fullFilePath); }
	else
	{
		char fileLine[500];

		while(!feof(fp))
		{
		    if (fgets(fileLine,100,fp))
		    { fileContents += String(fileLine); }
		}

		fclose(fp);
	}

	return fileContents;
}

/// <summary>
/// Determines if the path has a file
/// </summary>
bool AssetLibrary::fileExists(String fullFilePath )
{
	if( !fullFilePath.startsWith("/") || fullFilePath.isEmpty() )
	{ return false; }

	BaseUtils* utils = BaseUtils::Instance();
	if( utils->FileIsDir(fullFilePath.str()))
	{ return false; }

	struct stat buffer;
	return (stat(fullFilePath.str(), &buffer) == 0 );
}

/// <summary>
/// Determines if the fullFilePath points to an executable file
/// </summary>
bool AssetLibrary::executableExists(String fullFilePath )
{
	if( !fullFilePath.startsWith("/") )
	{ return false; }

	struct stat buffer;
	return ((stat(fullFilePath.str(), &buffer) == 0) && (buffer.st_mode & S_IXUSR));
}

/// <summary>
/// Writes the contents of insideFlag into a file at fullFilePath
/// </summary>
void AssetLibrary::writeFile( String fileName, String insideFlag )
{
	FILE* fp = fopen(fileName.str(), "w");

	if (fp == NULL)
	{ ThrowException::fileNotFoundException(fileName); }
	else
	{
		fprintf(fp, "%s", insideFlag.str());
		fclose(fp);
	}
}

/// <summary>
/// Returns file location of the sound: eg "piece.png" => /LF/Base/GlasgowUI/Shared/Textures/Themes/Cave/piece.png
/// </summary>
String AssetLibrary::getQtImagePath( String textureName )
{
	String base_path = CSystemData::Instance()->GetBasePath();
	printf("CSystemData::GetBasePath() == %s\n", base_path.str() );
	String filePath = String(g_BasePath + "Shared/Textures/Widgets/") + textureName + ".png";
	printf("FilePath for Widget: %s\n", filePath.str() );
	return filePath;
}

/// <summary>
/// Returns file location of the sound: eg "MOO.ogg" => /LF/Base/GlasgowUI/Shared/Sounds/MOO.ogg
/// </summary>
String AssetLibrary::getSoundPath( String relativeProjectPath )
{
	return g_BasePath + String("Shared/Sounds/") + relativeProjectPath + String(".ogg");
}

/// <summary>
/// Opens an animation file
/// </summary>
FILE* AssetLibrary::getAnimationFile( String animName )
{
	String completePath = g_BasePath + String("Shared/Animations/") + animName + String(".dat");
	FILE *fp = fopen(completePath.str(), "r");
	if (fp == NULL)
	{ ThrowException::fileNotFoundException(completePath); }

	return fp;
}

/// <summary>
/// Finds the place where the images are
/// </summary>
FILE* AssetLibrary::getImageFile( String imageName )
{
	String theme = SystemSettings::getTheme();
	String completePath = "";

	printf("Image name %s\n", imageName.str() );

	if( imageName.startsWith("/") )
	{ completePath = imageName; }
	else if( imageName.indexOf("/Fonts/") != -1 )
	{ completePath = g_BasePath + imageName;  }
	else if( theme != null && imageName == "themeAtlas.png" )
	{ completePath = g_BasePath + String("Shared/Textures/Themes/") + theme + "/" + imageName; }
	else
	{ completePath = g_BasePath + String("Shared/Textures/Default/") + imageName; }

//	printf("Complete Path %s\n", completePath.str() );
	FILE *fp = fopen(completePath.str(), "rb");

	//**************************************************************************
	// If Not found, this this might be a full relative path resource
	//**************************************************************************
	if (fp == NULL)
	{
		completePath = g_BasePath + imageName;
		printf("Resource not found. Trying %s\n", completePath.str() );

		fp = fopen(completePath.str(), "rb");
		if ( fp == NULL )
		{
			//attempt 2 failed
			ThrowException::fileNotFoundException(completePath);
		}
	}

	return fp;
}

/// <summary>
/// Opens a prefab file
/// </summary>
FILE* AssetLibrary::getPrefab( String prefabName )
{
	String path = g_BasePath + String("Shared/Prefabs/") + prefabName + String(".prefab");
	FILE *fp = fopen(path.str(), "r");
	if (fp == NULL)
	{ ThrowException::fileNotFoundException(path); }

	return fp;
}

/// <summary>
/// Returns a file for the csv string tables
/// </summary>
FILE* AssetLibrary::getCSVFile(String fileName )
{
	String completePath = g_BasePath + String("Shared/Layouts/") + fileName + String(".csv");
//	printf("AssetLibrary::getCSVFile : %s\n", completePath.str());
	FILE *fp = fopen(completePath.str(), "r");
	if (fp == NULL)
	{ ThrowException::fileNotFoundException(completePath); }

	return fp;
}

/// <summary>
/// Path processing helpers
/// </summary>
String AssetLibrary::getCompletePath( String relativeProjectPath )
{
	String theme = SystemSettings::getTheme();
	if (theme != NULL) {
		String path = g_BasePath + "Themes/" + theme + "/" + relativeProjectPath;
		if (fileExists(path))
		{ return path; }
	}

	// Check for asset in the Shared assets
	String path = g_BasePath + "Shared/" + relativeProjectPath;
	if (fileExists(path))
	{
		return path;
	}

	// Check for asset in the default theme
	path = g_BasePath + "Themes/default/" + relativeProjectPath;
	if (fileExists(path)) {
		return path;
	}

	ThrowException::fileNotFoundException(path);
}

//************************************************************************************************
// Opens file using Brio path and file options
//************************************************************************************************
FILE* AssetLibrary::getFileHandle(String fileName, String options)
{
	String texturePath = getCompletePath(fileName);
	FILE *fp = fopen(texturePath, options);
	if (fp == NULL)
	{ ThrowException::fileNotFoundException(fileName); }

	return fp;
}

//************************************************************************************************
// Opens file using Brio path
//************************************************************************************************
FILE* AssetLibrary::getBinaryFile(String fileName)
{ return AssetLibrary::getFileHandle( fileName, S("rb")); }

//************************************************************************************************
// Opens file using eclipse folder structure
//************************************************************************************************
FILE* AssetLibrary::getFile(String folderName, String fileName, String extension, String fileOptions )
{
	String fontDataFile;
	bool pathFound = false;
	String theme = SystemSettings::getTheme();
	// Check for asset in the current theme
	if (theme != NULL)
	{
		fontDataFile = g_BasePath + "Themes/" + theme + "/" + folderName + fileName + extension;
		if (fileExists(fontDataFile))
		{ pathFound = true; }
	}

	if (pathFound == false)
	{
		fontDataFile = g_BasePath + "Shared/" + folderName + fileName + extension;
		if (fileExists(fontDataFile))
		{ pathFound = true; }
	}

	if (pathFound == false)
	{
		fontDataFile = g_BasePath + "Themes/default/" + folderName + fileName + extension;
		if (fileExists(fontDataFile))
		{ pathFound = true; }
	}

	if (pathFound == false)
	{ fontDataFile = g_BasePath + folderName + fileName + extension; }

	if (!fileExists(fontDataFile))
	{ printf("[Error] File %s not found in theme or shared assets\n", (folderName + fileName + extension).str()); }

	FILE *fp = fopen(fontDataFile.str(), fileOptions.str());
	if (fp == NULL)
	{ ThrowException::fileNotFoundException(fontDataFile); }
	return fp;
}


//************************************************************************************************
// Initializes a line reader _lr_ for the stream _f_
//************************************************************************************************
void AssetLibrary::lr_init(struct line_reader *lr, FILE *f)
{
	lr->f = f;
	lr->buf = NULL;
	lr->siz = 0;
}

//************************************************************************************************
// Reads the next line. If successful, returns a pointer to the line,
// and sets *len to the number of characters, at least 1. The result is
// _not_ a C string; it has no terminating '\0'. The returned pointer
// remains valid until the next call to next_line() or lr_free() with
// the same _lr_.
//
// next_line() returns NULL at end of file, or if there is an error (on
// the stream, or with memory allocation).
//************************************************************************************************
char* AssetLibrary::next_line(struct line_reader *lr, size_t *len)
{
	size_t newsiz;
	int c;
	char *newbuf;

	*len = 0;			/* Start with empty line. */
	for (;;) {
		c = fgetc(lr->f);	/* Read next character. */
		if (ferror(lr->f))
			return NULL;

		if (c == EOF) {
			/*
			 * End of file is also end of last line,
		`	 * unless this last line would be empty.
			 */
			if (*len == 0)
				return NULL;
			else
				return lr->buf;
		} else {
			/* Append c to the buffer. */
			if (*len == lr->siz) {
				/* Need a bigger buffer! */
				newsiz = lr->siz + 4096;
				newbuf = (char *)realloc(lr->buf, newsiz);
				if (newbuf == NULL)
					return NULL;
				lr->buf = newbuf;
				lr->siz = newsiz;
			}
			lr->buf[(*len)++] = c;

			/* '\n' is end of line. */
			if (c == '\n')
				return lr->buf;
		}
	}
	return NULL;
}

//************************************************************************************************
// Frees memory used by _lr_; there is no "internal" memory if these are global funcs operating
// on passed in memory pointers
//************************************************************************************************
void AssetLibrary::lr_free(struct line_reader *lr)
{
	free(lr->buf);
	lr->buf = NULL;
	lr->siz = 0;
}

}

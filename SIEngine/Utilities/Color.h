#ifndef COLOR_H_
#define COLOR_H_

#include "SIEngine/Include/SIBase.h"

namespace SIUtils
{

class Color
{
public:
	Color()
	{
		r = 0;
		g = 0;
		b = 0;
		a = 0;
	}

	Color(float r1, float g1, float b1, float a1 )
	{ r = r1; g = g1; b = b1; a = a1; }

	Color(float r1, float g1, float b1 )
	{ r = r1; g = g1; b = b1; a = 1; }

	~Color() {}

	static Color Clear;
	static Color WhiteOpaque;
	static Color WhiteClear;
	static Color BlackOpaque;
	static Color BlackClear;
	static Color Red;
	static Color Green;
	static Color Blue;

	static Color fromHex( String hexCode );

	Color lerp(Color to, float time )
	{
		return Color(
				lerpFloat(r, to.r, time),
				lerpFloat(g, to.g, time),
				lerpFloat(b, to.b, time),
				lerpFloat(a, to.a, time) );
	}


	bool operator ==(Color& b)
	{ return Equals(b); }

	bool operator !=(Color& b)
	{ return !Equals(b); }

	bool Equals(Color& other)
	{ return (other.r == r && other.g == g && other.b == b && other.a == a ); }

	Color& operator=(Color rhs)
	{
		r= rhs.r; g= rhs.g; b = rhs.b; a = rhs.a;
		return *this;
	}

private:
	float lerpFloat(float from, float to, float time)
	{
		time = (time<0)?0:time;
		time = (time>1)?1:time;
		return  from * (1.0f - time) + to * time;
	}

public:
	float r;
	float g;
	float b;
	float a;
};

}

#endif /* COLOR_H_ */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/LFIncludes.h"

namespace SIUtils
{

/// <summary>
/// Identifies the 'category' of error as determined by the ErrorWidget
/// </summary>
enum ErrorID
{
	ErrorIDNone,
	ErrorIDController,
	ErrorIDCamera,
	ErrorIDNetwork,
	ErrorIDCartridge,
	ErrorIDSystem,
	ErrorIDContent,
	ErrorIDPetPlay
};

/// <summary>
/// Launch Type, Either Widgets, ContentItems, or paired state UI applications
/// </summary>
enum ExitReason
{
	ExitReasonNone,
	ExitReasonWidget,
	ExitReasonVideoBundle,
	ExitReasonErrorWidget,
	ExitReasonContentItem,
	ExitReasonCartridge,
	ExitReasonParentSettings,
	ExitReasonParentSettingsNewUser,
	ExitReasonChildScene,
	ExitReasonOutOfBox
};

/// <summary>
/// App ID
/// </summary>
enum AppID
{
	AppIDNone,
	AppIDOutOfBox,
	AppIDGlasgowUI,
	AppIDParentSettings,
	AppIDErrorWidget,
	AppIDVideoWidget,
	AppIDProfileWidget,
	AppIDFlappyFrog,
	AppIDExternal
};

/// <summary>
/// State of the requesting app when it launched the new app
/// </summary>
enum AppState
{
	AppStateContent,
	AppStateHub,
	AppStateCartridge,
	AppStateVideoBundle,
	AppStateSignIn,
	AppStateNewUser,
	AppStateParentLanding,
	AppStateNone
};

/// <summary>
/// Enum of system locales, perhaps move to Constants?
/// </summary>
typedef enum SystemLocales
{
	SystemLocaleDefault,
	SystemLocaleUnitedStates,
	SystemLocaleCanada,
	SystemLocaleIreland,
	SystemLocaleUnitedKingdom,
	SystemLocaleAustralia,
	SystemLocaleNewZealand,
	SystemLocaleMax
} SystemLocale;

/// <sumnmary>
/// Constants for static constants that should be used system wide
/// </sumnmary>
class Constants
{
public:

	/// <sumnmary>
	//// Locations of various files
	/// </sumnmary>
	class Paths
	{
	public:
		static String flagUiReady;
		static String flagUiLoaded;
		static String flagFirmwareInstalled;
		static String flagFirmwareUpdateReady;
		static String glasgowSettingsFile;
		static String guestProfileFile;
		static String firmwareVersion;
		static String macEthernetAddressFile;
		static String macWifiAddressFile;
		static String LFBulk;
		static String ethernetStatusFile;
	};

	/// <sumnmary>
	/// For names of keys used to set in files
	/// </sumnmary>
	class Parameters
	{
	public:
		static String keySneakPeeks;
		static String keyIsActivated;
		static String keyLastUser;
		static String keyLastTheme;
		static String keyTimeModifier;
		static String keyExitReason;
		static String keyExitAppState;
		static String keyData1;
		static String keyData2;
		static String keyLaunchFrom;
		static String keyLaunchTo;
		static String keyLaunchState;
		static String keyWristStrapComplete;
		static String keyLastGame;
	};

	/// <sumnmary>
	/// Timers and time based constants
	/// </sumnmary>
	class Time
	{
	public:
		static double kNanoSecondsInASecond;
		static float  registrationCodeTimeOut;
		static float  wifiInputTimer;
		static int 	  activationTimeOut;
	};

	/// <sumnmary>
	/// System level settings
	/// </sumnmary>
	class System
	{
	public:
		static String backDoorGateCode;
		static String backDoorUnlockCode;
		static String appName;
		static AppID appID;
		static int wifiPasswordMaximumCharLimit;
		static int userNameMaximumCharLimit;
		static int minBirthYear;
		static String genderMaleKey;
		static String genderFemaleKey;
		static int genderMaleEnum;
		static int genderFemaleEnum;
		static int maxNumUsers;
	};

	/// <sumnmary>
	/// UI constants
	/// </sumnmary>
	class UI
	{
	public:
		static int wifiPasswordDisplayLimit;
		static String guestProfileName;
		static String textTruncationEndPattern;
		static String encryptionDisplayPattern;
		static int wifiNetworkNameMaxLines;
		static int contentItemBannerTextMaxLines;
		static float addProfileButtonYOffset;
	};

	class Units
	{
	public:
		static double UnitGigaByte;
		static double UnitMegaByte;
		static double UnitKiloByte;
	};

	///<summary>
	/// Regular expressions, patterns
	///</summary>
	class Text
	{
	public:
		static int NPC_CarriageReturn;
		static int NPC_Space;
	};

	/// <sumnmary>
	/// Things relating to content, package and product ids
	/// </sumnmary>
	class Content
	{
	public:
		static long   petPlayWorldProductID;
	};

private:
	Constants();
	virtual ~Constants();
};

}

#endif

#include "Calender.h"

namespace SIUtils
{

String calMonths [] =  { "january", "february", "march", "april",
						"may", "june", "july", "august", "september",
						"october", "november", "december"
					  };


String calDays [] =  {	"01", "02", "03", "04", "05", "06", "07",
						"08", "09", "10", "11", "12", "13", "14",
						"15", "16", "17", "18", "19", "20", "21",
						"22", "23", "24", "25", "26", "27", "28",
						"29", "30", "31"
					};

Calender::Calender()
{}

Calender::~Calender()
{}


String Calender::dayStrForInt(int day)
{
	if(day > 31)
		day = 31;

	return calDays[day-1];
}

String Calender::formattedDayStrForInt(int day)
{
	String sDay = dayStrForInt(day);
	if(sDay.length() > 1 && sDay.indexOf("0") == 0)
	{
		sDay = sDay.substring(1);
	}

	return sDay;
}

String Calender::formattedDayStr(String day)
{
	if(day.length() > 1 && day.indexOf("0") == 0)
	{
		day = day.substring(1);
	}

	return day;
}

String Calender::monthStrForInt(int month)
{
	if(month > 12)
		month = 12;

	return calMonths[month-1];
}

String Calender::formattedMonthStrForInt(int month)
{
	String sMonth = monthStrForInt(month);
	sMonth = sMonth.toUpperCase();
	String localizationKey = String("PROFILE_CALENDER_MONTH_" + sMonth);
	String localized = StringsManager::getInstance()->getLocalizedString(localizationKey);
	return String(localized.substring(0,1).toUpperCase() + localized.substring(1).toLowerCase());
}

String Calender::formattedMonthStr(String month)
{
	month = month.toUpperCase();
	String localizationKey = String("PROFILE_CALENDER_MONTH_" + month);
	String localized = StringsManager::getInstance()->getLocalizedString(localizationKey);
	return String(localized.substring(0,1).toUpperCase() + localized.substring(1).toLowerCase());
}

/// <summary>
/// Helper to get days in a given month,year
/// </summary>
int Calender::daysInMonthYear(String month,String year)
{
	int m = getMonthIntRepresentation(month);
	switch(m)
	{
		case 1:case 3:case 5:case 7:case 8:case 10:case 12:
		{	return 31;}
		case 4:case 6:case 9:case 11:
		{	return 30;}
		case 2:
		{	return isLeapYear(year) ? 29 : 28;}
		default:break;
	}

	return 31;
}

/// <summary>
/// Helper to get month int value
/// </summary>
int Calender::getMonthIntRepresentation(String month)
{
	for( int i = 0; i<12; i++ )
	{
		if(month.equals(calMonths[i]))
			return i+1;
	}

	return 1;
}

/// <summary>
/// Helper to get day int value
/// </summary>
int Calender::getDayIntRepresentation(String day)
{
	for(int i= 0; i < 31; ++i)
	{
		if(day.equals(calDays[i]))
		{
			return i+1;
		}
	}

	return 0;
}

/// <summary>
/// Helper to get leapyear for a given year
/// </summary>
bool Calender::isLeapYear(String yearString)
{
	int year = yearString.toInt();

	if ( year > 1582 )
	{ return ((year % 4 ==0 && year % 100 != 0) || (year%400==0)); }
    else if ( year > 10 )
    { return (year % 4 == 0 ); }
    else
    {
    	switch (year)
    	{
			case -45:
				return true;
			case -42:
				return true;
			case -39:
				return true;
			case -36:
				return true;
			case -33:
				return true;
			case -30:
				return true;
			case -27:
				return true;
			case -24:
				return true;
			case -21:
				return true;
			case -18:
				return true;
			case -15:
				return true;
			case -12:
				return true;
			case -9:
				return true;
			default:
				return false;
    	}
    }

	return false;
}

bool Calender::isValidBirthYear(String year)
{
	time_t t = time(NULL);
	tm* timePtr = localtime(&t);
	int years = 1900 + timePtr->tm_year;

	int age = years - year.toInt();
	if ( age >= 0 && age < 125 )
	{
		return true;
	}

	return false;
}

} /* namespace Glasgow */

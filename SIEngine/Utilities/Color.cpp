#include "Color.h"

namespace SIUtils
{

Color Color::Red = Color( 1,0,0,1);
Color Color::Green = Color(0,1,0,1);
Color Color::Blue = Color(0,0,1,1);
Color Color::Clear = Color( 1,1,1,0);
Color Color::WhiteOpaque = Color(1,1,1,1);
Color Color::WhiteClear = Color(1,1,1,0);
Color Color::BlackOpaque = Color(0,0,0,1);
Color Color::BlackClear = Color(0,0,0,0);


Color Color::fromHex( String hexCode)
{
	Color color = Color::WhiteOpaque;


	//0xAARRGGBB
	if( !hexCode.startsWith("0x") )
	{
		//RRGGBB
		if( hexCode.length() == 6 )
		{
			int r, g, b;
			if( 3 == sscanf(hexCode.str(), "%2x%2x%2x", &r, &g, &b) )
			{
				color.r = ((float)r/255.0f);
				color.g = ((float)g/255.0f);
				color.b = ((float)r/255.0f);
				printf("Color::fromHex R[%f : %d] G[%f : %d] B[%f : %d]\n", color.r, r, color.g, g, color.b, b);
				return color;
			}
		}
	}

	return color;
}

}



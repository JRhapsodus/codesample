#ifndef __HASHTABLE_H_
#define __HASHTABLE_H_

#include "SIEngine/Include/SIBase.h"

namespace SICore
{ class GameObject; }

namespace SICollections
{

class IEnumeration
{
public:
	IEnumeration() { }
	virtual ~IEnumeration() { }
	virtual bool hasMoreElements() = 0;
	virtual BaseObject* nextElement() = 0;
};

/// <summary> 
/// Hash table enumeration
/// </summary> 
class HTEnum : public BaseObject, public IEnumeration
{
	CLASSEXTENDS(HTEnum, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	int		 m_nNumElements;
	int		 m_nIndex;
	BaseObject** m_objs;

	HTEnum(int nItems);
	~HTEnum();

	virtual bool hasMoreElements();
	virtual BaseObject* nextElement();
};

/// <summary> 
/// hash table for usage:   map["someitem"] = baseObject;
/// </summary> 
class HashTable : public BaseObject
{
	CLASSEXTENDS(HashTable,BaseObject);
	ADD_TO_CLASS_MAP;
protected:
	class HashProxy : public BaseObject
	{
		CLASSEXTENDS(HashProxy, BaseObject);
		ADD_TO_CLASS_MAP;

	        HashTable* h;
	        BaseObject* key;
	        String stringKey;

	public:
	        ~HashProxy()
	        { SAFE_RELEASE(key); }

	        HashProxy(HashTable *ht, BaseObject* k)
	    	{
	        	h = ht;
	        	key = k;
	        	stringKey = "";
	    	}

	        HashProxy(HashTable *ht, String k)
	        {
	        	h = ht;
	        	key = null;
	        	stringKey = k;
	        }

	        operator BaseObject() const
	        {
	        	if( key == NULL )
	        	{  return *h->get(stringKey); }

	        	return *h->get(key);
	        }

	        operator BaseObject*()
	        {
	        	if( key == NULL )
	        	{  return h->get(stringKey); }

	            return h->get(key);
	        }

	        operator GameObject*()
	        {
	        	if( key == NULL )
	        	{  return (GameObject*)h->get(stringKey); }

	            return (GameObject*)h->get(key);
	        }

	        operator String*()
	        {
	        	if( key == NULL )
	        	{  return (String*)h->get(stringKey); }

	            return (String*)h->get(key);
	        }

	        HashProxy &operator=(LPCSTR value)
	        {
	        	String* s = new String(value); //ref 1
	        	if( key == NULL )
	        	{ h->put(stringKey, s); } //ref 2
	        	else
	        	{ h->put(key, s); } //ref 2
	        	s->release();//ref 1
	            return *this;
	        }

	        HashProxy &operator=(String* value)
	        {
	        	if( key == NULL )
	        	{ h->put(stringKey, value); }
	        	else
	        	{ h->put(key, value); }
	            return *this;
	        }

	        HashProxy &operator=(String& value)
	        {
	        	if( key == NULL )
	        	{
	        		String* sVal = new String(value); //ref 1
	        		h->put(stringKey, sVal); //ref 2
	        		sVal->release();//ref 1
	        	}
	        	else
	        	{
	        		String* sVal = new String(value); //ref 1
	        		h->put(key, sVal); //ref 2
	        		sVal->release(); //ref 1

	        	}
	            return *this;
	        }

	        HashProxy &operator=(BaseObject& value)
	        {
	        	if( key == NULL )
	        	{ h->put(stringKey, &value); }
	        	else
	        	{ h->put(key, &value); }
	            return *this;
	        }

	        HashProxy &operator=(BaseObject* value)
	        {
	        	if( key == NULL )
	        	{ h->put(stringKey, value); }
	        	else { h->put(key, value); }
	            return *this;
	        }
	    };

	/// <summary> 
	/// Linked list of items.
	/// </summary>
	class HashItem
	{
	public:
		BaseObject*		m_key;
		BaseObject*		m_value;
		HashItem*		m_nextItem;
	};

	/// <summary> 
	/// Size of our hash table
	/// </summary> 
	int			m_nTableSize;

	/// <summary> 
	/// Pointer to first HashItem at each index
	/// </summary> 
	HashItem**	m_pTable;

	/// <summary> 
	/// Total number of items in our table
	/// </summary> 
	int			m_nItems;

	/// <summary> 
	/// Rehash
	/// </summary> 
	void rehash();

private:

	/// <summary> 
	/// Deletes all memory used for this item.
	/// </summary> 
	HashItem* remove(HashItem *);

public:
	HashTable();
	~HashTable();

	/// <summary> 
	/// Dictionary interface.
	/// </summary> 
	HTEnum*			elements();
	HTEnum*			keys();

	BaseObject* get(const BaseObject* key);
	BaseObject* get(const String& key);
	bool isEmpty();

	void	put(BaseObject* key, BaseObject* value);
	void	put(const String& key, BaseObject* value);
	void	put(LPCTSTR lpszKey, BaseObject* value);

	/// <summary> 
	///released pointer to value key pointed to. This may be an
	/// invalid pointer unless the program has taken aims
	/// to keep the reference count up.
	/// </summary> 
	BaseObject* remove( const BaseObject* key );
	BaseObject* remove( const BaseObject& key);

	//// <summary>
	//// Remove the given value from the hash table.
	//// </summary>
	bool removeValue( const BaseObject* value);
	inline int size() { return m_nItems; }

    HashProxy operator [] ( BaseObject* key)
    { return HashProxy(this, key); }

    HashProxy operator [] ( int key )
    {
    	String* pKey = new String(key);
    	return HashProxy(this, pKey);
    }

    HashProxy operator [] ( String key )
    {
    	String* pKey = new String(key);
    	return HashProxy(this, pKey);
    }

    HashProxy operator [] ( LPCSTR key )
    {
    	String* pKey = new String(key);
    	return HashProxy(this, pKey);
    }

    /// <summary> 
	/// Hashtable interface
	/// </summary> 
	void clear();
	BaseObject* clone();
	bool contains( const BaseObject* value);
	bool containsKey( const BaseObject* key);
	bool containsKey(const String& key);

};

}
#endif

#include "Hashtable.h"
#include "SIEngine/Core/Lock.h"
namespace SICollections
{


HTEnum::HTEnum(int nItems) : super()
{
	m_nNumElements = nItems;
	m_nIndex = 0;
	m_objs = new BaseObject*[nItems];
}
HTEnum::~HTEnum()
{
	for( int i=0; i < m_nNumElements; i++ )
	{ SAFE_RELEASE(m_objs[i]); }
	delete [] m_objs;
}

bool HTEnum::hasMoreElements()
{
	return m_nIndex < m_nNumElements;
}

BaseObject* HTEnum::nextElement()
{
	return m_objs[m_nIndex++];
}


void HashTable::rehash()
{
	SYNCHDESTROY();
	HashItem** pOldTable = m_pTable;
	int nOldSize = m_nTableSize;
	int nOldItemCount = m_nItems;

	m_nTableSize=m_nItems*4;
	m_pTable = new HashItem*[m_nTableSize];
	for( int i=0; i < m_nTableSize; i++ )
	{
		m_pTable[i] = null;
	}

	//****************************************************
	// go through all items in the old table and add them
	//****************************************************
	int index;
	int index2;
	HashItem* pItem;
	HashItem* pItemNext;
	for( index=0; index < nOldSize; index++ )
	{
		pItem = pOldTable[index];

		while (pItem != null)
		{
			pItemNext = pItem->m_nextItem;

			//****************************************************
			// Move this item to a new location.
			//****************************************************
			index2 = pItem->m_key->hashValue() % m_nTableSize;
			if (index2 < 0) index2 *= -1; // must be positive!
			pItem->m_nextItem = m_pTable[index2];
			m_pTable[index2] = pItem;

			pItem = pItemNext;
		}
	}

	m_nItems = nOldItemCount;
	delete [] pOldTable;

}

HashTable::HashItem* HashTable::remove(HashItem *pItem)
{
	HashItem* pNext = pItem->m_nextItem;
	SAFE_RELEASE(pItem->m_key);
	SAFE_RELEASE(pItem->m_value);
	delete pItem;
	return pNext;
}

HashTable::HashTable()
{
	m_nItems = 0;
	m_nTableSize=50;
	m_pTable = new HashItem*[50];
	for( int i=0; i < 50; i++ )
	{ m_pTable[i] = null; }
}

HashTable::~HashTable()
{
	clear();
	delete [] m_pTable;
}

HTEnum*	HashTable::elements()
{
	SYNCHDESTROY();
	HTEnum* enumlist = new HTEnum( this->m_nItems );

	int index;
	int i=0;
	HashItem* pItem;
	for( index=0; index < m_nTableSize; index++ )
	{
		pItem = m_pTable[index];

		while (pItem != null)
		{
			enumlist->m_objs[i++] = pItem->m_value;
			pItem->m_value->addRef();
			pItem = pItem->m_nextItem;
		}
	}

	return enumlist;
}

BaseObject* HashTable::get( const BaseObject* key)
{
	SYNCHDESTROY();
	int index = key->hashValue() % m_nTableSize;
	if (index < 0) index *= -1; // must be positive!
	HashItem* pItem;
	pItem = m_pTable[index];

	while (pItem != null)
	{
		if (key->equals(*(pItem->m_key)))
			return pItem->m_value;
		pItem = pItem->m_nextItem;
	}

	return null;
}

BaseObject* HashTable::get(const String& key)
{
	SYNCHDESTROY();
	int index = key.hashValue() % m_nTableSize;
	if (index < 0) index *= -1; // must be positive!
	HashItem* pItem;
	pItem = m_pTable[index];

	while (pItem != null)
	{
		if (key.equals(*(pItem->m_key)))
			return pItem->m_value;
		pItem = pItem->m_nextItem;
	}

	return null;
}

bool HashTable::isEmpty()
{ return m_nItems == 0; }

HTEnum* HashTable::keys()
{
	SYNCHDESTROY();
	HTEnum* enumlist = new HTEnum( this->m_nItems );

	int index;
	int i=0;
	HashItem* pItem;
	for( index=0; index < m_nTableSize; index++ )
	{
		pItem = m_pTable[index];

		while (pItem != null)
		{
			enumlist->m_objs[i++] = pItem->m_key;
			pItem->m_key->addRef();
			pItem = pItem->m_nextItem;
		}
	}


	return enumlist;
}

void HashTable::put(BaseObject* key, BaseObject* value)
{
	SYNCHDESTROY();
	int index = key->hashValue() % m_nTableSize;
	if (index < 0) index *= -1; // must be positive!
	HashItem* pItem = new HashItem();

	pItem->m_key  = key;
	key->addRef();
	pItem->m_value = value;
	value->addRef();
	pItem->m_nextItem = m_pTable[index];
	m_pTable[index] = pItem;
	m_nItems++;

	if (m_nItems*2 > m_nTableSize)
	{ rehash(); }
}

void HashTable::put(const String& key, BaseObject* value)
{
	String* pStr = new String(key); //ref 1
	put( pStr, value ); //ref 2
	pStr->release(); //ref 1
}

void HashTable::put( LPCTSTR key, BaseObject* value)
{
	String* pStr = new String(key);//ref 1
	put( pStr, value );//ref 2
	pStr->release();//ref 1
}

BaseObject* HashTable::remove( const BaseObject& key)
{
	return remove( &key );
}

BaseObject* HashTable::remove( const BaseObject* key)
{
	SYNCHDESTROY();

	int index = key->hashValue() % m_nTableSize;
	if (index < 0) index *= -1; // must be positive!
	HashItem* pItem;
	BaseObject* pValue;
	HashItem* pPrev = null;
	pItem = m_pTable[index];

	while (pItem != null)
	{
		if (key->equals(*(pItem->m_key)))
		{
			pValue = pItem->m_value;
			pItem  = remove(pItem);
			if (pPrev == null)
				m_pTable[index] = pItem;
			else
				pPrev->m_nextItem = pItem;
			m_nItems--;
			return pValue;
		}
		pPrev = pItem;
		pItem = pItem->m_nextItem;
	}

	return null;
}

void HashTable::clear()
{
	SYNCHDESTROY();

//	printf("HashTable::clear size:%d\n", m_nTableSize);
	int index;
	HashItem* pItem;
	for( index=0; index < m_nTableSize; index++ )
	{
		pItem = m_pTable[index];
		while (pItem != null)
		{
			pItem = remove(pItem);
		}
		m_pTable[index] = null;
	}
	m_nItems = 0;
//	printf("HashTable::clear done\n");
}

BaseObject* HashTable::clone()
{
	// not yet supported
	return null;
}

bool HashTable::removeValue( const BaseObject* value)
{
	SYNCHDESTROY();

	int index;
	HashItem* pItem;
	HashItem* pPrev=null;
	for( index=0; index < m_nTableSize; index++ )
	{
		pItem = m_pTable[index];
		pPrev = null;

		while (pItem != null)
		{
			if (value->equals(*(pItem->m_value)))
			{
				pItem  = remove(pItem);
				if (pPrev == null)
					m_pTable[index] = pItem;
				else
					pPrev->m_nextItem = pItem;
				m_nItems--;
				return true;
			}
			pPrev = pItem;
			pItem = pItem->m_nextItem;
		}
	}

	return false;
}

bool HashTable::contains( const BaseObject* value)
{
	SYNCHDESTROY();

	int index;
	HashItem* pItem;
	for( index=0; index < m_nTableSize; index++ )
	{
		pItem = m_pTable[index];

		while (pItem != null)
		{
			if (value->equals(*(pItem->m_value)))
				return true;
			pItem = pItem->m_nextItem;
		}
	}

	return false;
}

bool HashTable::containsKey( const BaseObject* key)
{
	SYNCHDESTROY();

	int index = key->hashValue() % m_nTableSize;
	if (index < 0) index *= -1; // must be positive!
	HashItem* pItem;
	pItem = m_pTable[index];

	while (pItem != null)
	{
		if (key->equals(*(pItem->m_key)))
		{ return true; }

		pItem = pItem->m_nextItem;
	}
	return false;
}

bool HashTable::containsKey(const String& key)
{
	SYNCHDESTROY();

	int index = key.hashValue() % m_nTableSize;
	if (index < 0) index *= -1; // must be positive!
	HashItem* pItem;
	pItem = m_pTable[index];

	while (pItem != null)
	{
		if (key.equals(*(pItem->m_key)))
		{ return true; }

		pItem = pItem->m_nextItem;
	}
	return false;
}

}

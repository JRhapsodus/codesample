/// <summary> 
///   Container Class for all objects in the framework, 
/// this is implemented as a dynamically sized array
/// of pointers
///
///	 Jeston Furqueron
/// 
/// </summary> 
#ifndef __ObjectArray__H_
#define __ObjectArray__H_

#include "SIEngine/Objects/BaseObject.h"
using namespace SICore;

namespace SICollections
{

/// <summary> 
/// An array of base objects ( strong referenced )
/// </summary> 
class ObjectArray : public BaseObject
{
	CLASSEXTENDS(ObjectArray, BaseObject);
	ADD_TO_CLASS_MAP;
private:
	/// <summary> 
	/// Number of elements in the array
	/// </summary> 
	int		m_nElements;

	/// <summary> 
	/// Number of elements possible in the array
	/// </summary> 
	int     m_nCapacity;     

	/// <summary>
	/// First element
	/// </summary> 
	int     m_nFirstElement;  

	/// <summary> 
	///  Could have been LPVOID's but it was best to use
	/// a basic cloning / ref counting class to ensure
	/// pointer validation
	/// </summary> 
	BaseObject  **m_Objects;
	
public:
	/// <summary> 
	/// Constructor
	/// </summary> 
	ObjectArray()
	{
		m_nCapacity		= 50;
		m_nElements		= 0;
		m_nFirstElement = 0;
		m_Objects		= new BaseObject*[m_nCapacity];
	}

	ObjectArray(int size)
	{
		m_nCapacity		= size;
		m_nElements		= 0;
		m_nFirstElement = 0;
		m_Objects		= new BaseObject*[m_nCapacity];
	}

	/// <summary> 
	/// To array
	/// </summary> 
	BaseObject** toArray()
	{ return m_Objects; }


	/// <summary> 
	/// Runs quick sort
	/// </summary> 
	void quickSort( CompareFunction compare);

	/// <summary> 
	/// Destructor
	/// </summary> 
	~ObjectArray();

	/// <summary> 
	/// Match array indexing notation
	/// </summary> 
	BaseObject* operator[]( int index )
	{ return elementAt(index); }

	/// <summary> 
	/// Returns the cell at the given location
	/// </summary> 
	virtual BaseObject*	lastElement()
	{
		if( m_nElements == 0 )
		{ return NULL; }

		return elementAt( m_nElements-1);
	}

	/// <summary> 
	/// Returns the cell at the given location
	/// </summary> 
	BaseObject*	elementAt( int nLoc )
	{
		if ( nLoc < 0 || nLoc >= m_nElements)
		{
			return NULL;
		}

		return m_Objects[ m_nFirstElement+nLoc ];
	}

	/// <summary> 
	/// Returns the number of elements in the array
	/// </summary> 
	inline int getSize()
	{
		return m_nElements;
	}

	/// <summary> 
	/// Increases the size of the array
	/// </summary> 
	void	grow( int nBy = 1 );

	/// <summary> 
	/// Sets the number of elements
	/// </summary> 
	void	setSize( int nSize );

	/// <summary> 
	/// Creates another copy with the same elements
	/// </summary> 
	virtual BaseObject* clone();

	/// <summary> 
	/// Push back function of the vector
	/// </summary> 
	void    addElement( BaseObject* obj );

	/// <summary> 
	/// Add all elements from other array
	/// </summary> 
	void    addElementsFromArray( ObjectArray* array );

	/// <summary> 
	/// Remove elements from other array
	/// </summary> 
	void removeElementsFromArray( ObjectArray* array );

	/// <summary> 
	/// Removes an element at a given location
	/// </summary> 
	void	removeElementAt( int nLocation );

	/// <summary> 
	/// Removes the given BaseObject
	/// </summary> 
	bool	removeElement( BaseObject& pItem );

	/// <summary> 
	/// Conversion from BaseObject* to BaseObject&
	/// </summary> 
	bool removeElement( BaseObject* pItem )
	{ return removeElement( *pItem ); }

	/// <summary> 
	/// Clears the vector
	/// </summary> 
	void	removeAllElements();

	/// <summary> 
	/// Finds a BaseObject by pointer
	/// </summary> 
	int		indexOf( BaseObject& obj );

	/// <summary> 
	/// Inserts a BaseObject at a given location
	/// </summary> 
	void	insertElementAt( BaseObject* obj, int nLocation );

	/// <summary> 
	/// Replaces an element at the location
	/// </summary> 
	void	setElementAt( BaseObject* obj, int nLocation );

};

/// <summary> 
/// Hard typed template object array Array<CameraObject>()
/// </summary> 
template <class T> class TArray	: public ObjectArray
{
public:
	inline T* operator[]( int index )
	{ return (T*)(elementAt(index)); }

	TArray<T>& operator=( const TArray<T>& rhs )
	{
		for( int i = 0; i<m_nElements; i++)
		{ m_Objects[i]->addRef(); }

		return *this;
	}

	virtual T* lastElement()
	{ return (T*)(ObjectArray::lastElement()); }

};

}
#endif

#ifndef ICLONEABLE_H_
#define ICLONEABLE_H_

namespace SICollections
{


class ICloneable
{
public:
	virtual ~ICloneable() {}
	virtual ICloneable* clone() = 0;
};

}

#endif /* ICLONEABLE_H_ */

//*******************************************************
//   Container Class for objects in the framework, 
// this is implemented as a dynamically sized array
// of pointers
//
//	 Jeston Furqueron
//*******************************************************
#include "SIEngine/Collections/ObjectArray.h"
#include <stdlib.h>
#include <algorithm>
#include "SIEngine/Core/Lock.h"

//*******************************************************
// Destructor, releases all elements because vector
// creates a reference to each object
//*******************************************************
namespace SICollections
{

ObjectArray::~ObjectArray()
{
	removeAllElements();
	delete [] m_Objects;
}

//*******************************************************
// QUick sort
//*******************************************************
void ObjectArray::quickSort( CompareFunction compare)
{
	std::qsort( m_Objects, m_nElements, sizeof(BaseObject*), compare);
}

//*******************************************************
// Resizes the vector
//*******************************************************
void ObjectArray::grow( int nBy )
{
	BaseObject** objs;
	int nNewCapacity = 0;
	int i = 0;

	nNewCapacity = (m_nCapacity*3)/2;

	if (nNewCapacity - 	m_nCapacity < nBy)
	{
		nNewCapacity = m_nCapacity + nBy;
	}

	objs = new BaseObject*[ nNewCapacity ];

	for( i=0; i < m_nElements; i++ )
	{
		objs[i] = m_Objects[m_nFirstElement+i];
	}
	
	//*******************************************************
	// must NULL unused entries otherwise setElementAt will 
	// try to release them.
	//*******************************************************
	for( ; i < nNewCapacity; i++ )
	{
		objs[i] = NULL;
	}
	delete [] m_Objects;

	m_nFirstElement = 0;
	m_nCapacity		= nNewCapacity;
	m_Objects       = objs;
}

//******************************************************
// Remove all elements from other array
//******************************************************
void ObjectArray::removeElementsFromArray( ObjectArray* array )
{
	for( int i = 0; i<array->getSize(); i++ )
	{
		removeElement( array->elementAt(i));
	}
}

//******************************************************
// Add all elements from other array
//******************************************************
void ObjectArray::addElementsFromArray( ObjectArray* array )
{
	SYNCHRONIZED();
	for( int i = 0; i<array->getSize(); i++ )
	{
		addElement( array->elementAt(i));
	}
}

//******************************************************
// Push back function of the vector
//******************************************************
void    ObjectArray::addElement( BaseObject* obj )
{
	SYNCHRONIZED();
	if ( m_nElements + 1 > m_nCapacity )
	{ grow(); }

	if ( m_nFirstElement+m_nElements+1 > m_nCapacity )
	{
		for( int i=0; i < m_nElements; i++ )
		{
			m_Objects[i] = m_Objects[	m_nFirstElement+i ];
		}
		m_nFirstElement = 0;
	}

	m_Objects[m_nFirstElement+m_nElements] = obj;
	if (obj != NULL)
	{
		obj->addRef();
	}

	m_nElements++;
}

//******************************************************
// Removes an element at a given location
//******************************************************
void ObjectArray::removeElementAt( int index )
{
	SYNCHRONIZED();
	if ( index < 0 || index >= m_nElements )
	{
		return;
	}

	if ( m_Objects[m_nFirstElement+index] != NULL )
	{
		m_Objects[m_nFirstElement+index]->release();
	}

	m_nElements--;

	if (index == 0)
	{
		m_Objects[m_nFirstElement] = NULL;
		m_nFirstElement++;
		return;
	}

	//******************************************************
	// Check which is the optimal way to shift
	//******************************************************
	if (m_nElements-index <= index)
	{
		//******************************************************
		// Shift elements after index
		//******************************************************
		for ( int i=index; i<m_nElements; i++ )
		{
			m_Objects[m_nFirstElement+i] = m_Objects[m_nFirstElement+i+1];
		}
		m_Objects[m_nFirstElement+m_nElements] = NULL;
	}
	else
	{
		//******************************************************
		// Shift elements before index
		//******************************************************
		for ( int i=index; i>0; i-- )
		{
			m_Objects[m_nFirstElement+i] = m_Objects[m_nFirstElement+i-1];
		}
		m_Objects[m_nFirstElement] = NULL;
		m_nFirstElement++;
	}
}


//******************************************************
// Inserts a cell at a given location
//******************************************************
void ObjectArray::insertElementAt( BaseObject* obj, int index )
{
	SYNCHRONIZED();

	if (index < 0 || index > m_nElements)
	{ return; }

	if (obj != NULL)
	{ obj->addRef(); }
	if (m_nElements == m_nCapacity)
	{ grow(); }

	//******************************************************
	// Check which is the optimal way to shift
	//******************************************************
	if ( (m_nElements-index <= index || m_nFirstElement==0) && 
		(m_nFirstElement+m_nElements < m_nCapacity) )
	{
		//******************************************************
		// Shift elements after index
		//******************************************************
		for ( int i=m_nElements; i>index; i-- )
		{
			m_Objects[m_nFirstElement+i] = m_Objects[m_nFirstElement+i-1];
		}
	}
	else
	{
		m_nFirstElement--;

		//******************************************************
		// Shift elements before index
		//******************************************************
		for ( int i=0; i<index; i++ )
		{
			m_Objects[m_nFirstElement+i] = m_Objects[m_nFirstElement+i+1];
		}
	}

	m_Objects[m_nFirstElement+index] = obj;
	m_nElements++;
}

//******************************************************
// Replaces an element at the location
//******************************************************
void ObjectArray::setElementAt( BaseObject* obj, int nIndex )
{
	SYNCHRONIZED();

	if (nIndex < 0)
	{ return; }

	if (nIndex >= m_nCapacity)
	{ grow( nIndex-m_nCapacity+1 ); }

	if ( m_nFirstElement+nIndex >= m_nCapacity )
	{
		//******************************************************
		// Shift to beginning
		//******************************************************
		for( int i=0; i < m_nElements; i++ )
		{
			m_Objects[i] = m_Objects[	m_nFirstElement+i ];
		}
		m_nFirstElement = 0;
	}

	if (obj != NULL)
	{
		obj->addRef();
	}
	if ( m_Objects[m_nFirstElement+nIndex]!=NULL )
	{
		m_Objects[m_nFirstElement+nIndex]->release();
	}
	m_Objects[m_nFirstElement+nIndex] = obj;

	//******************************************************
	// Update the number of elements
	//******************************************************
	if (nIndex >= m_nElements)
	{
		m_nElements = nIndex+1;
	}
}

//******************************************************
// Removes a given cell pointer
//******************************************************
bool ObjectArray::removeElement( BaseObject& pItem )
{
	SYNCHRONIZED();
	int index = indexOf( pItem );
	if (index != -1)
	{
		removeElementAt( index );
		return true;
	}
	return false;
}

//******************************************************
// Removes all elements
//******************************************************
void ObjectArray::removeAllElements()
{
	SYNCHRONIZED();

	for( int i=0; i < m_nElements; i++ )
	{ SAFE_RELEASE(m_Objects[m_nFirstElement+i]); }

	m_nElements=0;
	m_nFirstElement=0;
}

//******************************************************
// Uses the objects equals method to locate
//******************************************************
int	ObjectArray::indexOf( BaseObject& obj )
{
	SYNCHRONIZED();

	for( int i=0; i < m_nElements; i++ )
	{
		if (m_Objects[m_nFirstElement+i] != NULL && 
			m_Objects[m_nFirstElement+i]->equals( obj ))
		{
			return i;
		}
	}
	return -1;
}

//******************************************************
// Sets the number of elements
//******************************************************
void ObjectArray::setSize( int nSize )
{
	SYNCHRONIZED();

	if (m_nElements > nSize)
	{
		for( int i=nSize; i < m_nElements; i++ )
		{
			if ( m_Objects[m_nFirstElement+i] != NULL )
			{
				m_Objects[m_nFirstElement+i]->release();
			}
		}

		m_nElements = nSize;
	}
	else if ( nSize > m_nCapacity )
	{
		grow( nSize-m_nCapacity );
	}
}

//******************************************************
// Creates another copy with the same elements
//******************************************************
BaseObject* ObjectArray::clone()
{
	SYNCHRONIZED();

	ObjectArray* v = new ObjectArray(getSize());

	for( int i=0; i < getSize(); i++ )
	{ v->addElement(m_Objects[m_nFirstElement+i]); }

	return v;
}

}

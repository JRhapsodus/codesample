#include "SIEngine/Collections/ObjectCollection.h"
#include "SIEngine/Objects/GameObject.h"
#include "SIEngine/Core/Exception.h"

namespace SICollections
{

//************************************************************************
// Life cycle
//************************************************************************
ObjectCollection::ObjectCollection() : super()
{
	capacity = 10;
	parent = NULL;
	children = new ObjectArray(15);
}


ObjectCollection::~ObjectCollection()
{
	children->removeAllElements();
	SAFE_RELEASE(children);
}

String ObjectCollection::toString()
{
	return String("[Count ") + numChildren() + "]";
}

//************************************************************************
// Base class notifications
//************************************************************************
void ObjectCollection::onChildRemoved(GameObject* child)
{
	//base implementation: nothing
}

void ObjectCollection::onParentChanged(GameObject* oldParent, GameObject* newParent)
{

	//base implementation: nothing
}

void ObjectCollection::onChildAdded(GameObject* child)
{
	//base implementation: nothing
}

//************************************************************************
// Adopt Child calculates the new transform in the perspective of
// a new parent but keeps the same world location
//************************************************************************
void ObjectCollection::adoptChild( GameObject* child )
{
	//************************************************************************
	// In case removeFromParent was owner we are taking temporary control
	//************************************************************************
	child->addRef();
	Matrix childToWorld = child->getParent()->transform.getLocalToWorld();
	Matrix worldToParent = ((GameObject*)this)->transform.getWorldToLocal();
	Matrix childToParent = childToWorld * worldToParent;

	//remove from old
	child->removeFromParent();
	//add to us
	addChild(child);

	Vec3f oldPos = child->getPosition();
	Vec3f newPos = childToParent.multiplyPoint( oldPos );

	child->setPosition( newPos );
	child->setScale( childToParent * child->getScale() );
	child->release();
}



//************************************************************************
// Collection implementation
//************************************************************************
ObjectArray* ObjectCollection::getChildren()
{
	return children;
}

//************************************************************************
// Collection implementation
//************************************************************************
ObjectArray* ObjectCollection::toArray( bool subGraph )
{
	ObjectArray* sceneGraphFlat = new ObjectArray();
	ObjectArray* nodeStack = new ObjectArray();

	nodeStack->addElement( (GameObject*)this );

	while( nodeStack->getSize() >  0 )
	{

		GameObject* root = (GameObject*)nodeStack->elementAt(0);
		for( int i = 0; i<root->numChildren();i++ )
		{
			GameObject* child = (GameObject*)root->getChildren()->elementAt(i);
			if( subGraph )
			{
				nodeStack->addElement( child );
			}
			sceneGraphFlat->addElement( child );
		}

		nodeStack->removeElementAt(0);
	}

	return sceneGraphFlat;
}

//************************************************************************
// Collection implementation
//************************************************************************
ObjectArray* ObjectCollection::toPointerArray( bool subGraph )
{
	ObjectArray* sceneGraphFlat = new ObjectArray();
	ObjectArray* nodeStack = new ObjectArray();
	nodeStack->addElement( (GameObject*)this );

	while( nodeStack->getSize() >  0 )
	{
		GameObject* root = (GameObject*)nodeStack->elementAt(0);
		for( int i = 0; i<root->numChildren();i++ )
		{
			GameObject* child = (GameObject*)root->getChildren()->elementAt(i);
			if( subGraph )
			{ nodeStack->addElement( child ); }

			BaseObjectPointer* bop = new BaseObjectPointer( child);
			sceneGraphFlat->addElement( bop );
			bop->release();
		}

		nodeStack->removeElementAt(0);
	}
	nodeStack->release();
	return sceneGraphFlat;
}

//************************************************************************
// Remove child
//************************************************************************
void ObjectCollection::removeChild(GameObject* obj)
{
	if( obj->parent == this )
	{
		onChildRemoved(obj);
		children->removeElement(obj);
		nameToIndex.remove(obj->name);
	}
}

//************************************************************************
// Convenience call to eliminate cast in syntax
//************************************************************************
GameObject* ObjectCollection::getParent()
{ return (GameObject*)parent; }

//************************************************************************
// For Convenience, often times we want this to own the object when
// adding it to the scene graph
//************************************************************************
void ObjectCollection::addChildAndRelease(GameObject* obj)
{
	addChild(obj);
	obj->release();
}

//************************************************************************
// Add child
//************************************************************************
void ObjectCollection::addChild(GameObject* obj)
{
	if( obj->getParent() != NULL )
		ThrowException::sceneGraphException( obj->name );

	onChildAdded( obj );
	obj->onParentChanged( (GameObject*)obj->parent, (GameObject*)this);

	if( obj->parent != NULL )
	{
		//Possible throw exception? or Assert
		obj->removeFromParent();
		ThrowException::sceneGraphException( obj->name );
	}

	//addElement does the addRef
	children->addElement(obj);
	obj->parent = this;

	if ( obj->name.isEmpty())
	{
		ThrowException::sceneGraphException("NO NAME adding TO " + ((GameObject*)this)->name);
	}

	nameToIndex[obj->name] = obj;
}

//************************************************************************
// Remove from parent
//************************************************************************
void ObjectCollection::removeFromParent()
{
	onParentChanged( (GameObject*)parent, NULL);
	//remove self from parent, this will call release!
	parent->removeChild((GameObject*)this);
	parent = NULL;
}

//************************************************************************
// returns a specific child for direct array access
//************************************************************************
GameObject* ObjectCollection::childAtIndex(int index)
{
	if( index < 0 || index > numChildren() )
		return NULL;

	return (GameObject*)children->elementAt(index);
}

//************************************************************************
// Number of children
//************************************************************************
int ObjectCollection::numChildren()
{
	return children->getSize();
}

//************************************************************************
// Scan children for object
//************************************************************************
GameObject* ObjectCollection::find(String name)
{
	if( nameToIndex.containsKey(name))
	{ return (GameObject*)nameToIndex[name]; }

	return NULL;
}

/// <summary>
/// For help with syntax, a convenience call that casts GameObject
/// </summary>
TextLabel* ObjectCollection::findLabel(String name)
{ return (TextLabel*)find(name); }


/// <summary>
/// For help with syntax, a convenience call that casts GameObject
/// </summary>
SpriteObject* ObjectCollection::findSprite(String name)
{ return (SpriteObject*)find(name); }

/// <summary>
/// For help with syntax, a convenience call that casts GameObject
/// </summary>
ButtonObject* ObjectCollection::findButton(String name)
{ return (ButtonObject*)find(name); }

}

#ifndef IComparable_H_
#define IComparable_H_

namespace SICollections
{

typedef int(*CompareFunction)( const void* first, const void* other);

class IComparable
{
public:
	virtual ~IComparable(){}
	virtual int CompareTo( IComparable* comparable, CompareFunction* functionToCompare ) { return 0; }
};

}
#endif

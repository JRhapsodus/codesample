#ifndef INCLUDE_OBJECTCOLLECTION_H_
#define INCLUDE_OBJECTCOLLECTION_H_

#include "SIEngine/Collections/ObjectArray.h"
#include "SIEngine/Collections/Hashtable.h"

namespace SICore
{
	class GameObject;
	class TextLabel;
	class SpriteObject;
	class ButtonObject;
}

namespace SICollections
{

class ObjectCollection : public BaseObject
{
	CLASSEXTENDS(ObjectCollection, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	ObjectCollection();
	~ObjectCollection();

	/// <summary> 
	/// Public for gameObject.parent syntax
	/// </summary> 
	ObjectCollection* parent;

	/// <summary> 
	/// Normally I make an ICollection, and find the commons with  objectArray, but we are using a
	/// distinct naming difference so I will keep this nomenclature and not have a forced interface
	/// </summary> 
	void addChild(GameObject* obj);
	void addChildAndRelease(GameObject* obj);
	void removeChild(GameObject* obj);
	void removeFromParent();
	void adoptChild( GameObject* toAdopt );

	/// <summary> 
	/// ICollection  gettors
	/// </summary> 
	ObjectArray* getChildren();
	ObjectArray* toArray( bool subGraph );
	ObjectArray* toPointerArray( bool subGraph );
	GameObject* childAtIndex(int index);
	GameObject* find(String name);

	/// <summary>
	/// For help with syntax, a convenience call that casts GameObject
	/// </summary>
	TextLabel* findLabel(String name);

	/// <summary>
	/// For help with syntax, a convenience call that casts GameObject
	/// </summary>
	SpriteObject* findSprite(String name);

	/// <summary>
	/// For help with syntax, a convenience call that casts GameObject
	/// </summary>
	ButtonObject* findButton(String name);

	GameObject* getParent();
	int numChildren();

	/// <summary> 
	/// Notify callbacks: Implementation of NotifiyList : IList : ICollection
	/// </summary> 
   	virtual void onParentChanged(GameObject* oldParent, GameObject* newParent);
   	virtual void onChildAdded(GameObject* child);
   	virtual void onChildRemoved(GameObject* child);
   	virtual String toString();


protected:
   	int capacity;
    HashTable nameToIndex;
	ObjectArray* children;
};

}
#endif

#include "SpriteFrame.h"

namespace SICore
{

SpriteFrame::SpriteFrame() : super()
{
	uvArray[0].u = 0;
	uvArray[0].v = 1;

	uvArray[1].u = 0;
	uvArray[1].v = 0;

	uvArray[2].u = 1;
	uvArray[2].v = 0;

	uvArray[3].u = 1;
	uvArray[3].v = 1;
}

SpriteFrame::SpriteFrame( UV uvBottomLeft, UV uvTopLeft, UV uvTopRight, UV uvBottomRight )
{
	uvArray[0] = uvBottomLeft;
	uvArray[1] = uvTopLeft;
	uvArray[2] = uvTopRight;
	uvArray[3] = uvBottomRight;
}

UV SpriteFrame::getUV(int index)
{
	return uvArray[index];
}

SpriteFrame::SpriteFrame(float xMin, float yMin, float xMax, float yMax ) : super()
{
	UV topLeft(xMin, yMax);
	UV topRight(xMax, yMax);
	UV bottomLeft(xMin, yMin);
	UV bottomRight(xMax, yMin);

	uvArray[0] = bottomLeft;
	uvArray[1] = topLeft;
	uvArray[2] = topRight;
	uvArray[3] = bottomRight;
}

SpriteFrame::~SpriteFrame()
{ }

}

#include "ImageTarga.h"
#include <sstream>
#include <memory.h>
#include <DisplayTypes.h>
#include "SIEngine/Include/SIUtils.h"

namespace SICore
{

ImageTarga::ImageTarga() : super()
{
	screenStats.tPixelFormat = kPixelFormatARGB8888;
}

ImageTarga::~ImageTarga()
{ }

//************************************************************************************************************************
// Load TGA file from file
//************************************************************************************************************************
bool ImageTarga::load(String aFileNameTGA)
{
	FILE* fileDes = AssetLibrary::getBinaryFile(aFileNameTGA);

	//************************************************************************************************************************
	// Read header
	//************************************************************************************************************************
	TgaHeader header = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	size_t read = 0;
	read = fread(&header, sizeof(header), 1, fileDes);
	if (read != 1)
	{
		fclose(fileDes);
		return false;
	}
	if (header.theWidth == 0 || header.theHeight == 0)
	{
		printf("ERROR: [ImageTarga::load] The size of TGA image seems to be corrupted!\n");
		fclose(fileDes);
		return false;
	}
	if (header.theImageType != 2)
	{
		printf("ERROR: [ImageTarga::load] The compressed format is not supported!\n");
		fclose(fileDes);
		return false;
	}
	if (header.theBits != 32 && header.theBits != 24)
	{
		printf("ERROR: [ImageTarga::load] The 32 and 24 bits per pixel formats are supported only!\n");
		fclose(fileDes);
		return false;
	}

	//************************************************************************************************************************
	// Allocate pixels
	//************************************************************************************************************************
	uint tmpDataSize = header.theWidth * header.theHeight * header.theBits / 8; //<== This is really assuming a file format
	setInfo( header );
	allocateData(tmpDataSize);

	//************************************************************************************************************************
	// Stream pixels in
	//************************************************************************************************************************
	read = fread(imageData, sizeof(byte), tmpDataSize, fileDes);
	if (read != tmpDataSize)
	{ printf("WARNING: [ImageTarga::load] Image data was not loaded entirely!\n"); }

	//************************************************************************************************************************
	// Convert to RGBA file format
	//************************************************************************************************************************
	convertBGRA2RGBA();
	if (header.theYStart)
		flipVertical();

	if (fclose(fileDes))
	{ printf("WARNING: [ImageTarga::load] Couldn't properly close the \"%s\" file!\n", (const char*) aFileNameTGA.str()); }


	return true;
}
//**************************************************************************************************
// Sets info based on TGAHeader struct
//**************************************************************************************************
void ImageTarga::setInfo( TgaHeader& header )
{
	screenStats.width = header.theWidth;
	screenStats.height = header.theHeight;

	if( header.theBits == 32 )
	{ screenStats.tPixelFormat = kPixelFormatARGB8888; }
	else
	{ screenStats.tPixelFormat = kPixelFormatRGB888; }
}

//**************************************************************************************************
// Save targa file
//**************************************************************************************************
bool ImageTarga::save(String aFileNameTGA)
{
	FILE* fileDes = AssetLibrary::getFileHandle( aFileNameTGA, "wb" );

	//**********************************************************
	// Write TGA header into the file
	//**********************************************************
	size_t written = 0;
	TgaHeader header = { 0, 0, 2, 0, 0, 0, 0, 0, Width(), Height(), 32, 0 };
	written = fwrite(&header, sizeof(header), 1, fileDes);

	//**********************************************************
	// Write image data
	//**********************************************************
	uint dataSize = BytesPerPixel() * Width() * Height();
	written = fwrite(imageData, sizeof(byte), dataSize, fileDes);
	if (written != dataSize)
	{ printf("ERROR: [ImageTarga::save] File:\"%s\" was not written entirely.!\n", aFileNameTGA.str()); }
	if (fclose(fileDes))
	{ printf("ERROR: [ImageTarga::save] File:\"%s\" couldn't be closed!\n", aFileNameTGA.str()); }

	return true;
}

}

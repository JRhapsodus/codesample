#include "AnimationData.h"
#include "SIEngine/Core/Exception.h"

namespace SICore
{

AnimationData::AnimationData(ObjectArray* frameData, int frameCount_, float frameRate_, float animTime,
		AnimationEndOption option, bool loopReverse, int loops_, String name_) : super()
{
	frames = new ObjectArray();
	frames->addElementsFromArray(frameData);
	animationName = name_;
	frameCount = frameCount_;
	frameRate = frameRate_;
	loops = loops_;
	pingPong = loopReverse;
	endOption = option;
	animationTime = animTime;
	durationTime = animationTime*loops;
}

AnimationData::~AnimationData()
{ SAFE_RELEASE(frames); }

//*****************************************************
// Returns the total time the animation takes factoring
// in looping and ping ponging options
//*****************************************************
float 	AnimationData::getTotalTime()
{ return durationTime; }

//*****************************************************
// Returns number of frames in animation
//*****************************************************
int AnimationData::getFrameCount()
{ return frameCount; }

//*****************************************************
// Gets the EZGui framerate setting
//*****************************************************
float AnimationData::getFrameRate()
{ return frameRate; }

//*****************************************************
// Animation name
//*****************************************************
String AnimationData::getAnimationName()
{ return animationName; }

//*****************************************************
// Whether the animation loops
//*****************************************************
bool AnimationData::isLooping()
{ return (loops>0); }

//*****************************************************
// How many times the animation should loop
//*****************************************************
int	AnimationData::getLoopCount()
{ return loops; }

//*****************************************************
// The time for 1 pass of the animation not factoring
// in looping or ping ponging
//*****************************************************
float	AnimationData::getAnimationTime()
{ return animationTime; }

//*****************************************************
// Returns a frame of animation using a 0-1 normal time
//*****************************************************
SpriteFrame* AnimationData::getFrame( float time )
{
	return getFrame((int)(time*frameCount));
}

//*****************************************************
// Returns a specific clip of the animation
//*****************************************************
SpriteFrame* AnimationData::getFrame( int index )
{ return (SpriteFrame*)frames->elementAt(index); }

//*****************************************************
// Adds a frame to this animation
//*****************************************************
void AnimationData::addUVFrame( float xMin, float yMin, float xMax, float yMax )
{
	UV topLeft(xMin, yMax);
	UV topRight(xMax, yMax);
	UV bottomLeft(xMin, yMin);
	UV bottomRight(xMax, yMin);

	TRelease<SpriteFrame> frame(new SpriteFrame(bottomLeft, topLeft, topRight, bottomRight));
	frames->addElement(frame);
}


//*****************************************************
// Get the ending option
//*****************************************************
AnimationData::AnimationEndOption AnimationData::getEndOption()
{ return endOption; }

//*****************************************************
// Returns true if the animation ping pongs its frames
//*****************************************************
bool AnimationData::getPingPongs()
{ return pingPong; }

//*****************************************************
// This is a simple helper method for creating SpriteFrames
// which are left-to-right oriented within the texture.
//*****************************************************
AnimationData* AnimationData::generateAnimation(Size s, int frameWidth, int frameHeight)
{

	int fHorizontal = s.width_/frameWidth;
	int fVertical = s.height_/frameHeight;

	TRelease<ObjectArray> tempFrames( new ObjectArray());

	for (int j = 0; j < fVertical; j++)
	{
		for (int i = 0; i < fHorizontal; i++)
		{
			int x = i * frameWidth;
			int y = j * frameHeight;

			UV topLeft;
			UV topRight;
			UV bottomLeft;
			UV bottomRight;

			// Calculate the UVs based on our integer co-ordinates
			topLeft.u = x/s.width_;
			topLeft.v = y/s.height_;

			topRight.u = (x+frameWidth)/s.width_;
			topRight.v = topLeft.v;

			bottomRight.u = topRight.u;
			bottomRight.v = (y+frameHeight) / s.height_;

			bottomLeft.u = topLeft.u;
			bottomLeft.v = bottomRight.v;

			TRelease<SpriteFrame> frame(new SpriteFrame(bottomLeft, topLeft,topRight, bottomRight));
			tempFrames->addElement(frame);
		}
	}

	return new AnimationData( tempFrames, tempFrames->getSize(), 30, 1, AnimEndStatic, false, 0, "Default");
}

//*****************************************************
// Read in from file
//*****************************************************
AnimationData* AnimationData::deSerialize( FILE* fp )
{
	char animatioName[250];
	int numberFrames = 0;
	float frameRate = 0;
	float animTime = 0;
	int endOption = 0;
	int pingPong = 0;
	int loops = 0;

	if( fscanf(fp, "%249s %d %f %f %d %d %d", animatioName, &numberFrames, &frameRate, &animTime, &endOption, &pingPong, &loops) == 7 )
	{
		TRelease<ObjectArray> frames( new ObjectArray() );
		for( int i = 0; i<numberFrames; i++ )
		{
			float xmin, ymin, xmax, ymax;
			if( fscanf(fp, "%f %f %f %f", &xmin, &ymin, &xmax, &ymax ) == 4 )
			{ frames->addElement(TRelease<SpriteFrame>(new SpriteFrame(xmin, ymin, xmax, ymax))); }
		}

		return new AnimationData(frames, numberFrames, frameRate, animTime, (AnimationEndOption)endOption, (pingPong==9?false:true), loops, animatioName);
	}

	ThrowException::deserializeException();
}

}

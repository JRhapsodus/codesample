#include <sstream>

#include "FontData.h"
#include "SIEngine/Include/SIObjects.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Rendering/Materials/TextMaterial.h"

namespace SICore
{

FontGlyphKerning::FontGlyphKerning()
{
	amount = 0;
	first = 0;
	second = 0;
}

FontGlyphKerning::~FontGlyphKerning()
{ }

FontData *FontData::defaultFont()
{
	FontData *defaultFont = new FontData("default");
	return defaultFont;
}

FontData::FontData(String name_)
{
	fontName = name_;
	loadGlyphs();
}

FontData::~FontData()
{
	//Nothing is needed here, all members are stack objects
	//glyphs will release its members on ::~
}

Size FontData::getTextureSize()
{ return textureSize; }

String FontData::getTextureName()
{ return fontName; }


FontGlyph* FontData::getGlyph(int asciiValue)
{
	return (FontGlyph*)glyphs.get(asciiValue);
}

//********************************************************************************
// For each character in text get the appropriate SpriteData from the glyph
//********************************************************************************
ObjectArray *FontData::generateGlyphs(String text)
{
	//clear escaped carriage return
	text.replace("\\n", String("\n"));
	text.replace("\n", String("\n"));

	ObjectArray *glyphSprites = new ObjectArray();
	FontGlyph *lastGlyph = NULL;
	int xPos = 0;
	int yPos = 0;
	for (int i = 0; i < text.length(); i++)
	{
		char ch = text.charAt(i);
		int ic = (int)ch;

		if (ic == Constants::Text::NPC_CarriageReturn)
		{
			yPos -= lineHeight;
			xPos = 0;
			continue;
		}

		FontGlyph *glyph = getGlyph(ic);
		if (glyph == NULL)
		{ continue; }

		if (ic == Constants::Text::NPC_Space)
		{
			xPos += glyph->xadvance;
			lastGlyph = glyph;
			continue;
		}

		// Create a glyphSprite with that sprite data & font texture
		SpriteObject *glyphSprite = new SpriteObject(glyph->getSpriteData());
		int gX = xPos + (glyph->width / 2.0f) + glyph->xoffset;
		int gY = yPos - (glyph->height / 2.0f) - glyph->yoffset;

		// Set the Size & Position of the glyphSprite Size should come directly from the glyph data
		// position is calculated by advancing it's x position and incorporating kerning for previous char
		if (lastGlyph != NULL)
		{
			BaseObject* krnBase =  lastGlyph->kerningMap[ic];
			FontGlyphKerning* krn =(FontGlyphKerning*)krnBase;
			if (krn != NULL)
			{ gX += krn->amount; }
		}


		glyphSprite->setMaterial(TRelease<TextMaterial>( new TextMaterial(glyphSprite)));

		glyphSprite->setSize(Size(glyph->width, glyph->height));
		glyphSprite->setPosition(Vec3f(gX, gY, 0));
		glyphSprites->addElement(glyphSprite);
		glyphSprite->release();

		xPos += glyph->xadvance;
		lastGlyph = glyph;
	}

	return glyphSprites;
}

ObjectArray *FontData::generateLines(String text)
{
	text.replace("\\n", String("\n"));
	text.replace("\n", String("\n"));

	ObjectArray *lines = new ObjectArray();
	GameObject *line = new GameObject();
	lines->addElement(line);
	line->release();

	FontGlyph *lastGlyph = NULL;
	int xPos = 0;
	int yPos = 0;
	for (int i = 0; i < text.length(); i++)
	{
		char ch = text.charAt(i);
		int ic = (int)ch;

		if (ic == Constants::Text::NPC_CarriageReturn)
		{
			xPos = 0;
			line = new GameObject();
			lines->addElement(line);
			line->release();
			continue;
		}

		FontGlyph *glyph = getGlyph(ic);
		if (glyph == NULL)
		{ continue; }

		if (ic == Constants::Text::NPC_Space)
		{
			xPos += glyph->xadvance;
			lastGlyph = glyph;
			continue;
		}

		// Create a glyphSprite with that sprite data & font texture
		SpriteObject *glyphSprite = new SpriteObject(glyph->getSpriteData());
		int gX = xPos + (glyph->width / 2.0f) + glyph->xoffset;
		int gY = yPos - (glyph->height / 2.0f) - glyph->yoffset;

		// Set the Size & Position of the glyphSprite Size should come directly from the glyph data
		// position is calculated by advancing it's x position and incorporating kerning for previous char
		if (lastGlyph != NULL)
		{
			BaseObject* krnBase =  lastGlyph->kerningMap[ic];
			FontGlyphKerning* krn =(FontGlyphKerning*)krnBase;
			if (krn != NULL)
			{ gX += krn->amount; }
		}


		glyphSprite->setMaterial(TRelease<TextMaterial>( new TextMaterial(glyphSprite)));
		glyphSprite->setSize(Size(glyph->width, glyph->height));
		glyphSprite->setPosition(Vec3f(gX, gY, 0));
		line->addChild(glyphSprite);
		glyphSprite->release();


		xPos += glyph->xadvance;
		lastGlyph = glyph;
	}

	return lines;
}

ObjectArray* FontData::generateLinesWithMaxWidth(String text, int maxwidth, int maxLines)
{
	text.replace("\\n", String("\n"));
	text.replace("\n", String("\n"));
	//determine the truncation pattern width with current font
	float truncationPatternWidth = 0;
	String truncationPattern = Constants::UI::textTruncationEndPattern;
	FontGlyph *lastGlyph = NULL;
	for(int j = 0; j < truncationPattern.length(); j++)
	{
		char ch = text.charAt(j);
		int jc = (int)ch;
		FontGlyph *glyph = getGlyph(jc);
		if (glyph == NULL)
		{ continue; }

		if(lastGlyph != NULL)
		{
			BaseObject* krnBase =  lastGlyph->kerningMap[jc];
			FontGlyphKerning* krn =(FontGlyphKerning*)krnBase;
			if (krn != NULL)
			{ truncationPatternWidth += krn->amount; }
		}

		lastGlyph = glyph;
		truncationPatternWidth += glyph->xadvance;
	}

	int lastSpaceIndex = 0;
	int truncationInsertIndex = -1;
	float linewidth = 0;
	float lastSpaceWidth = 0;
	int lineCount = 0;

	lastGlyph = NULL;
	for (int i = 0; i < text.length(); i++)
	{
		char ch = text.charAt(i);
		int ic = (int)ch;

		if (ic == Constants::Text::NPC_CarriageReturn)
		{
			lineCount++;
			linewidth = 0;
			lastSpaceIndex = 0;
			truncationInsertIndex = -1;
			lastSpaceWidth = 0;
			continue;
		}

		if (ic == Constants::Text::NPC_Space)
		{
			lastSpaceWidth = linewidth;
			lastSpaceIndex = i;
		}

		FontGlyph *glyph = getGlyph(ic);
		if (glyph == NULL)
		{ continue; }


		if(lastGlyph != NULL)
		{
			BaseObject* krnBase =  lastGlyph->kerningMap[ic];
			FontGlyphKerning* krn =(FontGlyphKerning*)krnBase;
			if (krn != NULL)
			{ linewidth += krn->amount; }
		}

		lastGlyph = glyph;
				linewidth += glyph->xadvance;

		if(truncationInsertIndex == -1 && ((linewidth + truncationPatternWidth) >= maxwidth))
		{
			if(lineCount + 1 == maxLines)
			{
				truncationInsertIndex = i;
			}
		}

		if (linewidth >= maxwidth)
		{
			if(truncationInsertIndex != -1)
			{
				text = text.substring(0,truncationInsertIndex);
				text.append(truncationPattern);
				break;
			}
			else
			{
				lineCount++;
				truncationInsertIndex = -1;
				text.setCharAt(lastSpaceIndex, Constants::Text::NPC_CarriageReturn);
				linewidth -= lastSpaceWidth;
			}
		}
	}

	return generateLines(text);
}

void FontData::loadGlyphs()
{
	FILE* fp = AssetLibrary::getFile( "Shared/Fonts/", fontName, ".fnt", "rt");

	int r;
	struct line_reader lr;
	size_t len;
	char *line;
	AssetLibrary::lr_init(&lr, fp);

	// Load line 1 pattern : info face="%s" size=%d bold=%d italic=%d charset="%s" unicode=%d stretchH=%d smooth=%d aa=%d padding=%d,%d,%d,%d spacing=%d,%d
	char face_c[128];
	char charset_c[128];
	int bold_i, italic_i, unicode_i, smooth_i, aa_i;

	line = AssetLibrary::next_line(&lr, &len);
	r = sscanf(line, "info face=%127s size=%d bold=%d italic=%d charset=%127s unicode=%d stretchH=%d smooth=%d aa=%d padding=%d,%d,%d,%d spacing=%d,%d",
			face_c, &size, &bold_i, &italic_i, charset_c, &unicode_i,
			&stretchH, &smooth_i, &aa_i, &padding_top, &padding_left,
			&padding_right, &padding_bottom, &spacing_l, &spacing_r);
	if (r < 15)
	{ printf("Error parsing fnt info, line: 1\n"); }

	// Load line 2 pattern : common lineHeight=%d base=%d scaleW=%d scaleH=%d pages=%d packed=%d
	int scaleW, scaleH;
	line = AssetLibrary::next_line(&lr, &len);
	r = sscanf(line, "common lineHeight=%d base=%d scaleW=%d scaleH=%d pages=%d packed=%d",
			&lineHeight, &base, &scaleW, &scaleH, &pages, &packed);
	if (r < 6)
	{ printf("Error parsing fnt common, line: 2\n"); }
	textureSize = Size(scaleW, scaleH);

	// Load line 3 pattern : page id=%d file="%s"
	char file[128];
	int page_id;
	line = AssetLibrary::next_line(&lr, &len);
	r = sscanf(line, "page id=%d file=%127s", &page_id, file);
	if (r < 2)
	{ printf("Error parsing fnt page, line: 3\n"); }

	// Load line 4 pattern : chars count=%d
	int char_count;
	line = AssetLibrary::next_line(&lr, &len);
	r = sscanf(line, "chars count=%d", &char_count);
	if (r < 1) {
		printf("Error parsing character count, line: 4\n");
	}

	// Load chars pattern : char id=%d x=%d y=%d width=%d height=%d xoffset=%d yoffset=%d xadvance=%d page=%d chnl=%d letter="%s"
	for (int i = 0; i < char_count; i++) {
		char letter[128];
		FontGlyph *thisGlyph = new FontGlyph(this);
		line = AssetLibrary::next_line(&lr, &len);
		r = sscanf(line, "char id=%d x=%d y=%d width=%d height=%d xoffset=%d yoffset=%d xadvance=%d page=%d chnl=%d letter=%127s",
				&thisGlyph->id, &thisGlyph->x, &thisGlyph->y, &thisGlyph->width, &thisGlyph->height, &thisGlyph->xoffset,
				&thisGlyph->yoffset, &thisGlyph->xadvance, &thisGlyph->page, &thisGlyph->chnl, letter);

		if (r < 11) {
			printf("Error parsing fnt char, line: %d\n", i+5);
		}

		// Add the glyph to the hashmap...
		glyphs[thisGlyph->id] = thisGlyph;
		thisGlyph->generateUVs();
	}

	// Load kernings count pattern : kernings count=%d
	int kern_count;
	line = AssetLibrary::next_line(&lr, &len);
	if (line == NULL) return;
	r = sscanf(line, "kernings count=%d", &kern_count);
	if (r < 1) {
		printf("Error parsing fnt kernings count, line: %d\n", char_count + 5);
	}

	// Load kerning adjustments pattern : kerning first=%d second=%d amount=%d
	for (int i = 0; i < kern_count; i++)
	{
		FontGlyphKerning *krn = new FontGlyphKerning();

		line = AssetLibrary::next_line(&lr, &len);
		r = sscanf(line, "kerning first=%d second=%d amount=%d",
				&krn->first, &krn->second, &krn->amount);
		if (r < 3) {
			printf("Error parsing fnt kerning count, line: %d\n", char_count + 5 + i);
		}

		// Add kernings to relevant glyph kerning data
		FontGlyph *first = (FontGlyph *)glyphs.get(krn->first);
		if (first != NULL)
		{ first->kerningMap[krn->second] = krn; }
		else
		{ delete krn; }
	}

	// Set left over font object data
	face = String(face_c);
	charset = String(charset_c);

	bold = (bool) bold_i;
	italic = (bool) italic_i;
	unicode = (bool) unicode_i;
	smooth = (bool) smooth_i;
	aa = (bool) aa_i;

	AssetLibrary::lr_free(&lr);
	return;
}

int FontData::getLineHeight()
{ return lineHeight; }

int FontData::getBaseHeight()
{ return base; }

}

#ifndef FONTGLYPH_H_
#define FONTGLYPH_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Resources/SpriteData.h"
#include "SIEngine/Resources/FontData.h"

namespace SICore
{
	class FontData;

class FontGlyph : public BaseObject
{
	CLASSEXTENDS(FontGlyph,BaseObject);
	ADD_TO_CLASS_MAP;
public:
	FontGlyph(FontData *font_);
	virtual ~FontGlyph();

	FontData *font;
	SpriteData *getSpriteData();
	void generateUVs();

	/// Structural data from fnt file
	int id;

	int x;
	int y;
	int width;
	int height;
	int xoffset;
	int yoffset;
	int xadvance;
	int page;
	int chnl;
	String letter;
	HashTable kerningMap;

private:
	SpriteData *glyphSpriteData;
};

}

#endif /* FONTGLYPH_H_ */

#include "RawImage.h"

namespace SICore
{

///<summary>
/// Creates a buffer for use
///</summary>
RawImage::RawImage(int width, int height, tPixelFormat pixelFormat) : super()
{
	screenStats.width = width;
	screenStats.height = height;
	screenStats.tPixelFormat = pixelFormat;

	if( screenStats.tPixelFormat == kPixelFormatARGB8888 )
	{ allocateData( width, height, sizeof(byte)*4, 0 ); }
	else if ( screenStats.tPixelFormat == kPixelFormatRGB888 )
	{ allocateData( width, height, sizeof(byte)*3, 0 ); }
	else
	{ ThrowException::unsupportedException(String("RawImage::RawImage pixel format:") + (int)pixelFormat); }
}

///<summary>
/// Create a raw image  based on a currently existing buffer pointer, should we copy or share pointer?
///</summary>
RawImage::RawImage(byte* pixelBuffer, int width, int height, tPixelFormat pixelFormat) : super()
{
	imageData = pixelBuffer;
	screenStats.width = width;
	screenStats.height = height;
	screenStats.tPixelFormat = pixelFormat;
}

RawImage::~RawImage()
{
	printf("RawImage::~RawImage()\n");

}

/// <summary>
/// Load &  save
/// </summary>
bool RawImage::save(String aFileNameTGA)
{ return false; }

/// <summary>
/// Load &  save
/// </summary>
bool RawImage::load(String aFileNameTGA)
{ return false; }

///<summary>
/// Sets pixel at the pixel dimensions of col, row or x, y from upper left
///</summary>
void RawImage::setPixel(int col, int row, Color pixelColor)
{
	//We only support 32 bit RRGGBBBAA per channel right now
	if( screenStats.tPixelFormat == kPixelFormatARGB8888 )
	{
		imageData[col + row*screenStats.width] 	   = pixelColor.a;
		imageData[col + row*screenStats.width + 1] = pixelColor.r;
		imageData[col + row*screenStats.width + 2] = pixelColor.g;
		imageData[col + row*screenStats.width + 3] = pixelColor.b;
	}
	else
	{
		ThrowException::unsupportedException(String("RawImage::RawImage pixel format:") + (int)screenStats.tPixelFormat);
	}
}

///<summary>
/// Returns color of the pixel given at col, row or x,y from upper left
///</summary>
Color RawImage::getPixel(int col, int row)
{
	Color retColor;

	//We only support 32 bit RRGGBBBAA per channel right now
	if( screenStats.tPixelFormat == kPixelFormatARGB8888 )
	{
		retColor.a = imageData[col + row*screenStats.width];
		retColor.r = imageData[col + row*screenStats.width + 1];
		retColor.g = imageData[col + row*screenStats.width + 2];
		retColor.b = imageData[col + row*screenStats.width + 3];
	}
	else
	{
		ThrowException::unsupportedException(String("RawImage::RawImage pixel format:") + (int)screenStats.tPixelFormat);
	}

	return retColor;
}


} /* namespace SICore */

#ifndef ANIMATIONDATA_H_
#define ANIMATIONDATA_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Rendering/Texture.h"
#include "SIEngine/Resources/SpriteFrame.h"

using namespace SIRendering;


namespace SICore
{
class AnimationData : public BaseObject
{
	CLASSEXTENDS(AnimationData, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	enum AnimationEndOption
	{
		AnimEndNothing,
		AnimEndStatic,
		AnimEndReplay,
		AnimEndHide,
		AnimEndKill
	};

	AnimationData(ObjectArray* frameData, int frameCount, float frameRate, float animTime,
			AnimationEndOption option, bool loopReverse, int loops, String name );
	virtual ~AnimationData();

	/// <summary> 
	/// Returns number of frames in animation
	/// </summary> 
	int 	getFrameCount();

	/// <summary> 
	/// Gets the EZGui framerate setting
	/// </summary> 
	float 	getFrameRate();

	/// <summary> 
	/// Returns the total time the animation takes factoring
	/// in looping and ping ponging options
	/// </summary> 
	float 	getTotalTime();

	/// <summary> 
	/// Animation name
	/// </summary> 
	String	getAnimationName();

	/// <summary> 
	/// Adds a frame to this animation
	/// </summary> 
	void 	addUVFrame( float xMin, float yMin, float xMax, float yMax );

	/// <summary> 
	/// Whether the animation loops
	/// </summary> 
	bool 	isLooping();

	/// <summary> 
	/// How many times the animation should loop
	/// </summary> 
	int		getLoopCount();

	/// <summary> 
	/// The time for 1 pass of the animation not factoring
	/// in looping or ping ponging:
	/// 		(1.0f / frameRate) * frameCount;
	/// </summary> 
	float	getAnimationTime();

	/// <summary> 
	/// Returns a frame of animation using a 0-1 normal time
	/// </summary> 
	SpriteFrame* getFrame( float time );

	/// <summary> 
	/// Returns a specific clip of the animation
	/// </summary> 
	SpriteFrame* getFrame( int index );

	/// <summary> 
	/// Returns true if the animation ping pongs its frames
	/// </summary> 
	bool getPingPongs();

	/// <summary> 
	/// Get the ending option
	/// </summary> 
	AnimationData::AnimationEndOption getEndOption();

	/// <summary> 
	/// Read in from file
	/// </summary> 
	static AnimationData* deSerialize( FILE* fp );
	static AnimationData* generateAnimation(Size textureSize, int frameWidth, int frameHeight);
private:
	int frameCount;
	float frameRate;
	float durationTime;
	float animationTime;
	bool pingPong;
	int loops;
	AnimationData::AnimationEndOption endOption;
	ObjectArray* frames;
	String animationName;

};

}

#endif /* ANIMATIONDATA_H_ */

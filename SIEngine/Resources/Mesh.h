#ifndef MESH_H_
#define MESH_H_

#include <ImageIO.h>
#include <GLES2/gl2.h>
#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/SIGeometry.h"

namespace SICore
{

class Mesh : public BaseObject
{
	CLASSEXTENDS(Mesh, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	Mesh();
	Mesh( float *vertArray, short* indexArray, int numVert, int numIndices);

	virtual ~Mesh();

	void LoadMesh( String vertPath, String indexPath );
	void LoadMesh( float* vertArray, short* indexArray, int numVert, int numIndices );

	int vertCount;
	int indexCount;
	int *indices;
	LitPositionVertex* vertices;
	Triangle3f *triangles;
};

}

#endif /* MESH_H_ */

#ifndef FONTOBJECT_H_
#define FONTOBJECT_H_

#include <AppManager.h>
#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/SIGeometry.h"
#include "SIEngine/Rendering/Texture.h"
#include "SIEngine/Resources/FontGlyph.h"

LF_USING_BRIO_NAMESPACE();
using namespace SIRendering;

namespace SICore
{
	class FontGlyph;

/// <summary> 
/// Kerning struct
/// </summary> 
class FontGlyphKerning : public BaseObject
{
	CLASSEXTENDS(FontGlyphKerning, BaseObject);
	ADD_TO_CLASS_MAP;
public:

	FontGlyphKerning();
	virtual ~FontGlyphKerning();
	int first;
	int second;
	int amount;
};

/// <summary> 
/// FontData
/// </summary> 
class FontData: public BaseObject
{
	CLASSEXTENDS(FontData,BaseObject);
	ADD_TO_CLASS_MAP;
public:
	static FontData *defaultFont();

	FontData(String name);
	virtual ~FontData();

	String getTextureName();
	Size getTextureSize();

	ObjectArray*	generateLines(String text);
	ObjectArray*	generateLinesWithMaxWidth(String text, int width, int maxLines);
	ObjectArray*	generateGlyphs(String text);
	FontGlyph* 		getGlyph(int asciiValue);
	int				getLineHeight();
	int				getBaseHeight();

	String fontName;
	String face;
	Size textureSize;

	void loadGlyphs();

	/// Some of these may need to be public, but for now they'll reside here
	int size;
	bool bold;
	bool italic;
	bool unicode;
	int stretchH;
	bool smooth;
	bool aa;
	int padding_top;
	int padding_left;
	int padding_right;
	int padding_bottom;
	int spacing_l;
	int spacing_r;
	int lineHeight;
	int base;
	int pages;
	int packed;
	String charset;
	HashTable glyphs;
};

}
#endif /* FONTOBJECT_H_ */

#include "SpriteData.h"
#include "SIEngine/Rendering/TextureManager.h"

namespace SICore
{

SpriteData::SpriteData(String textureName_) : super()
{
	deferredLoading = false;
	currentAnimationTime = 0;
	currentFrameIndex = 0;
	currentAnimation = NULL;
	animations = new HashTable();
	staticFrame = new SpriteFrame();
	textureName = textureName_;
	stepDir = 1;
	numLoops = 0;
	hasEnded = true;
}

SpriteData::SpriteData(SpriteFrame* frame, String textureName_) : super()
{
	deferredLoading = false;
	currentAnimationTime = 0;
	currentFrameIndex = 0;
	currentAnimation = NULL;
	animations = new HashTable();
	staticFrame = frame;
	textureName = textureName_;
	staticFrame->addRef();
	stepDir = 1;
	numLoops = 0;
	hasEnded = true;
}

SpriteData::~SpriteData()
{ SAFE_RELEASE(staticFrame); }

//************************************************************************
// Set's the Static Frame, when no animation plays it uses this frame
// Most spriteObject's have no animation and thus this is the UV frame
//************************************************************************
void SpriteData::setStaticFrame( SpriteFrame* frame)
{
	SAFE_RELEASE(staticFrame);
	staticFrame = frame;
	frame->addRef();
}

//************************************************************************
// Returns the frame information for the sprite
//************************************************************************
SpriteFrame* SpriteData::getCurrentFrame()
{
	if( currentAnimation == null )
	{ return staticFrame; }
	else
	{ return currentAnimation->getFrame(currentFrameIndex); }
}

//************************************************************************
// Set frame based on frame number
//************************************************************************
void SpriteData::setFrame(int frameNumber)
{ currentFrameIndex = frameNumber; }

//************************************************************************
// Starts animation off
//************************************************************************
void SpriteData::startAnimation( String animationName )
{
	currentAnimation = getAnimation(animationName);
	currentFrameIndex = 0;
	currentAnimationTime = 0;
	stepDir = 1;
	numLoops = 0;
	hasEnded = false;
}

//************************************************************************
// Returns animation data for a named animation
//************************************************************************
AnimationData* SpriteData::getAnimation( String animationName )
{ return (AnimationData*)animations->get(animationName); }


//************************************************************************
// Adds animation to the spriteData object
//************************************************************************
void SpriteData::addAnimation( AnimationData* data )
{ animations->put(data->getAnimationName(), data); }

//************************************************************************
// Update animation
//************************************************************************
void SpriteData::update(float dt)
{
	if( currentAnimation != null && !hasEnded )
	{
		currentAnimationTime += dt;

		//************************************************************************
		// Get normalized position into current animation sequence, if we are
		// ping ponging then the frame number goes backwards after reaching end
		//************************************************************************
		float animdp = currentAnimationTime/currentAnimation->getAnimationTime();

		if( currentAnimation->getPingPongs() )
		{
			animdp = (currentAnimationTime/currentAnimation->getAnimationTime());

			if( stepDir < 0 )
			{ animdp = 1 - animdp; }
		}

		//************************************************************************
		// If looping, start the timer at the remainding time of cross over
		//************************************************************************
		if( currentAnimation->isLooping() )
		{
			if( currentAnimation->getPingPongs() && animdp <= 0 )
			{
				currentAnimationTime -= currentAnimation->getAnimationTime();
				numLoops++;
				stepDir = 1;
			}
			else if( animdp >= 1.0f )
			{
				currentAnimationTime -= currentAnimation->getAnimationTime();
				numLoops++;
				stepDir = -1;
			}
		}
		else if ( animdp > 1.0f )
		{
			//************************************************************************
			// Lets just use loop variable as a convenient way to end
			//************************************************************************
			numLoops++;
		}

		//************************************************************************
		// Advance frame: get the frame based on the normalized time
		//************************************************************************
		currentFrameIndex = (int)(animdp*(currentAnimation->getFrameCount()-1));

		if( currentFrameIndex >= currentAnimation->getFrameCount()-1)
		{ currentFrameIndex = currentAnimation->getFrameCount()-1; }
		if( currentFrameIndex <= 0 )
		{ currentFrameIndex = 0; }

		//************************************************************************
		// If are beyond the bounds of the loop count we have ended
		//************************************************************************
		if( numLoops > currentAnimation->getLoopCount() )
		{
			hasEnded = true;
			AnimationData::AnimationEndOption endOption = currentAnimation->getEndOption();
			String animationName = currentAnimation->getAnimationName();

			if( endOption == AnimationData::AnimEndStatic )
			{ currentAnimation = null; }
			else if ( endOption == AnimationData::AnimEndReplay )
			{ startAnimation(animationName); }
			else if ( endOption == AnimationData::AnimEndNothing )
			{ }
		}
	}
}

void SpriteData::setDeferredLoad( bool deferred )
{ deferredLoading = deferred; }

bool SpriteData::getDeferredLoad()
{ return deferredLoading; }

String SpriteData::getTextureName()
{ return textureName; }

/// <summary>
/// ************************************************************************
/// Sets Texture Name
/// ************************************************************************
/// </summary>
void SpriteData::setTexture( String newTexture )
{
	textureName = newTexture;
	Texture* texture = getTexture();
	Size dimension = texture->getSize();

	unsigned int widthPowerOf2 = (unsigned int)dimension.width_;
	widthPowerOf2--;
	widthPowerOf2 = (widthPowerOf2 >> 1) | widthPowerOf2;
	widthPowerOf2 = (widthPowerOf2 >> 2) | widthPowerOf2;
	widthPowerOf2 = (widthPowerOf2 >> 4) | widthPowerOf2;
	widthPowerOf2 = (widthPowerOf2 >> 8) | widthPowerOf2;
	widthPowerOf2 = (widthPowerOf2) | widthPowerOf2;
	widthPowerOf2++;

	unsigned int heightPowerOf2 = (unsigned int)dimension.height_;
	heightPowerOf2--;
	heightPowerOf2 = (heightPowerOf2 >> 1) | heightPowerOf2;
	heightPowerOf2 = (heightPowerOf2 >> 2) | heightPowerOf2;
	heightPowerOf2 = (heightPowerOf2 >> 4) | heightPowerOf2;
	heightPowerOf2 = (heightPowerOf2 >> 8) | heightPowerOf2;
	heightPowerOf2 = (heightPowerOf2) | heightPowerOf2;
	heightPowerOf2++;

	printf("New Texture Size [%f X %f] nextPowerOf2 [%d X %d]\n", dimension.width_, dimension.height_, widthPowerOf2, heightPowerOf2);

	TRelease<SpriteFrame> newFrame(new SpriteFrame() );
	setStaticFrame(newFrame);
}

/// <summary>
/// ************************************************************************
/// Returns texture reference for this sprite data
/// ************************************************************************
/// </summary>
Texture *SpriteData::getTexture()
{
	if( deferredLoading )
	{
		if( TextureManager::getInstance()->hasTexture(textureName) )
		{ return TextureManager::getInstance()->getTexture(textureName); }

		return NULL;
	}
	else
	{
		return TextureManager::getInstance()->getTexture(textureName);
	}

}

}

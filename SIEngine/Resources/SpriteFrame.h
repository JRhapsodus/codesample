#ifndef SPRITEFRAME_H_
#define SPRITEFRAME_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/SIGeometry.h"

namespace SICore
{

class SpriteFrame : public BaseObject
{
	CLASSEXTENDS(SpriteFrame, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	SpriteFrame();
	SpriteFrame(float xmin, float ymin, float xmax, float ymax );
	SpriteFrame( UV uvBottomLeft, UV uvTopLeft, UV uvTopRight, UV uvBottomRight );
	virtual ~SpriteFrame();

	/*
	 * Returns the UV for a given vertex. 0 is upper left, 1 is upper right, 2 lower right, 3 lower left
	 */
	UV getUV(int index);
private:
	UV uvArray[4];
};

}

#endif /* SPRITEFRAME_H_ */

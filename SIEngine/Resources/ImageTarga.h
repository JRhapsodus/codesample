#ifndef IMAGETARGA_H_
#define IMAGETARGA_H_

#include "SIEngine/Include/SIBase.h"
#include "IImage.h"

namespace SICore
{
/// <summary> 
/// Header information from TGA file format
/// </summary> 
#pragma pack(push, 1)
	typedef struct
	{
		unsigned char theIdentSize; /// size of ID field that follows 18 byte header (0 usually)
		unsigned char theColourMapType; /// type of colour map 0=none, 1=has palette
		unsigned char theImageType; /// type of image 0=none,1=indexed,2=rgb,3=grey,+8=rle packed

		short theColourMapStart;            /// first colour map entry in palette
		short theColourMapLength;            /// number of colours in palette
		unsigned char theColourMapBits; /// number of bits per palette entry 15,16,24,32

		short theXStart;                     /// image x origin
		short theYStart;                     /// image y origin
		short theWidth;                      /// image width in pixels
		short theHeight;                     /// image height in pixels

		unsigned char theBits;               /// image bits per pixel 8,16,24,32
		unsigned char theDescriptor;     /// image descriptor bits (vh flip bits)
		/// pixel data follows
	} TgaHeader;
#pragma pack(pop)


/// <summary> 
/// Targa image
/// </summary> 
class ImageTarga : public IImage
{
	CLASSEXTENDS(ImageTarga, IImage);
	ADD_TO_CLASS_MAP;
public:
	ImageTarga();
	virtual ~ImageTarga();

	/// <summary> 
	/// Load and save
	/// </summary> 
	bool save(String aFileNameTGA);
	bool load(String aFileNameTGA);

	/// <summary> 
	/// Sets info based on TGAHeader struct
	/// </summary> 
	void setInfo( TgaHeader& header );
};

}
#endif /* IMAGETARGA_H_ */

#ifndef SPRITEDATA_H_
#define SPRITEDATA_H_

#include "SpriteFrame.h"
#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Resources/AnimationData.h"
#include "SIEngine/Resources/SpriteFrame.h"
#include "SIEngine/Rendering/Texture.h"

using namespace SIRendering;

namespace SICore
{

/// <summary> 
/// Contains information for SpriteObject UV frame
/// </summary> 
class SpriteData : public BaseObject
{
	CLASSEXTENDS(SpriteData, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	SpriteData(String textureName);
	SpriteData(SpriteFrame* frame, String textureName);

	virtual ~SpriteData();

	/// <summary> 
	/// Returns current frame
	/// </summary> 
	SpriteFrame*	getCurrentFrame();

	/// <summary> 
	/// Returns texture
	/// </summary> 
	Texture*		getTexture();

	/// <summary> 
	/// Adds an animation
	/// </summary> 
	void 			addAnimation( AnimationData* data );

	/// <summary> 
	/// Sets frame
	/// </summary> 
	void			setFrame(int frameNumber);

	/// <summary> 
	/// Sets static frame : No animation playing
	/// </summary> 
	void			setStaticFrame( SpriteFrame* frame);

	/// <summary> 
	/// Starts animation
	/// </summary> 
	void 			startAnimation( String animationName );

	/// <summary> 
	/// Animation pump
	/// </summary> 
	void 			update(float dt);

	/// <summary> 
	/// Returns animation data for a name
	/// </summary> 
	AnimationData*	getAnimation( String animationName );

	/// <summary>
	/// Sets Texture Name
	/// </summary>
	void setTexture( String textureName );


	String getTextureName();


	void setDeferredLoad( bool deferred );
	bool getDeferredLoad();
private:
	/// <summary> 
	/// Animations
	/// </summary> 
	HashTable* animations;
	SpriteFrame* staticFrame;
	String textureName;

	/// <summary> 
	/// Animation Player
	/// </summary> 
	AnimationData* currentAnimation;
	bool hasEnded;
	bool deferredLoading;
	float currentAnimationTime;
	int currentFrameIndex;
	int stepDir;
	int numLoops;
};


}

#endif /* SPRITEDATA_H_ */

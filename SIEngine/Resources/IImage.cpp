#include <StringTypes.h>
#include "IImage.h"

namespace SICore
{


IImage::IImage() : super()
{
	imageData = NULL;
	usesAlpha = false;
}

IImage::~IImage()
{  delete [] imageData; }

//************************************************************************************************
// Bits
//************************************************************************************************
uint IImage::BitsPerPixel() const
{
	switch ( screenStats.tPixelFormat )
	{
	case (kPixelFormatRGB4444):
			return 16;
	case( kPixelFormatRGB565):
			return 16;
	case( kPixelFormatARGB8888):
			return 32;
	case( kPixelFormatRGB888):
			return 24;
	case( kPixelFormatYUV420):
			return 8;
	case( kPixelFormatYUYV422):
			return 16;
	case( kPixelFormatXRGB8888):
			return 32;
	default:
		return 0;
	}
}

//************************************************************************************************
// Is this really a per channel concept?
//************************************************************************************************
uint IImage::BytesPerPixel() const
{
	switch ( screenStats.tPixelFormat )
	{
	case (kPixelFormatRGB4444):
			return 4; //ARGB 4:4:4:4
	case( kPixelFormatRGB565):
			return 3; // RGB   5:6:5
	case( kPixelFormatARGB8888):
			return 4; //ARGB 8:8:8:8
	case( kPixelFormatRGB888):
			return 3; // RGB   8:8:8
	case( kPixelFormatYUV420):
			return 3; //4:2:0
	case( kPixelFormatYUYV422):
			return 3; //4:2:2
	case( kPixelFormatXRGB8888):
			return 4; // 8:8:8:8
	default:
		return 4;
	}
}

//************************************************************************************************
// Width
//************************************************************************************************
uint IImage::Width() const
{ return screenStats.width; }

//************************************************************************************************
// Height
//************************************************************************************************
uint IImage::Height() const
{ return screenStats.height; }

//************************************************************************************************
// Pixels
//************************************************************************************************
byte* IImage::RawPixels()
{ return imageData; }


//************************************************************************************************
// Allocations memory
//************************************************************************************************
void IImage::allocateData(uint aDataSize, byte aClearValue )
{
	delete [] imageData;
	imageData = new byte[aDataSize];
	memset(imageData, aClearValue, aDataSize);
}

//************************************************************************************************
// Allocates memory
//************************************************************************************************
void IImage::allocateData(uint aWidth, uint aHeight, uint aBitsPerPixel, byte aClearValue )
{
	uint dataSize = aWidth * aHeight * (aBitsPerPixel / 8);
	allocateData(dataSize, aClearValue);
}


void IImage::copyPixels(const byte* aData, uint aWidth, uint aHeight)
{



}

//************************************************************************************************************************
// Why would you ever do this as a software rasterized function?, you can just multiple *-1 by the transform
//************************************************************************************************************************
void IImage::flipVertical()
{
	uint halfHeight = (Height() - (Height() % 2)) / 2;
	uint upIndex = 0;
	uint bottomIndex = 0;
	byte byteC = 0;
	//
	for (uint y = 0; y < halfHeight; y++)
	{
		for (uint x = 0; x < Width(); x++)
		{
			for (uint c = 0; c < BytesPerPixel(); c++)
			{
				upIndex = y * Width() * BytesPerPixel() + x * BytesPerPixel() + c;
				bottomIndex = (Height() - y - 1) * Width() * BytesPerPixel() + x * BytesPerPixel() + c;
				//
				byteC = imageData[upIndex];
				imageData[upIndex] = imageData[bottomIndex];
				imageData[bottomIndex] = byteC;
			}
		}
	}
}

//************************************************************************************************
// Changes channels around
//************************************************************************************************
void IImage::convertBGRA2RGBA()
{
	if( imageData != NULL )
	{
		byte* ptrEnd = imageData + (Width() * Height() * BytesPerPixel());
		byte tmpVal = 0;
		for (byte *p = imageData, *p2 = (imageData + 2); p < ptrEnd; p += 4, p2 += 4)
		{
			tmpVal = *p2;
			*p2 = *p;
			*p = tmpVal;
		}
	}
}

//************************************************************************************************
// Pixel format
//************************************************************************************************
enum tPixelFormat IImage::PixelFormat()
{ return (tPixelFormat)screenStats.tPixelFormat; }

//************************************************************************************************
// Copies pixels from source target to dst target, This shoudl really be a static as we are not
//modifying our own imageData, so this is misleading. I will change in future. Part of tga loader
//************************************************************************************************
void IImage::copyPixels( uint aSrcWidth, uint aSrcHeight, uint aSrcBitsPerPixel, const byte* aSrcData,
		uint aDstWidth, uint aDstHeight, uint aDstBitsPerPixel, byte* aDstData) const
{
	if (aSrcWidth != aDstWidth || aSrcHeight != aDstHeight)
	{
		printf("ERROR: [Image::copyData] Dimensions are incorrect to copy data!\n");
		return;
	}

	unsigned int dstIndex = 0;
	unsigned int dstBytesPerPixel = aDstBitsPerPixel / 8;
	unsigned int srcIndex = 0;
	unsigned int srcBytesPerPixel = aSrcBitsPerPixel / 8;
	for (unsigned int y = 0; y < aSrcHeight; y++)
	{
		for (unsigned int x = 0; x < aSrcWidth; x++)
		{
			for (unsigned int c = 0; c < dstBytesPerPixel; c++)
			{
				dstIndex = y * aDstWidth * dstBytesPerPixel + x * dstBytesPerPixel + c;
				if (c < srcBytesPerPixel)
				{
					srcIndex = y * aSrcWidth * srcBytesPerPixel + x * srcBytesPerPixel + c;
					aDstData[dstIndex] = aSrcData[srcIndex];
				}
				else
				{
					aDstData[dstIndex] = 255;
				}
			}
		}
	}
}
}

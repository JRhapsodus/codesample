#include <stdio.h>
#include <sstream>
#include <memory.h>
#include <png.h>
#include <StringTypes.h>
#include <DisplayTypes.h>

#include "ImagePNG.h"
#include "SIEngine/Include/SIUtils.h"

namespace SICore
{


ImagePNG::ImagePNG(String aFileNameTGA) : super()
{
	isValid = load(aFileNameTGA);
}

ImagePNG::ImagePNG() : super()
{ }

ImagePNG::~ImagePNG()
{ }

//************************************************************************************************
// Load png
//************************************************************************************************
bool ImagePNG::load(String aFileNamePNG)
{
	//************************************************************************************************
	// Open file
	//************************************************************************************************
	screenStats.tPixelFormat = kPixelFormatARGB8888;
	FILE *fp = AssetLibrary::getImageFile(aFileNamePNG);

	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	png_infop info_ptr = png_create_info_struct(png_ptr);

	//************************************************************************************************
	// Set error handler
	//************************************************************************************************
	if (setjmp(png_jmpbuf(png_ptr)) || (png_ptr == NULL || info_ptr == NULL) )
	{
		printf("error from libpng\n");
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
		fclose(fp);
		return false;
	}

	//************************************************************************************************
	// init png reading
	//************************************************************************************************
    png_init_io(png_ptr, fp);
	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_EXPAND|PNG_TRANSFORM_STRIP_16, NULL);

	screenStats.width = info_ptr->width;
	screenStats.height = info_ptr->height;
	imageData = new byte[screenStats.width * screenStats.height * 4]; //AARRGGBB ?
	png_bytepp rows = png_get_rows(png_ptr, info_ptr);

	if( rows == null )
	{
		delete [] imageData;
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
		fclose(fp);
		return false;
	}

	int padding = 0;
	byte*	pDest = imageData;
	byte*	pSrc;
	byte	b;

	//************************************************************************************************
	// 1 channel gray scale png files
	//************************************************************************************************
	if (info_ptr->channels == 1)
	{
		for( uint r=0; r < info_ptr->height; r++)
		{
			pSrc = rows[r];
			for( int c=info_ptr->width; c-- > 0; )
			{
				*pDest++ = 0xff;
				b = *pSrc++;
				*pDest++ = b;
				*pDest++ = b;
				*pDest++ = b;
			}
			pDest += padding;
		}
	}
	//************************************************************************************************
	// 2 color channels with rows in RGB, no alpha
	//************************************************************************************************
	else if (info_ptr->channels == 2)
	{
		for( uint r=0; r < info_ptr->height; r++)
		{
			pSrc = rows[r];
			for( int c=info_ptr->width; c-- > 0; )
			{
				*pDest++ = *pSrc++;
				b = *pSrc++;
				*pDest++ = b;
				*pDest++ = b;
				*pDest++ = b;
			}

			pDest += padding;
		}
	}
	//************************************************************************************************
	// Rows in RGB
	//************************************************************************************************
	else if (info_ptr->channels == 3)
	{
		for( uint r=0; r < info_ptr->height; r++)
		{
			pSrc = rows[r];
			for( int c=info_ptr->width; c-- > 0; )
			{
				//big endian
				*pDest++ = *pSrc++;
				*pDest++ = *pSrc++;
				*pDest++ = *pSrc++;
				*pDest++ = 0xff; //set alpha to full
			}
			pDest += padding;
		}
	}
	//************************************************************************************************
	// AARRGGBB
	//************************************************************************************************
	else if (info_ptr->channels == 4)
	{
		usesAlpha = true;
		for( uint r=0; r < info_ptr->height; r++)
		{
			pSrc = rows[r];
			memcpy(pDest, pSrc, info_ptr->width*4);
			pDest += info_ptr->width*4;
			pDest += padding;
		}
	}

	//************************************************************************************************
	// Cleanup
	//************************************************************************************************
	png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
	fclose(fp);

	return true;
}


bool ImagePNG::save( String aFileNamePNG )
{
	return false;
}

}

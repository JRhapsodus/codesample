#ifndef IMAGEPNG_H_
#define IMAGEPNG_H_

#include "IImage.h"

namespace SICore
{

/// <summary> 
/// PNG image
/// </summary> 
class ImagePNG : public IImage
{
	CLASSEXTENDS(ImagePNG, IImage);
	ADD_TO_CLASS_MAP;
public:
	ImagePNG(String aFileNameTGA);
	ImagePNG();
	virtual ~ImagePNG();

	/// <summary> 
	/// Load and save
	/// </summary> 
	virtual bool save(String aFileNameTGA);
	virtual bool load(String aFileNameTGA);

	bool isValid;
};

}

#endif

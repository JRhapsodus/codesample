#include "Mesh.h"
#include "SIEngine/Utilities/AssetLibrary.h"
using namespace SIUtils;

namespace SICore
{

Mesh::Mesh() : super()
{
	vertCount = 0;
	indexCount = 0;
	indices = NULL;
	vertices = NULL;
	triangles = NULL;

}

Mesh::~Mesh()
{
	delete [] vertices;
	delete [] indices;
	delete [] triangles;
}

Mesh::Mesh(float* vertArray, short* indexArray,int numVert, int numIndices)  : super()
{
	vertCount = 0;
	indexCount = 0;
	indices = NULL;
	vertices = NULL;
	triangles = NULL;
	LoadMesh( vertArray, indexArray, numVert, numIndices);
}


//******************************************************************************
// Very crude loader, vertex data in count x,y,z format, this should be subclassed.
// IE:  ObjMesh  and XMesh with a path to the file as opposed to separate channels
//******************************************************************************
void Mesh::LoadMesh( String vertPath, String indexPath )
{
	//******************************************************************************
	// Read in verts
	//******************************************************************************
	FILE* vertFile = AssetLibrary::getFileHandle(vertPath, "r");
	int numberOfVerts = 0;
	int numRead = fscanf(vertFile, "%d ", &numberOfVerts );
	int count = 0;
	if( numRead == 1 )
	{
		vertices = new LitPositionVertex[numberOfVerts];
		vertCount = numberOfVerts;
		float x, y, z;
		for( int i = 0; i<numberOfVerts; i++ )
		{
			numRead = fscanf( vertFile, "%f,%f,%f,", &x, &y, &z);
			if( numRead == 3 )
			{
				vertices[i].position = Vec3f(x, y, z);
				vertices[i].color = Color( (float)(i%255)/255.0f, (float)(i%255)/255.0f, 1, 0.2f );
				count += numRead;
			}
			else
			{ printf("WENT BOOM %d", i); }
		}

		printf("READ IN %d VERTICES", count);
	}

	//******************************************************************************
	// Read in indices
	//******************************************************************************
	FILE* indexFile = AssetLibrary::getFileHandle(indexPath, "r");
	numRead = fscanf(indexFile, "%d ", &indexCount );
	if( numRead == 1 )
	{
		indices = new int[indexCount+1];
		count = 0;
		for ( int i = 0; i<=indexCount; i++ )
		{
			numRead = fscanf(indexFile, "%d,", &indices[i]);
			if( numRead == 1 )
			{ count += numRead; }
		}

		printf("READ IN %d INDICES", count);
	}


	fclose(vertFile);
	fclose(indexFile);

}

//******************************************************************************
// Loads a mesh by a copy of a static array reaaly, this is for cheap porting
//******************************************************************************
void Mesh::LoadMesh( float* vertArray, short* indexArray,int numVert, int numIndices )
{
	//Build internal vertices array
	vertCount = numVert;

	//fill vertices and color
	vertices = new LitPositionVertex[vertCount];
	for ( int i = 0; i<3*vertCount; i+=3 )
	{
		vertices[(int)(i/3)].position = Vec3f(vertArray[i], vertArray[i+1], vertArray[i+2] );
		vertices[(int)(i/3)].color = Color(0.1f * (i%10), 1, 1 );
	}

	//fill in indices from mesh
	indexCount = numIndices;
	indices = new int[indexCount];
	for ( int i = 0; i<indexCount; i++ )
	{ indices[i] = indexArray[i]; }
}

}

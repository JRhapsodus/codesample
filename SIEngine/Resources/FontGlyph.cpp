#include "FontGlyph.h"

namespace SICore
{

FontGlyph::FontGlyph(FontData *font_) {
	font = font_;
	id=0;
	x=0;
	y=0;
	width=0;
	height=0;
	xoffset=0;
	yoffset=0;
	xadvance=0;
	page=0;
	chnl=0;
	letter="";
	glyphSpriteData = NULL;
}

SpriteData *FontGlyph::getSpriteData() {
	return glyphSpriteData;
}

void FontGlyph::generateUVs()
{
	if (glyphSpriteData != NULL)
	{
		printf("Glyph UVs already generated\n");
		return;
	}

	Size s = font->getTextureSize();
	UV topLeft;
	UV topRight;
	UV bottomLeft;
	UV bottomRight;

	// Calculate the UVs based on our integer co-ordinates
	topLeft.u = (float)x/s.width_;
	topLeft.v = (float)y/s.height_;

	topRight.u = (float)(x+width)/s.width_;
	topRight.v = topLeft.v;

	bottomRight.u = topRight.u;
	bottomRight.v = (float)(y+height) / s.height_;

	bottomLeft.u = topLeft.u;
	bottomLeft.v = bottomRight.v;

	TRelease<SpriteFrame> frame(new SpriteFrame(bottomLeft, topLeft, topRight, bottomRight));
	glyphSpriteData = new SpriteData(frame, String("Shared/Textures/Fonts/") + font->getTextureName());
}

FontGlyph::~FontGlyph()
{
	glyphSpriteData->release();
}

}

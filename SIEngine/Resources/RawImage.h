#ifndef RAWIMAGE_H_
#define RAWIMAGE_H_

#include "SIEngine/Resources/IImage.h"
#include "SIEngine/Include/SIUtils.h"
#include <DisplayTypes.h>

LF_USING_BRIO_NAMESPACE();

namespace SICore
{

class RawImage : public IImage
{
	CLASSEXTENDS(RawImage, IImage);
	ADD_TO_CLASS_MAP;
public:
	RawImage(int width, int height, tPixelFormat pixelFormat);
	RawImage(byte* pixelBuffer, int width, int height, tPixelFormat pixelFormat);
	virtual ~RawImage();

	///<summary>
	/// Sets pixel at the pixel dimensions of col, row or x, y from upper left
	///</summary>
	void setPixel(int col, int row, Color pixelColor);

	///<summary>
	/// Returns color of the pixel given at col, row or x,y from upper left
	///</summary>
	Color getPixel(int col, int row);

	/// <summary>
	/// Load &  save
	/// </summary>
	virtual bool load(String aFileNameTGA);
	virtual bool save(String aFileNameTGA);

};

}
#endif

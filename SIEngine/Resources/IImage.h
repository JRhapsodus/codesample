#ifndef IIMAGE_H_
#define IIMAGE_H_

#include "SIEngine/Include/SIBase.h"
#include <DisplayTypes.h>

namespace SICore
{

/// <summary> 
/// Base Image class, holds pixels which are array of bytes...
/// </summary> 
class IImage : public BaseObject
{
	CLASSEXTENDS(IImage, BaseObject);
	ADD_TO_CLASS_MAP;

public:
	IImage();
	virtual ~IImage();

	/// <summary> 
	/// Information about the Image, Pixel format, bits, bytes, width, height
	/// </summary> 
	enum tPixelFormat PixelFormat();
	uint BytesPerPixel() const;
	uint BitsPerPixel() const;
	uint Width() const;
	uint Height() const;
	byte* RawPixels();

	/// <summary> 
	/// Load &  save
	/// </summary> 
	virtual bool load(String aFileNameTGA) = 0;
	virtual bool save(String aFileNameTGA) = 0;

	/// <summary> 
	/// Helper functions for all file formats
	/// </summary> 
	void copyPixels(uint aSrcWidth, uint aSrcHeight, uint aSrcBitsPerPixel, const byte* aSrcData, uint aDstWidth, uint aDstHeight, uint aDstBitsPerPixel, byte* aDstData) const;
	void copyPixels(const byte* aData, uint aWidth, uint aHeight);
	void allocateData(uint aDataSize, byte aClearValue = 127);
	void allocateData(uint aWidth, uint aHeight, uint aBitsPerPixel, byte aClearValue = 127);

	/// <summary> 
	/// Rasterize processing functions
	/// </summary> 
	void convertBGRA2RGBA();
	void flipVertical();

protected:
	bool usesAlpha;
	byte* imageData;
	tDisplayScreenStats screenStats;
};

}
#endif /* IIMAGE_H_ */

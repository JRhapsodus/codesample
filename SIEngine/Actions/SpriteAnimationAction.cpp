#include "SpriteAnimationAction.h"

namespace SIActions
{

SpriteAnimationAction::SpriteAnimationAction(GameObject *object, int startFrame_, int endFrame_, int fps_) : super(object) {
	startFrame = startFrame_;
	endFrame = endFrame_;
	fps = fps_;
	timeElapsed = 0;
	loop = true;
}

SpriteAnimationAction::~SpriteAnimationAction()
{ }

bool SpriteAnimationAction::isCompleted()
{
	int nFrames = endFrame - startFrame;

	float timeToTake = (1.0f/(float)fps)*nFrames;

	return timeElapsed>=timeToTake;
}

void SpriteAnimationAction::update(float dt)
{
	timeElapsed += dt;
	int nFrames = endFrame - startFrame;

	float timeToTake = (1.0f/(float)fps)*nFrames;
	float fraction = timeElapsed/timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;

	if (fraction == 1 && loop == true) {
		timeElapsed = 0;
	} else if (fraction == 1){
		onComplete();
		return;
	}

	int currentFrame = floorf(fraction * nFrames) + startFrame;

	SpriteObject *so = (SpriteObject *)GameObj;
	so->setFrame(currentFrame);
}

void SpriteAnimationAction::onComplete()
{
	SpriteObject *so = (SpriteObject *)GameObj;
	so->setFrame(endFrame);
	super::onComplete();
}

void SpriteAnimationAction::onStart()
{
	//SpriteObject *so = (SpriteObject *)GameObj;
	//so->setFrame(startFrame);
	super::onStart();
}

}

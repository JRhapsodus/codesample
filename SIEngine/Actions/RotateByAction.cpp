#include <stdio.h>
#include "SIEngine/Actions/RotateByAction.h"

namespace SIActions
{


RotateByAction::RotateByAction(GameObject *obj, Quaternion delta, float duration, EaseDelegate easing) : super(obj, duration)
{
	usedQuaternion = true;
	mathDelegate = easing;
	deltaQuat = delta;
}


RotateByAction::RotateByAction(GameObject *obj, Vec3f delta, float duration, EaseDelegate easing) : super(obj,duration)
{
	usedQuaternion = false;
	mathDelegate = easing;
	delta_ = delta;
}

RotateByAction::RotateByAction(GameObject * obj, Vec3f delta, float duration) :
	super(obj, duration)
{
	usedQuaternion = false;
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	delta_ = delta;
}

RotateByAction::RotateByAction(GameObject * obj, Vec3f delta) :
		super(obj, 0)
{
	usedQuaternion = false;
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	delta_ = delta;
}

RotateByAction::~RotateByAction()
{


}

void RotateByAction::update(float dt)
{
	super::update(dt);

	float fraction = timeElapsed / timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;
	fraction = (float)mathDelegate(fraction, 0, 1, 1);

	if( usedQuaternion )
	{
		GameObj->setRotation(Quaternion::nlerp(startQuat, destQuat, fraction, false));
	}
	else
	{
		Vec3f result = Vec3f::lerp(startRotation, destinationRotation, fraction);
		GameObj->setRotation( result );
	}
}

void RotateByAction::onComplete()
{
	if( usedQuaternion )
	{ GameObj->setRotation( destQuat );	}
	else
	{
		if( destinationRotation == Vec3f::Zero )
		{ GameObj->setRotation( Quaternion::identity ); }

		GameObj->setRotation( destinationRotation );
	}
	super::onComplete();
}

void RotateByAction::onStart() {

	startRotation = GameObj->getEulerRotation();
	destinationRotation = startRotation + delta_;
	startQuat = GameObj->getRotation();
	destQuat = startQuat * deltaQuat;
	super::onStart();
}

}

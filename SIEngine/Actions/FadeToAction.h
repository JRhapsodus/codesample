#ifndef FADETOACTION_H_
#define FADETOACTION_H_

#include "IntervalAction.h"

namespace SIActions
{

class FadeToAction : public IntervalAction
{
	CLASSEXTENDS(FadeToAction, IntervalAction);

public:
	FadeToAction(GameObject *obj, float destOpacity, float duration);
	FadeToAction(GameObject *obj, float destOpacity);
	FadeToAction(GameObject *obj, float destOpacity, float duration, EaseDelegate easing);
	virtual ~FadeToAction();

	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();

private:
	EaseDelegate mathDelegate;
	float startOpacity;
	float destinationOpacity;

};

}

#endif /* FADETOACTION_H_ */

#ifndef CALLBACKACTION_H_
#define CALLBACKACTION_H_

#include "BaseAction.h"

namespace SIActions
{

class CallBackAction : public BaseAction
{
	CLASSEXTENDS(CallBackAction, BaseAction);
public:
	CallBackAction(GameObject* obj, String callBackID);
	virtual ~CallBackAction();

	virtual void onStart();
	virtual bool isCompleted();
	virtual void update(float dt);

private:
	CallBackFunction toCall;
	String id;
};

}

#endif /* CALLBACKACTION_H_ */

#include "EaseFunctions.h"
#include <math.h>
#include "SIEngine/Geometry/Mathematics.h"

namespace SIActions
{

/// <summary>
/// Easing equation function for a simple linear tweening, with no easing.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::linear( double t, double b, double c, double d )
{
	return c * t / d + b;
}

/// <summary>
/// Easing equation function for an exponential (2^t) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::expoEaseOut( double t, double b, double c, double d )
{
	return ( t == d ) ? b + c : c * ( -pow( 2, -10 * t / d ) + 1 ) + b;
}

/// <summary>
/// Easing equation function for an exponential (2^t) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::expoEaseIn( double t, double b, double c, double d )
{
	return ( t == 0 ) ? b : c * pow( 2, 10 * ( t / d - 1 ) ) + b;
}

/// <summary>
/// Easing equation function for an exponential (2^t) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::expoEaseInOut( double t, double b, double c, double d )
{
	if ( t == 0 )
		return b;

	if ( t == d )
	return b + c;

	if ( ( t /= d / 2 ) < 1 )
	return c / 2 * pow( 2, 10 * ( t - 1 ) ) + b;

	return c / 2 * ( -pow( 2, -10 * --t ) + 2 ) + b;
}

/// <summary>
/// Easing equation function for an exponential (2^t) easing out/in:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::expoEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return expoEaseOut( t * 2, b, c / 2, d );

	return expoEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}

/// <summary>
/// Easing equation function for a circular (sqrt(1-t^2)) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::circEaseOut( double t, double b, double c, double d )
{
	t=t/d-1;
	return c * sqrt(1 - t*t) + b;
}

/// <summary>
/// Easing equation function for a circular (sqrt(1-t^2)) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::circEaseIn( double t, double b, double c, double d )
{
	t /= d;
	return -c * ( sqrt( 1 - ( t ) * t ) - 1 ) + b;
}

/// <summary>
/// Easing equation function for a circular (sqrt(1-t^2)) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::circEaseInOut( double t, double b, double c, double d )
{
	if ( ( t /= d / 2 ) < 1 )
		return -c / 2 * ( sqrt( 1 - t * t ) - 1 ) + b;

	t-=2;
	return c / 2 * ( sqrt( 1 - ( t ) * t ) + 1 ) + b;
}

/// <summary>
/// Easing equation function for a circular (sqrt(1-t^2)) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::circEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return circEaseOut( t * 2, b, c / 2, d );
	return circEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}


/// <summary>
/// Easing equation function for a quadratic (t^2) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quadEaseOut( double t, double b, double c, double d )
{
	t /= d;
	return -c * ( t ) * ( t - 2 ) + b;
}


/// <summary>
/// Easing equation function for a quadratic (t^2) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quadEaseIn( double t, double b, double c, double d )
{
	t /= d;
	return c * ( t ) * t + b;
}

/// <summary>
/// Easing equation function for a quadratic (t^2) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quadEaseInOut( double t, double b, double c, double d )
{
	if ( ( t /= d / 2 ) < 1 )
	return c / 2 * t * t + b;
	--t;
	return -c / 2 * ( ( t ) * ( t - 2 ) - 1 ) + b;
}

/// <summary>
/// Easing equation function for a quadratic (t^2) easing out/in:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quadEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return quadEaseOut( t * 2, b, c / 2, d );

	return quadEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}

/// <summary>
/// Easing equation function for a sinusoidal (sin(t)) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::sineEaseOut( double t, double b, double c, double d )
{
	return c * sin( t / d * ( M_PI / 2 ) ) + b;
}

/// <summary>
/// Easing equation function for a sinusoidal (sin(t)) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::sineEaseIn( double t, double b, double c, double d )
{
return -c * cos( t / d * ( M_PI / 2 ) ) + c + b;
}

/// <summary>
/// Easing equation function for a sinusoidal (sin(t)) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::sineEaseInOut( double t, double b, double c, double d )
{

	if ( ( t /= d / 2 ) < 1 )
	return c / 2 * ( sin( M_PI * t / 2 ) ) + b;
	return -c / 2 * ( cos( M_PI * --t / 2 ) - 2 ) + b;
}

/// <summary>
/// Easing equation function for a sinusoidal (sin(t)) easing in/out:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::sineEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return sineEaseOut( t * 2, b, c / 2, d );
	return sineEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}

/// <summary>
/// Easing equation function for a cubic (t^3) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::cubicEaseOut( double t, double b, double c, double d )
{
	t = t / d - 1;
	return c * ( ( t ) * t * t + 1 ) + b;
}

/// <summary>
/// Easing equation function for a cubic (t^3) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::cubicEaseIn( double t, double b, double c, double d )
{
	t /= d;
	return c * ( t ) * t * t + b;
}

/// <summary>
/// Easing equation function for a cubic (t^3) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::cubicEaseInOut( double t, double b, double c, double d )
{
	if ( ( t /= d / 2 ) < 1 )
	return c / 2 * t * t * t + b;

	t -= 2;
	return c / 2 * ( ( t ) * t * t + 2 ) + b;
}

/// <summary>
/// Easing equation function for a cubic (t^3) easing out/in:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::cubicEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return cubicEaseOut( t * 2, b, c / 2, d );

	return cubicEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}

/// <summary>
/// Easing equation function for a quartic (t^4) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quartEaseOut( double t, double b, double c, double d )
{
	t = t / d - 1;
	return -c * ( ( t ) * t * t * t - 1 ) + b;
}

/// <summary>
/// Easing equation function for a quartic (t^4) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quartEaseIn( double t, double b, double c, double d )
{
	t /= d;
	return c * ( t ) * t * t * t + b;
}

/// <summary>
/// Easing equation function for a quartic (t^4) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quartEaseInOut( double t, double b, double c, double d )
{
	if ( ( t /= d / 2 ) < 1 )
	return c / 2 * t * t * t * t + b;
	t -= 2;
	return -c / 2 * ( ( t ) * t * t * t - 2 ) + b;
}

/// <summary>
/// Easing equation function for a quartic (t^4) easing out/in:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quartEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return quartEaseOut( t * 2, b, c / 2, d );

	return quartEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}

/// <summary>
/// Easing equation function for a quintic (t^5) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quintEaseOut( double t, double b, double c, double d )
{
	t = t / d - 1;
	return c * ( ( t ) * t * t * t * t + 1 ) + b;
}

/// <summary>
/// Easing equation function for a quintic (t^5) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quintEaseIn( double t, double b, double c, double d )
{
	t /= d ;
	return c * t * t * t * t * t + b;
}

/// <summary>
/// Easing equation function for a quintic (t^5) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quintEaseInOut( double t, double b, double c, double d )
{
	if ( ( t /= d / 2 ) < 1 )
	return c / 2 * t * t * t * t * t + b;
	t -= 2;
	return c / 2 * ( t * t * t * t * t + 2 ) + b;
}

/// <summary>
/// Easing equation function for a quintic (t^5) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::quintEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return quintEaseOut( t * 2, b, c / 2, d );
	return quintEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}

/// <summary>
/// Easing equation function for an elastic (exponentially decaying sine wave) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::elasticEaseOut( double t, double b, double c, double d )
{
	if ( ( t /= d ) == 1 )
	return b + c;

	double p = d * .3;
	double s = p / 4;

	return ( c * pow( 2, -10 * t ) * sin( ( t * d - s ) * ( 2 * M_PI ) / p ) + c + b );
}

/// <summary>
/// Easing equation function for an elastic (exponentially decaying sine wave) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::elasticEaseIn( double t, double b, double c, double d )
{
	if ( ( t /= d ) == 1 )
	return b + c;

	double p = d * .3;
	double s = p / 4;

	t -= 1;
	return -( c * pow( 2, 10 * t ) * sin( ( t * d - s ) * ( 2 * M_PI ) / p ) ) + b;
}

/// <summary>
/// Easing equation function for an elastic (exponentially decaying sine wave) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::elasticEaseInOut( double t, double b, double c, double d )
{
	if ( ( t /= d / 2 ) == 2 )
	return b + c;

	double p = d * ( .3 * 1.5 );
	double s = p / 4;
	t-= 1;

	if ( t < 1 )
	{ return -.5 * ( c * pow( 2, 10 * t ) * sin( ( t * d - s ) * ( 2 * M_PI ) / p ) ) + b; }

	return c * pow( 2, -10 * t ) * sin( ( t * d - s ) * ( 2 * M_PI ) / p ) * .5 + c + b;
}

/// <summary>
/// Easing equation function for an elastic (exponentially decaying sine wave) easing out/in:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::elasticEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
		return elasticEaseOut( t * 2, b, c / 2, d );
	return elasticEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}


/// <summary>
/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::bounceEaseOut( double t, double b, double c, double d )
{
	if ( ( t /= d ) < ( 1 / 2.75 ) )
		return c * ( 7.5625 * t * t ) + b;
	else if ( t < ( 2 / 2.75 ) )
	{
		 t -= ( 1.5 / 2.75 );
		return c * ( 7.5625 * t * t + .75 ) + b;
	}
	else if ( t < ( 2.5 / 2.75 ) )
	{
		t -= ( 2.25 / 2.75 );
		return c * ( 7.5625 * t * t + .9375 ) + b;
	}

	t -= ( 2.625 / 2.75 );
	return c * ( 7.5625 * t * t + .984375 ) + b;
}

/// <summary>
/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::bounceEaseIn( double t, double b, double c, double d )
{
return c - bounceEaseOut( d - t, 0, c, d ) + b;
}

/// <summary>
/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::bounceEaseInOut( double t, double b, double c, double d )
{
if ( t < d / 2 )
return bounceEaseIn( t * 2, 0, c, d ) * .5 + b;
else
return bounceEaseOut( t * 2 - d, 0, c, d ) * .5 + c * .5 + b;
}

/// <summary>
/// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::bounceEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return bounceEaseOut( t * 2, b, c / 2, d );
	return bounceEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}

/// <summary>
/// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out:
/// decelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::backEaseOut( double t, double b, double c, double d )
{
	t = t / d - 1;
	return c * ( t * t * ( ( 1.70158 + 1 ) * t + 1.70158 ) + 1 ) + b;
}

/// <summary>
/// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in:
/// accelerating from zero velocity.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::backEaseIn( double t, double b, double c, double d )
{
	t /= d;
	return c * t * t * ( ( 1.70158 + 1 ) * t - 1.70158 ) + b;
}

/// <summary>
/// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in/out:
/// acceleration until halfway, then deceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::backEaseInOut( double t, double b, double c, double d )
{
	double s = 1.70158;
	s *= 1.525;
	if ( ( t /= d / 2 ) < 1 )
	{
	   return c / 2 * ( t * t * ( ( s + 1 ) * t - s ) ) + b;
	}
	t -= 2;
	return c / 2 * ( t * t * ( ( s + 1 ) * t + s ) + 2 ) + b;
}

/// <summary>
/// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out/in:
/// deceleration until halfway, then acceleration.
/// </summary>
/// <param name="t">Current time in seconds.</param>
/// <param name="b">Starting value.</param>
/// <param name="c">Final value.</param>
/// <param name="d">Duration of animation.</param>
/// <returns>The correct value.</returns>
double EaseFunctions::backEaseOutIn( double t, double b, double c, double d )
{
	if ( t < d / 2 )
	return backEaseOut( t * 2, b, c / 2, d );
	return backEaseIn( ( t * 2 ) - d, b + c / 2, c / 2, d );
}


}

#ifndef KillActionACTION_H_
#define KillActionYACTION_H_

#include "BaseAction.h"
namespace SIActions
{

class KillAction : public BaseAction
{
	CLASSEXTENDS(KillAction, BaseAction);
		ADD_TO_CLASS_MAP;
public:
	KillAction(GameObject* obj);
	virtual ~KillAction();

	virtual void onStart();
	virtual bool isCompleted();
	virtual void update(float dt);
};

}
#endif /* CALLBACKACTION_H_ */

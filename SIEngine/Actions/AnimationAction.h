
#ifndef ANIMATIONACTION_H_
#define ANIMATIONACTION_H_

#include "BaseAction.h"
namespace SIActions
{

class AnimationAction : public BaseAction
{
	CLASSEXTENDS(AnimationAction, BaseAction);
	ADD_TO_CLASS_MAP;
public:
	AnimationAction(SpriteObject* obj, String animationName);
	virtual ~AnimationAction();

	virtual void onStart();
	virtual bool isCompleted();
	virtual void update(float dt);

private:
	String animation;
};

}

#endif


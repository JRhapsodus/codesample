#include "AnimationAction.h"

namespace SIActions
{

AnimationAction::AnimationAction(SpriteObject* obj, String animationName) : super(obj)
{
	animation = animationName;
}


AnimationAction::~AnimationAction()
{ }

void AnimationAction::onStart()
{
	((SpriteObject*)GameObj)->playAnimation(animation);
	super::onStart();
}

bool AnimationAction::isCompleted()
{ return true; }

void AnimationAction::update(float dt)
{ }

}

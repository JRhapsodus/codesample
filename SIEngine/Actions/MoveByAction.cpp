
#include "MoveByAction.h"
#include "BaseAction.h"
#include <stdio.h>

namespace SIActions
{

MoveByAction::MoveByAction(GameObject * obj, Vec3f delta, float duration, EaseDelegate easing) : super(obj, duration)
{
	mathDelegate = easing;
	delta_ = delta;
}

MoveByAction::MoveByAction(GameObject * obj, Vec3f delta, float duration) : super(obj, duration)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	delta_ = delta;
}

MoveByAction::MoveByAction(GameObject * obj, Vec3f delta) : super(obj, 0)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	delta_ = delta;
}

MoveByAction::~MoveByAction() {

}

void MoveByAction::update(float dt)
{
	super::update(dt);
	float fraction = timeElapsed / timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;
	fraction = (float)mathDelegate( fraction, 0, 1, 1);
	GameObj->setPosition( Vec3f::lerp( startPosition, destinationPosition, fraction) );
}

void MoveByAction::onComplete()
{
	GameObj->setPosition( destinationPosition );
	super::onComplete();
}

void MoveByAction::onStart()
{
	destinationPosition = GameObj->getPosition() + delta_;
	startPosition = GameObj->getPosition();
	super::onStart();
}

}

#ifndef INCLUDE_ROTATETOACTION_H_
#define INCLUDE_ROTATETOACTION_H_

#include "IntervalAction.h"
#include "EaseFunctions.h"

namespace SIActions
{

class RotateToAction : public IntervalAction
{
	CLASSEXTENDS(RotateToAction,IntervalAction);
	ADD_TO_CLASS_MAP;	
public:

	RotateToAction(GameObject * obj, Quaternion q, float duration);
	RotateToAction(GameObject * obj, Vec3f rotation, float duration, EaseDelegate easing);
	RotateToAction(GameObject * obj, Vec3f rotation, float duration);
	RotateToAction(GameObject * obj, Vec3f rotation);
	~RotateToAction();

	void update(float dt);
	void onComplete();
	void onStart();

private:
	bool usesQuaternions;
	EaseDelegate mathDelegate;
	Vec3f startRotation;
	Vec3f destinationRotation;
	Quaternion startingQuaternion;
	Quaternion destinationRotationQuaternion;
};

}
#endif

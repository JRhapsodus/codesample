#ifndef INCLUDE_PLAYSOUNDACTION_H_
#define INCLUDE_PLAYSOUNDACTION_H_

#include "BaseAction.h"

namespace SIActions
{
class PlaySoundAction : public BaseAction
{
	CLASSEXTENDS(PlaySoundAction, BaseAction);
	ADD_TO_CLASS_MAP;
public:
	PlaySoundAction(GameObject* obj, String sound);
	virtual ~PlaySoundAction();

	virtual void onStart();
	virtual bool isCompleted();
	virtual void update(float dt);
private:
	String soundID;
};


}
#endif

#include "ChangeLabelAction.h"

namespace SIActions
{

ChangeLabelAction::ChangeLabelAction(TextLabel* obj, String text) : super(obj)
{
	newText = text;
}

ChangeLabelAction::~ChangeLabelAction()
{ }

void ChangeLabelAction::onStart()
{
	((TextLabel*)GameObj)->setText(newText);
	super::onStart();
}

bool ChangeLabelAction::isCompleted()
{ return true; }

void ChangeLabelAction::update(float dt)
{ }

}

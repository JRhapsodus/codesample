#ifndef SPRITEANIMATIONACTION_H_
#define SPRITEANIMATIONACTION_H_

#include "BaseAction.h"

namespace SIActions
{

class SpriteAnimationAction: public BaseAction
{
	CLASSEXTENDS(SpriteAnimationAction,BaseAction);
public:
	SpriteAnimationAction(GameObject *object, int startFrame, int endFrame, int fps);
	virtual ~SpriteAnimationAction();

	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();
	virtual bool isCompleted();

private:
	int startFrame;
	int endFrame;
	int fps;

	float timeElapsed;
	bool loop;
};

}
#endif /* SPRITEANIMATIONACTION_H_ */

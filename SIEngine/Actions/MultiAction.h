#ifndef MULTIACTION_H_
#define MULTIACTION_H_

#include "BaseAction.h"

namespace SIActions
{

class MultiAction : public BaseAction
{
	CLASSEXTENDS(MultiAction,BaseAction);

public:
	//// <summary>
	//// Takes in a prefilled array of BaseActions
	//// </summary>
	//// <param name="obj">GameObject to pass into the action</param>
	//// <param name="actionList">ObjectArray of actions</param>
	MultiAction(GameObject *obj, ObjectArray* actionList);

	//// <summary>
	//// For building the action list without a prefilled array
	//// </summary>
	//// <param name="obj">GameObject to pass into the action</param>
	MultiAction(GameObject *obj);

	//// <summary>
	//// Cleanup
	//// </summary>
	~MultiAction();

	//// <summary>
	//// Adds the given action to the list of contained actions
	//// </summary>
	//// <param name="action">Action to add</param>
	void addAction(BaseAction *action);

	//// <summary>
	//// Returns true when all contained actions are complete
	//// </summary>
	virtual bool isCompleted();

private:
	//// <summary>
	//// Updates all contained actions. Called via the owning GameObject.
	//// </summary>
	//// <param name="dt">Timeslice for current frame</param>
	virtual void update(float dt);

	//// <summary>
	//// Forwarding override to tell containing actions
	//// </summary>
	virtual void onStart();


	ObjectArray* actions;
};

}
#endif /* MULTIACTION_H_ */

#include "BaseAction.h"

namespace SIActions
{

BaseAction::BaseAction( GameObject* gameObj )
{
	GameObj = gameObj;
	//lets make actions be weak references !!!! we should crash if a script is running
	//on a destroyed object.
	started = false;
}

BaseAction::~BaseAction()
{
}

bool BaseAction::isStarted()
{
	return started;
}

void BaseAction::onStart()
{
	started = true;
}

void BaseAction::onComplete()
{

	//nothing - base
}

}

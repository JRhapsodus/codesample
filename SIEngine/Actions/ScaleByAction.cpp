#include "ScaleByAction.h"
#include <stdio.h>

namespace SIActions
{

ScaleByAction::ScaleByAction(GameObject * obj, Vec3f rotation, float duration, EaseDelegate easing) : super(obj, duration)
{
	mathDelegate = easing;


}

ScaleByAction::ScaleByAction(GameObject * obj, Vec3f delta, float duration) :
	IntervalAction(obj, duration)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	delta_ = delta;
}

ScaleByAction::ScaleByAction(GameObject * obj, Vec3f delta) :
	IntervalAction(obj, 0)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	delta_ = delta;
}

ScaleByAction::~ScaleByAction() {

}

void ScaleByAction::update(float dt)
{
	super::update(dt);
	float fraction = timeElapsed / timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;
	fraction = (float)mathDelegate( fraction, 0, 1, 1);
	GameObj->setScale( Vec3f::lerp(startScale, destinationScale, fraction) );
}

void ScaleByAction::onComplete()
{
	GameObj->setScale(destinationScale);
	super::onComplete();
}

void ScaleByAction::onStart() {

	startScale = GameObj->getScale();
	destinationScale = Vec3f(startScale.x*delta_.x, startScale.y*delta_.y, startScale.z*delta_.z);
	super::onStart();
}

}

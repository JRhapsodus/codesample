#ifndef INCLUDE_MOVETOACTION_H_
#define INCLUDE_MOVETOACTION_H_

#include "IntervalAction.h"
#include "EaseFunctions.h"

namespace SIActions
{

class MoveToAction : public IntervalAction 
{
	CLASSEXTENDS(MoveToAction,IntervalAction);
	ADD_TO_CLASS_MAP;
public:
	MoveToAction(GameObject* obj, Vec3f position, float duration, EaseDelegate easing);
	MoveToAction(GameObject* obj, Vec3f position, float duration);
	MoveToAction(GameObject* obj, Vec3f position);
	~MoveToAction();

	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();
private:
	EaseDelegate mathDelegate;
	Vec3f startPosition;
	Vec3f destinationPosition;
};

}

#endif

#ifndef INCLUDE_MOVEBYACTION_H_
#define INCLUDE_MOVEBYACTION_H_

#include "IntervalAction.h"

namespace SIActions
{

class MoveByAction : public IntervalAction
{
	CLASSEXTENDS(MoveByAction,IntervalAction);
		ADD_TO_CLASS_MAP;
public:
	MoveByAction(GameObject* obj, Vec3f position, float duration, EaseDelegate easing);
	MoveByAction(GameObject *obj, Vec3f delta, float duration);
	MoveByAction(GameObject *obj, Vec3f delta);
	~MoveByAction();

	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();

private:
	EaseDelegate mathDelegate;
	Vec3f delta_;
	Vec3f startPosition;
	Vec3f destinationPosition;
};

}
#endif

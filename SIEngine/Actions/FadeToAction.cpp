
#include "FadeToAction.h"
#include "BaseAction.h"
#include <stdio.h>

namespace SIActions
{

FadeToAction::FadeToAction(GameObject * obj, float op, float duration, EaseDelegate easing) : super(obj, duration)
{
	startOpacity = obj->getOpacitySelf();
	destinationOpacity = op;
	mathDelegate = easing;
}

FadeToAction::FadeToAction(GameObject * obj, float op) : super(obj, 0)
{
	startOpacity = obj->getOpacitySelf();
	destinationOpacity = op;
	mathDelegate = &EaseFunctions::cubicEaseInOut;
}

FadeToAction::FadeToAction(GameObject * obj, float op, float duration) : super(obj, duration)
{
	startOpacity = obj->getOpacitySelf();
	destinationOpacity = op;
	mathDelegate = &EaseFunctions::cubicEaseInOut;
}

FadeToAction::~FadeToAction() {

}

void FadeToAction::update(float dt)
{
	super::update(dt);
	float fraction = timeElapsed / timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;
	fraction = (float)mathDelegate( fraction, 0, 1, 1);

	float currentOp =  startOpacity * (1.0f - fraction) + destinationOpacity * fraction;
	GameObj->setOpacity( currentOp );
}

void FadeToAction::onComplete()
{
	GameObj->setOpacity( destinationOpacity );
	super::onComplete();
}

void FadeToAction::onStart()
{
	startOpacity = GameObj->getOpacitySelf();
	super::onStart();
}

}

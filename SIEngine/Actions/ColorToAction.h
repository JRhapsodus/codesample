#ifndef INCLUDE_COLORTOACTION_H_
#define INCLUDE_COLORTOACTION_H_

#include "IntervalAction.h"
#include "SIEngine/Include/SIUtils.h"

namespace SIActions
{

class ColorToAction : public IntervalAction
{
	CLASSEXTENDS(ColorToAction,IntervalAction);
public:
	ColorToAction(TextLabel* obj, Color destColor, float duration, EaseDelegate easing);
	ColorToAction(SpriteObject* obj, Color destColor, float duration, EaseDelegate easing);
	ColorToAction(SpriteObject* obj, Color destColor, float duration);
	~ColorToAction();

	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();
private:
	EaseDelegate mathDelegate;
	Color startColor;
	Color destinationColor;
};

}
#endif

#ifndef INCLUDE_ROTATEBYACTION_H_
#define INCLUDE_ROTATEBYACTION_H_

#include "IntervalAction.h"
#include "EaseFunctions.h"
using namespace SICore;

namespace SIActions
{

class RotateByAction : public IntervalAction
{
	CLASSEXTENDS(RotateByAction,IntervalAction);
	ADD_TO_CLASS_MAP;	
public:
	RotateByAction(GameObject *obj, Quaternion delta, float duration, EaseDelegate easing);
	RotateByAction(GameObject *obj, Vec3f delta, float duration, EaseDelegate easing);
	RotateByAction(GameObject *obj, Vec3f delta, float duration);
	RotateByAction(GameObject *obj, Vec3f delta);
	~RotateByAction();


	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();
private:
	bool usedQuaternion;
	EaseDelegate mathDelegate;
	Vec3f delta_;
	Quaternion deltaQuat;
	Quaternion startQuat;
	Quaternion destQuat;
	Vec3f startRotation;
	Vec3f destinationRotation;
};

}
#endif

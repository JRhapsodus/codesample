#ifndef EASEFUNCTIONS_H_
#define EASEFUNCTIONS_H_

namespace SIActions
{

class EaseFunctions
{
public:
	static double linear( double t, double b, double c, double d );
	static double expoEaseOut( double t, double b, double c, double d );
	static double expoEaseIn( double t, double b, double c, double d );
	static double expoEaseInOut( double t, double b, double c, double d );
	static double expoEaseOutIn( double t, double b, double c, double d );
	static double circEaseOut( double t, double b, double c, double d );
	static double circEaseIn( double t, double b, double c, double d );
	static double circEaseInOut( double t, double b, double c, double d );
	static double circEaseOutIn( double t, double b, double c, double d );
	static double quadEaseOut( double t, double b, double c, double d );
	static double quadEaseIn( double t, double b, double c, double d );
	static double quadEaseInOut( double t, double b, double c, double d );
	static double quadEaseOutIn( double t, double b, double c, double d );
	static double sineEaseIn( double t, double b, double c, double d );
	static double sineEaseOut( double t, double b, double c, double d );
	static double sineEaseInOut( double t, double b, double c, double d );
	static double sineEaseOutIn( double t, double b, double c, double d );
	static double cubicEaseOut( double t, double b, double c, double d );
	static double cubicEaseIn( double t, double b, double c, double d );
	static double cubicEaseInOut( double t, double b, double c, double d );
	static double cubicEaseOutIn( double t, double b, double c, double d );
	static double quartEaseOut( double t, double b, double c, double d );
	static double quartEaseIn( double t, double b, double c, double d );
	static double quartEaseInOut( double t, double b, double c, double d );
	static double quartEaseOutIn( double t, double b, double c, double d );
	static double quintEaseOut( double t, double b, double c, double d );
	static double quintEaseIn( double t, double b, double c, double d );
	static double quintEaseInOut( double t, double b, double c, double d );
	static double quintEaseOutIn( double t, double b, double c, double d );
	static double elasticEaseOut( double t, double b, double c, double d );
	static double elasticEaseIn( double t, double b, double c, double d );
	static double elasticEaseInOut( double t, double b, double c, double d );
	static double elasticEaseOutIn( double t, double b, double c, double d );
	static double bounceEaseOut( double t, double b, double c, double d );
	static double bounceEaseIn( double t, double b, double c, double d );
	static double bounceEaseInOut( double t, double b, double c, double d );
	static double bounceEaseOutIn( double t, double b, double c, double d );
	static double backEaseOut( double t, double b, double c, double d );
	static double backEaseIn( double t, double b, double c, double d );
	static double backEaseInOut( double t, double b, double c, double d );
	static double backEaseOutIn( double t, double b, double c, double d );

private:
	EaseFunctions() {}
	~EaseFunctions() {}
};

typedef double(*EaseDelegate)(double time, double start, double end, double duration);

}

#endif

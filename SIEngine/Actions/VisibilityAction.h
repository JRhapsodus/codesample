#ifndef VISIBILITYACTION_H_
#define VISIBILITYACTION_H_

#include "BaseAction.h"
namespace SIActions
{

class VisibilityAction : public BaseAction
{
	CLASSEXTENDS(VisibilityAction, BaseAction);
	ADD_TO_CLASS_MAP;	
public:
	VisibilityAction(GameObject* obj, bool visibility);
	virtual ~VisibilityAction();

	virtual void onStart();
	virtual bool isCompleted();
	virtual void update(float dt);

private:
	bool willBeVisible;

};

}
#endif /* CALLBACKACTION_H_ */

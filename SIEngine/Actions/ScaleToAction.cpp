#include "ScaleToAction.h"

namespace SIActions
{

ScaleToAction::ScaleToAction(GameObject * obj, Vec3f scale, float duration, EaseDelegate easing) : super(obj, duration)
{
	mathDelegate = easing;
	destinationScale = scale;
}

ScaleToAction::ScaleToAction(GameObject * obj, Vec3f scale, float duration) : super(obj, duration)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	destinationScale = scale;
}

ScaleToAction::ScaleToAction(GameObject * obj, Vec3f scale) : super(obj, 0)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	destinationScale = scale;
}

ScaleToAction::~ScaleToAction()
{

}

void ScaleToAction::update(float dt)
{
	super::update(dt);
	float fraction = timeElapsed / timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;
	fraction = (float)mathDelegate( fraction, 0, 1, 1);
	GameObj->setScale( Vec3f::lerp(startScale, destinationScale, fraction) );
}

void ScaleToAction::onComplete()
{
	GameObj->setScale( destinationScale );
	super::onComplete();
}

void ScaleToAction::onStart()
{
	startScale = GameObj->getScale();
	super::onStart();

}

}

#include "SIEngine/Actions/PlaySoundAction.h"
#include "SIEngine/Actions/BaseAction.h"
#include "SIEngine/Core/StringClass.h"
#include "SIEngine/Utilities/AssetLibrary.h"
#include "Glasgow/Scenes/SceneManager.h"

using namespace Glasgow;

namespace Glasgow
{
	class SceneManager;
}

namespace SIActions
{

PlaySoundAction::PlaySoundAction(GameObject* obj, String sound) : super(obj)
{
	soundID = sound;
}

PlaySoundAction::~PlaySoundAction()
{ }

void PlaySoundAction::onStart()
{
	Scene* currentScene = SceneManager::getInstance()->getScene();
	if( currentScene != null )
	{
		printf("PlaySoundAction::onStart()\n");
		currentScene->playSound(soundID);
	}

	super::onStart();
}

bool PlaySoundAction::isCompleted()
{ return true; }

void PlaySoundAction::update(float dt)
{ }


}

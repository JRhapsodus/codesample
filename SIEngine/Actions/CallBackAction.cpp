#include "CallBackAction.h"

namespace SIActions
{

CallBackAction::CallBackAction(GameObject* obj, String callBackID) : super(obj)
{
	id 	= callBackID;
	//toCall = callback;
}

CallBackAction::~CallBackAction()
{ }

void CallBackAction::onStart()
{
	//toCall();
	GameObj->onCallBack(id);
	super::onStart();
}

bool CallBackAction::isCompleted()
{ return true; }

void CallBackAction::update(float dt)
{ }


}

#include "RotateToAction.h"
#include "BaseAction.h"

#include <stdio.h>
#include <math.h>

namespace SIActions
{

RotateToAction::RotateToAction(GameObject * obj, Quaternion q, float duration) : super(obj, duration)
{
	usesQuaternions = true;
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	destinationRotationQuaternion = q;

}

RotateToAction::RotateToAction(GameObject * obj, Vec3f angle, float duration, EaseDelegate easing) : super(obj, duration)
{
	usesQuaternions = false;
	mathDelegate = easing;
	destinationRotation = angle;
}

RotateToAction::RotateToAction(GameObject * obj, Vec3f angle, float duration) :
	super(obj, duration)
{
	usesQuaternions = false;
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	destinationRotation = angle;
}

RotateToAction::RotateToAction(GameObject * obj, Vec3f angle) :
	super(obj, 0)
{
	usesQuaternions = false;
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	destinationRotation = angle;
}

RotateToAction::~RotateToAction()
{
}

void RotateToAction::update(float dt)
{
	super::update(dt);
	if( usesQuaternions )
	{
		float fraction = timeElapsed / timeToTake;
		fraction = (fraction<0)?0:fraction;
		fraction = (fraction>1)?1:fraction;
		fraction = (float)mathDelegate( fraction, 0, 1, 1);
		GameObj->setRotation(Quaternion::nlerp(startingQuaternion, destinationRotationQuaternion, fraction, true));
	}
	else
	{
		//Bad bad bad bad !!! Try to avoid Euler angels!!!!
		float fraction = timeElapsed / timeToTake;
		fraction = (fraction<0)?0:fraction;
		fraction = (fraction>1)?1:fraction;
		fraction = (float)mathDelegate( fraction, 0, 1, 1);
		Vec3f result = Vec3f::lerp(startRotation, destinationRotation, fraction);
		GameObj->setRotation( result );
	}
}


void RotateToAction::onComplete()
{
	if( usesQuaternions )
	{ GameObj->setRotation(destinationRotationQuaternion); }
	else
	{ GameObj->setRotation( destinationRotation ); }

	super::onComplete();
}

void RotateToAction::onStart()
{
	startingQuaternion = GameObj->getRotation();
	startRotation = GameObj->getEulerRotation();
	super::onStart();
}

}

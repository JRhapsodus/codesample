#include "ButtonStateAction.h"

namespace SIActions
{

ButtonStateAction::ButtonStateAction(ButtonObject* obj,ButtonState newState) : super(obj)
{
	state = newState;
}

ButtonStateAction::~ButtonStateAction()
{ }

void ButtonStateAction::onStart()
{
	((ButtonObject*)GameObj)->setState(state);
	super::onStart();
}

bool ButtonStateAction::isCompleted()
{ return true; }

void ButtonStateAction::update(float dt)
{ }

}

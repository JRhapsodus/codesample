#ifndef INCLUDE_SCALETOACTION_H_
#define INCLUDE_SCALETOACTION_H_

#include "IntervalAction.h"
#include "EaseFunctions.h"

namespace SIActions
{

class ScaleToAction : public IntervalAction
{
	CLASSEXTENDS(ScaleToAction,IntervalAction);
	ADD_TO_CLASS_MAP;	
public:
	ScaleToAction(GameObject* obj, Vec3f position, float duration, EaseDelegate easing);
	ScaleToAction(GameObject* obj, Vec3f scale, float duration);
	ScaleToAction(GameObject * obj, Vec3f scale);
	~ScaleToAction();

	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();
private:
	EaseDelegate mathDelegate;
	Vec3f startScale;
	Vec3f destinationScale;
};

}
#endif

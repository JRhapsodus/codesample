#ifndef INCLUDE_SCALEBYACTION_H_
#define INCLUDE_SCALEBYACTION_H_

#include "IntervalAction.h"
#include "EaseFunctions.h"

namespace SIActions
{


class ScaleByAction : public IntervalAction
{
	CLASSEXTENDS(ScaleByAction,IntervalAction);
	ADD_TO_CLASS_MAP;	
public:
	ScaleByAction(GameObject* obj, Vec3f position, float duration, EaseDelegate easing);
	ScaleByAction(GameObject * obj, Vec3f delta, float duration);
	ScaleByAction(GameObject * obj, Vec3f delta);
	~ScaleByAction();

	virtual void update(float dt);
	virtual void onComplete();
	virtual void onStart();

private:
	EaseDelegate mathDelegate;
	Vec3f delta_;
	Vec3f startScale;
	Vec3f destinationScale;
};

}
#endif

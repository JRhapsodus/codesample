#ifndef TIMEINTERVALACTION_H_
#define TIMEINTERVALACTION_H_

#include "BaseAction.h"
#include "EaseFunctions.h"
using namespace SICore;

namespace SIActions
{

class IntervalAction : public BaseAction
{
	CLASSEXTENDS(IntervalAction,BaseAction);
public:
	IntervalAction(GameObject* obj, float timeToTake);
	virtual ~IntervalAction();

	virtual bool isStarted();
	virtual bool isCompleted();
	virtual void onComplete();
	virtual void onStart();

	void update(float dt);
	void reset();

	float timeElapsed;
	float timeToTake;
};

}

#endif /* TIMEINTERVALACTION_H_ */

#include "KillAction.h"

namespace SIActions
{

KillAction::KillAction(GameObject* obj) : super(obj)
{
}

KillAction::~KillAction()
{ }

void KillAction::onStart()
{
	GameObj->removeFromParent();
	super::onStart();
}

bool KillAction::isCompleted()
{ return true; }

void KillAction::update(float dt)
{ }

}

#include "VisibilityAction.h"

namespace SIActions
{

VisibilityAction::VisibilityAction(GameObject* obj, bool visibility) : super(obj)
{
	willBeVisible = visibility;
}

VisibilityAction::~VisibilityAction()
{ }

void VisibilityAction::onStart()
{
	GameObj->setVisible(willBeVisible);
	super::onStart();
}

bool VisibilityAction::isCompleted()
{ return true; }

void VisibilityAction::update(float dt)
{ }

}

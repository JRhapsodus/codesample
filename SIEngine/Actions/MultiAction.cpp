#include "MultiAction.h"

namespace SIActions
{

//************************************************************************************
// Life Cycle
//************************************************************************************
MultiAction::MultiAction(GameObject *obj) : BaseAction(obj)
{
	actions = new ObjectArray();
}

MultiAction::MultiAction(GameObject *obj, ObjectArray* actionList) : BaseAction(obj)
{
	actions = actionList;
	// If ownership is being transferred it is the owners responsibility to release after MultiAction::construct
	actions->addRef();
}

MultiAction::~MultiAction()
{
	actions->release();
}

void MultiAction::addAction(BaseAction *action)
{
	actions->addElement(action);
}

//************************************************************************************
// When we start tell all of our actions they need to start, note: there is no need
// to listen for on complete notification as the children will be updated' and will
// naturally hit this point themselves
//************************************************************************************
void MultiAction::onStart()
{
	super::onStart();
	int n = actions->getSize();
	for (int i = 0;i < n;i++)
	{
		BaseAction *action = (BaseAction *)actions->elementAt(i);
		action->onStart();
	}
}

//************************************************************************************
// We finish when all of our actions have finished
//************************************************************************************
bool MultiAction::isCompleted()
{
	int n = actions->getSize();
	for (int i = 0;i < n;i++)
	{
		BaseAction *action = (BaseAction *)actions->elementAt(i);
		if (action->isCompleted() == false) {
			return false;
		}
	}
	return true;
}

//************************************************************************************
// Multi action, just loop through our actions and update
//************************************************************************************
void MultiAction::update(float dt)
{
	int n = actions->getSize();
	for (int i = 0;i < n;i++)
	{
		BaseAction *action = (BaseAction *)actions->elementAt(i);
		action->update(dt);
	}
}

}

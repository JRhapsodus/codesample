#include "ColorToAction.h"

namespace SIActions
{

ColorToAction::ColorToAction(TextLabel* obj, Color dest, float duration, EaseDelegate easing) : super((GameObject*)obj, duration)
{
	mathDelegate = easing;
	destinationColor = dest;
}

ColorToAction::ColorToAction(SpriteObject* obj, Color dest, float duration, EaseDelegate easing) : super((GameObject*)obj, duration)
{
	mathDelegate = easing;
	destinationColor = dest;
}


ColorToAction::ColorToAction(SpriteObject* obj, Color dest, float duration) : super((GameObject*)obj, duration)
{
	mathDelegate = &EaseFunctions::linear;
	destinationColor = dest;
}

ColorToAction::~ColorToAction()
{

}

void ColorToAction::update(float dt)
{
	IntervalAction::update(dt);
	float fraction = timeElapsed / timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;
	fraction = (float)mathDelegate( fraction, 0, 1, 1);
	Color newCol = startColor.lerp( destinationColor, fraction);

	if( GameObj->typeOf(TextLabel::type()))
	{ ((TextLabel*)GameObj)->setTextColor( newCol ); }
	else
	{ ((SpriteObject*)GameObj)->setColor( newCol ); }
}

void ColorToAction::onComplete()
{
	if( GameObj->typeOf(TextLabel::type()))
	{ ((TextLabel*)GameObj)->setTextColor( destinationColor ); }
	else
	{ ((SpriteObject*)GameObj)->setColor( destinationColor ); }

	super::onComplete();
}

void ColorToAction::onStart()
{
	if( GameObj->typeOf(TextLabel::type()))
	{ startColor = ((TextLabel*)GameObj)->getTextColor(); }
	else
	{ startColor = ((SpriteObject*)GameObj)->getColor(); }

	super::onStart();
}

}

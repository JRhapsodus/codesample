#include "MoveToAction.h"
#include <stdio.h>

namespace SIActions
{

MoveToAction::MoveToAction(GameObject * obj, Vec3f position, float duration, EaseDelegate easing) : super(obj, duration)
{
	mathDelegate = easing;
	destinationPosition = position;
}


MoveToAction::MoveToAction(GameObject * obj, Vec3f position, float duration) :
	super(obj, duration)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	destinationPosition = position;
}

MoveToAction::MoveToAction(GameObject * obj, Vec3f position) :
	IntervalAction(obj, 0)
{
	mathDelegate = &EaseFunctions::cubicEaseInOut;
	destinationPosition = position;
}

MoveToAction::~MoveToAction()
{

}

void MoveToAction::update(float dt)
{
	IntervalAction::update(dt);
	float fraction = timeElapsed / timeToTake;
	fraction = (fraction<0)?0:fraction;
	fraction = (fraction>1)?1:fraction;
	fraction = (float)mathDelegate( fraction, 0, 1, 1);
	GameObj->setPosition( Vec3f::lerp(startPosition, destinationPosition, fraction) );
}

void MoveToAction::onComplete()
{
	GameObj->setPosition( destinationPosition );
	super::onComplete();
}

void MoveToAction::onStart()
{
	startPosition = GameObj->getPosition();
	super::onStart();
}

}

#ifndef BUTTONSTATEACTION_H_
#define BUTTONSTATEACTION_H_

#include "BaseAction.h"
#include "SIEngine/Objects/ButtonObject.h"
namespace SIActions
{

class ButtonStateAction : public BaseAction
{
	CLASSEXTENDS(ButtonStateAction, BaseAction);
public:
	ButtonStateAction(ButtonObject* obj, ButtonState newState);
	virtual ~ButtonStateAction();

	virtual void onStart();
	virtual bool isCompleted();
	virtual void update(float dt);

private:
	ButtonState state;
};

}
#endif /* CALLBACKACTION_H_ */

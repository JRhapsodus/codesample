#include "IntervalAction.h"

namespace SIActions
{

IntervalAction::IntervalAction(GameObject* obj, float toTake) : super(obj)
{
	timeElapsed = 0;
	timeToTake = toTake;
}

IntervalAction::~IntervalAction()
{


}
bool IntervalAction::isStarted()
{
	return super::isStarted();
}

void IntervalAction::onComplete()
{
	return super::onComplete();

}
void IntervalAction::onStart()
{
	return super::onStart();
}

bool IntervalAction::isCompleted()
{
	return timeElapsed>=timeToTake;
}

void IntervalAction::update(float dt)
{
	timeElapsed += dt;
}

void IntervalAction::reset()
{
	timeElapsed = 0;
}

}

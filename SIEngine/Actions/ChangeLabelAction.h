#ifndef CHANGELABELACTION_H_
#define CHANGELABELACTION_H_

#include "BaseAction.h"
namespace SIActions
{

class ChangeLabelAction : public BaseAction
{
	CLASSEXTENDS(ChangeLabelAction, BaseAction);
public:
	ChangeLabelAction(TextLabel* obj, String text);
	virtual ~ChangeLabelAction();

	virtual void onStart();
	virtual bool isCompleted();
	virtual void update(float dt);

private:
	String newText;
};

}

#endif

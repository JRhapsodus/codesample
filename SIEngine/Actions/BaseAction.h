#ifndef INCLUDE_BASEACTION_H_
#define INCLUDE_BASEACTION_H_

#include "SIEngine/Include/SIObjects.h"

namespace SIActions
{

class BaseAction : public BaseObject
{
	CLASSEXTENDS(BaseAction, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	BaseAction( GameObject* gameObj );
	~BaseAction();
	GameObject* GameObj;

	virtual bool isCompleted()=0;
	virtual void update(float dt)=0;

	///notifies - for convenience
	virtual void onComplete();
	virtual void onStart();
	virtual bool isStarted();

	bool started;
};

}

#endif /* BASEACTION_H_ */

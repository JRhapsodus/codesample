#ifndef USERS_H_
#define USERS_H_

#include "SIEngine/Core/User.h"
#include "SIEngine/Collections/ObjectArray.h"

using namespace SICore;

namespace SIManagers
{
class UsersManager : public BaseObject
{
	CLASSEXTENDS(UsersManager, BaseObject);
public:
	static UsersManager*	getInstance();

	/// <summary>
	/// Maybe the manager should not expose its own reference to its internal
	/// array, as ultimately the manager's job is to manage it and this lets others
	/// change us on the outside
	/// </summary>
	ObjectArray*	getUsers();

	/// <summary>
	/// Returns number of profiles on the system
	/// </summary>
	int 			getNumberOfUsers();

	/// <summary>
	/// Deletes and removes the user from the device
	/// </summary>
	void 			deleteUser(User *user);


	/// <summary>
	/// Deletes and removes the user from the device by name
	/// </summary>
	void 			deleteUser(String name);

	/// <summary>
	/// Internal tracking of user
	/// </summary>
	void			setCurrentUser(User *user);

	/// <summary>
	/// Returns current user
	/// </summary>
	User *			getCurrentUser();

	/// <summary>
	/// Returns
	/// </summary>
	User* 			getUser(String userName);

	/// <summary>
	/// Adds a new reference to the profiles
	/// </summary>
	User*			newUser( String newUserName );

	/// <summary>
	/// Case insensitive match for user name
	/// </summary>
	bool			isUserNameUnique(String userName);
private:

	/// <summary>
	/// For packagemanager to sync the user profiles when they change
	/// </summary>
	static void* syncProfilesToWebWorker(void* arg);

	/// <summary>
	/// Makes the forwarding call to PackageManager
	/// </summary>
	void			syncProfilesToWeb();

	UsersManager();
	virtual 		~UsersManager();

	bool 			usedIDS[25];
	ObjectArray* 	users;
	User*			currentUser;
};

}
#endif /* USERS_H_ */

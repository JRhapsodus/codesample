#include "ConnectionManager.h"
#include "Glasgow/Scenes/SceneManager.h"
#include "pthread.h"
#include "Glasgow/GlasgowApp.h"
#include "SIEngine/Core/Lock.h"
namespace SIManagers
{

ConnectionManager* conManager = NULL;
ConnectionManager* ConnectionManager::getInstance()
{
	if (!conManager)
	{ conManager = new ConnectionManager(); }
	return conManager;
}

ConnectionManager::ConnectionManager() : super()
{
	printf("Creating network manager and getting services\n");
	leapFrogServersResponded = false;
	passwordToUse = "";
	networkInterface = NetworkManagerFactory::createInstance();
	networkModel = new NetworkingModel();
	postConnected = false;
	postDisconnected = false;
	postFailed		= false;
	postEtherConnect = false;
	postEtherDisconnect = false;
	networksDirty = 0;
	networkToUse = null;
	availableNetworks = null;
	wifiAttempts = 0;

	// connect their signal to our slot
	connect(networkInterface, SIGNAL(technologiesChanged(const QMap<QString, NetworkTechnology*> &,const QStringList&)),
			this,             SLOT(onTechnologiesChanged(const QMap<QString, NetworkTechnology*> &,const QStringList&)));

	//fired when model responds to networkInterface->servicesChanged
	connect(networkModel, SIGNAL(networksChanged()), this, SLOT(onServicesChanged()));
	connect(networkModel, SIGNAL(userInputRequested(QVariantMap)), this, SLOT(onUserInputRequested(QVariantMap)));
	connect(networkModel, SIGNAL(browserRequested(QString)), this, SLOT(onBrowserRequested(QString)));
	connect(networkModel, SIGNAL(errorReported(const QString &)), this, SLOT(onErrorReported(const QString &)));
}

ConnectionManager::~ConnectionManager()
{
	SAFE_RELEASE(availableNetworks);
	printf("ConnectionManager::~ConnectionManager()\n");
	disconnect(networkInterface, SIGNAL(technologiesChanged(const QMap<QString, NetworkTechnology*> &,const QStringList&)),
			this,             SLOT(onTechnologiesChanged(const QMap<QString, NetworkTechnology*> &,const QStringList&)));
	disconnect(networkModel, SIGNAL(networksChanged()), this, SLOT(onServicesChanged()));
	disconnect(networkModel, SIGNAL(userInputRequested(QVariantMap)), this, SLOT(onUserInputRequested(QVariantMap)));
	disconnect(networkModel, SIGNAL(browserRequested(QString)), this, SLOT(onBrowserRequested(QString)));
	disconnect(networkModel, SIGNAL(errorReported(const QString &)), this, SLOT(onErrorReported(const QString &)));
	delete networkModel;
	NetworkManagerFactory::destroyInstance();
}

///<summary>
/// Lets periodically find out if we can hit LeapFrog servers for better error casing in UI
///</summary>
void* ConnectionManager::powerOfGreySkull(void* arg)
{ GlasgowApp::appInstance->checkLeapFrogServers(); }

///<summary>
/// Different than onConnected in the regards that this is a check to see if
/// our required technologies driver is loaded
///</summary>
bool ConnectionManager::isReady()
{
	NetworkTechnology* ethernet = networkInterface->getTechnology("ethernet");
	NetworkTechnology* wifi = networkInterface->getTechnology("wifi");

	if ( ethernet != null )
	{
		bool connected = ethernet->connected() > 0 ? true : false;
		if( connected )
		{
			printf("ethernet\n");
			return true;
		}
	}

	if ( wifi != null )
	{
		bool connected = wifi->connected() > 0 ? true : false;
		if( connected )
		{
			printf("wifi\n");
			return true;
		}
	}

	return false;
}

bool ConnectionManager::isOnline()
{
	if(!isReady())
		return false;

	SYNCHRONIZED();
	if(networkToUse != null)
	{
		bool online = networkToUse->isOnline();
		return online;
	}

	return false;
}

///<summary>
/// Called when a wifi based networkService wants a passphrase or password
///</summary>
void ConnectionManager::onUserInputRequested(QVariantMap fields)
{
	printf("ConnectionManager::onUserInputRequested [%u]\n", pthread_self());
	QString responseKey;

	//Only 1 field assume it is the passphrase field
	if( fields.size() == 1 )
	{ responseKey = fields.begin().key(); }
	else
	{
		//There are multiple fields, lets look for PassPhrase or Password, else we should throw exception
		for(QVariantMap::const_iterator iter = fields.begin(); iter != fields.end(); ++iter)
		{
			printf("[%s] => [%s]\n", DEBUG_QSTRING(iter.key()), DEBUG_QSTRING(iter.value().toString()));

			String keyName = iter.key().toStdString().c_str();
			if( keyName.equalsIgnoreCase("PASSPHRASE") || keyName.equalsIgnoreCase("PASSWORD") )
			{
				//How to distinguish the name of the key for the password ?
				responseKey = iter.key();
				break;
			}
		}
	}

	QVariantMap responseMap;
	responseMap.insert(responseKey, passwordToUse.str());
	printf("Passing responseKey[%s] as [%s]\n", responseKey.toStdString().c_str(), passwordToUse.str() );
	networkModel->sendUserReply(responseMap);
}

///<summary>
/// Called when a wifi based networkService requests a browser (Starbucks) scenario
///</summary>
void ConnectionManager::onBrowserRequested(QString url)
{
	printf("ConnectionManager::onBrowserRequested [%s]\n", DEBUG_QSTRING(url));

	Scene* currentScene = SceneManager::getInstance()->getScene();
	if( currentScene != NULL )
	{
		String networkName = networkToUse != null ? networkToUse->getName() : pass1;
		currentScene->onNetworkError(networkName, "Browser Required");
	    if( networkToUse != null )
	    { networkToUse->forgetNetwork(); }
	}
}

///<summary>
/// Fired by any error triggered by the networkModel in connman
///</summary>
void ConnectionManager::onErrorReported(const QString &error)
{
	printf("!!!!!!!!!!!!!!!!! ConnectionManager::onErrorReported [%s]\n", DEBUG_QSTRING(error));
	//********************************************************************
	// We must stop the wifi time out timer since we got a reply, normally
	// the driver would consider the timeout as an error and call us here
	//********************************************************************
	stopWifiTimeout();

	//********************************************************************
	// Check if there is a scene, we may be loaded before the scene arrives
	// and be receiving messages prior to the scene
	//********************************************************************
	Scene* currentScene = SceneManager::getInstance()->getScene();
	if( currentScene != NULL )
	{
		String networkName = networkToUse != null ? networkToUse->getName() : pass1;
		String errorString = error.toStdString().c_str();

		//********************************************************************
		// Connman errors are ugly so we will over ride the commons
		//********************************************************************
		if( errorString.equalsIgnoreCase("invalid key") || errorString.equalsIgnoreCase("invalid-key") )
		{ errorString = "Could not connect or invalid password. Please try again."; }
		else if( errorString.equalsIgnoreCase("connection timed out") )
		{ errorString = "Authentication error or connection timed out."; }

		//********************************************************************
		// Sometimes connman just errors out on a low level, sometimes we
		// will want to give it a few more tries, before concluding it is
		// a legitimate error
		//********************************************************************
		if( wifiAttempts >= 3 )
		{
			wifiAttempts = 0;

			//********************************************************************
			//  Dispatch the error to the scene for handling
			//********************************************************************
			currentScene->onNetworkError(networkName, errorString);

			//********************************************************************
			// Forget the network since it has problems connecting
			//********************************************************************
			if( networkToUse != null )
			{ networkToUse->forgetNetwork(); }
		}
		else
		{ retryWifiConnection(); }
	}
}


///<summary>
/// Signal that is connected by an ethernet based networkService
///</summary>
void ConnectionManager::onEthernetChanged(const bool &powered)
{
	printf("ConnectionManager::onEthernetChanged [%u]\n", pthread_self());
	Scene* currentScene = SceneManager::getInstance()->getScene();
	if( currentScene != NULL )
	{
		if( powered )
		{
			postEtherConnect = true;
		    postEtherDisconnect = false;
		}
		else
		{
			postEtherDisconnect = true;
			postEtherConnect = false;
		}
	}
}

///<summary>
/// Mostly only called when the library first loads and adds in the network technologies
///</summary>
void ConnectionManager::onTechnologiesChanged(const QMap<QString, NetworkTechnology*> &added, const QStringList &removed)
{
	printf("ConnectionManager::onTechnologiesChanged  [%u]\n", pthread_self());
	QMap<QString, NetworkTechnology*>::const_iterator iter = added.begin();

	while ( iter != added.end() )
	{
		NetworkTechnology* netTech = iter.value();

		String techName = iter.key().toStdString().c_str();
		printf("QMap[%s] == %s\n", DEBUG_QSTRING(iter.key()), DEBUG_QSTRING(iter.value()->name()));
		printf("\tNetworkTechnology: Name(%s) Type(%s) Powered(%d) Connected(%d)\n", DEBUG_QSTRING(netTech->name()), DEBUG_QSTRING(netTech->type()), (int)netTech->powered(), (int)netTech->connected() );

		if( techName == "ethernet" )
		{ connect(netTech, SIGNAL(connectedChanged(const bool &)), this, SLOT(onEthernetChanged(const bool &))); }

		iter++;
	}
}

void* ConnectionManager::scanForWifiNetworksWorker(void* arg)
{
	((ConnectionManager*)arg)->networkModel->requestScan();
	return null;

}

///<summary>
/// Makes a call to scan for networks
///</summary>
void ConnectionManager::scanForWifiNetworks()
{
	pthread_t pInvokable;
	pthread_create(&pInvokable, NULL, &ConnectionManager::scanForWifiNetworksWorker, this);

}

///<summary>
/// Called whenever the networkService list is dirtied
///</summary>
void ConnectionManager::onServicesChanged()
{

	printf("ConnectionManager::onServicesChanged [%u]\n", pthread_self());
	ObjectArray* newServices = new ObjectArray();//bRef 1

	QVector<NetworkService*> allServices = networkInterface->getServices();
	for( int i = 0; i<allServices.size(); i++ )
	{
		NetworkService* service = allServices.at(i);
		if( service != null )
		{
			NetworkInfo* networkInfo = new NetworkInfo(service);//aRef1
			newServices->addElement(networkInfo);//aRef2
			networkInfo->release();//aRef1
		}
	}


	if( availableNetworks != null )
	{
		NetworkInfo *wiredInNew = null;
		NetworkInfo *wiredInOld = null;
		for( int i = 0; i<newServices->getSize(); i++ )
		{
			NetworkInfo* netInfo = (NetworkInfo*)newServices->elementAt(i);
			if( netInfo != null && (netInfo->isEthernet() || netInfo->getName().equalsIgnoreCase("WIRED")))
			{
				wiredInNew = netInfo;
				break;
			}
		}

		for( int i = 0; i<availableNetworks->getSize(); i++ )
		{
			NetworkInfo* netInfo = (NetworkInfo*)availableNetworks->elementAt(i);
			if( netInfo != null && (netInfo->isEthernet() || netInfo->getName().equalsIgnoreCase("WIRED")))
			{
				wiredInOld = netInfo;
				break;
			}
		}

		if( wiredInNew != null )
		{
			networkToUse = wiredInNew;
			wiredInNew->connectService();
		}
	}

	//connect to the wired
	SYNCHRONIZED();

	//debugLogServices();
	networkToUse = null;
	SAFE_RELEASE(availableNetworks);//aRef0  bRef0
	availableNetworks = newServices;

	for( int i = 0; i<newServices->getSize(); i++ )
	{
		NetworkInfo* info = (NetworkInfo*)newServices->elementAt(i);
		if( info != null && info->isOnline() )
		{ networkToUse = info; }
	}
}

///<summary>
/// Returns a networkService whose type is ethernet; only ever 1
///</summary>
NetworkInfo* ConnectionManager::getEthernetInfo()
{
	SYNCHRONIZED();
	if( availableNetworks != null )
	{
		for( int i = 0 ; i<availableNetworks->getSize(); i++ )
		{
			NetworkInfo* ethernetInfo = (NetworkInfo*)availableNetworks->elementAt(i);
			if( ethernetInfo->isEthernet() )
			{ return ethernetInfo; }
		}
	}

	return null;
}
///<summary>
/// Calls requestConnect on an ethernet service, the actual worker thread
///</summary>
void* ConnectionManager::connectToEthernetWorker(void* arg)
{

	ConnectionManager* connectionManager = (ConnectionManager*)arg;
	NetworkInfo* ethernetInfo = connectionManager->getEthernetInfo();
	if( ethernetInfo != null )
	{
		connectionManager->networkToUse = ethernetInfo;

		if( !ethernetInfo->isOnline() )
		{ ethernetInfo->connectService(); }
	}
}

///<summary>
/// Connects to a named service using a password, worker thread
///</summary>
void* ConnectionManager::connectToWifiWorker(void* arg)
{
	ConnectionManager* connectionManager = (ConnectionManager*)arg;
	String networkName = connectionManager->pass1;
	String wifiPassword = connectionManager->pass2;
	connectionManager->passwordToUse = wifiPassword;

	if(connectionManager->networkInterface->isAvailable())
	{
		NetworkInfo* networkToConnect = connectionManager->getNetworkInfo(networkName);
		if( networkToConnect != null )
		{
			connectionManager->networkToUse = networkToConnect;

			if( !networkToConnect->isOnline())
			{ networkToConnect->connectService(); }
		}
	}

	return 0;
}

///<summary>
/// Calls requestConnect on an ethernet service
///</summary>
void ConnectionManager::connectToEthernet()
{
	printf("ConnectionManager::connectToEthernet [%u]\n", pthread_self());
	stopWifiTimeout();
	pthread_t pInvokable;
	pthread_create(&pInvokable, NULL, &ConnectionManager::connectToEthernetWorker, this);
}

///<summary>
/// Retry connection to wifi network
///</summary>
void ConnectionManager::retryWifiConnection()
{
	printf("ConnectionManager::retryWifiConnection()\n");
	wifiAttempts++;
	stopWifiTimeout();
	startWifiTimeout();
	pthread_t pInvokable;
	pthread_create(&pInvokable, NULL, &ConnectionManager::connectToWifiWorker, this);
}

///<summary>
/// Connects to a named service using a password
///</summary>
void ConnectionManager::connectToWifiNetwork(String networkName, String wifiPassword)
{
	printf("ConnectionManager::connectToNetwork [%s] using [%s]\n", networkName.str(), wifiPassword.str());
	startWifiTimeout();
	wifiAttempts = 0;
    pass1 = networkName;
    pass2 = wifiPassword;
	pthread_t pInvokable;
	pthread_create(&pInvokable, NULL, &ConnectionManager::connectToWifiWorker, this);
}

///<summary>
/// Worker function to do threaded calls into connman wrapper,
/// this prevents the UI from getting thread locked
///</summary>
void* ConnectionManager::disconnectFromWifiNetworkWorker(void* arg)
{
	ConnectionManager* connectionManager = (ConnectionManager*)arg;

	if(connectionManager->networkInterface->isAvailable())
	{
		NetworkInfo* networkToDisconnect = connectionManager->getNetworkInfo(connectionManager->pass1a);
		if( networkToDisconnect != null )
		{
			if( networkToDisconnect->isOnline() )
			{ networkToDisconnect->forgetNetwork(); }
		}
	}

	return 0;
}

///<summary>
/// Disconnects a named service
///</summary>
void ConnectionManager::disconnectFromWifiNetwork(String networkName)
{
	if(networkName.isEmpty())
		return;

	printf("ConnectionManager::Disconnect Network [%s]\n", networkName.str());
	stopWifiTimeout();
	pass1a = networkName;
	pthread_t pInvokable;
	pthread_create(&pInvokable, NULL, &ConnectionManager::disconnectFromWifiNetworkWorker, this);
}

///<summary>
/// Start a wifi timeout
///</summary>
void ConnectionManager::startWifiTimeout()
{
	if(TimerManager::getInstance()->getTimer("WifiConnectTimeout") == null)
	{ TimerManager::getInstance()->addTimer("WifiConnectTimeout", 30, 0 ); }
}

///<summary>
/// cancel current timeout on connect wifi
///</summary>
void ConnectionManager::stopWifiTimeout()
{
	if(TimerManager::getInstance()->getTimer("WifiConnectTimeout") != null)
	{
		printf("ConnectionManager::cancelWifiTimeout [%u]\n", pthread_self());
		TimerManager::getInstance()->cancelTimer("WifiConnectTimeout");
	}
}

///<summary>
/// Treat a time out to a wifi typed service as an error
///</summary>
void ConnectionManager::wifiTimeoutExpired()
{
	printf("ConnectionManager::wifiTimeoutExpired [%u]\n", pthread_self());
	//do not allow the retry to occur on a 30 second time out,
	// The idea being, if a network doesnt resolve in 30 seconds, it is probably gone
	// or too weak of a signal to discern its status with reliability
	wifiAttempts = 3;

	QString error = "The connection timed out";
	onErrorReported(error);

}

///<summary>
/// Returns a networkService by name, null if not there
///</summary>
NetworkInfo* ConnectionManager::getNetworkInfo(String networkName)
{
	for( int i = 0 ; i<availableNetworks->getSize(); i++ )
	{
		NetworkInfo* netInfo = (NetworkInfo*)availableNetworks->elementAt(i);

		if( netInfo->getName().equalsIgnoreCase(networkName) )
		{ return netInfo; }
	}

	return null;
}

///<summary>
/// Called fired from a wrapped NetworkInfo which listens for onStateChanged to pass a context to us
///</summary>
void ConnectionManager::onServiceStateChanged(NetworkInfo* info)
{
	printf("ConnectionManager::onServiceStateChanged  [%u]\n", pthread_self());
	info->debugLog();

	if( info->isOnline() )
	{
		stopWifiTimeout();
		if( networkToUse != info )
		{
			networkToUse = info;
			postConnected = true;
		}
		networkToDisconnect = null;
		postDisconnected = false;
		postFailed = false;
	}
	else if(info->getState().equalsIgnoreCase("FAILURE") )
	{
		networkToDisconnect = info;
		postDisconnected = false;
		postFailed = true;
	}
	else if(info->getState().equalsIgnoreCase("DISCONNECT") )
	{
		networkToDisconnect = info;
		postDisconnected = true;
		postFailed = false;
	}
}

///<summary>
/// Update loop
///</summary>
static float pingTimer = 5;
void ConnectionManager::update(float dt)
{
	if( pingTimer > 0 )
	{
		pingTimer -= dt;
		if( pingTimer <= 0 )
		{
			pingTimer = 120;
			pthread_t pInvokable;
			pthread_create(&pInvokable, NULL, &ConnectionManager::powerOfGreySkull, this);
		}
	}

	if( postEtherDisconnect )
	{
		Scene* currentScene = SceneManager::getInstance()->getScene();
		if( currentScene != NULL )
		{ currentScene->onEthernetUnplugged(); }
		postEtherDisconnect = false;
	}

	if( postEtherConnect )
	{
		Scene* currentScene = SceneManager::getInstance()->getScene();
		if( currentScene != NULL )
		{ currentScene->onEthernetPluggedIn(); }
		postEtherConnect = false;
	}

	if( postConnected && networkToUse != null)
	{
		printf("ConnectionManager postConnected [%u]\n", pthread_self());
		Scene* currentScene = SceneManager::getInstance()->getScene();

		//todo pTHRED THIS UP
		GlasgowApp::appInstance->checkLeapFrogServers();

		if( currentScene != NULL )
		{
			String networkName = "";
			SYNCHRONIZED();
			networkName = networkToUse->getName();

			if( !networkName.isEmpty() )
			{ currentScene->onNetworkConnected(networkName); }
		}

		wifiAttempts = 0;
		postConnected = false;
	}
	else if(postDisconnected && networkToDisconnect != null)
	{
		printf("ConnectionManager postDisconnected [%u]\n", pthread_self());

		Scene* currentScene = SceneManager::getInstance()->getScene();
		if( currentScene != NULL && wifiAttempts == 0 )
		{

			String networkName = "";
			SYNCHRONIZED();
			networkName = networkToDisconnect->getName();

			if( !networkName.isEmpty() )
			{ currentScene->onNetworkError(networkName, "Connection dropped or invalid password. Please try again."); }
		}

		postDisconnected = false;
	}
	else if ( postFailed && networkToDisconnect != null)
	{
		printf("ConnectionManager postFailed\n");

		Scene* currentScene = SceneManager::getInstance()->getScene();
		if( currentScene != NULL && wifiAttempts == 0 )
		{
			String networkName = "";
			SYNCHRONIZED();
			networkName = networkToDisconnect->getName();

			if( !networkName.isEmpty() )
			{ currentScene->onNetworkError(networkName, "Invalid Password"); }
		}

		postFailed = false;
	}
}

///<summary>
/// Returns if a connection is via ethernet
///</summary>
bool ConnectionManager::hasEthernet()
{
	//Lets check this the right way, since using dbus is dog slow and thread locks, not to mention
	//there are race conditions at app start. L
	String ethernetStatus = AssetLibrary::getFileContents(Constants::Paths::ethernetStatusFile);
	int nStatus = ethernetStatus.toInt();
	if( nStatus == 1 )
	{
		printf("We have ethernet due to linux carrier\n");
		return true;
	}

	NetworkTechnology* netTech = networkInterface->getTechnology("ethernet");

	if ( netTech != null )
	{
		bool connected = netTech->connected() > 0 ? true : false;
		if( connected )
			return true;
	}

	return false;
}

///<summary>
/// Convenience call for UI
///</summary>
bool ConnectionManager::isServiceOnline( String networkName )
{
	NetworkInfo* netInfo = getNetworkInfo(networkName);
	if( netInfo != null )
	{ return netInfo->isOnline(); }

	return false;
}

///<summary>
/// Returns a string of the ip address for a given network name
///</summary>
String ConnectionManager::getIp(String networkName)
{
	NetworkInfo* netInfo = getNetworkInfo(networkName);
	if ( netInfo != null )
		return netInfo->getIP();

	return "";
}

///<summary>
/// Returns whether a given network is public
///</summary>
bool ConnectionManager::isNetworkSecure(String networkName)
{
	NetworkInfo* netInfo = getNetworkInfo(networkName);
	if( netInfo != null )
		return netInfo->isSecure();
	return false;
}

///<summary>
/// Returns an array of Objects pointing to NetworkServices
///</summary>
ObjectArray* ConnectionManager::getAllNetworks()
{
	ObjectArray* services = new ObjectArray();
	services->addElementsFromArray(availableNetworks);
	return services;
}

///<summary>
/// Debug log services
///</summary>
void ConnectionManager::debugLogServices()
{
	return;
	printf("========================================================\n");
	QVector<NetworkService*> services = networkInterface->getServices();

	for( int i = 0; i<services.size(); i++ )
	{
		NetworkService* service = services.at(i);
		if( service == null )
			continue;

		String serviceName = service->name().toStdString().c_str();
		if( serviceName.length() < 2 )
			continue;

		printf("\n%s == [Type:%s State:%s Strength:%d Favorite:%d]\n",
				DEBUG_QSTRING(service->name()),
				DEBUG_QSTRING(service->type()),
				DEBUG_QSTRING(service->state()),
				service->strength(),
				(int)service->favorite() );

		QStringList securityList = service->security();
		for (int j = 0; j < securityList.size(); j++)
		{ printf("\tsecurityList(%d) [%s]\n", j, DEBUG_QSTRING(securityList.at(j)) ); }

		QStringList dnsList = service->nameservers();
		for (int j = 0; j < dnsList.size(); j++)
		{ printf("\tnameservers(%d) [%s]\n", j, DEBUG_QSTRING(dnsList.at(j)) ); }

		QStringList nameServersList = service->nameserversConfig();
		for (int j = 0; j < nameServersList.size(); j++)
		{ printf("\tnameserversConfig(%d) [%s]\n", j, DEBUG_QSTRING(nameServersList.at(j)) ); }

		QStringList domainsList = service->domains();
		for (int j = 0; j < domainsList.size(); j++)
		{ printf("\tdomains(%d) [%s]\n", j, DEBUG_QSTRING(domainsList.at(j)) ); }

		QStringList domainsConfig = service->domainsConfig();
		for (int j = 0; j < domainsConfig.size(); j++)
		{ printf("\tservice->domainsConfig(%d) [%s]\n", j, DEBUG_QSTRING(domainsConfig.at(j)) ); }
	}
	printf("========================================================\n");
}


}

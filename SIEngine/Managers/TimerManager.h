#ifndef TIMERMANAGER_H_
#define TIMERMANAGER_H_

#include "SIEngine/Collections/Hashtable.h"
#include "SIEngine/Core/Event.h"

using namespace SICore;

namespace SIManagers
{

/// <summary> 
/// Timer class
/// </summary> 
class Timer : public BaseObject
{
	CLASSEXTENDS(Timer, BaseObject);
public:
	Timer(String name, float timeToTake, int userData) : super()
	{
		remainingTime = timeToTake;
		totalTime = timeToTake;
		id = name;
		data = userData;
	}
	~Timer() {}

	String getName() { return id; }
	bool isDone() { return (remainingTime <= 0 ); }
	void update( float dt ) { remainingTime -= dt; }
	void reset() { remainingTime = totalTime; }
	float getRemainingTime() { return remainingTime; }


	int data;
private:
	String id;
	float remainingTime;
	float totalTime;
};

/// <summary> 
/// Timer event class
/// </summary> 
class TimerEvent : public Event
{
	CLASSEXTENDS(TimerEvent, Event);
	ADD_TO_CLASS_MAP;
public:
	TimerEvent(String name) : super()
	{ id = name; }
	~TimerEvent() { }

	String id;
};

///***************************************************************************

///***************************************************************************
class TimerManager : public BaseObject
{
	CLASSEXTENDS(TimerManager, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	static TimerManager*	getInstance();
	static void	deleteInstance();

	void update( float dt );
	static void addTimer( String timerName, float length, int data = 0 );
	static void cancelTimer( String timerName );
	static void resetTimer( String timerName );
	static Timer* getTimer( String timerName );

private:
	TimerManager();
	virtual ~TimerManager();
	HashTable activeTimers;
};

}
#endif /* TIMERMANAGER_H_ */

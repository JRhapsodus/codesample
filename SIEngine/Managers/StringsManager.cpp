#include "StringsManager.h"
#include "SIEngine/Include/SIUtils.h"

using namespace SICore;

namespace SIManagers
{
StringsManager *g_StringInstance = NULL;

StringsManager* StringsManager::getInstance()
{
	if (g_StringInstance == NULL)
	{
		printf("StringsManager::getInstance()\n");
		g_StringInstance = new StringsManager();
	}
	return g_StringInstance;
}

void StringsManager::deleteInstance()
{
	if(g_StringInstance != NULL)
		g_StringInstance->release();
}

StringsManager::StringsManager()
{
	printf("StringsManager::StringsManager()\n");
	localizedStrings = new ObjectArray();
	loadedFiles = new ObjectArray();
	systemLocale = SystemSettings::getSystemLocale();
	for(int i = 0; i < (int)SystemLocaleMax; ++i)
	{
		localizedStrings->addElement( TRelease<HashTable>( new HashTable()) );
	}
}

StringsManager::~StringsManager()
{
	printf("StringsManager::~StringsManager()\n");
	localizedStrings->removeAllElements();
	SAFE_RELEASE(localizedStrings);

	loadedFiles->removeAllElements();
	SAFE_RELEASE(loadedFiles);
}

///<summary>
/// load all strings for a given filename
/// will load all locales automatically
///</summary
void StringsManager::loadStrings(String fileName)
{
	if(loadedFiles->indexOf(fileName)>-1)
	{
		printf("StringsManager::loadStrings file[%s] is already loaded\n", fileName.str());
		return;
	}

	for(int i = 0; i < (int)SystemLocaleMax; ++i)
	{
		String p = SystemSettings::getLocaleCode((SystemLocale)i) + "/" + fileName;
		FILE* fp = AssetLibrary::getCSVFile(p);
		BaseObject* bo = localizedStrings->elementAt((int)i);
		HashTable* table = (HashTable*)bo;
		readInfo(fp, table);
		fclose(fp);
	}

	loadedFiles->addElement(TRelease<String>(new String(fileName)));
	printf("StringsManager::loadStrings::file {%s} loaded\n",fileName.str());
}

///<summary>
/// So StringsManager doesn't have to ask SystemSettings
///</summary
void StringsManager::changeSystemLocale( SystemLocale newLocale )
{
	systemLocale = newLocale;
}

///<summary>
/// get the string for current locale
///</summary
String StringsManager::getLocalizedString(String _ID, SystemLocale locale)
{
	String localized = "";
	HashTable* strings = getHashTable(locale);
	if(!strings->isEmpty() && strings->containsKey(_ID))
	{
		BaseObject* bo = strings->get(_ID);
		localized = String((String*)bo,499);
	}

	return localized;
}

///<summary>
/// get the string for current locale
///</summary
String StringsManager::getLocalizedString(String _ID)
{ return getLocalizedString(_ID, systemLocale); }

///<summary>
/// get the hash table for a specific locale
///</summary
HashTable* StringsManager::getHashTable(SystemLocale locale)
{
	if((int)locale >= (int)SystemLocaleMax)
		locale = SystemLocaleDefault;

	BaseObject* bo =  localizedStrings->elementAt((int)locale);
	return (HashTable*)bo;
}

//****************************************************************************************
// Reads initial meta data of struct
//****************************************************************************************
void StringsManager::readInfo( FILE* fp, HashTable* table )
{
	char fileLine[3000];

	while (fgets (fileLine, 3000, fp))
	{
		char newLine = '\n';
		String charsToRemove;
		charsToRemove.append( newLine );
		charsToRemove.append( '\"' );


		String sFileLine = fileLine;
		String key = sFileLine.substring(0,sFileLine.indexOf(";"));
		String value = sFileLine.substring(sFileLine.indexOf(";")+1,sFileLine.length()-1);
		key = key.stripString(charsToRemove);
		value = value.stripString(charsToRemove);
		value.replace("\n", String(newLine));
		value.replace(String("''"), String('\"'));

		String* newS = new String(value); //ref 1
		table->put(key,newS); //ref 2
		newS->release(); //ref 1
	}
}

}

#ifndef STRINGSMANAGER_H_
#define STRINGSMANAGER_H_

#include "SIEngine/Collections/ObjectArray.h"
#include "SIEngine/Collections/Hashtable.h"
#include "SIEngine/Managers/SystemSettings.h"

using namespace SICore;

namespace SIManagers
{
class StringsManager : public BaseObject
{
	CLASSEXTENDS(StringsManager, BaseObject);
public:
	static StringsManager*	getInstance();
	static void	deleteInstance();

	///<summary>
	/// load all strings for a given filename
	/// will load all locales automatically
	///</summary
	void loadStrings(String fileName);

	///<summary>
	/// get the string for current locale
	///</summary
	String getLocalizedString(String _ID, SystemLocale locale);

	///<summary>
	/// get the string for current locale
	///</summary
	String getLocalizedString(String _ID);

	///<summary>
	/// So StringsManager doesn't have to ask SystemSettings
	///</summary
	void changeSystemLocale( SystemLocale newLocale );

private:
	StringsManager();
	virtual 		~StringsManager();

	///<summary>
	/// get the hash table for a specific locale
	///</summary
	HashTable* getHashTable(SystemLocale locale);

	/// <summary>
	/// Parsing operations
	/// </summary>
	void readInfo( FILE* fp, HashTable* table );

	SystemLocale 	systemLocale;
	ObjectArray*	localizedStrings;
	ObjectArray*	loadedFiles;
};

}

#endif /* STRINGSMANAGER_H_ */

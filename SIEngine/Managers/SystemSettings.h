#ifndef SYSTEMSETTINGS_H_
#define SYSTEMSETTINGS_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Core/User.h"
#include "SIEngine/Utilities/Constants.h"
#include "SIEngine/Core/LaunchParameters.h"
#include "SIEngine/Core/LaunchAppState.h"
#include <SettingsCfgFile.h>
#include <SystemData.h>
#include <sys/statvfs.h>

using namespace SIUtils;
using namespace SICore;

namespace SIManagers
{

/// <summary> 
/// SystemSettings
/// This class is concerned with global system settings from the various system level .cfg files
/// </summary> 
class SystemSettings : public BaseObject
{
	CLASSEXTENDS(SystemSettings, BaseObject);
public:
	/// <summary>
	/// Helper function to round to the nearest Divisor,
	/// TODO: move to math or primitives utils
	/// </summary>
	static unsigned long				roundDiv( unsigned long num, unsigned long divisor );

	/// <summary>
	/// Returns the total amount of space on the hard drive
	/// </summary>
	static double 				getTotalSpace(double unit);

	/// <summary>
	/// Returns the amount of used hard drive space
	/// </summary>
	static double 				getAvailableSpace(double unit);

	/// <summary>
	/// Returns the amount of available hard drive space
	/// </summary>
	static double				getUsedSpace(double unit);

	/// <summary> 
	/// System Information
	/// </summary>
	static String 				getSerialNumber();

	/// <summary>
	/// Returns the firmware version
	/// </summary>
	static String 				getFirmwareVersion();

	/// <summary>
	/// Returns the email account associated with the device
	/// </summary>
	static String 				getRegistrationEmail();

	/// <summary>
	/// Returns the mac address
	/// </summary>
	static String 				getEthernetMacAddress();

	/// <summary>
	/// Returns the mac address
	/// </summary>
	static String 				getWifiMacAddress();

	/// <summary>
	/// Returns the parent token
	/// </summary>
	static	String 				getParentToken();

	/// <summary> 
	/// Pin number handling
	/// </summary> 
	static bool 				verifyPin(String pin);

	/// <summary>
	/// Pin number handling
	/// </summary>
	static bool					verifyBackdoorSequence(String sequence);

	/// <summary> 
	/// Preference getters
	/// </summary> 
	static SystemLocale 		getSystemLocale();

	/// <summary>
	/// Variable tick time modifier
	/// </summary>
	static float		 		getTimeModifier();

	/// <summary>
	/// Frame step for per frame rendering, dev debug test
	/// </summary>
	static bool 				getFrameStep();

	/// <summary>
	/// Set when end of out of box is reached
	/// </summary>
	static bool					isInitialSetupDone();

	/// <summary>
	/// Debug over ride for wand mode, used for dev testing
	/// </summary>
	static bool					getUseWand();

	/// <summary>
	/// Debug UI value for tweaking the wand follow rate to reduce jitter
	/// </summary>
	static float				getWandSensitivity();

	/// <summary>
	/// Determines whether the UI had an activation success callback
	/// </summary>
	static bool					getIsActivated();

	/// <summary>
	/// Gets the last OOBE step during setup
	/// </summary>
	static String 				getLastOOBEStep();

	/// <summary>
	/// Converts our Locale string into a system locale enum
	/// </summary>
	static SystemLocale 		getLocale( String localeString );

	/// <summary>
	/// Gets the current theme
	/// </summary>
	static String 			    getTheme();

	/// <summary>
	/// Gets wrist strap gate lock
	/// </summary>
	static bool 				getIsWristStrapDone();

	/// <summary>
	/// Returns the name of the last user set
	/// </summary>
	static String				getLastUser();

	/// <summary>
	/// Returns the name of the last packageID played
	/// </summary>
	static String				getLastGame();

	/// <summary>
	/// Sets the pin
	/// </summary>
	static void 				setPin(String newPin);

	/// <summary> 
	/// Sets system locale enum
	/// </summary> 
	static void		 			setSystemLocale(SystemLocale newLocale);

	/// <summary>
	/// Set as last operation of Out of Box
	/// </summary>
	static void					setInitialSetupDone(bool done);

	/// <summary>
	/// Sets the name of the last user selected for persistent state convenience
	/// </summary>
	static void					setLastUser(String user);

	/// <summary>
	/// API Forwading call to let game's know what the 'signed in' user is
	/// </summary>
	static void 				setCurrentPlayerSlot( tSlotID slotID );

	/// <summary>
	/// Marks the system as having gone through activation
	/// </summary>
	static void 				setIsActivated(bool activated);

	/// <summary>
	/// Sets the current OOBE step, for returning to OOBE setup if disrupted
	/// </summary>
	static void 				setOOBEStep(String stepName);

	/// <summary>
	/// Sets the current theme
	/// </summary>
	static void 				setTheme(String themeName);

	/// <summary>
	/// Gets wrist strap gate lock
	/// </summary>
	static void 				setIsWristStrapDone(bool isDone);

	/// <summary>
	/// Sets last game played
	/// </summary>
	static void 				setLastGame(String packageID);

	/// <summary>
	/// Saves the current launch parameters
	/// </summary>
	static void					saveLaunchState(LaunchAppState launchState);

	/// <summary>
	/// Saves the current launch parameters
	/// </summary>
	static LaunchAppState		loadLaunchState();

	/// <summary>
	/// Converts our system Locale enum into an appropriate en-us code
	/// returns a usable locale code
	/// </summary>
	static String	getLocaleCode(SystemLocale locale);
private:
	/// <summary>
	/// For packagemanager to sync the system files when they change
	/// </summary>

	static void	updateSystemServiceCall();
	static void*	updateSystemServiceCallWorker(void* arg);


	SystemSettings();
	virtual 			~SystemSettings();

	/// <summary>
	/// Converts our system Locale enum into an appropriate en-us code
	/// returns a locale code used by LF systems
	/// </summary>
	static String 	getLFLocaleCode( SystemLocale locale );
};

}
#endif /* SYSTEMSETTINGS_H_ */

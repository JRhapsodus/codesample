#include "TimerManager.h"
#include "Glasgow/Scenes/SceneManager.h"
using namespace Glasgow;

namespace SIManagers
{

TimerManager *g_TimerInstance = NULL;

TimerManager* TimerManager::getInstance()
{
	if (g_TimerInstance == NULL)
	{
		printf("TimerManager::getInstance()\n");
		g_TimerInstance = new TimerManager();
	}
	return g_TimerInstance;
}

void TimerManager::deleteInstance()
{
	if(g_TimerInstance != NULL)
		g_TimerInstance->release();
}

TimerManager::TimerManager() : super()
{
	printf("TimerManager::TimerManager\n");
}

TimerManager::~TimerManager()
{
	printf("TimerManager::~TimerManager, activeTimers count:%d\n", activeTimers.size());
	activeTimers.clear();
	printf("TimerManager::~TimerManager()\n");
}

void TimerManager::update( float dt )
{
	HTEnum *e = activeTimers.elements();
	while (e->hasMoreElements())
	{
		Timer* timer = (Timer*)e->nextElement();
		timer->update(dt);
		if( timer->isDone() )
		{
			TRelease<TimerEvent> timerEvent( new TimerEvent(timer->getName()));
			activeTimers.removeValue(timer);
			SceneManager::getInstance()->getScene()->onTimerExpired(timerEvent);
		}
	}
	e->release();

}

void TimerManager::addTimer( String timerName, float length, int data )
{
	printf("TimerManager::addTimer  name:%s\n", timerName.str());
	getInstance()->activeTimers[timerName] = TRelease<Timer>(new Timer(timerName, length, data));
}

void TimerManager::cancelTimer( String timerName )
{
	BaseObject *t = getInstance()->activeTimers[timerName];
	if (t != NULL) {
		getInstance()->activeTimers.removeValue(t);
	}
}

void TimerManager::resetTimer( String timerName )
{
	BaseObject *o = getInstance()->activeTimers[timerName];
	if (o != NULL) {
		Timer *t = (Timer *)o;
		t->reset();
	}
}

Timer* TimerManager::getTimer(String timerName)
{
	BaseObject *o = getInstance()->activeTimers[timerName];
	return (Timer*)o;
}

}

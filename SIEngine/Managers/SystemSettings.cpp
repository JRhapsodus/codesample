#include "SystemSettings.h"
#include <CSystemData.h>
#include "SIEngine/Include/SIUtils.h"
#include "Glasgow/Scenes/SceneManager.h"
#include <LF/BaseUtils.h>

namespace SIManagers
{

SystemSettings::SystemSettings()
{
	ThrowException::invalidApiException();
}

SystemSettings::~SystemSettings()
{ }

/// <summary>
/// Gets the last OOBE step during setup
/// </summary>
String SystemSettings::getLastOOBEStep()
{
	SystemData firmwareData;
	firmwareData.load();
	String oobe = firmwareData.currentState.toStdString().c_str();
	return oobe;
}

/// <summary>
/// Sets the current OOBE step, for returning to OOBE setup if disrupted
/// </summary>
void SystemSettings::setOOBEStep(String stepName)
{
	printf("SystemSettings::setOOBEStep to [%s]\n", stepName.str());
	SystemData firmwareData;
	firmwareData.load();
	firmwareData.currentState = stepName.str();
	firmwareData.save();
}

/// <summary>
/// For packagemanager to sync the system files when they change
/// </summary>
void SystemSettings::updateSystemServiceCall()
{
	pthread_t threadID;
	pthread_create(&threadID, null, &SystemSettings::updateSystemServiceCallWorker, null);

}


/// <summary>
/// For packagemanager to sync the system files when they change
/// </summary>
void* SystemSettings::updateSystemServiceCallWorker(void* arg)
{
	RioPkgManager* packageManager = PackageManager::Instance();
	packageManager->UpdateProfiles(false);
	return 0;
}





/// <summary>
/// Required call for Leap so games can determine the currProfile
/// </summary>
void SystemSettings::setCurrentPlayerSlot( tSlotID slotID )
{
	printf("SystemSettings::setCurrentPlayerSlot to [%d]\n", (int)slotID);
	CSystem leapSystemData;
	leapSystemData.SetCurrentSlotID(slotID);
	leapSystemData.Save();
	updateSystemServiceCall();
}

/// <summary>
/// Determines whether the UI had an activation success callback
/// </summary>
bool SystemSettings::getIsActivated()
{
	CSettingsCfgFile glasgowSystemData;
	bool isActivated = false;

	if ( glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str()) )
	{
		if( glasgowSystemData.HasKey(Constants::Parameters::keyIsActivated.str()) )
		{ isActivated = (glasgowSystemData.GetUInt(Constants::Parameters::keyIsActivated) == 0) ? false : true; }

		glasgowSystemData.Close();
	}

	String parentToken = getParentToken();
	printf("SystemSettings::getIsActivated %s, token: %s\n", isActivated ? "true" : "false", parentToken.str());

	return isActivated || (parentToken.length() > 5 );
}

/// <summary>
/// Marks the system as having gone through activation
/// </summary>
void SystemSettings::setIsActivated(bool activated)
{
	printf("SystemSettings::setIsActivated to [%s]\n", ((activated==true)?"true":"false"));
	CSettingsCfgFile glasgowSystemData;
	glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str());
	glasgowSystemData.SetUInt(Constants::Parameters::keyIsActivated, ((activated==true)?1:0) );
	glasgowSystemData.Write();
	glasgowSystemData.Close();
}

/// <summary>
/// Sets the name of the last user selected for persistent state convenience
/// </summary>
void SystemSettings::setLastUser(String lastUser)
{
	printf("SystemSettings::setLastUser to [%s]\n", lastUser.str());
	CSettingsCfgFile glasgowSystemData;
	glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str());
	glasgowSystemData.SetStr(Constants::Parameters::keyLastUser, lastUser.str() );
	glasgowSystemData.Write();
	glasgowSystemData.Close();
}

/// <summary>
/// Sets the current theme
/// </summary>
void SystemSettings::setTheme(String themeName)
{
	printf("SystemSettings::setTheme to [%s]\n", themeName.str());
	CSettingsCfgFile glasgowSystemData;
	glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str());
	glasgowSystemData.SetStr(Constants::Parameters::keyLastTheme, themeName.str() );
	glasgowSystemData.Write();
	glasgowSystemData.Close();
}

/// <summary>
/// Gets the current theme
/// </summary>
String SystemSettings::getTheme()
{
	CSettingsCfgFile glasgowSystemData;
	String lastTheme = "UnderWater";
	if ( glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str()) )
	{
		if ( glasgowSystemData.HasKey(Constants::Parameters::keyLastTheme.str()) )
		{ lastTheme = glasgowSystemData.GetStr(Constants::Parameters::keyLastTheme.str()); }

		glasgowSystemData.Close();
	}

	return lastTheme;
}

/// <summary>
/// Gets wrist strap gate lock
/// </summary>
bool SystemSettings::getIsWristStrapDone()
{
	CSettingsCfgFile glasgowSystemData;
	if ( glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str()) )
	{
		bool isComplete = false;
		if( glasgowSystemData.HasKey(Constants::Parameters::keyWristStrapComplete.str() ) )
		{
			 isComplete = (glasgowSystemData.GetUInt(Constants::Parameters::keyWristStrapComplete.str())==0) ? false : true;
		}

		glasgowSystemData.Close();
		return isComplete;
	}

	return false;
}

/// <summary>
/// Sets last game played
/// </summary>
void SystemSettings::setLastGame(String packageID)
{
	CSettingsCfgFile glasgowSystemData;
	glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str());
	glasgowSystemData.SetStr(Constants::Parameters::keyLastGame, packageID.str() );
	glasgowSystemData.Write();
	glasgowSystemData.Close();
}

/// <summary>
/// Sets wrist strap gate lock
/// </summary>
void SystemSettings::setIsWristStrapDone(bool isDone)
{
	CSettingsCfgFile glasgowSystemData;
	glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str());
	glasgowSystemData.SetUInt(Constants::Parameters::keyWristStrapComplete.str(), isDone ? 1 : 0 );
	glasgowSystemData.Write();
	glasgowSystemData.Close();
}

/// <summary>
/// Returns the name of the last packageID played
/// </summary>
String SystemSettings::getLastGame()
{
	CSettingsCfgFile glasgowSystemData;
	String lastGame = "";
	if ( glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str()) )
	{
		if( glasgowSystemData.HasKey(Constants::Parameters::keyLastGame.str() ) )
		{ lastGame = glasgowSystemData.GetStr(Constants::Parameters::keyLastGame.str()); }

		glasgowSystemData.Close();
	}

	return lastGame;
}

/// <summary>
/// Returns the name of the last user set
/// </summary>
String SystemSettings::getLastUser()
{
	CSettingsCfgFile glasgowSystemData;
	String lastUser = Constants::UI::guestProfileName;
	if ( glasgowSystemData.Open(Constants::Paths::glasgowSettingsFile.str()) )
	{
		if( glasgowSystemData.HasKey(Constants::Parameters::keyLastUser.str() ) )
		{ lastUser = glasgowSystemData.GetStr(Constants::Parameters::keyLastUser.str()); }

		glasgowSystemData.Close();
	}
	return lastUser;
}

/// <summary>
/// Debug over ride for wand mode, used for dev testing
/// </summary>
bool SystemSettings::getUseWand()
{ return false; }

/// <summary>
/// Debug UI value for tweaking the wand follow rate to reduce jitter
/// </summary>
float SystemSettings::getWandSensitivity()
{ return 0.76f; }

/// <summary>
/// Frame step for per frame rendering, dev debug test
/// </summary>
bool SystemSettings::getFrameStep()
{ return false; }

/// <summary>
/// Helper function to round to the nearest Divisor,
/// TODO: move to math or primitives utils
/// </summary>

unsigned long SystemSettings::roundDiv( unsigned long num, unsigned long divisor )
{ return (num + (divisor/2)) / divisor; }

/// <summary>
/// Returns the total amount of space on the hard drive
/// </summary>
double SystemSettings::getTotalSpace(double inUnits)
{
	return ((double)((int64_t)Packages::GetTotalSpace()/Constants::Units::UnitGigaByte));


	struct statvfs buffer;
	int ret = statvfs("/LF/Bulk/", &buffer);
	const double total = (double)(((int64_t)buffer.f_blocks * (int64_t)buffer.f_bsize)/inUnits);
	const double available = (double)(((int64_t)buffer.f_bfree * (int64_t)buffer.f_bsize)/inUnits);
	const double used = total - available;
	return total;
}


/// <summary>
/// Returns the amount of used hard drive space
/// </summary>
double SystemSettings::getAvailableSpace(double inUnits)
{
    ///Get the freespace of the install filesystem in bytes
	return ((double)((int64_t)Packages::GetFreeSpace()/Constants::Units::UnitGigaByte));


	//Test output:
	/*
	struct statvfs vfs;
	statvfs("/LF/Bulk", &vfs);
	printf("\t\tf_bsize (block size): %lu\n"
	       "\t\tf_frsize (fragment size): %lu\n"
	       "\t\tf_blocks (size of fs in f_frsize units): %lu\n"
	       "\t\tf_bfree (free blocks): %lu\n"
	       "\t\tf_bavail free blocks for unprivileged users): %lu\n"
	       "\t\tf_files (inodes): %lu\n"
	       "\t\tf_ffree (free inodes): %lu\n"
	       "\t\tf_favail (free inodes for unprivileged users): %lu\n"
	       "\t\tf_fsid (file system ID): %lu\n"
	       "\t\tf_flag (mount flags): %lu\n"
	       "\t\tf_namemax (maximum filename length)%lu\n",
	       vfs.f_bsize,
	       vfs.f_frsize,
	       vfs.f_blocks,
	       vfs.f_bfree,
	       vfs.f_bavail,
	       vfs.f_files,
	       vfs.f_ffree,
	       vfs.f_favail,
	       vfs.f_fsid,
	       vfs.f_flag,
	       vfs.f_namemax);
	 */

	struct statvfs buffer;
	int ret = statvfs("/LF/Bulk/", &buffer);
	const double available = (double)(((int64_t)buffer.f_bavail * (int64_t)buffer.f_bsize)/inUnits);
	return available;
}

/// <summary>
/// Returns the amount of used hard drive space
/// </summary>
double SystemSettings::getUsedSpace(double inUnits)
{
	return getTotalSpace(0) - getAvailableSpace(0);
}

/// <summary>
/// System Information
/// </summary>
String SystemSettings::getSerialNumber()
{
	BaseUtils* utils = BaseUtils::Instance();
	String serialNumber = utils->GetSerialNumber();
	//delete utils; //??????
	return serialNumber;
}

/// <summary>
/// Returns the firmware version
/// </summary>
String SystemSettings::getFirmwareVersion()
{
	String firmwareVersion = AssetLibrary::getFileContents(Constants::Paths::firmwareVersion);
	firmwareVersion.replace('\n', "");
	return firmwareVersion;
}

/// <summary>
/// Returns the stored parent token
/// </summary>
String SystemSettings::getParentToken()
{
	SystemData firmwareInfo;
	firmwareInfo.load();
	String parentToken = firmwareInfo.parentToken.toStdString().c_str();
	return parentToken;
}

/// <summary>
/// Returns the mac address
/// </summary>
String SystemSettings::getWifiMacAddress()
{
	FILE* f = fopen(Constants::Paths::macWifiAddressFile.str(), "r");
	String mac = "";
	if(f)
	{
		char buf[10];
		while( !feof(f) )
		{
			fread(buf,1,1,f);
			if(buf[0] != '\n')
			{ mac += String(buf[0]); }
		}

		fclose(f);
	}
	return mac;
}

/// <summary>
/// Returns the mac address
/// </summary>
String SystemSettings::getEthernetMacAddress()
{
	FILE* f = fopen(Constants::Paths::macEthernetAddressFile.str(), "r");
	String mac = "";
	if(f)
	{
		char buf[10];
		while( !feof(f) )
		{
			fread(buf,1,1,f);
			if(buf[0] != '\n')
			{ mac += String(buf[0]); }
		}

		fclose(f);
	}
	return mac;
}

/// <summary>
/// Returns the email account associated with the device
/// </summary>
String SystemSettings::getRegistrationEmail()
{
	CSystem leapSystemData;
	String email = leapSystemData.GetParentEmail();
	return email;
}

/// <summary>
/// Pin number handling
/// </summary>
bool SystemSettings::verifyPin(String pin_)
{
	CSystem leapSystemData;
	String pin = leapSystemData.GetParentPIN();
	return pin.compareTo(pin_) == 0;
}

/// <summary>
/// Pin number handling
/// </summary>
bool SystemSettings::verifyBackdoorSequence(String sequence)
{
	bool backDoorMatch = false;
	if( sequence.equals(Constants::System::backDoorGateCode) )
	{
		backDoorMatch = true;
	}
	else if ( sequence.equals(Constants::System::backDoorUnlockCode) )
	{
		backDoorMatch = true;
		setIsActivated(true);
		SceneManager::getInstance()->getScene()->playSound("Base_AvatarSelectionStars");
	}

	return backDoorMatch;
}

/// <summary>
/// Sets the pin
/// </summary>
void SystemSettings::setPin(String newPin)
{
	printf("SystemSettings::setPin to [%s]\n", newPin.str());
	CSystem leapSystemData;
	CString cPin = newPin.str();
	leapSystemData.SetParentPIN(cPin);
	leapSystemData.Save();
	updateSystemServiceCall();
}

/// <summary>
/// Variable tick time modifier
/// </summary>
float SystemSettings::getTimeModifier()
{ return 1.0f; }


/// <summary>
/// Set when end of out of box is reached
/// </summary>
bool SystemSettings::isInitialSetupDone()
{
	SystemData firmwareData;
	firmwareData.load();
	String isDone = firmwareData.initialSetup.toStdString().c_str();
	return isDone.equalsIgnoreCase("COMPLETE");
}

/// <summary>
/// Preference getters
/// </summary>
SystemLocale SystemSettings::getSystemLocale()
{
	CSystem leapSystemData;
	SystemLocale realLocale = getLocale(leapSystemData.GetRealLocale());
	SystemLocale locale = getLocale(leapSystemData.GetLocale());
	printf("SystemSettings::getSystemLocale real: %d vs modified: %d\n", realLocale, locale);
	return realLocale;
}

/// <summary>
/// Sets system locale enum
/// </summary>
void SystemSettings::setSystemLocale(SystemLocale newLocale)
{
	CSystem leapSystemData;
	CString cStringLocale = getLFLocaleCode(newLocale).str();
	printf("SystemSettings::setSystemLocale cStringLocale: %s\n", getLFLocaleCode(newLocale).str());
	leapSystemData.SetLocale(cStringLocale);
	leapSystemData.Save();
	updateSystemServiceCall();
}

/// <summary>
/// Set as last operation of Out of Box
/// </summary>
void SystemSettings::setInitialSetupDone(bool done)
{
	SystemData firmwareData;

	firmwareData.load();
	if( done )
	{ firmwareData.initialSetup = "complete"; }
	else
	{ firmwareData.initialSetup = "no"; }
	firmwareData.save();
}

/// <summary>
/// Converts our Locale string into a system locale enum
/// </summary>
SystemLocale SystemSettings::getLocale( String localeString )
{
	if (localeString == "en-us" )
	{ return SystemLocaleUnitedStates; }
	else if (localeString == "en-gb")
	{ return SystemLocaleUnitedKingdom; }
	else if (localeString == "en-ca")
	{ return SystemLocaleCanada; }
	else if (localeString == "en-au")
	{ return SystemLocaleAustralia; }
	else if (localeString == "en-nz")
	{ return SystemLocaleNewZealand; }
	else if (localeString == "en-ie")
	{ return SystemLocaleIreland; }
	else if ( localeString.isEmpty())
	{ return SystemLocaleUnitedStates; }

	return SystemLocaleDefault;
}

/// <summary>
/// Saves the current launch parameters
/// </summary>
void SystemSettings::saveLaunchState(LaunchAppState launchParameters)
{
	CSettingsCfgFile glasgowSystemData;
	CSettingsCfgFile glasgowLaunchData;

	//Write out what this app wants the next one to do
	String launchParamsPath = Constants::Paths::LFBulk + String((int)launchParameters.launchParameters.to) + String("launchParams");
	glasgowLaunchData.Open(launchParamsPath.str());
	glasgowLaunchData.SetUInt(Constants::Parameters::keyLaunchFrom.str(), (int)launchParameters.launchParameters.from);
	glasgowLaunchData.SetUInt(Constants::Parameters::keyLaunchTo.str(), (int)launchParameters.launchParameters.to);
	glasgowLaunchData.SetUInt(Constants::Parameters::keyLaunchState.str(), (int)launchParameters.launchParameters.loadToState);
	glasgowLaunchData.Write();
	glasgowLaunchData.Close();

	//Write out the reason this app is leaving
	String appExitStatePath = Constants::Paths::LFBulk + Constants::System::appName + "exitState";
	glasgowSystemData.Open(appExitStatePath.str());
	glasgowSystemData.SetUInt(Constants::Parameters::keyExitAppState.str(), launchParameters.stateAtLastExit);
	glasgowSystemData.SetUInt(Constants::Parameters::keyExitReason.str(), launchParameters.exitReason);
	glasgowSystemData.SetStr(Constants::Parameters::keyData1.str(), launchParameters.data1.str());
	glasgowSystemData.SetStr(Constants::Parameters::keyData2.str(), launchParameters.data2.str());
	glasgowSystemData.Write();
	glasgowSystemData.Close();
}

/// <summary>
/// Saves the current launch parameters
/// </summary>
LaunchAppState SystemSettings::loadLaunchState()
{
	LaunchAppState lastAppState = LaunchAppState();
	LaunchParameters launchParameters = LaunchParameters();
	CSettingsCfgFile appLastStateFile;
	CSettingsCfgFile appLaunchParamsFile;

	//Load our last state exit
	String appLastExitStatePath = Constants::Paths::LFBulk + Constants::System::appName + String("exitState");
	if( appLastStateFile.Open(appLastExitStatePath.str()) )
	{
		lastAppState.exitReason 	 = (ExitReason)appLastStateFile.GetUInt(Constants::Parameters::keyExitReason.str());
		lastAppState.stateAtLastExit = (AppState)appLastStateFile.GetUInt(Constants::Parameters::keyExitAppState.str());
		lastAppState.data1 			 = appLastStateFile.GetStr(Constants::Parameters::keyData1.str());
		lastAppState.data2 			 = appLastStateFile.GetStr(Constants::Parameters::keyData2.str());
		appLastStateFile.Close();
	}

	// Load our requested launch parameters
	String launchParamsPath = Constants::Paths::LFBulk + String((int)Constants::System::appID) + String("launchParams");
	if( appLaunchParamsFile.Open(launchParamsPath.str()) )
	{
		launchParameters.from 			= (AppID)appLaunchParamsFile.GetUInt(Constants::Parameters::keyLaunchFrom);
		launchParameters.to 			= (AppID)appLaunchParamsFile.GetUInt(Constants::Parameters::keyLaunchTo);
		launchParameters.loadToState = (AppState)appLaunchParamsFile.GetUInt(Constants::Parameters::keyLaunchState);
		appLaunchParamsFile.Close();
	}

	lastAppState.launchParameters = launchParameters;
	return lastAppState;
}


/// <summary>
/// Converts our system Locale enum into an appropriate en-us code
/// </summary>
String SystemSettings::getLFLocaleCode( SystemLocale locale_ )
{
	//*******************************************************************************
	// XX - YY   Where XX is a language code:
	//		en => english, fr => french it => italian
	//			 Where YY is a country code
	//*******************************************************************************
	if (locale_ == SystemLocaleUnitedStates)
	{ return "en-us"; }
	else if (locale_ == SystemLocaleUnitedKingdom)
	{ return "en-gb"; }
	else if (locale_ == SystemLocaleCanada)
	{ return "en-ca"; }
	else if (locale_ == SystemLocaleAustralia)
	{ return "en-au"; }
	else if (locale_ == SystemLocaleNewZealand)
	{ return "en-nz"; }
	else if (locale_ == SystemLocaleIreland)
	{ return "en-ie"; }

	return "en-other";
}

/// <summary>
/// Converts our system Locale enum into an appropriate en-us code
/// </summary>
String SystemSettings::getLocaleCode( SystemLocale locale_ )
{
	//*******************************************************************************
	// XX - YY   Where XX is a language code:
	//		en => english, fr => french it => italian
	//			 Where YY is a country code
	//*******************************************************************************
	if (locale_ == SystemLocaleUnitedStates)
	{ return "en-us"; }
	else if (locale_ == SystemLocaleUnitedKingdom)
	{ return "en-gb"; }
	else if (locale_ == SystemLocaleCanada)
	{ return "en-ca"; }
	else if (locale_ == SystemLocaleAustralia)
	{ return "en-au"; }
	else if (locale_ == SystemLocaleNewZealand)
	{ return "en-nz"; }
	else if (locale_ == SystemLocaleIreland)
	{ return "en-ie"; }

	return "en-us";
}

}

#include "ContentManager.h"
#include "SIEngine/Core/Exception.h"
#include "SIEngine/Include/SIScenes.h"
#include "SIEngine/Include/SIUtils.h"
#include <AppIcon.h>

namespace SIManagers
{
ContentManager* contentManager = NULL;
ContentManager* ContentManager::getInstance()
{
	if (!contentManager)
	{ contentManager = new ContentManager(); }
	return contentManager;
}

ContentManager::ContentManager() : super()
{
	printf("ContentManager::ContentManager()\n");
	RioPkgManager* managerInstance = PackageManager::Instance();
    if (!connect(managerInstance, SIGNAL(RemoveFinished(const QString &)), this, SLOT(onRemoveFinished(const QString &))))
    	ThrowException::exception("Could Not Connect Signal RemoveFinished");
    if (!connect(managerInstance, SIGNAL(PreviewIconDownloaded(const QString &, const QString &)), this, SLOT(onPreviewIconDownloaded(const QString &, const QString &))))
    	ThrowException::exception("Could Not Connect Signal PreviewIconDownloaded");
    if (!connect(managerInstance, SIGNAL(PendingPackagesChanged(const QStringList &)), this, SLOT(onPendingPackagesChanged(const QStringList &))))
    	ThrowException::exception("Could Not Connect Signal PendingPackagesChanged");
    if (!connect(managerInstance, SIGNAL(NeedFirwmareUpdate(const QStringList &)), this, SLOT(onNeedFirwmareUpdate(const QStringList &))))
    	ThrowException::exception("Could Not Connect Signal NeedFirwmareUpdate");
    if (!connect(managerInstance, SIGNAL(LicenseError(const QString &, const QString &)), this, SLOT(onLicenseError(const QString &, const QString &))))
    	ThrowException::exception("Could Not Connect Signal LicenseError");
    if (!connect(managerInstance, SIGNAL(InstallStarted(const QString &)), this, SLOT(onInstallStarted(const QString &))))
    	ThrowException::exception("Could Not Connect Signal InstallStarted");
    if (!connect(managerInstance, SIGNAL(InstallFinished(const QString &)), this, SLOT(onInstallFinished(const QString &))))
    	ThrowException::exception("Could Not Connect Signal InstallFinished");
    if (!connect(managerInstance, SIGNAL(InstallError(const QString &, const QString &)), this, SLOT(onInstallError(const QString &, const QString &))))
    	ThrowException::exception("Could Not Connect Signal InstallError");
    if (!connect(managerInstance, SIGNAL(FreespaceError(qulonglong,qulonglong,const QStringList &)), this, SLOT(onFreespaceError(qulonglong,qulonglong,const QStringList &))))
    	ThrowException::exception("Could Not Connect Signal FreespaceError");
    if (!connect(managerInstance, SIGNAL(FirmwareXMLParsed(bool)), this, SLOT(onFirmwareXMLParsed(bool))))
    	ThrowException::exception("Could Not Connect Signal FirmwareXMLParsed");
    if (!connect(managerInstance, SIGNAL(EndpointURLsUpdated()), this, SLOT(onEndpointURLsUpdated())))
    	ThrowException::exception("Could Not Connect Signal EndpointURLsUpdated");
    if (!connect(managerInstance, SIGNAL(DownloadStarted(const QString &)), this, SLOT(onDownloadStarted(const QString &))))
    	ThrowException::exception("Could Not Connect Signal DownloadStarted");
    if (!connect(managerInstance, SIGNAL(DownloadProgress(const QString &, int)), this, SLOT(onDownloadProgress(const QString &, int))))
    	ThrowException::exception("Could Not Connect Signal DownloadProgress");
    if (!connect(managerInstance, SIGNAL(DownloadPaused(const QString &)), this, SLOT(onDownloadPaused(const QString &))))
    	ThrowException::exception("Could Not Connect Signal DownloadPaused");
    if (!connect(managerInstance, SIGNAL(DownloadFinished(const QString &)), this, SLOT(onDownloadFinished(const QString &))))
    	ThrowException::exception("Could Not Connect Signal DownloadFinished");
    if (!connect(managerInstance, SIGNAL(DownloadError(const QString &, const QString &)), this, SLOT(onDownloadError(const QString &, const QString &))))
    	ThrowException::exception("Could Not Connect Signal DownloadError");
    if (!connect(managerInstance, SIGNAL(DeviceInventoryCheckFinished()), this, SLOT(onDeviceInventoryCheckFinished())))
    	ThrowException::exception("Could Not Connect Signal DeviceInventoryCheckFinished");

    lastFirmWareStatus = FirmwareNotAvailabe;
    installedPackages = new ObjectArray();
    downloadingPackages = new ObjectArray();
    purchasedPackages = new ObjectArray();
    Packages::RunQuery();
    fillInstalledPackages();
    fillPurchasablePackages();
    fillDownloadingPackages();
}

/// <summary>
/// Fills our array with PackageInfo references
/// </summary>
void ContentManager::fillInstalledPackages()
{
	printf("ContentManager::fillInstalledPackages()\n");
	installedPackages->removeAllElements();
	QVector<PackageData*> orderedList;
	if ( Packages::GetInstalledPackages(orderedList) )
	{
		for ( int i = 0; i < orderedList.size(); i++ )
		{
			PackageData* pData = orderedList[i];
			AppIconLoader loader(NULL,false);
			loader.init(pData);
			loader.loadData();

			installedPackages->addElement( TRelease<PackageInfo>( new PackageInfo(pData) ) );
			((PackageInfo*)installedPackages->elementAt(i))->debugLog();
		}
	}
}

/// <summary>
/// Called whenever a packageData changes its status
/// </summary>
void ContentManager::onPackageChanged( tPackageStatus newStatus, PackageInfo* info )
{ SceneManager::getInstance()->getScene()->onPackageChanged(newStatus, info); }

///<summary>
/// Tells system to pause all
///</summary>
void ContentManager::pauseDownloads()
{ Packages::PauseActivity(); }

///<summary>
/// Request to Leap API to add the package to the download queue
///</summary>
void ContentManager::requestDownload(PackageInfo *info)
{
	if( info != null )
	{ Packages::RequestPackage(info->packageID.str()); }
}

///<summary>
/// Tells system to resume downloads
///</summary>
void ContentManager::resumeDownloads()
{ Packages::ResumeActivity(); }

///<summary>
/// Returns true if the system is currently paused
///</summary>
bool ContentManager::isDownloadsPaused()
{ return Packages::ActivityIsPaused(); }

///<summary>
/// Returns true if there are active downloads
///</summary>
bool ContentManager::hasDownloads()
{
	SYNCHRONIZED();
	return downloadingPackages->getSize() > 0;
}

/// <summary>
/// Fills our array with PackageInfo references
/// </summary>
void ContentManager::fillDownloadingPackages()
{
	printf("ContentManager::fillDownloadingPackages()\n");
	downloadingPackages->removeAllElements();
	QVector<PackageData*> orderedList;
	if ( Packages::GetInProgressPackages(orderedList) )
	{
		for ( int i = 0; i < orderedList.size(); i++ )
		{
			PackageData* pData = orderedList[i];
			AppIconLoader loader(NULL,false);
			loader.init(pData);
			loader.loadData();
			downloadingPackages->addElement( TRelease<PackageInfo>( new PackageInfo(pData) ) );
			((PackageInfo*)downloadingPackages->elementAt(i))->debugLog();
		}
	}
}

/// <summary>
/// Fills our array with PackageInfo references
/// </summary>
void ContentManager::fillPurchasablePackages()
{
	printf("ContentManager::fillPurchasablePackages()\n");
	purchasedPackages->removeAllElements();
	QVector<PackageData*> orderedList;
	if ( Packages::GetRequestablePackages(orderedList) )
	{
		for ( int i = 0; i < orderedList.size(); i++ )
		{
			PackageData* pData = orderedList[i];
			AppIconLoader loader(NULL,false);
			loader.init(pData);
			loader.loadData();
			purchasedPackages->addElement( TRelease<PackageInfo>( new PackageInfo(pData) ) );
			((PackageInfo*)purchasedPackages->elementAt(i))->debugLog();
		}
	}
}



/// <summary>
/// For updating this singeltons state
/// </summary>
static float checkTimer = 1;
static bool reqGate = false;
void ContentManager::update(float dt)
{
	if( checkTimer > 0 )
	{
		checkTimer -= dt;
		if( checkTimer <= 0 )
		{
			checkTimer = 3;
			if( !reqGate )
			{
				firmware_available = PackageManager::Instance()->CheckFirmwareUpgradeReady();
				reqGate = true;
			}
		}
	}

	if( reqGate )
	{
		if( firmware_available.isFinished() )
		{
			reqGate = false;
			if( firmware_available.isValid())
			{
				if( firmware_available.isError())
				{ lastFirmWareStatus = FirmwareNotAvailabe; }
				else
				{
					//Two cases here: pending, or ready
					if( firmware_available.value() == true )
					{ lastFirmWareStatus = FirmwareReady; }
					else
					{
						if( getDownloadsRemaining() > 0 )
						{ lastFirmWareStatus = FirmwarePending; }
						else
						{ lastFirmWareStatus = FirmwareNotAvailabe; }
					}
				}
			}
			else
			{ lastFirmWareStatus = FirmwareNotAvailabe; }
		}
		else
		{ /*waiting*/ }
	}
}

ContentManager::~ContentManager()
{
	printf("ContentManager::~ContentManager()\n");
	RioPkgManager* managerInstance = PackageManager::Instance();
    disconnect(managerInstance, SIGNAL(RemoveFinished(const QString &)), this, SLOT(onRemoveFinished(const QString &)));
    disconnect(managerInstance, SIGNAL(PreviewIconDownloaded(const QString &, const QString &)), this, SLOT(onPreviewIconDownloaded(const QString &, const QString &)));
    disconnect(managerInstance, SIGNAL(PendingPackagesChanged(const QStringList &)), this, SLOT(onPendingPackagesChanged(const QStringList &)));
    disconnect(managerInstance, SIGNAL(NeedFirwmareUpdate(const QStringList &)), this, SLOT(onNeedFirwmareUpdate(const QStringList &)));
    disconnect(managerInstance, SIGNAL(LicenseError(const QString &, const QString &)), this, SLOT(onLicenseError(const QString &, const QString &)));
    disconnect(managerInstance, SIGNAL(InstallStarted(const QString &)), this, SLOT(onInstallStarted(const QString &)));
    disconnect(managerInstance, SIGNAL(InstallFinished(const QString &)), this, SLOT(onInstallFinished(const QString &)));
    disconnect(managerInstance, SIGNAL(InstallError(const QString &, const QString &)), this, SLOT(onInstallError(const QString &, const QString &)));
    disconnect(managerInstance, SIGNAL(FreespaceError(qulonglong,qulonglong,const QStringList &)), this, SLOT(onFreespaceError(qulonglong,qulonglong,const QStringList &)));
    disconnect(managerInstance, SIGNAL(FirmwareXMLParsed(bool)), this, SLOT(onFirmwareXMLParsed(bool)));
    disconnect(managerInstance, SIGNAL(EndpointURLsUpdated()), this, SLOT(onEndpointURLsUpdated()));
    disconnect(managerInstance, SIGNAL(DownloadStarted(const QString &)), this, SLOT(onDownloadStarted(const QString &)));
    disconnect(managerInstance, SIGNAL(DownloadProgress(const QString &, int)), this, SLOT(onDownloadProgress(const QString &, int)));
    disconnect(managerInstance, SIGNAL(DownloadPaused(const QString &)), this, SLOT(onDownloadPaused(const QString &)));
    disconnect(managerInstance, SIGNAL(DownloadFinished(const QString &)), this, SLOT(onDownloadFinished(const QString &)));
    disconnect(managerInstance, SIGNAL(DownloadError(const QString &, const QString &)), this, SLOT(onDownloadError(const QString &, const QString &)));
    disconnect(managerInstance, SIGNAL(DeviceInventoryCheckFinished()), this, SLOT(onDeviceInventoryCheckFinished()));
}

/// <summary>
/// Periodic callback for when package manager checks for new packages and runs an audit
/// </summary>
void ContentManager::onDeviceInventoryCheckFinished()
{
	printf("ContentManager::onDeviceInventoryCheckFinished\n");
}

/// <summary>
/// Called when the device is updated. Primarily via a successfull activation request
/// </summary>
void ContentManager::onDeviceUpdated()
{
	printf("ContentManager::onDeviceUpdated\n");
}

/// <summary>
/// Download error
/// </summary>
void ContentManager::onDownloadError(const QString &pkgid, const QString &error_msg)
{
	printf("ContentManager::onDownloadError\n");
	if( SceneManager::getInstance()->getScene() != null )
	{  SceneManager::getInstance()->getScene()->onDownloadError();  }
}

/// <summary>
/// Download finished, though it would be better to get a PackageInfo structure with
/// an enum claiming it as a game or a firmware update as opposed to the split functions
/// for pending and installing lists
/// </summary>
void ContentManager::onDownloadFinished(const QString &pkgid)
{
	String package = pkgid.toStdString().c_str();

	if( SceneManager::getInstance()->getScene() != null )
	{ SceneManager::getInstance()->getScene()->onDonwloadFinished(package); }

	printf("ContentManager::onDownloadFinished\n");
}

/// <summary>
/// Download progressed. From what i have seen from package manager, 1 download at a time on
/// a queue where firmware packages are installed first
/// </summary>
void ContentManager::onDownloadPaused(const QString &pkgid)
{
	printf("ContentManager::onDownloadPaused\n");
}

void ContentManager::onPackageDownloadProgress(PackageInfo* info, int percent)
{


}

void ContentManager::onDownloadProgress(const QString &pkgid, int percent)
{
	String packageID = pkgid.toStdString().c_str();

	if( SceneManager::getInstance()->getScene() != null )
	{ SceneManager::getInstance()->getScene()->onDonwloadProgressed(packageID, percent); }
}

/// <summary>
/// Download started. I would expect a started call immediately after a finished call
/// </summary>
void ContentManager::onDownloadStarted(const QString &pkgid)
{
	String package = pkgid.toStdString().c_str();

	if( SceneManager::getInstance()->getScene() != null )
	{ SceneManager::getInstance()->getScene()->onDonwloadStarted(package); }

	printf("ContentManager::onDownloadStarted\n");
}

/// <summary>
/// Not really important to UI other than to track internal book keepng phases
/// </summary>
void ContentManager::onEndpointURLsUpdated()
{
	printf("ContentManager::onEndpointURLsUpdated\n");
}

/// <summary>
/// Not really important to UI other than to track internal book keepng phases
/// </summary>
void ContentManager::onFirmwareXMLParsed(bool firmware_upgrade_available)
{
	printf("ContentManager::onFirmwareXMLParsed\n");
	if( SceneManager::getInstance()->getScene() != null )
	{  SceneManager::getInstance()->getScene()->onPendingDownloadsChanged();  }
}

/// <summary>
/// Since the UI for v1 does not have any downloads this should never be hit
/// </summary>
void ContentManager::onFreespaceError(qulonglong freespace, qulonglong needed_space, const QStringList &pkg_ids)
{
	printf("ContentManager::onFreespaceError\n");
}

/// <summary>
/// Nothing much the UI needs to know about the installation process of a downloaded package
/// </summary>
void ContentManager::onInstallError(const QString &pkgid, const QString &error_msg)
{
	printf("ContentManager::onInstallError\n");
}

/// <summary>
/// Returns a PackageInfo based on ID
/// </summary>
PackageInfo* ContentManager::getPackageByID( String id )
{
	for ( int i = 0; i<installedPackages->getSize(); i++ )
	{
		PackageInfo* info = (PackageInfo*)installedPackages->elementAt(i);
		if( info->packageID == id )
		{ return info; }
	}

	for ( int i = 0; i<downloadingPackages->getSize(); i++ )
	{
		PackageInfo* info = (PackageInfo*)downloadingPackages->elementAt(i);
		if( info->packageID == id )
		{ return info; }
	}

	for ( int i = 0; i<purchasedPackages->getSize(); i++ )
	{
		PackageInfo* info = (PackageInfo*)purchasedPackages->elementAt(i);
		if( info->packageID == id )
		{ return info; }
	}

	return null;

}

/// <summary>
/// Required by the ContentManager app in Parent settings
/// </summary>
void ContentManager::onRemoveFinished(const QString &pkgid)
{
	printf("ContentManager::onRemoveFinished\n");
	String packageID = pkgid.toStdString().c_str();
	PackageInfo* info = getPackageByID(packageID);

	if( info != null )
	{ SceneManager::getInstance()->getScene()->onPackageDeleted(info); }
}

/// <summary>
/// Install finished
/// </summary>
void ContentManager::onInstallFinished(const QString &pkgid)
{
	printf("ContentManager::onInstallFinished\n");
}

/// <summary>
/// Install started
/// </summary>
void ContentManager::onInstallStarted(const QString &pkgid)
{
	printf("ContentManager::onInstallStarted\n");
}

/// <summary>
/// License error, though this is set for activation
/// </summary>
void ContentManager::onLicenseError(const QString &pkgid, const QString &error_msg)
{
	printf("ContentManager::onLicenseError\n");
}

/// <summary>
/// A call that is beyond out of box to flag the UI to display a reboot
/// </summary>
void ContentManager::onNeedFirwmareUpdate(const QStringList &for_pkg_ids)
{
	printf("ContentManager::onNeedFirwmareUpdate\n");
}

/// <summary>
/// New and exciting packages being pulled
/// </summary>
void ContentManager::onPendingPackagesChanged(const QStringList &pending_packages)
{
	fillDownloadingPackages();

	if( SceneManager::getInstance()->getScene() != null )
	{  SceneManager::getInstance()->getScene()->onPendingDownloadsChanged();  }
}

/// <summary>
/// Not used by UI at the moment
/// </summary>
void ContentManager::onPreviewIconDownloaded(const QString &pkgid, const QString &icon_path)
{
	printf("ContentManager::onPreviewIconDownloaded\n");
}

/// <summary>
/// Helper function that passes the version information back from a meta.inf file
/// </summary>
String ContentManager::getMetaInfVersion(String metaInfpath)
{
	metaInfpath.replace("meta.inf", "");
	CMetaInfFile* info = new CMetaInfFile(metaInfpath.str());
	String versionString = String(info->GetPackageVersion());
	delete info;
	return versionString;
}

///<summary>
/// Returns number of pending firmware packages still downloading
///</summary>
int ContentManager::getDownloadsRemaining()
{
	QStringList pendingFirmware = PackageManager::Instance()->GetPendingFirmware();
	return pendingFirmware.length();
}

///<summary>
/// Returns status enum for firmware states, Pending, Ready, NotAvailable
///</summary>
FirmwareStatus ContentManager::getFirmwareStatus()
{
//	printf("Last firmware status is [%d]\n", (int)lastFirmWareStatus);
	return lastFirmWareStatus;
}

///<summary>
/// Returns an array of PackageInfo*
///</summary>
ObjectArray* ContentManager::getPlayablePackages()
{
	ObjectArray* packages = getInstalledPackages();
	PackageInfo* packageInfo = ContentManager::getInstance()->getCartridgePackage();
	if ( packageInfo != null )
	{
		packageInfo->debugLog();
		packages->addElement(packageInfo);
	}

	return packages;
}

///<summary>
/// Returns an array of PackageInfo*
///</summary>
ObjectArray* ContentManager::getDownloadingPackages()
{ return downloadingPackages; }

///<summary>
/// Returns an array of PackageInfo*
///</summary>
ObjectArray* ContentManager::getPurchasablePackages()
{ return purchasedPackages; }

///<summary>
/// Returns an array of PackageInfo*
///</summary>
ObjectArray* ContentManager::getInstalledPackages()
{ return installedPackages; }

///<summary>
/// Returns a PackageInfo* referring to the cartridge
///</summary>
PackageInfo* ContentManager::getCartridgePackage()
{
	String dirName = "/LF/Cart/";
    QDir cartDir(dirName.str());
    cartDir.setFilter(QDir::NoDotAndDotDot | QDir::AllEntries);
    QStringList files = cartDir.entryList();
    String path = "";

    for(int i=0; i < files.size(); i++)
    {
        if(files[i] != "lib" && files[i] != "lost+found" )
        {
            path = files[i].toStdString().c_str();
            break;
        }
    }

    if ( path.isEmpty() )
    {
    	printf("Path is empty \n");
    	return NULL;
    }

    dirName = dirName + path + "/";
    printf("CARTRIDGE: found meta.inf at path : %s\n", dirName.str());

    PackageData* mCartData = Packages::GetCartPackage(dirName.str());
    if ( !mCartData )
    {
    	printf("mCartData is NULL \n");
    	return NULL;
    }

    // Check dependencies.
    CMetaInfo metaInfo(LeapFrog::Brio::CString(dirName.str()));
    PackageInfo* info =  new PackageInfo( mCartData );
    info->cartridgeDependencies = metaInfo.CheckCartDependencies();
    return info;
}


}

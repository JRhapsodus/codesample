#ifndef CONTENTMANAGER_H_
#define CONTENTMANAGER_H_

#include "SIEngine/Include/SIBase.h"
#include "Glasgow/Data/PackageInfo.h"

using namespace Glasgow;

///<summary>
/// Enum status for firmware
/// 	Ready 			- ready for install
///		Pending 		- still downloading
///		NotAvailable	- no frmware update needed
///</summary>
enum FirmwareStatus
{
	FirmwareUnknown,
	FirmwareReady,
	FirmwarePending,
	FirmwareNotAvailabe
};

namespace SIManagers
{

///<summary>
/// Watches for installs, downloads, cartridges, and gets information regarding leap's packageData
///</summary>
class ContentManager : public QObject, public BaseObject
{
	Q_OBJECT
	CLASSEXTENDS(ContentManager, BaseObject);
public:
	///<summary>
	/// This class interfaces through a single instance
	///</summary>
	static ContentManager* getInstance();
	virtual ~ContentManager();

	///<summary>
	/// Returns true if a firmware is needed
	///</summary>
	FirmwareStatus getFirmwareStatus();

	///<summary>
	/// Returns an array of PackageInfo*
	///</summary>
	ObjectArray* getInstalledPackages();

	///<summary>
	/// Returns an array of PackageInfo*
	///</summary>
	ObjectArray* getDownloadingPackages();

	///<summary>
	/// Returns an array of PackageInfo*
	///</summary>
	ObjectArray* getPurchasablePackages();

	///<summary>
	/// Tells system to pause all
	///</summary>
	void pauseDownloads();

	///<summary>
	/// Tells system to resume downloads
	///</summary>
	void resumeDownloads();

	///<summary>
	/// Request to Leap API to add the package to the download queue
	///</summary>
	void requestDownload(PackageInfo *);

	///<summary>
	/// Returns true if the system is currently paused
	///</summary>
	bool isDownloadsPaused();

	///<summary>
	/// Returns true if there are active downloads
	///</summary>
	bool hasDownloads();

	///<summary>
	/// Returns an array of PackageInfo structures ready for play
	///</summary>
	ObjectArray* getPlayablePackages();

	///<summary>
	/// Returns a PackageInfo* referring to the cartridge
	///</summary>
	PackageInfo* getCartridgePackage();

	///<summary>
	/// Returns number of pending firmware packages still downloading
	///</summary>
	int getDownloadsRemaining();

	/// <summary>
	/// Helper function that passes the version information back from a meta.inf file
	/// </summary>
	static String getMetaInfVersion(String metaInfpath);

	/// <summary>
	/// For updating this singeltons state
	/// </summary>
	void update(float dt);

	/// <summary>
	/// Called whenever a packageData changes its status
	/// </summary>
	void onPackageChanged( tPackageStatus newStatus, PackageInfo* info );

	/// <summary>
	/// onPackageDownloadProgress
	/// </summary>
	void onPackageDownloadProgress(PackageInfo* info, int percent);

protected slots:
	/// <summary>
	/// Periodic callback for when package manager checks for new packages and runs an audit
	/// </summary>
	void onDeviceInventoryCheckFinished();

	/// <summary>
	/// Called when the device is updated. Primarily via a successfull activation request
	/// </summary>
	void onDeviceUpdated();

	/// <summary>
	/// Download error
	/// </summary>
	void onDownloadError(const QString &pkgid, const QString &error_msg);

	/// <summary>
	/// Download finished, though it would be better to get a PackageInfo structure with
	/// an enum claiming it as a game or a firmware update as opposed to the split functions
	/// for pending and installing lists
	/// </summary>
	void onDownloadFinished(const QString &pkgid);

	/// <summary>
	/// Download finished, though it would be better to get a PackageInfo structure
	/// </summary>
	void onDownloadPaused(const QString &pkgid);

	/// <summary>
	/// Download progressed. From what i have seen from package manager, 1 download at a time on
	/// a queue where firmware packages are installed first
	/// </summary>
	void onDownloadProgress(const QString &pkgid, int percent);

	/// <summary>
	/// Download started. I would expect a started call immediately after a finished call
	/// </summary>
	void onDownloadStarted(const QString &pkgid);

	/// <summary>
	/// Not really important to UI other than to track internal book keepng phases
	/// </summary>
	void onEndpointURLsUpdated();

	/// <summary>
	/// Not really important to UI other than to track internal book keepng phases
	/// </summary>
	void onFirmwareXMLParsed(bool firmware_upgrade_available);

	/// <summary>
	/// Since the UI for v1 does not have any downloads this should never be hit
	/// </summary>
	void onFreespaceError(qulonglong freespace, qulonglong needed_space, const QStringList &pkg_ids);

	/// <summary>
	/// Nothing much the UI needs to know about the installation process of a downloaded package
	/// </summary>
	void onInstallError(const QString &pkgid, const QString &error_msg);

	/// <summary>
	/// Install finished
	/// </summary>
	void onInstallFinished(const QString &pkgid);

	/// <summary>
	/// Install started
	/// </summary>
	void onInstallStarted(const QString &pkgid);

	/// <summary>
	/// License error, though this is set for activation
	/// </summary>
	void onLicenseError(const QString &pkgid, const QString &error_msg);

	/// <summary>
	/// A call that is beyond out of box to flag the UI to display a reboot
	/// </summary>
	void onNeedFirwmareUpdate(const QStringList &for_pkg_ids);

	/// <summary>
	/// New and exciting packages being pulled
	/// </summary>
	void onPendingPackagesChanged(const QStringList &pending_packages);

	/// <summary>
	/// Not used by UI at the moment
	/// </summary>
	void onPreviewIconDownloaded(const QString &pkgid, const QString &icon_path);

	/// <summary>
	/// Required by the ContentManager app in Parent settings
	/// </summary>
	void onRemoveFinished(const QString &pkgid);

	/// <summary>
	/// Returns a PackageInfo based on ID
	/// </summary>
	PackageInfo* getPackageByID( String id );

protected:
	/// <summary>
	/// Fills our array with PackageInfo references
	/// </summary>
	void fillInstalledPackages();

	/// <summary>
	/// Fills our array with PackageInfo references
	/// </summary>
	void fillDownloadingPackages();

	/// <summary>
	/// Fills our array with PackageInfo references
	/// </summary>
	void fillPurchasablePackages();

	ObjectArray* installedPackages;
	ObjectArray* downloadingPackages;
	ObjectArray* purchasedPackages;

	QDBusPendingReply<bool> firmware_available;
	FirmwareStatus lastFirmWareStatus;
	///<summary>
	/// Wrapper class that is in charge of communication and helper functions
	///</summary>
	ContentManager();

};

}
#endif

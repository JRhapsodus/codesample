#ifndef CONNECTIONMANAGER_H_
#define CONNECTIONMANAGER_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/LFIncludes.h"
#include <syscall.h>

namespace SIManagers
{

class NetworkInfo;

///<summary>
/// ConnectionManager, manages the usage of the conman and connects to Wifi
///</summary>
class ConnectionManager : public QObject, public BaseObject
{
	Q_OBJECT
	CLASSEXTENDS(ConnectionManager, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	static ConnectionManager *getInstance();
	virtual ~ConnectionManager();

	///<summary>
	/// Lets periodically find out if we can hit LeapFrog servers for better error casing in UI
	///</summary>
	static void* powerOfGreySkull(void*);

  	///<summary>
  	/// Really we can make this a gettor,
  	///</summary>
     bool leapFrogServersResponded;

	///<summary>NetworkManager::getServices
    /// Returns an array of Objects pointing to NetworkServices
    ///</summary>
	ObjectArray* getAllNetworks();

    ///<summary>
    /// Connects to a named service using a password
    ///</summary>
    void connectToWifiNetwork(String networkName, String wifiPassword);

    ///<summary>
	/// Calls requestConnect on an ethernet service
	///</summary>
    void connectToEthernet();

    ///<summary>
	/// Disconnects from current wifi network
	///</summary>
	void disconnectFromWifiNetwork(String networkName);

    ///<summary>
    /// Start a timeout on connect wifi
	///</summary>
	void startWifiTimeout();

	///<summary>
	/// cancel current timeout on connect wifi
	///</summary>
	void stopWifiTimeout();

    ///<summary>
    /// wifi timeout finished
	///</summary>
	void wifiTimeoutExpired();

    ///<summary>
    /// Makes a call to scan for networks
    ///</summary>
    void scanForWifiNetworks();

    ///<summary>
    /// Returns a networkService by name, null if not there
    ///</summary>
    NetworkInfo* getNetworkInfo(String serviceName);

    ///<summary>
    /// Different than onConnected in the regards that this is a check to see if
    /// our required technologies driver is loaded
    ///</summary>
    bool isReady();

    ///<summary>
    /// Returns if a connection is via ethernet
    ///</summary>
    bool hasEthernet();

    ///<summary>
    /// Returns a string of the ip address for a given network name
    ///</summary>
    String getIp(String networkName);

    ///<summary>
	/// Returns whether a given network is public
	///</summary>
	bool isNetworkSecure(String networkName);

    ///<summary>
	/// Convenience call for UI
	///</summary>
	bool isServiceOnline( String networkName );

	///<summary>
	/// Convenience call for UI
	///</summary>
	bool isOnline();

    ///<summary>
	/// Update loop
	///</summary>
	void update(float dt);

    ///<summary>
    /// Called fired from a wrapped NetworkInfo which listens for onStateChanged to pass a context to us
    ///</summary>
    void onServiceStateChanged(NetworkInfo* info);

protected slots:
	///<summary>
	/// Called when a wifi based networkService wants a passphrase or password
	///</summary>
     void onUserInputRequested(QVariantMap fields);

     ///<summary>
     /// Called when a wifi based networkService requests a browser (Starbucks) scenario
     ///</summary>
     void onBrowserRequested(QString url);

     ///<summary>
     /// Fired by any error triggered by the networkModel
     ///</summary>
     void onErrorReported(const QString &error);

     ///<summary>
     /// Mostly only called when the library first loads and adds in the network technologies
     ///</summary>
     void onTechnologiesChanged(const QMap<QString, NetworkTechnology*> &added, const QStringList &removed);

     ///<summary>
     /// Signal that is connected by an ethernet based networkService
     ///</summary>
     void onEthernetChanged(const bool &powered);

     ///<summary>
     /// Called whenever the networkService list is dirtied
     ///</summary>
     void onServicesChanged();

protected:
     ///<summary>
     /// Retry connection to wifi network
     ///</summary>
     void retryWifiConnection();

     ///<summary>
     /// Returns a networkService whose type is ethernet; only ever 1
     ///</summary>
     NetworkInfo* getEthernetInfo();

     ///<summary>
     /// Worker function to do threaded calls into connman wrapper,
     /// this prevents the UI from getting thread locked
     ///</summary>
    static void* disconnectFromWifiNetworkWorker(void* arg);

    ///<summary>
    /// Worker function to do threaded calls into connman wrapper
    /// this prevents the UI from getting thread locked
    ///</summary>
    static void* connectToWifiWorker(void* arg);

    ///<summary>
    /// Worker function to do threaded calls into connman wrapper
    /// this prevents the UI from getting thread locked
    ///</summary>
    static void* connectToEthernetWorker(void* arg);

    ///<summary>
    /// Worker function to do threaded calls into connman wrapper
    /// this prevents the UI from getting thread locked
    ///</summary>
    static void* scanForWifiNetworksWorker(void* arg);

    //i hate this!
    bool postEtherConnect;
    bool postEtherDisconnect;

    bool postFailed;
    bool postConnected;
    bool postDisconnected;
    int wifiAttempts;
    bool networksDirty;
    //For passing variables into worker thread functions
    String pass1;
    String pass2;
    String pass1a;
    String passwordToUse;
    NetworkInfo* networkToUse;
    NetworkInfo* networkToDisconnect;

    ///<summary>
    /// A list of NetworkInfo* that wrap NetworkService* with their own state changed signales
    ///</summary>
    ObjectArray* availableNetworks;

    ///<summary>
    /// Used to get a list of technologies
    ///</summary>
	NetworkManager* networkInterface;

    ///<summary>
    /// LeapFrog's "wrapper"  interface into connman
    ///</summary>
	NetworkingModel* networkModel;

    ///<summary>
    /// Debug logs the list of services
	///</summary>
	void debugLogServices();

    ///<summary>
    /// Singleton pattern
    ///</summary>
    ConnectionManager();
};

///<summary>
/// TODO: this class is beyond a simple helper struct, it needs its own cpp
/// WifiConnection, helper struct that holds a reference to a Network service
///</summary>
class NetworkInfo : public QObject, public BaseObject
{
	Q_OBJECT;
	CLASSEXTENDS(NetworkInfo, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	NetworkService* service;

    ///<summary>
    /// Wrap a weak reference to NetworkService* returned from connMan in getServices
    ///</summary>
	NetworkInfo(NetworkService* src)
	{
		service = src;

		QString n = src->name();
		networkName = (n != null) ? n.toStdString().c_str() : "";
		strength = src->strength();

		connect(service, SIGNAL(stateChanged(const QString &)), this, SLOT(onStateChanged(const QString &)));
	}

    ///<summary>
    /// Debug helper functions
    ///</summary>
	void debugLog()
	{
		printf("Service [%s]\n", networkName.str());
		printf("\tState [%s]\n", getState().str());
		printf("\tTech [%s]\n", DEBUG_QSTRING(service->type()));
	}

    ///<summary>
    /// Returns true if this network represents a wifi based network
    ///</summary>
	bool isValidWifi()
	{
		if ( networkName.isEmpty() )
			return false;
		return !isEthernet();
	}

    ///<summary>
    /// Returns Name of network
    ///</summary>
	String getName()
	{ return networkName; }

    ///<summary>
    /// Returns the connman state of this network
    ///</summary>
	String getState()
	{ return service->state().toStdString().c_str(); }

    ///<summary>
    /// Returns true if this network represents an ethernet based network
    ///</summary>
	bool isEthernet()
	{
		String networkType = service->type().toStdString().c_str();
		//printf("NetworkType %s\n", networkType.str());
		return (networkType.equalsIgnoreCase("ethernet"));
	}

    ///<summary>
    /// Request to connman to ditch all knowledge of us
    ///</summary>
	void forgetNetwork()
	{
		service->requestRemove();
		service->requestDisconnect();
	}

    ///<summary>
    /// Returns true if this network is marked as a secure login requirement
    ///</summary>
	bool isSecure()
	{
		QStringList securityList = service->security();

		for(QStringList::iterator iter = securityList.begin(); iter != securityList.end(); ++iter)
		{
			QString current = *iter;
			printf("Security: %s\n", DEBUG_QSTRING(current));
			String security = current.toStdString().c_str();
			if (security.equalsIgnoreCase("none"))
				return false;
		}

		return true;
	}

    ///<summary>
    /// Wrapper call to request connman to connect by using this network
    ///</summary>
	void connectService()
	{
		service->requestConnect();
	}

    ///<summary>
    /// Should we test for online? Ask connman for our ipv4 string
    ///</summary>
	String getIP()
	{
		String ip = "";
		QVariantMap ipv4Map = service->ipv4();
		for(QVariantMap::const_iterator iter = ipv4Map.begin(); iter != ipv4Map.end(); ++iter)
		{ printf("[%s] => [%s]\n", DEBUG_QSTRING(iter.key()), DEBUG_QSTRING(iter.value().toString())); }
		QString qtIP = ipv4Map.value("Address").toString();
		ip = qtIP.toStdString().c_str();
		return ip;
	}

    ///<summary>
    /// Returns true if connman says our network state is 'online'
    ///</summary>
	bool isOnline()
	{
		String serviceState = service->state().toStdString().c_str();
		return (serviceState.equalsIgnoreCase("Online") || serviceState.equalsIgnoreCase("Ready") );
	}

	~NetworkInfo()
	{ disconnect(service, SIGNAL(stateChanged(const QString &)), this, SLOT(onStateChanged(const QString &))); }

	String networkName;
	int strength;

	protected slots:
	     void onStateChanged(const QString &state)
	     { ConnectionManager::getInstance()->onServiceStateChanged( this);}
};


}
#endif

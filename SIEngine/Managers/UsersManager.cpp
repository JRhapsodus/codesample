#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "UsersManager.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Managers/SystemSettings.h"

namespace SIManagers
{
UsersManager *usersInstance = NULL;

UsersManager::UsersManager()
{
	currentUser = NULL;
	users = new ObjectArray();

	for ( int i = 0; i<25; i++ )
	{ usedIDS[i] = false; }

	std::vector<tSlotID> profileSlots = PlayerInfo::GetSlotsWithProfiles();

	for ( std::vector<tSlotID>::size_type i = 0; i<profileSlots.size(); i++ )
	{
		tSlotID slotID = profileSlots[i];

		printf("ProfileID %d exists\n", slotID);

		if( slotID < Constants::System::maxNumUsers )
		{
			printf("ProfileID %d is valid\n", slotID);
			usedIDS[slotID] = true;
			User* user = User::loadUser(slotID);
			user->debugLog();
			users->addElement(user);
			user->release();
		}
	}
}

UsersManager::~UsersManager()
{
	printf("UsersManager::~UsersManager()\n");
	users->removeAllElements();
	SAFE_RELEASE(users);
}


UsersManager* UsersManager::getInstance()
{
	if (usersInstance == NULL)
	{
		printf("UsersManager::getInstance()\n");
		usersInstance = new UsersManager();
	}

	return usersInstance;
}

/// <summary>
/// Maybe the manager should not expose its own reference to its internal
/// array, as ultimately the manager's job is to manage it and this lets others
/// change us on the outside
/// </summary>
ObjectArray* UsersManager::getUsers()
{ return users; }

/// <summary>
/// Returns number of profiles on the system
/// </summary>
int UsersManager::getNumberOfUsers()
{ return users->getSize(); }

/// <summary>
/// Deletes and removes the user from the device by name
/// </summary>
void UsersManager::deleteUser(String name)
{
	for( int i = 0; i<users->getSize(); i++)
	{
		User* user = (User*)users->elementAt(i);
		if ( user->getDisplayName() == name )
			return deleteUser(user);
	}

	syncProfilesToWeb();
}

/// <summary>
/// Makes the forwarding call to PackageManager
/// </summary>
void UsersManager::syncProfilesToWeb()
{
	pthread_t threadID;
	pthread_create(&threadID, null, &UsersManager::syncProfilesToWebWorker, null);
}

/// <summary>
/// For packagemanager to sync the user profiles when they change
/// </summary>
void* UsersManager::syncProfilesToWebWorker(void* arg)
{
	RioPkgManager* packageManager = PackageManager::Instance();
	packageManager->UpdateProfiles(true);
}

/// <summary>
/// Deletes and removes the user from the device
/// </summary>
void UsersManager::deleteUser(User *user)
{
	user->debugLog();
	std::vector<tSlotID>::size_type index = user->getPlayerSLot();
	if(!user->removeProfile())
	{ ThrowException::exception("User profile failed deletion"); }

	usedIDS[index] = false;
	printf("UsersManager::deleteUser DELETED USER!\n");
	users->removeElement(user);//ref count 1 -> 0 will cause ::~Destruct()
	syncProfilesToWeb();

}


/// <summary>
/// Adds a new reference to the profiles
/// </summary>
User* UsersManager::newUser( String newUserName )
{
	newUserName = newUserName.trim();
	//extra logic to find available slot
	int availableSlot = -1;

	for ( int i = 0; i<25; i++ )
	{
		if( usedIDS[i] == false )
		{ availableSlot= i; break; }
	}

	//availableSlot is 0 based
	if( availableSlot >= Constants::System::maxNumUsers)
	{ ThrowException::exception("Maximum Users Reached"); }

	User* newUser = User::newUser(newUserName, availableSlot);
	usedIDS[availableSlot] = true;
	if ( newUser->save() )
	{ newUser->debugLog(); }

	users->addElement(newUser);
	syncProfilesToWeb();
	return newUser;
}

/// <summary>
/// Returns
/// </summary>
User* UsersManager::getUser(String userName )
{
	for (int i = 0; i < users->getSize(); i++)
	{
		User *user = (User *)users->elementAt(i);
		if ( user->getDisplayName().equals(userName) )
		{ return user; }
	}

	return NULL;
}

/// <summary>
/// Returns current user
/// </summary>
User* UsersManager::getCurrentUser()
{ return currentUser; }

/// <summary>
/// Internal tracking of user
/// </summary>
void UsersManager::setCurrentUser(User *user)
{
	printf("UsersManager::setCurrentUser\n");
	currentUser = user;
	//Save information for UI and Leap's current slotID
	SystemSettings::setLastUser(currentUser->getFirstName()); //Used by UI's own tracking; primary key name
	SystemSettings::setCurrentPlayerSlot(currentUser->getPlayerSLot()); //Used by leap; primary key slotID
}

bool UsersManager::isUserNameUnique(String userName)
{
	if( userName.equalsIgnoreCase(Constants::UI::guestProfileName))
		return false;

	userName = userName.toLowerCase();
	for (int i = 0; i < users->getSize(); i++)
	{
		User *user = (User *)users->elementAt(i);
		if ( user->getDisplayName().toLowerCase().equals(userName) )
		{ return false; }
	}

	return true;
}

}

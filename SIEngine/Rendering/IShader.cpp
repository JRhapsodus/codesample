#include "SIEngine/Rendering/IShader.h"
#include "SIEngine/Utilities/AssetLibrary.h"
#include "SIEngine/Core/OpenGlRenderer.h"
#include "SIEngine/Core/Exception.h"

using std::stringstream;

namespace SIRendering
{

IShader::IShader(String vertexPath, String fragmentPath ) : super()
{
	vertexShader = processShader(vertexPath, GL_VERTEX_SHADER);
	fragmentShader = processShader(fragmentPath, GL_FRAGMENT_SHADER);

	shaderHandle = glCreateProgram();
	if (shaderHandle == 0)
	{ return; }

	glAttachShader(shaderHandle, vertexShader);
	glAttachShader(shaderHandle, fragmentShader);
	glLinkProgram(shaderHandle);
	OpenGlRenderer::checkGLError();
}

GLint IShader::getUniformID( String attributeName )
{ return glGetUniformLocation(shaderHandle, attributeName.str()); }

GLint IShader::getAttributeID( String attributeName )
{ return glGetAttribLocation(shaderHandle, attributeName.str()); }

int IShader::processShader(String shaderPath, GLint shaderType)
{
	const char *strings[1] = { NULL };

	/* Create shader and load into GL. */
	int shader = glCreateShader(shaderType);
	strings[0] = loadShader(shaderPath);
	glShaderSource(shader, 1, strings, NULL);

	/* Clean up shader source. */
	free((void *) (strings[0]));
	strings[0] = NULL;

	/* Try compiling the shader. */
	glCompileShader(shader);
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	/* Dump debug info (source and log) if compilation failed. */
	if (status != GL_TRUE)
	{
		GLint length;
		char *debugSource = NULL;
		char *errorLog = NULL;

		glGetShaderiv(shader, GL_SHADER_SOURCE_LENGTH, &length);
		debugSource = (char *) malloc(length);
		glGetShaderSource(shader, length, NULL, debugSource);
		free(debugSource);

		/* Now get the info log. */
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
		errorLog = (char *) malloc(length);
		glGetShaderInfoLog(shader, length, NULL, errorLog);

		printf("Shader Compile error %s\n", errorLog);
		ThrowException::shaderNotCompiledException(shaderPath);
	}

	return shader;
}

char * IShader::loadShader(String shaderPath)
{
	FILE *file = AssetLibrary::getBinaryFile( shaderPath );

	if (file == NULL)
	{ return NULL; }

	/* Seek end of file. */
	fseek(file, 0, SEEK_END);
	long length = ftell(file);
	fseek(file, 0, SEEK_SET);
	char *shader = (char *) calloc(length + 1, sizeof(char));

	if (shader == NULL)
	{ return NULL; }

	size_t numberOfBytesRead = fread(shader, sizeof(char), length, file);
	if ( (long)numberOfBytesRead != length)
	{ return NULL; }

	shader[length] = '\0';
	fclose(file);
	return shader;
}

}

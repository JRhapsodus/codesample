#include "ShaderManager.h"
#include "SIEngine/Include/SIShaders.h"

namespace SIRendering
{

//****************************************************************************
// Load all needed shaders
//****************************************************************************
ShaderManager::ShaderManager()
{
	loadedShaders[ShaderNameTextured] = new TexturedShader();
	loadedShaders[ShaderNameTransformedColor] = new TransformedColorShader();
	loadedShaders[ShaderNameTexturedAlpha] = new TexturedAlphaShader();
	loadedShaders[ShaderNameColorOnly] = new ColorShader();
	loadedShaders[ShaderNameText] = new TextShader();
	loadedShaders[ShaderNameBlurred] = new BlurredShader();
	loadedShaders[ShaderNameLitText] = new LitTextShader();
	loadedShaders[ShaderNameTint] = new TintShader();
}

//****************************************************************************
// Return shader on mapping
//****************************************************************************
IShader* ShaderManager::getShader( ShaderName shaderName )
{
	IShader* shader = (IShader*)loadedShaders.get(shaderName);
	return shader;
}

//****************************************************************************
// Enumerate loadedShaders call release
//****************************************************************************
ShaderManager::~ShaderManager()
{ }

}

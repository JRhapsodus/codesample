#include "IMaterial.h"

namespace SIRendering
{

IMaterial::IMaterial(GameObject* go, IShader* s, IDrawMode* dm )
{
	gameObject = go;
	shader = s;
	drawMode = dm;
}

IMaterial::~IMaterial()
{ }

IShader* IMaterial::getShader()
{
	return shader;
}

IDrawMode* IMaterial::getDrawMode()
{ return drawMode; }


}

#ifndef TEXTUREMANAGER_H_
#define TEXTUREMANAGER_H_

#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Resources/RawImage.h"
#include "SIEngine/Rendering/MutableTexture.h"
#include "SIEngine/Resources/SpriteData.h"

namespace SIRendering
{

/// <summary> 
/// Probably belongs on the Texture class but is most useful here
/// </summary> 
typedef enum TextureType
{
	RAW,
	Mutable,
	PNG,
	TGA,
	JPG,
	ETC1,
	PKM
} TextureType;

/// <summary> 
/// True singelton, only point of access
/// </summary> 
class TextureManager : public BaseObject
{
	CLASSEXTENDS(TextureManager, BaseObject)
	ADD_TO_CLASS_MAP;
public:
	/// <summary>
	/// True singleton pattern
	/// </summary>
	static TextureManager* getInstance();

	/// <summary> 
	/// Returns Texture via name
	/// </summary> 
	Texture* getTexture( String textureName );

	/// <summary>
	/// Returns if the texture with the name is currently tracked
	/// </summary>
	bool hasTexture(String textureName );

	/// <summary> 
	/// This should not be here!!!!!!! Texture sheets have nothing to do with textures, which are
	/// primarily an interface for sending images to the GPU, not some 'Sprite/Atlas' feature
	/// </summary> 
	SpriteData *getSpriteData(String textureName);

	/// <summary>
	/// This should not be here!!!!!!! Texture sheets have nothing to do with textures, which are
	/// primarily an interface for sending images to the GPU, not some 'Sprite/Atlas' feature
	/// </summary>
	bool hasSpriteData(String textureName);

	/// <summary> 
	/// This should not be here!!!!!!! Texture sheets have nothing to do with textures, which are
	/// primarily an interface for sending images to the GPU, not some 'Sprite/Atlas' feature
	/// </summary> 
	void loadTextureSheet(String textureSheetName);

	/// <summary>
	/// Loads a texture, which for our uses just internally creates a PNG Image for the texture data
	/// </summary>
	void loadTexture( String textureName, TextureType type );

	/// <summary>
	/// Should create a texture that is offscreen for a rendering target
	/// </summary>
	MutableTexture* createOffScreenTexture( RawImage* offscreenImage, String textureName );

	/// <summary> 
	/// Unloads all textures from gpu
	/// </summary> 
	void unloadAllTextures();

	/// <summary>
	/// Unloads a specific texture from the gpu
	/// </summary>
	void unloadTexture(String textureName);
	/// <summary>
	/// Loads a texture using a TGA file, exposed so that some people can 'preload' textures prior to the auto load
	/// </summary>
	void loadTga( String textureName );

	/// <summary>
	/// Loads a texture using a PNG file, exposed so that some people can 'preload' textures prior to the auto load
	/// </summary>
	void loadPng( String textureName );

private:
	/// <summary>
	/// True singleton pattern
	/// </summary>
	virtual ~TextureManager();

	/// <summary>
	/// True singleton pattern
	/// </summary>
	TextureManager() { }

	HashTable loadedTextures;
	HashTable sheetSpriteData;
};

}

#endif /* TEXTUREMANAGER_H_ */

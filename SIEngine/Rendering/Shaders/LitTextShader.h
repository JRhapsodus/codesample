#ifndef LITTEXTSHADER_H
#define LITTEXTSHADER_H

#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{

class LitTextShader : public IShader
{
	CLASSEXTENDS(LitTextShader, IShader);

public:
	LitTextShader();
	LitTextShader( String vertexPath, String fragmentPath );

	virtual ~LitTextShader();

	virtual GLint getIndexBufferAttributeID();
	virtual GLint getUVBufferAttributeID();
	virtual GLint getColorBufferAttributeID();
	virtual GLint getVertexBufferAttributeID();
	virtual GLint getShaderAttributeID();
	virtual GLint getModelViewProjectionUniformID();

};

}
#endif /* LITTEXTSHADER_H */

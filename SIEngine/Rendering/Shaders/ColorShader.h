#ifndef COLORSHADER_H_
#define COLORSHADER_H_

#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{

class ColorShader : public IShader
{
	CLASSEXTENDS(ColorShader, IShader);

public:
	ColorShader();
	virtual ~ColorShader();

	virtual GLint getIndexBufferAttributeID();
	virtual GLint getUVBufferAttributeID();
	virtual GLint getColorBufferAttributeID();
	virtual GLint getVertexBufferAttributeID();
	virtual GLint getShaderAttributeID();
	virtual GLint getModelViewProjectionUniformID();
};

}
#endif /* TRANSFORMEDCOLORSHADER_H_ */

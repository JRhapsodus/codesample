#include "ColorShader.h"

namespace SIRendering
{

ColorShader::ColorShader() : super("Shaders/Colored.vsh", "Shaders/Colored.fsh")
{


}

ColorShader::~ColorShader()
{

}


GLint ColorShader::getIndexBufferAttributeID()
{
	return -1;
}

GLint ColorShader::getUVBufferAttributeID()
{
	return -1;
}

GLint ColorShader::getColorBufferAttributeID()
{
	return getAttributeID("a_v4FillColor");
}

GLint ColorShader::getVertexBufferAttributeID()
{
	return getAttributeID("a_v4Position");
}

GLint ColorShader::getShaderAttributeID()
{
	return shaderHandle;
}

GLint ColorShader::getModelViewProjectionUniformID()
{
	return -1;
}

}

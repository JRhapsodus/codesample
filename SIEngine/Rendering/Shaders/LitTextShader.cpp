#include "LitTextShader.h"

namespace SIRendering
{

LitTextShader::LitTextShader() : super("Shaders/LitText.vsh", "Shaders/LitText.fsh")
{ }

LitTextShader::LitTextShader( String vertexPath, String fragmentPath ) : super( vertexPath, fragmentPath )
{ }

LitTextShader::~LitTextShader()
{ }

GLint LitTextShader::getIndexBufferAttributeID()
{ return -1; }

GLint LitTextShader::getUVBufferAttributeID()
{ return getAttributeID("a_v2TexCoord"); }

GLint LitTextShader::getColorBufferAttributeID()
{ return -1; }

GLint LitTextShader::getVertexBufferAttributeID()
{ return getAttributeID("a_v4Position"); }

GLint LitTextShader::getShaderAttributeID()
{ return shaderHandle; }

GLint LitTextShader::getModelViewProjectionUniformID()
{ return getUniformID("u_m4ModelView"); }

}

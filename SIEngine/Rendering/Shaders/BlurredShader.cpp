#include "BlurredShader.h"

namespace SIRendering
{

BlurredShader::BlurredShader() : super("Shaders/UnlitTextured.vsh", "Shaders/BlurredTextured.fsh")
{ }

BlurredShader::~BlurredShader()
{ }

}

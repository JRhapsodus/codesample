#include "TintShader.h"

namespace SIRendering
{

TintShader::TintShader() : super("Shaders/Tinted.vsh", "Shaders/Tinted.fsh")
{ }

TintShader::~TintShader()
{ }


GLint TintShader::getIndexBufferAttributeID()
{ return -1; }

GLint TintShader::getUVBufferAttributeID()
{ return getAttributeID("a_v2TexCoord"); }

GLint TintShader::getColorBufferAttributeID()
{ return -1; }

GLint TintShader::getVertexBufferAttributeID()
{ return getAttributeID("a_v4Position"); }

GLint TintShader::getShaderAttributeID()
{ return shaderHandle; }

GLint TintShader::getModelViewProjectionUniformID()
{ return getUniformID("u_m4ModelView"); }


}

#ifndef TRANSFORMEDCOLORSHADER_H_
#define TRANSFORMEDCOLORSHADER_H_

#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{

class TransformedColorShader : public IShader
{
	CLASSEXTENDS(TransformedColorShader, IShader);

public:
	TransformedColorShader();
	virtual ~TransformedColorShader();

	virtual GLint getIndexBufferAttributeID();
	virtual GLint getUVBufferAttributeID();
	virtual GLint getColorBufferAttributeID();
	virtual GLint getVertexBufferAttributeID();
	virtual GLint getShaderAttributeID();
	virtual GLint getModelViewProjectionUniformID();
};

}
#endif /* TRANSFORMEDCOLORSHADER_H_ */

#ifndef TextShaderSHADER_H_
#define TextShaderSHADER_H_

#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{

class TextShader : public IShader
{
	CLASSEXTENDS(TextShader, IShader);

public:
	TextShader();
	virtual ~TextShader();

	virtual GLint getIndexBufferAttributeID();
	virtual GLint getUVBufferAttributeID();
	virtual GLint getColorBufferAttributeID();
	virtual GLint getVertexBufferAttributeID();
	virtual GLint getShaderAttributeID();
	virtual GLint getModelViewProjectionUniformID();
};

}
#endif /* TRANSFORMEDCOLORSHADER_H_ */

#include "TransformedColorShader.h"

namespace SIRendering
{

TransformedColorShader::TransformedColorShader() : super("Shaders/TransformedColor.vsh", "Shaders/TransformedColor.fsh")
{


}

TransformedColorShader::~TransformedColorShader()
{

}


GLint TransformedColorShader::getIndexBufferAttributeID()
{
	return -1;
}

GLint TransformedColorShader::getUVBufferAttributeID()
{
	return -1;
}

GLint TransformedColorShader::getColorBufferAttributeID()
{
	return getAttributeID("a_v3FillColor");
}

GLint TransformedColorShader::getVertexBufferAttributeID()
{
	return getAttributeID("a_v4Position");
}

GLint TransformedColorShader::getShaderAttributeID()
{
	return shaderHandle;
}

GLint TransformedColorShader::getModelViewProjectionUniformID()
{
	return getUniformID("u_m4ModelView");
}

}

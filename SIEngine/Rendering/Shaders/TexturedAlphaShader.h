#ifndef TEXTUREDALPHASHADER_H
#define TEXTUREDALPHASHADER_H

#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{


class TexturedAlphaShader : public IShader
{
	CLASSEXTENDS(TexturedAlphaShader, IShader);

public:
	TexturedAlphaShader();
	TexturedAlphaShader( String vertexPath, String fragmentPath );

	virtual ~TexturedAlphaShader();

	virtual GLint getIndexBufferAttributeID();
	virtual GLint getUVBufferAttributeID();
	virtual GLint getColorBufferAttributeID();
	virtual GLint getVertexBufferAttributeID();
	virtual GLint getShaderAttributeID();
	virtual GLint getModelViewProjectionUniformID();

};

}
#endif /* TRANSFORMEDCOLORSHADER_H_ */

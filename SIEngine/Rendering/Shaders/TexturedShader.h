#ifndef TEXTUREDSHADER_H
#define TEXTUREDSHADER_H

#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{


class TexturedShader : public IShader
{
	CLASSEXTENDS(TexturedShader, IShader);

public:
	TexturedShader();
	virtual ~TexturedShader();

	virtual GLint getIndexBufferAttributeID();
	virtual GLint getUVBufferAttributeID();
	virtual GLint getColorBufferAttributeID();
	virtual GLint getVertexBufferAttributeID();
	virtual GLint getShaderAttributeID();
	virtual GLint getModelViewProjectionUniformID();
};

}
#endif /* TRANSFORMEDCOLORSHADER_H_ */

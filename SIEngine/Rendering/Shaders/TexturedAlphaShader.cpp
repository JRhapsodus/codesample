#include "TexturedAlphaShader.h"

namespace SIRendering
{

TexturedAlphaShader::TexturedAlphaShader() : super("Shaders/UnlitTextured.vsh", "Shaders/UnlitTexturedAlpha.fsh")
{ }

TexturedAlphaShader::TexturedAlphaShader( String vertexPath, String fragmentPath ) : super( vertexPath, fragmentPath )
{ }

TexturedAlphaShader::~TexturedAlphaShader()
{ }

GLint TexturedAlphaShader::getIndexBufferAttributeID()
{ return -1; }

GLint TexturedAlphaShader::getUVBufferAttributeID()
{ return getAttributeID("a_v2TexCoord"); }

GLint TexturedAlphaShader::getColorBufferAttributeID()
{ return -1; }

GLint TexturedAlphaShader::getVertexBufferAttributeID()
{ return getAttributeID("a_v4Position"); }

GLint TexturedAlphaShader::getShaderAttributeID()
{ return shaderHandle; }

GLint TexturedAlphaShader::getModelViewProjectionUniformID()
{ return getUniformID("u_m4ModelView"); }

}

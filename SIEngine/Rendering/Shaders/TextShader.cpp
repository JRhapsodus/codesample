#include "TextShader.h"

namespace SIRendering
{

TextShader::TextShader() : super("Shaders/Text.vsh", "Shaders/Text.fsh")
{


}

TextShader::~TextShader()
{

}


GLint TextShader::getIndexBufferAttributeID()
{
	return -1;
}

GLint TextShader::getUVBufferAttributeID()
{
	return getAttributeID("a_v2TexCoord");
}

GLint TextShader::getColorBufferAttributeID()
{
	return getAttributeID("a_v4FillColor");
}

GLint TextShader::getVertexBufferAttributeID()
{
	return getAttributeID("a_v2ScreenPosition");
}

GLint TextShader::getShaderAttributeID()
{
	return shaderHandle;
}

GLint TextShader::getModelViewProjectionUniformID()
{
	return -1;
}

}

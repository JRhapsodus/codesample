#ifndef TINTSHADER_H_
#define TINTSHADER_H_

#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{

class TintShader : public IShader
{
	CLASSEXTENDS(TintShader, IShader);

public:
	TintShader();
	virtual ~TintShader();

	virtual GLint getIndexBufferAttributeID();
	virtual GLint getUVBufferAttributeID();
	virtual GLint getColorBufferAttributeID();
	virtual GLint getVertexBufferAttributeID();
	virtual GLint getShaderAttributeID();
	virtual GLint getModelViewProjectionUniformID();
};

}
#endif

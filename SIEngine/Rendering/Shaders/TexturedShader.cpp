#include "TexturedShader.h"

namespace SIRendering
{


TexturedShader::TexturedShader() : super("Shaders/UnlitTextured.vsh", "Shaders/UnlitTextured.fsh")
{
}

TexturedShader::~TexturedShader()
{ }


GLint TexturedShader::getIndexBufferAttributeID()
{
	return -1;
}

GLint TexturedShader::getUVBufferAttributeID()
{
	return getAttributeID("a_v2TexCoord");
}

GLint TexturedShader::getColorBufferAttributeID()
{
	return -1;
}

GLint TexturedShader::getVertexBufferAttributeID()
{
	return getAttributeID("a_v4Position");
}

GLint TexturedShader::getShaderAttributeID()
{
	return shaderHandle;
}

GLint TexturedShader::getModelViewProjectionUniformID()
{
	return getUniformID("u_m4ModelView");
}

}

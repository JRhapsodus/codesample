#ifndef BLURREDSHADER_H_
#define BLURREDSHADER_H_

#include "TexturedAlphaShader.h"

namespace SIRendering
{

class BlurredShader : public TexturedAlphaShader
{
	CLASSEXTENDS(BlurredShader, TexturedAlphaShader);
public:
	BlurredShader();
	virtual ~BlurredShader();

};

}
#endif /* BLURREDSHADER_H_ */

#ifndef WIREFRAMEMATERIAL_H_
#define WIREFRAMEMATERIAL_H_

#include "SIEngine/Rendering/IMaterial.h"
#include "SIEngine/Objects/SpriteObject.h"

namespace SIRendering
{

class WireFrameMaterial : public IMaterial
{
	CLASSEXTENDS(WireFrameMaterial,IMaterial);

public:
	virtual void doShaderPass();
	WireFrameMaterial(SpriteObject* go);
	WireFrameMaterial(GameObject* go, IShader* s, IDrawMode* dm ) : super(go,s,dm) {}
	virtual ~WireFrameMaterial();
};

}
#endif /* VERTEXLITMATERIAL_H_ */

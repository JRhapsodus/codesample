#include "TintMaterial.h"
#include "SIEngine/Rendering/ShaderManager.h"

namespace SIRendering
{

TintMaterial::TintMaterial(SpriteObject* go) :
	super(go, ShaderManager::getInstance().getShader(ShaderNameTint), SpriteObject::g_SpriteDrawMode )
{

}

TintMaterial::~TintMaterial()
{

}

void TintMaterial::doShaderPass()
{
	SpriteObject* sprite = (SpriteObject*)gameObject;
	float worldZ = sprite->getWorldPosition().z;
	float value = ((worldZ+300)/900.0f);
	value = (value<0)?0:value;
	value = (value>1)?1:value;

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sprite->getSpriteData()->getTexture()->getTextureID());

	glUniform1i( shader->getUniformID("u_s2dTexture"), 0);
	Color textColor = sprite->getColor();
	glUniform4f( shader->getUniformID("u_v4FillColor"), textColor.r, textColor.g, textColor.b, textColor.a );
	glUniform1f( shader->getUniformID("Blur"), value*0.5 );
}

}

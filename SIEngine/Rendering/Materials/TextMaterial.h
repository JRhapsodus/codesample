#ifndef TEXTMATERIAL_H_
#define TEXTMATERIAL_H_

#include "SIEngine/Rendering/IMaterial.h"
#include "SIEngine/Objects/SpriteObject.h"
#include "SIEngine/Objects/TextLabel.h"
namespace SICore
{
	class SpriteObject;
	class TextLabel;
}

namespace SIRendering
{

class TextMaterial : public IMaterial
{
	CLASSEXTENDS(TextMaterial,IMaterial);

public:
	virtual void doShaderPass();
	TextMaterial(SpriteObject* go);
	TextMaterial(TextLabel* go);
	TextMaterial(GameObject* go, IShader* s, IDrawMode* dm ) : super(go,s,dm) {}
	virtual ~TextMaterial();
};

}
#endif

#ifndef TRANSPARENTSPRITEMATERIAL_H_
#define TRANSPARENTSPRITEMATERIAL_H_

#include "SIEngine/Rendering/IMaterial.h"
#include "SIEngine/Objects/SpriteObject.h"

namespace SICore
{ class SpriteObject; }
using namespace SICore;

namespace SIRendering
{
class TransparentSpriteMaterial : public IMaterial
{
	CLASSEXTENDS(TransparentSpriteMaterial,IMaterial);

public:
	virtual void doShaderPass();
	TransparentSpriteMaterial(SpriteObject* go);
	TransparentSpriteMaterial(GameObject* go, IShader* s, IDrawMode* dm ) : super(go,s,dm) {}
	virtual ~TransparentSpriteMaterial();
};

}

#endif /* TRANSPARENTSPRITEMATERIAL_H_ */

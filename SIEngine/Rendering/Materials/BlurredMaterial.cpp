#include "BlurredMaterial.h"
#include "SIEngine/Core/OpenGlRenderer.h"
#include "SIEngine/Rendering/ShaderManager.h"
#include "Glasgow/Controls/ContentItem.h"
namespace SIRendering
{

BlurredMaterial::BlurredMaterial(SpriteObject* go) :
	super(go, ShaderManager::getInstance().getShader(ShaderNameBlurred), SpriteObject::g_SpriteDrawMode )
{

}

BlurredMaterial::~BlurredMaterial()
{

}

void BlurredMaterial::doShaderPass()
{
	SpriteObject* sprite = (SpriteObject*)gameObject;

	float worldZ = sprite->getWorldPosition().z;
	float value = ((worldZ+300)/900.0f);
	value = (value<0)?0:value;
	value = (value>1)?1:value;

	//this really shouldnt be in the material, but I dont want to repipe all the classes and move this calculation into
	//the object, as in letting the gameobject set the materials value from within its own update
	SpriteObject* shimmer = ((ContentItem*)sprite->getParent())->ringShimmer;
	shimmer->setOpacity( (1-value) * 0.75f );

	glActiveTexture(GL_TEXTURE0);
	OpenGlRenderer::checkGLError();
	glBindTexture(GL_TEXTURE_2D, sprite->getSpriteData()->getTexture()->getTextureID());
	OpenGlRenderer::checkGLError();
	glUniform1i( shader->getUniformID("u_s2dTexture"), 0);
	OpenGlRenderer::checkGLError();
	glUniform1f( shader->getUniformID("Blur"), value );
	OpenGlRenderer::checkGLError();
}

}

#include "WireFrameMaterial.h"
#include "SIEngine/Rendering/ShaderManager.h"

namespace SIRendering
{


WireFrameMaterial::WireFrameMaterial(SpriteObject* go) :
	super(go, ShaderManager::getInstance().getShader(ShaderNameTransformedColor), SpriteObject::g_SpriteDrawMode )
{

}

WireFrameMaterial::~WireFrameMaterial()
{

}

void WireFrameMaterial::doShaderPass()
{

}

}

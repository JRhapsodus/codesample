#include "VertexLitMaterial.h"
#include "SIEngine/Rendering/ShaderManager.h"

namespace SIRendering
{

VertexLitMaterial::VertexLitMaterial(SpriteObject* go) :
	super(go, ShaderManager::getInstance().getShader(ShaderNameTextured), SpriteObject::g_SpriteDrawMode )
{

}

VertexLitMaterial::~VertexLitMaterial()
{

}

void VertexLitMaterial::doShaderPass()
{
	SpriteObject* sprite = (SpriteObject*)gameObject;

	Texture* texture = sprite->getSpriteData()->getTexture();
	if( texture != NULL )
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture->getTextureID());
		glUniform1i( shader->getUniformID("u_s2dTexture"), 0);
	}
}

}

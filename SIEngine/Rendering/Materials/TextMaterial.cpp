#include "TextMaterial.h"
#include "SIEngine/Rendering/ShaderManager.h"

namespace SIRendering
{

TextMaterial::TextMaterial(TextLabel* go) :
		super(go, ShaderManager::getInstance().getShader(ShaderNameLitText), SpriteObject::g_SpriteDrawMode )
{

}

TextMaterial::TextMaterial(SpriteObject* go) :
	super(go, ShaderManager::getInstance().getShader(ShaderNameLitText), SpriteObject::g_SpriteDrawMode )
{

}

TextMaterial::~TextMaterial()
{

}

void TextMaterial::doShaderPass()
{
	SpriteObject* sprite = null;
	TextLabel* 		label = null;
	if( gameObject->typeOf( SpriteObject::type()))
	{ sprite = (SpriteObject*)gameObject; }
	else
	{ label = (TextLabel*)gameObject; }


	if( sprite != null )
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, sprite->getSpriteData()->getTexture()->getTextureID());
		glUniform1i( shader->getUniformID("u_s2dTexture"), 0);
		Color textColor = sprite->getColor();
		glUniform4f( shader->getUniformID("u_v4FillColor"), textColor.r, textColor.g, textColor.b, textColor.a );
	}
	else
	{
		if ( label->getSpriteData() != null )
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, label->getSpriteData()->getTexture()->getTextureID());
			glUniform1i( shader->getUniformID("u_s2dTexture"), 0);
			Color textColor = label->getTextColor();
			glUniform4f( shader->getUniformID("u_v4FillColor"), textColor.r, textColor.g, textColor.b, textColor.a );
		}
		else
		{
			printf(" BIG ERROR OCCURED %s\n", label->name.str() );
			printf(" BIG ERROR OCCURED 2 %s\n", label->getParent()->name.str() );

		}
	}



}

}

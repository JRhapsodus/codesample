#ifndef TINTMATERIAL_H_
#define TINTMATERIAL_H_

#include "SIEngine/Rendering/IMaterial.h"
#include "SIEngine/Objects/SpriteObject.h"

namespace SICore
{ class SpriteObject; }

namespace SIRendering
{
/// <summary> 
/// This is really the same a text material, since they both use the tint
/// color of a sprite object, though in the future these may be forked
/// specifically for text. So in that regard I liked the named class to
/// be different
/// </summary> 
class TintMaterial : public IMaterial
{
	CLASSEXTENDS(TintMaterial,IMaterial);

public:
	virtual void doShaderPass();
	TintMaterial(SpriteObject* go);
	TintMaterial(GameObject* go, IShader* s, IDrawMode* dm ) : super(go,s,dm) {}
	virtual ~TintMaterial();
};

}
#endif /* TINTMATERIAL_H_ */

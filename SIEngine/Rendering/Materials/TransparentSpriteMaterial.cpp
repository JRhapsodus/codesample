#include "TransparentSpriteMaterial.h"
#include "SIEngine/Rendering/ShaderManager.h"

namespace SIRendering
{

TransparentSpriteMaterial::TransparentSpriteMaterial(SpriteObject* go) :
	super(go, ShaderManager::getInstance().getShader(ShaderNameTexturedAlpha), SpriteObject::g_SpriteDrawMode )
{

}

TransparentSpriteMaterial::~TransparentSpriteMaterial()
{

}

void TransparentSpriteMaterial::doShaderPass()
{
	SpriteObject* sprite = (SpriteObject*)gameObject;

	Texture* texture = sprite->getSpriteData()->getTexture();
	if( texture != NULL )
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture->getTextureID());
		glUniform1i( shader->getUniformID("u_s2dTexture"), 0);
		glUniform1f( shader->getUniformID("Opacity"), sprite->getOpacitySelf() );
	}
}

}

#ifndef VERTEXLITMATERIAL_H_
#define VERTEXLITMATERIAL_H_

#include "SIEngine/Rendering/IMaterial.h"
#include "SIEngine/Objects/SpriteObject.h"

namespace SICore
{ class SpriteObject; }
using namespace SICore;


namespace SIRendering
{

class VertexLitMaterial : public IMaterial
{
	CLASSEXTENDS(VertexLitMaterial,IMaterial);

public:
	virtual void doShaderPass();
	VertexLitMaterial(SpriteObject* go);
	VertexLitMaterial(GameObject* go, IShader* s, IDrawMode* dm ) : super(go,s,dm) {}
	virtual ~VertexLitMaterial();
};

}
#endif /* VERTEXLITMATERIAL_H_ */

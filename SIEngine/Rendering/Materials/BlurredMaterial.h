#ifndef BLURREDMATERIAL_H_
#define BLURREDMATERIAL_H_

#include "SIEngine/Objects/SpriteObject.h"

namespace SIRendering
{

class BlurredMaterial : public IMaterial
{
	CLASSEXTENDS(BlurredMaterial,IMaterial);

public:
	virtual void doShaderPass();
	BlurredMaterial(SpriteObject* go);

	virtual ~BlurredMaterial();
};

}
#endif /* BLURREDMATERIAL_H_ */

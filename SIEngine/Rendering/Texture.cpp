#include "SIEngine/Rendering/Texture.h"
#include <GLES2/gl2ext.h>
#include <GLES2/gl2.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include "SIEngine/Core/OpenGlRenderer.h"
#include <GLES2/gl2.h>
namespace SIRendering
{

//************************************************************************************
// Create a texture from an image
//************************************************************************************
Texture::Texture(IImage* imageUsed, TextureSettings tsettings, GLenum textureSlot) : super()
{
	textureHandle = 0;
	settings = tsettings;
	activeTexture = textureSlot;
	size = Size(imageUsed->Width(), imageUsed->Height());

	createTextureGPU();
	sendSettingsGPU();
	uploadImageGPU(imageUsed);
}

//************************************************************************************
// Cleanup handle on gpu
//************************************************************************************
Texture::~Texture()
{
	if (textureHandle != 0)
	{
		glDeleteTextures(1, &textureHandle);
		glFinish();
	}

	OpenGlRenderer::checkGLError();
	printf("Texture::~Texture()\n");
}

//************************************************************************************
// Gettors
//************************************************************************************
Size Texture::getSize()
{ return size; }

GLuint Texture::getTextureID()
{ return textureHandle; }

//************************************************************************************
// Upload image data to GPU
//************************************************************************************
void Texture::uploadImageGPU(IImage* image)
{
	//************************************************************************************
	// Upload data to active texture
	//************************************************************************************
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	if( image->PixelFormat() == kPixelFormatARGB8888 )
	{ glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.width_, size.height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->RawPixels()); 	}
	else if ( image->PixelFormat() == kPixelFormatRGB888 )
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1024, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, image->RawPixels());
	}
	OpenGlRenderer::checkGLError();
}

//************************************************************************************
// Create handle, set the active texture, bind the handle to the active
//************************************************************************************
void Texture::createTextureGPU()
{
	glActiveTexture(GL_TEXTURE0);
	OpenGlRenderer::checkGLError();

	glGenTextures(1, &textureHandle);
	OpenGlRenderer::checkGLError();

	glBindTexture(GL_TEXTURE_2D, textureHandle);
	OpenGlRenderer::checkGLError();
}

//************************************************************************************
// Texture settings
//************************************************************************************
void Texture::sendSettingsGPU()
{
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, settings.minFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, settings.magFilter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, settings.wrapModeS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, settings.wrapModeT);

	OpenGlRenderer::checkGLError();
}


}

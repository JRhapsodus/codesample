#ifndef IMATERIAL_H_
#define IMATERIAL_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Rendering/IShader.h"
#include "SIEngine/Rendering/IDrawMode.h"
#include <GLES2/gl2.h>

namespace SICore
{ class GameObject; }

namespace SIRendering
{

class IShader;

class IMaterial : public BaseObject
{
	CLASSEXTENDS(IMaterial, BaseObject);
	ADD_TO_CLASS_MAP;

public:
	IMaterial(GameObject* go, IShader* s, IDrawMode* dm );
	virtual ~IMaterial();
	virtual void doShaderPass() = 0;

	IShader* getShader();
	IDrawMode* getDrawMode();

protected:
	GameObject* gameObject;
	IShader* shader;
	IDrawMode* drawMode;
};

}

#endif /* IMATERIAL_H_ */

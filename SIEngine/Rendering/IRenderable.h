#ifndef INCLUDE_IRENDERABLE_H_
#define INCLUDE_IRENDERABLE_H_

#include <GLES2/gl2.h>
#include "SIEngine/Rendering/IMaterial.h"

using namespace SIRendering;

class IRenderable
{
public:
	IRenderable() {}
	virtual ~IRenderable() {}
	virtual void render()  = 0;

	/// <summary> 
	/// Buffers
	/// </summary> 
	virtual int* 	getIndexBuffer() = 0;
	virtual float*  getVertexBuffer() = 0;

	/// <summary> 
	/// Buffer Sizes
	/// </summary> 
	virtual int	getVertexCount() = 0;
	virtual int	getIndexCount() = 0;

	/// <summary> 
	/// Render structs
	/// </summary> 
	virtual IMaterial* getMaterial() = 0;
};

#endif

#include "MutableTexture.h"
#include "SIEngine/Core/OpenGlRenderer.h"

namespace SIRendering
{

///<summary>
/// Mutable texture must hold onto its image as it is used to update the pixels on gpu
///</summary>
MutableTexture::MutableTexture(IImage* img, TextureSettings tsettings, GLenum textureSlot) : super( img, tsettings, textureSlot)
{
	image = img;
	image->addRef();
}

MutableTexture::~MutableTexture()
{
	printf("MutableTexture::~MutableTexture()\n");
	//Owned by camera mpi on camera feed destroy
	//SAFE_RELEASE(image);
}

///<summary>
/// Sets a new image to be tied to
///</summary>
void MutableTexture::setImage(IImage* newImg)
{
	SAFE_RELEASE(image);
	image = newImg;
	image->addRef();
}

///<summary>
/// Called when the 'coupled' IImage has had its image changed, and needs
/// to send to the gpu for reuploading. Lets defer this job to the materials
///</summary>
void MutableTexture::updatePixels()
{
	//printf("MutableTexture::updatePixels()\n");

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, getTextureID());
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	if( image->PixelFormat() == kPixelFormatRGB888 )
	{ glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image->Width(), image->Height(), GL_RGB, GL_UNSIGNED_BYTE, image->RawPixels()); }
	else
	{ glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image->Width(), image->Height(), GL_RGBA, GL_UNSIGNED_BYTE, image->RawPixels()); }

	OpenGlRenderer::checkGLError();
}


}

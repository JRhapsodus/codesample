#include "TextureManager.h"
#include "SIEngine/Resources/ImageTarga.h"
#include "SIEngine/Resources/ImagePNG.h"
#include "SIEngine/Resources/SpriteFrame.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"

using namespace SICore;

namespace SIRendering
{

static TextureManager* g_TexManagerInstance = null;

/// <summary>
/// True singleton pattern
/// </summary>
TextureManager* TextureManager::getInstance()
{
	if( g_TexManagerInstance == null )
	{ g_TexManagerInstance = new TextureManager(); }

	return g_TexManagerInstance;
}

//************************************************************************************
//By default lets just load the new texture whenever it is asked
//for, but in the future the scene manager will probably shell into this holding class
//************************************************************************************
Texture* TextureManager::getTexture( String textureName )
{
	if( !hasTexture(textureName))
	{ loadTexture(textureName, PNG); }

	BaseObject* texture = loadedTextures[textureName];
	return (Texture*)texture;
}

//******************************************************************
// Has texture by name?
//******************************************************************
bool TextureManager::hasTexture(String textureName )
{ return loadedTextures.containsKey(textureName); }

//******************************************************************
// Get a texture from a loaded sprite sheet
//******************************************************************
SpriteData *TextureManager::getSpriteData(String textureName)
{
	BaseObject *sd = sheetSpriteData[textureName];
	return (SpriteData *)sd;
}

//******************************************************************
// Has sprite by name?
//******************************************************************
bool TextureManager::hasSpriteData(String textureName)
{ return sheetSpriteData.containsKey(textureName); }

//******************************************************************
// Loard targa
//******************************************************************
void TextureManager::loadTga( String textureName )
{
	ImageTarga *tga = new ImageTarga();
	tga->load( S("Textures/") + textureName + ".tga");
	tga->flipVertical();

	TRelease<Texture> texture(new Texture(tga, TextureSettings( GL_LINEAR, GL_LINEAR, false, true), (GL_TEXTURE0 + loadedTextures.size()) ));
	loadedTextures[textureName] = texture;
	tga->release();
}

//******************************************************************
// Unload specific texture
//******************************************************************
void TextureManager::unloadTexture(String textureName)
{
	printf("Unloading texture: %s\n", textureName.str() );
	if( loadedTextures.containsKey(textureName) )
	{ loadedTextures.remove(textureName); }
}

//******************************************************************
// Load png
//******************************************************************
void TextureManager::loadPng( String textureName )
{
	printf("Loading Texture %s\n", textureName.str() );
	if( !loadedTextures.containsKey(textureName))
	{
		ImagePNG *png = NULL;

		if( textureName.indexOf(".png") >= 0 )
		{ png = new ImagePNG(textureName); }
		else
		{ png = new ImagePNG(textureName + ".png"); }

		if( png->isValid )
		{
			if( textureName == "progressBar")
			{
				printf("Loading texture progressBar as a POINT filtered texture\n");
				TRelease<Texture> texture(new Texture(png, TextureSettings( GL_NEAREST, GL_NEAREST, false, true), (GL_TEXTURE0 + loadedTextures.size())));
				loadedTextures[textureName] = texture;
				png->release();
			}
			else
			{
				TRelease<Texture> texture(new Texture(png, TextureSettings( GL_LINEAR, GL_LINEAR, false, true), (GL_TEXTURE0 + loadedTextures.size())));
				loadedTextures[textureName] = texture;
				png->release();
			}
		}
		else
		{
			printf("LOADING IMAGE FAILED HALF WAY THROUGH THE READ\n");
		}

	}
}

//******************************************************************
// Forking function for convenience
//******************************************************************
void TextureManager::loadTexture( String textureName, TextureType type )
{
	if( type == TGA )
	{ loadTga(textureName); }
	if( type == PNG )
	{ loadPng(textureName); }
}

//******************************************************************
// Loads a texture sheet in the format generated by GlasgowExporter
// a custom exporter for TexturePacker
//******************************************************************
void TextureManager::loadTextureSheet(String textureSheetName) {
	FILE* fp = AssetLibrary::getFile( S("SpriteSheets/"), textureSheetName, S(".dat"), S("rt"));

	int r;
	struct line_reader lr;
	size_t len;
	char *line;
	AssetLibrary::lr_init(&lr, fp);

	char textureName[128];
	int texw, texh;

	line = AssetLibrary::next_line(&lr, &len);
	r = sscanf(line, "%127s %d %d", textureName, &texw, &texh);
	if (r < 3)
	{ printf("Error parsing SpriteSheet %s, line: 1\n", textureSheetName.str()); }

	while (1)
	{
		line = AssetLibrary::next_line(&lr, &len);
		if (line == NULL || line[0] == '\n') {
			break;
		}
		char spriteName[128];
		int x, y, w, h;
		r = 0;
		r = sscanf(line, "%127s %d %d %d %d", spriteName, &x, &y, &w, &h);
		if (r < 5) {
			break;
		}

		UV topLeft;
		UV topRight;
		UV bottomLeft;
		UV bottomRight;
		topLeft.u = (float)x/(float)texw;
		topLeft.v = (float)y/(float)texh;

		topRight.u = (float)(x+w)/(float)texw;
		topRight.v = topLeft.v;

		bottomRight.u = topRight.u;
		bottomRight.v = (float)(y+h)/(float)texh;

		bottomLeft.u = topLeft.u;
		bottomLeft.v = bottomRight.v;

		TRelease<SpriteFrame> frame(new SpriteFrame(bottomLeft, topLeft, topRight, bottomRight));
		TRelease<SpriteData> spriteData(new SpriteData(frame, textureSheetName));
		sheetSpriteData[String(spriteName)] = spriteData;

		spriteData->addRef();
	}

	fclose(fp);
}

//******************************************************************
// Creates an offscreen texture, but really this could just be
//******************************************************************
MutableTexture* TextureManager::createOffScreenTexture( RawImage* offscreenImage, String textureName )
{
	MutableTexture* mt = new MutableTexture(offscreenImage, TextureSettings( GL_LINEAR, GL_LINEAR, false, true), (GL_TEXTURE0 + loadedTextures.size()) );
	loadedTextures[textureName] = mt;
	return mt;
}

//******************************************************************
// Releases all
//******************************************************************
void TextureManager::unloadAllTextures()
{
	printf("TextureManager::unloadAllTextures()\n");
	if (!loadedTextures.isEmpty())
		loadedTextures.clear();
}

TextureManager::~TextureManager()
{
	printf("TextureManager::~TextureManager()\n");
	unloadAllTextures();

}

}

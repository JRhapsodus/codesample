#ifndef SHADERMANAGER_H_
#define SHADERMANAGER_H_

#include "SIEngine/Collections/Hashtable.h"
#include "SIEngine/Collections/ObjectArray.h"
#include "SIEngine/Rendering/IShader.h"

namespace SIRendering
{

/// <summary> 
/// Refer to shaders in code via an enum rather than "magicString"
/// </summary> 
typedef enum ShaderName
{
	ShaderNameTextured,
	ShaderNameTexturedAlpha,
	ShaderNameTransformedColor,
	ShaderNameColorOnly,
	ShaderNameText,
	ShaderNameBlurred,
	ShaderNameLitText,
	ShaderNameTint
} ShaderName;

/// <summary> 
/// Manages the loading and unloading of all shaders
/// </summary> 
class ShaderManager
{
public:

	/// <summary> 
	/// True singelton, only point of access
	/// </summary> 
	static ShaderManager& getInstance()
	{
		static ShaderManager managerInstance;
		return managerInstance;
	}

	/// <summary> 
	/// Returns shader via name
	/// </summary> 
	IShader* getShader( ShaderName shaderName );
private:
	HashTable loadedShaders;

	/// <summary> 
	/// Life Cycle, Only we can create these
	/// </summary> 
	ShaderManager();
	virtual ~ShaderManager();

	/// <summary> 
	/// Deny copy constructors & Assignment operators
	/// </summary> 
	ShaderManager(ShaderManager const & ) { }
	void operator =(ShaderManager const &) { }
};

#endif /* SHADERMANAGER_H_ */

}

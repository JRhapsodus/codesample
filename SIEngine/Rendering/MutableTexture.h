#ifndef MUTABLETEXTURE_H_
#define MUTABLETEXTURE_H_

#include "SIEngine/Resources/IImage.h"
#include "SIEngine/Rendering/Texture.h"

namespace SIRendering
{

class MutableTexture : public Texture
{
	CLASSEXTENDS(MutableTexture, Texture);
public:
	///<summary>
	/// Mutable texture must hold onto its image as it is used to update the pixels on gpu
	///</summary>
	MutableTexture(IImage* img, TextureSettings tsettings, GLenum textureSlot);
	virtual ~MutableTexture();

	///<summary>
	/// Sets a new image to be tied to
	///</summary>
	void setImage(IImage* newImg);

	///<summary>
	/// Called when the 'coupled' IImage has had its image changed, and needs
	/// to send to the gpu for reuploading. Lets defer this job to the materials
	///</summary>
	void updatePixels();

private:
	IImage* image;

};

}
#endif

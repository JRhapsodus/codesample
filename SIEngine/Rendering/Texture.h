#ifndef TEXTURE_H_
#define TEXTURE_H_
#include <ImageIO.h>
#include <GLES2/gl2.h>
#include "SIEngine/Core/StringClass.h"
#include "SIEngine/Resources/IImage.h"
#include "SIEngine/Geometry/Primitives.h"

using namespace SIGeometry;

namespace SIRendering
{

/// <summary> 
/// Helper class to use with textures
/// </summary> 
class TextureSettings : public BaseObject
{
	CLASSEXTENDS(TextureSettings, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	GLuint minFilter;
	GLuint magFilter;
	GLuint wrapModeS;
	GLuint wrapModeT;
	bool compressed;
	bool hasAlpha;

	TextureSettings( GLuint min, GLuint mag, bool usesCompression, bool usesAlpha) : super()
	{
		minFilter = min;
		magFilter = mag;
		compressed = usesCompression;
		hasAlpha = usesAlpha;
		wrapModeS = GL_CLAMP_TO_EDGE;
		wrapModeT = GL_CLAMP_TO_EDGE;
	}

	TextureSettings() : super()
	{
		minFilter = 0;
		magFilter = 0;
		compressed = false;
		hasAlpha = true;
		wrapModeS = GL_CLAMP_TO_EDGE;
		wrapModeT = GL_CLAMP_TO_EDGE;
	}

	~TextureSettings() { }
};

/// <summary> 
/// OpenGL binded image data
/// </summary> 
class Texture : public BaseObject
{
	CLASSEXTENDS(Texture,BaseObject);
	ADD_TO_CLASS_MAP;

public:
	Texture(IImage* imageUsed, TextureSettings tsettings, GLenum textureSlot);
	~Texture();

	/// <summary> 
	/// Identifier on GPU
	/// </summary> 
	GLuint getTextureID();
	Size getSize();

private:
	/// <summary> 
	/// Three steps to binding a texture to GPU
	/// </summary> 
	virtual void createTextureGPU();
	virtual void sendSettingsGPU();
	virtual void uploadImageGPU(IImage* image);

	Size size;
	GLuint textureHandle;
	GLenum activeTexture;
	TextureSettings settings;
};

}
#endif /* TEXTURE_H_ */

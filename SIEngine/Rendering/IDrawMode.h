#ifndef INCLUDE_IDRAWMODE_H_
#define INCLUDE_IDRAWMODE_H_

#include <GLES2/gl2.h>
#include "SIEngine/Core/StringClass.h"
#include "SIEngine/Include/SIGeometry.h"

namespace SIRendering
{

typedef enum DrawMethod
{
	Indexed,
	TriangleStrip,
	Triangles,
	TexturedPolygon
} DrawMethod;

class IDrawMode : public BaseObject
{
	CLASSEXTENDS(IDrawMode, BaseObject);
public:
	DrawMethod DrawTechnique;
	GLenum TriangleMode;
	GLenum SrcBlendMode;
	GLenum DstBlendMode;
	GLenum DepthFunction;
	GLenum CullFace;
	Rect scissor;
	VertexFormat* VertFormat;

	IDrawMode() : super()
	{
		DrawTechnique = Indexed;
		TriangleMode = GL_TRIANGLES;
		SrcBlendMode = GL_SRC_ALPHA;
		DstBlendMode = GL_ONE_MINUS_SRC_ALPHA;
		DepthFunction = GL_LESS;
		CullFace = GL_DEPTH_TEST;
		VertFormat = LitTexturedPositionVertex::staticVertexFormat();
	}

	IDrawMode(DrawMethod method, VertexFormat* format, GLenum triangle, GLenum src, GLenum dst, GLenum depthFunc,
			Rect clipRect) : super()
	{
		scissor = clipRect;
		VertFormat = format;
		DrawTechnique = method;
		TriangleMode = triangle;
		SrcBlendMode = src;
		DstBlendMode = dst;
		DepthFunction = depthFunc;
		CullFace = GL_DEPTH_TEST;
	}


	IDrawMode(DrawMethod method, VertexFormat* format, GLenum triangle, GLenum src, GLenum dst, GLenum depthFunc) : super()
	{
		VertFormat = format;
		DrawTechnique = method;
		TriangleMode = triangle;
		SrcBlendMode = src;
		DstBlendMode = dst;
		DepthFunction = depthFunc;
		CullFace = GL_DEPTH_TEST;
	}

	virtual ~IDrawMode() {}
};

class ClipSpriteDrawMode : public IDrawMode
{
	CLASSEXTENDS( ClipSpriteDrawMode, IDrawMode );
	ClipSpriteDrawMode() : super(Triangles, TexturedPositionVertex::staticVertexFormat(), GL_TRIANGLES, GL_SRC_ALPHA,
			GL_ONE_MINUS_SRC_ALPHA, GL_LESS, Rect(Vec2f(200.0f,200.0f),Size(500.0f,500.0f)) )
	{ }
};


class SpriteDrawMode : public IDrawMode
{
	CLASSEXTENDS( SpriteDrawMode, IDrawMode );
	SpriteDrawMode() : super(Triangles, LitTexturedPositionVertex::staticVertexFormat(), GL_TRIANGLES, GL_SRC_ALPHA,
			GL_ONE_MINUS_SRC_ALPHA, GL_LEQUAL)
	{ }
};

class IndexTexturedAlphaMode : public IDrawMode
{
	CLASSEXTENDS( IndexTexturedAlphaMode, IDrawMode );
	IndexTexturedAlphaMode() : super(Indexed, LitTexturedPositionVertex::staticVertexFormat(), GL_TRIANGLES, GL_SRC_ALPHA,
			GL_ONE_MINUS_SRC_ALPHA, GL_LESS)
	{ }
};


class IndexColoredMode : public IDrawMode
{
	CLASSEXTENDS( IndexColoredMode, IDrawMode );
	IndexColoredMode() : super(Indexed, LitPositionVertex::staticVertexFormat(), GL_TRIANGLES, GL_SRC_ALPHA,
			GL_ONE_MINUS_SRC_ALPHA, GL_LESS)
	{ }
};


class ColoredMeshMode : public IDrawMode
{
	CLASSEXTENDS( ColoredMeshMode, IDrawMode );
	ColoredMeshMode() : super(Triangles, LitPositionVertex::staticVertexFormat(), GL_TRIANGLES, GL_SRC_ALPHA,
			GL_ONE_MINUS_SRC_ALPHA, GL_LESS)
	{ }
};

}
#endif

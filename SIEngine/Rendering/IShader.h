#ifndef INCLUDE_SHADER_H_
#define INCLUDE_SHADER_H_

#include "SIEngine/Core/StringClass.h"
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

using namespace SICore;

namespace SIRendering
{

class IShader : public BaseObject
{
	CLASSEXTENDS(Shader,BaseObject);
	ADD_TO_CLASS_MAP;
public:
	IShader() : super()
	{
		shaderHandle = -1;
		vertexShader = -1;
		fragmentShader = -1;
	}

	IShader( String vertexPath, String fragmentPath );

	/// <summary> 
	/// Get attributes and uniforms. I would prefer enums but this works
	/// </summary> 
	GLint getAttributeID( String attributeName );
	GLint getUniformID( String attributeName );

	/// <summary> 
	/// These are the things that make up various shader differences
	/// </summary> 
	virtual GLint   getIndexBufferAttributeID() = 0;
	virtual GLint 	getUVBufferAttributeID() = 0;
	virtual GLint	getColorBufferAttributeID() = 0;
	virtual GLint 	getVertexBufferAttributeID() = 0;
	virtual GLint 	getShaderAttributeID() = 0;
	virtual GLint 	getModelViewProjectionUniformID() = 0;
protected:
	GLuint shaderHandle;
	GLint vertexShader;
	GLint fragmentShader;
	int   processShader(String shaderPath, GLint shaderType);
	char* loadShader(String shaderPath);
};

}
#endif

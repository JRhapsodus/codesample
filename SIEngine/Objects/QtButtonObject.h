#ifndef QTBUTTONOBJECT_H_
#define QTBUTTONOBJECT_H_

#include "SIEngine/Objects/GameObject.h"
#include "QtSpriteObject.h"

namespace SICore
{

/// <summary>
/// State enums
/// </summary>
typedef enum QtButtonStates
{
	QtButtonStateNormal,
	QtButtonStateActive
} QtButtonState;


class QtButtonObject : public GameObject
{
	CLASSEXTENDS(QtButtonObject, GameObject);

public:

	QtButtonObject(QtSpriteObject* a, QtSpriteObject* n);
	virtual ~QtButtonObject();

	void setState( QtButtonState newState );

	/// <summary>
	/// Load from file
	/// </summary>
	static QtButtonObject* deSerialize( FILE* fp );

private:
	QtButtonState 	currentState;
	QtSpriteObject* active;
	QtSpriteObject* normal;

};

}
#endif

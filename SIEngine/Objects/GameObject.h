#ifndef INCLUDE_GAMEOBJECT_H_
#define INCLUDE_GAMEOBJECT_H_

#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Include/SIGeometry.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Rendering/IRenderable.h"
#include <LF/EventMessage.h>

typedef void(*CallBackFunction)(void);

LF_USING_BRIO_NAMESPACE();

namespace SICore
{ class CameraObject; }

namespace SIActions
{ class BaseAction; }

using namespace SIActions;
using namespace SIRendering;
using namespace SIGeometry;

namespace SICore
{
/// <summary>
/// State enums
/// </summary>
typedef enum ButtonStates
{
	ButtonStateNone,
	ButtonStateNormal,
	ButtonStateSelected,
	ButtonStateActive,
	ButtonStateDisabled
} ButtonState;


/// <summary> 
/// Data class that stores the state of an object
/// </summary>
class GameObjectState : public BaseObject
{
	CLASSEXTENDS(GameObjectState, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	GameObjectState() { originalVisibility = true; }
	~GameObjectState() { }
	Transform 	originalTransform;
	Color		oringalColor;
	bool		originalVisibility;
	float		originalOpacity;
	ButtonState originalButtonState;
};


/// <summary>
/// Used as a base object for on screen primitives
/// </summary> 
class GameObject : public ObjectCollection, public virtual IRenderable
{
	CLASSEXTENDS(GameObject, ObjectCollection);
	ADD_TO_CLASS_MAP;
public:
	GameObject();
	GameObject(String name);
	~GameObject();

	/// <summary> 
	/// Logic
	/// </summary> 
	void activate();
	void deactivate();
    GameObject* highestTouchableParent();
	void queueAction( BaseAction* action );
	bool descendantOf( GameObject* lineage );
	bool hasActions();
	void cancelActions();
	void cancelActionsAsComplete();
	void endActions();

	/// <summary> 
	/// Game loop constructs
	/// </summary> 
	virtual void render();
	virtual void update( float dt );
	virtual bool processInput( IEventMessage *message);
	virtual bool hitTest(int x, int y);

	/// <summary> 
	/// Scene graph management
	/// </summary> 
	virtual void onChildRemoved(GameObject* child);
	virtual void onParentChanged(GameObject* oldParent, GameObject* newParent);
	virtual void onChildAdded(GameObject* child);

	/// <summary> 
	/// Actions
	/// </summary> 
	virtual void moveTo(Vec3f position, float duration);
	virtual void moveToScaleTo(Vec3f position, Vec3f scale, float duration);
	void moveTo(Vec3f position);
	void moveBy(Vec3f delta, float duration);
	void moveBy(Vec3f delta);
	void rotateTo(Quaternion rotation, float duration);
	void rotateTo(Vec3f angle, float duration);
	void rotateTo(Vec3f angle);

	void rotateBy(Quaternion delta, float duration);

	void rotateBy(Vec3f delta, float duration);
	void rotateBy(Vec3f delta);
	void scaleToOriginal(float duration);
	void scaleTo(Vec3f size, float duration);
	void scaleTo(Vec3f size);
	void scaleBy(Vec3f delta, float duration);
	void scaleBy(Vec3f delta);
	void waitFor(float timeDelta);
	void fadeTo(float opacity, float timeDelta);
	void fadeTo(float opacity);
	void triggerSound(String soundName );
	void makeVisible(bool visibility);
	void kill();
	void callback(GameObject* toCall, String callbackID);

	/// <summary>
	/// Precanned animations
	/// </summary>
	virtual void shake(float delay = 0);
	virtual void bubbleIn( float delay = 0);
	virtual void bubbleOut( float delay = 0);
	virtual void fadeIn(float delay = 0);
	virtual void fadeOut(float delay = 0);
	float bubbleInChildren(float delay = 0, float staggerDelay = 0);
	float bubbleOutChildren(float delay = 0, float staggerDelay = 0);
	void recursiveFadeIn(float delay = 0);
	void recursiveFadeOut(float delay = 0);



	/// <summary> 
	/// IComparable
	/// </summary> 
	static int zSortCompare( const void* first, const void* second );
	static int nameComparison( const void* first, const void* other);

	/// <summary> 
	///IRenderable
	/// </summary> 
	virtual int* 	getIndexBuffer();
	virtual float*  getVertexBuffer();
	virtual int 	getVertexCount();
	virtual int 	getIndexCount();
	virtual IMaterial* getMaterial();

	/// <summary> 
	/// Transform
	/// </summary> 
	Vec3f getWorldPosition();
	Vec3f getWorldScale();
	Vec3f getWorldRotation();
	Vec3f getPosition();
	Vec3f getScale();
	Vec3f getEulerRotation();
	Quaternion getRotation();

	/// <summary> 
	/// Settors
	/// </summary> 
	void setPosition(Vec3f pos);
	void setScale(Vec3f scale);
	void setRotation(Vec3f rotation);
	void setRotation(Quaternion rotation);
	void setOpacity( float op );
	void lookAt( GameObject* lookAt);
	void setVisible(bool visible);
	void setVisibleRecursive(bool visible);
	void setCullingLayer(int layer);
	void setCullingLayerRecursive(int layer);
	void setMaterial( IMaterial* mat );
	void setOriginalScale();
	virtual void setOriginal();

	/// <summary>
	/// recursive function to refresh localization
	/// </summary>
	void refreshLocalizedStringsRecursive();
	/// <summary>
	/// called on refresh localization
	/// </summary>
	virtual void onRefreshLocalization();

	/// <summary>
	/// Sets this object to a particular state
	/// </summary>
	virtual void loadGameObjectState( GameObjectState newState );

	virtual void saveGameObjectState();


	/// <summary>
	/// Called by CallBackAction
	/// </summary>
	virtual void onCallBack( String id );

	/// <summary>
	/// Sets this object to a particular state
	/// </summary>
	virtual void loadOriginalStateRecursive();

	virtual void saveOriginalStateRecursive();

	/// <summary> 
	/// Queries
	/// </summary> 
	float getOpacitySelf();
	float getOpacityHierarchy();
	bool isVisibleSelf();
	bool isVisibleHierarchy();
	int getCullingLayer();

	/// <summary> 
	/// For loading from data file
	/// </summary> 
	static GameObject* deSerialize( FILE* fp );

	/// <summary> 
	/// Debug
	/// </summary> 
	virtual String toString();
	String getScriptOverview();

	/// <summary> 
	/// Geometry
	/// </summary> 
	virtual Box3f getLocalBoundingBox();
	virtual Box3f getWorldBoundingBox();
	virtual Box3f getScreenBoundingBox(CameraObject* camera);
	virtual Box3f getParentBoundingBox();
	virtual Box3f getChildBounds();


	/// <summary>
	/// Custom int, often times objects want to tag custom data to a gameobject,
	/// without having to create a complex subclass. A tag is a useful way if its
	/// a simple id, lookup in a dictionary, etc...
	/// </summary>
	int tag;

private:
	virtual void computeLocalBounds();
	void privateInit();
	void debugTestUnityMatrix();

protected:
	/// <summary> 
	/// Protected in case an object changes its transform
	/// data outside the bounds of the settors
	/// </summary> 
	void markChildrenTransformsDirty();

	ObjectArray* actions;
	IMaterial* material;
	Box3f localBoundingBox;
    int cullingLayer;

    bool showLocalAxis;
    bool visible;
	float alpha;

public:
	GameObjectState originalState;
	float totalTimeElapsed;
	/// <summary> 
	/// Data
	/// </summary> 
	String name; //""
	String sceneGraphName;
	Transform transform;
	bool touchable;
	bool clickThrough;
    bool simulate;
    Vec3f originalScale;
    Vec3f originalPos;
};

}

#endif

#ifndef TEXTLABEL_H_
#define TEXTLABEL_H_

#include "SIEngine/Objects/GameObject.h"
#include "SIEngine/Resources/FontData.h"

LF_USING_BRIO_NAMESPACE()

namespace SICore
{
	enum TextAlignment
	{
		TextAlignmentLeft,
		TextAlignmentCenter,
		TextAlignmentRight
	};

	enum TextAnchor
	{
		TextAnchorUpperLeft,
		TextAnchorUpperCenter,
		TextAnchorUpperRight,
		TextAnchorMiddleLeft,
		TextAnchorMiddleCenter,
		TextAnchorMiddleRight,
		TextAnchorLowerLeft,
		TextAnchorLowerCenter,
		TextAnchorLowerRight
	};

/// <summary> 
/// Adds a bunch of spriteobjects as children to render
/// </summary> 
class TextLabel : public GameObject
{
	CLASSEXTENDS(TextLabel, GameObject);
	ADD_TO_CLASS_MAP;
public:
	TextLabel();
	TextLabel(String text);
	TextLabel(String text, TextAlignment alignment, TextAnchor pAnchor);
	TextLabel(FontData *font, String text);
	TextLabel(FontData *font, String text, TextAlignment alignment, TextAnchor pAnchor);
	virtual ~TextLabel();

	/// <summary>
	/// called on refresh localization
	/// </summary>
	virtual void onRefreshLocalization();

	/// <summary> 
	/// Gets text of this label
	/// </summary> 
	String getText();

	/// <summary> 
	/// Labels can change alignment, their text, and color
	/// </summary> 
	void removeLastCharacter();
	void setText(String text);
	void setText(String text, int maxLines);
	void setAlignment(TextAlignment alignment);
	void setTextAnchor(TextAnchor pAnchor);
	void setTextColor( Color newColor );
	void setOriginalColor(Color newColor);
	Color getTextColor();

	virtual void loadGameObjectState( GameObjectState newState );

	/// <summary>
	///IRenderable requirement, Index buffer, NULL if not implemented
	/// </summary>
	virtual int* 	getIndexBuffer();

	/// <summary>
	///IRenderable requirement, Vertex buffer, null if not implemented
	/// </summary>
	virtual float*  getVertexBuffer();

	/// <summary>
	/// IRenderable requirement, Vertex buffer, NULL if not implemented
	/// </summary>
	virtual int 	getVertexCount();

	/// <summary>
	///IRenderable requirement, Vertex buffer, -1 if not implemented
	/// </summary>
	virtual int 	getIndexCount();

	/// <summary> 
	/// Labels can have a max width or -1 for natural width,
	/// exceeding maxWidth will act as a carriage return
	/// </summary> 
	void setMaxWidth(int maxWidth);
	int getMaxWidth();

	/// <summary> 
	/// Fade characters that are typed
	/// </summary> 
	void fadeLastCharacterIn();
	void fadeLastCharacterOut();
	void colorTo( Color dest, float time );
	void colorTo( Color dest);
	void changeTo(String newText);

	/// <summary>
	/// Precanned animations
	/// </summary>
	virtual void fadeIn(float delay = 0);
	virtual void fadeOut(float delay = 0);

	/// <summary> 
	/// Load from file
	/// </summary> 
	static TextLabel* deSerialize(FILE* fp);

	SpriteData* getSpriteData();
	void setBatched(bool batched);

	virtual void setOriginal();
private:
	void setFont(FontData *font);
	void updateText();

	SpriteData* spriteData;
	LitTexturedPositionVertex* vertices;
	int vertexCount;

	Color labelColor;
	Color originalColor;
	String text;
	FontData *font;
	TextAlignment align;
	TextAnchor	anchor;
	int maxLineWidth;
	int maxLineNum;
	bool batched;
};

}

#endif /* TEXTLABEL_H_ */

#include "SpriteObject.h"
#include "SIEngine/Include/SIActions.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIMaterials.h"

namespace SICore
{

SpriteDrawMode* SpriteObject::g_SpriteDrawMode = new SpriteDrawMode();

//*****************************************************************************
// LifeCycle
//*****************************************************************************
SpriteObject::SpriteObject( SpriteData* data ) : super()
{
	spriteData = data;
	spriteData->addRef();
	init();
}

SpriteObject::SpriteObject() : super()
{
	spriteData = null;
	init();
}

SpriteObject::~SpriteObject()
{
	//printf("SpriteObject::~SpriteObject\n");
	SAFE_RELEASE(spriteData);
	SAFE_RELEASE(material);
}

void SpriteObject::setSpriteData(SpriteData* spriteData_ )
{
	SAFE_RELEASE(spriteData);
	spriteData = spriteData_;
	spriteData->addRef();
	init();
}

void SpriteObject::setColor(Color newColor)
{ tintColor = newColor; }

Color SpriteObject::getColor()
{ return tintColor; }

//*****************************************************************************
// Common init
//*****************************************************************************
void SpriteObject::init()
{
	currentFrame = 0;
	alpha = 1;
	width = 1;
	height = 1;
	tintColor = Color::WhiteOpaque;
	material = new TransparentSpriteMaterial((SpriteObject*)this);
	updateVertices();
	updateUVs();
}

//*****************************************************************************
// Update UVs for animation based on when sprite data changes our vert uv data
//*****************************************************************************
void SpriteObject::updateUVs()
{
	if( spriteData != NULL )
	{
		SpriteFrame *currentFrame = spriteData->getCurrentFrame();
		vertices[0].uv = currentFrame->getUV(0);
		vertices[1].uv = currentFrame->getUV(1);
		vertices[2].uv = currentFrame->getUV(2);

		vertices[3].uv = currentFrame->getUV(0);
		vertices[4].uv = currentFrame->getUV(2);
		vertices[5].uv = currentFrame->getUV(3);
	}
}

//************************************************************************
// Sprite: Create data representing its' uv data
//************************************************************************
SpriteObject* SpriteObject::deSerialize( FILE* fp )
{
	SpriteObject* newObject = NULL;
	char textureName[250];
	float width, height;
	float xmin, ymin, xmax, ymax;
	int animationCount = 0;
	//Transform: 9, UV Rect: 4 Dimensions: 2 Name: 1
	if( fscanf(fp, "%249s %f %f %f %f %f %f %d", textureName, &width, &height, &xmin, &ymin, &xmax, &ymax, &animationCount ) == 8 )
	{
		SpriteData* data = new SpriteData(textureName);
		data->setStaticFrame(TRelease<SpriteFrame>(new SpriteFrame(xmin, ymin, xmax, ymax)));

		for( int i = 0; i<animationCount; i++ )
		{
			char animationName[100];
			if ( fscanf( fp, "%99s", animationName ) == 1 )
			{
				FILE* animationFile = AssetLibrary::getAnimationFile(animationName);
				TRelease<AnimationData> animData( AnimationData::deSerialize(animationFile));
				data->addAnimation(animData);
				fclose(animationFile);
			}
		}

		newObject = new SpriteObject(data);
		((SpriteObject*)newObject)->setSize( Size((float)width, (float)height) );
		data->release();
	}
	return newObject;
}

//*****************************************************************************
// For help in testing
//*****************************************************************************
void SpriteObject::debugLogVerts()
{
	for( int i = 0; i<6; i++ )
	{
		printf("Vert%d : p:<%f,%f,%f> uv:<%f,%f> color:<%f,%f,%f,%f>\n", i,
				(double)vertices[i].position.x, (double)vertices[i].position.y, (double)vertices[i].position.z,
				(double)vertices[i].uv.u, (double)vertices[i].uv.v,
				(double)vertices[i].color.r, (double)vertices[i].color.g, (double)vertices[i].color.b, (double)vertices[i].color.a);
	}
}

//*****************************************************************************
// Helper function that creates a single framed spriteObject
//*****************************************************************************
SpriteObject* SpriteObject::createSprite(SpriteData* data, String name, Size size)
{
	SpriteObject *newObject = new SpriteObject(data);
	newObject->name = name;
	newObject->setSize(size);
	return newObject;
}

//*****************************************************************************
// Reconfigure vertices in case width, height change
//*****************************************************************************
void SpriteObject::updateVertices()
{
	vertices[0].position.x = -.5f * width;
	vertices[0].position.y = -.5f * height;
	vertices[0].position.z = 0;
	vertices[0].color = Color::Red;

	vertices[1].position.x = -.5f * width;
	vertices[1].position.y = .5f * height;
	vertices[1].position.z = 0;
	vertices[1].color = Color::Red;

	vertices[2].position.x = .5f* width;
	vertices[2].position.y = .5f * height;
	vertices[2].position.z = 0;
	vertices[2].color = Color::Red;

	vertices[3].position.x = -.5f * width;
	vertices[3].position.y = -.5f * height;
	vertices[3].position.z = 0;
	vertices[3].color = Color::Red;

	vertices[4].position.x = .5f * width;
	vertices[4].position.y = .5f * height;
	vertices[4].position.z = 0;
	vertices[4].color = Color::Red;

	vertices[5].position.x = 0.5f * width;
	vertices[5].position.y = -0.5f * height;
	vertices[5].position.z = 0;
	vertices[5].color = Color::Red;
}

void SpriteObject::setVertColors(Color c)
{
	for( int i = 0; i<6; i++ )
	{ vertices[i].color = c; }
}

void SpriteObject::setFrame(int frame)
{
	spriteData->setFrame(frame);
}


void SpriteObject::playAnimation( String animationName )
{
	spriteData->startAnimation(animationName);
}

//*****************************************************************************
// Game Object constructs
//*****************************************************************************
bool SpriteObject::processInput( IEventMessage *message)
{ return super::processInput(message); }

bool SpriteObject::hitTest(int x, int y)
{
	//*****************************************************************************
	// We could also have a 2nd hittest mode pixelperfect, which looks up the pixel at the
	// texture where localPoint transforms into UV point to see if texture has valid pixel: alpha > 0
	//*****************************************************************************
	Vec3f localPoint = Vec3f(x, y, 0);
	localPoint = transform.getWorldToLocal().multiplyPoint(localPoint);
	return localBoundingBox.contains( localPoint);
}

//*****************************************************************************
// Output our material settings for this object
//*****************************************************************************
String SpriteObject::toString()
{
	return String(getCullingLayer());
}

void SpriteObject::render()
{
	if( spriteData->getTexture() == NULL )
	{
		//do nothing
	}
	else
	{
		super::render();
	}
}

void SpriteObject::update(float dt)
{
	spriteData->update(dt);
	updateUVs();
	super::update(dt);
}

//*****************************************************************************
// Content dimensions
//*****************************************************************************
void SpriteObject::setSize( Size size )
{
	width = size.width_;
	height = size.height_;
	updateVertices();
}

Size SpriteObject::getSize() {
	return Size(width, height);
}

void SpriteObject::setWidth( float w )
{
	width = w;
	updateVertices();
}
void SpriteObject::setHeight( float h )
{
	height = h;
	updateVertices();
}

//*****************************************************************************
// LocalBounds for this object
//*****************************************************************************
void SpriteObject::computeLocalBounds()
{
	localBoundingBox = Box3f( Vec3f::Zero, Vec3f(width, height, 1));
}

SpriteData* SpriteObject::getSpriteData()
{ return spriteData; }

//*****************************************************************************
// IRenderable
//*****************************************************************************
//Not using index drawing
int	SpriteObject::getIndexCount()
{ return 0; }

//Not using index drawing
int* SpriteObject::getIndexBuffer()
{ return NULL; }

//VertexBuffer is the first float for our first position vertex
float* SpriteObject::getVertexBuffer()
{ return &vertices[0].position.x; }

//Really we know it is 6, but for whatever reason if it changes
int SpriteObject::getVertexCount()
{ return 6; }


LitTexturedPositionVertex SpriteObject::getVertex(int index)
{ return vertices[index]; }

//******************************************************
// Actions for sprites
//******************************************************
void SpriteObject::colorTo( Color dest, float time )
{
	queueAction(TRelease<ColorToAction>( new ColorToAction(this,dest,time, EaseFunctions::cubicEaseInOut)));
}

/// <summary>
/// Precanned animations
/// </summary>
void SpriteObject::fadeIn(float delay)
{
	waitFor(delay);
	makeVisible(true);
	fadeTo(1,0.25f);
}

/// <summary>
/// Precanned animations
/// </summary>
void SpriteObject::fadeOut(float delay)
{
	waitFor(delay);
	fadeTo(0,0.25f);
	makeVisible(false);
}

void SpriteObject::setOriginal()
{
	setColor(Color::WhiteOpaque);
	super::setOriginal();
}

}

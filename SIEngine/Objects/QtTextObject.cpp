#include "QtTextObject.h"
#include "SIEngine/Include/SIManagers.h"
namespace SICore
{

QtTextObject::QtTextObject(String defaultText, QtTextAlignment align, U32 col ) : super()
{
	alignment = align;
	labelText = defaultText;
	isUnderlined = false;
	qtColor = col;
}

void QtTextObject::setQtColor(U32 c)
{ qtColor = c; }

U32 QtTextObject::getQtColor()
{ return qtColor; }

String QtTextObject::getLabelText()
{ return labelText; }

QtTextAlignment QtTextObject::getAlignment()
{ return alignment; }

/// <summary>
/// Load from file
/// </summary>
QtTextObject* QtTextObject::deSerialize( FILE* fp )
{
	char startingText[1000];
	int alignment;
	float width;
	float r, g, b, a;

	fscanf(fp, "%*[^\"]%*c%[^\"]\" %d %f %f %f %f %f ", startingText, &alignment, &width, &r, &g, &b, &a);

	int ri = 255*r;
	int gi = 255*g;
	int bi = 255*b;
	int ai = 255*a;

	U32 argb = ((ai&0x0ff)<<24)|( (ri&0x0ff)<<16)|((gi&0x0ff)<<8)|(bi&0x0ff);
	return new QtTextObject( startingText, (QtTextAlignment)alignment, argb );
}

/// <summary>
/// called on refresh localization
/// </summary>
void QtTextObject::onRefreshLocalization()
{
	String key = sceneGraphName;
	String locString = StringsManager::getInstance()->getLocalizedString(key);
	String prevString = getLabelText();
	if(!prevString.equals(locString))
	{
		setText(locString);
	}
}

void QtTextObject::setText( String text )
{ labelText = text; }

QtTextObject::~QtTextObject()
{ }

}

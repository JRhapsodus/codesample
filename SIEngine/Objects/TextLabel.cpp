#include "TextLabel.h"
#include "SIEngine/Objects/SpriteObject.h"
#include "SIEngine/Actions/ColorToAction.h"
#include "SIEngine/Actions/ChangeLabelAction.h"
#include "SIEngine/Include/SIMaterials.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIUtils.h"

namespace SICore
{

TextLabel::TextLabel() : super()
{
	font= NULL;
	text = "";
	align = TextAlignmentLeft;
	anchor = TextAnchorUpperLeft;
	maxLineNum = 0;
	maxLineWidth = -1;
	vertices = NULL;
	batched = true;
	spriteData = null;
	FontData *theFont = FontData::defaultFont();
	setFont(theFont);
}

TextLabel::TextLabel(String text_) : super()
{
	font= NULL;
	text = text_;
	align = TextAlignmentLeft;
	anchor = TextAnchorUpperLeft;
	maxLineNum = 0;
	maxLineWidth = -1;
	vertices = NULL;
	batched = true;
	spriteData = null;
	FontData *theFont = FontData::defaultFont();
	setFont(theFont);
	markChildrenTransformsDirty();

	refreshLocalizedStringsRecursive();
}

TextLabel::TextLabel(String text_, TextAlignment alignment, TextAnchor pAnchor) : super()
{
	font= NULL;
	text = text_;
	align = alignment;
	anchor = pAnchor;
	maxLineNum = 0;
	maxLineWidth = -1;
	vertices = NULL;
	batched = true;
	spriteData = null;
	FontData *theFont = FontData::defaultFont();
	setFont(theFont);
}

TextLabel::TextLabel(FontData *font_, String text_) : super()
{
	font= NULL;
	text = text_;
	align = TextAlignmentLeft;
	anchor = TextAnchorUpperLeft;
	maxLineNum = 0;
	maxLineWidth = -1;
	vertices = NULL;
	batched = true;
	spriteData = null;
	setFont(font_);
}

TextLabel::TextLabel(FontData *font_, String text_, TextAlignment alignment, TextAnchor pAnchor) : super()
{
	font= NULL;
	text = text_;
	align = alignment;
	anchor = pAnchor;
	maxLineNum = 0;
	maxLineWidth = -1;
	vertices = NULL;
	batched = true;
	spriteData = null;
	setFont(font_);
}

TextLabel::~TextLabel()
{
	delete [] vertices;
	SAFE_RELEASE(spriteData);
	SAFE_RELEASE(font);
}

/// <summary>
/// called on refresh localization
/// </summary>
void TextLabel::onRefreshLocalization()
{

	String key = sceneGraphName;
	String locString = StringsManager::getInstance()->getLocalizedString(key);
	String prevString = getText();


	if(!prevString.equals(locString) && !locString.isEmpty())
	{
		setText(locString);
	}
}

void TextLabel::removeLastCharacter()
{
	text = text.removeLastChar();
	fadeLastCharacterOut();
}

void TextLabel::setText(String text_)
{
	if( text_ == "" )
	{ setBatched(false); }

	text = text_;
	children->removeAllElements();
	updateText();
	setTextColor(labelColor);
}

void TextLabel::setText(String text_, int maxLines)
{
	if( text_ == "" )
	{ setBatched(false); }

	maxLineNum = maxLines;
	text = text_;
	children->removeAllElements();
	updateText();
	setTextColor(labelColor);
}

void TextLabel::loadGameObjectState( GameObjectState newState )
{
	super::loadGameObjectState(newState);
	setTextColor(originalColor);
}

String TextLabel::getText()
{ return text; }

//******************************************************
// "Label Text" red green blue alpha
//******************************************************
TextLabel* TextLabel::deSerialize(FILE* fp)
{
	TextLabel* newObject = NULL;
	char labelText[1000]; //becomes labelKey
	char fontName[500];
	float width;
	float r,g,b,a;
	int alignment;
	int anchor;

	if(fscanf(fp, "%*[^\"]%*c%[^\"]\" %d %d %f %f %f %f %f %499s", labelText, &alignment, &anchor, &width, &r, &g, &b, &a, fontName) == 9)
	{
//		printf( "%s %s %f %f %f %f %f %d %d\n", labelText, fontName, width, r, g, b, a, alignment, anchor);
		FontData* fontData = new FontData(fontName);
		newObject = new TextLabel(fontData, labelText, (TextAlignment)alignment, (TextAnchor)anchor);
		Color color = Color(r,g,b,a);
		newObject->setOriginalColor(color);
		newObject->setTextColor(color);
		newObject->setMaxWidth(width);
		fontData->release();
	}
	else
	{ ThrowException::deserializeException("TextLabel"); }

	return newObject;
}

//******************************************************
// Fades last character in
//******************************************************
void TextLabel::fadeLastCharacterIn()
{
	// Don't fade if last char was a space
	char lastChar = text.charAt(text.length()-1);
	if (lastChar == ' ') return;

	SpriteObject* lastCharacter = (SpriteObject*)(((GameObject*)children->lastElement())->getChildren()->lastElement());
	if( lastCharacter != null )
	{
		lastCharacter->setColor(Color(labelColor.r, labelColor.g, labelColor.b, 0));
		lastCharacter->colorTo(Color(labelColor.r, labelColor.g, labelColor.b, 1.0f), 0.18f);
	}
}

//******************************************************
// Fades last character out
//******************************************************
void TextLabel::fadeLastCharacterOut()
{
	SpriteObject* lastCharacter = (SpriteObject*)(((GameObject*)children->lastElement())->getChildren()->lastElement());

	if( lastCharacter != null )
	{
		lastCharacter->colorTo(Color(labelColor.r, labelColor.g, labelColor.b, 0.0f), 0.18f);
		lastCharacter->kill();
	}
}

Color TextLabel::getTextColor()
{ return labelColor; }

void TextLabel::setTextColor( Color newColor )
{
	labelColor = newColor;
	for( int i = 0; i<children->getSize(); i++ )
	{
		GameObject* line = (GameObject*)children->elementAt(i);
		ObjectArray *glyphs = line->getChildren();
		for (int j = 0; j < glyphs->getSize(); j++) {
			SpriteObject *characterSprite = (SpriteObject *)glyphs->elementAt(j);
			characterSprite->setColor(newColor);
		}
	}
}

void TextLabel::setOriginalColor(Color newColor)
{
	originalColor = newColor;
}

// Release all glyph sprites
void TextLabel::setFont(FontData *font_)
{
	children->removeAllElements();
	SAFE_RELEASE(font);

	font = font_;
	font->addRef();
	updateText();
}

void TextLabel::changeTo(String newText)
{
	TRelease<ChangeLabelAction> changeLabel( new ChangeLabelAction(this,newText) );
	queueAction(changeLabel);
}

void TextLabel::setAlignment(TextAlignment alignment)
{
	/*String n = "";
	switch(alignment)
	{
		case TextAlignmentLeft: n = "left"; break;
		case TextAlignmentCenter: n = "center"; break;
		case TextAlignmentRight: n = "right"; break;
	}
	printf("aligment: %s\n", n.str());*/
	align = alignment;
	children->removeAllElements();
	updateText();
}

void TextLabel::setTextAnchor(TextAnchor pAnchor)
{
	/*String n = "";
	switch(pAnchor)
	{
		case TextAnchorUpperLeft: n = "upperLeft"; break;
		case TextAnchorUpperCenter: n = "upperCenter"; break;
		case TextAnchorUpperRight: n = "upperRight"; break;
		case TextAnchorMiddleLeft: n = "middleLeft"; break;
		case TextAnchorMiddleCenter: n = "middleCenter"; break;
		case TextAnchorMiddleRight: n = "middleRight"; break;
		case TextAnchorLowerLeft: n = "lowerLeft"; break;
		case TextAnchorLowerCenter: n = "lowerCenter"; break;
		case TextAnchorLowerRight: n = "lowerRight"; break;

	}
	printf("anchor: %s\n", n.str());*/
	anchor = pAnchor;
	children->removeAllElements();
	updateText();
}

void TextLabel::setMaxWidth(int maxWidth)
{
	//printf("Max TextLabel Width %d\n", maxWidth);
	maxLineWidth = maxWidth;
	updateText();
}

int TextLabel::getMaxWidth()
{ return maxLineWidth; }

SpriteData* TextLabel::getSpriteData()
{ return spriteData; }

/// <summary>
///IRenderable requirement, Index buffer, NULL if not implemented
/// </summary>
int* TextLabel::getIndexBuffer()
{ return NULL; }

/// <summary>
///IRenderable requirement, Vertex buffer, null if not implemented
/// </summary>
float* TextLabel::getVertexBuffer()
{ return &vertices[0].position.x; }

/// <summary>
/// IRenderable requirement, Vertex buffer, NULL if not implemented
/// </summary>
int TextLabel::getVertexCount()
{ return vertexCount; }

/// <summary>
///IRenderable requirement, Vertex buffer, -1 if not implemented
/// </summary>
int TextLabel::getIndexCount()
{ return -1; }

void TextLabel::setBatched(bool batch)
{
	batched = batch;
	if( !batched )
	{
		SAFE_RELEASE(material);
		SAFE_RELEASE(spriteData);
		delete [] vertices;
		vertices = null;
	}
}

void TextLabel::updateText()
{
	//printf("TextLabel::updateText()\n");
	ObjectArray *lines = NULL;

	if (maxLineWidth <= 0)
	{ lines = font->generateLines(text); }
	else
	{ lines = font->generateLinesWithMaxWidth(text, maxLineWidth, maxLineNum); }

	int numLines = lines->getSize();
	float totalHeight =  numLines == 1 ? font->getBaseHeight() : numLines * font->getLineHeight();
	float maxWidth = 0;
	for( int i = 0; i<lines->getSize(); i++ )
	{
		GameObject* line = (GameObject*)lines->elementAt(i);
		Box3f bounds = line->getChildBounds();
		if(bounds.getWidth() > maxWidth)
			maxWidth = bounds.getWidth();
	}


	if( batched )
	{
		//remove old vertices
		if( vertices != null )
		{ delete [] vertices; }

		SAFE_RELEASE(spriteData);

		//create new verts based on all the children
		vertexCount = 0;
		for( int i = 0; i<lines->getSize(); i++ )
		{ vertexCount += ((GameObject*)lines->elementAt(i))->numChildren() * 6; }

		vertices = new LitTexturedPositionVertex[vertexCount];

		//Loop each line of the label
		int index      = 0;
		for( int i = 0; i<lines->getSize(); i++ )
		{
			GameObject* line = (GameObject*)lines->elementAt(i);
			Box3f bounds = line->getChildBounds();

			//vertex index at start of line
			int lineIndex = index;

			//loop each cahracter
			for ( int k = 0; k<line->numChildren(); k++ )
			{
				SpriteObject* glyphSprite = (SpriteObject*)line->childAtIndex(k);

				//Loop vertices
				for( int j = 0; j<6; j++ )
				{
					vertices[index] = glyphSprite->getVertex(j);
					vertices[index].position.x += glyphSprite->getPosition().x;
					vertices[index].position.y += glyphSprite->getPosition().y - (i*font->lineHeight);
					index++;
				}

				if( spriteData == null )
				{
					spriteData = new SpriteData(glyphSprite->getSpriteData()->getTextureName());
					setMaterial( TRelease<TextMaterial>(new TextMaterial(this)) );
				}
			}


			//apply alignment

			for ( int k = 0; k<line->numChildren(); k++ )
			{
				for( int j = 0; j<6; j++ )
				{
					switch(align)
					{
						case TextAlignmentLeft:		{}															break;
						case TextAlignmentCenter:	{ vertices[lineIndex].position.x -= (bounds.getWidth()/2);}	break;
						case TextAlignmentRight:	{ vertices[lineIndex].position.x -= bounds.getWidth();}		break;
					}

					int xPositionOffset = 0;
					int yPositionOffset = 0;
					float lineWidth = numLines == 0 ? bounds.getWidth() : maxWidth;
					switch(anchor)
					{
						case TextAnchorUpperLeft:
						{
							xPositionOffset = align == TextAlignmentLeft ? 0 : align == TextAlignmentRight ? lineWidth : lineWidth * 0.5f;
						}
						break;

						case TextAnchorUpperCenter:
						{
							xPositionOffset = align == TextAlignmentLeft ? -lineWidth * 0.5f : align == TextAlignmentRight ? lineWidth * 0.5f : 0;
							yPositionOffset = 0;
						}
						break;

						case TextAnchorUpperRight:
						{
							xPositionOffset = align == TextAlignmentLeft ? -lineWidth : align == TextAlignmentRight ? 0 : -lineWidth*0.5f;
							yPositionOffset = 0;
						}
						break;

						case TextAnchorMiddleLeft:
						{
							xPositionOffset = align == TextAlignmentLeft ? 0 : align == TextAlignmentRight ? lineWidth : lineWidth * 0.5f;
							yPositionOffset = -totalHeight/2.0f;
						}
						break;

						case TextAnchorMiddleCenter:
						{
							xPositionOffset = align == TextAlignmentLeft ? -lineWidth * 0.5f : align == TextAlignmentRight ? lineWidth * 0.5f : 0;
							yPositionOffset = -totalHeight/2.0f;
						}
						break;

						case TextAnchorMiddleRight:
						{
							xPositionOffset = align == TextAlignmentLeft ? -lineWidth : align == TextAlignmentRight ? 0 : -lineWidth*0.5f;
							yPositionOffset = -totalHeight/2.0f;
						}
						break;

						case TextAnchorLowerLeft:
						{
							xPositionOffset = align == TextAlignmentLeft ? 0 : align == TextAlignmentRight ? lineWidth : lineWidth * 0.5f;
							yPositionOffset = -totalHeight;
						}
						break;

						case TextAnchorLowerCenter:
						{
							xPositionOffset = align == TextAlignmentLeft ? -lineWidth * 0.5f : align == TextAlignmentRight ? lineWidth * 0.5f : 0;
							yPositionOffset = -totalHeight;
						}
						break;

						case TextAnchorLowerRight:
						{
							xPositionOffset = align == TextAlignmentLeft ? -lineWidth : align == TextAlignmentRight ? 0 : -lineWidth*0.5f;
							yPositionOffset = -totalHeight;
						}
						break;

						default:break;
					}

					vertices[lineIndex].position.x += xPositionOffset;
					vertices[lineIndex].position.y -= yPositionOffset;

					lineIndex++;
				}
			}
		}

		lines->release();
	}
	else
	{
		for (int i = 0; i < numLines; i++)
		{
			int xPosition = 0;
			int yPosition = -i * font->getLineHeight();
			int xPositionOffset = 0;
			int yPositionOffset = 0;

			GameObject *line = (GameObject *)lines->elementAt(i);
			line->setCullingLayerRecursive(getCullingLayer());
			addChild(line);
			Box3f bounds = line->getChildBounds();

			//alignment only affects relative x positioning
			switch(align)
			{
				case TextAlignmentLeft:		{ xPosition = 0;}						break;
				case TextAlignmentCenter:	{ xPosition = -bounds.getWidth()/2.0f;}	break;
				case TextAlignmentRight:	{ xPosition = -bounds.getWidth();}		break;
				default: break;
			}

			//calculate offset based on anhcor
			float lineWidth = numLines == 0 ? bounds.getWidth() : maxWidth;
			switch(anchor)
			{
				case TextAnchorUpperLeft:
				{
					xPositionOffset = align == TextAlignmentLeft ? 0 : align == TextAlignmentRight ? lineWidth : lineWidth * 0.5f;
				}
				break;

				case TextAnchorUpperCenter:
				{
					xPositionOffset = align == TextAlignmentLeft ? -lineWidth * 0.5f : align == TextAlignmentRight ? lineWidth * 0.5f : 0;
					yPositionOffset = 0;
				}
				break;

				case TextAnchorUpperRight:
				{
					xPositionOffset = align == TextAlignmentLeft ? -lineWidth : align == TextAlignmentRight ? 0 : -lineWidth*0.5f;
					yPositionOffset = 0;
				}
				break;

				case TextAnchorMiddleLeft:
				{
					xPositionOffset = align == TextAlignmentLeft ? 0 : align == TextAlignmentRight ? lineWidth : lineWidth * 0.5f;
					yPositionOffset = totalHeight/2.0f;
				}
				break;

				case TextAnchorMiddleCenter:
				{
					xPositionOffset = align == TextAlignmentLeft ? -lineWidth * 0.5f : align == TextAlignmentRight ? lineWidth * 0.5f : 0;
					yPositionOffset = totalHeight/2.0f;
				}
				break;

				case TextAnchorMiddleRight:
				{
					xPositionOffset = align == TextAlignmentLeft ? -lineWidth : align == TextAlignmentRight ? 0 : -lineWidth*0.5f;
					yPositionOffset = totalHeight/2.0f;
				}
				break;

				case TextAnchorLowerLeft:
				{
					xPositionOffset = align == TextAlignmentLeft ? 0 : align == TextAlignmentRight ? lineWidth : lineWidth * 0.5f;
					yPositionOffset = totalHeight;
				}
				break;

				case TextAnchorLowerCenter:
				{
					xPositionOffset = align == TextAlignmentLeft ? -lineWidth * 0.5f : align == TextAlignmentRight ? lineWidth * 0.5f : 0;
					yPositionOffset = totalHeight;
				}
				break;

				case TextAnchorLowerRight:
				{
					xPositionOffset = align == TextAlignmentLeft ? -lineWidth : align == TextAlignmentRight ? 0 : -lineWidth*0.5f;
					yPositionOffset = totalHeight;
				}
				break;

				default:break;
			}


			line->setPosition(Vec3f(xPosition + xPositionOffset, yPosition + yPositionOffset, 0));
		}

		lines->release();
	}
}

//******************************************************
// Actions for labels
//******************************************************
void TextLabel::colorTo( Color dest, float time )
{
	queueAction(TRelease<ColorToAction>( new ColorToAction(this,dest,time, EaseFunctions::cubicEaseInOut)));
}

void TextLabel::colorTo( Color dest)
{
	queueAction(TRelease<ColorToAction>( new ColorToAction(this,dest,0, EaseFunctions::cubicEaseInOut)));
}

/// <summary>
/// Precanned animations
/// </summary>
void TextLabel::fadeIn(float delay)
{

	Color color = getTextColor();
	waitFor(delay);
	color.a = 0;
	setTextColor( color );
	makeVisible(true);
	color.a = 1;
	colorTo( color, 0.25f );

}

/// <summary>
/// Precanned animations
/// </summary>
void TextLabel::fadeOut(float delay)
{
	Color color = getTextColor();
	color.a = 0;
	waitFor(delay);
	colorTo( color, 0.25f );
	makeVisible(false);
}

void TextLabel::setOriginal()
{
	setTextColor(originalColor);
	super::setOriginal();
}

}

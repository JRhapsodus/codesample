#ifndef INCLUDE_CAMERAOBJECT_H_
#define INCLUDE_CAMERAOBJECT_H_

#include "SIEngine/Objects/GameObject.h"

namespace SICore
{

class CameraObject : public GameObject
{
	CLASSEXTENDS(CameraObject, GameObject);
	ADD_TO_CLASS_MAP;
public:
	CameraObject();
	~CameraObject();

	/// <summary> 
	/// Creates a camera with Perspective or Orthograph projections: Left & Right hand
	/// </summary> 
	static CameraObject *perspective(bool leftHanded);
	static CameraObject *orthographic(bool leftHanded);
	void setupForOrthographic(bool isLeft);
	void setupForPerspective(bool isLeft);

	/// <summary> 
	/// Culling layers
	/// </summary> 
	int getCullingMask();
	void setCullingMask( int cullingMask );

	/// <summary> 
	/// View matrix for MVP model
	/// </summary> 
	Matrix getViewMatrix();

	/// <summary> 
	/// Load via file
	/// </summary> 
	static CameraObject* deSerialize(FILE* fp);


	/// <summary> 
	/// Compares cameras based on their rendering order
	/// </summary> 
	static int compareByRank( const void* first, const void* second );

	/// <summary> 
	/// GameObject
	/// </summary> 
	virtual void computeLocalBounds();
	Frustum getFrustum();

	/// <summary> 
	/// Data
	/// </summary> 
	GLbitfield clearOptions;
	Color clearColor;
	bool leftHanded;
	int renderOrder;
	bool isOrthographic;
	Matrix projection;

protected:
	int cullingLayerMask;
};

}
#endif

#include "SIEngine/Core/StringClass.h"
#include <StringTypes.h>

LF_USING_BRIO_NAMESPACE();

namespace SICore
{

CriticalSection   				BaseObject::s_critical;
bool 		      				BaseObject::s_bMainCritInit;
int 			  				BaseObject::gObjectCount = 0;
int 			  				BaseObject::gRefCount = 0;
std::map<std::string, int> 		BaseObject::gRefCountMap;

//**********************************************************************
// Ref counting for life cycle
//**********************************************************************
BaseObject::BaseObject()
{
	syncCount = 0;
	lockThread = 0;
	refCount = 1;
	gRefCount++;
	gObjectCount++;
	m_event = null;
	m_bCsMade = false;

	//*********************************************************************
	// we must make sure we create an object before creating any threads
	//*********************************************************************
	if (!s_bMainCritInit)
	{
		s_critical.initialize();
		s_bMainCritInit = true;
	}
}

BaseObject::~BaseObject()
{
	if( m_event != null )
	{ destroySemaphore(m_event); }
}

String BaseObject::toString()
{ return S("BaseObject h{") + hashValue() + S("}"); }

int BaseObject::hashValue() const
{ return 0; }

int BaseObject::getRefCount()
{ return refCount; }

void BaseObject::addRef()
{
	//__sync_add_and_fetch( &refCount, 1);
	refCount++;
	gRefCount++;
}

void BaseObject::release()
{
	if( this == NULL )
	{ return; }

	//__sync_fetch_and_sub( &refCount, 1);
	refCount--;
	gRefCount--;

	if( refCount <= 0 )
	{ delete this; }
}

bool BaseObject::isLockedByCurrentThread()
{ return (lockThread == pthread_self()); }

bool BaseObject::isLockedByAnotherThread()
{ return (!isLockedByCurrentThread() && syncCount > 0 ); }

void BaseObject::sync()
{
	//***********************************************************
	// Wait for semaphore: should NOT be called except by Lock
	//***********************************************************
	if (!m_bCsMade)
	{
		s_critical.enter();
		if (!m_bCsMade)
		{
			m_cs.initialize();
			m_bCsMade = true;
		}
		s_critical.leave();
	}

	m_cs.enter();
	syncCount++;
	lockThread = pthread_self();
}

void BaseObject::unsync()
{
	if ( !isLockedByCurrentThread() )
	{  throw "Error unsync"; }

	if( --syncCount == 0 )
	{ lockThread = 0; }

	m_cs.leave();
}

void BaseObject::wait()
{ wait(0xFFFFFFFF); }

void BaseObject::wait( long ms )
{
	if ( !isLockedByCurrentThread() )
	{  throw "Error wait"; }

	if (m_event == null)
	{
		if (m_event == null)
		{ m_event = createSemaphore(true,false); }
	}

	resetSemaphore( m_event );

	//*****************************************************************************
	// release Counted semaphore while we wait
	// TODO: really I would like to see typed semaphores:
	// BinarySemaphore: public Semaphore and CountedSemaphore : public Semaphore
	//*****************************************************************************
	int nSyncCount = syncCount;
	for( int i=0 ; i < nSyncCount; i++ )
	{ unsync(); }

	waitForSemaphore( m_event, ms );

	//*****************************************************************************
	// get the semaphore back with the locked mutex count that we had before.
	//*****************************************************************************
	for( int i=0 ; i < nSyncCount; i++ )
	{ sync(); }
}

void BaseObject::notify()
{
	if ( !isLockedByCurrentThread() )
	{  throw "Error notify"; }

	if (m_event == NULL)
		return;

	setSemaphore( m_event );
}

void BaseObject::notifyAll()
{ notify(); }

//**********************************************************************
// Cloneable objects return new instances with same member data
//**********************************************************************
ICloneable* BaseObject::clone()
{
	BaseObject* newObject = new BaseObject();
	return newObject;
}

//**********************************************************************
// Comparable objects can be compared to one another via different funcs
//**********************************************************************
int BaseObject::compareTo( IComparable* other, CompareFunction functionToCompare ) const
{ return functionToCompare( (void*)this, (void*)other); }

//**********************************************************************
// Compare two base object's based on memory address
//**********************************************************************
int BaseObject::memoryAddressComparison( const void* first, const void* other)
{
	if ( (BaseObject*)first == (BaseObject*)other )
	{ return 0; }

	return -1;
}

//**********************************************************************
// Helper function for the equals operator
//**********************************************************************
bool BaseObject::equals( BaseObject& otherObj ) const
{ return ( compareTo( (IComparable*)&otherObj, &BaseObject::memoryAddressComparison) == 0 ) ? true : false; }

}

#ifndef QTSPRITEOBJECT_H_
#define QTSPRITEOBJECT_H_

#include "SIEngine/Objects/GameObject.h"

namespace SICore
{

class QtSpriteObject: public GameObject
{
	CLASSEXTENDS(QtSpriteObject, GameObject);
public:
	QtSpriteObject(float width, float height, String texture);
	virtual ~QtSpriteObject();

	/// <summary>
	/// Load from file
	/// </summary>
	static QtSpriteObject* deSerialize( FILE* fp );

	/// <summary>
	/// In 3d world, content width is based on the local verts, here we track separately
	/// </summary>
	Size getContentSize();

	/// <summary>
	/// Overriden because in a blitting renderer, our dimensions behave different
	/// without a proper transform matrix
	/// </summary>
	virtual Box3f getLocalBoundingBox();

	/// <summary>
	/// Overriden because in a blitting renderer, our dimensions behave different
	/// without a proper transform matrix
	/// </summary>
	virtual Box3f getWorldBoundingBox();

	/// <summary>
	/// Overriden because in a blitting renderer, our dimensions behave different
	/// without a proper transform matrix
	/// </summary>
	virtual Box3f getScreenBoundingBox(CameraObject* camera);

	/// <summary>
	/// The buffer QtRenderer requires (really this is a texture), but our main Texture
	/// class is setup to deal with a 3d rendering pipeline. I would like to move this
	/// into the same interface as a subclass 2DTexture
	/// </summary>
	CBlitBuffer* getBlitBuffer();

	/// <summary>
	/// Loads the blit buffer
	/// </summary>
	virtual void loadTexture();

	/// <summary>
	/// This should be a subclassed feature
	/// </summary>
	void setClipRectBasedOnPercent( float dt );

private:
	bool		textureLoaded;
	String		textureToLoad;
	Rect		clipRect;
	float 		contentWidth;
	float		contentHeight;
	CBlitBuffer imageBuffer;
};

}
#endif

#include "ButtonObject.h"
#include <math.h>
#include "SIEngine/Include/SIDeserialize.h"
#include "SIEngine/Include/SIActions.h"

namespace SICore
{

ButtonObject::ButtonObject() : super()
{
	doBreathe		 = true;
	keepBackgroundOn = false;
	background = NULL;
	titleLabel = NULL;
	backgroundNormal = NULL;
	backgroundActive = NULL;
	backgroundDisabled = NULL;
	backgroundSelected = NULL;
	scaleOnActiveAmount = 0.1f;
	setState(ButtonStateNone);
	setState(ButtonStateNormal);
}

ButtonObject::ButtonObject(FontData *font, String titleLabel_) : super()
{
	doBreathe = true;
	keepBackgroundOn = false;
	TextLabel* newLabel = new TextLabel(font, titleLabel_);
	addChild(newLabel);
	newLabel->release();
	scaleOnActiveAmount = 0.1f;
	background = NULL;
	titleLabel = newLabel;
	backgroundNormal = NULL;
	backgroundActive = NULL;
	backgroundDisabled = NULL;
	backgroundSelected = NULL;
	setState(ButtonStateNone);
	setState(ButtonStateNormal);
}


ButtonObject::ButtonObject(SpriteObject *icon) : super()
{
	doBreathe = true;
	keepBackgroundOn = false;
	addChild(icon);
	background = NULL;
	titleLabel = NULL;
	backgroundNormal = NULL;
	backgroundActive = NULL;
	backgroundDisabled = NULL;
	backgroundSelected = NULL;
	scaleOnActiveAmount = 0.1f;
	setState(ButtonStateNone);
	setState(ButtonStateNormal);
}

ButtonObject::~ButtonObject()
{
}

//breathe disabled
void ButtonObject::setBreathe( bool enabled )
{
	doBreathe = enabled;
	totalTimeElapsed = 0;
}

void ButtonObject::loadGameObjectState( GameObjectState newState )
{
	super::loadGameObjectState(newState);
	setState(ButtonStateNone);
	setState(ButtonStateNormal);
}

/// <summary>
/// For scripting button state changes
/// </summary>
void ButtonObject::makeState(ButtonState newstate)
{
	queueAction( TRelease<ButtonStateAction>( new ButtonStateAction( this, newstate ) ) );
}

bool ButtonObject::isEnabled()
{
	return currentState != ButtonStateDisabled;
}

/// <summary>
/// Returns the logical state of the button
/// </summary>
ButtonState ButtonObject::getState()
{ return currentState; }

void ButtonObject::enable()
{
	setState(ButtonStateNormal);
}

void ButtonObject::disable()
{
	setState(ButtonStateDisabled);
}

ButtonObject* ButtonObject::deSerialize(FILE* fp)
{
	int type = 0;

	if( fscanf( fp, "%d", &type ) == 1 )
	{
		ButtonType baseButtonType = (ButtonType)type;
		if( baseButtonType == TypeANDT )
		{ return deSerializeANDT(fp); }
		else if( baseButtonType == TypeANT )
		{ return deSerializeANT(fp); }
		else if( baseButtonType == TypeAN )
		{ return deSerializeAN(fp); }
		else if( baseButtonType == TypeAND )
		{ return deSerializeAND(fp); }
		else if( baseButtonType == TypeAD )
		{ return deSerializeAD(fp); }
		else if( baseButtonType == TypeANDST )
		{ return deSerializeANDST(fp); }

		ThrowException::deserializeException();
	}
	else
	{
		ThrowException::deserializeException();
	}
}

ButtonObject* ButtonObject::deSerializeANDT(FILE* fp)
{
	SpriteObject* active = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* disabled = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* normal = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* selected = NULL;
	
	TextLabel*	  title = (TextLabel*)Scene::deSerializeNext(fp);
	ButtonObject* newButton = new ButtonObject();
	newButton->setBackground( normal, selected, disabled, active );
	newButton->setLabel( title );
	newButton->setState(ButtonStateNone);
	newButton->setState(ButtonStateNormal);

	active->release();
	normal->release();
	disabled->release();
	title->release();
	//selected->release();

	return newButton;
}



ButtonObject* ButtonObject::deSerializeANDST(FILE* fp)
{
	SpriteObject* active = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* disabled = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* normal = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* selected = (SpriteObject*)Scene::deSerializeNext(fp);
	
	TextLabel*	  title = (TextLabel*)Scene::deSerializeNext(fp);
	ButtonObject* newButton = new ButtonObject();
	newButton->setBackground( normal, selected, disabled, active );
	newButton->setLabel( title );

	active->release();
	normal->release();
	disabled->release();
	title->release();
	selected->release();

	newButton->setState(ButtonStateNone);
	newButton->setState(ButtonStateNormal);
	return newButton;
}

ButtonObject* ButtonObject::deSerializeANT(FILE* fp)
{
	SpriteObject* active = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* normal = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* disabled = NULL;
	SpriteObject* selected = NULL;
	
	TextLabel*	  title = (TextLabel*)Scene::deSerializeNext(fp);
	ButtonObject* newButton = new ButtonObject();

	newButton->setBackground( normal, selected, disabled, active );
	newButton->setLabel( title );
	newButton->setState(ButtonStateNone);
	newButton->setState(ButtonStateNormal);

	active->release();
	normal->release();
	//disabled->release();
	title->release();
	//selected->release();

	return newButton;
}

ButtonObject* ButtonObject::deSerializeAN(FILE* fp)
{
	SpriteObject* active = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* normal = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* disabled = NULL;
	SpriteObject* selected = NULL;
	
	TextLabel*	  title = NULL;
	ButtonObject* newButton = new ButtonObject();
	newButton->setBackground( normal, selected, disabled, active );
	newButton->setLabel( title );
	newButton->setState(ButtonStateNone);
	newButton->setState(ButtonStateNormal);

	active->release();
	normal->release();
	//disabled->release();
	//title->release();
	//selected->release();ButtonStateNormal

	return newButton;
}

ButtonObject* ButtonObject::deSerializeAND(FILE* fp)
{
	SpriteObject* active = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* disabled = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* normal = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* selected = NULL;
	
	TextLabel*	  title = NULL;
	ButtonObject* newButton = new ButtonObject();
	newButton->setBackground( normal, selected, disabled, active );
	newButton->setLabel( title );
	newButton->setState(ButtonStateNone);
	newButton->setState(ButtonStateNormal);

	active->release();
	normal->release();
	disabled->release();
	//title->release();
	//selected->release();

	return newButton;
}

ButtonObject* ButtonObject::deSerializeAD(FILE* fp)
{
	SpriteObject* active = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* normal = NULL;
	SpriteObject* disabled = (SpriteObject*)Scene::deSerializeNext(fp);
	SpriteObject* selected = NULL;
	
	TextLabel*	  title = NULL;
	ButtonObject* newButton = new ButtonObject();
	newButton->setBackground( normal, selected, disabled, active );
	newButton->setLabel( title );
	newButton->setState(ButtonStateNone);
	newButton->setState(ButtonStateNormal);

	active->release();
	//normal->release();
	disabled->release();
	//title->release();
	//selected->release();

	return newButton;
}

void ButtonObject::setLabel(TextLabel* titleLabel_)
{
	if( titleLabel_ != NULL )
	{
		titleLabel = titleLabel_;
		addChild(titleLabel);
	}
}

void ButtonObject::setBackground(SpriteObject *normal, SpriteObject *selected, SpriteObject *disabled, SpriteObject* active)
{
	if (normal != NULL)
	{
		addChild(normal);
		backgroundNormal = normal;
	}

	if (selected != NULL)
	{
		addChild(selected);
		backgroundSelected = selected;
	}

	if (disabled != NULL)
	{
		addChild(disabled);
		backgroundDisabled = disabled;
	}

	if( active != NULL )
	{
		addChild(active);
		backgroundActive = active;
	}

}

/// <summary>
/// Precanned animations
/// </summary>
void ButtonObject::bubbleOut( float delay)
{
	if(currentState != ButtonStateDisabled)
		setState(ButtonStateNormal);
	super::bubbleOut(delay);
}

/// <summary>
/// Precanned animations
/// </summary>
void ButtonObject::fadeOut(float delay)
{
	if(currentState != ButtonStateDisabled)
			setState(ButtonStateNormal);

	super::fadeOut(delay);
}

//********************************************************************************
// Called for convenience on derived classes to handle animation changes
//********************************************************************************
void ButtonObject::transition( TransitionState transition )
{
	if (transition == ActiveToNormal)
	{
		totalTimeElapsed = 0;
		scaleTo(originalScale, 0.25f);
	}
	if (transition == NormalToActive )
	{
		totalTimeElapsed = 0;
	}
}

void ButtonObject::setState(ButtonState state)
{
	ButtonState oldState = currentState;
	if (state == ButtonStateNormal)
	{
		if (backgroundSelected != NULL)
			backgroundSelected->setVisible(false);
		if (backgroundNormal != NULL)
			backgroundNormal->setVisible(true);
		if (backgroundDisabled != NULL)
			backgroundDisabled->setVisible(false);
		if (backgroundActive != NULL)
			backgroundActive->setVisible(false);
	}
	else if (state == ButtonStateSelected)
	{
		if (backgroundSelected != NULL)
			backgroundSelected->setVisible(true);
		if (backgroundNormal != NULL && !keepBackgroundOn )
			backgroundNormal->setVisible(false);
		if (backgroundDisabled != NULL)
			backgroundDisabled->setVisible(false);	
		if (backgroundActive != NULL)
			backgroundActive->setVisible(false);
	}
	else if (state == ButtonStateActive)
	{
		if (backgroundSelected != NULL)
			backgroundSelected->setVisible(false);
		if (backgroundNormal != NULL && !keepBackgroundOn)
			backgroundNormal->setVisible(false);
		if (backgroundDisabled != NULL)
			backgroundDisabled->setVisible(false);		
		if (backgroundActive != NULL)
			backgroundActive->setVisible(true);
	}
	else if (state == ButtonStateDisabled)
	{
		if (backgroundSelected != NULL)
			backgroundSelected->setVisible(false);
		if (backgroundNormal != NULL && !keepBackgroundOn)
			backgroundNormal->setVisible(false);
		if (backgroundDisabled != NULL)
			backgroundDisabled->setVisible(true);		
		if (backgroundActive != NULL)
			backgroundActive->setVisible(false);
	}

	currentState = state;

	if (oldState == ButtonStateNone)
	{
		if ( currentState == ButtonStateNormal)
		{ transition( NoneToNormal ); }
	}
	else if( oldState == ButtonStateNormal )
	{
		if( currentState == ButtonStateActive )
		{ transition( NormalToActive ); }
		if( currentState == ButtonStateSelected )
		{ transition( NormalToSelected ); }
		if( currentState == ButtonStateDisabled )
		{ transition( NormalToDisabled ); }
	}
	else if( oldState == ButtonStateActive )
	{
		if( currentState == ButtonStateNormal )
		{ transition( ActiveToNormal ); }
		if( currentState == ButtonStateSelected )
		{ transition( ActiveToSelected ); }
		if( currentState == ButtonStateDisabled )
		{ transition( ActiveToDisabled ); }
	}
	else if( oldState == ButtonStateSelected )
	{
		if( currentState == ButtonStateActive )
		{ transition( SelectedToActive ); }
		if( currentState == ButtonStateNormal )
		{ transition( SelectedToNormal ); }
		if( currentState == ButtonStateDisabled )
		{ transition( SelectedToDisabled ); }
	}
	else if( oldState == ButtonStateDisabled )
	{
		if( currentState == ButtonStateActive )
		{ transition( DisabledToActive ); }
		if( currentState == ButtonStateNormal )
		{ transition( DisabledToNormal ); }
		if( currentState == ButtonStateSelected )
		{ transition( DisabledToSelected ); }
	}
}

/// <summary>
/// Sets amount to scale when active
/// </summary>
void ButtonObject::setScaleOnActive(float scaleAmount )
{
	scaleOnActiveAmount = scaleAmount;
}

void ButtonObject::gentleBreathe(float dt)
{
	if( doBreathe && currentState == ButtonStateActive && !hasActions())
	{
		float scale = originalScale.x + scaleOnActiveAmount*fabs(sinf(1.5f*totalTimeElapsed));
		Vec3f newLocalScale = Vec3f( scale, scale, scale);
		setScale(newLocalScale);
	}
}

void ButtonObject::update(float dt)
{
	super::update(dt);
	gentleBreathe(dt);
}

void ButtonObject::setOriginal()
{
	doBreathe = true;
	keepBackgroundOn = false;
	setState(ButtonStateNone);
	setState(ButtonStateNormal);
	super::setOriginal();
}


}

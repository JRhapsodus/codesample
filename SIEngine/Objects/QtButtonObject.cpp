#include "QtButtonObject.h"
#include "SIEngine/Include/SIDeserialize.h"


namespace SICore
{

QtButtonObject::QtButtonObject(QtSpriteObject* a, QtSpriteObject* n)
{
	active = a;
	normal = n;
	addChild( a );
	addChild( n );

	setState( QtButtonStateNormal);

}


QtButtonObject::~QtButtonObject()
{ }

void QtButtonObject::setState( QtButtonState newState )
{
	if( newState == QtButtonStateActive )
	{
		active->setVisible(true);
		normal->setVisible(false);
	}
	else if ( newState == QtButtonStateNormal )
	{
		active->setVisible(false);
		normal->setVisible(true);
	}


}


/// <summary>
/// Load from file
/// </summary>
QtButtonObject* QtButtonObject::deSerialize( FILE* fp )
{
	TRelease<QtSpriteObject> active( (QtSpriteObject*)Scene::deSerializeNext(fp) );
	TRelease<QtSpriteObject> normal( (QtSpriteObject*)Scene::deSerializeNext(fp) );
	return new QtButtonObject( active, normal );
}

} /* namespace SICore */

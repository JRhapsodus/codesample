#include "SIEngine/Core/OpenGlRenderer.h"
#include <string.h>
#include <sstream>

#include "SIEngine/Objects/MeshObject.h"
#include "SIEngine/Rendering/Materials/VertexLitMaterial.h"

using namespace SIRendering;

namespace SICore
{
//Class static
IndexColoredMode* MeshObject::g_IndexedMeshDrawMode = new IndexColoredMode();

//****************************************************************************************
// Life cycle
//****************************************************************************************
MeshObject::MeshObject(Mesh* m ) : super()
{
	mesh = m;
	m->addRef();
	init();
}

MeshObject::MeshObject() : super()
{
	/* configure the shaders etc... */
	init();
}

MeshObject::~MeshObject()
{
	SAFE_RELEASE(mesh);
	SAFE_RELEASE(material);

}

void MeshObject::init()
{
	//Broken for now
	//material = new VertexLitMaterial((MeshObject*)this);
}

//****************************************************************************************
// Game Object
//****************************************************************************************
bool MeshObject::processInput( IEventMessage *message)
{
	return false;
}

bool MeshObject::hitTest(int x, int y)
{
	//*****************************************************************************
	// A 2nd method would be to do a raycast, and test collision with any of the
	// triangles in Triangles array
	//*****************************************************************************
	Vec3f localPoint = Vec3f(x, y, 0);
	localPoint = transform.getWorldToLocal().multiplyPoint(localPoint);
	return localBoundingBox.contains( localPoint);
}

void MeshObject::update( float dt )
{
	super::update(dt);
}

void MeshObject::render()
{
	super::render();
}

//****************************************************************************************
// TODO:
// When deserializing, load mesh by name rather than a hard coded cylinder
//****************************************************************************************
MeshObject* MeshObject::deSerialize(FILE* fp)
{
	MeshObject* newObject = NULL;
	TRelease<Mesh> cylinderMesh(new Mesh());
	cylinderMesh->LoadMesh("Models/CylinderVerts.txt", "Models/CylinderIndices.txt");

	char labelText[1000];
	fscanf(fp, "%*[^\"]%*c%[^\"]\"", labelText);
	newObject = new MeshObject(cylinderMesh);
	newObject->setVisible(false);
	return newObject;
}

//****************************************************************************************
// Loop through all vertices and find the far left, far right, far top, far bot-tom
//****************************************************************************************
void MeshObject::computeLocalBounds()
{
	int vertCount = mesh->vertCount;
	if( mesh->vertCount > 0 )
	{
		Vec3f minVec = Vec3f( MAXFLOAT, MAXFLOAT, MAXFLOAT );
		Vec3f maxVec = Vec3f( MINFLOAT, MINFLOAT, MINFLOAT );

		for( int i = 0; i<vertCount; i++ )
		{
			//We should not be assuming a specific vertex format!
			//But we can also say this is a future feature we don't
			//need right now. For now lets force this vert format
			LitPositionVertex vert = mesh->vertices[i];
			minVec.x = std::min( vert.position.x, minVec.x);
			minVec.y = std::min( vert.position.y, minVec.y);
			minVec.z = std::min( vert.position.z, minVec.z);

			maxVec.x = std::max( vert.position.x, maxVec.x);
			maxVec.y = std::max( vert.position.y, maxVec.y);
			maxVec.z = std::max( vert.position.z, maxVec.z);
		}

		localBoundingBox = Box3f( Vec3f::Zero, Vec3f(maxVec.x-minVec.x, maxVec.y-minVec.y, maxVec.z-minVec.z) );
	}
}


//****************************************************************************************
// IRenderable
//****************************************************************************************
int* 	MeshObject::getIndexBuffer()
{
	if( mesh != NULL )
		return mesh->indices;
	return NULL;
}

float*  MeshObject::getVertexBuffer()
{
	if( mesh != NULL )
		return &mesh->vertices[0].position.x;
	return NULL;
}

int MeshObject::getVertexCount()
{
	if( mesh != NULL )
		return mesh->vertCount;
	return 0;
}

Triangle3f* MeshObject::getTriangles()
{ return NULL; }

int	MeshObject::getIndexCount()
{
	if( mesh != NULL )
		return mesh->indexCount;
	return 0;
}

void MeshObject::actionTest()
{
	//for testing xform inheritance
	rotateTo( Vec3f(0,0,45), 4 );
	rotateTo( Vec3f(0,0,0), 4 );
	moveTo( Vec3f( .5, 0, 0), 1 );
	moveTo( Vec3f( -.5, 0, 0), 1 );
	moveTo( Vec3f( 0, 1, 0), 1 );
	moveTo( Vec3f( 0, 0, 0), 1 );
	//x axis rotation test
	rotateTo( Vec3f(-45,0,0), 1 );
	rotateTo( Vec3f(0,0,0), 1 );
	//y axis rotation test
	rotateTo( Vec3f(0,45,0), 1 );
	rotateTo( Vec3f(0,0,0), 1 );
	//Z axis rotation test
	rotateTo( Vec3f(0,0,45), 1 );
	rotateTo( Vec3f(0,0,0), 1 );
	//x axis scale test
	scaleTo( Vec3f( 2,1,1), 1 );
	scaleTo( Vec3f( 1,1,1), 1 );
	scaleTo( Vec3f( 1,2,1), 1 );
	scaleTo( Vec3f( 1,1,1), 1 );
	moveTo( Vec3f( 0, 0, 0), 1 );
}

}


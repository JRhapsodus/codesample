#ifndef INCLUDE_BASEOBJECT_H_
#define INCLUDE_BASEOBJECT_H_

#include "SIEngine/Include/LFIncludes.h"
#include "SIEngine/Collections/IComparable.h"
#include "SIEngine/Collections/ICloneable.h"
#include "SIEngine/Core/CriticalSection.h"
#include "SIEngine/Core/Semaphore.h"
#include <StringTypes.h>

LF_USING_BRIO_NAMESPACE();
using namespace SICollections;

/// <summary> 
/// Useful defines and macros
/// </summary> 
#define null NULL
#define __T(x)	x
#define _T(x)	__T(x)


//#define MIN(A,B) A < B ? A : B;

#define SAFE_RELEASE(a) if ((a) != NULL) { if( a != NULL ) { (a)->release(); (a)=NULL; } }
#define SAFE_RELEASE_ARRAY(a) if( (a)!=NULL) { for (int iii_count = 0; iii_count < a->getSize(); iii_count++)\
{ BaseObject *b = a->elementAt(iii_count); b->release(); } a->release(); a = null; }

#define DEBUG_QSTRING(a) a.toStdString().c_str()
/// <summary> 
///Bit operations
/// </summary> 
#define SET_BIT(val, bitIndex) val |= (1 << bitIndex)
#define CLEAR_BIT(val, bitIndex) val &= ~(1 << bitIndex)
#define TOGGLE_BIT(val, bitIndex) val ^= (1 << bitIndex)
#define BIT_IS_SET(val, bitIndex) (val & (1 << bitIndex))
#define BIT_IS_MASKED(val, bitMask) (val & bitMask)
#define ROUNDUP16(s) (((s)+15)&0xfff0)

/// <summary> 
/// TRelease helper macros for easier to read convenience in one liner calls
/// </summary> 
#define TRelease_Font(a) TRelease<FontData>(new FontData(a))
#define TRelease_SpriteData(a) TRelease<SpriteData>(new SpriteData(a))
#define TRelease_Sprite(a,b,c) TRelease<SpriteObject>(SpriteObject::createSprite(TRelease_SpriteData(a),b,c))
#define New_Sprite(a,b,c) SpriteObject::createSprite(TRelease_SpriteData(a),b,c))

/// <summary> 
/// Class factory pattern
/// </summary> 
#define FACTORY(cls)   class cls##Class : public Class {\
public: \
  cls##Class(const String& str) : Class(str) {} \
    BaseObject* newInstance() {  return new cls(); } \
  }; static void forceInclude()
#define FACTORYIMPL2(cls,param) static cls::cls##Class s_##cls##CLASS(#cls); void cls::forceInclude() {}
#define FACTORYFWDIMPL2(cls,clsName,param) static cls::cls##Class s_##cls##CLASS(clsName); void cls::forceInclude() {}

/// <summary> 
/// Operator new
/// </summary>
#define ADD_TO_CLASS_MAP public:
/*
\
	static void* operator new(std::size_t sz)\
{\
std::map<std::string, int>::iterator iter = gRefCountMap.find( type() );\
if( iter == gRefCountMap.end())\
{ gRefCountMap[type()] = 1; }\
else\
{ gRefCountMap[type()]++; }\
return ::operator new(sz);\
} \
static void operator delete(void *ptr)\
{\
	gRefCountMap[type()]--;\
	free(ptr);\
}\
*/


typedef struct {
    volatile int counter;
} atomic_t;

/// <summary>
/// Macro for adding typeOf and ClassName to the class, extends class
/// </summary> 
#define CLASSEXTENDS(name,extends)\
private:\
 typedef extends super;\
public:\
	virtual bool typeOf( CLASSTYPE t ) const\
	{\
		if( std::strcmp( t, const_cast<char*>(#name))==0)\
			return true;\
		return extends::typeOf(t);\
	}\
virtual CLASSTYPE classname() const { return const_cast<char*>(#name); } \
static CLASSTYPE type() { return const_cast<char*>(#name); } \

/// <summary> 
/// Macro for adding typeOf and ClassName to the class, no base class
/// </summary> 
#define CLASSDEFNOBASE(name)\
public:\
virtual bool typeOf( CLASSTYPE t ) const {\
	if ( std::strcmp( t, #name ) == 0 )\
	{ return true; }\
	return false;\
}\
virtual CLASSTYPE classname() const \
{ return const_cast<char*>(#name); } \
static CLASSTYPE type() { return const_cast<char*>(#name); } \

#define CLASSDEF1(name) \
public:\
virtual bool typeof( CLASSTYPE t ) const {\
	if ( std::strcmp( t, #name) == 0 ) return true; \
	return false; } \
virtual CLASSTYPE classname() const { return const_cast<char*>(#name); } \
static CLASSTYPE type() { return const_cast<char*>(#name); }

#define CLASSDEF2(name,c1,c2) \
public:\
virtual bool typeof( CLASSTYPE t ) const {\
	if ( std::strcmp( t, #name) == 0 ) return true; \
	if ( c1::typeof(t) ) return true; \
	if ( c2::typeof(t) ) return true; \
	return false; } \
virtual CLASSTYPE classname() const { return const_cast<char*>(#name); } \
static CLASSTYPE type() {return const_cast<char*>(#name);}  \
virtual void addRef() { c1::addRef(); } \
virtual void release() { c1::release(); } \
virtual void sync() { c1::sync(); } \
virtual void unsync() { c1::unsync(); } \
virtual void wait() { c1::wait(); } \
virtual void wait( long ms ) { c1::wait(ms); } \
virtual void notify() { c1::notify(); } \
virtual void notifyAll() { c1::notifyAll(); }

#define CLASSDEF2FULL(name,c1,c2) \
public:\
virtual bool typeof( CLASSTYPE t ) const {\
	if ( std::strcmp( t, #name) == 0 ) return true; \
	if ( c1::typeof(t) ) return true; \
	if ( c2::typeof(t) ) return true; \
	return false; } \
virtual CLASSTYPE classname() const { return const_cast<char*>(#name); } \
static CLASSTYPE type() {return const_cast<char*>(#name);}  \
virtual void addRef() { c1::addRef(); } \
virtual void release() { c1::release(); } \
virtual void sync() { c1::sync(); } \
virtual void unsync() { c1::unsync(); } \
virtual void wait() { c1::wait(); } \
virtual void wait( long ms ) { c1::wait(ms); } \
virtual void notify() { c1::notify(); } \
virtual void notifyAll() { c1::notifyAll(); } \
virtual int		hashValue() const { return c1::hashValue(); } \
virtual bool equals( Object& o ) const { return c1::equals(o); } \
virtual String  toString() { return c1::toString(); }

/// <summary> 
/// Synchronize macros for help in writing critical section blocks for threading safety
/// </summary>
#define SYNCHRONIZEOBJ(obj)  Lock lock##obj(obj);
#define SYNCHRONIZED()  Lock lockthis(*this,true);
#define SYNCHDESTROY()  Lock lockthis(*this,true);

/// <summary>
/// Windows convention types, I am just used to them over the years
/// </summary> 
typedef unsigned char byte;
typedef char* LPSTR, PSTR;
typedef const char *PCZZSTR;
typedef const char *LPCSTR, *PCSTR;
typedef const char *PCNZCH;
typedef char *PZZSTR;
typedef char *PNZCH;
typedef char TCHAR, *PTCHAR;
typedef const PSTR *PCZPSTR;
typedef PSTR *PZPSTR;
typedef LPSTR PTSTR, LPTSTR, PUTSTR, LPUTSTR;
typedef LPCSTR PCTSTR, LPCTSTR, PCUTSTR, LPCUTSTR;
typedef LPCSTR CLASSTYPE;

namespace SICore
{

class String;
class CriticalSection;

/// <summary> 
/// My attempt at adding C# like properties that map to functions
/// </summary> 
template<class _Prop_t,class _ObjClass_t>
class Property
{
    typedef _Prop_t (_ObjClass_t::* _pmGet_t)() const;
    typedef void (_ObjClass_t::* _pmSet_t)(_Prop_t);

    _ObjClass_t& m_objInstance;
    _pmGet_t     m_pmGet;
    _pmSet_t     m_pmSet;

public:
    Property(_ObjClass_t& objInstance, _pmGet_t pmGet, _pmSet_t pmSet):  m_objInstance(objInstance), m_pmGet(pmGet), m_pmSet(pmSet)
    {}
    operator _Prop_t() { return (m_objInstance.*m_pmGet)(); }
    void operator =(_Prop_t value) { (m_objInstance.*m_pmSet)(value); }
};

/// <summary> 
/// The root most system object. Can be compared and cloned
/// </summary> 
class BaseObject : public IComparable, public ICloneable
{
	CLASSDEFNOBASE(BaseObject);
public:
	static int gObjectCount;
	static int gRefCount;
	static std::map<std::string, int> gRefCountMap;

	BaseObject();
	virtual ~BaseObject();

	virtual int getRefCount();
	virtual void addRef();
	virtual void release();
	virtual bool equals( BaseObject& obj ) const;
	virtual int hashValue() const;
	virtual String toString();
	virtual void sync();
	virtual void unsync();
	bool isLockedByCurrentThread();
	bool isLockedByAnotherThread();
	void wait();
	void wait( long ms );
	void notify();
	void notifyAll();

	static int memoryAddressComparison( const void* first, const void* other);
	virtual int compareTo( IComparable* other, CompareFunction functionToCompare ) const;
	virtual ICloneable* clone();

protected:
	int refCount;
	long syncCount;

private:
	bool					  m_bCsMade;
	CriticalSection			  m_cs;
	Semaphore*				  m_event; // event for wait/notify

	//*************************************************
	// Critical section for posix conditions & events
	//*************************************************
	pthread_t   		  	  lockThread;
	static  CriticalSection   s_critical;
	static  bool		      s_bMainCritInit;
};

/// <summary> 
/// Wrapper class that contains a WEAK! pointer to a base object
/// </summary> 
class BaseObjectPointer : public BaseObject
{
	CLASSEXTENDS(BaseObjectPointer,BaseObject);
public:
	BaseObject* object;
	BaseObjectPointer(BaseObject* bo) : super()
	{
		object = bo;
	}
	~BaseObjectPointer() { }
};

/// <summary> 
/// This class assumes ownership of the object (but does NOT addRef).
/// 		ImageObject( TRelease<IImage>( new IImage(pathToFile) ) );
/// </summary> 
template <class T> class TRelease
{
private:
	T*	m_object;
public:
	typedef T* PT;

	/// no addRef! We want to delete this when we are destroyed, so keep refcount==1
	TRelease( T* t = NULL )	: m_object(t) {	}
	~TRelease()
	{
		if (m_object != NULL)
		{ m_object->release(); }
	}

	operator PT( ) const
	{ return m_object; }

	PT operator ->() const
	{ return m_object; }

	PT ptr() const
	{ return m_object; }

	bool valid() const
	{ return m_object != NULL; }

	void release()
	{
		if (m_object != NULL)
		{ m_object->release(); }
	}

	/// <summary> 
	/// No addRef!
	/// </summary> 
	TRelease& operator =(T* anObject)
	{
		if (m_object != NULL)
		{ m_object->release(); }
		m_object = anObject;
		return *this;
	}

private:
	/// <summary> 
	/// Private because I'm not sure what the semantics of this would be
	/// </summary> 
	TRelease& operator =(TRelease& aReleaser) { return *this; }
	TRelease(const TRelease& aReleaser)	: m_object(NULL) { }
};

}
#endif




#ifndef INCLUDE_DIALOG_H_
#define INCLUDE_DIALOG_H_

#include "SIEngine/Objects/ButtonObject.h"
#define DIALOG_PADDING 100

namespace SICore
{
enum DialogType
{
	DialogTypeRemoveContent,
	DialogTypeDeleteConfirmed,
	DialogTypeActivationFail,
	DialogTypeAreYouSure,
	DialogTypeHelp,
	DialogTypeSkipWifi,
	DialogTypeSkipEthernet,
	DialogTypeSkipActivation,
	DialogTypeError,
	DialogTypeExitSettings,
	DialogTypeDeleteProfile,
	DialogTypeDownloadError,
	DialogTypeInternetConnectionRequired
};

class DialogButtonOption
{
public:
	DialogButtonOption(const DialogButtonOption& optionsSrc)
	{
		display = optionsSrc.display;
		id = optionsSrc.id;
	}

	DialogButtonOption()
	{ }

	DialogButtonOption(String display_, String logical_)
	{
		if( display_.equalsIgnoreCase("OK"))
			display_ = "OK";
		display = display_;
		id	= logical_;
	}

	void debugLog()
	{
		printf("\tDialogButtonOption.display [%s]\n", display.str());
		printf("\tDialogButtonOption.id [%s]\n", id.str());
	}
	~DialogButtonOption(){}

	String display;
	String id;
};

class DialogOptions
{
public:
	DialogOptions(const DialogOptions& optionsSrc)
	{
		title = optionsSrc.title;
		prompt = optionsSrc.prompt;
		buttonA = optionsSrc.buttonA;
		buttonB = optionsSrc.buttonB;
		ignoreOldTrackedObjects = optionsSrc.ignoreOldTrackedObjects;
	}


	DialogOptions()
	{ ignoreOldTrackedObjects = false; }
	~DialogOptions() { }

	bool hasTwoButtons()
	{ return !buttonB.id.isEmpty(); }

	void debugLog()
	{
		printf("DialogOptions.title [%s]\n", title.str());
		printf("DialogOptions.prompt [%s]\n", prompt.str());
		printf("DialogOptions.ignoreOldTrackedObjects [%d]\n", ((ignoreOldTrackedObjects==true)?1:0));
		buttonA.debugLog();
		buttonB.debugLog();
	}

	String title;
	String prompt;
	DialogButtonOption buttonA;
	DialogButtonOption buttonB;
	bool ignoreOldTrackedObjects;
};


class DialogObject : public GameObject
{
	CLASSEXTENDS(DialogObject, GameObject);
	ADD_TO_CLASS_MAP;
public:
	DialogObject(SpriteObject* background, ButtonObject* button1, ButtonObject* button2,
			TextLabel* messageLabel, SpriteObject* screenDarken, TextLabel* titleLabel);
	~DialogObject();

	/// <summary> 
	/// Allow support to load from prefab
	/// </summary> 
	static DialogObject* deSerialize(FILE* fp);

	/// Text for dialog
	/// </summary> 
	void setPrompt(String title, String prompt);

	/// <summary> 
	/// Returns type of dialog for id'ing
	/// </summary>
	DialogType getDialogType();


	/// <summary>
	/// Two types of dialogs, single button and a 2 button
	/// </summary> 
	void setup(DialogType type, DialogOptions options);

	/// <summary>
	/// Dismisses the dialog without sending any "handle dialog" events. Instead, cancel dialog functions are called.
	/// </summary>
	void cancel();

	/// <summary> 
	/// Animations
	/// </summary> 
	void animateIn();
	void animateOut();
	void onActionPress();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary> 
	/// Public for access from scene who is really owner
	/// </summary> 
	ButtonObject* button1;
	ButtonObject* button2;

	DialogOptions options;
	DialogType dialogType;

private:
	/// <summary> 
	/// References
	/// </summary>

	SpriteObject* screenDarken;
	TextLabel* promptLabel;
	TextLabel* titleLabel;
	GameObject* rootNode;
};

}
#endif

#ifndef QTTEXTOBJECT_H_
#define QTTEXTOBJECT_H_

#include "SIEngine/Objects/GameObject.h"

namespace SICore
{
enum QtTextAlignment
{
	QtAlignmentLeft,
	QtAlignmentCenter,
	QtAlignmentRight
};

class QtTextObject : public GameObject
{
	CLASSEXTENDS(QtTextObject, GameObject);

public:
	QtTextObject(String defaultText, QtTextAlignment align, U32 color);
	virtual ~QtTextObject();
	static QtTextObject* deSerialize( FILE* fp );

	/// <summary>
	/// called on refresh localization
	/// </summary>
	virtual void onRefreshLocalization();

	void setText( String text );
	QtTextAlignment getAlignment();
	void setAlignment(QtTextAlignment alignment);
	void setQtColor(U32 c);
	U32 getQtColor();

	String getLabelText();

	bool isUnderlined;
private:
	QtTextAlignment alignment;
	U32  	qtColor;
	String labelText;
};

}
#endif

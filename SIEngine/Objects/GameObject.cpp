#include <EventMessage.h>
#include "SIEngine/Objects/GameObject.h"
#include "SIEngine/Core/OpenGlRenderer.h"
#include "SIEngine/Include/SIActions.h"
#include "Glasgow/GlasgowApp.h"

namespace SICore
{
static int g_objectID = 0;

GameObject::GameObject(String startingName) : ObjectCollection()
{
	privateInit();
	name = startingName;
}

GameObject::GameObject() : ObjectCollection()
{
	privateInit();
}

void GameObject::privateInit()
{
	name = String("obj") + String(g_objectID);
	g_objectID++;
	sceneGraphName = "";
	actions = new ObjectArray();
	clickThrough = false;
	simulate = true;
	touchable = false;
	visible = true;
	alpha = 1;
	material = NULL;
	cullingLayer = 0;
	showLocalAxis = false;
	totalTimeElapsed = 0;
	originalScale = Vec3f::One;
}

GameObject::~GameObject()
{
	SAFE_RELEASE(actions);
	SAFE_RELEASE(material);
}

//******************************************************
// Virtual override for loading from data file
//******************************************************
GameObject* GameObject::deSerialize( FILE* fp )
{ return new GameObject(); }

//******************************************************
// Tells engine to simulate this object
//******************************************************
void GameObject::activate()
{ simulate = true; }

//******************************************************
// Prevents update functions from being called
//******************************************************
void GameObject::deactivate()
{ simulate = false; }

//******************************************************
// Custom toString()
//******************************************************
String GameObject::toString()
{
	return "";
}

//******************************************************
// Mark transform for recalculation
//******************************************************
void GameObject::onChildRemoved(GameObject* child)
{
	if( child != NULL )
		child->transform.markDirty();
	transform.markDirty();
}

//******************************************************
// Mark transform for recalculation
//******************************************************
void GameObject::onParentChanged(GameObject* oldParent, GameObject* newParent)
{
	if( newParent != NULL )
	{
		transform.parentTransform = &newParent->transform;
		newParent->transform.markDirty();
	}
	else
	{
		transform.parentTransform = NULL;
	}

	transform.markDirty();
	GlasgowApp::mainState->getRenderer()->markSceneGraphDirty();
}

//******************************************************
// Mark transform for recalculation
//******************************************************
void GameObject::onChildAdded(GameObject* child)
{
	if( child != NULL)
		child->transform.markDirty();
	transform.markDirty();
}

//******************************************************
// Returns the child deepest in tree with the touchable
// flag. I am not sure about inheritance, but I say no
//******************************************************
GameObject* GameObject::highestTouchableParent()
{ return NULL; }

//******************************************************
// Action management for scripting
//******************************************************
void GameObject::queueAction( BaseAction* action )
{ actions->addElement(action); }

//******************************************************
// Cancels all actions, note: will call onComplete
// and thus the object could be in a quasi mid way
// state of an incomplete action
//******************************************************
void GameObject::cancelActionsAsComplete()
{
	for( int i = 0; i<actions->getSize(); i++ )
	{
		((BaseAction*)actions->elementAt(i))->onComplete();
	}

	actions->removeAllElements();

}


void GameObject::saveGameObjectState()
{
	originalState.originalTransform.setPosition(getPosition());
	originalState.originalTransform.setScale(getScale());
	originalState.originalTransform.setRotation(getRotation());
	originalState.originalVisibility = isVisibleSelf();
}

void GameObject::saveOriginalStateRecursive()
{
	saveGameObjectState();

	for( int i = 0; i<numChildren(); i++ )
	{
		GameObject* child = childAtIndex(i);
		child->saveOriginalStateRecursive();
	}

}

/// <summary>
/// Called by CallBackAction
/// </summary>
void GameObject::onCallBack( String id )
{
	//base
}

/// <summary>
/// Sets this object to a particular state
/// </summary>
void GameObject::loadOriginalStateRecursive()
{
	loadGameObjectState(originalState);

	for( int i = 0; i<numChildren(); i++ )
	{
		GameObject* child = childAtIndex(i);
		child->loadOriginalStateRecursive();
	}
}

/// <summary>
/// Sets this object to a particular state
/// </summary>
void GameObject::loadGameObjectState( GameObjectState newState )
{
	setRotation( newState.originalTransform.getRotation() );
	setScale( newState.originalTransform.getScale() );
	setPosition( newState.originalTransform.getPosition() );
	setVisible(newState.originalVisibility );
	cancelActions();
}

//******************************************************
// Cancels all actions, note: will not call onComplete
// and thus the object could be in a quasi mid way
// state of an incomplete action
//******************************************************
void GameObject::cancelActions()
{ actions->removeAllElements();	}

//******************************************************
// Similar to cancel actions but guarantees onComplete
// is called foreach queued action in order
//******************************************************
void GameObject::endActions()
{
	for( int i = 0; i<actions->getSize(); i++ )
	{
		BaseAction* action = (BaseAction*)actions->elementAt(i);
		action->onComplete();
	}

	actions->removeAllElements();
}

//******************************************************
// Returns true if object is running through a script
//******************************************************
bool GameObject::hasActions()
{
	if (actions->getSize() > 0) return true;
	return false;
}

//******************************************************
// Pump for object
//******************************************************
void GameObject::update( float dt )
{
	if( !simulate )
		return;

	totalTimeElapsed += dt;

	//******************************************************
	// Run only the current action until it is completed
	//******************************************************
	BaseAction *action = (BaseAction *)actions->elementAt(0);
	if (action != NULL)
	{
		if ( !action->isStarted())
		{
			action->onStart();
		}
		else
		{
			action->update(dt);
			if (action->isCompleted())
			{
				action->onComplete();
				actions->removeElementAt(0);
			}
		}
	}

	//Collection modified prevention
	if( children != null )
	{
		ObjectArray* childrenCollection = new ObjectArray();
		childrenCollection->addElementsFromArray( children );
		for (int i = 0;i < childrenCollection->getSize(); i++)
		{
			GameObject* child = (GameObject*)childrenCollection->elementAt(i);
			child->update(dt);
		}

		childrenCollection->removeAllElements();
		childrenCollection->release();
	}
}

/// <summary>
/// Precanned animation of shake
/// </summary>
void GameObject::shake(float delay)
{
	waitFor(delay);
	moveBy(Vec3f(-10, 0, 0), 0.01f);
	moveBy(Vec3f(20, 0, 0), 0.01f);
	moveBy(Vec3f(-20, 0, 0), 0.01f);
	moveBy(Vec3f(20, 0, 0), 0.01f);
	moveBy(Vec3f(-20, 0, 0), 0.01f);
	moveBy(Vec3f(20, 0, 0), 0.01f);
	moveBy(Vec3f(-10, 0, 0), 0.01f);
}


/// <summary>
/// Precanned animation of becomming visible and then scaling in
/// </summary>
void GameObject::bubbleIn( float delay )
{
	setScale(Vec3f::Zero);
	waitFor(delay);
	makeVisible(true);
	scaleTo(originalScale, 0.25f );
}

/// <summary>
/// Precanned animation of scaling off to nothing then hiding
/// </summary>
void GameObject::bubbleOut( float delay )
{
	waitFor(delay);
	scaleTo(Vec3f::Zero, 0.25f);
	makeVisible(false);
}

float GameObject::bubbleInChildren(float delay, float staggerDelay)
{
	for( int i = 0; i<getChildren()->getSize(); i++ )
	{
		BaseObject* bo = getChildren()->elementAt(i);
		if(bo->typeOf(GameObject::type()))
		{
			GameObject* child = (GameObject*)bo;
			child->bubbleIn(delay);
			delay += staggerDelay;
		}
	}

	return delay;
}

float GameObject::bubbleOutChildren(float delay, float staggerDelay)
{
	for( int i = 0; i<getChildren()->getSize(); i++ )
	{
		BaseObject* bo = getChildren()->elementAt(i);
		if(bo->typeOf(GameObject::type()))
		{
			GameObject* child = (GameObject*)bo;
			child->bubbleOut(delay);
			delay += staggerDelay;
		}
	}

	return delay;
}

void GameObject::fadeIn(float delay)
{
	//override
}

void GameObject::fadeOut(float delay)
{
	//override
}

void GameObject::recursiveFadeIn(float delay)
{
	fadeIn(delay);

	for( int i = 0; i<numChildren(); i++ )
	{
		GameObject* child = childAtIndex(i);
		child->recursiveFadeIn(delay);
	}
}

void GameObject::recursiveFadeOut(float delay)
{
	fadeOut(delay);

	for( int i = 0; i<numChildren(); i++ )
	{
		GameObject* child = childAtIndex(i);
		child->recursiveFadeOut(delay);
	}
}

//**********************************************************************
// Returns wether this object is a descendant of linear on scene graph
//**********************************************************************
bool GameObject::descendantOf( GameObject* lineage )
{
	GameObject* parent = getParent();
	while( parent != null )
	{
		if( parent == lineage )
		{ return true; }

		parent = parent->getParent();
	}

	return false;
}

//**********************************************************************
// Quack quack mr duck type!
//**********************************************************************
int GameObject::nameComparison( const void* first, const void* other )
{
	GameObject* boFirst = (GameObject*)first;
	GameObject* boOther = (GameObject*)other;
	return boFirst->name.equals(boOther->name);
}

//******************************************************
// IComparable: Compare via object's world Z
// Note: in 3d modeling interpenetrating models become
// an issue with this method, Really the triangles should
// be compared individually
//******************************************************
int GameObject::zSortCompare( const void* first, const void* second )
{
	const BaseObject* boFirst = *(const BaseObject **)first;
	const BaseObject* boSecond = *(const BaseObject **)second;
	const BaseObjectPointer* bopFirst = (BaseObjectPointer*)boFirst;
	const BaseObjectPointer* bopSecond = (BaseObjectPointer*)boSecond;
	GameObject* goFirst = (GameObject*)bopFirst->object;
	GameObject* goSecond = (GameObject*)bopSecond->object;

	float oZ = goFirst->getWorldPosition().z;
	float sZ = goSecond->getWorldPosition().z;

	if( oZ == sZ )
	{ return 0; }
	else if ( oZ < sZ )
	{
		if( GlasgowApp::getRenderer()->isLeftHanded())
			return 1;
		return -1;
	}

	//******************************************************
	//oZ > sZ    Oz in the world is more positiveZ
	//<0 The element oZ goes before the element sZ
	//******************************************************
	if( GlasgowApp::getRenderer()->isLeftHanded( ))
		return -1;
	return 1;
}


//******************************************************
//Transform
//******************************************************
void GameObject::markChildrenTransformsDirty()
{
	for (int i = 0;i < this->numChildren(); i++)
	{
		GameObject *child = this->childAtIndex(i);
		if( child != null )
		{
			child->transform.markDirty();
			child->markChildrenTransformsDirty();
		}
	}
}

//******************************************************************************************
// Local affine parts
//******************************************************************************************
Vec3f GameObject::getPosition()
{ return transform.getPosition(); }

void GameObject::setPosition(Vec3f pos)
{
	transform.setPosition(pos);
	markChildrenTransformsDirty();
}

Vec3f GameObject::getScale()
{ return transform.getScale(); }

void GameObject::setScale(Vec3f scale)
{
	transform.setScale(scale);
	markChildrenTransformsDirty();
}

void GameObject::setOriginalScale()
{
	transform.setScale(originalScale);
	markChildrenTransformsDirty();
}

void GameObject::setOriginal()
{
	cancelActions();
	setVisible(true);
	setOpacity(1);
	//setOriginalScale();
}

Quaternion GameObject::getRotation()
{ return transform.getRotation(); }

Vec3f GameObject::getEulerRotation()
{ return transform.getEulerRotation(); }

void GameObject::setRotation(Quaternion rotation)
{
	transform.setRotation(rotation);
	markChildrenTransformsDirty();
}

void GameObject::setRotation(Vec3f rotation)
{
	if ( rotation == Vec3f::Zero )
	{
		transform.setRotation(Quaternion::identity);
	}
	else
		transform.setEulerRotation(rotation);
	markChildrenTransformsDirty();
}

//********************************************************************************
// Opacity feature: Really this should be a generic color
//********************************************************************************
void GameObject::setOpacity(float op)
{ alpha = op; }

//********************************************************************************
// Returns self's opacity only
//********************************************************************************
float GameObject::getOpacitySelf()
{ return alpha; }

//********************************************************************************
// Returns additive blend's opacity from scene graph
//********************************************************************************
float GameObject::getOpacityHierarchy()
{
	if (parent != NULL)
	{
		GameObject *p = (GameObject *)parent;
		return p->getOpacityHierarchy() * alpha;
	}
	return getOpacitySelf();
}

//********************************************************************************
// Culling layers for cameras
//********************************************************************************
int GameObject::getCullingLayer()
{ return cullingLayer; }
void GameObject::setCullingLayer(int layer)
{
	cullingLayer = layer;
}

void GameObject::setCullingLayerRecursive(int layer)
{
	cullingLayer = layer;

	for( int i = 0; i<numChildren(); i++ )
	{
		GameObject* child = (GameObject*)getChildren()->elementAt(i);
		child->setCullingLayerRecursive(layer);
	}
}

//***********************************************************************************
// Creates quaternion that rotates forward vector to local target location
//        M * (localRight, localUp, localForward) = (worldRight, perpWorldUp, targetDirection)
// or     M = (worldRight, perpWorldUp, targetDirection) * (localRight, localUp, localForward)^{-1}.
// or     M = m1 * m2, where M represents rotation Matrix
//***********************************************************************************
void GameObject::lookAt(GameObject* lookAt)
{
	GameObject* owner = (GameObject*)parent;
	Vec3f targetLocation = (lookAt->getWorldPosition() - getWorldPosition()).normalized();
	Vec3f localTarget = owner->transform.getWorldToLocal()*targetLocation;
	Vec3f localUp = owner->transform.getWorldToLocal()*Vec3f::Up;
	Quaternion quat = Quaternion::lookRotation(localTarget, localUp);
	transform.setRotation(quat);
}

//********************************************************************************
// Visibility status
//********************************************************************************
void GameObject::setVisible(bool visible_)
{
	visible = visible_;
	markChildrenTransformsDirty();
	if( getParent() != NULL )
	{ getParent()->transform.markDirty(); }
}

//********************************************************************************
// Change this object's material
//********************************************************************************
void GameObject::setMaterial( IMaterial* mat )
{
	SAFE_RELEASE(material);
	material = mat;
	mat->addRef();
}

//********************************************************************************
// Set visible on entire subtree of children
//********************************************************************************
void GameObject::setVisibleRecursive(bool visible_)
{
	visible = visible_;
	for( int i = 0; i<numChildren(); i++ )
	{
		GameObject* child = (GameObject*)getChildren()->elementAt(i);
		child->setVisibleRecursive(visible_);
	}
}

//********************************************************************************
// Returns true if only this object is marked visible
//********************************************************************************
bool GameObject::isVisibleSelf()
{ return visible; }

//********************************************************************************
// Returns false if any point up the tree is marked not visible
//********************************************************************************
bool GameObject::isVisibleHierarchy()
{
	if (parent != NULL)
	{
		GameObject *p = (GameObject *)parent;
		if (p->isVisibleHierarchy() == false)
		{ return false; }
	}

	return isVisibleSelf();
}


/// <summary>
/// recursive function to refresh localization
/// </summary>
void GameObject::refreshLocalizedStringsRecursive()
{
	onRefreshLocalization();

	for( int i = 0; i<numChildren(); i++ )
	{
		GameObject* child = (GameObject*)getChildren()->elementAt(i);
		child->refreshLocalizedStringsRecursive();
	}
}

/// <summary>
/// called on refresh localization
/// </summary>
void GameObject::onRefreshLocalization()
{
	//base class
}

//********************************************************************************
// Our affine parts transformed in world space
//********************************************************************************
Vec3f GameObject::getWorldPosition()
{
	if( transform.parentTransform == NULL )
		return getPosition();
	else
	{
		Vec3f p = Vec3f::Zero;
		Matrix localtoWorld = transform.getLocalToWorld();
		Vec3f worldPosition = localtoWorld.multiplyPoint(p);

		//debugTestUnityMatrix();
		return worldPosition;
	}
}

//********************************************************************************
// Test to verify the matrix integrity and the agreement of L2W*Vec3 via Unity3d
//********************************************************************************
void GameObject::debugTestUnityMatrix()
{
	Vec3f p = Vec3f::Zero;
	Matrix localtoWorld = transform.getLocalToWorld();
	Vec3f worldPosition = localtoWorld.multiplyPoint(p);

	localtoWorld.debugOutput();

	if( (int)getPosition().x == 4 && (int)getPosition().y == 3 && (int)getPosition().z == -28 )
	{
		Vec3f pos = getPosition();
		//Local Position: <4.567699,3.59811,-28.07072>
		printf("LocalPosition: %f %f %f\n", (double)pos.x, (double)pos.y, (double)pos.z);

	}

	printf("World position %f %f %f", (double)worldPosition.x, (double)worldPosition.y, (double)worldPosition.z);
}

//********************************************************************************
// localScale transformed by localtoworld
//********************************************************************************
Vec3f GameObject::getWorldScale()
{
	Vec3f s = getScale();
	return transform.getLocalToWorld()*s;
}

Vec3f GameObject::getWorldRotation()
{
	Vec3f r = getEulerRotation();
	return transform.getLocalToWorld()*r;
}

//********************************************************************
// Hittesting: Base object, empty GameObjects have no hittest,
// This is guaranteed to be subclassed
//********************************************************************
bool GameObject::hitTest(int x, int y)
{ return false; }

//********************************************************************
// Bounding box of this object's unioned children
//********************************************************************
Box3f GameObject::getChildBounds()
{
	Box3f ret = Box3f::Empty;

	for (int i = 0;i < this->numChildren(); i++)
	{
		GameObject *child = this->childAtIndex(i);
		if (child->visible)
		{
			//localToParent to bring their data into our space
			Box3f r = child->getParentBoundingBox();

			if (!r.isEmpty())
			{
				if (ret.isEmpty())
				{ ret = r; }
				else
				{ ret = Box3f::unionWith(ret, r); }
			}
		}
	}

	return ret;
}

//********************************************************************
//base class implementation, this object doesn't really
//have any construct of meshs' or sprites, its just an empty node
//********************************************************************
void GameObject::computeLocalBounds()
{ localBoundingBox = Box3f::Empty; }

//********************************************************************
// Bounding box of this object in its space
//********************************************************************
Box3f GameObject::getLocalBoundingBox()
{
	computeLocalBounds();
	return localBoundingBox;
}

//********************************************************************
// output for script overview
//********************************************************************
String GameObject::getScriptOverview()
{
	String output = "Sequence for " + name + " === ";

	for( int i = 0; i<actions->getSize(); i++ )
	{
		BaseAction* action = (BaseAction*)actions->elementAt(i);
		output += action->classname();
	}

	return output;
}

//********************************************************************
//Bounding box of this objects and it's children in world space
//********************************************************************
Box3f GameObject::getWorldBoundingBox()
{
	//Our world box
	Matrix localToWorld = transform.getLocalToWorld();
	Box3f box = getLocalBoundingBox();
	if( !box.isEmpty())
		box = Box3f::transform(box, localToWorld);

	//Our Children's world box
	for (int i = 0;i < this->numChildren(); i++)
	{
		GameObject *child = this->childAtIndex(i);
		if (child->visible)
		{
			Box3f childWorldBounds = child->getWorldBoundingBox();
			box = Box3f::unionWith(box, childWorldBounds);
		}
	}

	//In case of planes
	if (box.getWidth() <= 0 && box.getHeight() > 0 )
	{
		box = Box3f( box.getX(), box.getY(), box.getZ(), 1, box.getHeight(), box.getDepth());
	}

	//In case of planes
	if( box.getHeight() <= 0 && box.getWidth()> 0 )
	{
		box = Box3f(box.getX(), box.getY(), box.getZ(), box.getWidth(), 1, box.getDepth());
	}

	return box;
}

//********************************************************************
//Bounding box of this objects and it's children in it's parent's space
//********************************************************************
Box3f GameObject::getParentBoundingBox()
{
	computeLocalBounds();
	if( localBoundingBox.isEmpty())
		return localBoundingBox;

	Matrix localToParent = transform.getLocalToParent();
	return Box3f::transform( localBoundingBox, localToParent );
}

//******************************************************
//IRenderable
//******************************************************
int* GameObject::getIndexBuffer()
{ return NULL;}

float* GameObject::getVertexBuffer()
{ return NULL; }

int GameObject::getVertexCount()
{ return 0; }

IMaterial* GameObject::getMaterial()
{ return material; }

int	GameObject::getIndexCount()
{ return 0; }

void GameObject::render()
{
	//***********************************************************************************************
	// Don't render invisible objects
	//***********************************************************************************************
	if ( isVisibleHierarchy() == false || getOpacitySelf() <= 0.02f )
	{ return; }

	//If global wire frame or our local show axis is turned on
	BaseRenderer* renderer = GlasgowApp::getRenderer();

	//***********************************************************************************************
	// The 3d scenes have useful debug features, but are only supported by the openGl Renderer
	//***********************************************************************************************
	if( renderer->typeOf( OpenGlRenderer::type() ) )
	{
		Vec3f worldstart = getWorldPosition();
		OpenGlRenderer* oGlRenderer = (OpenGlRenderer*)renderer;
		int z = oGlRenderer->mainCamera->getWorldPosition().z;



		if( showLocalAxis || oGlRenderer->showLocalAxis )
		{
			if( worldstart.z > z )
			{
				Vec3f localRight = Vec3f::Right*60;
				Vec3f worldEnd = transform.getLocalToWorld().multiplyPoint(localRight);

				if( worldEnd.z > z )
				{ oGlRenderer->renderLine( Line3f(worldstart,worldEnd), Color::Red); }

				Vec3f localUp = Vec3f::Up*60;
				worldEnd = transform.getLocalToWorld().multiplyPoint(localUp);
				if( worldEnd.z > z )
				{ oGlRenderer->renderLine( Line3f(worldstart,worldEnd), Color::Green); }

				Vec3f localForward = Vec3f::Back*60;
				worldEnd = transform.getLocalToWorld().multiplyPoint(localForward);
				if( worldEnd.z > z )
				{ oGlRenderer->renderLine( Line3f(worldstart,worldEnd), Color::Blue); }
			}
		}

		//world bounds does not affect us
		if( oGlRenderer->showBounds )
		{ oGlRenderer->renderWorldBox(getWorldBoundingBox()); }

		if( oGlRenderer->showWorldPositions )
		{
			Vec3f worldPosition = getWorldPosition();
			if( worldPosition.z > z )
			{
				Vec3f clippingCenter = getScreenBoundingBox(oGlRenderer->mainCamera).getCenter();
				Vec3f screenSpace = Vec3f( ((clippingCenter.x+1)/2)*1280, ((clippingCenter.y+1)/2)*720, 1);
				String debugText = name + " " + (int)worldPosition.x + " " + (int)worldPosition.y + " " + (int)worldPosition.z;
				oGlRenderer->renderTextToScreen(debugText.str(), screenSpace.x, screenSpace.y, 0.55f, Color::BlackOpaque);
			}
		}

		//if wireframe mode dont render the object
		if( oGlRenderer->wireFrameMode )
		{
			oGlRenderer->renderWireFrame(this);
			return;
		}
	}

	renderer->render(this);
}

bool GameObject::processInput( IEventMessage *message)
{
	for (int i = 0;i < this->numChildren(); i++)
	{
		GameObject *child = this->childAtIndex(i);
		if (child->processInput(message))
		{ return true; }
	}
	return false;
}

//******************************************************************************************
// Returns the bounding box as projected through the view &projection (Clip space)
//******************************************************************************************
Box3f GameObject::getScreenBoundingBox(CameraObject* camera)
{
	//If there is no camera, we must be in a QT framework, our bounds is
	if( camera == NULL )
	{ return getWorldBoundingBox(); }

	return getWorldBoundingBox().project(camera->getViewMatrix(), camera->projection);
}

//******************************************************************************************
// Actions
//******************************************************************************************
void GameObject::moveToScaleTo(Vec3f position, Vec3f scale, float duration)
{
	TRelease<MultiAction> multi( new MultiAction(this) );
	multi->addAction(  TRelease<MoveToAction>( new MoveToAction(this, position, duration, EaseFunctions::cubicEaseInOut)) );
	multi->addAction( TRelease<ScaleToAction>( new ScaleToAction(this, scale, duration, EaseFunctions::cubicEaseInOut)) );

	queueAction(multi);


}

void GameObject::moveTo(Vec3f position, float duration)
{ queueAction( TRelease<MoveToAction>( new MoveToAction(this, position, duration, EaseFunctions::cubicEaseInOut)) ); }

void GameObject::moveTo(Vec3f position)
{ queueAction( TRelease<MoveToAction>( new MoveToAction(this, position)) ); }

void GameObject::moveBy(Vec3f delta, float duration)
{ queueAction( TRelease<MoveByAction>( new MoveByAction(this, delta, duration)) ); }

void GameObject::moveBy(Vec3f delta)
{ queueAction( TRelease<MoveByAction>( new MoveByAction(this, delta)) ); }

void GameObject::rotateTo(Quaternion rotation, float duration)
{ queueAction( TRelease<RotateToAction>( new RotateToAction(this, rotation, duration)) ); }


void GameObject::rotateBy(Quaternion delta, float duration)
{
	queueAction( TRelease<RotateByAction>( new RotateByAction(this, delta, duration, EaseFunctions::cubicEaseInOut)) );
}




void GameObject::rotateTo(Vec3f rotation, float duration)
{ queueAction( TRelease<RotateToAction>( new RotateToAction(this, rotation, duration)) ); }

void GameObject::rotateTo(Vec3f rotation)
{ queueAction( TRelease<RotateToAction>( new RotateToAction(this, rotation)) ); }

void GameObject::rotateBy(Vec3f delta, float duration)
{ queueAction( TRelease<RotateByAction>( new RotateByAction(this, delta, duration)) ); }

void GameObject::rotateBy(Vec3f delta)
{ queueAction( TRelease<RotateByAction>( new RotateByAction(this, delta)) ); }

void GameObject::scaleToOriginal(float duration)
{ queueAction( TRelease<ScaleToAction>( new ScaleToAction(this, originalScale, duration, EaseFunctions::cubicEaseInOut)) ); }


void GameObject::scaleTo(Vec3f scale, float duration)
{ queueAction( TRelease<ScaleToAction>( new ScaleToAction(this, scale, duration, EaseFunctions::cubicEaseInOut)) ); }

void GameObject::scaleTo(Vec3f scale)
{ queueAction( TRelease<ScaleToAction>( new ScaleToAction(this, scale)) ); }

void GameObject::scaleBy(Vec3f delta, float duration)
{ queueAction( TRelease<ScaleByAction>( new ScaleByAction(this, delta, duration)) ); }

void GameObject::scaleBy(Vec3f delta)
{ queueAction( TRelease<ScaleByAction>( new ScaleByAction(this, delta)) ); }

void GameObject::kill()
{ queueAction( TRelease<KillAction>( new KillAction(this)) ); }

void GameObject::makeVisible(bool visibility)
{ queueAction( TRelease<VisibilityAction>( new VisibilityAction(this, visibility)) ); }

void GameObject::waitFor(float timeDelta)
{ queueAction( TRelease<IntervalAction>( new IntervalAction(this, timeDelta)) ); }

void GameObject::fadeTo(float opacity, float timeDelta)
{ queueAction( TRelease<FadeToAction>( new FadeToAction(this, opacity, timeDelta)) ); }

void GameObject::fadeTo(float opacity)
{ queueAction( TRelease<FadeToAction>( new FadeToAction(this, opacity)) ); }

void GameObject::triggerSound(String soundID)
{ queueAction( TRelease<PlaySoundAction>( new PlaySoundAction(this, soundID) ) ); }

void GameObject::callback(GameObject* toCall, String callbackID)
{ queueAction( TRelease<CallBackAction>( new CallBackAction(toCall, callbackID)) ); }

}

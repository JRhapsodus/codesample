#ifndef INCLUDE_BUTTON_H_
#define INCLUDE_BUTTON_H_

#include "SIEngine/Objects/SpriteObject.h"
#include "SIEngine/Objects/TextLabel.h"

namespace SICore
{

/// <summary> 
/// Button object
/// </summary> 
class ButtonObject : public GameObject
{
	CLASSEXTENDS(ButtonObject, GameObject);
	ADD_TO_CLASS_MAP;
public:

	typedef enum ButtonTypes
	{
		TypeDefault = 0,
		TypeANDT,
		TypeANT,
		TypeAN,
		TypeAND,
		TypeAD,
		TypeANDST
	} ButtonType;

	/// <summary> 
	/// State enums
	/// </summary> 
	enum TransitionState
	{
		NoneToNormal,
		NormalToActive,
		NormalToSelected,
		NormalToDisabled,
		ActiveToNormal,
		ActiveToSelected,
		ActiveToDisabled,
		SelectedToNormal,
		SelectedToActive,
		SelectedToDisabled,
		DisabledToNormal,
		DisabledToActive,
		DisabledToSelected

	};

	ButtonObject(FontData *font, String titleLabel_);
	ButtonObject(SpriteObject *icon);
	ButtonObject();
	virtual ~ButtonObject();

	/// <summary> 
	/// Standard 3 state button
	/// </summary> 
	void setBackground(SpriteObject *normal, SpriteObject *selected, SpriteObject *disabled, SpriteObject* active);
	void setLabel(TextLabel* titleLabel);

	/// <summary> 
	/// Really the only thing that makes this a button, is the tracking of a 'button' state
	/// </summary> 
	virtual void setState(ButtonState state);
	virtual void update(float dt);

	/// <summary>
	/// Returns the logical state of the button
	/// </summary>
	virtual ButtonState getState();

	virtual void loadGameObjectState( GameObjectState newState );

	/// <summary> 
	/// Transition states from one state to other for subclass animations
	/// </summary> 
	virtual void transition( TransitionState transition );

	/// <summary>
	/// Precanned animations
	/// </summary>
	virtual void bubbleOut( float delay = 0);
	virtual void fadeOut(float delay = 0);

	/// <summary>
	/// Sets amount to scale when active
	/// </summary>
	void setScaleOnActive(float scaleAmount );

	virtual void setOriginal();


	/// <summary>
	/// Public so scenes can tell its button how to breathe editorially
	/// </summary>
	float scaleOnActiveAmount;


	static ButtonObject* deSerialize(FILE* fp);

	bool isEnabled();
	void onAction();
	void enable();
	void disable();

	void setBreathe( bool enabled );

	/// <summary>
	/// For scripting button state changes
	/// </summary>
	void makeState(ButtonState newstate);

	bool		 keepBackgroundOn;
private:

	static ButtonObject* deSerializeANDT(FILE* fp);
	static ButtonObject* deSerializeANT(FILE* fp);
	static ButtonObject* deSerializeAN(FILE* fp);
	static ButtonObject* deSerializeAND(FILE* fp);
	static ButtonObject* deSerializeAD(FILE* fp);
	static ButtonObject* deSerializeANDST(FILE* fp);

protected:

	virtual void gentleBreathe(float dt);

	bool doBreathe;
	ButtonState   currentState;
	GameObject 	 *background;
	SpriteObject *backgroundNormal;
	SpriteObject *backgroundActive;
	SpriteObject *backgroundSelected;
	SpriteObject *backgroundDisabled;
	TextLabel 	 *titleLabel;
};

}
#endif

#ifndef INCLUDE_SPRITEOBJECT_H_
#define INCLUDE_SPRITEOBJECT_H_

#include "SIEngine/Objects/GameObject.h"
#include "SIEngine/Resources/SpriteData.h"

using namespace SIRendering;

namespace SICore
{

class SpriteObject : public GameObject
{
	CLASSEXTENDS(SpriteObject, GameObject);
	ADD_TO_CLASS_MAP;
public:

	SpriteObject();
	SpriteObject(SpriteData* spriteData);
	virtual ~SpriteObject();

	/// <summary> 
	/// Default draw mode for most sprites
	/// </summary> 
	static SpriteDrawMode* g_SpriteDrawMode;

	/// <summary> 
	/// GameObject
	/// </summary> 
	virtual void render();
	virtual void update( float dt );
	virtual bool processInput( IEventMessage *message);
	virtual bool hitTest(int x, int y);
	virtual void computeLocalBounds();
	virtual String toString();
	void debugLogVerts();

	/// <summary> 
	/// Sets sprite data
	/// </summary>
	void setSpriteData(SpriteData* spriteData_ );

	/// <summary>
	///IRenderable requirement, Index buffer, NULL if not implemented
	/// </summary> 
	virtual int* 	getIndexBuffer();

	/// <summary>
	///IRenderable requirement, Vertex buffer, null if not implemented
	/// </summary>
	virtual float*  getVertexBuffer();

	/// <summary>
	/// IRenderable requirement, Vertex buffer, NULL if not implemented
	/// </summary>
	virtual int 	getVertexCount();

	/// <summary>
	///IRenderable requirement, Vertex buffer, -1 if not implemented
	/// </summary>
	virtual int 	getIndexCount();

	/// <summary> 
	/// Load from file
	/// </summary> 
	static SpriteObject* deSerialize( FILE* fp );

	/// <summary> 
	/// Actions for sprites
	/// </summary> 
	void colorTo( Color dest, float time );

	/// <summary> 
	/// Content dimensions
	/// </summary> 
	void setWidth( float w );

	/// <summary>
	/// Content dimensions
	/// </summary>
	void setHeight( float h );

	/// <summary>
	/// Set size updates the vertices in model space using these dimensions
	/// </summary>
	void setSize( Size size);

	/// <summary>
	/// Returns content size
	/// </summary>
	Size getSize();

	LitTexturedPositionVertex getVertex(int index);

	/// <summary>
	/// Set which frame of the an animation
	/// </summary>
	void setFrame(int frame);

	/// <summary>
	/// Plays a named animation from sprite data
	/// </summary>
	void playAnimation( String animationName );

	/// <summary>
	/// Gets the sprite data struct
	/// </summary>
	SpriteData* getSpriteData();

	/// <summary>
	/// Certain shaders use the custom vertex format for coloring the verts
	/// </summary>
	void setVertColors(Color c);
	static SpriteObject* createSprite(SpriteData* data, String name, Size size);
	Color getColor();
	void setColor(Color newColor);

	/// <summary>
	/// Precanned animations
	/// </summary>
	virtual void fadeIn(float delay = 0);
	virtual void fadeOut(float delay = 0);

	virtual void setOriginal();

protected:
	void init();
	virtual void updateVertices();
	void updateUVs();
	Color tintColor;
	int currentFrame;
	SpriteData* spriteData;
	LitTexturedPositionVertex vertices[6];
	float width;
	float height;
};

}
#endif

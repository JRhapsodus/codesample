#ifndef INCLUDE_MESHOBJECT_H_
#define INCLUDE_MESHOBJECT_H_

#include <GLES2/gl2.h>
#include "SIEngine/Objects/GameObject.h"
#include "SIEngine/Resources/Mesh.h"

namespace SICore
{

class MeshObject : public GameObject
{
	CLASSEXTENDS(MeshObject, GameObject);
public:
	static IndexColoredMode* g_IndexedMeshDrawMode;
	MeshObject();
	MeshObject(Mesh* mesh);
	virtual ~MeshObject();

	/// <summary> 
	/// ActionTest
	/// </summary> 
	void actionTest();

	/// <summary> 
	/// GameObject
	/// </summary> 
	virtual void render();
	virtual void update( float dt );
	virtual bool processInput( IEventMessage *message);
	virtual bool hitTest(int x, int y);
	virtual void computeLocalBounds();


	static MeshObject* deSerialize(FILE* fp);

	/// <summary> 
	///IRenderable
	/// </summary> 
	virtual int* 	getIndexBuffer();
	virtual float*  getVertexBuffer();
	virtual int 	getVertexCount();
	virtual int 	getIndexCount();

	///this belongs to Mesh class possibly?
	virtual Triangle3f* getTriangles();

private:
	Mesh* mesh;
	void init();
};

}
#endif

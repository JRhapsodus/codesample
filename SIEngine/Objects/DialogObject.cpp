#include "DialogObject.h"
#include "SIEngine/Core/Scene.h"

#include "Glasgow/Scenes/SceneManager.h"
using namespace Glasgow;

namespace SICore
{

DialogObject::DialogObject(SpriteObject* background_, ButtonObject* button1_, ButtonObject* button2_,
		TextLabel* messageLabel_, SpriteObject* screenDarken_, TextLabel* titleLabel_) : super()
{
	rootNode = new GameObject();

	addChild(rootNode);
	rootNode->release();
	rootNode->addChild(background_);
	rootNode->addChild(button1_);
	rootNode->addChild(button2_);
	rootNode->addChild(messageLabel_);
	rootNode->addChild(titleLabel_);
	addChild(screenDarken_);

	screenDarken = screenDarken_;
	button1 = button1_;
	button2 = button2_;
	promptLabel = messageLabel_;
	titleLabel = titleLabel_;

	Size s = background_->getSize();
	promptLabel->setMaxWidth(530);


}

DialogObject::~DialogObject()
{
	printf("DialogObject::~DialogObject()\n");
}

//************************************************************************
// Two types of dialogs, single button and a 2 button
//************************************************************************
void DialogObject::setup( DialogType type, DialogOptions options_ )
{
	options 	= options_;
	dialogType 	= type;

	if( !options.hasTwoButtons() )
	{
		if (button2 != NULL)
			button2->setVisible(false);
		promptLabel->setAlignment(TextAlignmentCenter);
		promptLabel->setPosition( Vec3f(0, 133, 0));
		button1->setPosition( Vec3f( 0, -228, 0 ));
		((TextLabel*)button1->find("text"))->setText(options_.buttonA.display);
	}
	else
	{
		((TextLabel*)button1->find("text"))->setText(options_.buttonA.display);
		((TextLabel*)button2->find("text"))->setText(options_.buttonB.display);
	}

	titleLabel->setText(options.title);
	promptLabel->setText(options.prompt);

}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void DialogObject::addHotSpots( ObjectArray* trackingList )
{
	if( button1 != null )
		trackingList->addElement(button1);
	if ( button2 != null )
		trackingList->addElement(button1);
}

/// <summary>
/// Returns type of dialog for id'ing
/// </summary>
DialogType DialogObject::getDialogType()
{ return dialogType; }

//************************************************************************
// Fade and scale In
//************************************************************************
void DialogObject::animateIn()
{
	setPosition(Vec3f::Zero + Vec3f::Back*1500);
	screenDarken->setOpacity(0);
	screenDarken->fadeTo(0.75f, 0.25f);
	rootNode->setScale(Vec3f::Zero);
	rootNode->scaleTo(Vec3f::One, 0.7f);
}

//************************************************************************
// Fade and scale out
//************************************************************************
void DialogObject::animateOut()
{
	screenDarken->fadeTo(0, 0.7f);
	rootNode->scaleTo(Vec3f::Zero, 0.7f);

	waitFor(0.7f);
	kill();
}

//************************************************************************
// See if we selected one of our two options
//************************************************************************
void DialogObject::onActionPress()
{
	Scene* currentscene = SceneManager::getInstance()->getScene();
	GameObject* highlight = currentscene->getCurrentHighlight();

	if (highlight == button1 )
	{
		currentscene->onDialogHandled( dialogType, options.buttonA );
		animateOut();
	}
	else if ( highlight == button2 )
	{
		currentscene->onDialogHandled( dialogType, options.buttonB );
		animateOut();
	}
}

void DialogObject::cancel()
{
	Scene* currentscene = SceneManager::getInstance()->getScene();
	currentscene->onDialogCanceled( dialogType );
	animateOut();
}

//************************************************************************
// Allow support to load from prefab
//************************************************************************
DialogObject* DialogObject::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> background((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button1( (ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button2( (ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> messageLabel( (TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> screenDarken( (SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> titleLabel( (TextLabel*)Scene::deSerializeNext(fp));
	return new DialogObject(background, button1, button2, messageLabel, screenDarken, titleLabel);
}

}

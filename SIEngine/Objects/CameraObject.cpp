#include "CameraObject.h"

namespace SICore
{

CameraObject *CameraObject::perspective(bool leftHanded)
{
	CameraObject *cam = new CameraObject();
	cam->setupForPerspective(leftHanded);
	return cam;
}

CameraObject *CameraObject::orthographic(bool leftHanded)
{
	CameraObject *cam = new CameraObject();
	cam->setupForOrthographic(leftHanded);
	return cam;
}

Matrix CameraObject::getViewMatrix()
{ return transform.getLocalToWorld().inverse(); }

void CameraObject::setCullingMask( int cullingMask )
{ cullingLayerMask = cullingMask; }

int CameraObject::getCullingMask()
{ return cullingLayerMask; }

//********************************************************************************
// Life cycle
//********************************************************************************
CameraObject::CameraObject() : super()
{
	isOrthographic = true;
	name = "Camera";
	leftHanded = false;
	cullingLayerMask = 0;
	renderOrder = 0;
	clearOptions = GL_DEPTH_BUFFER_BIT;
	clearColor = Color::Clear;
}

CameraObject::~CameraObject()
{
	printf("CameraObject::~CameraObject\n");
}

//********************************************************************************
// cullingMask renderOrder
//********************************************************************************
CameraObject* CameraObject::deSerialize(FILE* fp)
{
	int renderOrder = 0;
	int cullingMask = 0;

	fscanf(fp, "%d %d", &cullingMask, &renderOrder);
	CameraObject* newCamera = CameraObject::perspective(true);
	newCamera->setCullingMask(cullingMask);
	newCamera->renderOrder = renderOrder;
	return newCamera;
}
//********************************************************************************
// Returns a frustum representing the viewing plane bounds of the camera
//********************************************************************************
Frustum CameraObject::getFrustum()
{
	Matrix local2World = transform.getLocalToWorld();
	return Frustum(local2World,projection);
}

//********************************************************************************
// Nothing as we don't take up any dimensions in world
//********************************************************************************
void CameraObject::computeLocalBounds()
{
	//nothing needed
}

//********************************************************************************
// Setups camera for orthographic projection
//********************************************************************************
void CameraObject::setupForOrthographic(bool isleft)
{
	leftHanded = isleft;
	isOrthographic = true;
	if( leftHanded )
	{ projection = Matrix::orthoLH(1280, 720, 0.01f, 10000); }
	else
	{ projection = Matrix::orthoRH(1280, 720, 0.01f, 10000); }

	name = "OrthographicCamera";
}

//********************************************************************************
// Compares 2 cameras to determine rendering order
//********************************************************************************
int CameraObject::compareByRank( const void* first, const void* second )
{
	const BaseObject* boFirst = *(const BaseObject **)first;
	const BaseObject* boSecond = *(const BaseObject **)second;
	const CameraObject* bopFirst = (CameraObject*)boFirst;
	const CameraObject* bopSecond = (CameraObject*)boSecond;

	int oZ = bopFirst->renderOrder;
	int sZ = bopSecond->renderOrder;

	if( oZ == sZ )
	{ return 0; }
	else if ( oZ < sZ )
	{ return -1; }

	return 1;
}
//********************************************************************************
// Setups camera for perspective projection
//********************************************************************************
void CameraObject::setupForPerspective(bool isleft)
{
	//Field of View, Aspect Ratio, Near Plane, Far Plane
	leftHanded = isleft;
	isOrthographic = false;

	if ( leftHanded )
	{ projection = Matrix::matrixPerspectiveLH(60.0f, 1280.0f/720.0f, 10, 5000); }
	else
	{ projection = Matrix::matrixPerspectiveRH(60.0f, 1280.0f/720.0f, 10, 5000); }

	setPosition( Vec3f( 0, 0, 8) );
	name = "PerspectiveCamera";
}

}

#include "QtSpriteObject.h"

namespace SICore
{

QtSpriteObject::QtSpriteObject(float width, float height, String texture)
{
	textureLoaded = false;
	textureToLoad = texture;
	contentWidth = width;
	contentHeight = height;
}

QtSpriteObject::~QtSpriteObject()
{ }

Size QtSpriteObject::getContentSize()
{
	if( clipRect.size_.width_ > 0 )
	{ return Size(clipRect.size_.width_, contentHeight); }

	return Size(contentWidth, contentHeight);
}

void QtSpriteObject::loadTexture()
{
	textureLoaded = true;
	imageBuffer.createFromPng( AssetLibrary::getQtImagePath(textureToLoad).str());
}


CBlitBuffer* QtSpriteObject::getBlitBuffer()
{
	if( !textureLoaded )
	{ loadTexture(); }

	return &imageBuffer;
}

/// <summary>
/// Overriden because in a blitting renderer, our dimensions behave different
/// without a proper transform matrix
/// </summary>
Box3f QtSpriteObject::getLocalBoundingBox()
{
	if( clipRect.size_.width_ > 0 )
	{
		return Box3f( Box3f(0, 0, 0, clipRect.size_.width_, contentHeight, 1) );
	}
	else
	{
		return Box3f( Box3f(0, 0, 0, contentWidth, contentHeight, 1) );
	}

}

/// <summary>
/// Overriden because in a blitting renderer, our dimensions behave different
/// without a proper transform matrix
/// </summary>
Box3f QtSpriteObject::getWorldBoundingBox()
{ return getScreenBoundingBox(NULL); }

/// <summary>
/// Overriden because in a blitting renderer, our dimensions behave different
/// without a proper transform matrix
/// </summary>
Box3f QtSpriteObject::getScreenBoundingBox(CameraObject* camera)
{
	if( clipRect.size_.width_ > 0 )
	{
		Vec3f position = getWorldPosition();
		return Box3f(640 + position.x - contentWidth/2,360 - position.y - contentHeight/2, 0, clipRect.size_.width_, contentHeight, 1);
	}
	else
	{
		Vec3f position = getWorldPosition();
		return Box3f(640 + position.x - contentWidth/2,360 - position.y - contentHeight/2, 0, contentWidth, contentHeight, 1);
	}
}

/// <summary>
/// This should be a subclassed feature
/// </summary>
void QtSpriteObject::setClipRectBasedOnPercent( float dt )
{ clipRect = Rect( Vec2f(0,0), Size( contentWidth*dt, contentHeight) ); }

/// <summary>
/// Load from file
/// </summary>
QtSpriteObject* QtSpriteObject::deSerialize( FILE* fp )
{
	char textureName[250];
	float width, height;
	if( fscanf(fp, "%f %f %249s ", &width, &height, textureName ) == 3 )
	{ return new QtSpriteObject( width, height, textureName); }
	return NULL;
}

} /* namespace Glasgow */

# About #

   This is a code sample from the LeapTV gaming console project. It show cases code that demonstrates a custom renderer, geometry processing, collections, code structure, shader & material implementations, and various other techniques applicable to gaming technologies. 
   The system classes and API were modelled after Java's naming conventions and represents a light weight c++ implementation of Java.
   Additionally, a lot of these calls matched Unity's implementation 1:1

# Performance #
   The console ran at over 100 frames per second running on the equivalent of an ODroid 2: 2 GB Ram, Mali Graphics Card [low float precision], 1.5 Ghz Quad Core. The system OS was Ubuntu. No known memory leaks.

# Structure #
   The SIEngine folder contains engine code and the Glasgow folder represents production implementation of the system.

Written using C and C++ by Jeston Furqueron
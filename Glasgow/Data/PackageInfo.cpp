#include "PackageInfo.h"
#include <sys/stat.h>
#include <SIEngine/Include/SIUtils.h>
using namespace LTM;
namespace Glasgow
{

PackageInfo::PackageInfo(PackageData* packageData_) : super()
{
	printf("PackageInfo::PackageInfo\n");
	packageData = packageData_;

	cartridgeDependencies = 0;
	isVideoBundle = false;
	appName = packageData->appName().toStdString().c_str();
	productID = packageData->productID();
	packageID = packageData->packageID().toStdString().c_str();
	appDirPath = packageData->appDirPath().toStdString().c_str();
	appSoPath = String(packageData->appSoPath().toStdString().c_str());
	category = packageData->category().toStdString().c_str();
	strtype = packageData->type().toStdString().c_str();
	statusAsString = packageData->statusAsString().toStdString().c_str();
	iconPath = packageData->iconPath().toStdString().c_str();
	installDate = packageData->installDate().toStdString().c_str();
	installSize = packageData->installSize();
	label = packageData->label().toStdString().c_str();
	getErrorString = packageData->getErrorString().toStdString().c_str();
	downloadProgress = packageData->downloadProgress();
	isNoLaunch = packageData->isNoLaunch();
	isDeletable = packageData->isDeletable();
	isAppQt = packageData->isAppQt();
	hideableType = packageData->hideableType();
	audioPath = packageData->audioPath().toStdString().c_str();

	connect(packageData, SIGNAL(statusChanged(tPackageStatus)), this, SLOT(onStateChanged(tPackageStatus)));
	connect(packageData, SIGNAL(onDownloadProgressUpdated(int)), this, SLOT(onDownloadProgressUpdated(int)));
	verifyPaths();
	determineIfVideoBundle();
}

PackageInfo::~PackageInfo()
{
	disconnect(packageData, SIGNAL(onDownloadProgressUpdated(int)), this, SLOT(onDownloadProgressUpdated(int)));
	disconnect(packageData, SIGNAL(statusChanged(tPackageStatus)), this, SLOT(onStateChanged(tPackageStatus)));
	printf("PackageInfo::~PackageInfo()\n");
}

/// <summary>
/// True if this info represents the cartridge
/// </summary>
bool PackageInfo::isCartridge()
{ return appDirPath.toUpperCase().startsWith("/LF/CART/"); }

/// <summary>
/// Helper function that out puts the json value
/// </summary>
void PackageInfo::printJSONValue( const Value &val )
{
	if( val.isString() ) { printf( "%s\n", val.asString().c_str() ); }
	else if( val.isBool() ) { printf( "%d\n", val.asBool() ); }
	else if( val.isInt() ) { printf( "%d\n", val.asInt() ); }
	else if( val.isUInt() ) { printf( "%u\n", val.asUInt() ); }
	else if( val.isDouble() ) { printf( "%f\n", val.asDouble() ); }
	else { printf("\n"); }
}

/// <summary>
/// Download progressed
/// </summary>
void PackageInfo::onDownloadProgressUpdated(int dlProgress)
{ ContentManager::getInstance()->onPackageDownloadProgress(this, dlProgress); }

/// <summary>
/// New state from package status
/// </summary>
void PackageInfo::onStateChanged(tPackageStatus newStatus )
{ ContentManager::getInstance()->onPackageChanged(newStatus,this); }

/// <summary>
/// Helper function
/// </summary>
VideoInfo* PackageInfo::createVideoInfo( String videoName, String iconPath, String baseDir )
{ return new VideoInfo( baseDir + String("VideoInfo.json"), baseDir, iconPath, videoName, videoName ); }

/// <summary>
/// Hard wired test
/// </summary>
bool PackageInfo::isPetPlayWorld()
{ return (productID == Constants::Content::petPlayWorldProductID); }

VideoInfo* PackageInfo::createVideoInfo( const Value &val )
{
	String activityID;
	String iconPath;
	String baseDir;
	String videoTitle;

	for( Value::ObjectValues::const_iterator itr = val.asMap().begin(); itr != val.asMap().end(); itr++ )
	{
		String strValue = "";
		String key 	 = itr->first.c_str();
		if( itr->second.isString() )
		{ strValue = itr->second.asCString(); }

		if( key.equalsIgnoreCase("ACTIVITYID") )
		{ activityID = strValue; }
		if( key.equalsIgnoreCase("ICON") )
		{ iconPath = strValue; }
		if( key.equalsIgnoreCase("PATH") )
		{ baseDir = strValue; }
		if( key.equalsIgnoreCase("VIDEOTITLE") )
		{ videoTitle = strValue; }
	}

	return new VideoInfo( appDirPath + baseDir + String("/") + String("VideoInfo.json"), appDirPath + baseDir, appDirPath + iconPath, activityID, videoTitle );
}

/// <summary>
/// Parses an entire BundleInfo.json to fill in videos array
/// </summary>
bool PackageInfo::parseBundleInfo( const Value &root, unsigned short depth /* = 0 */)
{
	depth += 1;
	if( root.size() > 0 )
	{
		for( Value::ObjectValues::const_iterator itr = root.asMap().begin(); itr != root.asMap().end(); itr++ )
		{
			for( int tab = 0 ; tab < depth; tab++)
			{ printf("\t"); }

			parseBundleInfo( itr->second, depth);

			//*****************************************
			// Creat key:value string
			//*****************************************
			String strValue = "";
			String key 	 = itr->first.c_str();
			if( itr->second.isString() )
			{ strValue = itr->second.asCString(); }

			//*****************************************
			// We have a new Video
			//*****************************************
			if( itr->second.isArray() || itr->second.isObject() )
			{
				TRelease<VideoInfo> videoInfo( createVideoInfo(itr->second) );

				videoInfo->backGroundImage 	= getValueFromJSON( root, "Background");
				videoInfo->fontColor 		= getValueFromJSON( root, "FontColor");
				videoInfo->bundleTitle 		= getValueFromJSON( root, "BundleTitle");

				videos.addElement(videoInfo);
			}
		}

		return true;
	}

	return true;
}

/// <summary>
/// Determines whether the path coming from the PackageData contains an appSoPath
/// that is a full path or a relative path to the base directory
/// </summary>
void PackageInfo::verifyPaths()
{
	printf("PackageInfo::verifyPaths()\n");
	//full path test
	if( !AssetLibrary::fileExists(audioPath) )
	{
		//try relative
		audioPath = appDirPath + audioPath ;
	}

	//Verify app.so path
	printf("PackageInfo::verifyPaths()\n");
	//Video bundles don't have an app, so its not really needed to verify
	if( isVideoBundle )
		return;

	//appSo path is completely empty, this is bad leap data. Lets default to App.so
	if( appSoPath.isEmpty() )
	{
		appSoPath = appDirPath + "App.so";
		printf("appSoPath is empty, defaulting to built full path [%s]\n", appSoPath.str());
	}

	//This will evaluate to true if there is a file represented by appSoPath
	if ( !AssetLibrary::executableExists(appSoPath) )
	{
		printf("No executable file found at path [%s]\n", appSoPath.str());

		//In case the Directory path does not end in the trailing
		if( !appDirPath.endsWith("/") )
			appDirPath.append("/");

		//Lets try to build a full path using the relative pieces
		String fullPath = appDirPath + appSoPath;
	}
	else
	{
		printf("App.so file found at [%s]\n", appSoPath.str());
	}
}

tPackageStatus PackageInfo::getPackageStatus()
{ return packageData->status(); }


/// <summary>
/// Helper function that treats the json node like a key->value gettor
/// </summary>
String PackageInfo::getValueFromJSON(const Value & rootNode, String keyName )
{
	for(Value::ObjectValues::const_iterator itr=rootNode.asMap().begin(); itr != rootNode.asMap().end(); itr++)
	{
		String key = itr->first.c_str();
		if( itr->second.isString() )
		{
			if( key == keyName )
			{ return itr->second.asCString(); }
		}
	}

	return "";
}

/// <summary>
/// Called to run the load sequence for a video bundle determination
/// </summary>
void PackageInfo::determineIfVideoBundle()
{
	printf("PackageInfo::determineIfVideoBundle()\n");
	String bundleJsonPath = appDirPath + "BundleInfoTHDS.json";
	CPath bundleCPath = bundleJsonPath.str();
	CJSonFile* file = new CJSonFile( bundleCPath, false);
	tErrType error = file->Load();

	if ( error == kNoErr )
	{
		isVideoBundle = true;
		parseBundleInfo(file->mRoot, 0);
	}

	String videoJsonPath = appDirPath + "VideoInfo.json";
	CPath videoCPath = videoJsonPath.str();
	CJSonFile* videoFile = new CJSonFile( videoCPath, false);
	tErrType videoError = videoFile->Load();
	if ( videoError == kNoErr )
	{
		isVideoBundle = true;

		VideoInfo* videoInfo = new VideoInfo( videoJsonPath, appDirPath, appDirPath + iconPath, appName, appName );
		videos.addElement(videoInfo);
		videoInfo->bundleTitle = appName;
		videoInfo->release();
	}

	delete videoFile;
	delete file;
}

/// <summary>
/// Prints out struct to console
/// </summary>
void PackageInfo::debugLog()
{
	printf("-------------------------------------------------\n");
	printf("appName          \t\t%s\n", appName.str());
	printf("productID        \t\t%d\n", productID );
	printf("packageID        \t\t%s\n", packageID.str());
	printf("appSoPath        \t\t%s\n", appSoPath.str());
	printf("audioPath        \t\t%s\n", audioPath.str());
	printf("appDirPath       \t\t%s\n", appDirPath.str());
	printf("category         \t\t%s\n", category.str());
	printf("type             \t\t%s\n", strtype.str());
	printf("statusAsString   \t\t%s\n", statusAsString.str());
	printf("iconPath         \t\t%s\n", iconPath.str());
	printf("installDate      \t\t%s\n", installDate.str());
	printf("installSize      \t\t%d\n", installSize);
	printf("label            \t\t%s\n", label.str());
	printf("getErrorString   \t\t%s\n", getErrorString.str());
	printf("downloadProgress \t\t%d\n", downloadProgress);
	printf("isNoLaunch       \t\t%d\n", (int)isNoLaunch);
	printf("isDeletable      \t\t%d\n", (int)isDeletable);
	printf("isAppQt          \t\t%d\n", (int)isAppQt);
	printf("hideableType     \t\t%d\n", (int)hideableType);
	printf("depencies        \t\t%d\n", cartridgeDependencies);

	if( isVideoBundle )
	{
		for( int i =0 ; i<videos.getSize(); i++ )
		{
			VideoInfo* info = (VideoInfo*)videos.elementAt(i);
			info->debugLog();
		}
	}

	printf("-------------------------------------------------\n");
}
}

#include "GradeInfo.h"

namespace Glasgow
{

HashTable GradeInfo::gradeEnumTable;
HashTable GradeInfo::gradeNameTable;

GradeInfo::GradeInfo(String nameOfGrade, int leapEnum, String key):super()
{
	gradeName = nameOfGrade;
	leapGradeEnum = leapEnum;
	localizationKey = key;
}

GradeInfo::~GradeInfo()
{
	printf("GradeInfo::~GradeInfo\n");
}

/// <summary>
/// Adding an explicit clearTable because I do not want global or
/// class spaced  stack objects being destructed past main()....
/// Lets keep control of the timing of destruction
/// </summary>
void GradeInfo::clearTable()
{
	gradeEnumTable.clear();
	gradeNameTable.clear();
}

//LTM.h enum
//enum {
//	kGrade_NoGrade=0x7F, //127
//	kGrade_EarlyPreK=0xFE, //254
//	kGrade_PreK=0xFF, //255
//	kGrade_K=0,
//	kGrade_1,
//	kGrade_2,
//	kGrade_3,
//	kGrade_4,
//	kGrade_5,
//	kGrade_6,
//	kGrade_7,
//	kGrade_8
//};

/// <summary>
/// This is a Class space global accessor, only this class should be creating GradeInfo*; to be cleared at app's end
/// { "gradePreschool", "gradePreK", "gradeKindergarten", "grade1", "grade2", "grade3","grade4", "grade5", "grade6", "gradeAdult"};
/// </summary>
void GradeInfo::fillTable()
{
	printf("GradeInfo::fillTable\n");

	TRelease<GradeInfo> gradePreschool(new GradeInfo("gradePreschool",254, "PROFILE_GRADE_GRADEPRESCHOOL"));
	gradeEnumTable[gradePreschool->leapGradeEnum] = gradePreschool;
	gradeNameTable[gradePreschool->gradeName] = gradePreschool;

	TRelease<GradeInfo> gradePreK(new GradeInfo("gradePreK",255, "PROFILE_GRADE_GRADEPREK"));
	gradeEnumTable[gradePreK->leapGradeEnum] = gradePreK;
	gradeNameTable[gradePreK->gradeName] = gradePreK;

	TRelease<GradeInfo> gradeKindergarten(new GradeInfo("gradeKindergarten",0, "PROFILE_GRADE_GRADEKINDERGARTEN"));
	gradeEnumTable[gradeKindergarten->leapGradeEnum] = gradeKindergarten;
	gradeNameTable[gradeKindergarten->gradeName] = gradeKindergarten;

	TRelease<GradeInfo> grade1(new GradeInfo("grade1",1,"PROFILE_GRADE_GRADE1"));
	gradeEnumTable[grade1->leapGradeEnum] = grade1;
	gradeNameTable[grade1->gradeName] = grade1;

	TRelease<GradeInfo> grade2(new GradeInfo("grade2",2,"PROFILE_GRADE_GRADE2"));
	gradeEnumTable[grade2->leapGradeEnum] = grade2;
	gradeNameTable[grade2->gradeName] = grade2;

	TRelease<GradeInfo> grade3(new GradeInfo("grade3",3,"PROFILE_GRADE_GRADE3"));
	gradeEnumTable[grade3->leapGradeEnum] = grade3;
	gradeNameTable[grade3->gradeName] = grade3;

	TRelease<GradeInfo> grade4(new GradeInfo("grade4",4,"PROFILE_GRADE_GRADE4"));
	gradeEnumTable[grade4->leapGradeEnum] = grade4;
	gradeNameTable[grade4->gradeName] = grade4;

	TRelease<GradeInfo> grade5(new GradeInfo("grade5",5,"PROFILE_GRADE_GRADE5"));
	gradeEnumTable[grade5->leapGradeEnum] = grade5;
	gradeNameTable[grade5->gradeName] = grade5;

	TRelease<GradeInfo> grade6(new GradeInfo("grade6",6, "PROFILE_GRADE_GRADE6"));
	gradeEnumTable[grade6->leapGradeEnum] = grade6;
	gradeNameTable[grade6->gradeName] = grade6;

	TRelease<GradeInfo> grade7(new GradeInfo("grade7",7, "PROFILE_GRADE_GRADE7"));
	gradeEnumTable[grade7->leapGradeEnum] = grade7;
	gradeNameTable[grade7->gradeName] = grade7;

	TRelease<GradeInfo> grade8(new GradeInfo("grade8",8, "PROFILE_GRADE_GRADE8"));
	gradeEnumTable[grade8->leapGradeEnum] = grade8;
	gradeNameTable[grade8->gradeName] = grade8;

	TRelease<GradeInfo> gradeAdult(new GradeInfo("gradeAdult",127,"PROFILE_GRADE_GRADEADULT"));
	gradeEnumTable[gradeAdult->leapGradeEnum] = gradeAdult;
	gradeNameTable[gradeAdult->gradeName] = gradeAdult;
}

/// <summary>
/// Gets a grade info based on an int key
/// </summary>
GradeInfo* GradeInfo::getGradeInfo( int leapEnum )
{
	BaseObject* bo = gradeEnumTable[leapEnum];
	if(bo == null)
		bo = gradeEnumTable[127];

	return (GradeInfo*)bo;
}

/// <summary>
/// Gets a grade info based on a string key
/// </summary>
GradeInfo* GradeInfo::getGradeInfo( String gradeName )
{
	BaseObject* bo = gradeNameTable[gradeName];
	if(bo == null)
		bo = gradeNameTable["gradeAdult"];

	return (GradeInfo*)bo;
}

}

/*
 * ClosedCaption.h
 *
 *  Created on: Jun 24, 2013
 *      Author: lfu
 */

#ifndef CLOSEDCAPTION_H_
#define CLOSEDCAPTION_H_

//#define MAXLINES 2
#include "SIEngine/Include/LFIncludes.h"
#include <GameStateHandler.h>
#include <iostream>
#include <vector>
#include <sstream>
#include <map>


#define NUMLINES 8

using namespace LeapFrog::Brio;

class ClosedCaption
{
public:
	ClosedCaption();
	virtual ~ClosedCaption();

	struct CaptionData
	{
		CaptionData()
		{
			for(int i=0; i<NUMLINES; ++i)
			{
				text[i] = "";
				row[i] = 1;
				col[i] = 0;
				color[i] = 0xffffffff;
				bUnderlined[i] = false;
			}
			currentLine = 0;
		}

		CString text[NUMLINES];
		int row[NUMLINES];
		int col[NUMLINES];
		U32 color[NUMLINES];
		bool bUnderlined[NUMLINES];

		int currentLine;
	};

	bool init(CPath path);
	//void close();
	void setOffset(S64 time);
	CaptionData *update(S64 time);

	CaptionData captionData;
	std::map<S64, CaptionData> captionDataList;

private:
	void getData();
	void addData();

	void convertData(std::string sCode);
	char getChar(std::string code);
	char getSpecialChar(std::string sCode);
	S64 getTime(const std::string timeString);
	void resetCaption();
	bool setPAC(char hiByte, char loByte);
	bool setColumn(int colData);
	void setColumnData(U32 color, int column, bool bUnderlined);

	std::vector<std::string> split(const std::string &s, char delim);
	std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);

	FILE * pFile;

	CString text;
	S64 nextTime;
	S64 offsetTime;

	bool bRclSet;
	bool bPacSet;

	std::string lastCode;

	S64 thisTime;
	int hourOffset, minOffset, secOffset, msecOffset;
};

#endif /* CLOSEDCAPTION_H_ */

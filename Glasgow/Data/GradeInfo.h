#ifndef GRADEINFO_H_
#define GRADEINFO_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/SIGeometry.h"
#include "SIEngine/Include/SICollections.h"

namespace Glasgow
{
/// <summary>
/// Helper struct to combine a readable string paired with a leapfrog enum for the grade
/// </summary>
class GradeInfo : public BaseObject
{
	CLASSEXTENDS(GradeInfo, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	/// <summary>
	/// This is a Class space global accessor, only this class
	/// should be creating GradeInfo*; to be cleared at app's
	/// end
	/// </summary>
	static void fillTable();

	/// <summary>
	/// Adding an explicit clearTable because I do not want global or
	/// class spaced  stack objects being destructed past main()....
	/// Lets keep control of the timing of destruction
	/// </summary>
	static void clearTable();

	/// <summary>
	/// Gets a grade info based on an int key
	/// </summary>
	static GradeInfo* getGradeInfo( int leapEnum );

	/// <summary>
	/// Gets a grade info based on a string key
	/// </summary>
	static GradeInfo* getGradeInfo( String gradeName );

	/// <summary>
	/// Public for convenience of a struct
	/// </summary>
	String gradeName;

	/// <summary>
	/// Public for convenience of a struct
	/// </summary>
	String localizationKey;

	/// <summary>
	/// Public for convenience of a struct
	/// </summary>
	int leapGradeEnum;

private:
	/// <summary>
	/// Dictionary for enum primary key
	/// </summary>
	static HashTable gradeEnumTable;

	/// <summary>
	/// Dictionary for name primary key
	/// </summary>
	static HashTable gradeNameTable;

	/// <summary>
	/// Class factory pattern
	/// </summary>
	GradeInfo(String nameOfGrade, int leapEnum, String key);
	virtual ~GradeInfo();

};

} /* namespace Glasgow */

#endif /* GRADEINFO_H_ */

#ifndef SOUNDINFO_H_
#define SOUNDINFO_H_

#include "SIEngine/Include/SIBase.h"
LF_USING_BRIO_NAMESPACE();

namespace Glasgow
{
enum SoundType
{
	SoundTypeSFX,
	SoundTypeMusic,
	SoundTypeVo
};

class SoundInfo : public BaseObject
{
	CLASSEXTENDS(SoundInfo, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	SoundInfo(tAudioID audioID, String name, SoundType type);
	virtual ~SoundInfo();

	tAudioID audioID;
	String 	soundID;
	SoundType soundType;

};

}
#endif

#ifndef VIDEOINFO_H_
#define VIDEOINFO_H_

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/SIGeometry.h"
#include <PackageManager.h>
#include <PackageData.h>
#include <VideoMPI.h>
#include <VideoTypes.h>

using namespace LTM;

namespace Glasgow
{

class VideoInfo : public BaseObject
{
	CLASSEXTENDS(VideoInfo, BaseObject);
	ADD_TO_CLASS_MAP;
public:

	void debugLog();
	VideoInfo(String jsonPath, String vidName);
	VideoInfo(String videoInfoPath, String dir, String icon, String id, String videoTitle);
	virtual ~VideoInfo();

	String	bundleTitle;
	String  videoTitle;
	String	videoName;
	String 	videoFile;
	String 	audioFile;
	String	jsonPath;
	String 	fontColor;
	float  	videoTime;
	float  	frameRate;
	float	sourceWidth;
	float 	sourceHeight;
	float 	targetWidth;
	float	targetHeight;

	String 	sccFile;
	int 	sccOffset;
	bool 	disableLogging;
	String 	iconPath;
	String 	baseDir;
	String 	activityID;
	String	backGroundImage;
	tDisplayHandle 	videoHandle;
	tVideoSurf		videoSurface;
};

}
#endif

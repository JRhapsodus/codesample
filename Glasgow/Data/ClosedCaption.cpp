/*
 * ClosedCaption.cpp
 *
 *  Created on: Jun 24, 2013
 *      Author: lfu
 */

#include "ClosedCaption.h"
#include <BaseUtils.h>

using namespace LeapFrog::Brio;
using namespace std;
ClosedCaption::ClosedCaption()
{
	offsetTime = 0;
	pFile = NULL;
}

ClosedCaption::~ClosedCaption()
{
	// TODO Auto-generated destructor stub
	printf("ClosedCaption destructor\n");
}

bool ClosedCaption::init(CPath path)
{
	printf("\nOpening ClosedCaption file: %s\n", path.c_str());

	if(BaseUtils::Instance()->FileIsDir(path) || !BaseUtils::Instance()->FileExists(path))
	{
		return false;
	}

	pFile = fopen(path.c_str(), "r");

	captionDataList.clear();
	captionDataList.begin();
	resetCaption();
	captionDataList[0] = captionData;

	if(pFile!=NULL)
	{
		while(!feof(pFile))
		{
			getData();
		}
		fclose (pFile);
		//printf("\n***********************\nnumItems: %d\n", captionDataList.size());
		return true;
	}
	else
	{
		return false;
	}
}

void ClosedCaption::setOffset(S64 time)
{
	offsetTime = time;
}

ClosedCaption::CaptionData *ClosedCaption::update(S64 time)
{
	std::map<S64, CaptionData>::iterator it = captionDataList.upper_bound(time);
	//printf("[%d]\n", (int)time);
	if(--it == captionDataList.begin() || time < 0)
	{
		//printf("NULL");
		return NULL;
	}
	return &(it)->second;
}

void ClosedCaption::getData()
{
	char thisLine[256];
	CString lineElement[10];

	fgets (thisLine, 256, pFile);

	const std::string s = thisLine;
	vector<string> elems = split(s, '\t');

	if(elems.size() == 2)
	{
		thisTime = getTime(elems[0]);

		vector<string> codes = split(elems[1], ' ');


		for(int i=0; i<codes.size(); ++i)
		{
			convertData(codes[i]);
		}
	}
}

void ClosedCaption::addData()
{
	if(captionData.currentLine == 0 && !bRclSet && !bPacSet)
	{
		return;
	}

	captionData.text[captionData.currentLine] = text;
	captionDataList[thisTime + offsetTime] = captionData;

/*
	printf("\n[%d + %d = %d]\n", (int)(thisTime), (int)(offsetTime), (int)(thisTime + offsetTime));

	for( int i=0; i< captionDataList[thisTime + offsetTime].currentLine + 1; ++i)
	{
		printf("\t%s ", captionDataList[thisTime + offsetTime].text[i].c_str());
		printf("(%d, %d) ", captionDataList[thisTime + offsetTime].col[i], captionDataList[thisTime + offsetTime].row[i]);
		printf("color: 0x%x ", (int)captionDataList[thisTime + offsetTime].color[i]);
		printf("underlined: %d\n", captionDataList[thisTime + offsetTime].bUnderlined[i]);
	}
	printf("\n");
*/
	resetCaption();
}

void ClosedCaption::convertData(std::string sCode)
{
	U16 code = strtol( (const char*)sCode.c_str(), NULL, 16);
	if( code == 0x942f )
	{
		addData();
	}
	else if ( code == 0x942c && !bRclSet)
	{
		/*printf("sCode: %s\n", sCode.c_str());
		printf("[%d] ", (int)(thisTime));
		printf("[%d]\n", (int)(offsetTime));

		for( int i=0; i< captionData.currentLine + 1; ++i)
		{
			printf("%s ", captionData.text[i].c_str());
			printf("(%d, %d) ", captionData.col[i], captionData.row[i]);
			printf("color: 0x%x ", (int)captionData.color[i]);
			printf("underlined: %d\n",captionData.bUnderlined[i]);
		}*/
		addData();
	}
	else if(!bRclSet)
	{
		//init
		switch(code)
		{
		case 0x94ae:
			//printf("{ENM}");
			break;
		case 0x9420:
			//printf("{RCL}");
			bRclSet = true;
			break;
		}
		lastCode = sCode;
	}
	else if(!bPacSet)
	{
		if(sCode == lastCode) return;

		//position
		//in case of doubles
		switch(code)
		{
		case 0x94ae:
		case 0x9420:
		case 0x942c:
			return;
			break;
		}

		char hiByte = strtol( (const char*)sCode.substr(0,2).c_str(), NULL, 16);
		char loByte = strtol( (const char*)sCode.substr(2,4).c_str(), NULL, 16);

		setPAC(hiByte, loByte);

		bPacSet = true;
		lastCode = sCode;
	}
	else
	{
		if( sCode == "97a1" )
		{
			//Tab Over 1
			captionData.col[captionData.currentLine] += 1;
		}
		else if( sCode == "97a2" )
		{
			//Tab Over 2
			captionData.col[captionData.currentLine] += 2;
		}
		else if( sCode == "9723" )
		{
			//Tab Over 3
			captionData.col[captionData.currentLine] += 3;
		}
		else
		{
			CString type = sCode.substr(0,2);

			char hiByte = strtol( (const char*)sCode.substr(0,2).c_str(), NULL, 16);

			switch(hiByte)
			{
			case 0x91:
			case 0x92:
			case 0x15:
			case 0x16:
			case 0x97:
			case 0x10:
			case 0x13:
			case 0x94:
			{
				//special character
				char character;
				character = getSpecialChar(sCode);

				if( character != 0 )
				{
					text = text + character;
					//printf("[%s]%c", sCode.c_str(), character);
				}
			}
				break;
			default:
			{
				//regular character
				char character;
				for(int i=0; i<3; i+=2)
				{
					character = getChar(sCode.substr(i,i+2));

					if( character != 0 && sCode != "8080" )
					{
						text = text + character;
						//printf("%c", character);
					}
				}
			}
				break;
			}
		}
	}
}

bool ClosedCaption::setPAC(char hiByte, char loByte)
{
	//printf("[setPAC: %x %x]", hiByte, loByte);
	int rowOffset = 1 + (0x2 & (loByte>>4))/2;
	//printf("rowOffset: %d\n", rowOffset);
	//int colData = (0x1F & loByte);
	switch(hiByte)
	{
	case 0x91:
		captionData.row[captionData.currentLine] = rowOffset;
		return setColumn(loByte);
		break;
	case 0x92:
		captionData.row[captionData.currentLine] = rowOffset + 2;
		return setColumn(loByte);
		break;
	case 0x15:
		captionData.row[captionData.currentLine] = rowOffset + 4;
		return setColumn(loByte);
		break;
	case 0x16:
		captionData.row[captionData.currentLine] = rowOffset + 6;
		return setColumn(loByte);
		break;
	case 0x97:
		captionData.row[captionData.currentLine] = rowOffset + 8;
		return setColumn(loByte);
		break;
	case 0x10:
		captionData.row[captionData.currentLine] = rowOffset + 10;
		return setColumn(loByte);
		break;
	case 0x13:
		captionData.row[captionData.currentLine] = rowOffset + 11;
		return setColumn(loByte);
		break;
	case 0x94:
		captionData.row[captionData.currentLine] = rowOffset + 13;
		return setColumn(loByte);
		break;
	}
}

bool ClosedCaption::setColumn(int colData)
{
	switch(colData)
	{
	case 0xd0:
	case 0x70:
		setColumnData(0xFFFFFFFF, 0, false);
		break;
	case 0x51:
	case 0xf1:
		setColumnData(0xFFFFFFFF, 0, true);
		break;
	case 0xc2:
	case 0x62:
		setColumnData(0xFF00FF00, 0, false);
		break;
	case 0x43:
	case 0xe3:
		setColumnData(0xFF00FF00, 0, true);
		break;
	case 0xc4:
	case 0x64:
		setColumnData(0xFF0000FF, 0, false);
		break;
	case 0x45:
	case 0xe5:
		setColumnData(0xFF0000FF, 0, true);
		break;
	case 0x46:
	case 0xe6:
		setColumnData(0xFF00FFFF, 0, false);
		break;
	case 0xc7:
	case 0x67:
		setColumnData(0xFF00FFFF, 0, true);
		break;
	case 0xc8:
	case 0x68:
		setColumnData(0xFFFF0000, 0, false);
		break;
	case 0x49:
	case 0xe9:
		setColumnData(0xFFFF0000, 0, true);
		break;
	case 0x4a:
	case 0xea:
		setColumnData(0xFFFFFF00, 0, false);
		break;
	case 0xcb:
	case 0x6b:
		setColumnData(0xFFFFFF00, 0, true);
		break;
	case 0x4c:
	case 0xec:
		setColumnData(0xFFFF00FF, 0, false);
		break;
	case 0xcd:
	case 0x6d:
		setColumnData(0xFFFF00FF, 0, true);
		break;
	case 0x52:
	case 0xf2:
		setColumnData(0xFFFFFFFF, 4, false);
		break;
	case 0xd3:
	case 0x73:
		setColumnData(0xFFFFFFFF, 4, true);
		break;
	case 0x54:
	case 0xf4:
		setColumnData(0xFFFFFFFF, 8, false);
		break;
	case 0xd5:
	case 0x75:
		setColumnData(0xFFFFFFFF, 8, true);
		break;
	case 0xd6:
	case 0x76:
		setColumnData(0xFFFFFFFF, 12, false);
		break;
	case 0x57:
	case 0xf7:
		setColumnData(0xFFFFFFFF, 12, true);
		break;
	case 0x58:
	case 0xf8:
		setColumnData(0xFFFFFFFF, 16, false);
		break;
	case 0xd9:
	case 0x79:
		setColumnData(0xFFFFFFFF, 16, true);
		break;
	case 0xda:
	case 0x7a:
		setColumnData(0xFFFFFFFF, 20, false);
		break;
	case 0x5b:
	case 0xfb:
		setColumnData(0xFFFFFFFF, 20, true);
		break;
	case 0xdc:
	case 0x7c:
		setColumnData(0xFFFFFFFF, 24, false);
		break;
	case 0x5d:
	case 0xfd:
		setColumnData(0xFFFFFFFF, 24, true);
		break;
	case 0x5e:
	case 0xfe:
		setColumnData(0xFFFFFFFF, 28, false);
		break;
	case 0xdf:
	case 0x7f:
		setColumnData(0xFFFFFFFF, 28, true);
		break;
	default:
		return false;
		break;
	}
	return true;
}

void ClosedCaption::setColumnData(U32 color, int column, bool bUnderlined)
{
	captionData.color[captionData.currentLine] = color;
	captionData.col[captionData.currentLine] = column;
	captionData.bUnderlined[captionData.currentLine] = bUnderlined;
}

char ClosedCaption::getSpecialChar(std::string sCode)
{
	U16 code = strtol( (const char*)sCode.c_str(), NULL, 16);
	char character = 0;
	switch(code)
	{
	case 0x91b0:
		text+=L'®';
		break;
	case 0x9131:
		text+=L'°';
		break;
	case 0x9132:
		text+=L'½';
		break;
	case 0x91b3:
		text+=L'¿';
		break;
	case 0x9134:
		text+=L'™';
		break;
	case 0x91b5:
		text+=L'¢';
		break;
	case 0x91b6:
		text+=L'£';
		break;
	case 0x9137:	//musical note
		text+=L'∆';
		//character = '#';
		break;
	case 0x9138:
		text+=L'à';
		break;
	case 0x91b9:
		character = ' ';
		break;
	case 0x91ba:
		text+=L'è';
		break;
	case 0x913b:
		text+=L'â';
		break;
	case 0x91bc:
		text+=L'ê';
		break;
	case 0x913d:
		text+=L'î';
		break;
	case 0x913e:
		text+=L'ô';
		break;
	case 0x91bf:
		text+=L'û';
		break;
	case 0x9220:
		text+=L'Á';
		break;
	case 0x92a1:
		text+=L'É';
		break;
	case 0x92a2:
		text+=L'Ó';
		break;
	case 0x9223:
		text+=L'Ú';
		break;
	case 0x92a4:
		text+=L'Ü';
		break;
	case 0x9225:
		text+=L'ü';
		break;
	case 0x9226:
		text+=L'‘';
		break;
	case 0x92a7:
		text+=L'¡';
		break;
	case 0x92a8:
		text+=L'*';
		break;
	case 0x9229:
		//text+=L'’';
		break;
	case 0x922a:
		text+=L'—';
		break;
	case 0x92ab:
		text+=L'©';
		break;
	case 0x922c:
		//service mark
		text+=L'∂';
		//text+="sm";
		break;
	case 0x92ad:
		text+=L'•';
		break;
	case 0x92ae:
		text+=L'“';
		break;
	case 0x922f:
		text+=L'”';
		break;
	case 0x92b0:
		text+=L'À';
		break;
	case 0x9231:
		text+=L'Â';
		break;
	case 0x9232:
		text+=L'Ç';
		break;
	case 0x92b3:
		text+=L'È';
		break;
	case 0x9234:
		text+=L'Ê';
		break;
	case 0x92b5:
		text+=L'Ë';
		break;
	case 0x92b6:
		text+=L'ë';
		break;
	case 0x9237:
		text+=L'Î';
		break;
	case 0x9238:
		text+=L'Ï';
		break;
	case 0x92b9:
		text+=L'ï';
		break;
	case 0x92ba:
		text+=L'Ô';
		break;
	case 0x923b:
		text+=L'Ù';
		break;
	case 0x92bc:
		text+=L'ù';
		break;
	case 0x923d:
		text+=L'Û';
		break;
	case 0x923e:
		text+=L'«';
		break;
	case 0x92bf:
		text+=L'»';
		break;
	case 0x1320:
		text+=L'Ã';
		break;
	case 0x13a1:
		text+=L'ã';
		break;
	case 0x13a2:
		text+=L'Í';
		break;
	case 0x1323:
		text+=L'Ì';
		break;
	case 0x13a4:
		text+=L'ì';
		break;
	case 0x1325:
		text+=L'Ò';
		break;
	case 0x1326:
		text+=L'ò';
		break;
	case 0x13a7:
		text+=L'Õ';
		break;
	case 0x13a8:
		text+=L'õ';
		break;
	case 0x1329:
		text+=L'{';
		break;
	case 0x132a:
		text+=L'}';
		break;
	case 0x13ab:
		text+="\\";
		break;
	case 0x132c:
		text+=L'^';
		break;
	case 0x13ad:
		text+=L'_';
		break;
	case 0x13ae:
		text+=L'¦';
		break;
	case 0x132f:
		text+=L'~';
		break;
	case 0x13b0:
		text+=L'Ä';
		break;
	case 0x1331:
		text+=L'ä';
		break;
	case 0x1332:
		text+=L'Ö';
		break;
	case 0x13b3:
		text+=L'ö';
		break;
	case 0x1334:
		text+=L'ß';
		break;
	case 0x13b5:
		text+=L'¥';
		break;
	case 0x13b6:
		text+=L'¤';
		break;
	case 0x1337:
		text+=L'|';
		break;
	case 0x1338:
		text+=L'Å';
		break;
	case 0x13b9:
		text+=L'å';
		break;
	case 0x13ba:
		text+=L'Ø';
		break;
	case 0x133b:
		text+=L'ø';
		break;
	case 0x13bc:
		//Box Drawings Light Down and Right
		text+=L'⅛';
		break;
	case 0x133d:
		//Box Drawings Light Down and Left
		text+=L'⅜';
		break;
	case 0x133e:
		//Box Drawings Light Up and Right
		text+=L'⅝';
		break;
	case 0x13bf:
		//Box Drawings Light Up and Left
		text+=L'⅞';
		break;
	default:
		//Not a special character. Reposition?
		char hiByte = strtol( (const char*)sCode.substr(0,2).c_str(), NULL, 16);
		char loByte = strtol( (const char*)sCode.substr(2,4).c_str(), NULL, 16);

		++captionData.currentLine;
		if(!setPAC(hiByte, loByte))
		{
			--captionData.currentLine;
		}
		else
		{
			captionData.text[captionData.currentLine-1] = text;
			text = "";
		}

		break;
	}
	return character;
}

char ClosedCaption::getChar(std::string sCode)
{
	char code;
	code = 0x7f & strtol( (const char*)sCode.c_str(), NULL, 16);
	switch(code)
	{
	case 0x2a:
		code = 0;
		text = text + "á";
		break;
	case 0x5c:
		code = 0;
		text = text + "é";
		break;
	case 0x5e:
		code = 0;
		text = text + "í";
		break;
	case 0x5f:
		code = 0;
		text = text + "ó";
		break;
	case 0x60:
		code = 0;
		text = text + "ú";
		break;
	case 0x7b:
		code = 0;
		text = text + "ç";
		break;
	case 0x7c:
		code = 0;
		text = text + "÷";
		break;
	case 0x7d:
		code = 0;
		text = text + "Ñ";
		break;
	case 0x7e:
		code = 0;
		text = text + "ñ";
		break;
	case 0x7f:
		code = 0;
		text = text + "-";
		break;
	}

	return code;
}

S64 ClosedCaption::getTime(const std::string timeString)
{
	S64 hour;
	S64 min;
	S64 sec;
	S64 msec;

	std::vector<std::string> timeVector = split(timeString, ':');

	hour = atoi(timeVector[0].c_str());
	min = atoi(timeVector[1].c_str());

	std::vector<std::string> secondsElement = split(timeVector[2], ';');
	if(secondsElement.size() == 1)
	{
		sec = atoi(timeVector[2].c_str());
		msec = 1000*(atoi(secondsElement[1].c_str()))/30;

		//printf("sec: %d, msec: %d\n", (int)sec, (int)msec);
	}
	else
	{
		sec = atoi(secondsElement[0].c_str());
		msec = 1000*(atoi(secondsElement[1].c_str()))/30;
	}

	//define time offset from first timestamp
	if(captionDataList.size() == 1)
	{
		hourOffset = hour;
		minOffset = min;
		secOffset = sec;
		msecOffset = msec;
	}

	S64 t = (((hour-hourOffset)*3600) + ((min-minOffset)*60) + (sec-secOffset))*1000 + (msec-msecOffset);
	if (t < 1) t = 1;
	return t;
}

std::vector<std::string> &ClosedCaption::split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> ClosedCaption::split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

void ClosedCaption::resetCaption()
{
	for( int i=0; i<NUMLINES; ++i)
	{
		captionData.text[i] = "";
		captionData.row[i] = 1;
		captionData.col[i] = 0;
		captionData.color[i] = 0xFFFFFFFF;
		captionData.bUnderlined[i] = false;
	}
	captionData.currentLine = 0;

	bRclSet = false;
	bPacSet = false;
	text = "";
}

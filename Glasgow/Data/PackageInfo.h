#ifndef PACKAGEINFO_H_
#define PACKAGEINFO_H_

#include "SIEngine/Include/SIBase.h"
#include "VideoInfo.h"
#include <PackageManager.h>
#include <PackageData.h>

using namespace LTM;

namespace Glasgow
{
class PackageInfo : public QObject, public BaseObject
{
	Q_OBJECT;
	CLASSEXTENDS(PackageInfo, BaseObject);
	ADD_TO_CLASS_MAP;
public:
	PackageInfo(PackageData* packageData);
	virtual ~PackageInfo();

	/// <summary>
	/// Helper function
	/// </summary>
	VideoInfo* createVideoInfo( const Value &val );

	/// <summary>
	/// Helper function
	/// </summary>
	VideoInfo* createVideoInfo( String videoName, String iconPath, String baseDir );

	/// <summary>
	/// Hard wired test
	/// </summary>
	bool isPetPlayWorld();

	/// <summary>
	/// True if this info represents the cartridge
	/// </summary>
	bool isCartridge();

	/// <summary>
	/// Determines whether the path coming from the PackageData contains an appSoPath
	/// that is a full path or a relative path to the base directory
	/// </summary>
	void verifyPaths();

	/// <summary>
	/// Called to run the load sequence for a video bundle determination
	/// </summary>
	void determineIfVideoBundle();

	/// <summary>
	/// Prints out struct to console
	/// </summary>
	void debugLog();

	/// <summary>
	/// Helper function that treats the json node like a key->value gettor
	/// </summary>
	String getValueFromJSON(const Value& rootNode, String keyName );


	//I hate this, but I dont know enough about QT to keep the QT objects in memory,
	// There are few structs that I need to 'mirror' so this can easily be one, but ideally
	// this will be updated to find the proper way to hold a strong reference to packageData
	String appName;
	long productID;
	String packageID;
	String appSoPath;
	String appDirPath;
	String category;
	String strtype;
	String statusAsString;
	String iconPath;
	String installDate;
	long installSize;
	String label;
	String audioPath;
	String getErrorString;
	long downloadProgress;
	bool isNoLaunch;
	bool isDeletable;
	bool isAppQt;
	bool hideableType;
	bool isVideoBundle;
	ObjectArray videos;
	int	cartridgeDependencies;

	tPackageStatus getPackageStatus();

private:
	PackageData* packageData;
protected slots:
	/// <summary>
	/// New state from package status
	/// </summary>
     void onStateChanged(tPackageStatus packageStatus);

 	/// <summary>
 	/// Download progressed
 	/// </summary>
     void onDownloadProgressUpdated(int);


protected:
	/// <summary>
	/// Helper function that out puts the json value
	/// </summary>
	void printJSONValue( const Value &val );

	/// <summary>
	/// Parses an entire BundleInfo.json to fill in videos array
	/// </summary>
	bool parseBundleInfo( const Value &root, unsigned short depth = 0 );
};
}
#endif

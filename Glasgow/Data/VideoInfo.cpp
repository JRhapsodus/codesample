#include "VideoInfo.h"

namespace Glasgow
{

VideoInfo::VideoInfo(String jsonVideoPath, String vidName) : super()
{
	videoTitle = vidName;
	videoName = vidName;
	baseDir = jsonVideoPath;
	String jsonPath_ = jsonVideoPath + "VideoInfo.json";
	printf("Loading VideoInfo from file %s\n", jsonPath_.str() );

	CPath bundleCPath = jsonPath_.str();
	CJSonFile* file = new CJSonFile( bundleCPath, false);
	tErrType error = file->Load();

	if ( error == kNoErr )
	{
		jsonPath = jsonPath_;
		for( Value::ObjectValues::const_iterator itr = file->mRoot.asMap().begin(); itr != file->mRoot.asMap().end(); itr++ )
		{
			String strValue = "";
			String key 	 = itr->first.c_str();
			if( itr->second.isString() )
			{ strValue = itr->second.asCString(); }
			else if ( itr->second.isInt() )
			{ strValue = String(itr->second.asInt()); }
			else if ( itr->second.isDouble() )
			{ strValue = String(itr->second.asDouble()); }

			if( key.equalsIgnoreCase("VIDEOFILE") )
			{ videoFile = String(baseDir + strValue); }
			else if( key.equalsIgnoreCase("AUDIOFILE") )
			{ audioFile = String(baseDir + strValue); }
			else if( key.equalsIgnoreCase("VIDEOTIME") )
			{ videoTime = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("VIDEOTITLE") )
			{ videoTitle = strValue; }
			else if( key.equalsIgnoreCase("FRAMERATE") )
			{ frameRate = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("SOURCEWIDTH") )
			{ sourceWidth = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("SOURCEHEIGHT") )
			{ sourceHeight = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("TARGETWIDTH") )
			{ targetWidth = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("TARGETHEIGHT") )
			{ targetHeight = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("SCCFILE") )
			{ sccFile = baseDir + strValue; }
			else if( key.equalsIgnoreCase("SCCOFFSET") )
			{ sccOffset = strValue.toInt(); }
			else if( key.equalsIgnoreCase("DISABLELOGGING") )
			{ disableLogging = (strValue.toInt() == 0 ) ? false : true; }
		}
	}

	delete file;
}

VideoInfo::VideoInfo(String videoInfoPath, String dir, String icon, String id, String title) : super()
{
	videoName = id;
	iconPath = icon;
	baseDir = dir;
	activityID = id;
	videoTitle = title;

	CPath bundleCPath = videoInfoPath.str();
	CJSonFile* file = new CJSonFile( bundleCPath, false);
	tErrType error = file->Load();

	if ( error == kNoErr )
	{
		jsonPath = videoInfoPath;
		for( Value::ObjectValues::const_iterator itr = file->mRoot.asMap().begin(); itr != file->mRoot.asMap().end(); itr++ )
		{
			String strValue = "";
			String key 	 = itr->first.c_str();
			if( itr->second.isString() )
			{ strValue = itr->second.asCString(); }
			else if ( itr->second.isInt() )
			{ strValue = String(itr->second.asInt()); }
			else if ( itr->second.isDouble() )
			{ strValue = String(itr->second.asDouble()); }

			if( key.equalsIgnoreCase("VIDEOFILE") )
			{ videoFile = baseDir + strValue; }
			else if( key.equalsIgnoreCase("AUDIOFILE") )
			{ audioFile = baseDir + strValue; }
			else if( key.equalsIgnoreCase("VIDEOTIME") )
			{ videoTime = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("FRAMERATE") )
			{ frameRate = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("SOURCEWIDTH") )
			{ sourceWidth = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("SOURCEHEIGHT") )
			{ sourceHeight = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("TARGETWIDTH") )
			{ targetWidth = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("TARGETHEIGHT") )
			{ targetHeight = strValue.toFloat(); }
			else if( key.equalsIgnoreCase("SCCFILE") )
			{ sccFile = baseDir + strValue; }
			else if( key.equalsIgnoreCase("SCCOFFSET") )
			{ sccOffset = strValue.toInt(); }
			else if( key.equalsIgnoreCase("DISABLELOGGING") )
			{ disableLogging = (strValue.toInt() == 0 ) ? false : true; }
		}


	}

	delete file;
}

VideoInfo::~VideoInfo()
{
	printf("VideoInfo::~VideoInfo\n");
}


void VideoInfo::debugLog()
{
	printf("\tVideoInfo =========> %s\n", activityID.str());
	printf("\t\tbaseDir \t\t %s \n", baseDir.str());
	printf("\t\tvideoFile \t\t %s \n", videoFile.str());
	printf("\t\taudioFile \t\t %s \n", audioFile.str());
	printf("\t\tsccFile \t\t %s \n", sccFile.str());
	printf("\t\ticonPath \t\t %s \n", iconPath.str());
	printf("\t\tvideoTime \t\t %f \n", videoTime);
	printf("\t\tframeRate \t\t %f \n", frameRate);
	printf("\t\tsourceWidth \t\t %f \n", sourceWidth);
	printf("\t\tsourceHeight \t\t %f \n", sourceHeight);
	printf("\t\ttargetWidth \t\t %f \n", targetWidth);
	printf("\t\ttargetHeight \t\t %f \n", targetHeight);
	printf("\t\tvideoTitle \t\t %s \n", videoTitle.str());
	printf("\t\tfontColor \t\t %s \n", fontColor.str());
	printf("\t\tbackGround \t\t %s \n", backGroundImage.str());

}

}

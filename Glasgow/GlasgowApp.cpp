#include "GlasgowApp.h"
#include "MainState.h"
//#include "WNDState.h"

namespace Glasgow
{
static int  noParams = 0;
static bool firstUpdate = false;
static bool hasEntered = false;
MainState*  GlasgowApp::mainState = NULL;
GlasgowApp* GlasgowApp::appInstance = NULL;
bool g_AppShutDownGate = false;

/*
 * All initialization and memory allocations should be handled in the Enter state
 * Constructor should only be used for getting arguments that may be passed during launch.
 */
GlasgowApp::GlasgowApp(String defaultScene_, bool useQtRenderer) : App(noParams, null)
{
	printf("Enter GlasgowApp::GlasgowApp\n");
	suspended		= false;
	isQtRender 		= useQtRenderer;
	state_handler_ 	= null;
	mainState		= null;
	updateTimer 	= new QTimer(this);
	defaultScene 	= defaultScene_;

	connect(updateTimer, SIGNAL(timeout()), this, SLOT(Update()));
	debugLogLaunchParameters();
	App::FinishLoad();
}

GlasgowApp::~GlasgowApp()
{
	if( mainState != null || state_handler_ != null )
	{
		printf("The app is destroyed without releasing state handler / mainstate\n");
		printf("This is a critical mis match of the app life cycle as ::Exit should have been called first\n");
		ThrowException::invalidApiException();
	}

	printf("GlasgowApp::~GlasgowApp\n");
}


void GlasgowApp::checkLeapFrogServers()
{
	VerifyLfConnectivity(true);
}


void GlasgowApp::HandleLfConnectivityUpdate(bool isConnected)
{
	if( !suspended && !g_AppShutDownGate)
	{
		App::HandleLfConnectivityUpdate(isConnected);
		ConnectionManager::getInstance()->leapFrogServersResponded = isConnected;
	}
}

///<summary>
/// Logs all parameters inside the appData
///</summary>
void GlasgowApp::debugLogLaunchParameters()
{
	printf("mAppData launch parameters\n");
	for(QVariantMap::const_iterator iter = mAppData.begin(); iter != mAppData.end(); ++iter)
	{ printf("mAppData [%s] => [%s]\n", DEBUG_QSTRING(iter.key()), ((QString*)iter.value().data())->toStdString().c_str()); }
}


///<summary>
/// Forwarding call to get the QT App's return data struct for ::Resume
///</summary>
QVariantMap GlasgowApp::getLaunchParameters()
{ return mAppData; }

///<summary>
/// Life cycle of LeapFrog appserver App class
///</summary>
void GlasgowApp::Suspend()
{
	printf("GlasgowApp::Suspend\n");
	suspended = true;
	mainState->Suspend();
}

///<summary>
/// Life cycle of LeapFrog appserver App class
///</summary>
void GlasgowApp::Resume()
{
	printf("GlasgowApp::Resume\n");
	suspended = false;
	mainState->Resume();
	updateTimer->start(1);
}

///<summary>
/// Logs all parameters inside the returnData
///</summary>
void GlasgowApp::debugLogReturnData()
{
	printf("Outputting returnData\n");
	QVariantMap returnData;
	GetReturnedData(returnData);
	for(QVariantMap::const_iterator iter = returnData.begin(); iter != returnData.end(); ++iter)
	{ printf("GetReturnedData [%s] => [%s]\n", DEBUG_QSTRING(iter.key()), DEBUG_QSTRING(iter.value().toString())); }
}


///<summary>
/// Triggered via the qt timer
///</summary>
void GlasgowApp::Update()
{
	if( !g_AppShutDownGate )
	{
		if( suspended )
			return;

		// this will call update on the current state
		state_handler_->Run();
	}
}

///<summary>
/// Reference to the main renderer
///</summary>
BaseRenderer* GlasgowApp::getRenderer()
{ return mainState->getRenderer(); }

///<summary>
/// Forwarding call to set the QT App's return data
///</summary>
void GlasgowApp::setReturnData( QVariantMap returnMap )
{
	printf("SetReturn data\n");
	mReturnData = returnMap;
}


///<summary>
/// Life cycle of LeapFrog appserver App class
///</summary>
void GlasgowApp::Enter()
{
	if(hasEntered)
	{
		printf("GlasgowApp::Enter Enter already called.\n");
		return;
	}

	printf("GlasgowApp::Enter\n");
	hasEntered = true;
	state_handler_ = new CGameStateHandler();
	mainState = new MainState(defaultScene, isQtRender);
	state_handler_->Request(mainState, NULL);
	updateTimer->start(1);
	printf("Done GlasgowApp::Enter\n");
}

///<summary>
/// Life cycle of LeapFrog appserver App class
///</summary>
void GlasgowApp::Exit()
{
	if( !g_AppShutDownGate )
	{
		printf("GlasgowApp::Exit\n");
		g_AppShutDownGate = true;
		updateTimer->stop();
		disconnect(updateTimer, SIGNAL(timeout()), this, SLOT(Update()));
		GlasgowApp::mainState->Exit();
		delete mainState;
		delete state_handler_;
		mainState = null;
		state_handler_ = null;

	}
}

}

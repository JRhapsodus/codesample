#include "MainState.h"
#include <time.h>
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIUtils.h"
#include "Glasgow/Data/GradeInfo.h"
#include <Hardware/HWControllerEventMessage.h>
#include <MultiProfile/PlayerInfo.h>
#include <USBDeviceTypes.h>

using namespace LeapFrog::MultiProfile;

namespace Glasgow
{

using LeapFrog::Brio::U8;
using LeapFrog::Brio::U16;
using LeapFrog::Brio::CPath;
const LeapFrog::Brio::tEventType kListenerTypes[] = { kHWAllAnalogStickEvents, kHWAllControllerEvents, kAllCameraEvents, kAllButtonEvents, kAllCartridgeEvents, kAllEvenListenerEvents, kAllUSBDeviceEvents };

//*****************************************************************
// Static wand, I think there will always be 4 and only 4?
//*****************************************************************
GlasgowApp* MainState::appInstance = NULL;

//*****************************************************************
// Button Events
//*****************************************************************
U32 MainState::g_Buttons[] =
{
		kButtonUp, kButtonDown, kButtonRight, kButtonLeft, kButtonA, kButtonB, kButtonLeftShoulder,
		kButtonRightShoulder, kButtonMenu, kButtonHint, kButtonPause, kButtonBrightness, kHeadphoneJackDetect,
		kCartridgeDetect, kButtonVolumeDown, kButtonVolumeUp, kButtonEscape
};

MainState::MainState(String defaultS, bool qtRender) :
		CGameStateBase(),
				IEventListener(kListenerTypes, ArrayCount(kListenerTypes))
{
	printf("Loading MainState \n");
	sceneManager 		= NULL;
	renderer 			= NULL;
	keyStates 			= 0;
	lastKeyStates 		= 0;
	cursorStrength 		= 0;
	controllerSyncInProgress	 = false;
	firstRender 		= false;
	globalPause 		= false;
	visionDisplayHandle	= 0;
	firstUpdate			= false;
	visionGate			= false;
	postCartReady		= false;
    postCartRemoved		= false;
	defaultScene 		= defaultS;
	quitPosted 			= 0;
	fixedTimeStep 		= 0.016f;
	screen_width_ 		= 1280;
	screen_height_ 		= 720;
	cameraUpdateTimer 	= 0;
	analogRepeatTimer 	= 0;
	frameIndex 			= 0;
	doNextFrame 		= true;
	dispatchSyncPush	= false;
	visionAlgorithm 	= NULL;
	visionMPI			= NULL;
	timeSinceLastRender	= 1;
	isQtRenderer 		= qtRender;
    dispatchCameraConnect 		= false;
    dispatchCameraDisconnect 	= false;
    masterControllerID = -1;

	clock_gettime(CLOCK_MONOTONIC, &previous_render_time_);

	//*********************************************************************
	// Start up singeletons for our system components
	//*********************************************************************
	createSingeltons();

	//*********************************************************************
	// Load information about the last user logged in
	//*********************************************************************
	User* lastUser = UsersManager::getInstance()->getUser(SystemSettings::getLastUser());
	String avatarName = "AvatarItem4";
	String themeName = "UnderWater";
	if( lastUser != null )
	{ themeName = lastUser->getTheme(); }

	SystemSettings::setTheme(themeName);

	for (int i = 0; i < 10; i++)
	{ frameTimes[i] = 0; }

	printf("End Loading MainState \n");
}

MainState::~MainState()
{
	printf("MainState::~MainState\n");

	TimerManager::getInstance()->release();
	UsersManager::getInstance()->release();
	ContentManager::getInstance()->release();
	StringsManager::getInstance()->release();
	ConnectionManager::getInstance()->release();
	SceneManager::getInstance()->release();
	TextureManager::getInstance()->release();
	getRenderer()->release();
	GradeInfo::clearTable();

	printf("Calling PackageManager::Destroy\n");
	PackageManager::Destroy();
	printf("Post calling PackageManager::Destroy\n");
	printf("End MainState::~MainState\n");
}

/// <summary>
/// Creates the renderer
/// </summary>
void MainState::createRenderer()
{
	if( isQtRenderer )
	{ renderer = QtRenderer::getInstance(); }
	else
	{ renderer = OpenGlRenderer::getInstance(); }
}

/// <summary>
/// Creates all singeltons & managers used by the app
/// </summary>
void MainState::createSingeltons()
{
	printf("MainState::createSingeltons()\n");
	createRenderer();
	PackageManager::Create();
	Packages::Create();
	GradeInfo::fillTable();
	sceneManager = SceneManager::getInstance();
	UsersManager::getInstance();
	TimerManager::getInstance();
	TextureManager::getInstance();
	ConnectionManager::getInstance();
	ContentManager::getInstance();
	StringsManager::getInstance();
}

/// <summary>
/// Returns the renderer used by the app
/// </summary>
BaseRenderer* MainState::getRenderer()
{ return renderer; }

/// <summary>
/// Life cycle
/// </summary>
void MainState::Enter(CStateData* userData)
{
	printf("MainState::Enter\n");
	AssetLibrary::writeFile(Constants::Paths::flagUiLoaded, "loaded");

	//hhhHeee hhhhhhaaaawwwww
	system("rm -rf /LF/Bulk/Data/Local/254");

	int t0 = time(NULL);
	registerEventListeners();
	int t1 = time(NULL);
	printf ("Time elapsed registering EventListeners: %d secs\n", (t1 - t0));

	printf("Loading scene %s\n", defaultScene.str());

	t0 = time(NULL);
	sceneManager->loadScene(defaultScene);
	t1 = time(NULL);
	printf ("Time loading scene: %d secs\n", (t1 - t0));
	printf("Done loading scene %s\n", defaultScene.str());

	if( Constants::System::appID != AppIDVideoWidget )
	{ PackageManager::Instance()->ExitingGame(); }


	globalPause = false;
	clock_gettime(CLOCK_MONOTONIC, &previous_render_time_);

	connectDefaultController();
}

/// <summary>
/// Life cycle
/// </summary>
void MainState::Exit()
{
	printf("MainState::Exit()\n");
	unregisterEventListeners();
}

/// <summary>
/// Life cycle
/// </summary>
void MainState::Suspend()
{
	globalPause = true;
	unregisterEventListeners();
}

/// <summary>
/// Life cycle
/// </summary>
void MainState::Resume()
{
	printf("MainState::Resume()\n");
	registerEventListeners();

	globalPause = false;
	PackageManager::Instance()->ExitingGame();

	clock_gettime(CLOCK_MONOTONIC, &previous_render_time_);

	if( SceneManager::getInstance()->getScene() != null )
	{ SceneManager::getInstance()->getScene()->onReturnToApp(); }

	connectDefaultController();
}

/// <summary>
/// Prints out the frames per second
/// </summary>
void MainState::printFPS()
{
	float span = 0;
	for (int i = 0; i < 10; i++)
	{ span += frameTimes[i]; }

	float spf = span / 10.0f;
	int frameRate = 1;
	if (spf != 0)
	{ frameRate = (int) (1 / spf); }

	if( BaseRenderer::renderCount % 100 == 0 )
	{ printf("FrameRate: %d\n", frameRate); }
}

/// <summary>
/// Required by leap to signal when to clear the device loading video's frame buffer
/// it should be set to black and thus our screen darken should kick in
/// </summary>
void MainState::writeUIReadyFlag()
{
	printf("Writing UI Flag to %s\n", Constants::Paths::flagUiReady.str());
	AssetLibrary::writeFile(Constants::Paths::flagUiReady,"ready");
	firstRender = true;
}

/// <summary>
/// Main Game Loop
/// </summary>
void MainState::Update(CGameStateHandler* sh)
{
	if( !firstUpdate )
	{
		printf("First MainState::Update()\n");
		firstUpdate = true;
	}

	//Thread collapse
	if( quitPosted > 0 )
	{
		quitPosted--;
		if( quitPosted <= 0 )
		{
			printf("Quit Was posted, exit out\n");
			appInstance->DoExit();
			return;
		}
	}

	//Thread collapse
	if( postCartReady )
	{
		if( SceneManager::getInstance()->getScene() != null )
		{ SceneManager::getInstance()->getScene()->onCartridgeReady(); }
		postCartReady = false;
	}

	//Thread collapse
	if( postCartRemoved )
	{
		if( SceneManager::getInstance()->getScene() != null )
		{ SceneManager::getInstance()->getScene()->onCartridgeRemoved(); }
		postCartRemoved = false;
	}

	double dt;
	timespec currentTime;
	clock_gettime(CLOCK_MONOTONIC, &currentTime);

	Scene* currentScene = sceneManager->getScene();
	if (firstRender == false && currentScene != null )
	{
		previous_render_time_ = currentTime;
	}
	else
	{
		dt = currentTime.tv_sec - previous_render_time_.tv_sec;
		dt += (currentTime.tv_nsec - previous_render_time_.tv_nsec) / Constants::Time::kNanoSecondsInASecond;
		dt *= (double) SystemSettings::getTimeModifier();

		//*****************************************************************
		// Frames per second counter
		//*****************************************************************
		frameIndex = (frameIndex + 1) % 10;
		frameTimes[frameIndex] = dt;

		if( SceneManager::getInstance()->getScene() == NULL )
		{ ThrowException::exception("Current Scene is NULL \n"); }

		//*****************************************************************
		// Collect Input
		//*****************************************************************
		doInputUpdate(dt);

		//*****************************************************************
		// Respect global pause bool, we still need to render on pause
		//*****************************************************************
		if (!globalPause)
		{
			if (doNextFrame)
			{
				if (SystemSettings::getFrameStep())
				{
					doNextFrame = false;
					dt = fixedTimeStep;
				}


				//Watch out for Game Engine pit of despair computer science problem
				if( dt > 1 )
					dt = 0.1f;
				sceneManager->getScene()->update(dt);

			}
		}

		TimerManager::getInstance()->update(dt);
		ConnectionManager::getInstance()->update(dt);
		ContentManager::getInstance()->update(dt);

		//*****************************************************************
		// Due to the vsync / tearing bugs and the fact we can't set
		// vsync, if we are above 60 frames per second, we are basically
		// swapping a back buffers whose contents remain the same, as such
		// we shouldn't call a render chain until 1/60th of a second has
		// passed
		//*****************************************************************
		timeSinceLastRender += dt;
		if( timeSinceLastRender > 1.0f/60.0f)
		{
			timeSinceLastRender = 0;
			renderer->render(sceneManager->getScene());
			sceneManager->getScene()->postRender();
		}

		previous_render_time_ = currentTime;
	}

	//printFPS();
}

//*****************************************************************
// Sends events through the scene graph
//*****************************************************************
void MainState::dispatchEvent(Event *event)
{
	if (event != NULL)
	{
		Scene* currentScene = sceneManager->getScene();

		if (event->typeOf(ButtonEvent::type()))
		{ currentScene->onButtonEvent((ButtonEvent *) event); }
		else if (event->typeOf(AccelerationEvent::type()))
		{ currentScene->onAccelerationEvent((AccelerationEvent *) event); }
		else if (event->typeOf(AnalogStickEvent::type()))
		{ currentScene->onAnalogStickEvent((AnalogStickEvent *) event); }
	}
}

//*****************************************************************
// Process button releases
//*****************************************************************
void MainState::dispatchButtonReleases(int keystate, int buttonMask)
{
	if ((keystate & buttonMask) == buttonMask)
	{ dispatchEvent(TRelease<ButtonEvent>(new ButtonEvent(buttonMask, ButtonActionRelease))); }
}

void MainState::dispatchButtonPress(int keystate, int buttonMask)
{
	if ((keystate & buttonMask) == buttonMask)
	{ dispatchEvent(TRelease<ButtonEvent>(new ButtonEvent(buttonMask, ButtonActionPress))); }
}

void MainState::turnControllerLEDOff( HWController* controller)
{
	return;

	if (controller && controller->IsConnected())
	{ controller->SetLEDColor(kHWControllerLEDOff); }
}


void MainState::turnControllerLEDOn(HWController* controller)
{
	return;

	if (controller && controller->IsConnected())
	{
		printf("Changing color for controller: %d\n", controller->GetID());
		HWControllerLEDColorMask colors = controller->GetAvailableLEDColors();
		//Scan for a valid color
		for( int i = 0; i<5; i++ )
		{
			HWControllerRGBLEDColor color = U8(1<<i);
			if (colors & color)
			{
				controller->SetLEDColor(color);
				break;
			}
		}
	}
}


void MainState::trackWand(HWController* controller)
{
	return;

	if (controller!= null && controller->IsConnected())
	{
		if (controller->StartTracking(controller->GetLEDColor()) != kNoErr)
		{ printf("Failed to start tracking color\n"); }
		else
		{ printf("Starting to track controllerID = %d\n", (int)controller->GetID() ); }
	}
}

//*****************************************************************
// Starts the vision MPI
//*****************************************************************
void MainState::startVision()
{
	ThrowException::unsupportedException("We do not support vision yet! This should never be called\n");

	if( visionGate )
		return;

	visionGate = true;
	printf("STARTING VISION\n");

	//*****************************************************************
	// Clean up old vision
	//*****************************************************************
	if( visionMPI != null )
		delete visionMPI;

	visionMPI = new VNVisionMPI();

	//*****************************************************************
	//  Create vision display / surface if it isn't available
	//*****************************************************************
	if( visionDisplayHandle == 0 )
	{
		printf("Creating Offscreen Texture Buffer for wand\n");
		const tDisplayScreenStats* myStats = displayManager.GetScreenStats(0);
		visionDisplayHandle 			= displayManager.CreateHandle(240, 320, kPixelFormatYUYV422, NULL);
		videoDisplaySurface.width 		= displayManager.GetWidth(visionDisplayHandle);
		videoDisplaySurface.pitch 		= displayManager.GetPitch(visionDisplayHandle);
		videoDisplaySurface.height 		= displayManager.GetHeight(visionDisplayHandle);
		videoDisplaySurface.buffer 		= displayManager.GetBuffer(visionDisplayHandle);
		videoDisplaySurface.format 		= displayManager.GetPixelFormat(visionDisplayHandle);
		printf("videoDisplaySurface_ is [%d x %d]\n", (int)videoDisplaySurface.width, (int)videoDisplaySurface.height);
	}

	//*****************************************************************
	// Create algorithm to use
	//*****************************************************************
	if( visionAlgorithm == NULL )
	{
		VNInputParameters params;
		params.push_back(std::pair<CString, float>("VNWTAreaToStartScaling", 1100.f));
		params.push_back(std::pair<CString, float>("VNWTMinPercentToScale", 0.25f));
		params.push_back(std::pair<CString, float>("VNWTMinWandArea", 45.f));
		visionAlgorithm =  new VNWandTracker(&params);
		visionMPI->SetAlgorithm(visionAlgorithm);
	}

	//*****************************************************************
	// Call Start
	//*****************************************************************
	tErrType error =  visionMPI->Start(&videoDisplaySurface, false);
	if (error == kVNVideoCaptureFailed)
	{ printf("Error starting visionMPI: kVNVideoCaptureFailed\n"); }
	else if (error == kVNCameraDoesNotSupportRequiredVisionFormat)
	{ printf("Error starting visionMPI: kVNCameraDoesNotSupportRequiredVisionFormat\n"); }
	else if (error == kVNVideoSurfaceNotOfCorrectSizeForVisionCapture)
	{ printf("Error starting visionMPI: kVNVideoSurfaceNotOfCorrectSizeForVisionCapture\n"); }
}

//*****************************************************************
// Collects input for dispatching
//*****************************************************************
void MainState::doInputUpdate(float dt)
{
	if( dispatchCameraConnect )
	{
		if( SceneManager::getInstance()->getScene() != null )
		{ SceneManager::getInstance()->getScene()->onCameraPluggedIn(); }

		dispatchCameraConnect = false;
	}

	if( dispatchCameraDisconnect )
	{
		if( SceneManager::getInstance()->getScene() != null )
		{ SceneManager::getInstance()->getScene()->onCameraUnplugged(); }
		dispatchCameraDisconnect = false;
	}

	if( dispatchSyncPush )
	{
		if( !controllerSyncInProgress )
		{
			controllerSyncInProgress = true;
			tErrType error = controllerMpi.EnableControllerSync(true);
			if( SceneManager::getInstance()->getScene() != null )
			{ SceneManager::getInstance()->getScene()->onSyncPushed(); }
		}

		dispatchSyncPush = false;
	}

	//******************************************************************************************
	// Key presses
	//******************************************************************************************
	if (keyStates != lastKeyStates)	// && (db < 1.0f/12.0f))
	{
		int same = keyStates & lastKeyStates;
		int presses = keyStates - same;
		int releases = (~keyStates) & lastKeyStates;
		for (int i = 0; i < 16; i++)
		{
			dispatchButtonPress(presses, g_Buttons[i]);
			dispatchButtonReleases(releases, g_Buttons[i]);
		}
	}

	lastKeyStates = keyStates;

	//******************************************************************************************
	//Joy stick
	//******************************************************************************************
	analogRepeatTimer -= (float) dt;
	if (!SceneManager::getInstance()->getScene()->isWandEnabled() && fabsf(cursorStrength) > 0.0f)
	{
		if (analogRepeatTimer > 0)
		{ return; }

		// Difference in directions
		float tolerance = 0.2f;
		if (((float) fabs(cursor.x) > tolerance) || ((float) fabs(cursor.y) > tolerance))
		{
			analogRepeatTimer = 0.25f;
			TRelease<AnalogStickEvent> sevent(new AnalogStickEvent());
			sevent->analogStick = cursor;
			dispatchEvent(sevent);
		}
	}
}

/// <summary>
/// Wrapper call to do a clean app shut down
/// </summary>
void MainState::quit(QVariantMap response)
{
	if ( quitPosted <= 0 )
	{
		printf("MainState::quit(QVariantMap response)\n");
		appInstance->setReturnData(response);
		quitPosted = 10;
	}
}

/// <summary>
/// Sub delegation function that processes input if it applies to any system
/// subsystem. else return false to signify the message did not apply to controller
/// </summary>
bool MainState::processSystemMessage(const IEventMessage &msgIn)
{
	if (msgIn.GetEventType() == kButtonStateChanged )
	{
		const LeapFrog::Brio::CButtonMessage & msg = reinterpret_cast<const LeapFrog::Brio::CButtonMessage&>(msgIn);
		tButtonData2 buttonData = msg.GetButtonState2();

		//This is really a way to deal with threading, we should probably post ALL
		//messages into hard typed Class queue and process on ::Update as opposed to dispatching
		// directly from the input thread
		if(buttonData.buttonTransition & kButtonSync)
		{
			printf("kButtonSync\n");
			dispatchSyncPush = true;
		}

		if( buttonData.buttonTransition & kButtonEscape )
		{
			printf("WARNING SYSTEM IS BEING POWERED OFF\n");
		}

		return true;
	}

	if (msgIn.GetEventType() == kCaptureTimeOutEvent )
	{
		printf("kCaptureTimeOutEvent\n");
		return true;
	}

	if( msgIn.GetEventType() == kUSBDeviceStateChange )
	{
		printf("MainState:: kUSBDeviceStateChange\n");
		const CUSBDeviceMessage& msg = reinterpret_cast<const CUSBDeviceMessage&>(msgIn);
		tUSBDeviceData deviceData = msg.GetUSBDeviceState();

		if ( deviceData.USBDeviceState & kUSBDeviceConnected == kUSBDeviceConnected )
		{
		    dispatchCameraConnect 		= true;
		}
		else
		{
			dispatchCameraDisconnect 		= true;
		}

		return true;
	}

	return false;
}

/// <summary>
/// Sub delegation function that processes input if it applies to the controller
/// subsystem. else return false to signify the message did not apply to controller
/// </summary>
bool MainState::processCatridgeMessage(const IEventMessage &msgIn)
{
	//****************************************************************************************************************
	// Listening in on cartridge events
	//****************************************************************************************************************
	if (msgIn.GetEventType() == kCartridgeStateChanged )
	{
		printf("kCartridgeStateChanged\n");
		const CCartridgeMessage& msg = reinterpret_cast<const CCartridgeMessage&>(msgIn);

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_INSERTED )
		{
			printf("CARTRIDGE STATE: CARTRIDGE_STATE_INSERTED\n");
			//We really don't care about this really, just when it becomes ready
		}

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_DRIVER_READY )
		{ printf("CARTRIDGE STATE: CARTRIDGE_STATE_DRIVER_READY\n"); }

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_READY )
		{
			printf("CARTRIDGE STATE: CARTRIDGE_STATE_READY\n");
			postCartReady = true;
		}

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_REMOVED )
		{
			printf("CARTRIDGE STATE: CARTRIDGE_STATE_REMOVED\n");
			postCartRemoved = true;
		}

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_FS_CLEAN )
		{ printf("CARTRIDGE STATE: CARTRIDGE_STATE_FS_CLEAN\n"); }

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_CLEAN )
		{ printf("CARTRIDGE STATE: CARTRIDGE_STATE_CLEAN\n"); }

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_REINSERT )
		{ printf("CARTRIDGE STATE: CARTRIDGE_STATE_REINSERT\n"); }

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_REBOOT )
		{ printf("CARTRIDGE STATE: CARTRIDGE_STATE_REBOOT\n"); }

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_RESTART_APPMANAGER )
		{ printf("CARTRIDGE STATE: CARTRIDGE_STATE_RESTART_APPMANAGER\n"); }

		if ( msg.GetCartridgeState().cartridgeState == CARTRIDGE_STATE_UNKNOWN )
		{ printf("CARTRIDGE STATE: CARTRIDGE_STATE_UNKNOWN\n"); }

		return true;
	}

	return false;
}

/// <summary>
/// Forwarding call into FW MPI
/// </summary>
void MainState::getAllControllers(std::vector<HWController*> & controllers)
{
	controllerMpi.GetAllControllers(controllers);
}

/// <summary>
/// 0 is default controller
/// </summary>
HWController* MainState::getController(int controllerID)
{ return controllerMpi.GetControllerByID(controllerID); }


/// <summary>
/// Sub delegation function that processes input if it applies to the controller
/// subsystem. else return false to signify the message did not apply to controller
/// </summary>
bool MainState::processCameraMessage(const IEventMessage &msgIn)
{
	if (msgIn.GetEventType() == kCaptureTimeOutEvent )
	{
		printf("kCaptureTimeOutEvent\n");
		return true;
	}

	if (msgIn.GetEventType() == kCaptureQuotaHitEvent )
	{
		printf("kCaptureQuotaHitEvent\n");
		return true;
	}

	if (msgIn.GetEventType() == kCaptureStoppedEvent )
	{
		printf("kCaptureStoppedEvent\n");
		return true;
	}

	if (msgIn.GetEventType() == kCameraRemovedEvent )
	{
		printf("kCameraRemovedEvent\n");
		return true;
	}

	if (msgIn.GetEventType() == kCaptureFrameEvent )
	{
		printf("kCaptureFrameEvent\n");
		return true;
	}

	return false;
}


/// <summary>
/// Sub delegation function that processes input if it applies to the controller
/// subsystem. else return false to signify the message did not apply to controller
/// </summary>
bool MainState::processControllerMessage(const IEventMessage &msgIn)
{
	if( msgIn.GetEventType() == kHWControllerSyncFailure )
	{
		controllerSyncInProgress = false;
		const HWControllerEventMessage& msg = reinterpret_cast<const HWControllerEventMessage&>(msgIn);
		HWController *controller = const_cast<HWController*>(msg.GetController());

		if( SceneManager::getInstance()->getScene() != null )
		{ SceneManager::getInstance()->getScene()->onControllerSynced(controller, false); }
	}

	if ( msgIn.GetEventType() == kHWControllerSyncSuccess )
	{
		controllerSyncInProgress = false;
		const HWControllerEventMessage& msg = reinterpret_cast<const HWControllerEventMessage&>(msgIn);
		HWController *controller = const_cast<HWController*>(msg.GetController());

		if( SceneManager::getInstance()->getScene() != null )
		{ SceneManager::getInstance()->getScene()->onControllerSynced(controller, true); }
	}

	//*********************************************************************************************************
	// Listening in on Controller events
	//*********************************************************************************************************
	if (msgIn.GetEventType() == kHWControllerModeChanged )
	{
		printf("kHWControllerModeChanged\n");

		const HWControllerEventMessage& msg = reinterpret_cast<const HWControllerEventMessage&>(msgIn);
		HWController *controller = const_cast<HWController*>(msg.GetController());

		//Do not process the wand messages for the first version of this application.
		/*
		if (controller->GetCurrentMode() == kHWControllerWandMode)
		{
			turnControllerLEDOn(controller);
			trackWand(controller);
			if( SceneManager::getInstance()->getCurrentScene() != null )
			{ SceneManager::getInstance()->getCurrentScene()->onWandTurnedOn(controller); }
		}
		else
		{
			turnControllerLEDOff(controller);
			if( SceneManager::getInstance()->getCurrentScene() != null )
			{  SceneManager::getInstance()->getCurrentScene()->onWandTurnedOff(controller); }
		}
		return true;
		*/
	}

	if (msgIn.GetEventType() == kHWControllerLowBattery )
	{
		printf("kHWControllerLowBattery\n");
		return true;
	}


	if (msgIn.GetEventType() == kHWControllerDisconnected )
	{
		printf("kHWControllerDisconnected\n");
		const HWControllerEventMessage& msg = reinterpret_cast<const HWControllerEventMessage&>(msgIn);
		HWController *controller = const_cast<HWController*>(msg.GetController());


		if( masterControllerID >= 0  )
		{
			std::vector<HWController*> controllers;
			controllerMpi.GetAllControllers(controllers);
			if ( controllers.size() == 0 )
			{
				printf("There are no more controllers! As such there is no master!");
			}
			else if ( masterControllerID == controller->GetID() )
			{
				printf("Controller disconnected was the master, we need a new one\n");
				masterControllerID = -1;
				for ( int i = 0; i<(int)controllers.size(); i++ )
				{
					HWController* newController = controllers[i];
					if( newController->IsConnected() )
					{
						masterControllerID = newController->GetID();
						printf("Master controller changed to [%d]\n", masterControllerID);
					}
				}
			}
		}
		else
		{
			printf("A CONTROLLER DISCONNECTED without a master set?!? how can this be?!\n");
		}


		if( SceneManager::getInstance()->getScene() != null )
		{ SceneManager::getInstance()->getScene()->onControllerDisconnected(controller); }

		return true;
	}


	if (msgIn.GetEventType() == kHWControllerConnected )
	{
		printf("kHWControllerConnected\n");
		const HWControllerEventMessage& msg = reinterpret_cast<const HWControllerEventMessage&>(msgIn);
		HWController *controller = const_cast<HWController*>(msg.GetController());

		if( masterControllerID == -1 )
		{ masterControllerID = controller->GetID(); }
		else
		{
			//new controller coming in

		}

		if( controller->GetCurrentMode() == kHWControllerWandMode )
		{
			turnControllerLEDOn(controller);
			trackWand(controller);

			if( SceneManager::getInstance()->getScene() != null )
			{
				SceneManager::getInstance()->getScene()->onControllerConnected(controller);
				SceneManager::getInstance()->getScene()->onWandTurnedOn(controller);
			}
		}
		else
		{
			turnControllerLEDOff(controller);
			if( SceneManager::getInstance()->getScene() != null )
			{
				SceneManager::getInstance()->getScene()->onControllerConnected(controller);
				SceneManager::getInstance()->getScene()->onWandTurnedOff(controller);
			}
		}

		return true;
	}



	if (msgIn.GetEventType() == kHWControllerButtonStateChanged )
	{
		const HWControllerEventMessage& aclmsg = reinterpret_cast<const HWControllerEventMessage&>(msgIn);
		const HWController *controller = aclmsg.GetController();
		tButtonData2 buttonData = controller->GetButtonData();

		if ( masterControllerID == controller->GetID() )
		{
			keyStates = buttonData.buttonState; //keystate
		}
		return true;
	}


	if (msgIn.GetEventType() == kHWControllerAnalogStickDataChanged)
	{
		const HWControllerEventMessage& aclmsg = reinterpret_cast<const HWControllerEventMessage&>(msgIn);
		const HWController *controller = aclmsg.GetController();

		if ( masterControllerID == controller->GetID() )
		{
			tHWAnalogStickData acldata = controller->GetAnalogStickData();
			int x = roundf(((acldata.x / 0.72f) * 100) / 10);
			int y = roundf(((acldata.y / 0.72f) * 100) / 10);
			if (x > 10)
			{ x = 10; }
			if (y > 10)
			{ y = 10; }

			cursor = Vec2f((float) x / 10.0f, (float) y / 10.0f);
			cursorStrength = roundf(fabsf(hypotf(x, y))) / 10.0f;
			if (cursorStrength > 1.0f)
			{ cursorStrength = 1.0f; }
		}

		return true;
	}

	return false;
}

/// <summary>
/// IEventListener* protocol, WARNING: SOME EVENTS CAN COME IN THREADED!!!!
/// The right architecture here is to wrap each Leap message, into a queue that
/// gets processed on the update thread. No feature in this system needs the fidelity
/// of threaded real time, so 60 frames per second should be enough speed
/// </summary>
tEventStatus MainState::Notify(const IEventMessage &msgIn)
{
	//****************************************************************************************************************
	// Listening in on System events
	//****************************************************************************************************************
	if( processSystemMessage( msgIn ) )
	{ return kEventStatusOK; }

	//****************************************************************************************************************
	// Listening in on Cartridge events
	//****************************************************************************************************************
	if( processCatridgeMessage( msgIn ) )
	{ return kEventStatusOK; }

	//****************************************************************************************************************
	// Listening in on Camera events
	//****************************************************************************************************************
	if( processCameraMessage( msgIn ) )
	{ return kEventStatusOK; }

	//****************************************************************************************************************
	// Listening in on Controller events
	//****************************************************************************************************************
	if( processControllerMessage( msgIn ) )
	{ return kEventStatusOK; }

	return kEventStatusOK;
}

/// <summary>
/// Registers the listeners into Leap's MPIs
/// </summary>
void MainState::registerEventListeners(void)
{
	printf("Registering event listeners\n");
	event_mpi.RegisterEventListener(this);
	printf("System initialized\n");
}

/// <summary>
/// Clean up
/// </summary>
void MainState::unregisterEventListeners(void)
{
	printf("MainState::unregisterEventListeners\n");
	event_mpi.UnregisterEventListener(this);
}

void MainState::connectDefaultController()
{
	// This is in case the controller came pre connected and we wont get
	// a connected message, this is for cases when we arent listening
	// but leaps system connected first / race condition
	std::vector<HWController*> controllers;
	controllerMpi.GetAllControllers(controllers);
	for( int i = 0; i<(int)controllers.size(); i++ )
	{
		HWController* controller = controllers[i];
		if( controller->IsConnected())
		{
			masterControllerID = controller->GetID();
			break;
		}
	}
}

}

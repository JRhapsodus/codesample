#ifndef GRAPHICS_SPRITE_INCLUDE_SPRITEAPP_H_
#define GRAPHICS_SPRITE_INCLUDE_SPRITEAPP_H_

#include <App.h>
#include <AppInterface.h>
#include <AppManager.h>
#include <LTM.h>
#include <CSystemData.h>
#include <KernelMPI.h>
#include <ButtonMPI.h>
#include <DisplayMPI.h>
#include <iostream>
#include <stdio.h>

#include "SIEngine/Include/SIBase.h"
#include "SIEngine/Include/SIRendering.h"
#include "SIEngine/Core/Scene.h"

#include "Glasgow/MainState.h"

namespace Glasgow
{
class MainState;

class GlasgowApp : public App
{
	Q_OBJECT
public:
	GlasgowApp(String defaultScene_, bool useQtRenderer);
    virtual ~GlasgowApp();

    ///<summary>
    /// Life cycle of LeapFrog appserver App class
    ///</summary>
    virtual void Enter();

    ///<summary>
    /// Life cycle of LeapFrog appserver App class
    ///</summary>
    virtual void Exit();

    ///<summary>
    /// Life cycle of LeapFrog appserver App class
    ///</summary>
    virtual void Suspend();

    ///<summary>
    /// Life cycle of LeapFrog appserver App class
    ///</summary>
    virtual void Resume();

    ///<summary>
    /// Logs all parameters inside the appData
    ///</summary>
    void debugLogLaunchParameters();

    ///<summary>
    /// Logs all parameters inside the returnData
    ///</summary>
    void debugLogReturnData();

    ///<summary>
    /// Reference to the main renderer
    ///</summary>
    static BaseRenderer* getRenderer();

    ///<summary>
    /// Reference to the main app instance
    ///</summary>
    static GlasgowApp* 	appInstance;

    ///<summary>
    /// Reference to the running state of the app
    ///</summary>
    static MainState*  	mainState;

    ///<summary>
    /// Forwarding call to get the QT App's return data struct for ::Resume
    ///</summary>
    QVariantMap getLaunchParameters();

    ///<summary>
    /// Forwarding call to set the QT App's return data
    ///</summary>
    void setReturnData( QVariantMap returnMap );
public slots:

	///<summary>
	/// Triggered via the qt timer
	///</summary>
 	void Update();

 	void checkLeapFrogServers();
 	virtual void HandleLfConnectivityUpdate(bool isConnected);

protected:
    CGameStateHandler* state_handler_;        ///< manages the state of the app
    String defaultScene;

    //Held due to construction vrs. lazy init in ::Enter
    bool isQtRender;

private:
    //Global state
    bool suspended;
    QTimer* updateTimer;
    GlasgowApp(const GlasgowApp&);
    GlasgowApp& operator =(const GlasgowApp&);
};

}

#endif  // GRAPHICS_SPRITE_INCLUDE_SPRITEAPP_H_


#include "CameraFeedObject.h"
#include "SIEngine/Include/SIRendering.h"
#include "SIEngine/Include/SIMaterials.h"

namespace Glasgow
{


CameraFeedObject::CameraFeedObject() : super()
{
	int camWidth = 640;
	int camHeight = 480;
	displayHandle_ = displayManager_.CreateHandle(camHeight, camWidth, kPixelFormatRGB888, NULL);

	/// Used to create the surface for video capture
	videoDisplaySurface_.width 	= displayManager_.GetWidth(displayHandle_);
	videoDisplaySurface_.height = displayManager_.GetHeight(displayHandle_);
	videoDisplaySurface_.pitch 	= displayManager_.GetPitch(displayHandle_);
	videoDisplaySurface_.buffer = displayManager_.GetBuffer(displayHandle_);
	videoDisplaySurface_.format = displayManager_.GetPixelFormat(displayHandle_);

	printf("Video Display [%d %d]\n", videoDisplaySurface_.width, videoDisplaySurface_.height );

	/// We are using the buffer of the videoDisplaySurface to create our RawImage
	rawImage = new RawImage( videoDisplaySurface_.buffer, camWidth, camHeight, kPixelFormatRGB888 );

	/// The mutable texture addRef's this image, which uses the Image's buffer to update the glSubTexImage
	mutableTexture = TextureManager::getInstance()->createOffScreenTexture( rawImage, "CameraFeedOffScreen" );

	TRelease<SpriteData> data( new SpriteData("CameraFeedOffScreen") );
	setSpriteData(data);
	setSize(Size(camWidth, camHeight));

	//rawImage is mantained by mutable Texture
	rawImage->release();
	SAFE_RELEASE(material);
	material = new VertexLitMaterial(this);

	videoCapture_ = 0;
	retryTimer = 0;
}

bool CameraFeedObject::startFeed()
{
	printf("CameraFeedObject::startFeed()\n");
	videoCapture_ = cameraMPI_.StartVideoCapture(&videoDisplaySurface_, null, "", 0, false);

	if( videoCapture_ != 0 )
	{ displayManager_.UnRegister(displayHandle_, kDisplayScreenAllScreens); }

	return (videoCapture_ != 0);
}

void CameraFeedObject::stopFeed()
{
	printf("CameraFeedObject::stopFeed()\n");
	if(videoCapture_ != 0)
	{ cameraMPI_.StopVideoCapture(videoCapture_); }
}

void CameraFeedObject::render()
{
	super::render();
}

void CameraFeedObject::update(float dt)
{
	super::update(dt);
	retryTimer -= dt;

	if( videoCapture_ != 0 && isVisibleHierarchy() )
	{
		UV UV_0 = UV(0, 0.5f*480.0f/512.0f);
		UV UV_1 = UV(0, 0);
		UV UV_2 = UV(0.5f*640.0f/1024.0f, 0);
		UV UV_3 = UV(0.5f*640.0f/1024.0f, 0.5f*480.0f/512.0f);

		vertices[0].uv = UV_0;
		vertices[1].uv = UV_1;
		vertices[2].uv = UV_2;

		vertices[3].uv = UV_0;
		vertices[4].uv = UV_2;
		vertices[5].uv = UV_3;

		if( mutableTexture != NULL )
		{
			if( OpenGlRenderer::renderCount % 4 == 0 )
			{ mutableTexture->updatePixels(); }
		}
	}
}

CameraFeedObject::~CameraFeedObject()
{
	printf("CameraFeedObject::~CameraFeedObject()\n");
	stopFeed();
	//TextureManager::getInstance()->unloadTexture("CameraFeedOffScreen");
	SAFE_RELEASE(mutableTexture);
	printf("displayManager_.DestroyHandle\n");
	displayManager_.DestroyHandle(displayHandle_, false);
	printf("End CameraFeedObject::~CameraFeedObject()\n");
}


} /* namespace Glasgow */

#include "VideoObject.h"
#include "SIEngine/Include/SIRendering.h"
#include "SIEngine/Include/SIMaterials.h"

namespace Glasgow
{

VideoObject::VideoObject(String videoPath_) : super()
{
	videoPath = videoPath_;

	//get info about video
	videoHandle = videomgr.StartVideo(videoPath.str());
	videomgr.GetVideoInfo(videoHandle, &videoInfo);
	videomgr.StopVideo(videoHandle);

	printf( "----------------------------\n");
	printf( "Creating buffer [ %d x %d ] \n", videoInfo.width, videoInfo.height );
	printf( "FPS %d \n", videoInfo.fps );
	printf( "----------------------------\n");

	displayHandle = displayManager_.CreateHandle(videoInfo.height, videoInfo.width, kPixelFormatARGB8888, NULL);

	/// Create off screen texture for decoding video
	videoDisplaySurface_.width 	= displayManager_.GetWidth(displayHandle);
	videoDisplaySurface_.height = displayManager_.GetHeight(displayHandle);
	videoDisplaySurface_.pitch 	= displayManager_.GetPitch(displayHandle);
	videoDisplaySurface_.buffer = displayManager_.GetBuffer(displayHandle);
	videoDisplaySurface_.format = displayManager_.GetPixelFormat(displayHandle);

	/// We are using the buffer of the videoDisplaySurface to create our RawImage
	rawImage = new RawImage( videoDisplaySurface_.buffer, videoInfo.width, videoInfo.height, kPixelFormatARGB8888 );

	/// The mutable texture addRef's this image, which uses the Image's buffer to update the glSubTexImage
	mutableTexture = TextureManager::getInstance()->createOffScreenTexture( rawImage, "VideoOffScreen" );

	TRelease<SpriteData> data( new SpriteData("VideoOffScreen") );
	setSpriteData(data);
	setSize(Size(videoInfo.width, videoInfo.height));
	videoHandle = videomgr.StartVideo(videoPath.str(), videoPath.str(), &videoDisplaySurface_ );

	SAFE_RELEASE(material);
	material = new VertexLitMaterial(this);
}

tEventStatus VideoObject::Notify(const IEventMessage& msg)
{
  return kEventStatusOK;
}

void VideoObject::render()
{
	super::render();
}

void VideoObject::update(float dt)
{
	super::update(dt);
	if( isVisibleHierarchy() )
	{
		UV UV_0 = UV(0, 1);
		UV UV_1 = UV(0, 0);
		UV UV_2 = UV(1, 0);
		UV UV_3 = UV(1, 1);

		vertices[0].uv = UV_0;
		vertices[1].uv = UV_1;
		vertices[2].uv = UV_2;

		vertices[3].uv = UV_0;
		vertices[4].uv = UV_2;
		vertices[5].uv = UV_3;

		if( mutableTexture != NULL && videomgr.IsVideoPlaying(videoHandle))
		{
			mutableTexture->updatePixels();
		}
	}

}

VideoObject::~VideoObject()
{
	SAFE_RELEASE(mutableTexture);
	SAFE_RELEASE(rawImage);
}

} /* namespace Glasgow */

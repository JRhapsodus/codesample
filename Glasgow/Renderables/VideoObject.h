#ifndef VIDEOOBJECT_H_
#define VIDEOOBJECT_H_

#include "SIEngine/Include/SIObjects.h"
#include "SIEngine/Include/SIGeometry.h"
#include "SIEngine/Resources/RawImage.h"
#include "SIEngine/Rendering/MutableTexture.h"
#include <CameraMPI.h>
#include <DisplayMPI.h>
#include <DisplayTypes.h>
#include <VideoMPI.h>
#include <VideoTypes.h>

LF_USING_BRIO_NAMESPACE()

namespace Glasgow
{

class VideoObject : public SpriteObject
{
	CLASSEXTENDS(VideoObject, SpriteObject);
	ADD_TO_CLASS_MAP;
public:
	VideoObject(String videoPath_);
	virtual ~VideoObject();

	/// <summary>
	/// In charge of updating the pixels of its image and texture
	/// /<summary>
	virtual void update(float dt);
	virtual void render();
	/// <summary>
	/// Recommended by CameraMPI
	/// /<summary>
	tEventStatus Notify(const IEventMessage& msg);
	void swizzleColors();
private:

	tVideoInfo		videoInfo;
	String 			videoPath;
	CVideoMPI 		videomgr;
	CDisplayMPI 	displayManager_;
	MutableTexture* mutableTexture;
	RawImage*		rawImage;
	tDisplayHandle  displayHandle;
	tVideoHndl 		videoHandle;
	tVideoSurf 		videoDisplaySurface_;
};

}

#endif /* CAMERAFEEDOBJECT_H_ */

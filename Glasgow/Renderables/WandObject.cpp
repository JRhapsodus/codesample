#include "Glasgow/MainState.h"
#include "WandObject.h"
#include "SIEngine/Geometry/Matrix.h"
#include "SIEngine/Managers/SystemSettings.h"
#include "SIEngine/Include/SIMaterials.h"
#include "Glasgow/Scenes/Child/ChildScene.h"
#include "Glasgow/Particles/Particle.h"
#include "SIEngine/Rendering/TextureManager.h"

using namespace SIUtils;
using namespace SICore;
using namespace SIGeometry;

namespace Glasgow
{


WandObject::WandObject(QtSpriteObject* navRight_, QtSpriteObject* navLeft_, QtSpriteObject* navAction_, QtSpriteObject* navWand_)
{
	//************************************************************************
	//Disable glasgow's auto shut off feature
	//************************************************************************
	//CAppManager::Instance()->EnableInactivity(false);
	snapToObject = NULL;
	lastWandScreenPosition = Vec3f(-5000, -5000, 0);
	setPosition(Vec3f(-5000, -5000, 0));
	name = "Wand";
	sampleCount = 0;
	snapLockOut = -1;
	wandLockOut = 0;
	spawnTimer = 0.1f;
	sustainCounter = -1;

	navWand = navWand_;
	navAction = navAction_;
	navRight = navRight_;
	navLeft = navLeft_;
	addChild(navWand);
	addChild(navRight);
	addChild(navLeft);
	addChild(navAction);

	navAction_->release();

	navRight->fadeTo(0, 0.01f);
	navLeft->fadeTo(0, 0.01f);
	navAction->fadeTo(0, 0.01f);

	for( int i = 0; i<SAMPLES_TO_TAKE;i++)
	{ samples[i] = Vec3f(-5000, -5000, 0); }

	//controllerMPI = new HWControllerMPI();
	setVisible(false);
}


WandObject::WandObject(SpriteObject* navRight_, SpriteObject* navLeft_, SpriteObject* navAction_, SpriteObject* navWand_) : super()
{
	//************************************************************************
	//Disable glasgow's auto shut off feature
	//************************************************************************
	//CAppManager::Instance()->EnableInactivity(false);
	snapToObject = NULL;
	lastWandScreenPosition = Vec3f(-5000, -5000, 0);
	setPosition(Vec3f(-5000, -5000, 0));
	name = "Wand";
	sampleCount = 0;
	snapLockOut = -1;
	wandLockOut = 0;
	spawnTimer = 0.1f;
	sustainCounter = -1;

	navWand = navWand_;
	navAction = navAction_;
	navRight = navRight_;
	navLeft = navLeft_;
	addChild(navWand);
	addChild(navRight);
	addChild(navLeft);
	addChild(navAction);
	navAction_->release();


	navRight->fadeTo(0, 0.01f);
	navLeft->fadeTo(0, 0.01f);
	navAction->fadeTo(0, 0.01f);

	for( int i = 0; i<SAMPLES_TO_TAKE;i++)
	{ samples[i] = Vec3f(-5000, -5000, 0); }

	setVisible(false);
}

WandObject::~WandObject()
{
	printf("WandObject::~WandObject()\n");
}

//************************************************************************
// Returns the wand's localposition in its parent as translated by vision
//************************************************************************
Vec3f WandObject::getLocalPos(Vec3f screenPoint)
{
	//************************************************************************
	// Project screen point into world point on camera's zNear plane
	//************************************************************************
	if( ((CameraObject*)parent)->isOrthographic )
	{
		return Vec3f( screenPoint.x * 1280.0f/2.0f, screenPoint.y * 720.0f/2.0f, 10);
	}
	else
	{
		Vec3f worldPosition = Matrix::unProject(screenPoint, ((CameraObject*)parent)->getViewMatrix(), ((CameraObject*)parent)->projection, Rect(0,0,1280,720) );
		return ((CameraObject*)parent)->transform.getWorldToLocal().multiplyPoint(worldPosition);
	}
}

//************************************************************************
// Overriding to exclude children from the bounding box region, this will
// let us flock children through the space but keep our core 'wand' sprite
// as the primary hit test box
//************************************************************************
Box3f WandObject::getWorldBoundingBox()
{
	//Our world box
	Matrix localToWorld = transform.getLocalToWorld();
	Box3f box = navWand->getLocalBoundingBox();
	if( !box.isEmpty())
		box = Box3f::transform(box, localToWorld);

	return box;
}

//************************************************************************
// Returns the screen position of the wand as translated by the vision MPI
//************************************************************************
Vec3f WandObject::getScreenPosition()
{
	//************************************************************************
	// Get wand screen position (0,0)->[1,1] where opengl is [-1,-1] -> [1,1]
	//************************************************************************
	HWController* controller = GlasgowApp::mainState->getController(0);

	if ( controller != null )
	{
		printf(" WAND Position from Vision [%d, %d]\n", (int)controller->GetLocation().x, (int)controller->GetLocation().y);
		float offX = (controller->GetLocation().x/320.0f)*1280;
		float offY = (controller->GetLocation().y/240.0f)*720;
		Vec3f wandScreenPosition = Vec3f(offX, offY, -1);

		//************************************************************************
		//Convert wand screen space into opengl screen space
		//************************************************************************
		float dx = wandScreenPosition.x/1280.0f;
		float dy = wandScreenPosition.y/720.0f;

		wandScreenPosition.x = -1 + 2*dx;
		wandScreenPosition.y = -1*(-1 + 2*dy);

		return wandScreenPosition;
	}

	return Vec3f::Zero;
}

//************************************************************************
// Sets local position based on the gl clip space screen position
//************************************************************************
void WandObject::setScreenPosition(Vec3f destScreenPosition, float smoothing)
{
	CameraObject* camera = (CameraObject*)parent;
	Vec3f currentScreenPosition = getScreenBoundingBox(camera).getCenter();


	Vec3f moveToScreenSpace = Vec3f::lerp(currentScreenPosition, destScreenPosition, smoothing);
	moveToScreenSpace.z = -0.8f;

	Vec3f localPos = getLocalPos(moveToScreenSpace);
	if( camera->isOrthographic )
	{
		localPos = getLocalPos(destScreenPosition);
		localPos.z = 1;
	}

	transform.setPosition( localPos );
	markChildrenTransformsDirty();

	if ( camera->isOrthographic )
	{
		transform.setScale( Vec3f(1,1,1));
	}
	else
	{
		float oscillate = 0.03f; // + 0.005f * sinf(totalTimeElapsed);
		setScale( Vec3f(oscillate, oscillate, oscillate));
		navAction->setScale( Vec3f::One*0.5f );
		navLeft->setScale( Vec3f::One*0.4f );
		navRight->setScale( Vec3f::One*0.4f );
	}
}

//************************************************************************
// Sets the proper state for the nav icon
//************************************************************************
void WandObject::fadeNavIcon(GameObject* wandEntered)
{
 	if( wandEntered != null )
	{
		bool bnavLeft = false;
		bool bnavRight = false;
		if( wandEntered->name == "RotateLeftButton" || wandEntered->name == "SliderLeft"  )
			bnavLeft = true;
		if( wandEntered->name == "RotateRightButton" || wandEntered->name == "SliderRight" )
			bnavRight = true;

		if( bnavLeft )
		{
			navLeft->waitFor(0.25f);
			navRight->waitFor(0.25f);
			navAction->waitFor(0.25f);

			navLeft->cancelActions();
			navLeft->fadeTo(0.7f, 0.25f);
			navAction->cancelActions();
			navAction->fadeTo(0, 0.25f);
			navRight->cancelActions();
			navRight->fadeTo(0, 0.25f);

			fadeTo( 0, 0.15f);
		}
		else if ( bnavRight )
		{
			navLeft->waitFor(0.25f);
			navRight->waitFor(0.25f);
			navAction->waitFor(0.25f);

			navRight->cancelActions();
			navRight->fadeTo(0.7f, 0.25f);
			navAction->cancelActions();
			navAction->fadeTo(0, 0.25f);
			navLeft->cancelActions();
			navLeft->fadeTo(0, 0.25f);

			fadeTo( 0, 0.15f);
		}
		else
		{
			navLeft->waitFor(0.25f);
			navRight->waitFor(0.25f);
			navAction->waitFor(0.25f);

			navAction->cancelActions();
			navAction->fadeTo(0.7f, 0.25f);
			navLeft->cancelActions();
			navLeft->fadeTo(0, 0.25f);
			navRight->cancelActions();
			navRight->fadeTo(0, 0.25f);
		}
	}
	else
	{
		navAction->cancelActions();
		navAction->fadeTo(0, 0.25f);
		navLeft->cancelActions();
		navLeft->fadeTo(0, 0.25f);
		navRight->cancelActions();
		navRight->fadeTo(0, 0.25f);
		cancelActions();
		fadeTo( 1, 0.15f);
	}
}

//************************************************************************
// Sets lock out time
//************************************************************************
void WandObject::setLockOut( float time )
{

	navRight->cancelActions();
	navLeft->cancelActions();
	navAction->cancelActions();
	cancelActions();


	navRight->fadeTo(0, 0.15f);
	navLeft->fadeTo(0, 0.15f);
	navAction->fadeTo(0, 0.15f);
	fadeTo(1, 0.15f);
	wandLockOut = time;
}

float WandObject::getLockOutTimer()
{ return wandLockOut; }

/// <summary>
/// Load from file
/// </summary>
WandObject* WandObject::deSerialize(FILE* fp)
{
	GameObject* action = Scene::deSerializeNext(fp);

	//*****************************************************************************
	// We have two types of wand architectures, one for Qt system, & 1 for 3d
	//*****************************************************************************
	if( action->typeOf(SpriteObject::type()) )
	{
		TRelease<SpriteObject> left((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> right((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> wand((SpriteObject*)Scene::deSerializeNext(fp));
        return new WandObject( right, left, (SpriteObject*)action, wand );
	}
	else
	{
		TRelease<QtSpriteObject> left((QtSpriteObject*)Scene::deSerializeNext(fp));
		TRelease<QtSpriteObject> right((QtSpriteObject*)Scene::deSerializeNext(fp));
		TRelease<QtSpriteObject> wand((QtSpriteObject*)Scene::deSerializeNext(fp));
        return new WandObject( right, left, (QtSpriteObject*)action, wand );
	}
}


static int particleID = 0;

//************************************************************************
// Move the wand around screen, snap to objects
//************************************************************************
void WandObject::update( float dt )
{
	return super::update(dt);
	/*

	wandLockOut -= dt;
	Scene* scene = SceneManager::getInstance()->getCurrentScene();
	if( !scene->isWandEnabled() )
	{
		setVisible(false);
		return;
	}

	if( hasActions() )
	{ return; }

	setVisible(true);
	spawnTimer -= dt;

	CameraObject* camera = (CameraObject*)parent;

	if( spawnTimer < 0 && !camera->isOrthographic )
	{
		TRelease<SpriteData> spriteData( new SpriteData("sparkle_element0012"));
		TRelease<Particle> particle( new Particle( spriteData) );

		particle->lifeSpan = randomFloat(0, 1.5f);
		particle->gravity = Vec3f::Zero;

		float randomX = -1 + randomFloat(0, 3);
		float randomY = -1 + randomFloat(0, 3);
		float randomZ = -1 + randomFloat(0, 3);

		particle->rotationRate = (int) (-500 + randomFloat(0, 1) * 1000);
		particle->velocity = Vec3f(randomX, randomY, randomZ);

		particle->startColor = Color( 211.0f/255.0f, 247.0f/255.0f, 61.0f/255.0f ,1 );
		particle->endColor = Color( 211.0f/255.0f, 247.0f/255.0f, 61.0f/255.0f , 0 );

		particle->setSize( Size( 25, 25 ) );
		particle->setScale( Vec3f::One * 0.05f );
		particle->setMaterial(TRelease<TintMaterial>(new TintMaterial(particle)));
		particle->fadeTo(0, particle->lifeSpan);
		particle->name = String("Particle") + particleID;
		particleID++;

		getParent()->addChild(particle);
		particle->setPosition(getPosition());
		spawnTimer = 0.05f;
	}

	//************************************************************************
	// Subtle pulse
	//************************************************************************
	//setOpacity( 0.85f + 0.15f*sinf(2*totalTimeElapsed));
	static int lockOutAmount = 100;
	float smoothing = SystemSettings::getWandSensitivity();

	if( snapLockOut > 0 )
		snapLockOut--;

	//************************************************************************
	// If we haven't snapped to anything, move to where the wand is
	//************************************************************************
	Vec3f newWandScreenPosition = getScreenPosition();
	sampleCount++;
	samples[sampleCount%SAMPLES_TO_TAKE] = newWandScreenPosition;

	//************************************************************************
	// Use moving average
	//************************************************************************
	if( sampleCount > SAMPLES_TO_TAKE )
	{
		Vec3f average = Vec3f::Zero;
		for( int i = 0; i<SAMPLES_TO_TAKE; i++ )
		{ average = Vec3f(average.x+samples[i].x, average.y+samples[i].y, average.z+samples[i].z); }

		average = Vec3f(average.x/SAMPLES_TO_TAKE, average.y/SAMPLES_TO_TAKE, -1);
		newWandScreenPosition = average;
	}

	//************************************************************************
	// See if we are inside any object
	//************************************************************************
	ObjectArray* snappingObjects = SceneManager::getInstance()->getCurrentScene()->getHotSpots();
	GameObject* overlapping = NULL;

	if( wandLockOut <= 0 )
	{
		for( int i = 0; i<snappingObjects->getSize(); i++ )
		{
			GameObject* snapTo = (GameObject*)snappingObjects->elementAt(i);
			if( snapTo != NULL && snapTo->isVisibleHierarchy())
			{
				Box3f screenBox = snapTo->getScreenBoundingBox((CameraObject*)parent);
				Vec3f centerPoint = getScreenBoundingBox((CameraObject*)parent).getCenter();

				if( screenBox.containsIgnoreDepth(centerPoint))
				{
					overlapping = snapTo;
					break;
				}
			}
		}

		//************************************************************************
		// If we overlap and havent' snapped, lets snap to it
		//************************************************************************
		if ( snapToObject == NULL && overlapping != NULL)
		{
			snapLockOut = lockOutAmount;
			snapToObject = overlapping;
			sustainCounter = 0.21f;
		}
		//************************************************************************
		// We went to another object
		//************************************************************************
		else if ( snapToObject != NULL && overlapping != snapToObject && overlapping != NULL )
		{
			//************************************************************************
			// Exit previous
			//************************************************************************
			snapLockOut = lockOutAmount;
			fadeNavIcon(overlapping);
			SceneManager::getInstance()->getInstance()->getCurrentScene()->onWandEvent(new WandEvent(WandEventLeave, snapToObject));

			//************************************************************************
			// Enter new
			//************************************************************************
			snapToObject = overlapping;
			sustainCounter = 0.21f;
		}
		//************************************************************************
		// If we don't overlap but have snapped, lets unsnap
		//************************************************************************
		else if ( snapToObject != NULL && overlapping == NULL )
		{
			snapLockOut = lockOutAmount;

			SceneManager::getInstance()->getInstance()->getCurrentScene()->onWandEvent(new WandEvent(WandEventLeave, snapToObject));
			snapToObject = NULL;
			fadeNavIcon(NULL);
			sustainCounter = -1;
		}
		else if ( overlapping != NULL && snapToObject == overlapping )
		{
			if( sustainCounter > 0 )
			{
				sustainCounter -= dt;
				if( sustainCounter < 0 )
				{
					SceneManager::getInstance()->getInstance()->getCurrentScene()->onWandEvent(new WandEvent(WandEventEnter, snapToObject));
					fadeNavIcon(overlapping);
				}
			}
		}
	}

	//************************************************************************
	// Now move torwards our new position
	//************************************************************************
	setScreenPosition(newWandScreenPosition, smoothing);
	*/
}


}


#ifndef CAMERAFEEDOBJECT_H_
#define CAMERAFEEDOBJECT_H_

#include "SIEngine/Include/SIObjects.h"
#include "SIEngine/Include/SIGeometry.h"
#include "SIEngine/Resources/RawImage.h"
#include "SIEngine/Rendering/MutableTexture.h"
#include <CameraMPI.h>
#include <DisplayMPI.h>
#include <DisplayTypes.h>

LF_USING_BRIO_NAMESPACE()
namespace Glasgow
{

class CameraFeedObject : public SpriteObject
{
	CLASSEXTENDS(CameraFeedObject, SpriteObject);

public:
	CameraFeedObject();
	virtual ~CameraFeedObject();

	/// <summary>
	/// In charge of updating the pixels of its image and texture
	/// /<summary>
	virtual void update(float dt);

	virtual bool startFeed();

	virtual void stopFeed();

	virtual void render();

	/// <summary>
	/// Recommended by CameraMPI
	/// /<summary>
	void swizzleColors();

private:
	float retryTimer;
	MutableTexture* mutableTexture;
	RawImage*		rawImage;
	CCameraMPI 		cameraMPI_;
	CDisplayMPI 	displayManager_;
	tDisplayHandle 	displayHandle_;
	tVidCapHndl 	videoCapture_;
	tVideoSurf 		videoDisplaySurface_;
};

}

#endif /* CAMERAFEEDOBJECT_H_ */

#ifndef WANDOBJECT_H_
#define WANDOBJECT_H_

#include "SIEngine/Objects/SpriteObject.h"
#include "SIEngine/Objects/QtSpriteObject.h"


using namespace SICore;

#define SAMPLES_TO_TAKE 3

namespace Glasgow
{

class WandObject : public GameObject
{
	CLASSEXTENDS(WandObject, GameObject);
public:
	WandObject(SpriteObject* navRight_, SpriteObject* navLeft_, SpriteObject* navAction_, SpriteObject* navWand_);
	WandObject(QtSpriteObject* navRight_, QtSpriteObject* navLeft_, QtSpriteObject* navAction_, QtSpriteObject* navWand_);
	virtual ~WandObject();

	/// <summary>
	/// Load from file
	/// </summary>
	static WandObject* deSerialize(FILE* fp);

	/// <summary>
	/// Custom bounding box for a complex object
	/// </summary>
	virtual Box3f getWorldBoundingBox();

	/// <summary>
	/// Sets an amount of time where the wand will not trigger input dispatches
	/// </summary>
	void setLockOut( float time );

	/// <summary>
	/// Query for lockout timer
	/// </summary>
	float getLockOutTimer();

	/// <summary> 
	/// Base gameObject override
	/// </summary> 
	virtual void update( float dt );

private:
	void fadeNavIcon(GameObject* snapToObject);

	GameObject* navWand;
	GameObject* navRight;
	GameObject* navLeft;
	GameObject* navAction;

	//HWControllerMPI *controllerMPI;

	float spawnTimer;
	/// <summary> 
	/// Current object 'underneath' wand
	/// </summary> 
	GameObject* snapToObject;

	/// <summary> 
	/// For moving average
	/// </summary> 
	Vec3f samples[SAMPLES_TO_TAKE];
	int sampleCount;
	float wandLockOut;
	float sustainCounter;

	/// <summary> 
	/// So the wand near edge of object doesn't trigger high event triggers
	/// </summary> 
	int snapLockOut;
	Vec3f lastWandScreenPosition;

	/// <summary> 
	/// Helper functions for dealing with coordinate system changes from MPI
	/// </summary> 
	void setScreenPosition(Vec3f screenPoint, float lerp);
	Vec3f getScreenPosition();
	Vec3f getLocalPos(Vec3f glScreenPosition);

};

}
#endif /* WANDOBJECT_H_ */

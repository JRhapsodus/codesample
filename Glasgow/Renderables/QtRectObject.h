#ifndef QTRECTOBJECT_H_
#define QTRECTOBJECT_H_

#include "SIEngine/Objects/GameObject.h"

namespace Glasgow
{

class QtRectObject : public GameObject
{
	CLASSEXTENDS(QtRectObject, GameObject);
public:
	QtRectObject(Color col, Size contentSize);
	virtual ~QtRectObject();

	/// <summary>
	/// Return color of the rect
	/// </summary>
	Color getRectColor();

	/// <summary>
	/// Overriden because in a blitting renderer, our dimensions behave different
	/// without a proper transform matrix
	/// </summary>
	virtual Box3f getLocalBoundingBox();

	/// <summary>
	/// Overriden because in a blitting renderer, our dimensions behave different
	/// without a proper transform matrix
	/// </summary>
	virtual Box3f getWorldBoundingBox();

	/// <summary>
	/// Overriden because in a blitting renderer, our dimensions behave different
	/// without a proper transform matrix
	/// </summary>
	virtual Box3f getScreenBoundingBox(CameraObject* camera);

private:
	Color 	color;
	Size 	contentSize;
};

}
#endif

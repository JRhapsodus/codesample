#include "QtRectObject.h"

namespace Glasgow
{

QtRectObject::QtRectObject(Color col, Size rectSize) : super()
{
	color = col;
	contentSize = rectSize;
}

QtRectObject::~QtRectObject()
{
	printf("QtRectObject::~QtRectObject()\n");
}

/// <summary>
/// Return color of the rect
/// </summary>
Color QtRectObject::getRectColor()
{ return color; }

/// <summary>
/// Overriden because in a blitting renderer, our dimensions behave different
/// without a proper transform matrix
/// </summary>
Box3f QtRectObject::getLocalBoundingBox()
{ return Box3f( 0, 0, 0, contentSize.width_, contentSize.height_, 1 ); }

/// <summary>
/// Overriden because in a blitting renderer, our dimensions behave different
/// without a proper transform matrix
/// </summary>
Box3f QtRectObject::getWorldBoundingBox()
{
	return getScreenBoundingBox(NULL);
}

/// <summary>
/// Overriden because in a blitting renderer, our dimensions behave different
/// without a proper transform matrix
/// </summary>
Box3f QtRectObject::getScreenBoundingBox(CameraObject* camera)
{
	Vec3f pos = getPosition();
	return Box3f(pos.x , pos.y, pos.z, contentSize.width_, contentSize.height_, 1 );
}



}

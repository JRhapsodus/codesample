#include "AvatarItem.h"

namespace Glasgow
{


AvatarItem::AvatarItem(SpriteObject* icon_, TextLabel* nameLabel_, SpriteObject* profileActive_, SpriteObject* rim_, String prefabToUse) : super()
{
	addChild(icon_);
	addChild(nameLabel_);
	addChild(profileActive_);
	addChild(rim_);

	user = null;
	isNewUser = false;
	activeProfileSelection = profileActive_;
	rim = rim_;
	nameLabel = nameLabel_;
	prefabName = prefabToUse;
	activeProfileSelection->setVisible(false);
	nameLabel->setVisible(false);

	//This is to mantain depth but 'feel' flat5e081965dcb94eff6c3a710ef5b5a4d22a0092f7
	Vec3f pos = rim->getPosition();
	pos.z = -0.001f;
	rim->setPosition(pos);

	setItemType();

	setState(ButtonStateNormal);
}

///<summary>
/// Returns true if the item represents a guest user
///</summary>
bool AvatarItem::isGuest()
{
	return itemType == ItemUserAsGuest;
}

String AvatarItem::getUserName()
{
	return userName;

}
AvatarItem::~AvatarItem() {
	printf("AvatarItem::~AvatarItem()\n");
}


///<summary>
/// Sets which type of button this is
///</summary>
void  AvatarItem::setItemType()
{
	if( prefabName == "AvatarItemGuest" )
	{ itemType = ItemUserAsGuest; }
	else if( prefabName == "AvatarItemNotChosen" )
	{ itemType = ItemUserWithoutAvatar; }
	else if( prefabName == "AvatarItemNewUser" )
	{ itemType = ItemUserAsNewUser; }
	else
	{ itemType = ItemUserWithAvatar; }
}


void AvatarItem::fadeNameOut()
{
	nameLabel->cancelActions();
	nameLabel->setTextColor( Color(1,1,1,1));
	nameLabel->colorTo( Color( 1,1,1,0), 0.25f );
	nameLabel->setVisible(false);
}

///<summary>
/// Returns the type of AvatarItem this is.
///</summary>
AvatarItem::AvatarItemType AvatarItem::getItemType()
{ return itemType; }

void AvatarItem::setupAsLoginAvatar( String name )
{
	userName = name;
	nameLabel->setVisible(false);
	nameLabel->setText(name);
	nameLabel->setTextColor( Color( 1,1,1,0) );
	nameLabel->colorTo( Color( 1,1,1,1), 0.4f );
	if( name == "New User")
	{
		isNewUser = true;
		find("nameLabel")->setVisible(false);
	}

	setItemType();
}

void AvatarItem::update( float dt )
{
	//***************************************************************
	// Lookat the backward's direction torwards the camera
	//***************************************************************
	Vec3f localUp = getParent()->transform.getWorldToLocal()*(Vec3f::Up);
	float angle = Vec3f::Up.angleBetween(localUp);
	transform.setRotation(Quaternion::angleAxis(angle*57.2957795f, Vec3f::Right));
	super::update(dt);
}

String AvatarItem::getPrefabCloneName()
{
	return prefabName;
}

void AvatarItem::transition( TransitionState transition )
{
	if( transition == NormalToActive )
	{
		totalTimeElapsed = 0;
		activeProfileSelection->cancelActions();
		activeProfileSelection->setVisible(true);
		activeProfileSelection->setOpacity(0);
		activeProfileSelection->fadeTo(1, 0.25f);
	}
	else if ( transition == ActiveToNormal )
	{
		totalTimeElapsed = 0;
		activeProfileSelection->cancelActions();
		activeProfileSelection->fadeTo(0, 0.25f);
		activeProfileSelection->setVisible(false);
		scaleTo( originalScale, 0.1f );
	}
}

///<summary>
/// Returns user reference
///</summary>
User* AvatarItem::getUser()
{ return user; }

///<summary>
/// Set a weak reference to user
///</summary>
void AvatarItem::setUser( User* user_ )
{ user = user_; }


bool AvatarItem::getIsNewUser()
{ return isNewUser;}




AvatarItem* AvatarItem::deSerialize(FILE* fp)
{
	char prefabTouse[250];
	if( fscanf(fp, "%249s", prefabTouse) == 1 )
	{
		TRelease<SpriteObject> i((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<TextLabel> nl((TextLabel*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> pi((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> r((SpriteObject*)Scene::deSerializeNext(fp));
		return new AvatarItem( i, nl, pi, r, prefabTouse);
	}

	ThrowException::deserializeException();

}

}

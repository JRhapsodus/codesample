
#include "ProgressBarObject.h"
#include "SIEngine/Include/SIDeserialize.h"
#include "SIEngine/Include/SIScenes.h"

namespace Glasgow
{

ProgressBarObject::ProgressBarObject( TextLabel* available, SpriteObject* background, TextLabel* currentSpace,
		SpriteObject* l, SpriteObject* m, SpriteObject* r) : super()
{
	currentProgress = 0;

	addChild(available);
	addChild(background);
	addChild(currentSpace);
	addChild(l);
	addChild(m);
	addChild(r);

	animateBar = true;
	availableLabel = available;
	currentSpaceLabel = currentSpace;
	middle = m;
	right = r;
	left = l;
	destProgress= 0;
	right->setPosition( right->getPosition() + Vec3f::Back*10);
	left->setPosition( left->getPosition() + Vec3f::Back*10);
}

ProgressBarObject::~ProgressBarObject()
{
	printf("ProgressBarObject::~ProgressBarObject()\n");
}

/// <summary>
/// Load from file
/// </summary>
ProgressBarObject* ProgressBarObject::deSerialize(FILE* fp)
{
	TRelease<TextLabel> available((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> background((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> current((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> l((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> m((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> r((SpriteObject*)Scene::deSerializeNext(fp));

	return new ProgressBarObject(available, background, current, l, m, r);
}

/// <summary>
/// Gets progress percentage
/// </summary>
float ProgressBarObject::getProgress()
{ return currentProgress; }

/// <summary>
/// Sets the title of the progress bar
/// </summary>
void ProgressBarObject::setLabel(String primary, String secondary)
{
	currentSpaceLabel->setText(primary);
	availableLabel->setText(secondary);
}

/// <summary>
/// When true the progress bar animates to its destination
/// </summary>
void ProgressBarObject::setAnimatedBar( bool animState )
{ animateBar = animState; }

/// <summary>
/// Sets progress bar [0,1]
/// </summary>
void ProgressBarObject::setProgress( float normalizedProgress )
{
	if( normalizedProgress < 0 || normalizedProgress > 1 )
	{ ThrowException::invalidParameterException("normalizedProgress"); }

	destProgress = normalizedProgress;
}

/// <summary>
/// Custom updated to handle progress percentage display update
/// </summary>
void ProgressBarObject::update(float dt)
{
	//TODO: really we should find a smart way to calculate velocity
	if( currentProgress < destProgress )
	{
		currentProgress += 0.05f*dt;
		if( currentProgress > destProgress )
		{ currentProgress = destProgress; }
	}

	// If the bar isn't animated set progress to its destination progress
	if( !animateBar )
	{ currentProgress = destProgress; }

	//middle scale must be 6
	float defaultWidth = 10;
	float totalWidth   = 600;
	float fullSale	   = 55;

	Vec3f currentScale = middle->getScale();
	middle->setScale(Vec3f( fullSale * currentProgress, currentScale.y, currentScale.z));

	float currentWidth = middle->getScale().x * defaultWidth;
	float startX = left->getPosition().x + 45/2 + (currentWidth/2);
	middle->setPosition(Vec3f( startX, middle->getPosition().y, middle->getPosition().z ) );

	startX = middle->getPosition().x + currentWidth/2 + 45/2;
	right->setPosition( Vec3f( startX, right->getPosition().y, right->getPosition().z ) );

	super::update(dt);
}


}


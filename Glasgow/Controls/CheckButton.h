#ifndef CHECKBUTTON_H_
#define CHECKBUTTON_H_

#include "TextButton.h"
#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class CheckButton: public TextButton
{
	CLASSEXTENDS(CheckButton, TextButton);
public:
	CheckButton(Color normal,
				Color selected,
				TextLabel* text,
				SpriteObject* normalBacking,
				SpriteObject* selectedBacking,
				SpriteObject *tickNormal,
				SpriteObject *tickSelected);
	virtual ~CheckButton();

	static CheckButton* deSerialize(FILE* fp);

	virtual void transition( TransitionState transition );

	bool isChecked();
	void setChecked(bool isChecked);
	String getText();
private:
	SpriteObject *tickNormal;
	SpriteObject *tickSelected;

	bool checked;

};

}
#endif /* CHECKBUTTON_H_ */

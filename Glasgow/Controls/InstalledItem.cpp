#include "InstalledItem.h"

namespace Glasgow
{

InstalledItem::InstalledItem(
		SpriteObject* active_,
		SpriteObject* deleteIcon_,
		SpriteObject* gameicon_,
		TextLabel* nameLabel_,
		SpriteObject* rim_,
		TextLabel* storageLabel_
) : super() {

	addChild(gameicon_);
	addChild(nameLabel_);
	addChild(rim_);
	addChild(active_);
	addChild(storageLabel_);
	addChild(deleteIcon_);
	packageInfo = null;
	deleteIcon = deleteIcon_;
	gameicon = gameicon_;
	nameLabel = nameLabel_;
	rim = rim_;
	rimSelected = active_;
	storageLabel = storageLabel_;
	deleteIcon->setOpacity(0.0f);
	rimSelected->setOpacity(0.0f);
}

InstalledItem::~InstalledItem()
{
	printf("InstalledItem::~InstalledItem()\n");
}

void InstalledItem::update( float dt )
{ super::update(dt); }


PackageInfo* InstalledItem::getPackageInfo()
{ return packageInfo; }

bool InstalledItem::isDeletable()
{ return packageInfo->isDeletable; }

void InstalledItem::setPackageInfo( PackageInfo* info )
{
	packageInfo = info;
	originalScale = Vec3f::One*0.8f;
	setScale(Vec3f::One*0.8f);
	char fileSize[100];
	sprintf( fileSize, "%.2f", ((double)info->installSize/Constants::Units::UnitMegaByte) );
	setStorage(String(fileSize) + String(" MB"));

	setItemName(info->label);

	if( info->isVideoBundle )
	{ setItemName(  ((VideoInfo*)info->videos.elementAt(0))->bundleTitle ); }

	printf("InstalledItem:: icon path %s\n", info->iconPath.str() );
	if( AssetLibrary::fileExists(info->iconPath) )
	{ setBackGroundImage(info->iconPath); }
}


/// <summary>
/// ************************************************************************************
/// Sets background
/// ************************************************************************************
/// </summary>
void InstalledItem::setBackGroundImage( String texture )
{
	gameicon->getSpriteData()->setTexture(texture);
	gameicon->setPosition(  gameicon->getPosition() + Vec3f::Right*5 + Vec3f::Down*5);
	gameicon->setScale( Vec3f::One*0.65f);
}

void InstalledItem::setStorage(String storageText_)
{
	storageLabel->setText(storageText_);
}

void InstalledItem::setItemName(String nameLabel_)
{
	nameLabel->setText(nameLabel_);
}

String InstalledItem::getItemName()
{
	return nameLabel->getText();
}

String InstalledItem::getStorage()
{
	return storageLabel->getText();

}

void InstalledItem::transition( TransitionState transition ) {
	super::transition(transition);

	if (transition == NormalToActive)
	{
		rimSelected->fadeTo(1.0f, 0.25);

		if( packageInfo->isDeletable )
		{
			deleteIcon->fadeTo(1, 0.25f);
		}

	} else if (transition == ActiveToNormal)
	{
		rimSelected->fadeTo(0.0f, 0.25);
		deleteIcon->fadeTo(0, 0.25f);
	}
}

InstalledItem* InstalledItem::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> 	active((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> 	di((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> 	icon((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> 	nameLabel((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> 	rim((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> 	storageLabel((TextLabel*)Scene::deSerializeNext(fp));

	return new InstalledItem(active, di, icon, nameLabel, rim, storageLabel);
}

}

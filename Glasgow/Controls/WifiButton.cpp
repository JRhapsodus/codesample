#include "WifiButton.h"

namespace Glasgow
{

WifiButton::WifiButton(SpriteObject* active,
		SpriteObject* background,
		TextLabel* text,
		SpriteObject* selected,
		SpriteObject* wifi1,
		SpriteObject* wifi2,
		SpriteObject* wifi3,
		SpriteObject* wifi4) : super()
{
	networkNameLabel = text;
	wifiStrengthIcons[WifiStrengthWeakest] = wifi1;
	wifiStrengthIcons[WifiStrengthWeak] = wifi2;
	wifiStrengthIcons[WifiStrengthStrong] = wifi3;
	wifiStrengthIcons[WifiStrengthStrongest] = wifi4;
	backgroundActive = active;
	backgroundSelected = selected;
	backgroundNormal = background;

	addChild(wifiStrengthIcons[WifiStrengthWeakest]);
	addChild(wifiStrengthIcons[WifiStrengthWeak]);
	addChild(wifiStrengthIcons[WifiStrengthStrong]);
	addChild(wifiStrengthIcons[WifiStrengthStrongest]);
	addChild(backgroundActive);
	addChild(backgroundSelected);
	addChild(backgroundNormal);
	addChild(networkNameLabel);

	backgroundActive->setVisible(false);
	backgroundSelected->setVisible(false);

	setWifiStrength(WifiStrengthStrong);
	setNetworkName("");
	setState(ButtonStateNormal);
}

WifiButton::~WifiButton()
{
	printf("WifiButton::~WifiButton()\n");
}


void WifiButton::transition(TransitionState transition)
{
	super::transition(transition);
	backgroundNormal->setVisible(true);
	totalTimeElapsed = 0;

	if (transition == ActiveToSelected)
	{
		backgroundActive->setVisible(false);
	}
	else if (transition == SelectedToNormal)
	{
		backgroundActive->setVisible(false);
	}
	else if (transition == NormalToSelected)
	{
		backgroundActive->setVisible(false);
	}
	else if (transition == ActiveToNormal)
	{
		backgroundActive->setVisible(false);
	}
	else if(transition == NormalToActive)
	{
		backgroundActive->setVisible(true);
	}
}

//********************************************************************************
// Sets wifi strength
//********************************************************************************
void WifiButton::setWifiStrength( WifiStrength strength)
{
	currentStrength = strength;
	wifiStrengthIcons[WifiStrengthWeakest]->setVisible(false);
	wifiStrengthIcons[WifiStrengthWeak]->setVisible(false);
	wifiStrengthIcons[WifiStrengthStrong]->setVisible(false);
	wifiStrengthIcons[WifiStrengthStrongest]->setVisible(false);
	wifiStrengthIcons[strength]->setVisible(true);
}

//********************************************************************************
// Sets network name
//********************************************************************************
void WifiButton::setNetworkName( String networkName )
{
	networkNameLabel->setText(networkName, Constants::UI::wifiNetworkNameMaxLines);
//	printf("wifibutton:setnetworkName maxwidth: %d\n", networkNameLabel->getMaxWidth());
}

//********************************************************************************
// Load from file, there are 7 objects that make up a wifi button
//********************************************************************************
WifiButton* WifiButton::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> active((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> background((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> selected((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel>	txt((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> wifi1((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> wifi2((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> wifi3((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> wifi4((SpriteObject*)Scene::deSerializeNext(fp));

	return new WifiButton( active, background, txt, selected, wifi1, wifi2, wifi3, wifi4);
}

}

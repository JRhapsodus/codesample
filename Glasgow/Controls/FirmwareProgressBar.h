#ifndef FIRMWAREPROGRESSBAR_H_
#define FIRMWAREPROGRESSBAR_H_

#include "ProgressBarObject.h"
#include "SIEngine/Core/Scene.h"

namespace Glasgow
{

class FirmwareProgressBar : public ProgressBarObject
{
	CLASSEXTENDS(FirmwareProgressBar, ProgressBarObject);
	ADD_TO_CLASS_MAP;
public:
	FirmwareProgressBar( TextLabel* available, SpriteObject* background, TextLabel* currentSpace, SpriteObject* l, SpriteObject* m, SpriteObject* r);
	virtual ~FirmwareProgressBar();

	/// <summary>
	/// Load from file
	/// </summary>
	static FirmwareProgressBar* deSerialize(FILE* fp);

	/// <summary>
	/// Download progress
	/// </summary>
	virtual void handleDownloadProgress( String package, int downloadProgress);

	/// <summary>
	/// Download finished
	/// </summary>
	virtual void handleDonwloadFinished( String packageID );

	/// <summary>
	/// Download started
	/// </summary>
	virtual void handleDonwloadStarted( String packageID );

	/// <summary>
	/// Sets up display for the number of downloads
	/// </summary>
	void setDisplay( int totalDownloads );

	/// <summary>
	/// Set when the download is finished
	/// </summary>
	bool getIsFinished();


	virtual void update(float dt );

private:
	float   lastProgressHeard;
	int		totalDownloads;
	int 	finishedDownloads;
	int 	remainingDownloads;
	float 	currentDownloadProgress;
	float	percentPerDownload;
	bool	isFinished;
};

}
#endif

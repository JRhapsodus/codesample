#include "CheckButton.h"

namespace Glasgow
{

CheckButton::CheckButton(Color normal,
		Color selected,
		TextLabel* text,
		SpriteObject* normalBacking,
		SpriteObject* selectedBacking,
		SpriteObject *tickNormal_,
		SpriteObject *tickSelected_) : super(normal, selected, text, normalBacking, selectedBacking)
{
	tickNormal = tickNormal_;
	tickSelected = tickSelected_;

	addChild(tickNormal);
	addChild(tickSelected);

	tickNormal->setVisible(false);
	tickSelected->setVisible(false);
	checked = false;
}

CheckButton::~CheckButton()
{
}


bool CheckButton::isChecked() {
	return checked;
}

void CheckButton::setChecked(bool isChecked)
{
	checked = isChecked;
	totalTimeElapsed = 0;
	if (checked == false)
	{
		tickNormal->setVisible(false);
		tickSelected->setVisible(false);
	}
	else if (currentState == ButtonStateActive)
	{
		tickNormal->setVisible(false);
		tickSelected->setVisible(true);
	}
	else if (currentState == ButtonStateNormal)
	{
		tickNormal->setVisible(true);
		tickSelected->setVisible(false);
	}
}

void CheckButton::transition( TransitionState transition )
{
	super::transition(transition);

	totalTimeElapsed = 0;
	scaleTo( originalScale, 0.25f );
	if (transition == ActiveToSelected)
	{
		checked = true;
		tickNormal->setVisible(false);
		tickSelected->setVisible(checked);
	}
	else if (transition == ActiveToNormal)
	{
		tickNormal->setVisible(checked);
		tickSelected->setVisible(false);
	}
	else if (transition == SelectedToNormal)
	{
		tickNormal->setVisible(checked);
		tickSelected->setVisible(false);
	}
	else if (transition == NormalToSelected)
	{
		checked = true;
		tickNormal->setVisible(false);
		tickSelected->setVisible(checked);
	}
	else if (transition == DisabledToActive )
	{
		tickNormal->setVisible(checked);
		tickSelected->setVisible(true);
		tickNormal->setVisible(true);
	}
}

CheckButton* CheckButton::deSerialize(FILE* fp)
{
	Color normal;
	Color selected;

	if( fscanf(fp, "%f %f %f %f %f %f %f %f", &normal.r, &normal.g, &normal.b, &normal.a, &selected.r, &selected.g, &selected.b, &selected.a) == 8)
	{
		TRelease<SpriteObject> normalBacking((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> selectedBacking((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<TextLabel> text((TextLabel*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> tickNormal((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> tickSelected((SpriteObject*)Scene::deSerializeNext(fp));

		return new CheckButton(normal, selected, text, normalBacking, selectedBacking, tickNormal, tickSelected);
	}

	return NULL;
}

String CheckButton::getText()
{
	return buttonText->getText();
}

}

#include "ContentItem.h"
#include "SIEngine/Include/SIMaterials.h"
#include "SIEngine/Include/SIScenes.h"
#include "SIEngine/Rendering/TextureManager.h"


namespace Glasgow
{
/// <summary>
/// ************************************************************************************
/// Represents a visual ringed content item that can be launched within the ChildScene
/// ************************************************************************************
/// </summary>
ContentItem::ContentItem(SpriteObject* banner_, TextLabel* bannerText_, SpriteObject* backingImage_,SpriteObject* lockIcon_,
		SpriteObject* playIcon_, SpriteObject* ring_, SpriteObject* ringShimmer_ ) : super()
{
	audioTimer = 0;
	originalLocalPosition = Vec3f::Max;
	ring = ring_;
	ringShimmer = ringShimmer_;
	playIcon = playIcon_;
	backing = backingImage_;
	lockIcon = lockIcon_;
	bannerText = bannerText_;
	banner = banner_;
	clusterOffset = Vec3f::Zero;
	randomSeed = randomFloat(0, 6);

	Vec3f curPos = lockIcon->getPosition();
	curPos.z = -0.1f;
	lockIcon->setPosition( curPos);

	addChild(ring_);
	addChild(ringShimmer_);
	addChild(playIcon_);
	addChild(backingImage_);
	addChild(lockIcon_);
	addChild(bannerText_);
	addChild(banner_);

	banner->setVisible(false);
	ringShimmer->setVisible(true);
	bannerText->setVisible(false);
	playIcon->setVisible(false);

	setChildrenBlurShader();
	ring->colorTo(Color::Red, 15);
	ringShimmer->setOpacity(0.5f);
	contentType = ContentApp;
	packageInfo = NULL;
}

ContentItem::~ContentItem()
{
	printf("ContentItem::~ContentItem()\n");
	if( packageInfo->isCartridge() )
	{
		printf("Removing cartridge texture\n");
		TextureManager::getInstance()->unloadTexture( packageInfo->iconPath );
	}
}

/// <summary>
/// ************************************************************************************
/// Handle custom transitions
/// ************************************************************************************
/// </summary>
void ContentItem::transition(TransitionState transition)
{
	if( transition == NormalToActive )
	{
		audioTimer = 1.25;
		Color color = bannerText->getTextColor();
		bannerText->cancelActions();
		bannerText->setVisible(true);
		bannerText->setTextColor(Color(color.r, color.g, color.b, 0));
		bannerText->waitFor(0.2f);
		bannerText->colorTo(Color(color.r, color.g, color.b, 1), 0.25f);

		banner->setVisible(false);
		banner->makeVisible(true);
		banner->setFrame(0);
		banner->playAnimation("bannerAppear");

		float glowAmount = 1.4f;
		Color hover = Color(rimColor.r * glowAmount, rimColor.g * glowAmount, rimColor.b * glowAmount, 1);
		ring->colorTo( hover, 0.5f);
	}
	else if( transition == ActiveToNormal )
	{
		audioTimer = 0;
		banner->setVisible(false);
		bannerText->setVisible(false);
		ring->colorTo(rimColor, 0.5f);
	}

	super::transition(transition);
}

/// <summary>
/// ************************************************************************************
/// Sets background
/// ************************************************************************************
/// </summary>
void ContentItem::setBackGroundImage( String texture )
{
	backing->getSpriteData()->setTexture(texture);
}

/// <summary>
/// ************************************************************************************
/// Load from file
/// ************************************************************************************
/// </summary>
ContentItem* ContentItem::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> banner_((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> bannerText_((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> backingImage_((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> lockImage_((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> playIcon_((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> ring_((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> ringShimmer_((SpriteObject*)Scene::deSerializeNext(fp));

	return new ContentItem(banner_, bannerText_, backingImage_, lockImage_, playIcon_, ring_, ringShimmer_ );
}

/// <summary>
/// ************************************************************************************
/// Custom over ride for facing the camera and bobbing in space
/// ************************************************************************************
/// </summary>
void ContentItem::update( float dt )
{

	super::update(dt);

	//***************************************************************
	// Triggers a vo audio sound after 1.25 seconds
	//***************************************************************
	if( audioTimer > 0 )
	{
		audioTimer -= dt;
		if( audioTimer <= 0 )
		{
			if( AssetLibrary::fileExists(packageInfo->audioPath) )
			{
				Scene::instancePlayVo(packageInfo->audioPath);
			}
		}
	}

	//***************************************************************
	// Record original position on first tick
	//***************************************************************
	if( originalLocalPosition == Vec3f::Max )
	{ originalLocalPosition = transform.getPosition(); }

	//***************************************************************
	// Bob up and down sinusoidally
	//***************************************************************
	if( !hasActions() )
	{
		Vec3f currentLocalPosition = getPosition();

		Vec3f bobOffset = Vec3f(0, 3.1f*sinf(totalTimeElapsed+randomSeed), 0 );
		Vec3f newLocalPosition = originalLocalPosition + clusterOffset + bobOffset;
		setPosition(newLocalPosition);
	}

	//***************************************************************
	// Lookat the backward's direction torwards the camera
	//***************************************************************
	Vec3f targetLocation = Vec3f::Back;
	Vec3f localTarget = ((GameObject*)parent)->transform.getWorldToLocal()*targetLocation;
	Vec3f localUp = ((GameObject*)parent)->transform.getWorldToLocal()*Vec3f::Up;
	Quaternion quat = Quaternion::lookRotation(localTarget, localUp);
	quat *= Quaternion::angleAxis(180, Vec3f::Up);
	transform.setRotation(quat);
}

/// <summary>
/// ************************************************************************************
/// Sets the banner text
/// ************************************************************************************
/// </summary>
void ContentItem::setBannerText( String name )
{
	appName = name;
	bannerText->setText(appName, Constants::UI::contentItemBannerTextMaxLines);
}

/// <summary>
/// Gets the app name
/// </summary>
String ContentItem::getAppName()
{ return appName; }

/// <summary>
/// ************************************************************************************
/// Sets the content path
/// ************************************************************************************
/// </summary>
void ContentItem::setContentType( ContentType content )
{ contentType = content; }

/// <summary>
/// ************************************************************************************
/// gets the path and app to launch when selected
/// ************************************************************************************
/// </summary>
String ContentItem::getContentPath()
{
	if( packageInfo == null )
	{ return ""; }
	else
	{ return packageInfo->appSoPath; }
}

/// <summary>
/// ***********************************************************************************
/// Shows banner
/// ***********************************************************************************
/// </summary>
void ContentItem::showBanner()
{
	banner->setVisible(true);
	bannerText->setVisible(true);
}

/// <summary>
/// ************************************************************************************
/// Struct container holding weak reference to packageData from PackageManager
/// ************************************************************************************
/// </summary>
void ContentItem::setPackageInfo( PackageInfo* info )
{
	packageInfo = info;
	lockIcon->setVisible(false);
}

/// <summary>
/// ************************************************************************************
/// Struct container holding weak reference to packageData from PackageManager
/// ************************************************************************************
/// </summary>
PackageInfo* ContentItem::getPackageInfo()
{ return packageInfo; }


/// <summary>
/// ************************************************************************************
/// Bubbles in item visually
/// ************************************************************************************
/// </summary>
void ContentItem::bubbleIn( float dt )
{
	float waitTime = dt;
	setVisible(true);
	setScale(Vec3f::Zero);
	bannerText->setVisible(false);
	banner->setVisible(false);
	playIcon->setVisible(false);
	waitFor(waitTime);
	scaleTo( originalScale, 0.35f );
	ring->cancelActions();
	ring->setColor( Color( 1,1,1,1) );
	ring->waitFor(waitTime);
	ring->colorTo(rimColor, 0.5f);
}

/// <summary>
/// ************************************************************************************
/// Bubbles in item visually
/// ************************************************************************************
/// </summary>
void ContentItem::bubbleOut( float dt )
{
	waitFor(dt);
	scaleTo(Vec3f::Zero, 0.25f);
	makeVisible(false);
}

/// <summary>
/// ************************************************************************************
/// Changes the ring color
/// ************************************************************************************
/// </summary>
void ContentItem::setRingColor( Color ringColor )
{ rimColor = ringColor; }

/// <summary>
/// Changes the ring color
/// </summary>
void ContentItem::changeRingColorFast( Color ringColor )
{
	rimColor = ringColor;
	ring->cancelActions();
	ring->colorTo(ringColor, 0.5f);
}


/// <summary>
/// Changes the ring color
/// </summary>
void ContentItem::changeRingColor( Color ringColor )
{
	rimColor = ringColor;
	ring->cancelActions();
	ring->colorTo(ringColor, 1.5f);
}

/// <summary>
/// Override move to since we want custom commands for this object
/// </summary>
void ContentItem::moveTo(Vec3f position, float duration)
{
	totalTimeElapsed = 0;
	originalLocalPosition = position;
	super::moveTo(position + clusterOffset, duration);
}

/// <summary>
/// ************************************************************************************
/// Goes through children and replaces their material with a bluring material
/// ************************************************************************************
/// </summary>
void ContentItem::setChildrenBlurShader()
{
	ring->setMaterial(TRelease<TintMaterial>(new TintMaterial(ring)));
	backing->setMaterial( TRelease<BlurredMaterial>(new BlurredMaterial(backing)));
}


}


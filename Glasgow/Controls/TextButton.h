#ifndef TEXTBUTTON_H_
#define TEXTBUTTON_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

/// <summary> 
/// This is really just a normal button but is subclassed for ease of use
/// in deserializing and also presetting the feature set of the button
/// </summary> 
class TextButton : public ButtonObject
{
	CLASSEXTENDS(TextButton, ButtonObject);

public:
	TextButton(Color normal, Color selected, TextLabel* text, SpriteObject* normalBacking, SpriteObject* selectedBacking );
	virtual ~TextButton();

	static TextButton* deSerialize(FILE* fp);

	virtual void update(float dt );
	virtual void transition( TransitionState transition );

	TextLabel* buttonText;

protected:
	Color normalColor;
	Color selectedColor;

};

}
#endif /* TEXTBUTTON_H_ */

#include "PurchasedItem.h"

namespace Glasgow
{

PurchasedItem::PurchasedItem(
		SpriteObject* active_,
		SpriteObject* background_,
		ObjectArray* arrayOfDlIcons_,
		TextLabel* errorLabel_,
		TextLabel* titleLabel_,
		SpriteObject* gameIcon_,
		TextLabel* storageLabel_
	 ) : super()
{

	scaleOnActiveAmount = 0.04f;
	dlProgressIcons = arrayOfDlIcons_;
	dlProgressIcons->addRef();

	for( int i =0  ;i<dlProgressIcons->getSize(); i++ )
	{ addChild((SpriteObject*)dlProgressIcons->elementAt(i)); }
	addChild(active_);
	addChild(background_);
	addChild(errorLabel_);
	addChild(titleLabel_);
	addChild(gameIcon_);
	addChild(storageLabel_);

	active = active_;
	gameIcon = gameIcon_;
	errorLabel = errorLabel_;
	storageLabel = storageLabel_;
	titleLabel  = titleLabel_;
	background = background_;

	errorLabel->setVisible(false);
	active->setVisible(false);
	packageInfo = null;
}

PurchasedItem::~PurchasedItem()
{
	printf("PurchasedItem::~PurchasedItem()\n");
	SAFE_RELEASE(dlProgressIcons);
	SAFE_RELEASE(packageInfo);
}

void PurchasedItem::setTitle( String title )
{
	titleLabel->setText( title );
}

void PurchasedItem::setStorage( String storage )
{
	storageLabel->setText( storage );
}

void PurchasedItem::setBackGroundImage( String texture )
{
	if( AssetLibrary::fileExists(texture) )
	{ gameIcon->getSpriteData()->setTexture(texture); }
}


void PurchasedItem::setPackageInfo(PackageInfo* info)
{
	packageInfo = info;
	packageInfo->addRef();
	setTitle(info->label);
	setBackGroundImage(info->iconPath);
	if( info->isVideoBundle )
	{ setTitle(  ((VideoInfo*)info->videos.elementAt(0))->bundleTitle ); }
}

void PurchasedItem::setError( String error )
{
	errorLabel->setText( error );
}

void PurchasedItem::update( float dt )
{
	if( packageInfo != null )
	{
		tPackageStatus packageStatus = packageInfo->getPackageStatus();

		if( packageStatus == kPackageStatus_Pending )
		{ errorLabel->setText("Queued"); }
		else if ( packageStatus == kPackageStatus_Downloading )
		{ errorLabel->setText("Downloading"); }
		else if ( packageStatus == kPackageStatus_Installing )
		{ errorLabel->setText("Installing"); }
		else if ( packageStatus == kPackageStatus_Installed )
		{ errorLabel->setText("Installed"); }
		else if ( packageStatus == kPackageStatus_Removing )
		{ errorLabel->setText("Uninstalling"); }
		else if ( packageStatus == kPackageStatus_Removed )
		{ errorLabel->setText("Removed"); }
		else
		{ errorLabel->setText("Error"); }
	}
	super::update(dt);
}

void PurchasedItem::transition( TransitionState transition )
{
	if (transition == NormalToActive)
	{
		active->setVisible(true);
	}
	else if (transition == ActiveToNormal)
	{
		totalTimeElapsed = 0;
		scaleToOriginal(0.8f);
		active->setVisible(false);
	}
}

//*******************************************************************************************************************
//Roll Over
//	backing
//	rollover Object
//*******************************************************************************************************************
PurchasedItem* PurchasedItem::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> active((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> background((SpriteObject*)Scene::deSerializeNext(fp));

	//this is donkey
	TRelease<ObjectArray> download( new ObjectArray());
	for ( int i = 0; i<10; i++ )
	{ download->addElement((SpriteObject*)Scene::deSerializeNext(fp)); }

	TRelease<TextLabel> errorlabel((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> gameTitle((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> gameicon((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> storageLabel((TextLabel*)Scene::deSerializeNext(fp));

	return new PurchasedItem(active,background,download,errorlabel,gameTitle,gameicon,storageLabel);
}

}

#include "LoginButton.h"

namespace Glasgow
{

LoginButton::LoginButton(SpriteObject *rimNormal, SpriteObject *rimSelected, SpriteObject *plate, TextLabel *label, SpriteObject *avatar)
{
	profileImage = avatar;
	bubbleNormal = rimNormal;
	bubbleActive = rimSelected;
	plateNormal = plate;
	username = label;
	bubble = NULL;
	user = NULL;
	loginButtonType = LoginButtonNone;

	addChild(bubbleNormal);
	addChild(bubbleActive);
	addChild(plateNormal);
	addChild(username);
	addChild(profileImage);

	bubbleActive->setVisible(false);
	setState(ButtonStateNormal);
}

LoginButton::~LoginButton()
{
	printf("LoginButton::~LoginButton()\n");
	SAFE_RELEASE(user);
}

User *LoginButton::getUser()
{
	return user;
}

void LoginButton::setUser(User *user_)
{
	//todo set aavatar on user set

	loginButtonType = LoginButtonUser;
	user = user_;
	printf("SetUser named %s\n", user->getDisplayName().str());
	username->setText(user->getDisplayName());
	user->addRef();
}

void LoginButton::setupForAddUser() {
	loginButtonType = LoginButtonAddUser;
	// TODO: Localize strings
	username->setText("Add User");
	name = "Add User";
	// Background image "profile-addUser"

}

LoginButtonType LoginButton::getLoginButtonType() {
	return loginButtonType;
}
void LoginButton::transition(TransitionState state)
{
	super::transition(state);

	if( state == NormalToActive)
	{
		bubbleNormal->setVisible(false);
		bubbleActive->setVisible(true);
	}
	else if (state == ActiveToNormal)
	{
		bubbleNormal->setVisible(true);
		bubbleActive->setVisible(false);
	}
}

LoginButton* LoginButton::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> avatar((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> label((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> plate((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> bubbleNormal((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> bubbleActive((SpriteObject*)Scene::deSerializeNext(fp));

	return new LoginButton(bubbleNormal, bubbleActive, plate, label, avatar);
}

}

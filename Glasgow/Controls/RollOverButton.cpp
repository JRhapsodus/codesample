#include "RollOverButton.h"

namespace Glasgow
{

RollOverButton::RollOverButton( SpriteObject* normalBacking, TextLabel* roll ) : super()
{
	addChild(normalBacking);
	addChild(roll);

	rollOver = roll;
	rollOver->setVisible(false);
	originalRollOverScale = rollOver->getScale();
}

RollOverButton::~RollOverButton()
{ }
void RollOverButton::transition(TransitionState transition)
{
	super::transition(transition);

	if( transition == NormalToActive )
	{
		totalTimeElapsed = 0;
		rollOver->setVisible(true);
		rollOver->scaleTo(rollOver->originalScale, 0.25f);
		rollOver->cancelActions();
	}
	else if ( transition == ActiveToNormal )
	{
		totalTimeElapsed = 0;
		rollOver->scaleTo( Vec3f::Zero, 0.25f);
		rollOver->cancelActions();

	}
}

//*******************************************************************************************************************
//Roll Over
//	backing
//	rollover Object
//*******************************************************************************************************************
RollOverButton* RollOverButton::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> normalBacking((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> roll((TextLabel*)Scene::deSerializeNext(fp));
	return new RollOverButton(normalBacking, roll);
}

}

#include "GateLockObject.h"

namespace Glasgow
{

GateLockObject::GateLockObject(	ButtonObject *button0_,
								ButtonObject *button1_,
								ButtonObject *button2_,
								ButtonObject *button3_,
								ButtonObject *button4_,
								ButtonObject *button5_,
								ButtonObject *button6_,
								ButtonObject *button7_,
								ButtonObject *button8_,
								ButtonObject *button9_,
								ButtonObject *backspace_)
{
	button0 = button0_;
	button1 = button1_;
	button2 = button2_;
	button3 = button3_;
	button4 = button4_;
	button5 = button5_;
	button6 = button6_;
	button7 = button7_;
	button8 = button8_;
	button9 = button9_;
	backspace = backspace_;

	addChild(button0);
	addChild(button1);
	addChild(button2);
	addChild(button3);
	addChild(button4);
	addChild(button5);
	addChild(button6);
	addChild(button7);
	addChild(button8);
	addChild(button9);
	addChild(backspace);
}

GateLockObject::~GateLockObject()
{ }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void GateLockObject::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(button0);
	trackingList->addElement(button1);
	trackingList->addElement(button2);
	trackingList->addElement(button3);
	trackingList->addElement(button4);
	trackingList->addElement(button5);
	trackingList->addElement(button6);
	trackingList->addElement(button7);
	trackingList->addElement(button8);
	trackingList->addElement(button9);
	trackingList->addElement(backspace);
}

GateLockObject* GateLockObject::deSerialize(FILE* fp)
{
	TRelease<ButtonObject> button0((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button1((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button2((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button3((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button4((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button5((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button6((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button7((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button8((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> button9((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> backspace((ButtonObject*)Scene::deSerializeNext(fp));
	return new GateLockObject(	button0, button1, button2, button3, button4, button5, button6, button7, button8, button9, backspace);
}

}

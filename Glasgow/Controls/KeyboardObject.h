#ifndef KEYBOARDOBJECT_H_
#define KEYBOARDOBJECT_H_

#include "SIEngine/Include/SICore.h"
#include "SIEngine/Include/SIObjects.h"

namespace Glasgow
{

class KeyboardObject : public GameObject
{
	CLASSEXTENDS(KeyboardObject, GameObject);
public:
	typedef enum GlyphGroups
	{
		GlyphGroupNone,
		GlyphGroupUpperCase,
		GlyphGroupLowerCase,
		GlyphGroupDecimal,
		GlyphGroupSymbols
	} GlyphGroup;

	typedef enum CursorGroups
	{
		CursorGroupSwitcher,
		CursorGroupGlyphs
	} CursorGroup;


	KeyboardObject(Scene* owner, bool showSymbolKeyboard);
	virtual ~KeyboardObject();

	/// <summary> 
	/// Base scene input responders
	/// </summary> 
	bool onButtonPressAction();
	String typedCharacter();
	void onButtonPressBack();
	bool onSelectionChange( GameObject* oldSelection, GameObject* newSelection );
	void setupHotSpots();
	void removeTrackedObjects();
	void setText(String text);
	void setMaxCharacters( int maxLength );
	String getText();
	GameObject* getDefaultHighLight();
	void setLeadingCaps(bool value);
	bool getLeadingCaps();
	void reset();

	bool isButtonGlyphGroup(ButtonObject* button);

private:
	int maximumCharacters;
	void setGlyphGroup(GlyphGroup group);
	void appendCharacter(String c);
	void backspace();


	GlyphGroup 		currentGroup;

	GameObject* 	selectedGlyph;
	GameObject* 	glyphsUpperCase;
	GameObject* 	glyphsLowerCase;
	GameObject* 	glyphsDecimal;
	GameObject* 	glyphsSymbols;

	ButtonObject* 	uppercaseButton;
	ButtonObject* 	lowercaseButton;
	ButtonObject* 	decimalButton;
	ButtonObject* 	symbolButton;

	Scene* 			owningScene;
	ObjectArray* 	trackedObjects;

	bool			isLeadingCaps;
	bool			isShowingSymbols;

	String text;
};

}

#endif /* KEYBOARDOBJECT_H_ */


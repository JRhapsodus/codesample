#ifndef PROGRESSBAROBJECT_H_
#define PROGRESSBAROBJECT_H_

#include "SIEngine/Include/SIObjects.h"

namespace Glasgow
{

class ProgressBarObject : public GameObject
{
	CLASSEXTENDS( ProgressBarObject, GameObject);
	ADD_TO_CLASS_MAP;
public:
	ProgressBarObject( TextLabel* available, SpriteObject* background, TextLabel* currentSpace, SpriteObject* l, SpriteObject* m, SpriteObject* r);
	virtual ~ProgressBarObject();

	/// <summary>
	/// Load from file
	/// </summary>
	static ProgressBarObject* deSerialize(FILE* fp);

	/// <summary>
	/// Sets the title of the progress bar
	/// </summary>
	void setLabel(String primary, String secondary);

	/// <summary>
	/// Custom updated to handle progress percentage display update
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Sets progress bar [0,1]
	/// </summary>
	void setProgress( float normalizedProgress );

	/// <summary>
	/// When true the progress bar animates to its destination
	/// </summary>
	void setAnimatedBar( bool animState );

	/// <summary>
	/// Gets progress percentage
	/// </summary>
	float getProgress();

private:
	bool animateBar;
	float  vel;
	float destProgress;
	float currentProgress;
	TextLabel* availableLabel;
	TextLabel* currentSpaceLabel;

	SpriteObject* middle;
	SpriteObject* right;
	SpriteObject* left;
};

}
#endif /* PROGRESSBAROBJECT_H_ */

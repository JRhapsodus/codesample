#ifndef GATELOCKOBJECT_H_
#define GATELOCKOBJECT_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class GateLockObject: public GameObject
{
	CLASSEXTENDS(GateLockObject, GameObject);
public:
	GateLockObject(	ButtonObject *button0,
				   	ButtonObject *button1,
				   	ButtonObject *button2,
				   	ButtonObject *button3,
				   	ButtonObject *button4,
				   	ButtonObject *button5,
				   	ButtonObject *button6,
				   	ButtonObject *button7,
				   	ButtonObject *button8,
				   	ButtonObject *button9,
				   	ButtonObject *backspace
				  );
	virtual ~GateLockObject();

	static GateLockObject* deSerialize(FILE* fp);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	void addHotSpots( ObjectArray* trackingList );

private:
	ButtonObject *button0;
	ButtonObject *button1;
	ButtonObject *button2;
	ButtonObject *button3;
	ButtonObject *button4;
	ButtonObject *button5;
	ButtonObject *button6;
	ButtonObject *button7;
	ButtonObject *button8;
	ButtonObject *button9;

	ButtonObject *backspace;
};

}

#endif /* GATELOCKOBJECT_H_ */

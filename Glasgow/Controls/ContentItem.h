#ifndef FACINGCAMERAOBJECT_H_
#define FACINGCAMERAOBJECT_H_

#include "SIEngine/Include/SIObjects.h"
#include "SIEngine/Include/SIGeometry.h"
#include "Glasgow/Data/PackageInfo.h"

namespace Glasgow
{

enum ContentType
{
	ContentApp,
	ContentVideo
};

class ContentItem : public ButtonObject
{
	CLASSEXTENDS(ContentItem, ButtonObject);
public:
	ContentItem(SpriteObject* banner, TextLabel* bannerText, SpriteObject* backingImage, SpriteObject* lockIcon_,
			SpriteObject* playIcon, SpriteObject* ring, SpriteObject* ringShimmer );
	virtual ~ContentItem();

	/// <summary>
	/// Goes through children and replaces their material with a bluring material
	/// </summary>
	void setChildrenBlurShader();

	/// <summary>
	/// Load from file
	/// </summary>
	static ContentItem* deSerialize(FILE* fp);

	/// <summary>
	/// Handle custom transitions
	/// </summary>
	virtual void transition(TransitionState transition);

	/// <summary>
	/// Custom over ride for facing the camera and bobbing in space
	/// </summary>
	virtual void update( float dt );

	/// <summary>
	/// Bubbles in item visually
	/// </summary>
	virtual void bubbleIn( float dt );

	/// <summary>
	/// Bubbles in item visually
	/// </summary>
	virtual void bubbleOut( float dt );

	/// <summary>
	/// Determines if this content item represents an app, video, or internal launcher
	/// </summary>
	ContentType contentType;

	/// <summary>
	/// Changes the ring color
	/// </summary>
	void setRingColor( Color ringColor );

	/// <summary>
	/// Changes the ring color
	/// </summary>
	void changeRingColor( Color ringColor );

	/// <summary>
	/// Changes the ring color
	/// </summary>
	void changeRingColorFast( Color ringColor );

	/// <summary>
	/// Sets the path and app to launch when selected
	/// </summary>
	void setContentType( ContentType content );

	/// <summary>
	/// ***********************************************************************************
	/// Sets the banner text
	/// ***********************************************************************************
	/// </summary>
	void setBannerText( String name );

	/// <summary>
	/// ***********************************************************************************
	/// Shows banner
	/// ***********************************************************************************
	/// </summary>
	void showBanner();

	/// <summary>
	/// ***********************************************************************************
	/// gets the path and app to launch when selected
	/// ***********************************************************************************
	/// </summary>
	String getContentPath();

	/// <summary>
	/// ***********************************************************************************
	/// Gets the app name
	/// ***********************************************************************************
	/// </summary>
	String getAppName();

	/// <summary>
	/// ***********************************************************************************
	/// Override move to since we want custom commands for this object
	/// ***********************************************************************************
	/// </summary>
	virtual void moveTo(Vec3f position, float duration);

	/// </summary>
	/// ***********************************************************************************
	/// Accessed via a materil
	/// ***********************************************************************************
	/// </summary>
	SpriteObject* ringShimmer;

	/// <summary>
	/// ***********************************************************************************
	/// Sets background based on the full path to a texture
	/// ************************************************************************************
	/// </summary>
	void setBackGroundImage( String texture );

	/// <summary>
	/// ************************************************************************************
	/// Struct container holding weak reference to packageData from PackageManager
	/// ************************************************************************************
	/// </summary>
	void setPackageInfo( PackageInfo* info );

	/// <summary>
	/// ************************************************************************************
	/// Struct container holding weak reference to packageData from PackageManager
	/// ************************************************************************************
	/// </summary>
	PackageInfo* getPackageInfo();

	Vec3f originalLocalPosition;
	Vec3f clusterOffset;
private:
	Color rimColor;
	float randomSeed;
	float audioTimer;

	PackageInfo* packageInfo;
	SpriteObject* ring;
	SpriteObject* playIcon;
	SpriteObject* lockIcon;
	SpriteObject* backing;
	SpriteObject* banner;
	TextLabel* 	  bannerText;

	String appName;
};

}
#endif

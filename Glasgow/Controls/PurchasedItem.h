#ifndef PurchasedItem_H
#define PurchasedItem_H

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

/// <summary> 
///
/// </summary> 
class PurchasedItem : public ButtonObject
{
	CLASSEXTENDS(PurchasedItem, ButtonObject);

public:
	PurchasedItem(
			SpriteObject* active,
			SpriteObject* background,
			ObjectArray* arrayOfDlIcons,
			TextLabel* errorLabel,
			TextLabel* titleLabel,
			SpriteObject* gameIcon,
			TextLabel* storageLabel
		 );
	virtual ~PurchasedItem();


	void setTitle( String title );
	void setStorage( String storage );
	void setError( String error );
	void setPackageInfo(PackageInfo* info);
	void setBackGroundImage( String texture );
	static PurchasedItem* deSerialize(FILE* fp);

	virtual void update( float dt );

	/// <summary> 
	/// Transition states from one state to other for subclass animations
	/// </summary> 
	virtual void transition( TransitionState transition );
private:
	SpriteObject* active;
	SpriteObject* background;
	SpriteObject* gameIcon;
	TextLabel* errorLabel;
	TextLabel* storageLabel;
	TextLabel* titleLabel;
	ButtonObject* downloadButton;
	ObjectArray* dlProgressIcons;
	PackageInfo* packageInfo;

};

}

#endif /* PurchasedItem_H */

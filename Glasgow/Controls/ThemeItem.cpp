#include "ThemeItem.h"

namespace Glasgow
{

ThemeItem::ThemeItem(SpriteObject* icon_, String name)  : super()
{
	addChild(icon_);
	themeName = name;
	icon = icon_;
	setState(ButtonStateNormal);
}

ThemeItem::~ThemeItem()
{
	printf("ThemeItem::~ThemeItem()\n");
}

ThemeItem* ThemeItem::deSerialize(FILE* fp)
{
	char themeName[250];
	if( fscanf(fp, "%249s", themeName) == 1 )
	{
		String name = themeName;
		TRelease<SpriteObject> i( (SpriteObject*)Scene::deSerializeNext(fp) );
		return new ThemeItem(i, name);
	}

	ThrowException::deserializeException();
}

void ThemeItem::update(float dt)
{
	super::update(dt);
}

void ThemeItem::transition(TransitionState transition)
{
	if( transition == NormalToActive )
	{
		totalTimeElapsed = 0;
	}
	else if ( transition == ActiveToNormal )
	{
		totalTimeElapsed = 0;
	}
}

String ThemeItem::getThemeName()
{ return themeName;}

}

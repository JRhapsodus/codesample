#ifndef SLIDEROBJECT_H_
#define SLIDEROBJECT_H_

#include "SIEngine/Include/SIDeserialize.h"

using namespace SICore;

namespace Glasgow
{

class SliderObject : public GameObject
{
	CLASSEXTENDS(SliderObject, GameObject);
	ADD_TO_CLASS_MAP;
public:
	/// <summary>
	/// Helper enum to prefill the slider with our known slider presets
	/// </summary>
	enum SliderType
	{
		SliderTypeAudio,
		SliderTypeAvatar,
		SliderTypeTheme,
		SliderTypeGallery,
		SliderTypeVideo,
		SliderTypeWifi
	};

	/// <summary>
	/// 2 element deserialize
	/// </summary>
	SliderObject(SpriteObject* sliderLeft, SpriteObject* sliderRight);
	virtual ~SliderObject();
	virtual void loadGameObjectState(GameObjectState state);

	/// <summary>
	/// Returns the selected item
	/// </summary>
	GameObject* getSelectedItem();

	/// <summary>
	/// Add item to slider object
	/// </summary>
	void addItem(GameObject* itemToAdd);

	/// <summary>
	/// Add item to slider object
	/// </summary>
	void removeItem(GameObject* itemToRemove);

	/// <summary>
	/// Tick override
	/// </summary>
	virtual void update( float dt );

	/// <summary>
	/// Respond to input
	/// </summary>
	virtual void onButtonPressAction(GameObject* currentHighlight);

	/// <summary>
	/// Respond to input
	/// </summary>
	virtual bool onAnalogChange(GameObject* objectLeft, GameObject* objectEntered);

	/// <summary>
	/// Load from file
	/// </summary>
	static SliderObject* deSerialize(FILE* fp);

	/// <summary>
	/// Preset filler helper
	/// </summary>
	void setupSlider( SliderObject::SliderType type );

	int getSelectedIndex();

	/// <summary>
	/// Secondary type of setup which uses objects to preset the items
	/// </summary>
	void setupSlider( SliderObject::SliderType type, ObjectArray* arrayOfInfo );

	/// <summary>
	/// Animation heper
	/// </summary>
	void staggerElementsIn();

	/// <summary>
	/// Animation helper
	/// </summary>
	void setItemsAsInactive();

	/// <summary>
	/// Sets the selection of the slider to a specifc element
	/// </summary>
	void setSelected(String itemName);

	/// <summary>
	/// Animation helper
	/// </summary>
	void setItemsAsActive();

	/// <summary>
	/// Animation helper
	/// </summary>
	void animateInitialEntry();

	/// <summary>
	/// Helper animation functions
	/// </summary>
	void animateOutOnTop();

	/// <summary>
	/// Helper animation functions
	/// </summary>
	void animateOutOnBottom();

	/// <summary>
	/// Removes or loads the current visible texture list
	/// </summary>
	void loadAppropriateTextures();

	/// <summary>
	/// Adds appropriate objects into the tracking list from scene
	/// </summary>
	virtual void addObjectsToTracking( ObjectArray* trackingList );

protected:
	/// <summary>
	/// Returns if slider is at the edge bounds
	/// </summary>
	bool canMoveLeft();

	/// <summary>
	/// Returns if slider is at the edge bounds
	/// </summary>
	bool canMoveRight();

	/// <summary>
	/// Animates slider elements going Left
	/// </summary>
	virtual void positionElements();

	/// <summary>
	/// Animates slider elements going left
	/// </summary>
	virtual void moveSliderLeft();

	/// <summary>
	/// Animates slider elements going right
	/// </summary>
	virtual void moveSliderRight();

	SliderType sliderType;

	int selectedIndex;
	GameObject* sliderLeft;
	GameObject* sliderRight;
	ObjectArray sliderElements;
};




}

#endif


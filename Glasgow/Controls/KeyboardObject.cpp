#include "KeyboardObject.h"

#include "SIEngine/Include/SIObjects.h"
#include "SIEngine/Include/SIRendering.h"
#include "SIEngine/Resources/FontData.h"
#include "Glasgow/Controls/TextButton.h"

using namespace SICollections;
using namespace SIActions;
using namespace SIGeometry;
using namespace SIUtils;
using namespace SICore;

namespace Glasgow
{

KeyboardObject::KeyboardObject(Scene* owner, bool showSymbolKeyboard) : super()
{
	isShowingSymbols = showSymbolKeyboard;
	maximumCharacters = 10;
	TextureManager::getInstance()->loadTextureSheet("keyboard");
	Size buttonSize = Size(90, 85);
	FontData *font = new FontData("Montserrat-LF-24");
	// Create the character buttons
	SpriteData *charButtonNormal = TextureManager::getInstance()->getSpriteData("keyboard-btnNormal");
	SpriteData *charButtonSelected = TextureManager::getInstance()->getSpriteData("keyboard-btnSelected");

	Color normalColor = Color(0.25f, 0.25f, 0.25f, 1.0f);
	Color selectedColor = Color((float)0xA4/(float)0xFF, (float)0xEF/(float)0xFF, (float)0x35/(float)0xFF);

	trackedObjects = new ObjectArray();
	owningScene = owner;
	selectedGlyph = null;
	text = "";
	currentGroup = GlyphGroupNone;
	isLeadingCaps = false;

	// Create the selection buttons

	Size switcherSize = Size(110, 110);

	// Uppercase switcher button

	SpriteObject* uppercaseNormal = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swUppercaseNormal"));
	uppercaseNormal->setSize(switcherSize);
	SpriteObject* uppercaseSelected = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swUppercaseSelected"));
	uppercaseSelected->setSize(switcherSize);

	uppercaseButton = new ButtonObject();
	uppercaseButton->setBackground( uppercaseNormal, NULL, NULL, uppercaseSelected );
	uppercaseButton->setState( ButtonStateNormal );
	uppercaseNormal->release();
	uppercaseSelected->release();

	addChild(uppercaseButton);

	uppercaseButton->name = "UpperCaseButton";
	uppercaseButton->release();

	// Lowercase switcher button
	SpriteObject* lowercaseNormal = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swLowercaseNormal"));
	lowercaseNormal->setSize(switcherSize);
	SpriteObject* lowercaseSelected = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swLowercaseSelected"));
	lowercaseSelected->setSize(switcherSize);

	lowercaseButton = new ButtonObject();
	lowercaseButton->setBackground(lowercaseNormal, NULL, NULL, lowercaseSelected);
	lowercaseButton->setState(ButtonStateNormal);
	lowercaseNormal->release();
	lowercaseSelected->release();

	addChild(lowercaseButton);
	lowercaseButton->name = "LowerCaseButton";
	lowercaseButton->release();

	// Decimal switcher button
	SpriteObject* decimalNormal = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swDecimalNormal"));
	decimalNormal->setSize(switcherSize);
	SpriteObject* decimalSelected = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swDecimalSelected"));
	decimalSelected->setSize(switcherSize);

	decimalButton = new ButtonObject();
	decimalButton->setBackground(decimalNormal, NULL, NULL, decimalSelected);
	decimalButton->setState(ButtonStateNormal);
	decimalNormal->release();
	decimalSelected->release();

	addChild(decimalButton);
	decimalButton->name = "DecimalButton";
	decimalButton->release();

	// Symbol switcher button
	SpriteObject* symbolNormal = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swSymbolNormal"));
	symbolNormal->setSize(switcherSize);
	SpriteObject* symbolSelected = new SpriteObject(TextureManager::getInstance()->getSpriteData("keyboard-swSymbolSelected"));
	symbolSelected->setSize(switcherSize);
	symbolButton = new ButtonObject();

	symbolButton->setBackground(symbolNormal, NULL, NULL, symbolSelected);
	symbolButton->setState(ButtonStateNormal);
	symbolNormal->release();
	symbolSelected->release();

	addChild(symbolButton);
	if(!isShowingSymbols)
		symbolButton->setVisible(false);

	symbolButton->name = "SymbolButton";
	symbolButton->release();

	uppercaseButton->setPosition(Vec3f(-540,140, -300));
	lowercaseButton->setPosition(Vec3f(-540,50, -300));
	decimalButton->setPosition(Vec3f(-540,-40, -300));
	symbolButton->setPosition(Vec3f(-540,-130, -300));


	glyphsUpperCase = new GameObject();
	glyphsUpperCase->name = "UpperCaseGlyphs";
	String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	int x = 0, y = 0;

	for (int i = 0; i < characters.length(); i++)
	{
		SpriteObject *buttonNormal = new SpriteObject(charButtonNormal);
		buttonNormal->setSize(buttonSize);

		SpriteObject *buttonSelected = new SpriteObject(charButtonSelected);
		buttonSelected->setSize(buttonSize);

		TextLabel *charLabel = new TextLabel(font, S(characters.charAt(i)), TextAlignmentCenter, TextAnchorMiddleCenter);
		charLabel->setPosition(Vec3f(0,5,-10));

		TextButton *buttonGlyph = new TextButton(normalColor, selectedColor, charLabel, buttonNormal, buttonSelected);
		buttonGlyph->name = characters.charAt(i);

		if( i == 0 )
		{
			buttonGlyph->setState(ButtonStateActive);
			selectedGlyph = buttonGlyph;
		}

		buttonNormal->release();
		buttonSelected->release();
		glyphsUpperCase->addChild(buttonGlyph);
		buttonGlyph->release();
		buttonGlyph->setPosition(Vec3f(x,y, -10));

		// Advance and return line
		x += 88;
		if (x >= 528) {
			x = 0;
			y -= 88;
		}
	}
	SpriteData *spaceData = TextureManager::getInstance()->getSpriteData("keyboard-iconSpaceNormal");
	SpriteData *backspaceData = TextureManager::getInstance()->getSpriteData("keyboard-iconBackspaceNormal");

	SpriteData *spaceSelectedData = TextureManager::getInstance()->getSpriteData("keyboard-iconSpaceSelected");
	SpriteData *backspaceSelectedData = TextureManager::getInstance()->getSpriteData("keyboard-iconBackspaceSelected");


	/* Space key */
	ButtonObject *spGlyph = new ButtonObject();
	spGlyph->name = "Space";

	SpriteObject *space = new SpriteObject(spaceData);
	space->setSize(buttonSize);
	SpriteObject *spaceSelected = new SpriteObject(spaceSelectedData);
	spaceSelected->setSize(buttonSize);

	spGlyph->setBackground(space, NULL, NULL, spaceSelected);

	glyphsUpperCase->addChild(spGlyph);
	spGlyph->setPosition(Vec3f(528 - 88 - 88, y, -10));
	spGlyph->setState(ButtonStateNormal);
	spGlyph->release();
	space->release();
	spaceSelected->release();

	/* Backspace key */
	ButtonObject *bkspGlyph = new ButtonObject();
	bkspGlyph->name = "Backspace";

	SpriteObject *backspace = new SpriteObject(backspaceData);
	backspace->setSize(buttonSize);
	SpriteObject *backspaceSelected = new SpriteObject(backspaceSelectedData);
	backspaceSelected->setSize(buttonSize);

	bkspGlyph->setBackground(backspace, NULL, NULL, backspaceSelected);

	glyphsUpperCase->addChild(bkspGlyph);
	bkspGlyph->setPosition(Vec3f(528 - 88,y, -10));
	bkspGlyph->setState(ButtonStateNormal);
	bkspGlyph->release();
	backspace->release();
	backspaceSelected->release();

	addChild(glyphsUpperCase);
	glyphsUpperCase->setPosition(Vec3f(-400,140,-500));
	glyphsUpperCase->release();

	glyphsLowerCase = new GameObject();
	glyphsLowerCase->name = "LowerCaseGlyphs";
	characters = "abcdefghijklmnopqrstuvwxyz";

	x = 0, y = 0;

	for (int i = 0; i < characters.length(); i++)
	{
		SpriteObject *buttonNormal = new SpriteObject(charButtonNormal);
		buttonNormal->setSize(buttonSize);

		SpriteObject *buttonSelected = new SpriteObject(charButtonSelected);
		buttonSelected->setSize(buttonSize);

		TextLabel *charLabel = new TextLabel(font, S(characters.charAt(i)), TextAlignmentCenter, TextAnchorMiddleCenter);
		charLabel->setPosition(Vec3f(0,5,-10));

		TextButton *buttonGlyph = new TextButton(normalColor, selectedColor, charLabel, buttonNormal, buttonSelected);
		buttonGlyph->name = characters.charAt(i);

		buttonNormal->release();
		buttonSelected->release();
		glyphsLowerCase->addChild(buttonGlyph);
		buttonGlyph->release();
		buttonGlyph->setPosition(Vec3f(x,y,-10));

		// Advance and return line
		x += 88;
		if (x >= 528) {
			x = 0;
			y -= 88;
		}
	}
	/* Space key */
	spGlyph = new ButtonObject();
	spGlyph->name = "Space";

	space = new SpriteObject(spaceData);
	space->setSize(buttonSize);
	spaceSelected = new SpriteObject(spaceSelectedData);
	spaceSelected->setSize(buttonSize);

	spGlyph->setBackground(space, NULL, NULL, spaceSelected);

	glyphsLowerCase->addChild(spGlyph);
	spGlyph->setPosition(Vec3f(528 - 88 - 88, y, -10));
	spGlyph->setState(ButtonStateNormal);
	spGlyph->release();
	space->release();
	spaceSelected->release();

	/* Backspace key */
	bkspGlyph = new ButtonObject();
	bkspGlyph->name = "Backspace";

	backspace = new SpriteObject(backspaceData);
	backspace->setSize(buttonSize);
	backspaceSelected = new SpriteObject(backspaceSelectedData);
	backspaceSelected->setSize(buttonSize);

	bkspGlyph->setBackground(backspace, NULL, NULL, backspaceSelected);

	glyphsLowerCase->addChild(bkspGlyph);
	bkspGlyph->setPosition(Vec3f(528 - 88,y,0));
	bkspGlyph->setState(ButtonStateNormal);
	bkspGlyph->release();
	backspace->release();
	backspaceSelected->release();

	addChild(glyphsLowerCase);
	glyphsLowerCase->setPosition(Vec3f(-400,140,-500));
	glyphsLowerCase->release();

	glyphsDecimal = new GameObject();
	glyphsDecimal->name = "DecimalGlyphs";
	characters = "0123456789";

	x = 0, y = 0;

	for (int i = 0; i < characters.length(); i++)
	{
		SpriteObject *buttonNormal = new SpriteObject(charButtonNormal);
		buttonNormal->setSize(buttonSize);

		SpriteObject *buttonSelected = new SpriteObject(charButtonSelected);
		buttonSelected->setSize(buttonSize);

		TextLabel *charLabel = new TextLabel(font, S(characters.charAt(i)), TextAlignmentCenter, TextAnchorMiddleCenter);
		charLabel->setPosition(Vec3f(0,5,-10));

		TextButton *buttonGlyph = new TextButton(normalColor, selectedColor, charLabel, buttonNormal, buttonSelected);
		buttonGlyph->name = characters.charAt(i);

		buttonNormal->release();
		buttonSelected->release();
		glyphsDecimal->addChild(buttonGlyph);
		buttonGlyph->release();
		buttonGlyph->setPosition(Vec3f(x,y,-10));

		// Advance and return line
		x += 88;
		if (x >= 528) {
			x = 0;
			y -= 88;
		}
	}

	/* Space key */
	/* Space key */
	spGlyph = new ButtonObject();
	spGlyph->name = "Space";

	space = new SpriteObject(spaceData);
	space->setSize(buttonSize);
	spaceSelected = new SpriteObject(spaceSelectedData);
	spaceSelected->setSize(buttonSize);

	spGlyph->setBackground(space, NULL, NULL, spaceSelected);

	glyphsDecimal->addChild(spGlyph);
	spGlyph->setPosition(Vec3f(528 - 88 - 88, y, -10));
	spGlyph->setState(ButtonStateNormal);
	spGlyph->release();
	space->release();
	spaceSelected->release();

	/* Backspace key */
	bkspGlyph = new ButtonObject();
	bkspGlyph->name = "Backspace";

	backspace = new SpriteObject(backspaceData);
	backspace->setSize(buttonSize);
	backspaceSelected = new SpriteObject(backspaceSelectedData);
	backspaceSelected->setSize(buttonSize);

	bkspGlyph->setBackground(backspace, NULL, NULL, backspaceSelected);

	glyphsDecimal->addChild(bkspGlyph);
	bkspGlyph->setPosition(Vec3f(528 - 88,y, -10));
	bkspGlyph->setState(ButtonStateNormal);
	bkspGlyph->release();
	backspace->release();
	backspaceSelected->release();

	addChild(glyphsDecimal);
	glyphsDecimal->setPosition(Vec3f(-400,140,-500));
	glyphsDecimal->release();
	glyphsSymbols = new GameObject();
	glyphsSymbols->name = "SymbolsGlyphs";
	characters = "@#_+-=!\"$%^&*()/?|\\~`{}[]:;'<>.,";

	x = 0, y = 0;

	for (int i = 0; i < characters.length(); i++)
	{
		SpriteObject *buttonNormal = new SpriteObject(charButtonNormal);
		buttonNormal->setSize(buttonSize);

		SpriteObject *buttonSelected = new SpriteObject(charButtonSelected);
		buttonSelected->setSize(buttonSize);

		TextLabel *charLabel = new TextLabel(font, S(characters.charAt(i)), TextAlignmentCenter, TextAnchorMiddleCenter);
		charLabel->setPosition(Vec3f(0,5,0));

		TextButton *buttonGlyph = new TextButton(normalColor, selectedColor, charLabel, buttonNormal, buttonSelected);
		buttonGlyph->name = characters.charAt(i);

		buttonNormal->release();
		buttonSelected->release();
		glyphsSymbols->addChild(buttonGlyph);
		buttonGlyph->release();
		buttonGlyph->setPosition(Vec3f(x,y,-10));

		// Advance and return line
		x += 88;
		if (x >= 528)
		{
			x = 0;
			y -= 72;
		}
	}

	/* Space key */
	spGlyph = new ButtonObject();
	spGlyph->name = "Space";

	space = new SpriteObject(spaceData);
	space->setSize(buttonSize);
	spaceSelected = new SpriteObject(spaceSelectedData);
	spaceSelected->setSize(buttonSize);

	spGlyph->setBackground(space, NULL, NULL, spaceSelected);

	glyphsSymbols->addChild(spGlyph);
	spGlyph->setPosition(Vec3f(528 - 88 - 88, y, -10));
	spGlyph->setState(ButtonStateNormal);
	spGlyph->release();
	space->release();
	spaceSelected->release();

	/* Backspace key */
	bkspGlyph = new ButtonObject();
	bkspGlyph->name = "Backspace";

	backspace = new SpriteObject(backspaceData);
	backspace->setSize(buttonSize);
	backspaceSelected = new SpriteObject(backspaceSelectedData);
	backspaceSelected->setSize(buttonSize);

	bkspGlyph->setBackground(backspace, NULL, NULL, backspaceSelected);

	glyphsSymbols->addChild(bkspGlyph);
	bkspGlyph->setPosition(Vec3f(528 - 88,y,-10));
	bkspGlyph->setState(ButtonStateNormal);
	bkspGlyph->release();
	backspace->release();
	backspaceSelected->release();

	addChild(glyphsSymbols);
	if(!isShowingSymbols)
		glyphsSymbols->setVisible(false);
	glyphsSymbols->setPosition(Vec3f(-400,140,-500));
	glyphsSymbols->release();

	setGlyphGroup(GlyphGroupUpperCase);


	uppercaseButton->setBreathe(false);
	lowercaseButton->setBreathe(false);
	decimalButton->setBreathe(false);
	symbolButton->setBreathe(false);

}

KeyboardObject::~KeyboardObject()
{
	SAFE_RELEASE(trackedObjects);
}

void KeyboardObject::setGlyphGroup(GlyphGroup group)
{
	if(group == GlyphGroupSymbols && !isShowingSymbols)
		return;

	currentGroup = group;
	uppercaseButton->setState(ButtonStateNormal);
	lowercaseButton->setState(ButtonStateNormal);
	decimalButton->setState(ButtonStateNormal);
	symbolButton->setState(ButtonStateNormal);

	if (group == GlyphGroupUpperCase)
	{
		uppercaseButton->setState(ButtonStateActive);
		glyphsUpperCase->setVisible(true);
		glyphsLowerCase->setVisible(false);
		glyphsDecimal->setVisible(false);
		glyphsSymbols->setVisible(false);
	}
	else if (group == GlyphGroupLowerCase)
	{
		lowercaseButton->setState(ButtonStateActive);
		glyphsUpperCase->setVisible(false);
		glyphsLowerCase->setVisible(true);
		glyphsDecimal->setVisible(false);
		glyphsSymbols->setVisible(false);
	}
	else if (group == GlyphGroupDecimal)
	{
		decimalButton->setState(ButtonStateActive);
		glyphsUpperCase->setVisible(false);
		glyphsLowerCase->setVisible(false);
		glyphsDecimal->setVisible(true);
		glyphsSymbols->setVisible(false);
	}
	else if (group == GlyphGroupSymbols)
	{
		symbolButton->setState(ButtonStateActive);
		glyphsUpperCase->setVisible(false);
		glyphsLowerCase->setVisible(false);
		glyphsDecimal->setVisible(false);
		glyphsSymbols->setVisible(true);
	}

	setupHotSpots();
}

GameObject* KeyboardObject::getDefaultHighLight()
{
	return selectedGlyph;
}

void KeyboardObject::reset()
{
	if(selectedGlyph)
		((ButtonObject*)selectedGlyph)->setState(ButtonStateNormal);

	selectedGlyph = NULL;
	text = "";
	currentGroup = GlyphGroupNone;
	isLeadingCaps = false;

	setGlyphGroup(GlyphGroupUpperCase);
	selectedGlyph = (ButtonObject*)glyphsUpperCase->getChildren()->elementAt(0);
	((ButtonObject*)selectedGlyph)->setState(ButtonStateActive);
}

void KeyboardObject::appendCharacter(String c)
{
	text += c;
}

void KeyboardObject::backspace()
{
	text = text.removeLastChar();
}


void KeyboardObject::setText(String text_)
{
	text = text_;
}

String KeyboardObject::getText()
{
	return text;
}

void KeyboardObject::setLeadingCaps(bool value)
{
	isLeadingCaps = value;
}

bool KeyboardObject::getLeadingCaps()
{
	return isLeadingCaps;
}


void KeyboardObject::onButtonPressBack()
{
	if(text.length() > 0)
	{ backspace(); }

	removeTrackedObjects();
}

void KeyboardObject::removeTrackedObjects()
{
	owningScene->removeTrackedObjects(trackedObjects);
}

//************************************************************************************
// A button is pressed
//************************************************************************************
bool KeyboardObject::onButtonPressAction()
{
	if( !isVisibleHierarchy() )
		return false;

	if( selectedGlyph == NULL )
		return false;

	if(selectedGlyph->descendantOf(this))
	{
		if (selectedGlyph->name == "Backspace" && text.length() > 0)
		{
			backspace();
			return true;
		}
		else if (text.length() < maximumCharacters -1 && selectedGlyph->name == "Space" )
		{
			if(text.isEmpty())
				return false;

			appendCharacter(" ");
			return true;
		}
		else if ((text.length() < maximumCharacters) && selectedGlyph->name.length() == 1)
		{
			appendCharacter(selectedGlyph->name);
			if(isLeadingCaps && text.length() == 1)
			{
				isLeadingCaps = false;
				if(currentGroup == GlyphGroupUpperCase)
				{
					((ButtonObject*)selectedGlyph)->setState(ButtonStateNormal);
					setGlyphGroup(GlyphGroupLowerCase);
					selectedGlyph = (ButtonObject*)glyphsLowerCase->getChildren()->elementAt(0);
					((ButtonObject*)selectedGlyph)->setState(ButtonStateActive);
				}

			}
			return true;
		}
	}

	return false;
}

//************************************************************************************
// Base scene feature: setup objects to track for wand input and analog stick
//************************************************************************************
void KeyboardObject::setupHotSpots()
{
	trackedObjects->removeAllElements();

	if (currentGroup == GlyphGroupUpperCase)
	{ trackedObjects->addElementsFromArray(glyphsUpperCase->getChildren()); }
	else if (currentGroup == GlyphGroupLowerCase)
	{ trackedObjects->addElementsFromArray(glyphsLowerCase->getChildren()); }
	else if (currentGroup == GlyphGroupDecimal)
	{ trackedObjects->addElementsFromArray(glyphsDecimal->getChildren()); }
	else if (currentGroup == GlyphGroupSymbols && isShowingSymbols)
	{ trackedObjects->addElementsFromArray(glyphsSymbols->getChildren()); }

	trackedObjects->addElement(uppercaseButton);
	trackedObjects->addElement(lowercaseButton);
	trackedObjects->addElement(decimalButton);
	if(isShowingSymbols)
		trackedObjects->addElement(symbolButton);

	//add new tracked objects
	owningScene->addTrackedObjects(trackedObjects);
}

//************************************************************************************
// Sets maximum characters that can be typed in
//************************************************************************************
void KeyboardObject::setMaxCharacters( int maxLength )
{ maximumCharacters = maxLength; }

//************************************************************************************
// Returns the last typed character if actionPress returns true
//************************************************************************************
String KeyboardObject::typedCharacter()
{
	return "";
}

bool KeyboardObject::isButtonGlyphGroup(ButtonObject* button)
{
	if( button == uppercaseButton )
		return true;
	if( button == lowercaseButton )
		return true;
	if ( button == decimalButton )
		return true;
	if ( button == symbolButton )
		return true;

	return false;
}


//************************************************************************************
// Respond to scene's selection changes
//************************************************************************************
bool KeyboardObject::onSelectionChange( GameObject* oldSelection, GameObject* newSelection )
{
	bool handled = false;

	if( oldSelection != NULL  && newSelection != NULL )
	{
		//inside to out
		if( oldSelection->descendantOf(this) && !newSelection->descendantOf(this) )
		{
			owningScene->setCurrentHighlight(newSelection);

			if( isButtonGlyphGroup((ButtonObject*)oldSelection) )
			{
				((ButtonObject*)oldSelection)->setState(ButtonStateActive);
				((ButtonObject*)oldSelection)->setBreathe(false);
			}

			return false;
		}

		//outside in!
		if( !oldSelection->descendantOf(this) && newSelection->descendantOf(this) )
		{
			((ButtonObject*)oldSelection)->setState(ButtonStateNormal);

			if( isButtonGlyphGroup((ButtonObject*)newSelection) )
			{
				uppercaseButton->setState(ButtonStateNormal);
				lowercaseButton->setState(ButtonStateNormal);
				decimalButton->setState(ButtonStateNormal);
				symbolButton->setState(ButtonStateNormal);
				owningScene->setCurrentHighlight(newSelection);

				if (newSelection == uppercaseButton)
				{ setGlyphGroup(GlyphGroupUpperCase); }
				else if (newSelection == lowercaseButton)
				{ setGlyphGroup(GlyphGroupLowerCase); }
				else if (newSelection == decimalButton)
				{ setGlyphGroup(GlyphGroupDecimal); }
				else if (newSelection == symbolButton)
				{ setGlyphGroup(GlyphGroupSymbols); }
				else
				{ setGlyphGroup(currentGroup); }

				((ButtonObject*)newSelection)->setState(ButtonStateActive);
				((ButtonObject*)newSelection)->setBreathe(true);
			}
			else
			{
				selectedGlyph = newSelection;
			}

			return false;
		}

		if( oldSelection->descendantOf(this) && newSelection->descendantOf(this) )
		{
			if( isButtonGlyphGroup((ButtonObject*)oldSelection) )
			{
				((ButtonObject*)oldSelection)->setState(ButtonStateNormal);
				((ButtonObject*)oldSelection)->setBreathe(false);
			}

			if( isButtonGlyphGroup((ButtonObject*)newSelection) )
			{
				((ButtonObject*)newSelection)->setState(ButtonStateActive);
				((ButtonObject*)newSelection)->setBreathe(true);
			}

			owningScene->setCurrentHighlight(newSelection);
			selectedGlyph = newSelection;

			GlyphGroup prevGroup = currentGroup;
			if (newSelection == uppercaseButton)
			{ setGlyphGroup(GlyphGroupUpperCase); }
			else if (newSelection == lowercaseButton)
			{ setGlyphGroup(GlyphGroupLowerCase); }
			else if (newSelection == decimalButton)
			{ setGlyphGroup(GlyphGroupDecimal); }
			else if (newSelection == symbolButton)
			{ setGlyphGroup(GlyphGroupSymbols); }
			else
			{ setGlyphGroup(currentGroup); }

			if(prevGroup != currentGroup)
				isLeadingCaps = false;

			return true;
		}

		return false;
	}
}

}

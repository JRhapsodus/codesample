#include "DownloadItem.h"

namespace Glasgow
{

DownloadItem::DownloadItem(
		SpriteObject* active_,
		SpriteObject* background_,
		ObjectArray* arrayOfDlIcons_,
		TextLabel* errorLabel_,
		TextLabel* titleLabel_,
		SpriteObject* gameIcon_,
		TextLabel* storageLabel_ ) : super()
{
	scaleOnActiveAmount = 0.04f;
	dlProgressIcons = arrayOfDlIcons_;
	dlProgressIcons->addRef();
	for( int i =0  ;i<dlProgressIcons->getSize(); i++ )
	{ addChild((SpriteObject*)dlProgressIcons->elementAt(i)); }
	addChild(active_);
	addChild(background_);
	addChild(errorLabel_);
	addChild(titleLabel_);
	addChild(gameIcon_);
	addChild(storageLabel_);

	active = active_;
	active->setVisible(false);
	gameIcon = gameIcon_;
	errorLabel = errorLabel_;
	errorLabel->setText("");
	storageLabel = storageLabel_;
	storageLabel->setText("");
	titleLabel  = titleLabel_;
	background = background_;

	storageLabel->setVisible(false);
	packageInfo = null;
}

DownloadItem::~DownloadItem()
{
	printf("DownloadItem::~DownloadItem()\n");
	SAFE_RELEASE(packageInfo);
}

void DownloadItem::setTitle( String title )
{
	titleLabel->setText( title );
}

void DownloadItem::setStorage( String storage )
{
	storageLabel->setText( storage );
}

/// <summary>
/// Sets background
/// </summary>
void DownloadItem::setBackGroundImage( String texture )
{
	if( AssetLibrary::fileExists(texture) )
	{ gameIcon->getSpriteData()->setTexture(texture); }
}

void DownloadItem::setPackageInfo(PackageInfo* info)
{
	packageInfo = info;
	packageInfo->addRef();
	setTitle(info->label);
	setBackGroundImage( packageInfo->iconPath );
	if( info->isVideoBundle )
	{ setTitle(  ((VideoInfo*)info->videos.elementAt(0))->bundleTitle ); }
}

void DownloadItem::setError( String error )
{
	errorLabel->setText( error );
}

void DownloadItem::update( float dt )
{
	if( packageInfo != null )
	{
		tPackageStatus packageStatus = packageInfo->getPackageStatus();

		if( packageStatus == kPackageStatus_Pending )
		{ errorLabel->setText("Queued"); }
		else if ( packageStatus == kPackageStatus_Downloading )
		{ errorLabel->setText("Downloading"); }
		else if ( packageStatus == kPackageStatus_Installing )
		{ errorLabel->setText("Installing"); }
		else if ( packageStatus == kPackageStatus_Installed )
		{ errorLabel->setText("Installed"); }
		else if ( packageStatus == kPackageStatus_Removing )
		{ errorLabel->setText("Uninstalling"); }
		else if ( packageStatus == kPackageStatus_Removed )
		{ errorLabel->setText("Removed"); }
		else
		{ errorLabel->setText("Error"); }
	}

	super::update(dt);
}

void DownloadItem::transition( TransitionState transition )
{
	if (transition == NormalToActive) {
		active->setVisible(true);
	} else if (transition == ActiveToNormal)
	{
		totalTimeElapsed = 0;
		scaleToOriginal(0.8f);
		active->setVisible(false);
	}
}

//*******************************************************************************************************************
//Roll Over
//	backing
//	rollover Object
//*******************************************************************************************************************
DownloadItem* DownloadItem::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> active((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> background((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<ObjectArray> download( new ObjectArray());
	for ( int i = 0; i<10; i++ )
	{ download->addElement((SpriteObject*)Scene::deSerializeNext(fp)); }
	TRelease<TextLabel> errorLabel((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> titleLabel((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> gameIcon((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> storageLabel((TextLabel*)Scene::deSerializeNext(fp));

	return new DownloadItem(active,  background, download, errorLabel,
			titleLabel, gameIcon, storageLabel );
}

}

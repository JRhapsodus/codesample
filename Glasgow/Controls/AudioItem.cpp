#include "AudioItem.h"

namespace Glasgow
{

AudioItem::AudioItem(SpriteObject* active_, SpriteObject* icon_, TextLabel* nameArtist,
		SpriteObject* nameplate, TextLabel* nameTitle, SpriteObject* rim_, String songToUse) : super()
{
	addChild(active_);
	addChild(icon_);
	addChild(nameArtist);
	addChild(nameplate);
	addChild(nameTitle);
	addChild(rim_);

	rim_->setVisible(false);
	audioName = songToUse;
	active = active_;
	active->setVisible(false);
	setState(ButtonStateNormal);
}

AudioItem::~AudioItem() {}


void AudioItem::update( float dt )
{
	//***************************************************************
	// Lookat the backward's direction torwards the camera
	//***************************************************************
	Vec3f localUp = getParent()->transform.getWorldToLocal()*(Vec3f::Up);
	float angle = Vec3f::Up.angleBetween(localUp);
	transform.setRotation(Quaternion::angleAxis(angle*57.2957795f, Vec3f::Right));
	active->setRotation( active->getRotation()*Quaternion::angleAxis(10*dt, Vec3f::Forward));
	super::update(dt);
}

String AudioItem::getAudioName()
{
	return audioName;
}

void AudioItem::transition( TransitionState transition )
{
	if( transition == NormalToActive )
	{
		totalTimeElapsed = 0;
		active->cancelActions();
		active->setVisible(true);
		active->setOpacity(0);
		active->fadeTo(1, 0.25f);
	}
	else if ( transition == ActiveToNormal )
	{
		totalTimeElapsed = 0;
		active->cancelActions();
		active->fadeTo(0, 0.25f);
		active->setVisible(false);
		scaleTo( originalScale, 0.1f );
	}
}

AudioItem* AudioItem::deSerialize(FILE* fp)
{
	char songName[250];
	if( fscanf(fp, "%249s", songName) == 1 )
	{
		TRelease<SpriteObject> a((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> i((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<TextLabel> nl((TextLabel*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> np((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<TextLabel> pi((TextLabel*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> r((SpriteObject*)Scene::deSerializeNext(fp));
		return new AudioItem( a, i, nl, np, pi, r, songName);
	}

	ThrowException::deserializeException();

}

}

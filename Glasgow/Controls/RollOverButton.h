#ifndef RollOver_H_
#define RollOver_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

/// <summary> 
/// This is a button that scales in a roll over object on active state
/// </summary> 
class RollOverButton : public ButtonObject
{
	CLASSEXTENDS(RollOverButton, ButtonObject);

public:
	RollOverButton(SpriteObject* normalBacking, TextLabel* rollOver );
	virtual ~RollOverButton();

	static RollOverButton* deSerialize(FILE* fp);
	virtual void transition(TransitionState transition);

private:
	Vec3f originalRollOverScale;
	TextLabel* rollOver;
};

}

#endif /* TEXTBUTTON_H_ */

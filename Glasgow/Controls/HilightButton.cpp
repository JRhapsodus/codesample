#include "HilightButton.h"

namespace Glasgow
{

HilightButton::HilightButton(SpriteObject* normalBacking, SpriteObject* rimNormal, SpriteObject* selected  ) : super()
{
	rimSelected = selected;
	rim = rimNormal;

	addChild(normalBacking);
	addChild(rim);
	addChild(selected);

	rimSelected->setVisible(false);
	setState(ButtonStateNormal);
	originalLocalPosition = Vec3f::Max;
}

HilightButton::~HilightButton()
{ }


//*******************************************************************************************************************
// Handle response for animations
//*******************************************************************************************************************
void HilightButton::transition( TransitionState transition )
{
	super::transition(transition);

	if( transition == NormalToActive )
	{
		totalTimeElapsed = 0;
		rimSelected->cancelActions();
		rimSelected->setColor( Color::WhiteClear );
		rimSelected->setOpacity(0);
		rimSelected->setVisible(true);
		rimSelected->fadeTo(1, 0.35f);
	}
	else if ( transition == ActiveToNormal )
	{
		rimSelected->cancelActions();
		rimSelected->fadeTo(0, 0.35f);
	}
}

//*******************************************************************************************************************
//Roll Over
//	backing
//	rollover Object
//*******************************************************************************************************************
HilightButton* HilightButton::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> normalBacking((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> rim((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> rimSelected((SpriteObject*)Scene::deSerializeNext(fp));
	return new HilightButton(normalBacking, rim, rimSelected);
}

}

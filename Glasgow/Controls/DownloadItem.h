#ifndef DownloadItem_H
#define DownloadItem_H

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

/// <summary> 
///
/// </summary> 
class DownloadItem : public ButtonObject
{
	CLASSEXTENDS(DownloadItem, ButtonObject);

public:
	DownloadItem( 
		SpriteObject* active,
		SpriteObject* background,
		ObjectArray* arrayOfDlIcons,
		TextLabel* errorLabel,
		TextLabel* titleLabel,
		SpriteObject* gameIcon,
		TextLabel* storageLabel );
	virtual ~DownloadItem();

	/// <summary>
	/// Sets background
	/// </summary>
	void setBackGroundImage( String texture );
	void setTitle( String title );
	void setStorage( String storage );
	void setError( String error );
	void setPackageInfo( PackageInfo* info);
	static DownloadItem* deSerialize(FILE* fp);

	virtual void update( float dt );

	/// <summary> 
	/// Transition states from one state to other for subclass animations
	/// </summary> 
	virtual void transition( TransitionState transition );
private:
	SpriteObject* active;
	SpriteObject* background;
	ObjectArray* dlProgressIcons;;
	TextLabel* errorLabel;
	TextLabel* titleLabel;
	ButtonObject* download;
	SpriteObject* gameIcon;
	TextLabel* storageLabel;
	PackageInfo* packageInfo;
};

}
#endif /* TEXTBUTTON_H_ */

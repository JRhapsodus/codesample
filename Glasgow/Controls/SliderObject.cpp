#include "SliderObject.h"
#include "SIEngine/Include/SIScenes.h"
#include "Glasgow/Controls/AudioItem.h"
#include "Glasgow/Controls/AvatarItem.h"
#include "Glasgow/Controls/ThemeItem.h"
#include "Glasgow/Controls/GalleryItem.h"
#include "Glasgow/Data/VideoInfo.h"

using namespace SICore;

namespace Glasgow
{

SliderObject::SliderObject( SpriteObject* left, SpriteObject* right ) :
		super()
{
	sliderType = SliderTypeAvatar;
	sliderLeft = left;
	sliderRight = right;
	selectedIndex = -1;
	sliderLeft->setOpacity(0);
	sliderRight->setOpacity(0);
	addChild(sliderLeft);
	addChild(sliderRight);
}

SliderObject::~SliderObject()
{
	printf("SliderObject::~SliderObject()\n");
	if( sliderType == SliderTypeVideo )
	{
		for( int i = 0; i<sliderElements.getSize(); i++ )
		{
			((GalleryItem*)sliderElements[i])->unloadTexture();
			((GalleryItem*)sliderElements[i])->removeFromParent();
		}
	}


	sliderElements.removeAllElements();

}

//Re-intialization
void SliderObject::loadGameObjectState(GameObjectState state)
{
	super::loadGameObjectState(state);

	if( sliderType == SliderTypeVideo )
	{
		for( int i = 0; i<sliderElements.getSize(); i++ )
		{
			((GalleryItem*)sliderElements[i])->unloadTexture();
			((GalleryItem*)sliderElements[i])->removeFromParent();
		}
	}

	sliderElements.removeAllElements();
	selectedIndex = -1;
	sliderLeft->setOpacity(0);
	sliderRight->setOpacity(0);
}


/// <summary>
/// Load from file
/// </summary>
SliderObject* SliderObject::deSerialize( FILE* fp )
{
	TRelease<SpriteObject> l((SpriteObject*) Scene::deSerializeNext(fp));
	TRelease<SpriteObject> r((SpriteObject*) Scene::deSerializeNext(fp));
	return new SliderObject(l, r);
}

/// <summary>
/// Returns the selected item
/// </summary>
GameObject* SliderObject::getSelectedItem()
{
	if ( selectedIndex == -1 )
	{
		return NULL;
	}
	return (GameObject*) sliderElements[selectedIndex];
}

/// <summary>
/// Positions all the elements
/// </summary>
void SliderObject::positionElements()
{
	if ( selectedIndex == -1 )
		return;

	static float slideTime = 0.45f;

	for ( int i = 0; i < sliderElements.getSize(); i++ )
	{
		if ( sliderElements[i] != null )
		{ ((GameObject*) sliderElements[i])->cancelActions(); }

		if( sliderType == SliderTypeVideo )
		{
			((GalleryItem*)sliderElements[i])->setCentered(false);
			((ButtonObject*)sliderElements[i])->setState(ButtonStateNormal);
		}
	}

	if( sliderType == SliderTypeVideo )
	{

		if( sliderElements.getSize() == 2 )
		{
			if( selectedIndex == 0 )
			{
				((GameObject*)sliderElements[0])->moveTo(Vec3f(-90.0f, 0, 25), slideTime);
				((GameObject*)sliderElements[1])->moveTo(Vec3f(160.0f, 0, 100), slideTime);
			}
			else
			{
				((GameObject*)sliderElements[0])->moveTo(Vec3f(-160.0f, 0, 100), slideTime);
				((GameObject*)sliderElements[1])->moveTo(Vec3f(90.0f, 0, 25), slideTime);

			}

			((GalleryItem*)sliderElements[selectedIndex])->setCentered(true);
			((ButtonObject*)sliderElements[selectedIndex])->setState(ButtonStateActive);

		}
		else if ( sliderElements.getSize() == 3 )
		{
			if( selectedIndex == 1 )
			{
				if (sliderElements[selectedIndex - 1] != null && (selectedIndex - 1) >= 0)
				{ ((GameObject*)sliderElements[selectedIndex - 1])->moveTo(Vec3f(-200.0f, 0, 75), slideTime); }
				if (sliderElements[selectedIndex - 2] != null && (selectedIndex - 2) >= 0)
				{ ((GameObject*)sliderElements[selectedIndex - 2])->moveTo(Vec3f(-275.0f, 0, 100), slideTime); }

				((GalleryItem*)sliderElements[selectedIndex])->setCentered(true);
				((ButtonObject*)sliderElements[selectedIndex])->setState(ButtonStateActive);
				((GalleryItem*)sliderElements[selectedIndex])->moveTo(Vec3f::Zero, slideTime);

				if (sliderElements[selectedIndex + 1] != null && (selectedIndex + 1) <= sliderElements.getSize()-1)
				{ ((GameObject*)sliderElements[selectedIndex + 1])->moveTo(Vec3f(200.0f, 0, 75), slideTime); }
				if (sliderElements[selectedIndex + 2] != null && (selectedIndex + 2) <= sliderElements.getSize()-1)
				{ ((GameObject*)sliderElements[selectedIndex + 2])->moveTo(Vec3f(275.0f, 0, 100), slideTime); }


			}
			else
			{
				if (sliderElements[selectedIndex - 1] != null && (selectedIndex - 1) >= 0)
				{ ((GameObject*)sliderElements[selectedIndex - 1])->moveTo(Vec3f(-150.0f, 0, 50), slideTime); }
				if (sliderElements[selectedIndex - 2] != null && (selectedIndex - 2) >= 0)
				{ ((GameObject*)sliderElements[selectedIndex - 2])->moveTo(Vec3f(-275.0f, 0, 100), slideTime); }

				((GalleryItem*)sliderElements[selectedIndex])->setCentered(true);
				((ButtonObject*)sliderElements[selectedIndex])->setState(ButtonStateActive);
				((GalleryItem*)sliderElements[selectedIndex])->moveTo(Vec3f::Zero, slideTime);

				if (sliderElements[selectedIndex + 1] != null && (selectedIndex + 1) <= sliderElements.getSize()-1)
				{ ((GameObject*)sliderElements[selectedIndex + 1])->moveTo(Vec3f(150.0f, 0, 50), slideTime); }
				if (sliderElements[selectedIndex + 2] != null && (selectedIndex + 2) <= sliderElements.getSize()-1)
				{ ((GameObject*)sliderElements[selectedIndex + 2])->moveTo(Vec3f(275.0f, 0, 100), slideTime); }
			}
		}
		else
		{
			if (sliderElements[selectedIndex - 1] != null && (selectedIndex - 1) >= 0)
			{ ((GameObject*)sliderElements[selectedIndex - 1])->moveTo(Vec3f(-110.0f, 0, 50), slideTime); }
			if (sliderElements[selectedIndex - 2] != null && (selectedIndex - 2) >= 0)
			{ ((GameObject*)sliderElements[selectedIndex - 2])->moveTo(Vec3f(-275.0f, 0, 150), slideTime); }
			if (sliderElements[selectedIndex - 3] != null && (selectedIndex - 3) >= 0)
			{ ((GameObject*)sliderElements[selectedIndex - 3])->moveTo(Vec3f(-500, 0, 280), slideTime); }

			printf("Set centered true for %s\n", ((GameObject*)sliderElements[selectedIndex])->name.str());

			((GalleryItem*)sliderElements[selectedIndex])->setCentered(true);
			((ButtonObject*)sliderElements[selectedIndex])->setState(ButtonStateActive);
			((GalleryItem*)sliderElements[selectedIndex])->moveTo(Vec3f::Zero, slideTime);

			if (sliderElements[selectedIndex + 1] != null && (selectedIndex + 1) <= sliderElements.getSize()-1)
			{ ((GameObject*)sliderElements[selectedIndex + 1])->moveTo(Vec3f(110.0f, 0, 50), slideTime); }
			if (sliderElements[selectedIndex + 2] != null && (selectedIndex + 2) <= sliderElements.getSize()-1)
			{ ((GameObject*)sliderElements[selectedIndex + 2])->moveTo(Vec3f(275.0f, 0, 150), slideTime); }
			if (sliderElements[selectedIndex + 3] != null && (selectedIndex + 3) <= sliderElements.getSize()-1)
			{ ((GameObject*)sliderElements[selectedIndex + 3])->moveTo(Vec3f(500.0f, 0, 280), slideTime); }

			for (int i = selectedIndex + 4; i < sliderElements.getSize(); i++)
			{
				if (sliderElements[i] != null)
				{ ((GameObject*)sliderElements[i])->moveTo(Vec3f(600, 0, 280), slideTime); }
			}

			for (int i = selectedIndex - 4; i >= 0; i--)
			{
				if (sliderElements[i] != null)
				{ ((GameObject*)sliderElements[i])->moveTo(Vec3f(-600, 0, 280), slideTime); }
			}
		}

	}
	else
	{
		if ( sliderElements[selectedIndex - 1] != null && (selectedIndex - 1) >= 0 )
		{
			((GameObject*) sliderElements[selectedIndex - 1])->moveTo(Vec3f(-94, 0, 60), slideTime);
		}
		if ( sliderElements[selectedIndex - 2] != null && (selectedIndex - 2) >= 0 )
		{
			((GameObject*) sliderElements[selectedIndex - 2])->moveTo(Vec3f(-182, 0, 70), slideTime);
		}
		if ( sliderElements[selectedIndex - 3] != null && (selectedIndex - 3) >= 0 )
		{
			((GameObject*) sliderElements[selectedIndex - 3])->moveTo(Vec3f(-274, 0, 80), slideTime);
		}
		if ( sliderElements[selectedIndex - 4] != null && (selectedIndex - 4) >= 0 )
		{
			((GameObject*) sliderElements[selectedIndex - 4])->moveTo(Vec3f(-400, 0, 90), slideTime);
		}

		((GameObject*) sliderElements[selectedIndex])->moveTo(Vec3f(0, 0, 50), slideTime);

		if ( sliderElements[selectedIndex + 1] != null && (selectedIndex + 1) <= sliderElements.getSize() - 1 )
		{
			((GameObject*) sliderElements[selectedIndex + 1])->moveTo(Vec3f(94, 0, 60), slideTime);
		}
		if ( sliderElements[selectedIndex + 2] != null && (selectedIndex + 2) <= sliderElements.getSize() - 1 )
		{
			((GameObject*) sliderElements[selectedIndex + 2])->moveTo(Vec3f(182, 0, 70), slideTime);
		}
		if ( sliderElements[selectedIndex + 3] != null && (selectedIndex + 3) <= sliderElements.getSize() - 1 )
		{
			((GameObject*) sliderElements[selectedIndex + 3])->moveTo(Vec3f(274, 0, 80), slideTime);
		}
		if ( sliderElements[selectedIndex + 4] != null && (selectedIndex + 4) <= sliderElements.getSize() - 1 )
		{
			((GameObject*) sliderElements[selectedIndex + 4])->moveTo(Vec3f(400, 0, 90), slideTime);
		}

		for ( int i = selectedIndex + 5; i < sliderElements.getSize(); i++ )
		{
			if ( sliderElements[i] != null )
			{
				((GameObject*) sliderElements[i])->moveTo(Vec3f(400, 0, 90), slideTime);
			}
		}

		for ( int i = selectedIndex - 5; i >= 0; i-- )
		{
			if ( sliderElements[i] != null )
			{
				((GameObject*) sliderElements[i])->moveTo(Vec3f(-400, 0, 90), slideTime);
			}
		}
	}
}

void SliderObject::loadAppropriateTextures()
{
	if ( sliderType == SliderTypeVideo )
	{
		//Just load them all
		for( int i = 0; i<sliderElements.getSize(); i++ )
		{
			if( sliderElements[i] != null )
			{ ((GalleryItem*)sliderElements[i])->loadTexture(); }
		}
	}
}

/// <summary>
/// Animates slider elements going Left
/// </summary>
void SliderObject::moveSliderLeft()
{
	if ( selectedIndex < sliderElements.getSize() - 1 && sliderElements[selectedIndex + 1] != null )
	{
		ButtonObject* oldItem = (ButtonObject*) sliderElements[selectedIndex];
		oldItem->cancelActions();
		oldItem->setState(ButtonStateNormal);
		oldItem->setScale( oldItem->originalScale );

		selectedIndex++;
		loadAppropriateTextures();
		positionElements();
		((ButtonObject*) getSelectedItem())->setState(ButtonStateActive);
	}
}

/// <summary>
/// Animates slider elements going right
/// </summary>
void SliderObject::moveSliderRight()
{
	if ( selectedIndex > 0 && sliderElements[selectedIndex - 1] != null )
	{
		ButtonObject* oldItem = (ButtonObject*) sliderElements[selectedIndex];
		oldItem->cancelActions();
		oldItem->setState(ButtonStateNormal);
		oldItem->setScale( oldItem->originalScale );

		selectedIndex--;
		loadAppropriateTextures();
		positionElements();
		((ButtonObject*) getSelectedItem())->setState(ButtonStateActive);
	}
}

/// <summary>
/// Add item to slider object
/// </summary>
void SliderObject::removeItem( GameObject* itemToRemove )
{
	printf("SliderObject::removeItem %s\n", itemToRemove->name.str());
	sliderElements.removeElement(itemToRemove);
	selectedIndex--;

	if ( selectedIndex < 0 && sliderElements.getSize() > 0 )
	{
		selectedIndex = 0;
	}

	loadAppropriateTextures();
	positionElements();
	itemToRemove->scaleTo(Vec3f::Zero, 0.25f);
	itemToRemove->kill();
}

/// <summary>
/// Adds appropriate objects into the tracking list from scene
/// </summary>
void SliderObject::addObjectsToTracking( ObjectArray* trackedObjects )
{
	trackedObjects->addElement(getSelectedItem());

	if ( canMoveLeft() )
	{ trackedObjects->addElement(sliderLeft); }

	if ( canMoveRight() )
	{ trackedObjects->addElement(sliderRight); }
}

/// <summary>
/// Returns if slider is at the edge bounds
/// </summary>
bool SliderObject::canMoveLeft()
{
	return selectedIndex > 0;
}

/// <summary>
/// Returns if slider is at the edge bounds
/// </summary>
bool SliderObject::canMoveRight()
{
	return selectedIndex < sliderElements.getSize() - 1;
}

/// <summary>
/// Respond to input, returns true if the slider rotated
/// </summary>
bool SliderObject::onAnalogChange( GameObject* objectLeft, GameObject* objectEntered )
{
	if ( objectLeft == getSelectedItem() )
	{
		if ( objectEntered == sliderRight )
		{
			sliderRight->scaleToOriginal(0.01f);
			moveSliderLeft();
			return true;
		}
		else if ( objectEntered == sliderLeft )
		{
			sliderLeft->scaleToOriginal(0.01f);
			moveSliderRight();
			return true;
		}
		else if ( selectedIndex != -1 )
		{
			((ButtonObject*) getSelectedItem())->setState(ButtonStateNormal);
		}
	}
	else
	{
		if ( objectLeft != NULL && objectEntered == sliderRight )
		{
			if ( objectLeft->typeOf(ButtonObject::type()) )
			{
				((ButtonObject*) objectLeft)->setState(ButtonStateNormal);
			}

			return true;
		}
		else if ( objectLeft != NULL && objectEntered == sliderLeft )
		{
			if ( objectLeft->typeOf(ButtonObject::type()) )
			{
				((ButtonObject*) objectLeft)->setState(ButtonStateNormal);
			}

			return true;
		}
	}

	if ( objectEntered == getSelectedItem() )
	{
		((ButtonObject*) getSelectedItem())->setState(ButtonStateActive);
	}

	return false;
}

int SliderObject::getSelectedIndex()
{
	return selectedIndex;
}

/// <summary>
/// Secondary type of setup which uses objects to preset the items
/// </summary>
void SliderObject::setupSlider( SliderObject::SliderType type, ObjectArray* arrayOfInfo )
{
	sliderType = type;
	if( sliderType == SliderTypeVideo )
	{

		//Clean up old
		for( int i = 0; i<sliderElements.getSize(); i++ )
		{
			((GalleryItem*)sliderElements[i])->unloadTexture();
			((GalleryItem*)sliderElements[i])->removeFromParent();
		}

		sliderElements.removeAllElements();

		//we have slider info

		for( int i = 0; i<arrayOfInfo->getSize(); i++ )
		{
			TRelease<GalleryItem> gallery((GalleryItem*)Scene::instantiate("GalleryItem"));
			addItem(gallery);
			gallery->name = String("VideoItem") + i;
			VideoInfo* info = (VideoInfo*)arrayOfInfo->elementAt(i);
			gallery->setVideoInfo(info);
			printf("Setting up VideoItem for %s\n", info->iconPath.str());
			gallery->setPreview( info->iconPath );
		}
	}
	selectedIndex = 0;
	loadAppropriateTextures();
	positionElements();
}

/// <summary>
/// Preset filler helper
/// </summary>
void SliderObject::setupSlider( SliderObject::SliderType type )
{
	sliderType = type;
	if ( type == SliderTypeAvatar )
	{
		for ( int i = 0; i < 30; i++ )
		{
			TRelease<AvatarItem> avatarChoice((AvatarItem*) Scene::instantiate(String("AvatarItem") + i));
			addItem(avatarChoice);
		}
	}
	else if ( type == SliderTypeTheme )
	{
		for ( int i = 0; i < 4; i++ )
		{
			TRelease<ThemeItem> themeChoice((ThemeItem*) Scene::instantiate(String("ThemeItem") + i));
			addItem(themeChoice);
		}
	}

	selectedIndex = (int) (sliderElements.getSize() / 2);
	positionElements();
}

/// <summary>
/// Add item to slider object
/// </summary>
void SliderObject::addItem( GameObject* itemToAdd )
{
	addChild(itemToAdd);
	sliderElements.addElement(itemToAdd);
}

/// <summary>
/// Animation heper
/// </summary>
void SliderObject::setItemsAsActive()
{
	for ( int i = 0; i < sliderElements.getSize(); i++ )
	{
		GameObject* element = (GameObject*) sliderElements[i];
		for ( int j = 0; j < element->numChildren(); j++ )
		{
			SpriteObject* object = (SpriteObject*) element->childAtIndex(j);
			object->fadeTo(1, 0.25f);
		}

		element->fadeTo(1, 0.25f);
	}
}

/// <summary>
/// Animation heper
/// </summary>
void SliderObject::setItemsAsInactive()
{
	for ( int i = 0; i < sliderElements.getSize(); i++ )
	{
		GameObject* element = (GameObject*) sliderElements[i];
		for ( int j = 0; j < element->numChildren(); j++ )
		{
			SpriteObject* object = (SpriteObject*) element->childAtIndex(j);
			object->fadeTo(0.5f, 0.25f);
		}
	}
}

/// <summary>
/// Animation heper
/// </summary>
void SliderObject::animateOutOnTop()
{
	cancelActions();
	moveBy(Vec3f(0, 200, -200), 1.55f);
	makeVisible(false);
	moveBy(Vec3f(0, -200, 200), 0.01f);
}

/// <summary>
/// Animation heper
/// </summary>
void SliderObject::animateOutOnBottom()
{
	cancelActions();
	moveBy(Vec3f(0, -200, -200), 1.55f);
	makeVisible(false);
	moveBy(Vec3f(0, 200, 200), 0.01f);
}

/// <summary>
/// Animation heper
/// </summary>
void SliderObject::staggerElementsIn()
{
}

/// <summary>
/// Animation heper
/// </summary>
void SliderObject::animateInitialEntry()
{
	setVisible(true);
	cancelActions();
	setPosition(Vec3f(0, getPosition().y, getPosition().z));
	moveBy(Vec3f(0, -200, -200), 0.01f);
	waitFor(0.5f);
	moveBy(Vec3f(0, 200, 200), 1.5f);
}

/// <summary>
/// Sets the selection of the slider to a specifc element
/// </summary>
void SliderObject::setSelected( String itemName )
{
	if ( sliderType == SliderTypeAvatar )
	{
		int index = sliderElements.getSize() / 2;
		for ( int i = 0; i < sliderElements.getSize(); i++ )
		{
			AvatarItem* item = (AvatarItem*) sliderElements[i];
			if ( item->getPrefabCloneName() == itemName )
			{
				selectedIndex = i;
				positionElements();
				return;
			}
		}
	}
	else if ( sliderType == SliderTypeTheme )
	{
		int index = sliderElements.getSize() / 2;
		for ( int i = 0; i < sliderElements.getSize(); i++ )
		{
			ThemeItem* item = (ThemeItem*) sliderElements[i];
			if ( item->getThemeName() == itemName )
			{
				selectedIndex = i;
				positionElements();
				return;
			}
		}
	}
}

/// <summary>
/// Tick override
/// </summary>
void SliderObject::update( float dt )
{
	super::update(dt);
}

/// <summary>
/// Respond to input
/// </summary>
void SliderObject::onButtonPressAction( GameObject* currentHighlight )
{

}

}


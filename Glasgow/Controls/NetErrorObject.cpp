#include "NetErrorObject.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"
namespace Glasgow
{

NetErrorObject::NetErrorObject(TextLabel* er, SpriteObject* eI, TextLabel* h, ButtonObject* rB, SpriteObject* wI, ButtonObject* sB ) : super()
{
	printf("NetErrorObject::NetErrorObject\n");
	errorLabel 		= er;
	noEthernetIcon 	= eI;
	errorHeader 	= h;
	reconnectButton = rB;
	noWifiIcon 		= wI;
	skipButton		= sB;
	skipButton->setVisible(false);

	if( er == null || eI == null || h == null || rB == null || wI == null )
		ThrowException::deserializeException();

	addChild(sB);
	addChild(er);
	addChild(eI);
	addChild(h);
	addChild(rB);
	addChild(wI);
}

NetErrorObject::~NetErrorObject()
{
	printf("NetErrorObject::~NetErrorObject()");
}

///<summary>
/// Support load from file
///</summary>
NetErrorObject* NetErrorObject::deSerialize(FILE* fp)
{
	TRelease<TextLabel> er((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> eI((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> h((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> rB((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<ButtonObject> sB((ButtonObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> wI((SpriteObject*)Scene::deSerializeNext(fp));
	return new NetErrorObject( er, eI, h, rB, wI, sB);
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void NetErrorObject::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(reconnectButton);
}

///<summary>
/// reverse animation of display network error
///</summary>
void NetErrorObject::animateOut()
{
	errorHeader->cancelActions();
	errorLabel->cancelActions();
	noEthernetIcon->cancelActions();
	noWifiIcon->cancelActions();
	reconnectButton->cancelActions();

	errorHeader->fadeOut(0);
	errorLabel->fadeOut(0);
	noEthernetIcon->bubbleOut(0);
	noWifiIcon->bubbleOut(0);
	reconnectButton->bubbleOut(0);
}

///<summary>
/// Sets network error state
///</summary>
void NetErrorObject::displayNetworkError( String header, String error )
{
	printf("NetErrorObject::displayNetworkError\n");
	errorHeader->setText(header);
	errorLabel->setText(error);
	errorHeader->fadeIn(0.5);
	errorLabel->fadeIn(0.6);
	reconnectButton->bubbleIn(1);
	skipButton->setVisible(false);

	noEthernetIcon->setVisible(true);
	noEthernetIcon->bubbleIn(0.25f);
	noWifiIcon->setVisible(false);
}

} /* namespace Glasgow */

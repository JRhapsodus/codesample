#ifndef GALLERYITEM_H_
#define GALLERYITEM_H_

#include "SIEngine/Include/SIDeserialize.h"
#include "Glasgow/Data/VideoInfo.h"

namespace Glasgow
{

class GalleryItem : public ButtonObject
{
	CLASSEXTENDS(GalleryItem, ButtonObject);
public:
	GalleryItem(SpriteObject* galleryFrameActive, SpriteObject* galleryFrameCentered );
	virtual ~GalleryItem();
	void setPreview( String fileName );
	virtual void transition( TransitionState transition );
	static GalleryItem* deSerialize(FILE* fp);


	void unloadTexture();
	void loadTexture();

	void setCentered(bool c);

	void setVideoInfo(VideoInfo* info);
	VideoInfo* getVideoInfo();
private:
	bool centered;
	SpriteObject* preview;

	SpriteObject* activeFrame;
	SpriteObject* centerFrame;
	String textureName;
	VideoInfo* videoInfo;
};

}
#endif

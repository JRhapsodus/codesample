#ifndef VERTICALSLIDER_H_
#define VERTICALSLIDER_H_

#include "SIEngine/Include/SIControls.h"

namespace Glasgow
{

class VerticalSlider : public SliderObject
{
	CLASSEXTENDS(VerticalSlider, SliderObject);
public:
	VerticalSlider(SpriteObject* sliderLeft, SpriteObject* sliderRight);
	virtual ~VerticalSlider();

	/// <summary>
	/// Tick override
	/// </summary>
	virtual void update( float dt );

	/// <summary>
	/// Respond to input
	/// </summary>
	virtual void onButtonPressAction(GameObject* currentHighlight);

	/// <summary>
	/// Respond to input
	/// </summary>
	virtual bool onAnalogChange(GameObject* objectLeft, GameObject* objectEntered);

	/// <summary>
	/// Load from file
	/// </summary>
	static VerticalSlider* deSerialize(FILE* fp);

	/// <summary>
	/// Position elements after a slider has changed selection
	/// </summary>
	virtual void positionElements();

	/// <summary>
	/// Animates slider elements going Left
	/// </summary>
	void positionFirstElements();

	/// <summary>
	/// Adds appropriate objects into the tracking list from scene
	/// </summary>
	virtual void addObjectsToTracking( ObjectArray* trackingList );

	/// <summary>
	/// Animates slider elements going left
	/// </summary>
	virtual void moveSliderLeft();

	/// <summary>
	/// Animates slider elements going right
	/// </summary>
	virtual void moveSliderRight();

	/// <summary>
	/// Prepare for scan
	/// </summary>
	void prepareForScan();

	///<summary>
	/// remove all buttons
	///</summary>
	void removeAll();

protected:
	static Vec3f topElementPosition;


	int bottomRange;
	int topRange;


};

}
#endif

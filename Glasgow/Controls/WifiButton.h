#ifndef WIFIBUTTON_H_
#define WIFIBUTTON_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

enum WifiStrength
{
	WifiStrengthWeakest,
	WifiStrengthWeak,
	WifiStrengthStrong,
	WifiStrengthStrongest
};
/// <summary> 
/// This is a button that has a check mark, and 4 wifi states
/// </summary> 
class WifiButton : public ButtonObject
{
	CLASSEXTENDS(WifiButton, ButtonObject);

public:
	/*
	 * 2 "wifiName2" 0 1 -177.2793 -48.34906 0 1 1 1 0 0 0 WifiButton
		3 "background" 0 0 -1.525879E-05 0 0 1 1 1 0 0 0 SpriteObject oobeAtlas 450 75 0 0.04736328 0.2197266 0.01074219
		3 "selected" 0 0 -163.1908 0 -50 0.9125 1 1 0 0 0 SpriteObject oobeAtlas 45 45 0.953125 0.7768555 0.9750977 0.7548828
		3 "text" 0 0 0 0 -261.9082 0.4189735 0.4189735 0.4189735 0 0 0 TextLabel "Network Name" 1 0.1803922 0.4745098 0.3215686 0.9843137
		3 "wifi1" 0 0 154.257 0 -50 0.9125 1 1 0 0 0 SpriteObject oobeAtlas 60 53 0.953125 0.8076172 0.9824219 0.7817383
		3 "wifi2" 0 0 154.257 0 -50 0.9125 1 1 0 0 0 SpriteObject oobeAtlas 60 53 0.953125 0.8383789 0.9824219 0.8125
		3 "wifi3" 0 0 154.257 0 -50 0.9125 1 1 0 0 0 SpriteObject oobeAtlas 60 53 0.953125 0.8691406 0.9824219 0.8432617
		3 "wifi4" 0 0 154.257 0 -50 0.9125 1 1 0 0 0 SpriteObject oobeAtlas 60 53 0.953125 0.8999023 0.9824219 0.8740234
	 *
	 */
	WifiButton(
			SpriteObject* active,
			SpriteObject* background,
			TextLabel* text,
			SpriteObject* selected,
			SpriteObject* wifi1,
			SpriteObject* wifi2,
			SpriteObject* wifi3,
			SpriteObject* wifi4);

	virtual ~WifiButton();

	void setWifiStrength( WifiStrength strength);
	void setNetworkName( String networkName );

	static WifiButton* deSerialize(FILE* fp);
	virtual void transition(TransitionState transition);

private:
	TextLabel* 		networkNameLabel;
	SpriteObject* 	wifiStrengthIcons[(WifiStrengthStrongest+1)];
	WifiStrength 	currentStrength;
};

}
#endif /* TEXTBUTTON_H_ */

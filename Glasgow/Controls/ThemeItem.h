#ifndef THEMEITEM_H_
#define THEMEITEM_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class ThemeItem : public ButtonObject
{
	CLASSEXTENDS(ThemeItem, ButtonObject);
	ADD_TO_CLASS_MAP;
public:

	ThemeItem(SpriteObject* icon_, String name);
	virtual ~ThemeItem();

	static ThemeItem* deSerialize(FILE* fp);
	virtual void transition(TransitionState transition);

	virtual void update(float dt);
	String getThemeName();

private:
	String themeName;
	SpriteObject* icon;

};

}
#endif /* THEMEITEM_H_ */

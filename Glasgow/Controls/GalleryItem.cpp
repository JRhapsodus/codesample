#include "GalleryItem.h"
#include "SIEngine/Include/SIRendering.h"
namespace Glasgow
{

GalleryItem::GalleryItem(SpriteObject* galleryFrame, SpriteObject* galleryFrameCentered ) : super()
{
	addChild(galleryFrame);
	addChild(galleryFrameCentered);
	centered = false;
	activeFrame = galleryFrame;
	preview = NULL;
	activeFrame->setVisible(true);
	activeFrame->setOpacity(0);
	doBreathe = false;
	centerFrame = galleryFrameCentered;
}

GalleryItem::~GalleryItem()
{
	printf("GalleryItem::~GalleryItem\n");
	unloadTexture();
}

void GalleryItem::setVideoInfo(VideoInfo* info)
{ videoInfo = info; }

VideoInfo* GalleryItem::getVideoInfo()
{ return videoInfo; }

void GalleryItem::setCentered( bool center)
{
	centered = center;
	if( !center )
	{
		activeFrame->setOpacity(0);
		centerFrame->setOpacity(0);
		centerFrame->setVisible(false);
	}
	else
	{
		centerFrame->setVisible(true);
	}
}

void GalleryItem::unloadTexture()
{
	if( TextureManager::getInstance() != null )
	{
		TextureManager::getInstance()->unloadTexture( textureName );
	}

}

void GalleryItem::loadTexture()
{ TextureManager::getInstance()->loadPng( textureName ); }

void GalleryItem::setPreview( String fileName )
{
	textureName = fileName;

	TRelease<SpriteData> spriteData( new SpriteData(fileName));
	TRelease<SpriteObject> sprite( new SpriteObject(spriteData) );
	spriteData->setDeferredLoad( true );
	sprite->setSize( Size( 400, 300 ) );
	sprite->setScale( activeFrame->getScale() * 1.01f);
	addChild(sprite);
	preview = sprite;
}

void GalleryItem::transition( TransitionState transition )
{
	if( transition == NormalToActive )
	{
		totalTimeElapsed = 0;
		activeFrame->cancelActions();
		centerFrame->cancelActions();

		if( centered )
		{
			activeFrame->fadeTo(1, 0.25f);
			centerFrame->fadeTo(0, 0.25f);
		}
		else
		{
			activeFrame->fadeTo(0, 0.25f);
			centerFrame->fadeTo(0, 0.25f);
		}
	}
	else if ( transition == ActiveToNormal )
	{
		printf("GalleryItem normal : %s\n", name.str());
		totalTimeElapsed = 0;
		activeFrame->cancelActions();
		centerFrame->cancelActions();

		if( !centered )
		{
			activeFrame->fadeTo(0, 0.25f);
			centerFrame->fadeTo(0, 0.25f);
		}
		else
		{
			activeFrame->fadeTo(0, 0.25f);
			centerFrame->fadeTo(1, 0.25f);
		}
	}
}


GalleryItem* GalleryItem::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> frame((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> focused((SpriteObject*)Scene::deSerializeNext(fp));
	return new GalleryItem( frame, focused );
}

}

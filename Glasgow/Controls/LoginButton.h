#ifndef LOGINBUTTON_H_
#define LOGINBUTTON_H_

#include "SIEngine/Include/SIDeserialize.h"
#include "SIEngine/Core/User.h"

namespace Glasgow
{

typedef enum LoginButtonTypes
{
	LoginButtonNone,
	LoginButtonUser,
	LoginButtonAddUser,
	LoginButtonGuest
} LoginButtonType;


class LoginButton : public ButtonObject
{
	CLASSEXTENDS(LoginButton, ButtonObject);
	ADD_TO_CLASS_MAP;
public:
	LoginButton(User *user);
	LoginButton(String textLabel, String backgroundImage);
	LoginButton(SpriteObject *rimNormal, SpriteObject *rimSelected, SpriteObject *plate, TextLabel *label, SpriteObject *avatar);
	virtual ~LoginButton();

	virtual void transition(TransitionState state);
	User *getUser();
	void setUser(User *user);
	void setupForAddUser();
	LoginButtonType getLoginButtonType();

	static LoginButton* deSerialize(FILE* fp);

private:
	void init(String textLabel, String backgroundImage);
	User		 *user;
	LoginButtonType loginButtonType;
	TextLabel 	 *username;
	SpriteObject *plateNormal;
	GameObject	 *bubble;
	SpriteObject *bubbleNormal;
	SpriteObject *bubbleActive;
	SpriteObject *profileImage;
};

}

#endif /* LOGINBUTTON_H_ */

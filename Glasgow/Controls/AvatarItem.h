#ifndef AVATARITEM_H_
#define AVATARITEM_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class AvatarItem : public ButtonObject
{
	CLASSEXTENDS(AvatarItem, ButtonObject);
	ADD_TO_CLASS_MAP;
public:
	typedef enum AvatarItemTypes
	{
		ItemUserWithoutAvatar,
		ItemUserWithAvatar,
		ItemUserAsGuest,
		ItemUserAsNewUser
	} AvatarItemType;


	AvatarItem(SpriteObject* icon_, TextLabel* nameLabel_, SpriteObject* profileActive_, SpriteObject* rim_, String prefabToUse);
	virtual ~AvatarItem();

	///<summary>
	/// Support load from file
	///</summary>
	static AvatarItem* deSerialize(FILE* fp);

	///<summary>
	/// Custom Transitions
	///</summary>
	virtual void transition( TransitionState transition );

	///<summary>
	/// Custom features
	///</summary>
	virtual void update( float dt );

	///<summary>
	/// Setups up the Avatar Item with the 'Login' state
	///</summary>
	void setupAsLoginAvatar( String name );

	///<summary>
	/// Set a weak reference to user
	///</summary>
	void setUser( User* user );

	///<summary>
	/// Fade name label out
	///</summary>
	void fadeNameOut();

	///<summary>
	/// Returns user reference
	///</summary>
	User* getUser();

	///<summary>
	/// The prefab name
	///</summary>
	String getPrefabCloneName();

	///<summary>
	/// Returns the user string
	///</summary>
	String getUserName();

	///<summary>
	/// Returns true if the item represents a guest user
	///</summary>
	bool isGuest();

	///<summary>
	/// Returns the type of AvatarItem this is.
	///</summary>
	AvatarItem::AvatarItemType getItemType();

	bool getIsNewUser();

private:
	///<summary>
	/// Sets which type of button this is
	///</summary>
	void  setItemType();

	AvatarItemType	itemType;
	bool 			isNewUser;
	User* 			user;
	SpriteObject* 	activeProfileSelection;
	SpriteObject* 	rim;
	TextLabel* 	  	nameLabel;
	String 			userName;
	String 			prefabName;
};

}
#endif /* AVATARITEM_H_ */

#ifndef HilightButton_H_
#define HilightButton_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

/// <summary> 
/// This is a button that scales in a roll over object on active state
/// </summary> 
class HilightButton : public ButtonObject
{
	CLASSEXTENDS(HilightButton, ButtonObject);

public:
	HilightButton(SpriteObject* normalBacking, SpriteObject* rim, SpriteObject* rimSelected  );
	virtual ~HilightButton();

	static HilightButton* deSerialize(FILE* fp);
	virtual void transition( TransitionState transition );
private:
	SpriteObject* rim;
	SpriteObject* rimSelected;
	Vec3f originalLocalPosition;
};

}
#endif /* TEXTBUTTON_H_ */

#include "VerticalSlider.h"

namespace Glasgow
{

Vec3f VerticalSlider::topElementPosition = Vec3f(0, 115, 0);

VerticalSlider::VerticalSlider(SpriteObject* sliderLeft, SpriteObject* sliderRight) : super(sliderLeft, sliderRight)
{
	selectedIndex = 0;
	bottomRange = 0;

	sliderLeft->makeVisible(false);
	sliderRight->makeVisible(false);
}

VerticalSlider::~VerticalSlider()
{
	printf("VerticalSlider::~VerticalSlider()\n");

	for( int i = 0; i<sliderElements.getSize(); i++ )
	{
		GameObject* go = (GameObject*)sliderElements[i];
		if( go->getParent() != null )
		{ go->removeFromParent(); }
	}

	sliderElements.removeAllElements();
}

/// <summary>
/// Tick override
/// </summary>
void VerticalSlider::update( float dt )
{ super::update(dt); }

/// <summary>
/// Load from file
/// </summary>
VerticalSlider* VerticalSlider::deSerialize(FILE* fp)
{
	TRelease<SpriteObject> l((SpriteObject*) Scene::deSerializeNext(fp));
	TRelease<SpriteObject> r((SpriteObject*) Scene::deSerializeNext(fp));
	return new VerticalSlider(l, r);
}

/// <summary>
/// Adds appropriate objects into the tracking list from scene
/// </summary>
void VerticalSlider::addObjectsToTracking( ObjectArray* trackingList )
{
	for( int i = bottomRange; i<=topRange && i<sliderElements.getSize(); i++ )
	{ trackingList->addElement((GameObject*)sliderElements.elementAt(i)); }

	if(sliderElements.getSize() > 0)
	{
		trackingList->addElement(sliderLeft);
		trackingList->addElement(sliderRight);
	}
}

/// <summary>
/// Respond to input
/// </summary>
void VerticalSlider::onButtonPressAction(GameObject* currentHighlight)
{ }

/// <summary>
/// Prepare for scan
/// </summary>
void VerticalSlider::prepareForScan()
{
	printf("//////////////////////////////////////////////\n");
	printf("//////////////////////////////////////////////\n");
	printf("//////////////////////////////////////////////\n");
	printf("VerticalSlider::prepareForScan Number of children: %d\n", getChildren()->getSize());
	selectedIndex = -1;
	float waitTime = 0;

	for(int i = 0; i < sliderElements.getSize(); ++i)
	{
		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
		if(button->isVisibleHierarchy())
		{
			button->waitFor(waitTime);
			button->scaleTo( Vec3f::Zero, 0.25f);
			waitTime += 0.1f;
			button->kill();
		}
		else
		{
			button->removeFromParent();
		}
	}

//	for( int i = 0; i<bottomRange && i < sliderElements.getSize(); i++ )
//	{
//		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
//		button->removeFromParent();
//	}
//
//	for( int i = bottomRange; i<=topRange && i<sliderElements.getSize(); i++ )
//	{
//		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
//		button->waitFor(waitTime);
//		button->scaleTo( Vec3f::Zero, 0.25f);
//		waitTime += 0.1f;
//		button->kill();
//	}
//
//	for( int i = topRange+1; i<sliderElements.getSize(); i++ )
//	{
//		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
//		button->removeFromParent();
//	}

	sliderElements.removeAllElements();
	sliderLeft->scaleTo(Vec3f::Zero, 0.25f);
	sliderRight->scaleTo(Vec3f::Zero, 0.25f);
}

///<summary>
/// remove all buttons
///</summary>
void VerticalSlider::removeAll()
{
	for( int i = 0; i<sliderElements.getSize(); i++ )
	{
		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
		button->removeFromParent();
	}

	sliderElements.removeAllElements();
	sliderLeft->setScale(Vec3f::Zero);
	sliderRight->setScale(Vec3f::Zero);

	printf("VerticalSlider::removeAll numChildren: %d\n", getChildren()->getSize());
}

/// <summary>
/// Respond to input
/// </summary>
bool VerticalSlider::onAnalogChange(GameObject* objectLeft, GameObject* objectEntered)
{
	if( objectLeft != null && objectEntered != null )
	{
		if( !objectLeft->descendantOf(this) && !objectEntered->descendantOf(this) )
			return false;

		if( objectLeft != sliderLeft && objectLeft != sliderRight && sliderElements.indexOf(*objectLeft) == -1 )
		{
			objectLeft->cancelActions();
			((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
			((ButtonObject*)getSelectedItem())->setState(ButtonStateActive);

			//this object is outside of our slider, so lets just select the indexed object
			return true;
		}
		else if( objectLeft->typeOf(WifiButton::type()) && objectEntered == sliderRight )
		{
			moveSliderRight();
			return true;
		}
		else if( objectLeft->typeOf(WifiButton::type()) && objectEntered == sliderLeft )
		{
			moveSliderLeft();
			return true;
		}
		else if( objectLeft->typeOf(WifiButton::type()) && objectEntered->typeOf( WifiButton::type()) )
		{
			ButtonObject* oldItem = (ButtonObject*) sliderElements[selectedIndex];
			oldItem->cancelActions();
			oldItem->setState(ButtonStateNormal);

			if( sliderElements.indexOf(*objectLeft) < sliderElements.indexOf(*objectEntered) )
			{ selectedIndex++; }
			else
			{ selectedIndex--; }

			ButtonObject* newItem = (ButtonObject*) sliderElements[selectedIndex];
			newItem->cancelActions();
			newItem->setState(ButtonStateActive);

			printf("Object Left index: %d Object entered index: %d\n", sliderElements.indexOf(*objectLeft), sliderElements.indexOf(*objectEntered));
			printf("New range [%d -> %d] Selected: %d\n", bottomRange, topRange, selectedIndex);
			return true;
		}
	}

	return false;
}

/// <summary>
/// Position elements after a slider has changed selection
/// </summary>
void VerticalSlider::positionElements()
{
	//For the items above the bottom range,
	for( int i = 0; i<bottomRange && i < sliderElements.getSize(); i++ )
	{
		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
		button->cancelActions();
		button->moveToScaleTo( topElementPosition + Vec3f::Up*55, Vec3f::Zero, 0.25f );
	}

	//For the items within the visual range
	for( int i = bottomRange; i<=topRange && i < sliderElements.getSize(); i++ )
	{
		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
		button->cancelActions();
		button->moveToScaleTo( topElementPosition + (Vec3f::Down*55*(i-bottomRange)), button->originalScale, 0.25f );
	}

	//For the items at the bottom range
	for( int i = topRange+1; i<sliderElements.getSize(); i++ )
	{
		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);
		button->cancelActions();
		button->moveToScaleTo( topElementPosition + (Vec3f::Down*55*5), Vec3f::Zero, 0.25f );
	}
}

/// <summary>
/// Animates slider elements going left
/// </summary>
void VerticalSlider::moveSliderLeft()
{
	if ( selectedIndex > 0  )
	{
		bottomRange--;
		topRange = bottomRange+4;

		ButtonObject* oldItem = (ButtonObject*) sliderElements[selectedIndex];
		oldItem->cancelActions();
		oldItem->setState(ButtonStateNormal);

		selectedIndex--;
		getSelectedItem()->setVisible(true);
		getSelectedItem()->setScale(Vec3f::Zero);
		getSelectedItem()->setPosition( topElementPosition + (Vec3f::Up*55) );
		((ButtonObject*) getSelectedItem())->setState(ButtonStateActive);
		positionElements();
	}

	if ( topRange == sliderElements.getSize()-1 )
	{ sliderRight->setVisible(false); }
	else
	{ sliderRight->setVisible(true); }

	if( bottomRange > 0 )
	{ sliderLeft->setVisible(true); }
	else
	{ sliderLeft->setVisible(false); }
}


/// <summary>
/// Animates slider elements going right
/// </summary>
void VerticalSlider::moveSliderRight()
{
	if ( selectedIndex < sliderElements.getSize() - 1 && sliderElements[selectedIndex + 1] != null )
	{
		bottomRange++;
		topRange = bottomRange+4;

		ButtonObject* oldItem = (ButtonObject*) sliderElements[selectedIndex];
		oldItem->cancelActions();
		oldItem->setState(ButtonStateNormal);

		selectedIndex++;
		getSelectedItem()->setVisible(true);
		getSelectedItem()->setScale(Vec3f::Zero);
		getSelectedItem()->setPosition( topElementPosition + (Vec3f::Down*55*5) );
		((ButtonObject*) getSelectedItem())->setState(ButtonStateActive);
		positionElements();
	}

	if ( topRange == sliderElements.getSize()-1 )
	{ sliderRight->setVisible(false); }
	else
	{ sliderRight->setVisible(true); }

	if( bottomRange > 0 )
	{
		sliderLeft->scaleTo( sliderLeft->originalScale, 0.25f );
		sliderLeft->setVisible(true);

	}
	else
	{ sliderLeft->setVisible(false); }
}

/// <summary>
/// Animates slider elements going Left
/// </summary>
void VerticalSlider::positionFirstElements()
{
	bottomRange = 0;
	topRange = bottomRange + 4;

	if( sliderElements.getSize() > 0 )
	{ selectedIndex = 0; }

	float waitTime = 0.5f;
	for( int i = 0; i<sliderElements.getSize(); i++ )
	{
		WifiButton* button = (WifiButton*)sliderElements.elementAt(i);

		if( i > topRange )
		{
			button->setVisible(false);
			button->setPosition( topElementPosition + (Vec3f::Down*55*5) );
			button->setScale(Vec3f::Zero);
		}
		else
		{
			button->setScale( Vec3f::Zero );
			button->waitFor(waitTime);
			button->setPosition( topElementPosition + (Vec3f::Down*55*i) );
			button->scaleTo( button->originalScale, 0.25f );
		}

		waitTime += 0.1f;
	}

	if( sliderElements.getSize() > topRange )
	{
		sliderRight->waitFor(0.5f + (0.1f * 4) );
		sliderRight->setScale(Vec3f::Zero);
		sliderRight->makeVisible(true);
		sliderRight->scaleTo( sliderRight->originalScale, 0.25f );
	}
}


}

#include "TextButton.h"

namespace Glasgow
{

TextButton::TextButton( Color normal, Color selected, TextLabel* text,
		SpriteObject* normalBacking,
		SpriteObject* selectedBacking ) : super()
{
	normalColor = normal;
	selectedColor = selected;
	buttonText = text;
	backgroundActive = selectedBacking;
	backgroundNormal = normalBacking;

	addChild(text);
	addChild(normalBacking);
	addChild(selectedBacking);

	selectedBacking->setPosition( selectedBacking->getPosition() + Vec3f::Back*5);
	text->setPosition( text->getPosition() + Vec3f::Back*15);

	buttonText->setTextColor( normalColor );
	buttonText->setOriginalColor(normalColor);
	backgroundActive->setVisible(false);
	currentState = ButtonStateNormal;
	keepBackgroundOn = true;
}

TextButton::~TextButton()
{ }

void TextButton::update(float dt )
{
	if( hasActions() )
		totalTimeElapsed = 0;

	super::update(dt);
}

void TextButton::transition( TransitionState transition )
{
	totalTimeElapsed = 0;

	if (transition == NormalToActive)
	{
		backgroundActive->cancelActions();
		backgroundActive->setVisible(true);
		backgroundActive->setOpacity(0);
		backgroundActive->fadeTo(1, 0.25f);
		buttonText->cancelActions();
		buttonText->setTextColor( selectedColor);

		//horrible style!!! todo: refactor later
		if(parent != null && getParent()->name == "numPad")
		{ buttonText->scaleTo( buttonText->originalScale*1.35f, 0.25f); }
	}
	else if (transition == ActiveToNormal)
	{
		backgroundActive->cancelActions();
		buttonText->cancelActions();
		backgroundActive->fadeTo(0, 0.25f);
		buttonText->setTextColor( normalColor);

		if( parent != null && getParent()->name == "numPad")
		{ buttonText->scaleTo( buttonText->originalScale, 0.25f); }

		this->cancelActions();
		this->scaleTo(this->originalScale, 0.25f);
	}
	else if (transition == ActiveToSelected)
	{
		backgroundActive->cancelActions();
		backgroundActive->setVisible(true);
		backgroundActive->setOpacity(0);
		backgroundActive->fadeTo(1, 0.25f);

		buttonText->cancelActions();
		buttonText->setTextColor( selectedColor);
	}
	else if (transition == SelectedToNormal)
	{
		backgroundActive->cancelActions();
		backgroundActive->fadeTo(0, 0.25f);
		buttonText->cancelActions();
		buttonText->setTextColor( normalColor);
		if( parent != null && getParent()->name == "numPad")
		{ buttonText->scaleTo( buttonText->originalScale, 0.25f); }

	}
}

//*******************************************************************************************************************
//2 "localeAustralia" 0 1 TextButton 0.1803922 0.4627451 0.3019608 1 1 1 1 1 221.2294 -53.70477 0 1 1 1 0 0 0
//	3 "selected" 0 0 SpriteObject oobeAtlas 450 75 0.5517578 0.6074219 0.7714844 0.5708008 0 0 0 0.9125 1 1 0 0 0
//	3 "text" 0 0 TextLabel "Australia" 1 0.1803922 0.4745098 0.3215686 0.9843137 0 3 -261.9082 0.418974 0.418974 0.418974 0 0 0
//*******************************************************************************************************************
TextButton* TextButton::deSerialize(FILE* fp)
{
	Color normal;
	Color selected;

	if( fscanf(fp, "%f %f %f %f %f %f %f %f", &normal.r, &normal.g, &normal.b, &normal.a, &selected.r, &selected.g, &selected.b, &selected.a) == 8)
	{
		TRelease<SpriteObject> normalBacking((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<SpriteObject> selectedBacking((SpriteObject*)Scene::deSerializeNext(fp));
		TRelease<TextLabel> text((TextLabel*)Scene::deSerializeNext(fp));

		return new TextButton(normal, selected, text, normalBacking, selectedBacking);
	}

	return NULL;
}

}

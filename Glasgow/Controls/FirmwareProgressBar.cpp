#include "FirmwareProgressBar.h"
#include "SIEngine/Include/SIScenes.h"

namespace Glasgow
{

FirmwareProgressBar::FirmwareProgressBar( TextLabel* available, SpriteObject* background, TextLabel* currentSpace, SpriteObject* l, SpriteObject* m, SpriteObject* r)
	: super( available, background, currentSpace, l, m, r)
{
	isFinished = false;
	totalDownloads = 0;
	finishedDownloads = 0;
	remainingDownloads = 0;
	currentDownloadProgress = 0;
	percentPerDownload = 0;
	lastProgressHeard = 0;

	setLabel("There are downloads remaining.", "");
}

FirmwareProgressBar::~FirmwareProgressBar()
{
	printf("FirmwareProgressBar::~FirmwareProgressBar()\n");
}

/// <summary>
/// Load from file
/// </summary>
FirmwareProgressBar* FirmwareProgressBar::deSerialize(FILE* fp)
{
	TRelease<TextLabel> available((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> background((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<TextLabel> current((TextLabel*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> l((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> m((SpriteObject*)Scene::deSerializeNext(fp));
	TRelease<SpriteObject> r((SpriteObject*)Scene::deSerializeNext(fp));
	return new FirmwareProgressBar(available, background, current, l, m, r);
}

/// <summary>
/// Download finished
/// </summary>
void FirmwareProgressBar::handleDonwloadFinished( String packageID )
{
	if( isVisibleHierarchy() )
	{
		finishedDownloads++;
		printf("Finished: %d, Total: %d, Remaining %d\n", finishedDownloads, totalDownloads,remainingDownloads );
		if( finishedDownloads == totalDownloads  )
		{ isFinished = true; }
	}

	lastProgressHeard = 0;
}

/// <summary>
/// Set when the download is finished
/// </summary>
bool FirmwareProgressBar::getIsFinished()
{
	return isFinished;
}


/// <summary>
/// Download progress
/// </summary>
void FirmwareProgressBar::handleDownloadProgress( String package, int downloadProgress)
{
	currentDownloadProgress = ((float)downloadProgress)/100.0f;
	float totalProgress = percentPerDownload*currentDownloadProgress + percentPerDownload*(float)finishedDownloads;
	totalProgress = (totalProgress>1)?1:totalProgress;
	totalProgress = (totalProgress<0)?0:totalProgress;
	if( isVisibleHierarchy() )
	{ setProgress(totalProgress); }

	lastProgressHeard = 0;
}

void FirmwareProgressBar::update(float dt )
{
	if( isVisibleHierarchy())
	{
		lastProgressHeard += dt;

		if( lastProgressHeard > 60 && !isFinished )
		{
			//throw enetwork error ?
			SceneManager::getInstance()->getScene()->onDownloadError();
			lastProgressHeard = -50000;
		}
	}

	super::update(dt);

}


/// <summary>
/// Download started
/// </summary>
void FirmwareProgressBar::handleDonwloadStarted( String packageID )
{
//	printf("FirmwareProgressBar::handleDonwloadStarted for %s\n", packageID.str());
	if( isVisibleHierarchy() )
	{
		remainingDownloads--;
		if( remainingDownloads == 1 )
		{ setLabel(String("1 download remaining."), ""); }
		else if ( remainingDownloads > 1 )
		{ setLabel(String(remainingDownloads) + " downloads remaining.", ""); }
		else if( remainingDownloads < 0 )
		{ setLabel("There are downloads remaining.", ""); }
	}

	lastProgressHeard = 0;
}

/// <summary>
/// Sets up display for the number of downloads
/// </summary>
void FirmwareProgressBar::setDisplay( int totalDownloads_ )
{
	printf("Firmware pending! Throw progress Bar for %d packages\n", totalDownloads);
	totalDownloads 	   		= totalDownloads_;
	remainingDownloads 		= totalDownloads;
	currentDownloadProgress = 0;
	percentPerDownload		= 1/(float)totalDownloads;
	setProgress(0);

	if( remainingDownloads == 1 )
	{ setLabel(String("1 download remaining."), ""); }
	else if ( remainingDownloads > 1 )
	{ setLabel(String(remainingDownloads) + " downloads remaining.", ""); }
	else if( remainingDownloads <= 0 )
	{ setLabel("There are downloads remaining.", ""); }

	lastProgressHeard = 0;
}



}

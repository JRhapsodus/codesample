#ifndef INSTALLEDITEM_H_
#define INSTALLEDITEM_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class InstalledItem: public ButtonObject
{
	CLASSEXTENDS(InstalledItem, ButtonObject)
	ADD_TO_CLASS_MAP
public:
	InstalledItem(
			SpriteObject* active,
			SpriteObject* deleteicon,
			SpriteObject* icon,
			TextLabel* nameLabel,
			SpriteObject* rim,
			TextLabel* storageLabel
	);
	virtual ~InstalledItem();

	static InstalledItem* deSerialize(FILE* fp);

	virtual void update( float dt );

	/// <summary> 
	/// Transition states from one state to other for subclass animations
	/// </summary> 
	virtual void transition( TransitionState transition );

	/// <summary>
	/// Sets background
	/// </summary>
	void setBackGroundImage( String texture );

	void setStorage(String storageText);
	void setItemName(String nameLabel);
	String getItemName();
	String getStorage();
	PackageInfo* getPackageInfo();

	bool isDeletable();
	void setPackageInfo( PackageInfo* info );
private:
	SpriteObject*	deleteIcon;
	SpriteObject*	gameicon;
	TextLabel*		nameLabel;
	SpriteObject*	rim;
	SpriteObject*	rimSelected;
	TextLabel*		storageLabel;
	PackageInfo* 	packageInfo;
};

}
#endif /* INSTALLEDITEM_H_ */

#ifndef AUDIOITEM_H_
#define AUDIOITEM_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class AudioItem : public ButtonObject
{
	CLASSEXTENDS(AudioItem, ButtonObject);
public:
	AudioItem(SpriteObject* active_, SpriteObject* icon_, TextLabel* nameArtist, SpriteObject* nameplate, TextLabel* nameTitle, SpriteObject* rim_, String songToUse);
	virtual ~AudioItem();

	static AudioItem* deSerialize(FILE* fp);
	virtual void transition( TransitionState transition );
	virtual void update( float dt );
	String getAudioName();

private:
	SpriteObject* active;
	String audioName;
};

}
#endif /* AVATARITEM_H_ */

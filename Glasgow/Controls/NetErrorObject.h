#ifndef NETERROROBJECT_H_
#define NETERROROBJECT_H_

#include "SIEngine/Include/SIDeserialize.h"
#include "SIEngine/Include/SIControls.h"

namespace Glasgow
{

class NetErrorObject : public GameObject
{
	CLASSEXTENDS(NetErrorObject, GameObject);
	ADD_TO_CLASS_MAP;

public:
	NetErrorObject(TextLabel* er, SpriteObject* eI, TextLabel* h, ButtonObject* rB, SpriteObject* wI, ButtonObject* sB );
	virtual ~NetErrorObject();

	///<summary>
	/// Support load from file
	///</summary>
	static NetErrorObject* deSerialize(FILE* fp);

	///<summary>
	/// Sets network error state
	///</summary>
	void displayNetworkError( String errorHeader, String errorString );

	///<summary>
	/// reverse animation of display network error
	///</summary>
	void animateOut();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	TextLabel* 		errorLabel;
	SpriteObject* 	noEthernetIcon;
	SpriteObject* 	noWifiIcon;
	TextLabel* 		errorHeader;
	ButtonObject* 	reconnectButton;
	ButtonObject* 	skipButton;

};

}

#endif

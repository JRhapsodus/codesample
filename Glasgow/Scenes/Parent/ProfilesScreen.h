#ifndef PROFILESSCREEN_H_
#define PROFILESSCREEN_H_

#include "ParentScreen.h"
#include "SIEngine/Include/GlasgowControls.h"

namespace Glasgow
{

class ProfilesScreen : public ParentScreen
{
	CLASSEXTENDS(ProfilesScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	ProfilesScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~ProfilesScreen();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle the press back ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressBack(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual void handleDialogClosed( DialogType dialogType, DialogButtonOption option );

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Bubble out items animation
	/// </summary>
	void bubbleOutItems(float delay, bool all = true);

	/// <summary>
	/// Bubble in items animation
	/// </summary>
	void bubbleInItems( float delay );

	bool itemsHaveActions();

	void setUserData(AvatarItem* item);

	/// <summary>
		/// Base scene feature, tick
		/// </summary>
		virtual void update(float dt);

private:

	/// <summary>
	/// Positions the logins on screen
	/// </summary>
	void positionAvatarLogins();

	/// <summary>
	/// Creates an avatar login per user
	/// </summary>
	void createLoginIcons();

	/// <summary>
	/// remove all avatar logins
	/// </summary>
	void clearLoginIcons();

	/// <summary>
	/// remove all avatar logins
	/// </summary>
	void resetLoginIcons();

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

	String getLocalizedStringFromGradeButtonName(String buttonName);

	void setLocalizedGradeTextInChildren(GameObject* root);

	ButtonObject*	gradeButton;
	CheckButton*	previewsButton;
	ButtonObject*	removeButton;
	ButtonObject*	doneButton;
	ButtonObject*	gradeBackButton;

	TextLabel*		genderLabel;
	TextLabel*		gradeLabel;
	TextLabel*		gradeLevelPrompt;

	TextLabel*		header;
	GameObject*		detailRoot;
	GameObject*		usersRoot;
	GameObject*		infoRoot;
	GameObject*		actionsRoot;
	GameObject* 	gradeLevelRoot;

	Vec3f			origAvatarItemPos;
	bool			isEditing;
	bool			isEditingGrade;

	float 			resetAvatarsTimer;
	bool			needsAvatarReset;

	AvatarItem* 	selectedAvatar;
	ObjectArray 	avatarLogins;

	float 			resetProfilesTimer;
};

}
#endif

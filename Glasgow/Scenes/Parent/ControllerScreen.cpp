#include "ControllerScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

ControllerScreen::ControllerScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	connectStep1Icon		= findSprite("connectStep1");
	connectStep2Icon		= findSprite("connectStep2");
	connectHeaderLabel		= findLabel("header");
	connectSubHeaderLabel	= findLabel("connectSubHeader");

	errorPrompt 	= findLabel("errorPrompt");
	errorIcon		= findSprite("errorIcon");

	congratulationsRoot		= find("congratulationsRoot");

	setupInitialState();
}

void ControllerScreen::setupInitialState()
{
	congratulationsRoot->setVisibleRecursive(false);

	hasPairedController 	= false;
	isShowingError			= false;

	errorIcon->setVisible(false);
	errorPrompt->setVisible(false);
}


void ControllerScreen::loadGameObjectState(GameObjectState state)
{
	printf("ControllerScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

ControllerScreen::~ControllerScreen()
{
	printf("ControllerScreen::~ControllerScreen\n");
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ControllerScreen::handlePressAction(GameObject*& objectHighlight)
{
	if (hasPairedController)
	{
		return true;
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ControllerScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ControllerScreen::cameraArrived()
{
	printf("ControllerScreen::cameraArrived()\n");
	parentScene->showHelpButton(0.75f);
	parentScene->showBackButton(0.75f,true);

	connectHeaderLabel->makeVisible(true);
	connectHeaderLabel->setTextColor(Color(1,1,1,0));
	connectHeaderLabel->colorTo(Color(1,1,1,1),1);

	connectSubHeaderLabel->makeVisible(true);
	connectSubHeaderLabel->setTextColor(Color(1,1,1,0));
	connectSubHeaderLabel->colorTo(Color(1,1,1,1),1);

	congratulationsRoot->setVisibleRecursive(false);

	float waitTime = 1.0f;

	connectStep1Icon->setVisible(false);
	connectStep1Icon->waitFor(waitTime);
	connectStep1Icon->moveBy( Vec3f::Left*800, 0);
	connectStep1Icon->makeVisible(true);
	connectStep1Icon->moveBy( Vec3f::Right*800, 1);

	connectStep2Icon->setVisible(false);
	connectStep2Icon->waitFor(waitTime);
	connectStep2Icon->moveBy( Vec3f::Right*800, 0);
	connectStep2Icon->makeVisible(true);
	connectStep2Icon->moveBy( Vec3f::Left*800, 1);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ControllerScreen::getArrivalSelection()
{ return NULL; }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ControllerScreen::addHotSpots( ObjectArray* trackingList )
{}

/// <summary>
/// Controller Sync Event
/// </summary>
void ControllerScreen::handleControllerSync(bool success)
{
	printf("ControllerScreen::handleControllerSync  -- %B", success);

	// If the controller failed to synch then show the error message
	if(!success)
	{
		// Show the synch error if we're not already showing it and if the weclcome intro has finished playing
		if(!isShowingError && !hasPairedController)
		{
			showSynchError();
			hideSynchInstructions();
			// Dismiss the error with a timeout
			TimerManager::addTimer("HideSynchError", 10, 0);
		}
	}
}

/// <summary>
/// Controller Connected Event
/// </summary>
void ControllerScreen::handleControllerConnected()
{
	if(!hasPairedController)
	{
		pairingComplete();
	}
}

/// <summary>
/// Controller Disconnected Event
/// </summary>
void ControllerScreen::handleControllerDisconnected()
{
	printf("ControllerScreen::handleControllerDisconnected");
}

/// <summary>
/// pairing of controller success
/// </summary>
void ControllerScreen::pairingComplete()
{
	printf("Pairing Complete\n");

	if(isShowingError)
	{
		TimerManager::cancelTimer("HideSynchError");
		hideSynchError();
	}

	hasPairedController = true;

	hideSynchInstructions();
	showCongratulations();
}

void ControllerScreen::showSynchError()
{
	isShowingError = true;

	errorPrompt->fadeIn(0.5f);
	errorIcon->fadeIn(0.5f);
}

void ControllerScreen::hideSynchError()
{
	errorPrompt->fadeOut(0);
	errorIcon->fadeOut(0);

	isShowingError = false;
}

void ControllerScreen::showSynchInstructions()
{
	connectHeaderLabel->fadeIn(0);
	connectSubHeaderLabel->fadeIn(0);

	connectStep1Icon->makeVisible(true);
	connectStep2Icon->makeVisible(true);
	connectStep1Icon->moveTo(Vec3f(-282, -32, 0), 1);
	connectStep2Icon->moveTo(Vec3f(263, -39, 0), 1);
}

void ControllerScreen::hideSynchInstructions()
{
	connectHeaderLabel->fadeOut(0);
	connectSubHeaderLabel->fadeOut(0);

	connectStep1Icon->moveBy( Vec3f::Left*800, 1);
	connectStep2Icon->moveBy( Vec3f::Right*800, 1);
	connectStep1Icon->makeVisible(false);
	connectStep2Icon->makeVisible(false);
}

void ControllerScreen::showCongratulations()
{
	congratulationsRoot->setVisibleRecursive(true);
	congratulationsRoot->recursiveFadeIn(0);
}

} /* namespace Glasgow */

#include "PurchasedManagerScreen.h"
#include "ParentScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

PurchasedManagerScreen::PurchasedManagerScreen( GameObject* objectRoot, ParentScene* scene ) :
		super(objectRoot, scene)
{
	pauseButton = findButton("pauseAllButton");
	pauseButton->setVisible(false);
	headerLabel = findLabel("headerLabel");
	noPurchasesLabel = findLabel("noPurchasesLabel");
	noPurchasesLabel->setVisible(false);

	purchasedItems = new ObjectArray();

}
/// <summary>
/// If we want to handle analog changes
/// </summary>
bool PurchasedManagerScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	printf("PurchasedManagerScreen::handleAnlogChange");
	return super::handleAnlogChange(objectLeft, objectEntered, currentHighlight);
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool PurchasedManagerScreen::handlePressAction(GameObject*& objectHighlight)
{
	printf("PurchasedManagerScreen::handlePressAction");

	return super::handlePressAction(objectHighlight);
}
/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void PurchasedManagerScreen::cameraArrived()
{
	parentScene->showBackButton(0.25f, true);

	discoverPurchasedItems();
	addPurchasedItemUIToScreen();
}

/// <summary>
/// Populate the internal purchased item array
/// </summary>
void PurchasedManagerScreen::discoverPurchasedItems()
{
	//TODO: Set this var to actually grab the real number from somewhere???
	ObjectArray* purchases = ContentManager::getInstance()->getPurchasablePackages();
	int numberOfPurchasedItems = purchases->getSize();

	for ( int i = 0; i < numberOfPurchasedItems; i++ )
	{
		TRelease<PurchasedItem> purchase((PurchasedItem*) Scene::instantiate(PurchasedItem::type()));
		purchase->name = String("PI") + String(i);
		purchasedItems->addElement(purchase);
		purchase->setPackageInfo( (PackageInfo*)purchases->elementAt(i));
	}
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void PurchasedManagerScreen::addHotSpots( ObjectArray* trackingList )
{
	for ( int i = 0; i < purchasedItems->getSize(); i++ )
	{
		trackingList->addElement(purchasedItems->elementAt(i));
	}
}

/// <summary>
/// Add purchased Item UI elements to the screen
/// </summary>
void PurchasedManagerScreen::addPurchasedItemUIToScreen()
{
	if ( purchasedItems->getSize() < 1 )
	{
		noPurchasesLabel->setVisible(true);
	}
	else
	{
		float purchasedItemVertSpacing = 100.f;
		for ( int i = 0; i < purchasedItems->getSize(); i++ )
		{

			((PurchasedItem*) purchasedItems->elementAt(i))->setPosition(
					noPurchasesLabel->getPosition() - Vec3f(0, purchasedItemVertSpacing * i, 0));
			addChild((PurchasedItem*) purchasedItems->elementAt(i));
		}
	}
}

PurchasedManagerScreen::~PurchasedManagerScreen()
{
	printf("PurchasedManagerScreen::~PurchasedManagerScreen\n");
	SAFE_RELEASE(purchasedItems);
}

/// <summary>
/// Handle package changed
/// </summary>
void PurchasedManagerScreen::handlePackageChanged( tPackageStatus newStatus, PackageInfo* info )
{
	printf("PurchasedManagerScreen::handlePackageChanged!\n packageID: %s status: %s progress: %d\n",
			info->packageID.str(), info->statusAsString.str(), info->downloadProgress);
}

/// <summary>
/// Base scene feature, tick
/// </summary>
void PurchasedManagerScreen::update( float dt )
{
	super::update(dt);
}

} /* namespace Glasgow */

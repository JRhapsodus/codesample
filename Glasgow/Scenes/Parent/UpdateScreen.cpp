#include "UpdateScreen.h"
#include "ParentScene.h"
#include <sys/reboot.h>
#include <signal.h>

namespace Glasgow
{
float UpdateScreen::dlFinishedTimer = 20;
float UpdateScreen::dlProgressTest = 0;
float UpdateScreen::oneSecondTimer = 1;
UpdateScreen::UpdateScreen(GameObject* root, ParentScene* scene) : super(root, scene)
{
	systemUpdatedRoot 	 = find("completeRoot");
	systemUpdatingRoot	 = find("updatingRoot");
	updateButton		 = systemUpdatingRoot->findButton("updateButton");
	countdownLabel 		 = systemUpdatingRoot->findLabel("updatePromptCountdown");

	createObjects();

	setupInitialState();
}

UpdateScreen::~UpdateScreen()
{
	printf("UpdateScreen::~UpdateScreen()\n");
	SAFE_RELEASE(netError);
	SAFE_RELEASE(progressBar);
}

void UpdateScreen::setupInitialState()
{
	retestTimer = 0;
	readyTimer	= 0;
	rebootTimer = 0;
	killTimer 	= 0;
	errorShown	= false;
	countdownLabel->setVisible(false);
}

void UpdateScreen::loadGameObjectState(GameObjectState state)
{
	printf("UpdateScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// Create netError and progressBar
/// </summary>
void UpdateScreen::createObjects()
{
	netError = (NetErrorObject*)Scene::instantiate(NetErrorObject::type()); //ref1
	find("netErrorContainer")->addChild(netError);//ref 2 ::~ -> ref 1
	netError->setVisible(false);
	netError->setPosition(Vec3f::Zero);

	progressBar = (FirmwareProgressBar*)Scene::instantiate(FirmwareProgressBar::type());

	find("updatingRoot")->find("progressBarContainer")->addChild(progressBar);
	progressBar->setVisible(false);
	progressBar->setPosition(Vec3f::Zero);
	progressBar->setCullingLayerRecursive(parentScene->getCamera(0)->getCullingMask());
}

/// <summary>
/// Over ridden for timer based polling in ContentManager
/// </summary>
void UpdateScreen::update(float dt)
{
	if( killTimer > 0 )
	{
		killTimer -= dt;
		if( killTimer <= 0 )
		{
			printf("Rebooting due to firmware update !\n");
			sync();
			::kill(1, 15);
		}
	}

	if( rebootTimer > 0 )
	{
		int secondsLeft = (int)rebootTimer;

		//TODO Localization
		if ( secondsLeft > 1 )
		{ countdownLabel->setText(String("Your LeapTV will automatically restart in ") + String(secondsLeft) + String(" seconds.")); }
		else
		// TODO: Localize strings
		{ countdownLabel->setText(String("Your LeapTV will automatically restart in 1 second.")); }

		rebootTimer -= dt;
		if ( rebootTimer <= 0 )
		{
			killTimer = 1;
			parentScene->screenDarken->setVisible(true);
			parentScene->screenDarken->setOpacity(0);
			parentScene->screenDarken->fadeTo(1, 0.5f);
		}
	}

	//Enable to test the progress bar interface
	//testProgressBarUpdate(dt);
	firmwareTestUpdate(dt);
	super::update(dt);
}

/// <summary>
/// In case package manager discovers new downloads the progress bar must be 'reset' with the new data
/// </summary>
void UpdateScreen::handlePendingChanged()
{
	printf("UpdateScreen::handlePendingChanged()\n");

	if( progressBar->isVisibleHierarchy() )
	{
		FirmwareStatus status = ContentManager::getInstance()->getFirmwareStatus();
		if ( status == FirmwarePending )
		{
			progressBar->setDisplay(ContentManager::getInstance()->getDownloadsRemaining());
			readyTimer = 0;
		}
		else if ( status == FirmwareReady )
		{
			readyTimer = 2;
		}

	}
}

/// <summary>
/// Called before showing progress bar
/// </summary>
void UpdateScreen::firmwareTestUpdate(float dt)
{
	if( readyTimer > 0 )
	{
		readyTimer-= dt;
		if( readyTimer< 0 )
		{
			printf("Asking package manager if it is ready\n");
			if( ContentManager::getInstance()->getFirmwareStatus() == FirmwareReady )
			{
				printf("Package manager says it is now ready\n");
				rebootTimer = 20;
				countdownLabel->fadeIn(0.25f);
				parentScene->setCurrentHighlight(updateButton);
				updateButton->setState(ButtonStateActive);
				updateButton->cancelActions();
				updateButton->bubbleIn(0.5f);
			}
			else
			{
				printf("Firmware packages are downloaded but package manager says it is not ready!!??\n");
				readyTimer = 2;
			}
		}
	}
}

/// <summary>
/// Test function for simulating with the package manager interface
/// </summary>
void UpdateScreen::testProgressBarUpdate(float dt)
{
	if( !errorShown )
	{
		if( dlFinishedTimer > 0 )
		{
			dlFinishedTimer -= dt;
			if( dlFinishedTimer <= 0 )
			{
				handleDonwloadFinished("MOO");
				handleDonwloadStarted("MOO2");
				dlFinishedTimer= 30;
			}
		}

		if( oneSecondTimer > 0 )
		{
			oneSecondTimer -= dt;
			if( oneSecondTimer <= 0 )
			{
				oneSecondTimer = 1;
				dlProgressTest = 1- (dlFinishedTimer/30);
				dlProgressTest = (dlProgressTest>1)?1:dlProgressTest;
				handleDownloadProgress("SOME PACKAGE", (int)(100*dlProgressTest));
			}
		}
	}
}

/// <summary>
/// Animation transition into a networking error
/// </summary>
void UpdateScreen::showNetworkError( String header, String error )
{
	printf("UpdateScreen::showNetworkError => [%s]", error.str());

	systemUpdatingRoot->find("icon")->cancelActions();
	progressBar->cancelActions();
	systemUpdatingRoot->findLabel("downloadPrompt")->cancelActions();
	systemUpdatingRoot->findLabel("updateHeader")->cancelActions();
	systemUpdatingRoot->find("updatePrompt")->cancelActions();

	systemUpdatingRoot->find("icon")->bubbleOut(0);
	progressBar->bubbleOut(0);
	systemUpdatingRoot->findLabel("downloadPrompt")->fadeOut(0);
	systemUpdatingRoot->findLabel("updateHeader")->fadeOut(0);
	netError->setVisible(true);
	netError->displayNetworkError(header, error);
	parentScene->setupHotSpots();
	parentScene->showBackButton(0.5,false);
	parentScene->setCurrentHighlight(netError->reconnectButton);
	errorShown = true;
}

/// <summary>
/// Remove network error
/// </summary>
void UpdateScreen::hideNetworkError()
{
	if (errorShown)
	{
		netError->animateOut();
		progressBar->bubbleIn(0.5f);
		systemUpdatingRoot->find("icon")->bubbleIn(0.5f);
		systemUpdatingRoot->findLabel("downloadPrompt")->fadeIn(0.5f);
		systemUpdatingRoot->findLabel("updateHeader")->fadeIn(0.5f);
		parentScene->hideBackButton();
		errorShown = false;
	}

	parentScene->setupHotSpots();
}

/// <summary>
/// Network error occured, thrown by connman
/// </summary>
void UpdateScreen::handleNetworkError(String networkName, String networkError)
{
	printf("UpdateScreen::handleNetworkError ==> %s\n", networkError.str());

	//No need to care if network goes down when system is ready to update
	if( progressBar->isVisibleHierarchy() )
	{ showNetworkError("Check Connection", networkError); }

	parentScene->setupHotSpots();
}

/// <summary>
/// Thrown by package-manager
/// </summary>
void UpdateScreen::handleDownloadError()
{
	// what to do ?!
	//No need to care if network goes down when system is ready to update
	if( isVisibleHierarchy() )
	{
		DialogOptions options;
		options.title   = "Check Connection";
		options.prompt  = "Download could not complete, you can try updating your LeapTV at a later time.";
		options.buttonA = DialogButtonOption("Update Later", "latersskaters");
		parentScene->showModalDialog(DialogTypeDownloadError, options);
		return;
	}

	parentScene->setupHotSpots();
	super::handleDownloadError();
}


/// <summary>
/// Network connection made
/// </summary>
void UpdateScreen::handleNetworkConnected( String networkName )
{
	if(errorShown)
	{
		hideNetworkError();
	}

	calculateDownloadInfo();
}

/// <summary>
/// Download of the current package has changed [0,100]
/// </summary>
void UpdateScreen::handleDownloadProgress( String package, int downloadProgress)
{
	progressBar->handleDownloadProgress(package, downloadProgress);
}

/// <summary>
/// Tests the status for the requirement of a firmware update and update
/// the required UI that may be needed
/// </summary>
void UpdateScreen::checkFirmwareUpdate()
{
	printf("Checking to see if firmware update is ready, pending or none\n");

	FirmwareStatus status = ContentManager::getInstance()->getFirmwareStatus();

	if (  status == FirmwareReady)
	{
		printf("Firmware is ready to install!\n");
		systemUpdatedRoot->setVisible(false);
		systemUpdatingRoot->setVisible(true);
		countdownLabel->fadeIn(0.25f);
		systemUpdatingRoot->find("downloadPrompt")->setVisible(false);
		systemUpdatingRoot->find("progressBarContainer")->setVisible(false);
		updateButton->bubbleIn(0.5f);
		parentScene->setCurrentHighlight(updateButton);
		rebootTimer = 20;
	}
	else if ( status  == FirmwarePending)
	{
		calculateDownloadInfo();
		progressBar->setVisible(true);
		systemUpdatingRoot->find("updatePrompt")->setVisible(false);
		systemUpdatedRoot->setVisible(false);
		updateButton->setVisible(false);

		if( netError->isVisibleHierarchy())
		{ parentScene->showBackButton(1.25f, true); }
	}
	else
	{
		printf("No firmware is needed to update\n");
		progressBar->setVisible(false);
		updateButton->setVisible(false);
		// TODO: Localize strings
		systemUpdatingRoot->findLabel("downloadPrompt")->setText("Your LeapTV is already up to date!\n");
		Vec3f pos = systemUpdatingRoot->findLabel("downloadPrompt")->getPosition();
		pos.x = 0;
		systemUpdatingRoot->findLabel("downloadPrompt")->setPosition(pos);
		systemUpdatingRoot->find("progressBarContainer")->setVisible(false);
		systemUpdatingRoot->find("updatePrompt")->setVisible(false);
		systemUpdatedRoot->setVisible(false);
		parentScene->showBackButton(1,true);
	}
}

/// <summary>
/// Called before showing progress bar
/// </summary>
void UpdateScreen::calculateDownloadInfo()
{
	progressBar->setDisplay( ContentManager::getInstance()->getDownloadsRemaining() );
}

/// <summary>
/// Animation helper
/// </summary>
void UpdateScreen::showFinished()
{
	progressBar->bubbleOut(0);
	systemUpdatingRoot->find("downloadPrompt")->fadeOut(0);
	systemUpdatingRoot->find("updatePrompt")->fadeIn(0.25f);

	printf("UpdateScreen::showFinished()\n");
	printf("Ask package manager if it is ready since we are finished with the download\n");
	if( ContentManager::getInstance()->getFirmwareStatus() == FirmwareReady )
	{
		printf("Package manager says it is ready\n");
		countdownLabel->fadeIn(0.25f);
		updateButton->bubbleIn(0.5f);
		parentScene->setCurrentHighlight(updateButton);

		rebootTimer = 20;
	}
	else
	{
		printf("Firmware packages are downloaded but package manager says it is not ready!!??\n");
		readyTimer = 2;
	}
}

/// <summary>
/// Download finished
/// </summary>
void UpdateScreen::handleDonwloadFinished( String packageID )
{
	if (!progressBar->getIsFinished() )
	{
		progressBar->handleDonwloadFinished(packageID);
		if( progressBar->getIsFinished() )
		{ showFinished(); }
	}

	printf("UpdateScreen::handleDonwloadFinished for %s\n", packageID.str());
}

/// <summary>
/// Download started
/// </summary>
void UpdateScreen::handleDonwloadStarted( String packageID )
{
	if (!progressBar->getIsFinished() )
	{ progressBar->handleDonwloadStarted(packageID); }
}

/// <summary>
/// Needed for firmware dialog prompt
/// </summary>
void UpdateScreen::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	printf("UpdateScreen::handleDialogClosed\n");
	if(dialogType == DialogTypeDownloadError)
	{parentScene->popScreen();}

	super::handleDialogClosed(dialogType, option);
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void UpdateScreen::cameraArrived()
{
	printf("UpdateScreen::cameraArrived!\n");
	parentScene->showHelpButton(1.0f,false);

	//post firmware update
	if( AssetLibrary::fileExists(Constants::Paths::flagFirmwareInstalled) )
	{
		printf("%s exists!\n", Constants::Paths::flagFirmwareInstalled.str());
		AssetLibrary::removeFile(Constants::Paths::flagFirmwareInstalled);
		String firmwareVersion = SystemSettings::getFirmwareVersion();

		//parentScene->showBackButton(1,true);
		systemUpdatedRoot->setVisible(true);
		systemUpdatingRoot->setVisible(false);
		parentScene->showBackButton(1,true);

		progressBar->setVisible(false);
		// TODO: Localize strings
		systemUpdatedRoot->findLabel("firmwareVersion")->setText(SystemSettings::getFirmwareVersion());
	}
	else
	{
		printf("[%s] does not exist!\n", Constants::Paths::flagFirmwareInstalled.str());
		checkFirmwareUpdate();
	}
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void UpdateScreen::cameraLeaving()
{
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void UpdateScreen::addHotSpots( ObjectArray* trackingList )
{
	printf("UpdateScreen::addHotSpots\n");

	if( updateButton->isVisibleHierarchy() )
	{ trackingList->addElement(updateButton); }

	if( netError->isVisibleHierarchy() )
	{ netError->addHotSpots(trackingList); }

	super::addHotSpots(trackingList);
}

/// <summary>
/// If we want to handle the press action ourselves, usually welcomeHeaderLabelmeans the
/// action click is self contained within the page
/// </summary>
bool UpdateScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(objectHighlight != null)
	{
		if( objectHighlight == updateButton && updateButton->isVisibleHierarchy() )
		{
			killTimer = 1;
			parentScene->screenDarken->setVisible(true);
			parentScene->screenDarken->setOpacity(0);
			parentScene->screenDarken->fadeTo(1, 0.5f);
			return true;
		}
		else if(objectHighlight->name == "reconnectButton" && objectHighlight->isVisibleHierarchy() )
		{
			parentScene->pushScreen(ParentScene::PS_NETWORK_SCREEN);
			return true;
		}
	}
	return super::handlePressAction(objectHighlight);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* UpdateScreen::getArrivalSelection()
{
	printf("UpdateScreen::getArrivalSelection()\n");

	if( systemUpdatingRoot->isVisibleHierarchy() )
	{ return updateButton; }

	return null;
}


} /* namespace Glasgow */

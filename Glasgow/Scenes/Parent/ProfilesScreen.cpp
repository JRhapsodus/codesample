#include "ProfilesScreen.h"
#include "ParentScene.h"
namespace Glasgow
{

ProfilesScreen::ProfilesScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	detailRoot = findLabel("detailRoot");
	usersRoot = find("usersRoot");
	infoRoot = detailRoot->find("infoRoot");
	actionsRoot = detailRoot->find("actionsRoot");
	gradeLevelRoot = detailRoot->find("gradeLevelRoot");
	header = findLabel("header");

	gradeButton = actionsRoot->findButton("gradeButton");
	previewsButton = (CheckButton*)actionsRoot->find("previewsButton");
	removeButton = actionsRoot->findButton("removeButton");
	doneButton = detailRoot->findButton("doneButton");
	gradeBackButton = detailRoot->findButton("gradeBackButton");

	genderLabel = infoRoot->findLabel("genderLabel");
	gradeLabel = infoRoot->findLabel("gradeLabel");
	gradeLevelPrompt = detailRoot->findLabel("gradeLevelPrompt");

	setupInitialState();
}

ProfilesScreen::~ProfilesScreen()
{
	printf("ProfilesScreen::~ProfilesScreen");
}

void ProfilesScreen::setupInitialState()
{
	needsAvatarReset = false;
	resetAvatarsTimer = 0;
	selectedAvatar = NULL;
	isEditing = false;
	isEditingGrade = false;

	clearLoginIcons();

	doneButton->setVisible(false);
	gradeBackButton->setVisible(false);
	infoRoot->setVisible(false);
	actionsRoot->setVisible(false);
	gradeLevelRoot->setVisible(false);
	gradeLevelPrompt->setVisible(false);

	setLocalizedGradeTextInChildren(gradeLevelRoot);
}

String ProfilesScreen::getLocalizedStringFromGradeButtonName(String buttonName)
{
	GradeInfo* gradeInfo = GradeInfo::getGradeInfo(buttonName);
	return StringsManager::getInstance()->getLocalizedString(gradeInfo->localizationKey);
}

void ProfilesScreen::setLocalizedGradeTextInChildren(GameObject* root)
{
	for( int i = 0; i<root->numChildren(); i++ )
	{
		GameObject* child = root->childAtIndex(i);

		if(child->typeOf(ButtonObject::type()))
		{
			TextLabel *label = child->findLabel("text");
			label->setText(getLocalizedStringFromGradeButtonName(child->name));
		}
	}
}

void ProfilesScreen::loadGameObjectState(GameObjectState state)
{
	printf("ProfilesScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ProfilesScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(needsAvatarReset)
		return false;

	if(objectHighlight != null)
	{
		if(objectHighlight->descendantOf(detailRoot))
		{
			User* user = selectedAvatar->getUser();

			if(isEditingGrade)
			{
				parentScene->lockOutInput(1.25f);

				isEditingGrade = false;

				// If the user didn't select the back button, then by process of elimination they choose a grade button
				// So update this profile's grade
				if(objectHighlight != gradeBackButton)
				{
					String n = ((CheckButton*)objectHighlight)->name;
					GradeInfo* grade = GradeInfo::getGradeInfo(n);
					if(grade == null)
						ThrowException::exception("No valid gradeInfo found.\n");

					user->setGrade(grade->leapGradeEnum);
					user->save();
				}

				selectedAvatar->setUser(user);
				setUserData(selectedAvatar);

				gradeBackButton->bubbleOut(0.5f);

				float delay = 0.25f;
				for( int i = 0; i<gradeLevelRoot->getChildren()->getSize(); i++ )
				{
					BaseObject* bo = gradeLevelRoot->getChildren()->elementAt(i);
					if(bo->typeOf(GameObject::type()))
					{
						GameObject* child = (GameObject*)bo;
						if(child != objectHighlight)
							child->cancelActionsAsComplete();
						child->bubbleOut(delay);
					}
				}

				gradeLevelPrompt->fadeOut();

				infoRoot->waitFor(1);
				infoRoot->makeVisible(true);
				infoRoot->recursiveFadeIn(1);
				actionsRoot->waitFor(1);
				actionsRoot->makeVisible(true);
				actionsRoot->bubbleInChildren(1,0.05f);
				selectedAvatar->bubbleIn(1);

				doneButton->bubbleIn(1.25f);

				parentScene->setCurrentHighlight(gradeButton);
				parentScene->setupHotSpots();
				return true;
			}
			else if(isEditing)
			{
				if(objectHighlight == doneButton)
				{
					parentScene->lockOutInput(1);

					user->save();

					isEditing = false;

					doneButton->setState(ButtonStateNormal);
					doneButton->bubbleOut(0);
					objectHighlight = null;

					actionsRoot->bubbleOutChildren(0,0.05f);
					infoRoot->recursiveFadeOut(0);

					selectedAvatar->cancelActions();
					selectedAvatar->moveTo(origAvatarItemPos, 0.75f);
					selectedAvatar->makeState(ButtonStateActive);
					parentScene->removeSelection();
					objectHighlight = selectedAvatar;

					bubbleInItems(1);
					parentScene->showBackButton(1);
					parentScene->setupHotSpots();
					return true;
				}
				else if(objectHighlight == gradeButton)
				{
					parentScene->lockOutInput(1);

					isEditingGrade = true;

					gradeButton->setState(ButtonStateNormal);
					objectHighlight = null;

					doneButton->bubbleOut(0);
					actionsRoot->bubbleOutChildren(0,0.05f);
					infoRoot->recursiveFadeOut(0);

					selectedAvatar->cancelActions();
					selectedAvatar->bubbleOut(0);

					gradeLevelPrompt->fadeIn(0.5f);
					gradeLevelRoot->waitFor(0.5f);
					gradeLevelRoot->makeVisible(true);
					gradeLevelRoot->bubbleInChildren(0.5f,0.01f);

					gradeBackButton->bubbleIn(0.5f);

					parentScene->setCurrentHighlight(gradeLevelRoot->findButton("gradePreschool"));
					parentScene->setupHotSpots();
					return true;
				}
				else if(objectHighlight == previewsButton)
				{
					bool previewsOn = user->getSneakPeeksEnabled();
					user->setSneakPeeksEnabled(!previewsOn);
					user->save();
					previewsButton->setChecked(!previewsOn);
					return true;
				}
				else if(objectHighlight == removeButton)
				{
					parentScene->lockOutInput(0.25f);
					removeButton->setState(ButtonStateNormal);
					objectHighlight = null;

					// TODO: Localize strings
					DialogOptions options;
					options.title 	= "Delete Profile";
					options.prompt  = "Are you sure you want to delete this profile?";
					options.buttonA = DialogButtonOption("Yes", "Yes");
					options.buttonB = DialogButtonOption("No", "No");
					options.ignoreOldTrackedObjects = true;
					parentScene->showModalDialog(DialogTypeDeleteProfile, options);
					return true;
				}
			}
		}
		else
		{

			//check for avatar items pressed

			bool isAvatar = false;
			for(int i = 0; i < avatarLogins.getSize(); ++i)
			{
				AvatarItem* user = (AvatarItem*) avatarLogins[i];
				if(objectHighlight == user)
				{
					isAvatar = true;
					break;
				}
			}

			if(isAvatar && selectedAvatar != null)
			{
				if(selectedAvatar->getIsNewUser())
				{
					selectedAvatar->setState(ButtonStateNormal);
					objectHighlight = null;
					parentScene->lockOutInput(2);
					parentScene->pushScreen(ParentScene::PS_EDIT_PROFILES_SCREEN);
					return true;
				}
				else
				{
					parentScene->hideBackButton(0);
					parentScene->lockOutInput(2);

					((AvatarItem*)objectHighlight)->setState(ButtonStateNormal);
					objectHighlight = null;
					isEditing = true;
					bubbleOutItems(0, false);

					origAvatarItemPos = selectedAvatar->getPosition();
					selectedAvatar->waitFor(0.25f);
					selectedAvatar->moveTo(Vec3f(-125,13,0),0.75f);

					infoRoot->waitFor(0.75f);
					infoRoot->makeVisible(true);
					infoRoot->recursiveFadeIn(0.75f);
					actionsRoot->waitFor(1);
					actionsRoot->makeVisible(true);
					actionsRoot->bubbleInChildren(1,0.05f);
					doneButton->bubbleIn(1);

					parentScene->setCurrentHighlight(gradeButton);
					parentScene->setupHotSpots();

					return true;
				}
			}
		}


	}

	return false;
}

/// <summary>
/// If we want to handle the press back ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ProfilesScreen::handlePressBack(GameObject*& objectHighlight)
{
	if(needsAvatarReset)
		return false;

	if(isEditing)
	{
		if(isEditingGrade)
		{
			parentScene->lockOutInput(1);
			isEditingGrade = false;

			gradeLevelRoot->bubbleOutChildren(0.25f,0);
			gradeLevelPrompt->fadeOut();

			gradeBackButton->bubbleOut(0.25f);

			infoRoot->waitFor(1);
			infoRoot->makeVisible(true);
			infoRoot->recursiveFadeIn(1);
			actionsRoot->waitFor(1);
			actionsRoot->makeVisible(true);
			actionsRoot->bubbleInChildren(1,0.05f);
			selectedAvatar->bubbleIn(1);

			doneButton->bubbleIn(1.25f);

			parentScene->setCurrentHighlight(gradeButton);
			parentScene->setupHotSpots();
		}
		else
		{
			parentScene->lockOutInput(1);
			isEditing = false;

			doneButton->bubbleOut(0);

			actionsRoot->bubbleOutChildren(0,0.05f);
			infoRoot->recursiveFadeOut(0);

			selectedAvatar->cancelActions();
			selectedAvatar->moveTo(origAvatarItemPos, 0.75f);
			selectedAvatar->makeState(ButtonStateActive);
			parentScene->removeSelection();
			objectHighlight = selectedAvatar;

			bubbleInItems(1);

			parentScene->showBackButton(1);
			parentScene->setupHotSpots();
		}
	}
}

void ProfilesScreen::setUserData(AvatarItem* item)
{
	if(item == null)
		return;

	if(item->getIsNewUser())
	{
		//todo: localization insert
		header->setText("New User");
	}
	else
	{
		User* user = item->getUser();
		header->setText(user->getDisplayName());

		GradeInfo* gradeInfo = GradeInfo::getGradeInfo(user->getGrade());
		gradeLabel->setText(StringsManager::getInstance()->getLocalizedString(gradeInfo->localizationKey));

		String genderKey = user->getGender() == Constants::System::genderMaleKey ? "PROFILE_GENDER_MALE" : "PROFILE_GENDER_FEMALE";
		genderLabel->setText(StringsManager::getInstance()->getLocalizedString(genderKey));
		previewsButton->setChecked(user->getSneakPeeksEnabled());

		ObjectArray* children = gradeLevelRoot->getChildren();
		for(int i = 0; i < children->getSize(); ++i)
		{
			BaseObject* obj = children->elementAt(i);
			if(obj->typeOf(CheckButton::type()))
			{
				if(((CheckButton*)obj)->name == gradeInfo->gradeName)
					((CheckButton*)obj)->setChecked(true);
				else
					((CheckButton*)obj)->setChecked(false);

				((CheckButton*)obj)->setState(ButtonStateNormal);
			}
		}


		if(avatarLogins.getSize()<=2)
		{ removeButton->disable(); }
		else
		{ removeButton->enable(); }
	}
}

/// <summary>
/// Overridden to snoop for analog changes
/// </summary>
bool ProfilesScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	if(isEditing)
		return false;

	if( objectEntered != NULL && objectEntered->typeOf(AvatarItem::type()) )
	{
		if( selectedAvatar != objectEntered )
		{
			for ( int i = 0; i < avatarLogins.getSize(); i++ )
			{
				AvatarItem* user = (AvatarItem*) avatarLogins[i];
				user->scaleTo( user->originalScale, 0.25f );
			}

			selectedAvatar = (AvatarItem*)objectEntered;
			setUserData(selectedAvatar);

			parentScene->playSound("Base_Highlight");
		}
	}
	else
	{
		for ( int i = 0; i < avatarLogins.getSize(); i++ )
		{
			AvatarItem* user = (AvatarItem*) avatarLogins[i];
			user->scaleTo( user->originalScale, 0.25f );
		}
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
void ProfilesScreen::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	if(dialogType == DialogTypeDeleteProfile)
	{
		if(option.id == "Yes")
		{
			parentScene->lockOutInput(2);
			UsersManager::getInstance()->deleteUser(selectedAvatar->getUser());

			printf("ProfilesScreen::handleDialogClosed DELETED USER\n");

			isEditing = false;
			doneButton->setState(ButtonStateNormal);
			doneButton->bubbleOut(0);
			actionsRoot->bubbleOutChildren(0,0.05f);
			infoRoot->recursiveFadeOut(0);

			selectedAvatar = null;

			resetLoginIcons();

			for(int i = 0; i < avatarLogins.getSize(); ++i)
			{
				AvatarItem* avatarItem = (AvatarItem*)avatarLogins[i];
				avatarItem->setVisible(false);
			}

			bubbleInItems(1);
			selectedAvatar = (AvatarItem*)avatarLogins[0];
			setUserData(selectedAvatar);

			parentScene->showBackButton(1);

			// we want to get a call on dialog close finished to
			// set a new selection
			parentScene->setCurrentHighlight(selectedAvatar);
		}
		else
		{
			parentScene->setCurrentHighlight(detailRoot->findButton("doneButton"));
		}
	}
	super::handleDialogClosed(dialogType,option);
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ProfilesScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f,false);

	resetLoginIcons();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ProfilesScreen::getArrivalSelection()
{
	printf("ProfilesScreen::getArrivalSelection()\n");

	//return first object
	if(avatarLogins.getSize() > 0)
	{
		selectedAvatar = (AvatarItem*)avatarLogins[0];
		setUserData(selectedAvatar);

		return selectedAvatar;
	}

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ProfilesScreen::addHotSpots( ObjectArray* trackingList )
{
	if(needsAvatarReset)
		return;

	printf("ProfilesScreen::addHotSpots\n");
	if(isEditing)
	{
		if(isEditingGrade)
		{
			for(int i = 0; i < gradeLevelRoot->getChildren()->getSize(); ++i)
			{
				BaseObject* bo = gradeLevelRoot->childAtIndex(i);
				if(bo->typeOf(ButtonObject::type()))
				{
					trackingList->addElement((ButtonObject*)bo);
				}

			}
			trackingList->addElement((ButtonObject*)gradeBackButton);
		}
		else
		{
			trackingList->addElement(removeButton);
			trackingList->addElement(previewsButton);
			trackingList->addElement(gradeButton);
			trackingList->addElement(doneButton);
		}
	}
	else
	{
		//add elements from usersroot
		for(int i = 0; i < avatarLogins.getSize(); ++i)
		{
			trackingList->addElement(avatarLogins[i]);
		}
	}
}

/// <summary>
/// remove all avatar logins
/// </summary>
void ProfilesScreen::clearLoginIcons()
{
	printf("Clearing avatarlogins\n");
	avatarLogins.removeAllElements();

	printf("Clearing avatar items off scenegraph\n");
	while(usersRoot->numChildren()>0)
	{
		GameObject* child = usersRoot->childAtIndex(0);
		child->cancelActions();
		printf("Removing child named %s \n", child->name.str());
		child->removeFromParent();
	}

	printf("AvatarLogins count: %d\n", avatarLogins.getSize());
}

void ProfilesScreen::resetLoginIcons()
{
	printf("Reseting avatarlogins\n");
	clearLoginIcons();
	createLoginIcons();
	positionAvatarLogins();
}

/// <summary>
/// Creates an avatar login per user
/// </summary>
void ProfilesScreen::createLoginIcons()
{
	printf("ProfilesScreen::creatingLoginIcons\n");
	//****************************************************************************************************
	// Add in user icons
	//****************************************************************************************************
	ObjectArray* users = UsersManager::getInstance()->getUsers();
	for ( int i = 0; i < users->getSize(); i++ )
	{
		User* user = (User*) users->elementAt(i);
		TRelease<AvatarItem> userItem((AvatarItem*)Scene::instantiate(user->getAvatar()));
		userItem->name = user->getDisplayName() + String(i);
		usersRoot->addChild(userItem);
		userItem->setUser(user);
		userItem->setupAsLoginAvatar(user->getFirstName());
		avatarLogins.addElement(userItem);
		printf("adding child: %s\n", userItem->name.str());
	}

	if ( avatarLogins.getSize() < Constants::System::maxNumUsers )
	{
		TRelease<AvatarItem> newUser((AvatarItem*)Scene::instantiate("AvatarItemNewUser"));
		newUser->name = "NewUserAvatarItem";
		usersRoot->addChild(newUser);
		newUser->setupAsLoginAvatar("New User");
		avatarLogins.addElement(newUser);
		printf("adding child: %s\n", newUser->name.str());
	}

	usersRoot->setScale(Vec3f(2,2,2));
}

/// <summary>
/// Positions the logins on screen
/// </summary>
void ProfilesScreen::positionAvatarLogins()
{
	printf("ProfilesScreen::positionAvatarLogins\n");

	int nUsers = avatarLogins.getSize();
	if(nUsers == 1)
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(0, 0, 0));
	}
	else if ( nUsers == 2 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-80, 0, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(80, 0, 0));
	}
	else if ( nUsers == 3 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-120, 0, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(0, 0, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(120, 0, 0));
	}
	else if ( nUsers == 4 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-80, 50, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(80, 50, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(-80, -50, 0));
		((AvatarItem*) avatarLogins[3])->setPosition(Vec3f(80, -50, 0));
	}
	else if ( nUsers == 5 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-120, 50, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(0, 50, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(120, 50, 0));
		((AvatarItem*) avatarLogins[3])->setPosition(Vec3f(-60, -50, 0));
		((AvatarItem*) avatarLogins[4])->setPosition(Vec3f(60, -50, 0));
	}
	else if ( nUsers == 6 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-120, 50, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(0, 50, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(120, 50, 0));
		((AvatarItem*) avatarLogins[3])->setPosition(Vec3f(-120, -50, 0));
		((AvatarItem*) avatarLogins[4])->setPosition(Vec3f(0, -50, 0));
		((AvatarItem*) avatarLogins[5])->setPosition(Vec3f(120, -50, 0));
	}
}

bool ProfilesScreen::itemsHaveActions()
{
	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		if( ((AvatarItem*)avatarLogins[i])->hasActions() )
			return true;
	}

	return false;

}

/// <summary>
/// Base scene feature, tick
/// </summary>
void ProfilesScreen::update(float dt)
{
	super::update(dt);
}

/// <summary>
/// Bubble out items animation
/// </summary>
void ProfilesScreen::bubbleOutItems(float delay, bool all)
{
	printf("Bubbling Out Items\n");
	float time = delay;
	float timeInc = 0.15f;

	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		GameObject* scaleOut = (GameObject*) avatarLogins[i];
		if(!all && selectedAvatar != null && scaleOut == selectedAvatar)
			continue;

		scaleOut->waitFor(time);
		scaleOut->scaleTo(Vec3f::Zero, 0.25f);
		time += timeInc;
	}
}

/// <summary>
/// Bubble in items animation
/// </summary>
void ProfilesScreen::bubbleInItems( float time )
{
	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		GameObject* scaleOut = (GameObject*) avatarLogins[i];
		if(selectedAvatar != null && scaleOut == selectedAvatar)
			continue;

		printf("bubbling in avatar: %s\n", scaleOut->name.str());
		scaleOut->waitFor(time);
		scaleOut->makeVisible(true);
		scaleOut->setScale(Vec3f::Zero);
		scaleOut->scaleTo(scaleOut->originalScale, 0.25f);
		time += 0.1f;
	}
}


} /* namespace Glasgow */

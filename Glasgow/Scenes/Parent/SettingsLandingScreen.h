#ifndef SETTINGSLANDINGSCREEN_H_
#define SETTINGSLANDINGSCREEN_H_

#include "ParentScreen.h"

namespace Glasgow
{

class SettingsLandingScreen : public ParentScreen
{
	CLASSEXTENDS(SettingsLandingScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	SettingsLandingScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~SettingsLandingScreen();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Over ridden for a screen reset
	/// </summary>
	virtual void loadGameObjectState(GameObjectState state);

private:
	void setupInitialState();

	ButtonObject* controllerButton;
	ButtonObject* localeButton;
	ButtonObject* parentLockButton;
	ButtonObject* networkButton;
	ButtonObject* systemInfoButton;
	ButtonObject* parentAccountButton;
};

}

#endif

#include "LandingScreen.h"
#include "ParentScene.h"
namespace Glasgow
{

LandingScreen::LandingScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	editProfilesButton 		= findButton("editProfileButton");
	settingsButton 			= findButton("settingsButton");
	manageContentButton		= findButton("manageContentButton");
	downloadManagerButton 	= findButton("downloadManagerButton");
	downloadIcon			= findSprite("downloadActive");
	errorIcon				= findSprite("errorActive");
	errorIcon->setVisible(false);
	downloadIcon->setVisible(false);
	setupInitialState();
}

LandingScreen::~LandingScreen()
{
	printf("LandingScreen::~LandingScreen()\n");
}

void LandingScreen::setupInitialState()
{}

void LandingScreen::loadGameObjectState(GameObjectState state)
{
	printf("LandingScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}


///<summary>
/// Callback
///</summary>
void LandingScreen::onCallBack( String id )
{
	if( id == "recheck" )
	{
		if( ContentManager::getInstance()->hasDownloads() )
		{
			downloadIcon->originalScale = Vec3f::One*0.6f;
			downloadIcon->setScale(Vec3f::Zero);
			downloadIcon->setOpacity(0);
			downloadIcon->setPosition( downloadManagerButton->getPosition() + Vec3f::Up*25 + Vec3f::Back*100 );
			downloadIcon->setVisible(true);

			//Fade In
			TRelease<MultiAction> fadeScaleIn( new MultiAction(downloadIcon) );
			fadeScaleIn->addAction(TRelease<ScaleToAction>( new ScaleToAction(downloadIcon, downloadIcon->originalScale, 0.5f, EaseFunctions::linear)));
			fadeScaleIn->addAction(TRelease<FadeToAction>( new FadeToAction(downloadIcon, 1.0f, 0.5f, EaseFunctions::linear)));

			downloadIcon->queueAction(fadeScaleIn);

			//Move Down
			downloadIcon->queueAction(TRelease<MoveByAction>(new MoveByAction(downloadIcon, Vec3f::Down*55, 0.75f, EaseFunctions::cubicEaseInOut)));

			//Fade out
			TRelease<MultiAction> fadeScaleOut( new MultiAction(downloadIcon) );
			fadeScaleOut->addAction(TRelease<ScaleToAction>( new ScaleToAction(downloadIcon, Vec3f::Zero, 0.1f, EaseFunctions::linear)));
			fadeScaleOut->addAction(TRelease<FadeToAction>( new FadeToAction(downloadIcon, 0, 0.1f, EaseFunctions::linear)));
			downloadIcon->queueAction(fadeScaleOut);
			downloadIcon->waitFor(0.5f);
			downloadIcon->callback(this, "recheck");
		}
	}
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool LandingScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(objectHighlight != null)
	{
		if(objectHighlight == editProfilesButton)
		{
			printf("Pressed editProfilesButton\n");
			parentScene->pushScreen(ParentScene::PS_PROFILES_SCREEN);
			return true;
		}
		else if(objectHighlight == settingsButton)
		{
			printf("Pressed settingsButton\n");
			parentScene->pushScreen(ParentScene::PS_SETTINGS_LANDING_SCREEN);
			printf("Settings Button Pressed!\n");
			return true;
		}
		else if(objectHighlight == manageContentButton)
		{
			printf("Pressed manageContentButton\n");
			parentScene->pushScreen(ParentScene::PS_CONTENT_MANAGER_SCREEN);
			return true;
		}
		else if(objectHighlight == downloadManagerButton)
		{
			printf("Pressed downloadManagerButton\n");
			parentScene->pushScreen(ParentScene::PS_DOWNLOAD_MANAGER_SCREEN);
			return true;
		}
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool LandingScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void LandingScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f,false);
	onCallBack("recheck");
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* LandingScreen::getArrivalSelection()
{ return settingsButton; }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void LandingScreen::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(downloadManagerButton);
	trackingList->addElement(manageContentButton);
	trackingList->addElement(editProfilesButton);
	trackingList->addElement(settingsButton);
}


} /* namespace Glasgow */

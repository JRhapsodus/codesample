#include "SettingsLandingScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

SettingsLandingScreen::SettingsLandingScreen(GameObject* objectRoot, ParentScene* scene): super(objectRoot, scene)
{
	controllerButton = findButton("controllerButton");
	localeButton = findButton("localeButton");
	parentLockButton = findButton("parentLockButton");
	networkButton = findButton("networkButton");
	systemInfoButton = findButton("systemInfoButton");
	parentAccountButton = findButton("parentAccountButton");

	setupInitialState();
}

SettingsLandingScreen::~SettingsLandingScreen()
{
	printf("SettingsLandingScreen::~SettingsLandingScreen\n");
}

void SettingsLandingScreen::setupInitialState()
{}

void SettingsLandingScreen::loadGameObjectState(GameObjectState state)
{
	printf("SettingsLandingScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool SettingsLandingScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(objectHighlight != null)
	{
		if(objectHighlight == controllerButton)
		{
			printf("Pressed controllerButton\n");
			parentScene->pushScreen(ParentScene::PS_CONTROLLER_SCREEN);
		}
		else if(objectHighlight == localeButton)
		{
			printf("Pressed localeButton\n");
			parentScene->pushScreen(ParentScene::PS_LOCALE_SCREEN);
		}
		else if(objectHighlight == parentLockButton)
		{
			printf("Pressed parentLockButton\n");
			parentScene->pushScreen(ParentScene::PS_GATE_LOCK_SCREEN);
		}
		else if(objectHighlight == networkButton)
		{
			printf("Pressed networkButton\n");
			parentScene->pushScreen(ParentScene::PS_NETWORK_SCREEN);
		}
		else if(objectHighlight == systemInfoButton)
		{
			printf("Pressed systemInfoButton\n");
			parentScene->pushScreen(ParentScene::PS_SYSTEM_SETTINGS_SCREEN);
		}
		else if(objectHighlight == parentAccountButton)
		{
			printf("Pressed parentAccountButton\n");
			parentScene->pushScreen(ParentScene::PS_PARENT_ACCOUNT_SCREEN);
		}
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool SettingsLandingScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void SettingsLandingScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f,false);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* SettingsLandingScreen::getArrivalSelection()
{ return networkButton; }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void SettingsLandingScreen::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(networkButton);
	trackingList->addElement(localeButton);
	trackingList->addElement(parentLockButton);
	trackingList->addElement(controllerButton);
	trackingList->addElement(systemInfoButton);
	trackingList->addElement(parentAccountButton);
}


} /* namespace Glasgow */

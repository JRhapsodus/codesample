#ifndef CONTENTMANAGERSCREEN_H_
#define CONTENTMANAGERSCREEN_H_
#include "ParentScreen.h"


namespace Glasgow
{

class ContentManagerScreen: public ParentScreen
{
	CLASSEXTENDS(ContentManagerScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	ContentManagerScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~ContentManagerScreen();

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual void handleDialogClosed( DialogType dialogType, DialogButtonOption option );

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle B button in a custom way
	/// </summary>
	virtual bool handlePressBack(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	///
	/// </summary>
	virtual void handlePackageChanged(tPackageStatus newStatus, PackageInfo* info );

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	///<summary>
	/// Called when a package is deleted
	///</summary>
	virtual void handlePackageDeleted( PackageInfo* info );

private:
	InstalledItem* selectedItem;
	ButtonObject* viewPurchasedButton;
	ProgressBarObject* progressBar;
	ObjectArray* installedPackages;
	ObjectArray installedItems;

};

}
#endif

#ifndef CHANGEPINSCREEN_H_
#define CHANGEPINSCREEN_H_

#include "ParentScreen.h"
#include "SIEngine/Include/GlasgowControls.h"

namespace Glasgow
{

class ChangePinScreen: public ParentScreen
{
	CLASSEXTENDS(ChangePinScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	ChangePinScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~ChangePinScreen();

	/// <summary>
	/// saves user settings
	/// </summary>
	virtual void saveSettings();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Custom update
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Pin entered
	/// </summary>
	void pinEntered();

	/// <summary>
	/// Pin entered incorrectly, start over
	/// </summary>
	void pinError();

	/// <summary>
	/// resets the enter root settings to display for entering new pin
	/// </summary>
	void resetEnterRoot(float delay);

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

	typedef enum EnterPinStates
	{
		ENTER_NEW_PIN_STATE,
		CONFIRM_NEW_PIN_STATE,
		FINAL_PIN_STATE
	}EnterPinState;

private:
	void ApplyPinValue(TextLabel*& label, String& pin, String value);

	//*************************************
	// Scene Graph Objects
	//*************************************
	GateLockObject* 	numPad;
	GameObject*			enterRoot;
	GameObject*			finalRoot;
	GameObject*			lockRoot;
	GameObject*			confirmPinField;
	GameObject*			pinField;

	TextLabel* 			confirmPrompt;
	TextLabel*			newPinPrompt;

	TextLabel*			errorConfirmPinPrompt;

	TextLabel*			pinLabel;
	TextLabel*			confirmPinLabel;
	TextLabel*			finalPinLabel;

	SpriteObject*		lockIcon;
	SpriteObject*		unlockIcon;
	//*************************************

	EnterPinState		pinState;

	String 				enteredPin;
	String 				confirmPin;
	String				pinSequence;

	Vec3f				origPinPosition;
	Vec3f				origFinalRootPosition;
	Vec3f				origEnterRootPosition;
};

}

#endif

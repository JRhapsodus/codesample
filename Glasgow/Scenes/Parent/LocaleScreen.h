#ifndef LOCALESCREEN_H_
#define LOCALESCREEN_H_

#include "ParentScreen.h"
namespace Glasgow
{

class LocaleScreen : public ParentScreen
{
	CLASSEXTENDS(LocaleScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	LocaleScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~LocaleScreen();

	/// <summary>
	/// saves user settings
	/// </summary>
	virtual void saveSettings();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle the press back ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressBack(GameObject*& objectHighlight);


	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();


protected:

	/// <summary>
	/// Deselects all checkboxes on screen
	/// </summary>
	void deselectAllCheckBoxes();

	/// <summary>
	/// Returns the selected locale
	/// </summary>
	String getLocaleName();

	/// <summary>
	/// Returns the button for the locale
	/// </summary>
	CheckButton* getLocaleButton(SystemLocale locale);

	CheckButton* 	selectedLocale;
	SystemLocale 	chosenLocale;
};

} /* namespace Glasgow */
#endif /* LOCALESCREEN_H_ */

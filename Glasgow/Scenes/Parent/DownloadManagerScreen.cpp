#include "DownloadManagerScreen.h"
#include "ParentScene.h"
namespace Glasgow
{

DownloadManagerScreen::DownloadManagerScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	headerLabel = findLabel("headerLabel");
	noDownloadsLabel = findLabel("noDownloadsLabel");
	noDownloadsLabel->setVisible(false);
	pauseAllButton = findButton("pauseAllButton");
	resumeAllButton = findButton("resumeButton");

}

DownloadManagerScreen::~DownloadManagerScreen()
{

}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void DownloadManagerScreen::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(resumeAllButton);
	trackingList->addElement(pauseAllButton);
	for ( int i = 0; i < downloadItems.getSize(); i++ )
	{
		trackingList->addElement(downloadItems.elementAt(i));
	}
}

/// <summary>
///
/// </summary>
void DownloadManagerScreen::handlePackageDownloadProgressed( PackageInfo* info, int percent)
{
	printf("Recieved downloading package info! %d\% complete!\n", percent);
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool DownloadManagerScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(objectHighlight != NULL && objectHighlight->typeOf(ButtonObject::type()))
	{
		ButtonObject* button = (ButtonObject*)objectHighlight;
		if(button == pauseAllButton)
		{
			ContentManager::getInstance()->pauseDownloads();
			setButtonDisplay(true);
			objectHighlight = resumeAllButton;
			return true;
		}
		else if(button == resumeAllButton)
		{
			objectHighlight = pauseAllButton;
			ContentManager::getInstance()->resumeDownloads();
			setButtonDisplay(false);
			return true;
		}
	}

	return false;
}

/// <summary>
/// Handle package changed
/// </summary>
void DownloadManagerScreen::handlePackageChanged(tPackageStatus newStatus, PackageInfo* info )
{
	printf("DownloadManagerScreen:: we got a new status!");
	switch(newStatus)
	{
		case kPackageStatus_Downloaded:
			printf("kPackageStatus_Downloaded");
					break;
		case kPackageStatus_Downloading:
			printf("kPackageStatus_Downloading ");
					break;
		case kPackageStatus_Installed:
			printf("kPackageStatus_Installed ");
					break;
		case kPackageStatus_Installing:
			printf("kPackageStatus_Installing ");
					break;
		case kPackageStatus_Pending:
			printf("kPackageStatus_Pending ");
					break;
		case kPackageStatus_Removed:
			printf("kPackageStatus_Removed ");
					break;
		case kPackageStatus_Removing:
			printf("kPackageStatus_Removing ");
					break;
		case kPackageStatus_Unknown:
			printf("kPackageStatus_Unknown ");
					break;

		default:
			printf("recieved unlisted kPackageStatus");
	}
	printf("\n");
}

/// <summary>
/// Sets the pause/resume button visibility
/// </summary>
void DownloadManagerScreen::setButtonDisplay(bool paused)
{
	if( downloadItems.getSize() == 0)
	{
		pauseAllButton->setVisible(false);
		resumeAllButton->setVisible(false);
	}
	else
	{
		pauseAllButton->setVisible(!paused);
		resumeAllButton->setVisible(paused);
	}
}

/// <summary>
/// clears and fills the download tracking array
/// </summary>
void DownloadManagerScreen::populateDownloadArray()
{
	downloadItems.removeAllElements();

	ObjectArray* dl = ContentManager::getInstance()->getDownloadingPackages();

	//downloadItems.aContentManager::getInstance()->getDownloadingPackages();
	//TODO: Make sure this actually grabs the real number of downloading items
	int numberOfDownloadingItems = downloadItems.getSize();

	for ( int i = 0; i < numberOfDownloadingItems; i++ )
	{
		TRelease<DownloadItem> downloadItm((DownloadItem*) Scene::instantiate(DownloadItem::type()));
		downloadItm->name = String("DwnItm") + String(i);
		downloadItems.addElement(downloadItm);
		downloadItm->setPackageInfo((PackageInfo*)dl->elementAt(i));
	}
}

/// <summary>
/// add ui elements for the download tracking array items
/// </summary>
void DownloadManagerScreen::populateDownloadUI()
{
	if ( downloadItems.getSize() < 1 )
	{
		noDownloadsLabel->setVisible(true);
	}
	else
	{
		float downloadItemVertSpacing = 100.f;
		for ( int i = 0; i < downloadItems.getSize(); i++ )
		{
			((DownloadItem*) downloadItems.elementAt(i))->setPosition(
					noDownloadsLabel->getPosition() - Vec3f(0, downloadItemVertSpacing * i, 0));
			addChild((DownloadItem*) downloadItems.elementAt(i));
		}
	}
}
/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void DownloadManagerScreen::cameraArrived()
{
	setButtonDisplay(ContentManager::getInstance()->isDownloadsPaused());
	parentScene->showBackButton(0.75f,true);
    populateDownloadArray();
    populateDownloadUI();
}

}

#ifndef REGISTERSCREEN_H_
#define REGISTERSCREEN_H_

#include "ParentScreen.h"
#include "SIEngine/Include/SIControls.h"

namespace Glasgow
{

class RegisterScreen :  public QObject, public ParentScreen
{
	Q_OBJECT
	CLASSEXTENDS(RegisterScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	RegisterScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~RegisterScreen();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Overridden so we can poll the activationRequest
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual void handleDialogClosed( DialogType dialogType, DialogButtonOption option );

	virtual void handleDialogCanceled( DialogType dialogType );

	/// <summary>
	/// Animation transition into a networking error
	/// </summary>
	void showNetworkError( String header, String error );

	/// <summary>
	/// Remove network error
	/// </summary>
	void hideNetworkError();

	/// <summary>
	/// Updates activation request
	/// </summary>
	void updateActivationRequest(float dt );

	/// <summary>
	/// Updates activation request
	/// </summary>
	void updateSetUp(float dt );

	/// <summary>
	/// Start activation request
	/// </summary>
	void startActivationRequest();

	/// <summary>
	/// Network error occured
	/// </summary>
	virtual void handleNetworkError(String networkName, String networkError);

	/// <summary>
	/// Network connection made
	/// </summary>
	virtual void handleNetworkConnected( String networkName );

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

public slots:
	void onDeviceUpdated();

private:
	/// <summary>
	/// Helper call to instantiate a netError object to handle reconnect / skip flow
	/// </summary>
	void createNetError();

	/// <summary>
	/// Activation code succeeded
	/// </summary>
	void doActivationRequestFinished();

	/// <summary>
	/// Activation code timed out
	/// </summary>
	void doActivationTimeOut();

	/// <summary>
	/// Activation code failed
	/// </summary>
	void doActivationFail();

	//***************************
	// scenegraph objects
	//***************************
	NetErrorObject* netError;
	float attempts;
	float setupTimeOut;
	float activationCodeTimeOut;
	int activationRequestTrys;
	QDBusPendingReply<QString> activationRequest;
	GameObject* activatedRoot;
	GameObject* activationRoot;
	TextLabel*  activateCodeLabel;
	bool netErrorActive;
	bool processed;


	//***************************
};

} /* namespace Glasgow */

#endif /* REGISTERSCREEN_H_ */

/*
 * ControllerScreen.h
 *
 *  Created on: Aug 21, 2014
 *      Author: lfu
 */

#ifndef CONTROLLERSCREEN_H_
#define CONTROLLERSCREEN_H_

#include "ParentScreen.h"

namespace Glasgow
{

class ControllerScreen : public ParentScreen
{
	CLASSEXTENDS(ControllerScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	ControllerScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~ControllerScreen();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Controller Sync Event
	/// </summary>
	virtual void handleControllerSync(bool success);

	/// <summary>
	/// Controller Connected Event
	/// </summary>
	virtual void handleControllerConnected();

	/// <summary>
	/// Controller Disconnected Event
	/// </summary>
	virtual void handleControllerDisconnected();

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

	///<summary>
	/// Successful pairing of controller
	/// </summary>
	void pairingComplete();

	void showSynchError();
	void hideSynchError();

	void showSynchInstructions();
	void hideSynchInstructions();

	void showCongratulations();

private:
	bool hasPairedController;
	bool isShowingError;

	SpriteObject* 	connectStep1Icon;
	SpriteObject* 	connectStep2Icon;
	TextLabel*		connectHeaderLabel;
	TextLabel*		connectSubHeaderLabel;
	GameObject *	congratulationsRoot;

	TextLabel*		errorPrompt;
	SpriteObject*	errorIcon;

};

} /* namespace Glasgow */
#endif /* CONTROLLERSCREEN_H_ */

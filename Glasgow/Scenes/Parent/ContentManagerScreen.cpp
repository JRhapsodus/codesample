#include "ContentManagerScreen.h"
#include "ParentScene.h"
namespace Glasgow
{

ContentManagerScreen::ContentManagerScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	viewPurchasedButton = findButton("viewPurchased");
	progressBar = (ProgressBarObject*)Scene::instantiate(ProgressBarObject::type());
	addChild(progressBar);
	progressBar->setVisible(true);
	progressBar->setPosition(Vec3f(0, 150, 0));
	progressBar->setCullingLayerRecursive(scene->getCamera(0)->getCullingMask());
	progressBar->setScale(Vec3f::One);
	progressBar->setAnimatedBar(false);
	installedPackages = null;
	selectedItem = null;

	printf("ContentManagerScreen::ContentManagerScreen\n");
}

ContentManagerScreen::~ContentManagerScreen()
{ }


/// <summary>
/// onDialogHandled
/// </summary>
void ContentManagerScreen::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	parentScene->removeSelection();
	if( dialogType == DialogTypeRemoveContent && option.id == "remove" )
	{
		if( selectedItem != null && selectedItem->getPackageInfo() != null )
		{
			printf("Removing: %s\n", selectedItem->name.str());
			selectedItem->getPackageInfo()->debugLog();
			Packages::RemovePackage(selectedItem->getPackageInfo()->packageID.str());
			selectedItem = null;
		}

		return;
	}

	return super::handleDialogClosed( dialogType, option );
}

bool ContentManagerScreen::handlePressBack(GameObject*& objectHighlight)
{
	if( objectHighlight != null && objectHighlight->typeOf(InstalledItem::type()))
	{
		printf("Pressed installed item\n");

		InstalledItem* installedItem = (InstalledItem*)objectHighlight;
		selectedItem = installedItem;

		if( installedItem->isDeletable() )
		{
			DialogOptions options;
			options.title 	= "Remove Content?";
			options.prompt  = "FPO: Are you sure you want to remove " + installedItem->getPackageInfo()->appName + " ?";
			options.buttonA = DialogButtonOption("Not Yet", "notYet");
			options.buttonB = DialogButtonOption("Remove", "remove");
			options.ignoreOldTrackedObjects = true;
			parentScene->showModalDialog(DialogTypeRemoveContent, options);
			return true;
		}
	}
	return false;
}


/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ContentManagerScreen::handlePressAction(GameObject*& objectHighlight)
{
	if( objectHighlight == viewPurchasedButton )
	{
		printf("Pressed viewPurchased\n");

		//NOCHECKIN: don't always allow access!
		if( true  /*SystemSettings::getIsActivated()*/ )
		{
			parentScene->pushScreen(ParentScene::PS_PURCHASED_MANAGER_SCREEN);
		}
		else
		{
			DialogOptions options;
			options.title 	= "Requires Registration";
			options.prompt  = "FPO: Viewing purchased items requires registration.";
			options.buttonA = DialogButtonOption("Register Now", "notYet");
			options.buttonB = DialogButtonOption("Ok", "notYet");
			options.ignoreOldTrackedObjects = true;
			parentScene->showModalDialog(DialogTypeRemoveContent, options);
		}

		return true;
	}

	return super::handlePressAction(objectHighlight);
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ContentManagerScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return super::handleAnlogChange(objectLeft, objectEntered, currentHighlight); }

/// <summary>
/// Handle package changed
/// </summary>
void ContentManagerScreen::handlePackageChanged(tPackageStatus newStatus, PackageInfo* info )
{



}

///<summary>
/// Called when a package is deleted
///</summary>
void ContentManagerScreen::handlePackageDeleted( PackageInfo* info )
{
	//************************************************************************************************
	// Display a custom dialog in the case we are offline warning about account syncing
	//************************************************************************************************
	if( !ConnectionManager::getInstance()->isOnline() )
	{
		DialogOptions options;
		options.title 	= "Deleted";
		options.prompt  = "Your app has been deleted. Please connect to the Internet to sync this change with your account.";
		options.buttonA = DialogButtonOption("Close", "close");
		parentScene->showModalDialog(DialogTypeDeleteConfirmed, options);
	}

	//************************************************************************************************
	// TODO: kevin
	// Do the actual removing, do note that at this time a gap will remain on screen and the remaining
	// items should animate into their new positions
	//************************************************************************************************
	for( int i = 0; i<installedItems.getSize(); i++ )
	{
		InstalledItem* item = (InstalledItem*)installedItems.elementAt(i);
		if( item->getPackageInfo() == info )
		{
			item->bubbleOut(0);
			item->kill();
			installedItems.removeElementAt(i);
			i--;
		}
	}
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ContentManagerScreen::cameraArrived()
{
	if( installedItems.getSize() > 0 )
	{
		//clean up old items
		for( int i = 0; i < installedItems.getSize(); i++ )
		{
			InstalledItem* item = (InstalledItem*)installedItems.elementAt(i);
			item->removeFromParent();
		}
		installedItems.removeAllElements();
	}

	installedPackages = ContentManager::getInstance()->getInstalledPackages();
	for( int i = 0; i<installedPackages->getSize(); i++ )
	{
		PackageInfo* info = (PackageInfo*)installedPackages->elementAt(i);
		InstalledItem* item = (InstalledItem*)Scene::instantiate(InstalledItem::type());
		addChild(item);
		item->name = String("InstalledItem") + String(i);
		item->setPosition( Vec3f::Left*450 + Vec3f::Down*100 + Vec3f::Right*275*i);
		item->setCullingLayerRecursive(parentScene->getCamera(0)->getCullingMask());
		item->setPackageInfo(info);
		installedItems.addElement(item);
	}

	parentScene->showBackButton(0.75f,false);
	double totalSpace = SystemSettings::getTotalSpace(Constants::Units::UnitGigaByte);
	double usedSpace = SystemSettings::getUsedSpace(Constants::Units::UnitGigaByte);
	double availableSpace = SystemSettings::getAvailableSpace(Constants::Units::UnitGigaByte);

	double percentage = usedSpace/availableSpace;
	percentage = (percentage > 1.0) ? 1.0 : percentage;
	percentage = (percentage < 0) ? 0 : percentage;

	char strUsed[50];
	char strTotal[50];
	sprintf( strUsed, "%.2f", usedSpace);
	sprintf( strTotal, "%.2f", totalSpace);

	progressBar->setProgress( percentage );
	progressBar->setLabel( String(strUsed) + String(" GB"), String(" used of ") + String( strTotal ) + String(" GB") );
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ContentManagerScreen::getArrivalSelection()
{
	if( installedItems.getSize() > 0 )
	{ return (GameObject*)installedItems.elementAt(0); }

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ContentManagerScreen::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(viewPurchasedButton);

	for( int i = 0; i<installedItems.getSize(); i++ )
	{ trackingList->addElement( (GameObject*)installedItems.elementAt(i) ); }
}


}

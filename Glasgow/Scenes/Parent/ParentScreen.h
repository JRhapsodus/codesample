#ifndef PARENTSCREEN_H_
#define PARENTSCREEN_H_

#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Include/SIActions.h"

namespace Glasgow
{

class ParentScene;

class ParentScreen : public GameObject
{
	CLASSEXTENDS(ParentScreen, GameObject);
	ADD_TO_CLASS_MAP;

public:
	ParentScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~ParentScreen();

	/// <summary>
	/// Controller Sync Event
	/// </summary>
	virtual void handleControllerSync(bool success);

	/// <summary>
	/// Controller Connected Event
	/// </summary>
	virtual void handleControllerConnected();

	/// <summary>
	/// Controller Disconnected Event
	/// </summary>
	virtual void handleControllerDisconnected();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle the press back ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressBack(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual void handleDialogClosed( DialogType dialogType, DialogButtonOption option );

	/// <summary>
	/// If we want to handle cancels
	/// </summary>
	virtual void handleDialogCanceled( DialogType dialogType);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Inpuit forwarding call to handle selection changes
	/// </summary>
	virtual void handleSelectionChange( GameObject* old, GameObject* newObj);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// When a package has changed state
	/// </summary>
	virtual void handlePackageChanged( tPackageStatus newStatus, PackageInfo* info);

	/// <summary>
	/// Network error occured
	/// </summary>
	virtual void handleNetworkError(String networkName, String networkError);

	/// <summary>
	///
	/// </summary>
	virtual void handlePackageDownloadProgressed( PackageInfo* info, int percent);

	/// <summary>
	/// Network connection made
	/// </summary>
	virtual void handleNetworkConnected( String networkName );

	/// <summary>
	/// Download finished
	/// </summary>
	virtual void handleDonwloadFinished( String packageID );

	/// <summary>
	///
	/// </summary>
	virtual void handlePendingChanged();

	/// <summary>
	/// Download started
	/// </summary>
	virtual void handleDonwloadStarted( String packageID );

	///<summary>
	/// Called when a package is deleted
	///</summary>
	virtual void handlePackageDeleted( PackageInfo* info );

	/// <summary>
	///
	/// </summary>
	virtual void handleDownloadError();

	/// <summary>
	/// Download progress
	/// </summary>
	virtual void handleDownloadProgress( String package, int downloadProgress);

protected:
	ParentScene* parentScene;
};

}

#endif

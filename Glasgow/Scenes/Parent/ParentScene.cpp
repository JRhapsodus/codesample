#include "Glasgow/GlasgowApp.h"
#include "ParentScene.h"
#include "SIEngine/Include/SIScenes.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIActions.h"
#include "SIEngine/Include/SIRendering.h"
#include "Glasgow/Renderables/WandObject.h"
#include "Glasgow/Controls/RollOverButton.h"
#include "Glasgow/Controls/WifiButton.h"
#include "Glasgow/Renderables/VideoObject.h"

namespace Glasgow
{


ParentScene::~ParentScene()
{
	printf("ParentScene::~ParentScene()\n");
	SAFE_RELEASE(pinScreen);
	SAFE_RELEASE(contentScreen);
	SAFE_RELEASE(downloadScreen);
	SAFE_RELEASE(editProfileScreen);
	SAFE_RELEASE(profilesScreen);
	SAFE_RELEASE(gateLockScreen);
	SAFE_RELEASE(landingScreen);
	SAFE_RELEASE(localeScreen);
	SAFE_RELEASE(detailsScreen);
	SAFE_RELEASE(purchasedScreen);
	SAFE_RELEASE(settingsLandingScreen);
	SAFE_RELEASE(systemSettingsScreen);
	SAFE_RELEASE(wifiScreen);
	SAFE_RELEASE(ethernetScreen);
	SAFE_RELEASE(controllerScreen);
	SAFE_RELEASE(parentAccountScreen);
	SAFE_RELEASE(registerScreen);
	screens.clear();
}

void PreloadTextureThread::run()
{
	printf("PreloadTextureThread::run()\n");
	scene->loaded = true;
	printf("PreloadTextureThread ending\n");
}

ParentScene::ParentScene() : super("Layouts/ParentScene.dat", "ParentSceneStrings")
{
	printf("ParentScene::ParentScene\n");

	TextureManager::getInstance()->loadPng("parentScreenAtlas");
	TextureManager::getInstance()->loadPng("prefabAtlas");
	TextureManager::getInstance()->loadPng("avatarAtlas");
	TextureManager::getInstance()->loadPng("avatarAtlas2");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-22");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-24");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-40");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-60");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/MontserratBold-LF-24");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/MontserratBold-LF-40");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/MontserratBold-LF-60");

	analogMaxDistance = 2.0f;
	getCamera(0)->setupForOrthographic(true);

	backButtonTimer = -1;
	willHighlightBackButton = false;
	helpButtonTimer	= -1;
	willHighlightHelpButton = false;

	numEthernetChecks = 0;
	fillReferences();
	setupInitialState();

	playMusic("Base_OOBEmusic");

	currentHighlight = NULL;
	currentScreen  	 = NULL;

	newUserFromChildScene = false;
	enterFromFWUpdate = false;
	fwDownloadErrorOccured = false;

	if( requestedState.loadToState == AppStateNewUser && requestedState.from == AppIDGlasgowUI )
	{ newUserFromChildScene = true; }

	//load root scene
	//need to check for update flag
	//post firmware update
	if( AssetLibrary::fileExists(Constants::Paths::flagFirmwareInstalled) )
	{
		printf("%s exists!\n", Constants::Paths::flagFirmwareInstalled.str());
		AssetLibrary::removeFile(Constants::Paths::flagFirmwareInstalled);
		enterFromFWUpdate = true;
		replaceScreen(PS_UPDATE_SCREEN);
	}
	else
	{
		printf("[%s] does not exist!\n", Constants::Paths::flagFirmwareInstalled.str());
		//show parent lock gate
		replaceScreen(PS_GATE_LOCK_SCREEN);
	}
	GlasgowApp::mainState->writeUIReadyFlag();
}

//***************************************************************************
// Fill in references
	//***************************************************************************
void ParentScene::fillReferences()
{
	printf("Filling references. ParentScene\n");

	pinScreen 						= new ChangePinScreen(worldRoot->find("pinScreen"), this);
	contentScreen 					= new ContentManagerScreen(worldRoot->find("contentManagerScreen"), this);
	downloadScreen 					= new DownloadManagerScreen(worldRoot->find("downloadScreen"), this);
	profilesScreen					= new ProfilesScreen(worldRoot->find("profilesScreen"), this);
	editProfileScreen				= new EditProfilesScreen(worldRoot->find("editProfileScreen"), this );
	gateLockScreen					= new GateLockScreen(worldRoot->find("gateLockScreen"), this );
	landingScreen					= new LandingScreen(worldRoot->find("landingScreen"), this );
	localeScreen					= new LocaleScreen( worldRoot->find("localeScreen"), this);
	detailsScreen					= new PackageDetailsScreen( worldRoot->find("detailsScreen"), this);
	purchasedScreen					= new PurchasedManagerScreen(worldRoot->find("purchasedScreen"), this);
	settingsLandingScreen			= new SettingsLandingScreen(worldRoot->find("settingsLandingScreen"), this);
	systemSettingsScreen			= new SystemSettingsScreen(worldRoot->find("systemSettingsScreen"), this);
	wifiScreen						= new WifiScreen(worldRoot->find("wifiScreen"),this);
	ethernetScreen					= new EthernetScreen(worldRoot->find("ethernetScreen"),this);
	controllerScreen				= new ControllerScreen(worldRoot->find("controllerScreen"), this);
	parentAccountScreen				= new ParentAccountScreen(worldRoot->find("parentAccountScreen"), this);
	registerScreen					= new RegisterScreen(worldRoot->find("registerScreen"), this);
	updateScreen					= new UpdateScreen(worldRoot->find("updateScreen"), this);

	pinScreen->saveOriginalStateRecursive();
	contentScreen->saveOriginalStateRecursive();
	downloadScreen->saveOriginalStateRecursive();
	profilesScreen->saveOriginalStateRecursive();
	editProfileScreen->saveOriginalStateRecursive();
	gateLockScreen->saveOriginalStateRecursive();
	landingScreen->saveOriginalStateRecursive();
	localeScreen->saveOriginalStateRecursive();
	detailsScreen->saveOriginalStateRecursive();
	purchasedScreen->saveOriginalStateRecursive();
	settingsLandingScreen->saveOriginalStateRecursive();
	systemSettingsScreen->saveOriginalStateRecursive();
	wifiScreen->saveOriginalStateRecursive();
	ethernetScreen->saveOriginalStateRecursive();
	controllerScreen->saveOriginalStateRecursive();
	parentAccountScreen->saveOriginalStateRecursive();
	registerScreen->saveOriginalStateRecursive();
	updateScreen->saveOriginalStateRecursive();


	screens[(int)PS_PIN_SCREEN] 				= pinScreen;
	screens[(int)PS_CONTENT_MANAGER_SCREEN]		= contentScreen;
	screens[(int)PS_DOWNLOAD_MANAGER_SCREEN]	= downloadScreen;
	screens[(int)PS_PROFILES_SCREEN]			= profilesScreen;
	screens[(int)PS_EDIT_PROFILES_SCREEN]		= editProfileScreen;
	screens[(int)PS_GATE_LOCK_SCREEN]			= gateLockScreen;
	screens[(int)PS_LANDING_SCREEN]				= landingScreen;
	screens[(int)PS_LOCALE_SCREEN]				= localeScreen;
	screens[(int)PS_PACKAGE_DETAILS_SCREEN]		= detailsScreen;
	screens[(int)PS_PURCHASED_MANAGER_SCREEN]	= purchasedScreen;
	screens[(int)PS_SETTINGS_LANDING_SCREEN]	= settingsLandingScreen;
	screens[(int)PS_SYSTEM_SETTINGS_SCREEN]		= systemSettingsScreen;
	screens[(int)PS_CONTROLLER_SCREEN]			= controllerScreen;
	screens[(int)PS_PARENT_ACCOUNT_SCREEN]		= parentAccountScreen;
	screens[(int)PS_REGISTER_SCREEN]			= registerScreen;
	screens[(int)PS_UPDATE_SCREEN]				= updateScreen;

	//defaulting to wifiScreen
	screens[(int)PS_NETWORK_SCREEN] = wifiScreen;

	//global elements
	background 						= worldRoot->findSprite("Background");
	prevButton						= worldRoot->findButton("backButton");
	helpButton						= worldRoot->findButton("helpButton");
	screenDarken					= worldRoot->findSprite("screenDarken");
}

//***************************************************************************
// Lets save our O(n) qsort by limiting the scene graph to the current pages
//***************************************************************************
void ParentScene::setupInitialState()
{
	prevButton->setVisible(false);
	helpButton->setVisible(false);
	worldRoot->findLabel("helpLabel")->setVisible(false);
	screenDarken->waitFor(1.0f);
	screenDarken->fadeTo(0, 0.5f);
	screenDarken->waitFor(1.0f);
	screenDarken->fadeTo(0, 0.5f);
	screenDarken->makeVisible(false);
	printf("ParentScene::setupInitialState\n");
}

/// <summary>
/// Sets up scene for new user entry as determined from childScene requirements
/// </summary>
void ParentScene::setupForNewUser()
{
	newUserFromChildScene = true;
}

/// <summary>
/// Setup hotspots
/// </summary>
void ParentScene::setupHotSpots()
{
	if(currentModal == null)
	{
		trackedObjects->removeAllElements();

		if( currentScreen != null )
		{
			currentScreen->addHotSpots(trackedObjects);
			trackedObjects->addElement(prevButton);
			trackedObjects->addElement(helpButton);
		}
	}
}


/// <summary>
/// push a new screen to the view stack
/// </summary>
void ParentScene::pushScreen( ParentScreenName screenName )
{
	//update network screen
	if(screenName == PS_NETWORK_SCREEN)
	{
		if(ConnectionManager::getInstance()->hasEthernet())
		{ screens[(int)PS_NETWORK_SCREEN] = ethernetScreen;}
		else{screens[(int)PS_NETWORK_SCREEN] = wifiScreen;}
	}

	if(screens.containsKey((int)screenName))
	{
		BaseObject* bo = screens[(int)screenName];
		pushScreen((ParentScreen*)bo);
	}
}

/// <summary>
/// push a new screen to the view stack
/// </summary>
void ParentScene::pushScreen( ParentScreen* newScreen )
{
	if(currentScreen == null || currentScreen == newScreen)
		return;

	printf("ParentScene::pushScreen %s -> %s\n", currentScreen->name.str(), newScreen->name.str());

	lockOutInput(1);

	removeSelection();
	prevButton->bubbleOut();
	hideHelpButton(0);

	currentScreen->moveBy(Vec3f::Left * 1280, 0.5f);
	currentScreen->makeVisible(false);
	currentScreen->kill();

	playSound("Base_RightScroll");

	newScreen->refreshLocalizedStringsRecursive();
	newScreen->loadOriginalStateRecursive();
	newScreen->setPosition( Vec3f(1280, 0, 0) );
	newScreen->setVisible(true);
	newScreen->moveTo( Vec3f::Zero, 1 );

	worldRoot->addChild(newScreen);
	newScreen->cameraArrived();
	currentScreen = newScreen;
	screenQueue.addElement(newScreen);

	currentHighlight = newScreen->getArrivalSelection();
	if( currentHighlight != null )
	{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }

	setupHotSpots();
}

/// <summary>
/// pops the last screen from the view
/// </summary>
ParentScreen* ParentScene::popScreen()
{
	if(currentScreen == null)
		return null;

	printf("ParentScene::popScreen %s\n", currentScreen->name.str());

	lockOutInput(1);

	removeSelection();
	prevButton->bubbleOut();
	hideHelpButton(0);

	currentScreen->moveBy(Vec3f::Right * 1280, 0.5f);
	currentScreen->makeVisible(false);
	currentScreen->kill();

	screenQueue.removeElementAt(screenQueue.getSize()-1);
	playSound("Base_RightScroll");

	ParentScreen* newScreen = null;
	printf("ScreenQueue [%d]\n", screenQueue.getSize());
	if(screenQueue.getSize()>0)
	{
		newScreen = (ParentScreen*)screenQueue.lastElement();
	}

	if(newScreen != null)
	{
		newScreen->setPosition( Vec3f::Left * 1280 );
		newScreen->setVisible(true);
		newScreen->moveTo( Vec3f::Zero, 1 );

		worldRoot->addChild(newScreen);
		newScreen->refreshLocalizedStringsRecursive();
		newScreen->cameraArrived();
		currentScreen = newScreen;

		currentHighlight = newScreen->getArrivalSelection();
		if( currentHighlight != null )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }

		printf("-> %s\n", newScreen->name.str());
	}
	else
	{
		currentScreen == null;
	}

	setupHotSpots();


	return currentScreen;
}

/// <summary>
/// push a new screen to the view stack
/// </summary>
void ParentScene::replaceScreen( ParentScreenName screenName )
{
	//update network screen
	if(screenName == PS_NETWORK_SCREEN)
	{
		if(ConnectionManager::getInstance()->hasEthernet())
		{ screens[(int)PS_NETWORK_SCREEN] = ethernetScreen;}
		else{screens[(int)PS_NETWORK_SCREEN] = wifiScreen;}
	}

	if(screens.containsKey((int)screenName))
	{
		BaseObject* bo = screens[(int)screenName];
		replaceScreen((ParentScreen*)bo);
	}
}

/// <summary>
/// replaces the last screen from the view
/// </summary>
void ParentScene::replaceScreen(ParentScreen* newScreen)
{
	if(newScreen == null)
		ThrowException::exception("ParentScene::replaceScreen - screen does not exist");

	lockOutInput(1);

	//unload screen first
	//all new screens will enter from the right, with exiting screens going left
	printf("ParentScene::replaceScreen ");
	if(currentScreen != null)
	{
		if(currentScreen == newScreen)
			return;

		removeSelection();
		prevButton->bubbleOut();
		hideHelpButton(0);

		printf("removed: %s ", currentScreen->name.str());
		currentScreen->moveBy(Vec3f::Left * 1280, 0.5f);
		currentScreen->makeVisible(false);
		currentScreen->kill();

		screenQueue.removeElementAt(screenQueue.getSize()-1);
	}

	playSound("Base_RightScroll");

	printf("added: %s\n", newScreen->name.str());
	newScreen->refreshLocalizedStringsRecursive();
	newScreen->loadOriginalStateRecursive();
	newScreen->setPosition( Vec3f(1280, 0, 0) );
	newScreen->setVisible(true);
	newScreen->moveTo( Vec3f::Zero, 1 );

	if( newScreen->getParent() != null )
	{ printf("WOAH WOAH WOAH we already have a parent, we can't be added to worldRoot!\n"); }
	else
	{ worldRoot->addChild(newScreen); }

	newScreen->cameraArrived();

	currentScreen = newScreen;
	screenQueue.addElement(currentScreen);

	currentHighlight = newScreen->getArrivalSelection();
	if( currentHighlight != null )
	{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }

	setupHotSpots();
}

/// <summary>
/// force exit to child scene
/// </summary>
void ParentScene::exitToChildScene()
{
	lockOutInput(2);
	screenDarken->setVisible(true);
	screenDarken->setOpacity(0);
	screenDarken->fadeTo(1, 1.5f);
	TimerManager::getInstance()->addTimer("ToChild", 1.5f, 0);
}

/// <summary>
/// parent gate has been passed
/// </summary>
void ParentScene::parentGatePassed()
{
	if(screenQueue.getSize() > 1)
	{
		BaseObject* prevScreen = screenQueue.elementAt(screenQueue.getSize()-2);

		if(((ParentScreen*)prevScreen) == settingsLandingScreen )
		{
			replaceScreen(PS_PIN_SCREEN);
		}
		else
		{
			popScreen();
		}
	}
	else
	{
		//just entered parents
		if(newUserFromChildScene)
		{
			replaceScreen(PS_EDIT_PROFILES_SCREEN);
		}
		else
		{
			replaceScreen(PS_LANDING_SCREEN);
		}
	}
}

///<summary>
/// returns screen being displayed
///</summary>
ParentScreen* ParentScene::getCurrentScreen()
{ return currentScreen;}


/// <summary>
/// Child scene uses timers
/// </summary>
void ParentScene::onTimerExpired( TimerEvent* timer )
{

	if( timer->id == "ToChild" )
	{
		exitTimer = 1;
		return;
	}
	else if( timer->id == "ScanRequest" )
	{
		wifiScreen->doScan();
		setupHotSpots();
	}
	else if(timer->id == "WifiConnectTimeout")
	{
		ConnectionManager::getInstance()->wifiTimeoutExpired();
	}
	else if(timer->id == "HideSynchError")
	{
		controllerScreen->hideSynchError();
		controllerScreen->showSynchInstructions();
	}
	else if(timer->id == "HandleSynchError")
	{
		controllerScreen->handleControllerSync(false);
	}
	else if(timer->id == "HandleConnected")
	{
		controllerScreen->handleControllerConnected();
	}

}

void ParentScene::onPendingDownloadsChanged()
{
	if( currentScreen == updateScreen )
	{
		updateScreen->handlePendingChanged();
	}
}

/// <summary>
/// Download error'd out
/// </summary>
void ParentScene::onDownloadError()
{
	fwDownloadErrorOccured = true;
	if( currentScreen == updateScreen )
	{
		updateScreen->handleDownloadError();
	}

	super::onDownloadError();
}


///<summary>
/// Called when a package is deleted
///</summary>
void ParentScene::onPackageDeleted( PackageInfo* info )
{
	downloadScreen->handlePackageDeleted(info);
	purchasedScreen->handlePackageDeleted(info);
	contentScreen->handlePackageDeleted(info);
}



/// <summary>
///
/// </summary>
void ParentScene::onPackageDownloadProgressed( PackageInfo* info, int percent)
{
	downloadScreen->handlePackageDownloadProgressed(info, percent);
	purchasedScreen->handlePackageDownloadProgressed(info, percent);
	contentScreen->handlePackageDownloadProgressed(info, percent);
}

void ParentScene::onPackageChanged(tPackageStatus newStatus, PackageInfo* info )
{
	downloadScreen->handlePackageChanged(newStatus, info);
	purchasedScreen->handlePackageChanged(newStatus, info);
	contentScreen->handlePackageChanged(newStatus, info);
}

/// <summary>
/// Download has progressed
/// </summary>
void ParentScene::onDonwloadProgressed( String packageID, int downloadProgress )
{
	updateScreen->handleDownloadProgress(packageID, downloadProgress);
}

/// <summary>
/// Download finished
/// </summary>
void ParentScene::onDonwloadFinished( String packageID )
{
	updateScreen->handleDonwloadFinished(packageID);
}

/// <summary>
/// Download started
/// </summary>
void ParentScene::onDonwloadStarted( String packageID )
{
	updateScreen->handleDonwloadStarted(packageID);
}

/// <summary>
/// Custom handling of analog changes
/// </summary>
void ParentScene::onAnalogChange(GameObject* objectLeft, GameObject* objectEntered )
{
	if( currentModal != null )
	{ return super::onAnalogChange(objectLeft, objectEntered); }

	if( selectionLockOut > 0 )
		return;

	if( currentScreen != null && currentScreen->handleAnlogChange(objectLeft, objectEntered, currentHighlight) )
	{
		setupHotSpots();
	}
	else
	{ super::onAnalogChange(objectLeft, objectEntered); }
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void ParentScene::onButtonHint()
{
	printf("ParentScene::onButtonHint\n");
	if( currentModal != null || currentScreen == null || !helpButton->isVisibleHierarchy())
	return super::onButtonHint();


	if(currentScreen != null)
	{
		playSound("Content_Hint");
		lockOutInput(0.8f);

		String dialogName = "Help";
		String title = "Help";
		String prompt = "For issues with your LeapTV, visit leapfrog.com/support/leaptv";
		String option = "Ok";

		//show modal dialog based on step
		if(currentScreen == ethernetScreen || currentScreen == wifiScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_NETWORK_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_NETWORK_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_NETWORK_BUTTON");

		}
		else if(currentScreen == parentAccountScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_PARENT_ACCOUNT_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_PARENT_ACCOUNT_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_PARENT_ACCOUNT_BUTTON");
		}
		else if(currentScreen == gateLockScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_GATELOCK_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_GATELOCK_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_GATELOCK_BUTTON");
		}
		else if(currentScreen == pinScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_PIN_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_PIN_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_PIN_BUTTON");
		}
		else if(currentScreen == controllerScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_CONTROLLER_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_CONTROLLER_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_CONTROLLER_BUTTON");
		}
		else if(currentScreen == systemSettingsScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_SYSTEM_INFO_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_SYSTEM_INFO_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_SYSTEM_INFO_BUTTON");
		}
		else if(currentScreen == contentScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_CONTENT_MANAGER_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_CONTENT_MANAGER_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_CONTENT_MANAGER_BUTTON");
		}
		else if(currentScreen == registerScreen)
		{
			title 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_REGISTRATIONCODE_HEADER");
			prompt 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_REGISTRATIONCODE_TEXT");
			option 	= StringsManager::getInstance()->getLocalizedString("PARENTS_HELP_REGISTRATIONCODE_BUTTON");
		}

		DialogOptions options;
		options.title 	= title;
		options.prompt  = prompt;
		options.buttonA = DialogButtonOption(option, "Ok");
		showModalDialog(DialogTypeHelp, options);
	}

	super::onButtonHint();
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void ParentScene::onButtonHome()
{
	printf("ParentScene::onButtonHint\n");
	if( currentModal != null || currentScreen == null)
	return super::onButtonHome();

	printf(" ParentScene::onButtonHome\n");
	requestExitSettings();
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void ParentScene::onButtonPressAction()
{
	if( currentModal != null )
	{
		super::onButtonPressAction();
		return;
	}

	if( currentHighlight != null && currentHighlight->name == "backButton" && currentHighlight->isVisibleHierarchy() )
	{
		if(screenQueue.getSize() == 1)
		{
			requestExitSettings();
			prevButton->setState(ButtonStateNormal);
		}
		else
		{
			popScreen();
		}
		return;
	}
	else if( currentHighlight != null && currentHighlight->name == "helpButton" && currentHighlight->isVisibleHierarchy())
	{
		onButtonHint();
		return;

	}
	else if( currentScreen != null && currentScreen->handlePressAction(currentHighlight))
	{
		playSound("Parent_Keystroke_Backspace");

		setupHotSpots();
		return;
	}

	super::onButtonPressAction();
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void ParentScene::onButtonPressBack()
{
	if( currentModal != null )
	{
		super::onButtonPressBack();
		return;
	}

	if(currentScreen != null && currentScreen->handlePressBack(currentHighlight))
	{
		playSound("Parent_Keystroke_Backspace");
		setupHotSpots();
		return;
	}

	if(prevButton->isVisibleHierarchy() && prevButton->hasActions() == false)
	{
		if(screenQueue.getSize() == 1)
		{
			requestExitSettings();
		}
		else
		{
			popScreen();
		}
	}

	super::onButtonPressBack();
}

/// <summary>
/// Are you sure? Dialog
/// </summary>
void ParentScene::requestExitSettings()
{
	if(enterFromFWUpdate)
	{
		exitToChildScene();
	}
	else
	{
		// TODO: Localize Strings
		DialogOptions options;
		options.title 	= "Exit Settings";
		options.prompt  = "Are you sure you want to exit Settings?";
		options.buttonA = DialogButtonOption("Yes", "Yes");
		options.buttonB = DialogButtonOption("No", "No");
		showModalDialog(DialogTypeExitSettings, options);
	}
}


/// <summary>
/// Base scene feature, handling controller pairing
/// </summary>
void ParentScene::onControllerSynced(HWController* controller, bool success)
{
	if ( currentScreen != null && currentScreen == controllerScreen )
	{ currentScreen->handleControllerSync(success);}

	super::onControllerSynced(controller, success);
}

/// <summary>
/// Base scene feature, handling controller connecting
/// </summary>
void ParentScene::onControllerConnected(HWController* controller)
{
	if ( currentScreen != null && currentScreen == controllerScreen )
	{ currentScreen->handleControllerConnected();}

	super::onControllerConnected(controller);
}

/// <summary>
/// Base scene feature, handling controller disconnecting
/// </summary>
void ParentScene::onControllerDisconnected(HWController* controller)
{
	if ( currentScreen != null && currentScreen == controllerScreen )
	{ currentScreen->handleControllerDisconnected();}

	super::onControllerDisconnected(controller);
}


/// <summary>
/// onDialogHandled
/// </summary>
void ParentScene::onDialogHandled( DialogType dialogType, DialogButtonOption option )
{
	if(dialogType == DialogTypeExitSettings && option.id == "Yes")
	{
		exitToChildScene();
	}
	else if( dialogType == DialogTypeActivationFail )
	{
		if( option.id == "Quit" )
		{ popScreen(); }
		else
		{ currentScreen->handleDialogClosed(dialogType, option); }
	}
	else if ( dialogType == DialogTypeSkipActivation )
	{
		if ( option.id == "Yes" )
		{ popScreen(); }
		else
		{ currentScreen->handleDialogClosed(dialogType, option); }
	}
	else
	{ currentScreen->handleDialogClosed(dialogType, option); }

	super::onDialogHandled(dialogType, option);
}

/// <summary>
/// When a dialog is canceled, meaning an option wasn't selected but it was dismissed.
/// </summary>
void ParentScene::onDialogCanceled( DialogType dialogType)
{
	if(currentScreen != null)
		currentScreen->handleDialogCanceled(dialogType);
	super::onDialogCanceled(dialogType);
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void ParentScene::onSelectionChange( GameObject* old, GameObject* newObj)
{
	if( old != null && newObj != null )
	{
		playSound("Parent_Highlight");
		printf("Selection changed from %s to %s \n", old->name.str(), newObj->name.str() );
	}
	else if ( old != null && newObj == null )
	{  printf("Selection changed from %s to nothing. \n", old->name.str() ); }
	else if ( old == null && newObj != null )
	{  printf("Selection changed from nothing to %s. \n", newObj->name.str() ); }

	if(old != null &&  old->typeOf( ButtonObject::type()) && old != newObj )
	{
		((ButtonObject*)old)->cancelActionsAsComplete();
		((ButtonObject*)old)->setState(ButtonStateNormal);
	}

	if(newObj != null &&  newObj->typeOf( ButtonObject::type()) )
	{
		((ButtonObject*)newObj)->setState(ButtonStateActive);
	}


	if( currentModal == null )
	{
		if( currentScreen != null )
		{ currentScreen->handleSelectionChange(old, newObj); }
	}

	super::onSelectionChange(old, newObj);
}


void ParentScene::onEthernetPluggedIn()
{
	if( currentScreen == wifiScreen )
	{
		replaceScreen(ethernetScreen);
	}
	else
	{ super::onEthernetPluggedIn(); }
}

void ParentScene::onEthernetUnplugged()
{
	if( currentScreen == ethernetScreen )
	{
		replaceScreen(wifiScreen);
	}
	else
	{ super::onEthernetUnplugged();}
}

/// <summary>
/// Network error occured
/// </summary>
void ParentScene::onNetworkError(String networkName, String networkError)
{
	printf("ParentScene::onNetworkError [%s] msg[%s]\n", networkName.str(), networkError.str());
	if( currentScreen != null )
	{
		currentScreen->handleNetworkError(networkName, networkError);

		if( currentScreen == wifiScreen )
		{
			currentHighlight = currentScreen->getArrivalSelection();
			if( currentHighlight != NULL )
			{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
		}
	}
}

/// <summary>
/// Network connection made
/// </summary>
void ParentScene::onNetworkConnected( String networkName )
{
	printf("ParentScene::onNetworkConnected for %s\n", networkName.str());
	if( currentScreen != null && (currentScreen == wifiScreen || currentScreen == ethernetScreen))
	{ currentScreen->handleNetworkConnected(networkName); }

	//allow activation and download step to go ahead and handle this when off the scene graph
	registerScreen->handleNetworkConnected(networkName);
	updateScreen->handleNetworkConnected(networkName);
}


/// <summary>
/// Base scene feature, show contextual back button
/// </summary>currentScreen
void ParentScene::showBackButton(float delay, bool isCurrentHighlight)
{
	if( delay <= 0 )
		{ ThrowException::invalidApiException(); }

	willHighlightBackButton = isCurrentHighlight;
	backButtonTimer = delay;
}

/// <summary>
/// Base scene feature, hide contextual back button
/// </summary>
void ParentScene::hideBackButton(float delay)
{
	printf("hiding parent scene global back button.\n");
	if(currentHighlight == prevButton)
		removeSelection();
	prevButton->bubbleOut(delay);
}

/// <summary>
/// Base scene feature, show contextual help button
/// </summary>
void ParentScene::showHelpButton(float delay, bool isCurrentHighlight)
{
	if( delay <= 0 )
	{ ThrowException::invalidApiException(); }

	willHighlightHelpButton = isCurrentHighlight;
	helpButtonTimer = delay;
}

/// <summary>
/// Base scene feature, hide contextual help button
/// </summary>
void ParentScene::hideHelpButton(float delay)
{
	if(currentHighlight == helpButton)
		removeSelection();

	helpButton->bubbleOut(delay);
	worldRoot->findLabel("helpLabel")->fadeOut(delay);
}

/// <summary>
/// Base scene feature, tick
/// </summary>
void ParentScene::update(float dt)
{
	if( exitTimer > 0 )
	{
		exitTimer -= dt;
		if( exitTimer <= 0 )
		{SceneManager::getInstance()->launchChildScene(Constants::System::appID);}
	}

	if ( backButtonTimer > 0 )
	{
		backButtonTimer -= dt;
		if ( backButtonTimer <= 0)
		{
			printf("showing parent scene global back button.\n");
			prevButton->cancelActions();
			prevButton->bubbleIn(0);
			setupHotSpots();

			if(willHighlightBackButton)
			{
				prevButton->originalScale.debugLog();
				printf("Actually taking over current highlight\n");
				//Don't override selection if dialog is up
				if( currentModal == null )
				{ setCurrentHighlight(prevButton); }
				else
				{ preDialogSelection = prevButton; }
			}

			willHighlightBackButton = false;
		}
	}

	if ( helpButtonTimer > 0 )
	{
		helpButtonTimer -= dt;
		if ( helpButtonTimer <= 0)
		{

			helpButton->bubbleIn(0);
			worldRoot->findLabel("helpLabel")->fadeIn(0);
			setupHotSpots();

			if(willHighlightHelpButton)
			{
				printf("Actually taking over current highlight\n");
				//Don't override selection if dialog is up
				if( currentModal == null )
				{ setCurrentHighlight(helpButton); }
				else
				{ preDialogSelection = helpButton; }
			}

			willHighlightHelpButton = false;
		}
	}

	super::update(dt);
}

}

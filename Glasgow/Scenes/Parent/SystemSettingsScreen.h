#ifndef SYSTEMSETTINGSSCREEN_H_
#define SYSTEMSETTINGSSCREEN_H_

#include "ParentScreen.h"

namespace Glasgow
{

class SystemSettingsScreen : public ParentScreen
{
	CLASSEXTENDS(SystemSettingsScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	SystemSettingsScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~SystemSettingsScreen();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

private:
	bool highlightUpdate;
	ButtonObject* updateButton;
	ButtonObject* resetButton;

	TextLabel* serialNoLabel;
	TextLabel* firmwareVersionLabel;
	TextLabel* macAddressLabel;
	TextLabel* upToDatePrompt;
};

} /* namespace Glasgow */
#endif /* SYSTEMSETTINGSSCREEN_H_ */

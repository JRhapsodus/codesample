#ifndef PACKAGEDETAILSSCREEN_H_
#define PACKAGEDETAILSSCREEN_H_

#include "ParentScreen.h"

namespace Glasgow
{

class PackageDetailsScreen : public ParentScreen
{
	CLASSEXTENDS(PackageDetailsScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	PackageDetailsScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~PackageDetailsScreen();
};

} /* namespace Glasgow */
#endif /* PACKAGEDETAILSSCREEN_H_ */

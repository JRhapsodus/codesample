#include "ChangePinScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

ChangePinScreen::ChangePinScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	enterRoot				= find("enterRoot");
	finalRoot				= find("finalRoot");
	lockRoot				= enterRoot->find("lockRoot");

	numPad					= (GateLockObject*)enterRoot->find("numPad");
	confirmPinField			= enterRoot->find("confirmPinField");
	pinField				= enterRoot->find("pinField");

	confirmPrompt			= enterRoot->findLabel("confirmPrompt");
	newPinPrompt			= enterRoot->findLabel("newPinPrompt");

	errorConfirmPinPrompt	= enterRoot->findLabel("errorConfirmPinPrompt");

	confirmPinLabel			= confirmPinField->findLabel("confirmPinText");
	pinLabel				= pinField->findLabel("pinText");
	finalPinLabel			= finalRoot->findLabel("finalPin");

	lockIcon				= lockRoot->findSprite("lockedIcon");
	unlockIcon				= lockRoot->findSprite("unlockedIcon");

	pinState				= ENTER_NEW_PIN_STATE;

	enteredPin = confirmPin = "";
	pinSequence = "";

	origPinPosition 		= pinField->getPosition();
	origFinalRootPosition	= finalRoot->getPosition();
	origEnterRootPosition	= enterRoot->getPosition();

	pinLabel->setBatched(false);
	confirmPinLabel->setBatched(false);

	setupInitialState();
}

ChangePinScreen::~ChangePinScreen()
{
	printf("ChangePinScreen::~ChangePinScreen()\n");
}

void ChangePinScreen::setupInitialState()
{
	finalRoot->setPosition(origFinalRootPosition);
	enterRoot->setPosition(origEnterRootPosition);

	errorConfirmPinPrompt->setVisible(false);
	confirmPrompt->setVisible(false);
	newPinPrompt->setVisible(false);
	pinField->setVisible(true);
	confirmPinField->setVisible(false);
	lockRoot->setVisible(false);		//hiding this for GLUI-294
	lockIcon->setVisible(false);
	unlockIcon->setVisible(false);
	finalRoot->setVisible(false);
	numPad->setVisible(true);

	pinLabel->setText("");
	confirmPinLabel->setText("");
	finalPinLabel->setText("");

	confirmPin = enteredPin = "";
	pinSequence = "";

	pinState = ENTER_NEW_PIN_STATE;
}


void ChangePinScreen::loadGameObjectState(GameObjectState state)
{
	printf("ChangePinScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

void ChangePinScreen::saveSettings()
{
	printf("ChangePinScreen::saveSettings");
}
/// <summary>
/// Pin entered
/// </summary>
void ChangePinScreen::pinEntered()
{
	switch(pinState)
	{
		case ENTER_NEW_PIN_STATE:
			//show next state
			parentScene->lockOutInput(0.75f);

			pinField->moveBy(Vec3f::Up * 95, 0.75f);
			confirmPinField->bubbleIn(1);

			newPinPrompt->fadeOut();
			confirmPrompt->fadeIn(0.75f);

			// Hide the error label if it was showing from an earlier failure
			errorConfirmPinPrompt->cancelActions();
			errorConfirmPinPrompt->fadeOut();

			pinState = CONFIRM_NEW_PIN_STATE;
			break;

		case CONFIRM_NEW_PIN_STATE:
			if(confirmPin == enteredPin)
			{
				//confirmed pin success
				parentScene->lockOutInput(1);

				confirmPrompt->fadeOut();

				finalRoot->setPosition(origFinalRootPosition);
				enterRoot->setPosition(origEnterRootPosition);
				finalRoot->moveBy(Vec3f::Right*1280, 0.001f);
				finalPinLabel->setText(confirmPin);
				finalRoot->makeVisible(true);

				enterRoot->moveBy(Vec3f::Left*1280, 1);
				enterRoot->makeVisible(false);
				finalRoot->moveTo(origFinalRootPosition, 1);

				//save the pins to settings
				SystemSettings::setPin(confirmPin);
				enteredPin = confirmPin = "";
				pinSequence = "";

				pinState = FINAL_PIN_STATE;

				parentScene->showBackButton(1,true);
			}
			else
			{
				pinError();
			}
			break;

		default:
			break;
	}
	pinSequence = "";
}

/// <summary>
/// Pin entered incorrectly, start over
/// </summary>
void ChangePinScreen::pinError()
{
	parentScene->lockOutInput(1);

	float delay = 0.25f;
	pinLabel->setText("");
	confirmPinLabel->setText("");
	pinField->moveTo(origPinPosition,0.01f);
	confirmPinField->setVisible(false);

	lockIcon->fadeIn(delay);
	unlockIcon->fadeOut(delay);

	confirmPrompt->recursiveFadeOut(delay);
	newPinPrompt->recursiveFadeIn(delay);

	enteredPin = confirmPin = "";
	pinSequence = "";

	errorConfirmPinPrompt->cancelActions();
	errorConfirmPinPrompt->setVisible(false);
	errorConfirmPinPrompt->fadeIn();
	errorConfirmPinPrompt->waitFor(4);
	errorConfirmPinPrompt->fadeOut();

	pinState = ENTER_NEW_PIN_STATE;
}

/// <summary>
/// resets the enter root settings to display for entering new pin
/// </summary>
void ChangePinScreen::resetEnterRoot(float delay)
{
	pinLabel->waitFor(delay);
	pinLabel->changeTo("");
	confirmPinLabel->waitFor(delay);
	confirmPinLabel->changeTo("");
	pinField->waitFor(delay);
	pinField->moveTo(origPinPosition,0.01f);
	confirmPinField->bubbleOut(delay);

	lockIcon->fadeIn(delay);
	unlockIcon->fadeOut(delay);

	confirmPrompt->recursiveFadeOut(delay);
	newPinPrompt->recursiveFadeIn(delay);

	enteredPin = confirmPin = "";
	pinSequence = "";

	errorConfirmPinPrompt->cancelActions();
	errorConfirmPinPrompt->waitFor(delay);
	errorConfirmPinPrompt->makeVisible(false);
}

void ChangePinScreen::ApplyPinValue(TextLabel*& label, String& pin, String value)
{
	if( value == "backspacebutton" )
	{
		if(pin.length() > 0)
		{
			pinSequence.append(value);
			pin = pin.substring(0, pin.length()-1);
			parentScene->playSound("Parent_Keystroke_Backspace");
			label->removeLastCharacter();
		}
	}
	else
	{
		pinSequence.append(value);
		parentScene->playSound("Parent_Keystroke");
		pin += value;
		int len = pin.length();
		String encryptedPin = "";
		while(len--)
		{ encryptedPin.append(Constants::UI::encryptionDisplayPattern); }
		label->setText(encryptedPin);
		label->fadeLastCharacterIn();
		if( pin.length() == 4 )
		{ pinEntered(); }
	}
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ChangePinScreen::handlePressAction(GameObject*& objectHighlight)
{
	if ( objectHighlight != null && objectHighlight->descendantOf(numPad) )
	{
		parentScene->lockOutInput(0.18f);

		String pinValue = objectHighlight->name;

		switch(pinState)
		{
			case ENTER_NEW_PIN_STATE:	ApplyPinValue(pinLabel, enteredPin, pinValue); break;

			case CONFIRM_NEW_PIN_STATE:	ApplyPinValue(confirmPinLabel, confirmPin, pinValue); break;

			default: break;
		}

		return true;
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ChangePinScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ChangePinScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f);
	parentScene->showHelpButton(0.75f);

	newPinPrompt->recursiveFadeIn(1.5f);
	lockIcon->fadeIn();

	pinState = ENTER_NEW_PIN_STATE;
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ChangePinScreen::getArrivalSelection()
{ return numPad->find("1"); }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ChangePinScreen::addHotSpots( ObjectArray* trackingList )
{
	if(pinState != FINAL_PIN_STATE)
	{	numPad->addHotSpots(trackingList);}
}

/// <summary>
/// Custom update
/// </summary>
void ChangePinScreen::update(float dt)
{
	super::update(dt);
}

}

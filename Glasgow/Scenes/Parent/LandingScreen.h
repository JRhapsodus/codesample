#ifndef LANDINGSCREEN_H_
#define LANDINGSCREEN_H_

#include "ParentScreen.h"

namespace Glasgow
{

class LandingScreen : public ParentScreen
{
	CLASSEXTENDS(LandingScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	LandingScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~LandingScreen();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	virtual void loadGameObjectState(GameObjectState state);

	///<summary>
	/// Callback
	///</summary>
	virtual void onCallBack( String id );

	void setupInitialState();

private:
	SpriteObject* downloadIcon;
	SpriteObject* errorIcon;
	ButtonObject* manageContentButton;
	ButtonObject* downloadManagerButton;
	ButtonObject* editProfilesButton;
	ButtonObject* settingsButton;
};

} /* namespace Glasgow */
#endif /* LANDINGSCREEN_H_ */

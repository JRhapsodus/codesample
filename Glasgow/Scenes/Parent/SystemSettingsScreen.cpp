#include "SystemSettingsScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

SystemSettingsScreen::SystemSettingsScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	updateButton 			= findButton("updateButton");
	resetButton				= findButton("resetButton");
	firmwareVersionLabel 	= findLabel("firmwareVersion");
	serialNoLabel 			= findLabel("serialNoLabel");
	macAddressLabel			= findLabel("MACAddress");
	upToDatePrompt			= findLabel("upToDatePrompt");

	setupInitialState();
}

SystemSettingsScreen::~SystemSettingsScreen()
{ printf("SystemSettingsScreen::~SystemSettingsScreen\n"); }


void SystemSettingsScreen::setupInitialState()
{
	highlightUpdate = false;
	updateButton->setVisible(false);
	resetButton->setVisible(false);
	upToDatePrompt->setVisible(false);
}

void SystemSettingsScreen::loadGameObjectState(GameObjectState state)
{
	printf("SystemSettingsScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}
/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool SystemSettingsScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(objectHighlight != null)
	{
		if(objectHighlight == updateButton)
		{
			if(ConnectionManager::getInstance()->isOnline())
			{
				bool lfServersOnline = ConnectionManager::getInstance()->leapFrogServersResponded;
				if(lfServersOnline)
				{
					printf("SystemSettingsScreen::Pressed updateButton\n");
					parentScene->pushScreen(ParentScene::PS_UPDATE_SCREEN);
				}
				else
				{
					DialogOptions options;
					options.title 	= "Error";
					options.prompt  = "We are unable to connect to LeapFrog servers. Please check your Internet connection and try again later.";
					options.buttonA = DialogButtonOption("OK", "Ok");
					parentScene->showModalDialog(DialogTypeInternetConnectionRequired, options);
					return true;
				}
			}
			else
			{
				DialogOptions options;
				options.title 	= "Network Error";
				options.prompt  = "System update requires an internet connection. Go to Settings, then Network to set up an internet connection.";
				options.buttonA = DialogButtonOption("OK", "Ok");
				parentScene->showModalDialog(DialogTypeInternetConnectionRequired, options);
				return true;
			}

		}
		else if(objectHighlight == resetButton)
		{ printf("Pressed resetButton\n"); }
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool SystemSettingsScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void SystemSettingsScreen::cameraArrived()
{
	serialNoLabel->setText(SystemSettings::getSerialNumber());
	firmwareVersionLabel->setText(SystemSettings::getFirmwareVersion());

	if( ConnectionManager::getInstance()->hasEthernet() )
	{ macAddressLabel->setText(SystemSettings::getEthernetMacAddress()); }
	else
	{ macAddressLabel->setText(SystemSettings::getWifiMacAddress()); }



	FirmwareStatus status = ContentManager::getInstance()->getFirmwareStatus();
	if( status == FirmwareReady || status == FirmwarePending )
	{
		printf("SystemSettingsScreen::cameraArrived  [%s]\n", status == FirmwareReady ? "FirmwareReady" : "FirmwarePending");
		if(parentScene->fwDownloadErrorOccured)
		{
			updateButton->cancelActions();
			updateButton->setVisible(false);
			parentScene->showBackButton(0.75f,true);
		}
		else
		{
			highlightUpdate = true;
			parentScene->showBackButton(0.75f,false);
			updateButton->bubbleIn();
		}
	}
	else
	{
		printf("SystemSettingsScreen::cameraArrived  [FirmwareUnavailable]\n");
		upToDatePrompt->recursiveFadeIn();
		parentScene->showBackButton(0.75f,true);
	}
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* SystemSettingsScreen::getArrivalSelection()
{
	if(highlightUpdate || updateButton->isVisibleHierarchy())
		return updateButton;

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void SystemSettingsScreen::addHotSpots( ObjectArray* trackingList )
{
	if(updateButton->isVisibleHierarchy())
		trackingList->addElement(updateButton);
	if(resetButton->isVisibleHierarchy())
		trackingList->addElement(resetButton);
}


} /* namespace Glasgow */

#ifndef GATELOCKSCREEN_H_
#define GATELOCKSCREEN_H_

#include "ParentScreen.h"
#include "SIEngine/Include/GlasgowControls.h"

namespace Glasgow
{

class GateLockScreen : public ParentScreen
{
	CLASSEXTENDS(GateLockScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	GateLockScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~GateLockScreen();
	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Custom update
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Pin entered
	/// </summary>
	void pinEntered();

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

private:
	String 				enteredPin;
	String				pinSequence;
	GateLockObject* 	numPad;
	TextLabel* 			errorPrompt;
	TextLabel*			pinLabel;

	float				exitTimer;
};

} /* namespace Glasgow */
#endif /* GATELOCKSCREEN_H_ */

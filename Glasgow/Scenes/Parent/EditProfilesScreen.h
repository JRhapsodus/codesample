#ifndef EDITPROFILESSCREEN_H_
#define EDITPROFILESSCREEN_H_

#include "ParentScreen.h"
#include "SIEngine/Include/GlasgowControls.h"

namespace Glasgow
{

class EditProfilesScreen : public ParentScreen
{
	CLASSEXTENDS(EditProfilesScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	EditProfilesScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~EditProfilesScreen();

	enum CreationStep
	{
		CreationStepGender,
		CreationStepGrade,
		CreationStepMonth,
		CreationStepDay,
		CreationStepYear,
		CreationStepConfirm,
		CreationStepCreated,
		CreationStepName
	};

	/// <summary>
	/// saves user settings
	/// </summary>
	virtual void saveSettings();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Custom update
	/// </summary>
	virtual void update(float dt);

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

private:

	/// <summary>
	/// Takes the new user information and adds the Glasgow based profile
	/// </summary>
	void addNewUser();

	/// <summary>
	/// Animates to the next step and returns the new default selection
	/// </summary>
	GameObject* gotoNextSubStep();

	/// <summary>
	/// validates the user name field
	/// </summary>
	bool validNameEntry(String str);

	/// <summary>
	/// returns the localized string for a given grade button name
	/// </summary>
	String getLocalizedStringFromGradeButtonName(String buttonName);

	void setLocalizedGradeTextInChildren(GameObject* root);

	CreationStep 	currentStep;

	String			chosenGender;
	String			chosenDay;
	String			chosenMonth;
	String			chosenGrade;
	String			chosenYear;
	String			chosenName;
	GateLockObject* yearNumPad;
	KeyboardObject* keyboard;
	GameObject*		birthDayRoot;
	GameObject* 	birthMonthRoot;
	GameObject* 	birthYearRoot;
	GameObject* 	genderRoot;
	GameObject* 	gradeLevelRoot;
	GameObject* 	nameRoot;
	GameObject*		confirmRoot;

	bool 			changing;
	bool			isExiting;
	float			exitTimer;

	TextLabel*		headerPrompt;
	TextLabel*		nameErrorPrompt;
	TextLabel*		nameEnterPrompt;

	ButtonObject*	nameDoneButton;
	TextLabel*		yearField;
	TextLabel*		nameField;
	GameObject*		nameFieldContainer;

	Calender*		calender;

	float exitToChildTimer;
	User* currentUser;
};

}
#endif

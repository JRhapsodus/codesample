#include "EthernetScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

EthernetScreen::EthernetScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	ipAddress					= findLabel("ipAddress");
	macAddress					= findLabel("macAddress");
	connectedPrompt				= findLabel("connectedPrompt");
	connectingPrompt			= findLabel("connectingPrompt");
	errorPrompt					= findLabel("errorPrompt");
	connectionSprite			= findSprite("connectionStatus");
	disconnectedSprite			= findSprite("connectionFailed");

	setupInitialState();
}

EthernetScreen::~EthernetScreen()
{
	printf("EthernetScreen::~EthernetScreen()\n");

}

void EthernetScreen::setupInitialState()
{
	testConnectionTimer = -1;
	ipAddress->setVisible(false);
	macAddress->setVisible(false);
	connectedPrompt->setVisible(false);
	errorPrompt->setVisible(false);
	connectionSprite->setVisible(false);
	disconnectedSprite->setVisible(false);
}


void EthernetScreen::loadGameObjectState(GameObjectState state)
{
	printf("EthernetScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool EthernetScreen::handlePressAction(GameObject*& objectHighlight)
{

	return false;

}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool EthernetScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void EthernetScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f,true);
	parentScene->showHelpButton(0.75f);

	ipAddress->setVisible(false);
	macAddress->setVisible(false);
	connectedPrompt->setVisible(false);
	connectingPrompt->recursiveFadeIn(1);
	errorPrompt->setVisible(false);
	connectionSprite->setVisible(false);
	connectionSprite->waitFor(1);
	connectionSprite->makeVisible(true);
	connectionSprite->scaleTo( connectionSprite->originalScale, 0.25f);
	disconnectedSprite->setVisible(false);
	testConnectionTimer = 3;
	ConnectionManager::getInstance()->connectToEthernet();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* EthernetScreen::getArrivalSelection()
{
	return null;

}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void EthernetScreen::addHotSpots( ObjectArray* trackingList )
{}

/// <summary>
/// Network error occured
/// </summary>
void EthernetScreen::handleNetworkError(String networkName, String networkError)
{
}

/// <summary>
/// Network connection made
/// </summary>
void EthernetScreen::handleNetworkConnected( String networkName )
{
	parentScene->playSound("Base_TransformSuccess");
}

/// <summary>
/// Asks connection manager for information regarding the test
/// </summary>
void EthernetScreen::testConnection()
{
	bool isOnline = ConnectionManager::getInstance()->isServiceOnline("Wired");
	String ip = ConnectionManager::getInstance()->getIp("Wired");

	printf("EthernetScreen::testConnection() => Online [%d] ipAddress [%s]\n", (int)isOnline, ip.str());

	if( isOnline )
	{
		parentScene->playSound("Base_TransformSuccess");
		FILE* f = fopen("/sys/class/net/eth0/address", "r");
		String mac = "";
		if(f)
		{
			char buf[10];
			while( !feof(f) )
			{
				fread(buf,1,1,f);
				if(buf[0] != '\n')
				{ mac += String(buf[0]); }
			}

			fclose(f);
		}

		//TODO Localize
		macAddress->setText(String("MAC Address: ") + mac);
		ipAddress->setText(String("IP Address: ") + ip);

		ipAddress->recursiveFadeIn(0.25f);
		macAddress->recursiveFadeIn(0.25f);
		connectedPrompt->recursiveFadeIn(0.25f);

		connectingPrompt->recursiveFadeOut();
	}
	else
	{
		parentScene->playSound("Base_WiFiDisconnected");
		errorPrompt->recursiveFadeIn(0.25f);
		connectingPrompt->recursiveFadeOut();

		connectionSprite->setVisible(false);
		disconnectedSprite->setVisible(true);
	}
}


/// <summary>
/// Custom update
/// </summary>
void EthernetScreen::update(float dt)
{
	if( testConnectionTimer > 0 )
	{
		testConnectionTimer -= dt;
		if( testConnectionTimer <= 0 )
		{ testConnection(); }
	}

	super::update(dt);
}


}

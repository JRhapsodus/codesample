#ifndef PURCHASEDMANAGERSCREEN_H_
#define PURCHASEDMANAGERSCREEN_H_


#include "ParentScreen.h"

namespace Glasgow
{

class PurchasedManagerScreen : public ParentScreen
{
	CLASSEXTENDS(SystemSettingsScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	PurchasedManagerScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~PurchasedManagerScreen();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Handle package changed
	/// </summary>
	virtual void handlePackageChanged(tPackageStatus newStatus, PackageInfo* info );

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Base scene feature, tick
	/// </summary>
	void update(float dt);

private:
	void addPurchasedItemUIToScreen();
	void discoverPurchasedItems();
	TextLabel* headerLabel;
	TextLabel* noPurchasesLabel;
	ButtonObject* pauseButton;
	ObjectArray* purchasedItems;
};

} /* namespace Glasgow */
#endif /* PURCHASEDMANAGERSCREEN_H_ */

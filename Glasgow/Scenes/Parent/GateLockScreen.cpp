#include "GateLockScreen.h"
#include "ParentScene.h"
namespace Glasgow
{

GateLockScreen::GateLockScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	numPad					= (GateLockObject*)find("numpad");
	pinLabel				= findLabel("inputField");
	errorPrompt				= findLabel("errorLabel");

	pinLabel->setBatched(false);

	setupInitialState();
}

GateLockScreen::~GateLockScreen()
{
	printf("GateLockScreen::~GateLockScreen()\n");
}

void GateLockScreen::setupInitialState()
{
	errorPrompt->setVisible(false);
	enteredPin = "";
	pinSequence	= "";
	exitTimer = 0;
	pinLabel->setText("");
}

void GateLockScreen::loadGameObjectState(GameObjectState state)
{
	printf("ChangePinScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// Pin entered
/// </summary>
void GateLockScreen::pinEntered()
{
	printf("Pin entered:  %s\n", enteredPin.str() );
	if(SystemSettings::verifyPin(enteredPin) || SystemSettings::verifyBackdoorSequence(pinSequence))
	{
		//Success case!
		parentScene->removeSelection();
		parentScene->lockOutInput(1);
		errorPrompt->cancelActions();
		errorPrompt->recursiveFadeOut();
		exitTimer = 0.5f;
	}
	else
	{
		//error case
		parentScene->lockOutInput(1.5f);

		enteredPin = "";
		pinSequence = "";
		pinLabel->waitFor(1);
		pinLabel->changeTo("");

		errorPrompt->cancelActions();
		errorPrompt->recursiveFadeIn(0.5f);
		errorPrompt->recursiveFadeOut(4);
	}
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool GateLockScreen::handlePressAction(GameObject*& objectHighlight)
{

	if ( objectHighlight != null && objectHighlight->descendantOf(numPad) )
	{
		parentScene->lockOutInput(0.18f);

		String pinValue = objectHighlight->name;
		pinSequence.append(pinValue);
		if( pinValue == "backspacebutton" &&  enteredPin.length() > 0 )
		{
			enteredPin = enteredPin.substring(0, enteredPin.length()-1);
//			parentScene->playSound("Parent_Keystroke_Backspace");
			pinLabel->removeLastCharacter();
			if(enteredPin.length() == 0)
				pinSequence = "";
		}
		else if(pinValue != "backspacebutton")
		{
//			parentScene->playSound("Parent_Keystroke");
			enteredPin += pinValue;

			int len = enteredPin.length();
			String encryptedPin = "";
			while(len--)
			{ encryptedPin.append(Constants::UI::encryptionDisplayPattern); }
			pinLabel->setText(encryptedPin);
			pinLabel->fadeLastCharacterIn();
			if( enteredPin.length() == 4 )
			{
				pinEntered();
			}
		}
		else
		{
			pinSequence = "";
		}

	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool GateLockScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void GateLockScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f,false);
	parentScene->showHelpButton(0.75f);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* GateLockScreen::getArrivalSelection()
{ return numPad->find("1"); }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void GateLockScreen::addHotSpots( ObjectArray* trackingList )
{
	numPad->addHotSpots(trackingList);
}

/// <summary>
/// Custom update
/// </summary>
void GateLockScreen::update(float dt)
{
	if(exitTimer > 0)
	{
		exitTimer -= dt;
		if(exitTimer <= 0)
			parentScene->parentGatePassed();
	}

	super::update(dt);
}



}

#include "LocaleScreen.h"
#include "ParentScene.h"
namespace Glasgow
{

LocaleScreen::LocaleScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	setupInitialState();
}

LocaleScreen::~LocaleScreen()
{
	printf("LocaleScreen::~LocaleScreen()\n");
}

void LocaleScreen::setupInitialState()
{}

void LocaleScreen::loadGameObjectState(GameObjectState state)
{
	printf("LocaleScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// saves user settings
/// </summary>
void LocaleScreen::saveSettings()
{
	SystemSettings::setSystemLocale(chosenLocale);
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void LocaleScreen::addHotSpots( ObjectArray* trackingList )
{
	printf("LocaleScreen::addHotSpots\n");

	trackingList->addElement( find("localeAustralia") );
	trackingList->addElement( find("localeCanada") );
	trackingList->addElement( find("localeIreland") );
	trackingList->addElement( find("localeNewZealand") );
	trackingList->addElement( find("localeOther") );
	trackingList->addElement( find("localeUK") );
	trackingList->addElement( find("localeUnitedStates") );
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool LocaleScreen::handlePressAction(GameObject*& objectHighlight)
{
	if( objectHighlight != null && objectHighlight->typeOf(CheckButton::type()) )
	{
		deselectAllCheckBoxes();

		selectedLocale = (CheckButton*)objectHighlight;
		selectedLocale->setState(ButtonStateActive);
		selectedLocale->setChecked(true);
		String locale =  objectHighlight->name.str();

		printf("Chose Locale: %s\n", locale.str());

		if(locale.equals("localeAustralia"))
		{ chosenLocale = SystemLocaleAustralia;}
		else if(locale.equals("localeCanada"))
		{ chosenLocale = SystemLocaleCanada;}
		else if(locale.equals("localeIreland"))
		{ chosenLocale = SystemLocaleIreland;}
		else if(locale.equals("localeNewZealand"))
		{ chosenLocale = SystemLocaleNewZealand;}
		else if(locale.equals("localeUK"))
		{ chosenLocale = SystemLocaleUnitedKingdom;}
		else if(locale.equals("localeUnitedStates"))
		{ chosenLocale = SystemLocaleUnitedStates;}
		else
		{ chosenLocale = SystemLocaleDefault;}

		parentScene->lockOutInput(0.5f);
		StringsManager::getInstance()->changeSystemLocale(chosenLocale);
		SystemSettings::setSystemLocale(chosenLocale);
		parentScene->getWorldRoot()->refreshLocalizedStringsRecursive();

		return true;
	}

	return false;
}

/// <summary>
/// Deselects all checkboxes on screen
/// </summary>
void LocaleScreen::deselectAllCheckBoxes()
{
	ObjectArray* children = getChildren();
	for(int i = 0; i < children->getSize(); ++i)
	{
		BaseObject* obj = children->elementAt(i);
		if(obj->typeOf(CheckButton::type()))
		{
			((CheckButton*)obj)->setChecked(false);
			((CheckButton*)obj)->setState(ButtonStateNormal);
		}
	}
}

/// <summary>
/// If we want to handle the press back ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool LocaleScreen::handlePressBack(GameObject*& objectHighlight)
{return false;}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool LocaleScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	return super::handleAnlogChange(objectLeft, objectEntered, currentHighlight);
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void LocaleScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f,false);

	deselectAllCheckBoxes();
	chosenLocale = SystemSettings::getSystemLocale();
	selectedLocale = getLocaleButton(chosenLocale);
	if(selectedLocale != null)
	{
		selectedLocale->setState(ButtonStateActive);
		selectedLocale->setChecked(true);
	}
}

/// <summary>
/// Returns the selected locale
/// </summary>
String LocaleScreen::getLocaleName()
{ return selectedLocale->name; }

/// <summary>
/// Returns the button for the locale
/// </summary>
CheckButton* LocaleScreen::getLocaleButton(SystemLocale locale)
{
	switch(locale)
	{
		case SystemLocaleAustralia: 	return (CheckButton*)findButton("localeAustralia");		break;
		case SystemLocaleCanada:		return (CheckButton*)findButton("localeCanada");		break;
		case SystemLocaleIreland:		return (CheckButton*)findButton("localeIreland");		break;
		case SystemLocaleNewZealand:	return (CheckButton*)findButton("localeNewZealand");	break;
		case SystemLocaleUnitedKingdom:	return (CheckButton*)findButton("localeUK");			break;
		case SystemLocaleUnitedStates:	return (CheckButton*)findButton("localeUnitedStates");	break;
		case SystemLocaleDefault:		return (CheckButton*)findButton("localeOther");			break;
	}

	return null;
}
/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void LocaleScreen::cameraLeaving()
{
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* LocaleScreen::getArrivalSelection()
{ return selectedLocale; }


}

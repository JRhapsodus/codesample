#include "WifiScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

WifiScreen::~WifiScreen()
{
	printf("WifiScreen::~WifiScreen()\n");
	wifiSlider->removeAll();
}

WifiScreen::WifiScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	connectedRoot		= find("connectedRoot");
	connectingRoot		= find("connectionStatus");
	passwordRoot		= find("passwordRoot");
	wifiList 			= find("wifiList");

	ethernetPrompt		= findLabel("ethernetPrompt");
	entryPrompt			= findLabel("entryPrompt");
	connectingPrompt	= findLabel("connectingPrompt");
	errorPrompt			= findLabel("errorPrompt");
	scanButton			= findButton("scanButton");
	wifiBackButton		= findButton("wifiBackButton");

	connectedPrompt		= connectedRoot->findLabel("connectedPrompt");
	disconnectButton	= connectedRoot->findButton("disconnectButton");

	wifiSlider		 	= (VerticalSlider*)wifiList->find("verticalSlider");
	noNetworksPrompt	= wifiList->findLabel("noNetworksFound");
	scanningPrompt		= wifiList->findLabel("scanning");

	macAddress			= entryPrompt->findLabel("macAddress");


	passwordField		= passwordRoot->find("passwordField")->findLabel("inputEntry");
	connectButton		= passwordRoot->findButton("connectButton");

	connectedStrength1  = connectingRoot->findSprite("wifi1");
	connectedStrength2  = connectingRoot->findSprite("wifi2");
	connectedStrength3  = connectingRoot->findSprite("wifi3");
	connectedStrength4  = connectingRoot->findSprite("wifi4");
	transmitting  		= connectingRoot->findSprite("wifiAnimation");

	keyboard			= new KeyboardObject((Scene*)scene, true);
	passwordRoot		= find("passwordRoot");
	passwordField		= passwordRoot->find("passwordField")->findLabel("inputEntry");
	connectButton		= passwordRoot->findButton("connectButton");

	passwordField->setBatched(false);
	passwordRoot->addChild(keyboard);

	keyboard->release();

	setupInitialState();
}

void WifiScreen::setupInitialState()
{
	connected 			= false;
	networkError 		= false;
	firstScan 			= true;
	keyboardPresent 	= false;
	attemptingConnect 	= false;
	connectingTimer		= -1;

	wifiBackButton->setVisible(false);
	passwordRoot->setVisible(false);
	passwordField->setText("");

	connectedRoot->setVisible(false);
	disconnectButton->setVisible(false);

	keyboard->setVisible(false);
	keyboard->reset();
	keyboard->setPosition( Vec3f( 0, -75, -100 ) );
	keyboard->setText("");
	keyboard->setMaxCharacters(Constants::System::wifiPasswordMaximumCharLimit);

	wifiSlider->removeAll();
	wifiList->setPosition(Vec3f(0, -85, wifiList->getPosition().z));
}

void WifiScreen::loadGameObjectState(GameObjectState state)
{
	printf("WifiScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// Custom update
/// </summary>
void WifiScreen::update(float dt)
{
	if( connectingTimer > 0 )
	{
		connectingTimer -= dt;
		if( connectingTimer <= 0 )
		{
			String passwordEntered = keyboard->getText();
			networkError = false;
			ConnectionManager::getInstance()->connectToWifiNetwork( currentNetworkName, passwordEntered );
		}
	}

	super::update(dt);
}

/// <summary>
/// Network error occured
/// </summary>
void WifiScreen::handleNetworkError(String networkName, String errorMessage)
{
	parentScene->lockOutInput(0);
	if( keyboardPresent )
		return;

	if(!currentNetworkName.equalsIgnoreCase(networkName))
		return;

	if( transmitting->isVisibleSelf() )
	{
		attemptingConnect = false;
		parentScene->playSound("Base_WiFiDisconnected");
		networkError = true;
		transmitting->setVisible(false);
		connectedStrength1->setVisible(true);

		errorPrompt->recursiveFadeIn(0.25f );
		TextLabel* errorLabel = errorPrompt->findLabel("errorLabel");
		errorLabel->setText(errorMessage);
		connectedRoot->setVisible(false);
		connectingPrompt->recursiveFadeOut(0.25f );
		entryPrompt->recursiveFadeOut(0.25f);

		scanButton->bubbleIn();
		GameObject* selection = selectedWifiButton();
		if(selection)
			parentScene->setCurrentHighlight(selection);
		else
			parentScene->setCurrentHighlight(scanButton);

		parentScene->setupHotSpots();
	}
}


/// <summary>
/// Network connection made
/// </summary>
void WifiScreen::handleNetworkConnected( String networkName )
{
	parentScene->lockOutInput(0);

	if( keyboardPresent )
		return;

	printf("WifiScreen::handleNetworkConnected\n");
	if( transmitting->isVisibleSelf() )
	{
		attemptingConnect = false;
		parentScene->playSound("Base_TransformSuccess");
		wifiList->scaleTo(Vec3f::Zero, 0.5f);
		scanButton->scaleTo(Vec3f::Zero, 0.5f);
		connectingRoot->waitFor(0.5f);
		connectingRoot->moveTo(Vec3f(-171, 40, 120), 0.5f);
		transmitting->setVisible(false);
		connectedStrength4->setVisible(true);
		entryPrompt->recursiveFadeOut();
		connectingPrompt->recursiveFadeOut();

		if( ConnectionManager::getInstance()->hasEthernet())
		{ connectedPrompt->findLabel("macAddress")->setText(SystemSettings::getEthernetMacAddress()); }
		else
		{ connectedPrompt->findLabel("macAddress")->setText(SystemSettings::getWifiMacAddress()); }
		connectedRoot->waitFor(1);
		connectedRoot->makeVisible(true);
//		disconnectButton->setVisible(false);
//		disconnectButton->bubbleIn(1);
		connectedPrompt->recursiveFadeIn(1);
		parentScene->showBackButton(0.1f,true);
		connected = true;
		parentScene->setupHotSpots();
	}
}


/// <summary>
/// Setup display for scan
/// </summary>
void WifiScreen::setDisplayForScan()
{
	if( firstScan )
	{
		entryPrompt->setVisible(false);
		connectingRoot->setVisible(false);
		connectedRoot->setVisible(false);
		connectedStrength1->setVisible(false);
		connectedStrength2->setVisible(false);
		connectedStrength3->setVisible(false);
		connectedStrength4->setVisible(false);
		transmitting->setVisible(false);
		connectingPrompt->setVisible(false);
		errorPrompt->setVisible(false);
		ethernetPrompt->setVisible(false);
		noNetworksPrompt->setVisible(false);
		scanButton->setVisible(false);
		wifiBackButton->setVisible(false);
		connectedPrompt->setVisible(false);
		scanningPrompt->setVisible(true);

		parentScene->lockOutInput(2);
		TimerManager::getInstance()->addTimer("ScanRequest", 2, 0 );
	}
	else
	{
		scanButton->cancelActions();

		scanButton->scaleTo(Vec3f::Zero, 0.25f);
		connectingRoot->setVisible(false);

		entryPrompt->recursiveFadeOut();
		noNetworksPrompt->recursiveFadeOut();
		connectingPrompt->recursiveFadeOut();
		errorPrompt->recursiveFadeOut();

		scanningPrompt->recursiveFadeIn(0.5f);
		parentScene->lockOutInput(2);
		TimerManager::getInstance()->addTimer("ScanRequest", 2, 0 );
	}

	if( ConnectionManager::getInstance()->hasEthernet())
	{
		connectedPrompt->findLabel("macAddress")->setText(SystemSettings::getEthernetMacAddress());
		macAddress->setText(SystemSettings::getEthernetMacAddress());
	}
	else
	{
		connectedPrompt->findLabel("macAddress")->setText(SystemSettings::getWifiMacAddress());
		macAddress->setText(SystemSettings::getWifiMacAddress());
	}
}

/// <summary>
/// Initiates a scan request
/// </summary>
void WifiScreen::doScan()
{
	printf("WifiScreen::doScan()\n");
	ConnectionManager::getInstance()->scanForWifiNetworks();

	onScanFinished();
}

/// <summary>
/// Returns selected wifibutton from slider
/// </summary>
WifiButton* WifiScreen::selectedWifiButton()
{ return (WifiButton*)wifiSlider->getSelectedItem(); }


/// <summary>
/// When scan completes
/// </summary>
void WifiScreen::onScanFinished()
{
	firstScan = false;

	wifiList->moveTo( Vec3f(-225, -85, wifiList->getPosition().z), 0.5f );

	TRelease<ObjectArray> networks( ConnectionManager::getInstance()->getAllNetworks() );

	//Found networks
	if( networks->getSize() > 0 )
	{
		for ( int i = 0; i<networks->getSize(); i++ )
		{
			NetworkInfo* networkConnection = (NetworkInfo*)networks->elementAt(i);
			if(!networkConnection->isValidWifi())
				continue;

			TRelease<WifiButton> wifiButton( (WifiButton*)Scene::instantiate("WifiButton") );
			wifiButton->name = networkConnection->networkName;
			wifiSlider->addItem( wifiButton );
			wifiButton->setNetworkName( networkConnection->networkName );

			// 0 -> 25
			if( networkConnection->strength > 0 && networkConnection->strength < 26 )
			{ wifiButton->setWifiStrength(  WifiStrengthWeakest ); }
			// 26-50
			else if ( networkConnection->strength < 51 )
			{ wifiButton->setWifiStrength(  WifiStrengthWeak ); }
			//51 - 75
			else if ( networkConnection->strength < 76 )
			{ wifiButton->setWifiStrength(  WifiStrengthStrong ); }
			// 76-> 100
			else
			{ wifiButton->setWifiStrength(  WifiStrengthStrongest ); }

			wifiButton->setPosition( Vec3f::Zero );
		}

		wifiSlider->positionFirstElements();
		scanningPrompt->recursiveFadeOut();
		entryPrompt->recursiveFadeIn(0.5f);

		parentScene->setCurrentHighlight(selectedWifiButton());
		scanButton->bubbleIn(0.5f);
	}
	else
	{
		noNetworksPrompt->recursiveFadeIn(0.5f);
		scanningPrompt->recursiveFadeOut();
		entryPrompt->recursiveFadeIn(0.5f );
		scanButton->bubbleIn(0.5f);
		parentScene->setCurrentHighlight(scanButton);
	}

	parentScene->setupHotSpots();
}


/// <summary>
/// Tries to attempt a wifi connection
/// </summary>
bool WifiScreen::collectPassword(WifiButton* wifiButton)
{
	currentNetworkName = wifiButton->name;

	if( ConnectionManager::getInstance()->isServiceOnline(currentNetworkName) )
	{
		wifiList->scaleTo(Vec3f::Zero, 0.5f);
		scanButton->scaleTo(Vec3f::Zero, 0.5f);
		connectingRoot->waitFor(0.5f);
		connectingRoot->makeVisible(true);
		connectingRoot->moveBy(Vec3f::Left*350 + Vec3f::Down*25, 0.5f);
		transmitting->setVisible(false);
		connectedStrength4->setVisible(true);
		wifiBackButton->setVisible(false);

		entryPrompt->recursiveFadeOut();
		errorPrompt->recursiveFadeOut();
		connectingPrompt->recursiveFadeOut();

		if( ConnectionManager::getInstance()->hasEthernet())
		{ connectedPrompt->findLabel("macAddress")->setText(SystemSettings::getEthernetMacAddress()); }
		else
		{ connectedPrompt->findLabel("macAddress")->setText(SystemSettings::getWifiMacAddress()); }

		connectedRoot->waitFor(1);
		connectedRoot->makeVisible(true);
//		disconnectButton->setVisible(false);
//		disconnectButton->bubbleIn(1);
		connectedPrompt->recursiveFadeIn(1);
		parentScene->showBackButton(0.1f,true);
		connected = true;

		lastNetworkName = currentNetworkName;
		return false;
	}
	else if(ConnectionManager::getInstance()->isNetworkSecure(currentNetworkName) == false)
	{
		attemptConnection(currentNetworkName);
		lastNetworkName = currentNetworkName;
		return false;
	}
	else
	{
		parentScene->hideBackButton();
		keyboardPresent = true;
		connectButton->setVisible(false);
		passwordRoot->setPosition( Vec3f::Right*1280 );
		passwordRoot->setVisible(true);
		passwordRoot->moveBy( Vec3f::Left*1280, 1 );
		wifiBackButton->bubbleIn(1);

		keyboard->setVisible(true);

		//For password re-entry lets not reset it completely
		//so users can see what was typed and change accordingly???
		if( lastNetworkName != currentNetworkName )
		{
			keyboard->reset();
			keyboard->setText("");
			passwordField->setText("");
		}

		connectingRoot->moveBy( Vec3f::Left*1280, 1 );
		errorPrompt->moveBy( Vec3f::Left*1280, 1 );
		connectingPrompt->moveBy( Vec3f::Left*1280, 1
				);

		wifiList->moveBy( Vec3f::Left*1280, 1 );
		noNetworksPrompt->moveBy( Vec3f::Left*1280, 1 );
		scanningPrompt->moveBy( Vec3f::Left*1280, 1 );
		ethernetPrompt->moveBy( Vec3f::Left*1280, 1 );
		entryPrompt->moveBy( Vec3f::Left*1280, 1 );
		scanButton->moveBy( Vec3f::Left*1280, 1 );

		TextLabel* networkNameLabel = passwordRoot->findLabel("enterPasswordText")->findLabel("networkName");
		networkNameLabel->setText(wifiButton->name);
		lastNetworkName = currentNetworkName;
		return true;
	}
}

///<summary>
///animation helper
///</summary>
void WifiScreen::cancelCollectPassword()
{
	printf("WifiScreen::cancelCollectPassword\n");
	if(keyboardPresent)
	{
		parentScene->lockOutInput(1);

		keyboardPresent = false;
		passwordRoot->moveBy( Vec3f::Right*1280, 1 );
		passwordRoot->makeVisible(false);

		keyboard->setText("");
		passwordField->setText("");

		wifiBackButton->setState(ButtonStateNormal);
		wifiBackButton->bubbleOut(0);

		parentScene->showBackButton(0.1f);

		connectingRoot->moveBy( Vec3f::Right*1280, 1 );
		errorPrompt->moveBy( Vec3f::Right*1280, 1 );
		connectingPrompt->moveBy( Vec3f::Right*1280, 1 );

		wifiList->moveBy( Vec3f::Right*1280, 1 );
		noNetworksPrompt->moveBy( Vec3f::Right*1280, 1 );
		scanningPrompt->moveBy( Vec3f::Right*1280, 1 );
		ethernetPrompt->moveBy( Vec3f::Right*1280, 1 );
		entryPrompt->moveBy( Vec3f::Right*1280, 1 );
		scanButton->moveBy( Vec3f::Right*1280, 1 );

		GameObject* selection = selectedWifiButton();
		if(selection)
			parentScene->setCurrentHighlight(selection);
		else
			parentScene->setCurrentHighlight(scanButton);
	}

}

void WifiScreen::disconnect()
{
	parentScene->lockOutInput(2.0f);
	ConnectionManager::getInstance()->disconnectFromWifiNetwork(currentNetworkName);

	connectedRoot->waitFor(0.5f);
	connectedRoot->makeVisible(false);
	disconnectButton->bubbleOut();
	connectedPrompt->recursiveFadeOut();
	connectingRoot->moveBy(Vec3f::Left*1280, 0.25f);
	connectingRoot->makeVisible(false);
	connectingRoot->setPosition( Vec3f( 142, 44, 120 ) );

	transmitting->waitFor(0.5f);
	transmitting->setVisible(true);
	wifiList->setPosition(Vec3f(0, -85, wifiList->getPosition().z));
	wifiList->bubbleIn(0.5f);
	wifiSlider->prepareForScan();
	connected = false;
	setDisplayForScan();
}

/// <summary>
/// Tries to attempt a wifi connection
/// </summary>
void WifiScreen::attemptConnection(String networkName)
{
	attemptingConnect = true;
	if( keyboardPresent )
	{
		connectButton->setState(ButtonStateNormal);
		keyboardPresent = false;
		passwordRoot->moveBy( Vec3f::Right*1280, 1 );
		wifiList->moveBy( Vec3f::Right*1280, 1 );
		noNetworksPrompt->moveBy( Vec3f::Right*1280, 1 );
		scanningPrompt->moveBy( Vec3f::Right*1280, 1 );
		ethernetPrompt->moveBy( Vec3f::Right*1280, 1 );
		entryPrompt->moveBy( Vec3f::Right*1280, 1 );

		wifiBackButton->bubbleOut(0);

		errorPrompt->moveBy( Vec3f::Right*1280, 1 );
		errorPrompt->setVisible(false);

		scanButton->moveBy( Vec3f::Right*1280, 1 );
	}

	connectingPrompt->setVisible(true);
	connectingPrompt->findLabel("labelNetworkName")->setText(networkName);

	connectingPrompt->recursiveFadeIn(0.5f);

	connectingRoot->setPosition( Vec3f( 142, 44, 120 ) );
	connectingPrompt->setPosition( Vec3f( 245, 88, -260 ) );
	connectingRoot->setScale( Vec3f::Zero );
	connectingRoot->waitFor(0.5f);
	connectingRoot->makeVisible(true);
	connectingRoot->scaleTo(connectingRoot->originalScale, 0.5f );
	transmitting->setVisible(true);
	transmitting->playAnimation("TransmittingParent");

	entryPrompt->setVisible(false);
	scanButton->setVisible(false);

	parentScene->showBackButton(0.1f,false);
	parentScene->setupHotSpots();

	connectingTimer = 1.5f;

}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool WifiScreen::handlePressAction(GameObject*& objectHighlight)
{
	if ( objectHighlight != null && !attemptingConnect)
	{
		if( objectHighlight == scanButton)
		{
			parentScene->lockOutInput(1);
			((ButtonObject*)objectHighlight)->setState(ButtonStateNormal);
			((ButtonObject*)objectHighlight)->scaleTo( Vec3f::Zero);

			wifiSlider->prepareForScan();
			setDisplayForScan();
			objectHighlight = null;
			return true;
		}
		else if ( objectHighlight == connectButton)
		{
			attemptConnection(currentNetworkName);
			parentScene->lockOutInput(Constants::Time::wifiInputTimer);
			return true;
		}
		else if(objectHighlight == disconnectButton)
		{
			disconnect();
		}
		else if ( objectHighlight->typeOf(WifiButton::type()) )
		{
			((WifiButton*)objectHighlight)->setState(ButtonStateNormal);

			if( collectPassword((WifiButton*)objectHighlight) )
			{objectHighlight = keyboard->getDefaultHighLight(); }
			parentScene->lockOutInput(1);
			return true;
		}
		else if (objectHighlight == wifiBackButton)
		{
			parentScene->lockOutInput(1);
			cancelCollectPassword();
		}
		else if ( keyboard->onButtonPressAction() )
		{
			parentScene->lockOutInput(0.2f);
			String txt = keyboard->getText();
			if(txt.length() > Constants::UI::wifiPasswordDisplayLimit)
				txt = txt.substring(txt.length()-Constants::UI::wifiPasswordDisplayLimit,txt.length());

			if(txt.length() == 0)
				connectButton->setVisible(false);
			else if(!connectButton->isVisibleSelf())
				connectButton->bubbleIn();
			parentScene->setupHotSpots();

			if (objectHighlight->name == "Backspace")
			{
				// Fade last character out takes 0.2f so we block for that long
				passwordField->removeLastCharacter();
				passwordField->changeTo(txt);
				parentScene->playSound("Parent_Keystroke_Backspace");
			}
			else
			{
				parentScene->playSound("Parent_Keystroke");
				passwordField->setText(txt);
				passwordField->fadeLastCharacterIn();
				objectHighlight = keyboard->getDefaultHighLight();
			}
		}
	}

	return false;
}

/// <summary>
/// If we want to handle the press back ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool WifiScreen::handlePressBack(GameObject*& objectHighlight)
{
	if(keyboardPresent && wifiBackButton->isVisibleHierarchy())
	{
		parentScene->lockOutInput(1);
		cancelCollectPassword();
		return true;
	}

	return false;
}



/// <summary>
/// If we want to handle analog changes
/// </summary>
bool WifiScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{

	if( !connected )
	{
		if ( keyboardPresent && keyboard->onSelectionChange(objectLeft, objectEntered) )
		{
			if( objectEntered != connectButton )
			{
				parentScene->playSound("Base_Highlight");
				currentHighlight = keyboard->getDefaultHighLight();
			}

			return true;
		}
		if ( !keyboardPresent && wifiSlider->onAnalogChange(objectLeft, objectEntered ) )
		{
			parentScene->playSound("Base_MenuScroll");
			currentHighlight = wifiSlider->getSelectedItem();
			return true;
		}
	}

	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void WifiScreen::cameraArrived()
{
	printf("WifiScreen::cameraArrived()\n");
	parentScene->showBackButton(0.75f, false);
	parentScene->showHelpButton(0.75f);

	wifiSlider->prepareForScan();
	setDisplayForScan();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* WifiScreen::getArrivalSelection()
{
	if( networkError )
	{ return wifiSlider->getSelectedItem(); }

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void WifiScreen::addHotSpots( ObjectArray* trackingList )
{
	if( !connected )
	{
		wifiSlider->addObjectsToTracking(trackingList);

		if(!keyboardPresent)
		{
			if(scanButton->isVisibleHierarchy())
				trackingList->addElement(scanButton);
		}
		else
		{
			if(connectButton->isVisibleHierarchy())
				trackingList->addElement(connectButton);
			if(wifiBackButton->isVisibleHierarchy())
				trackingList->addElement(wifiBackButton);
		}

	}
	else
	{
//		trackingList->addElement(disconnectButton);
	}

	if( keyboardPresent )
	{ keyboard->setupHotSpots(); }
}



}


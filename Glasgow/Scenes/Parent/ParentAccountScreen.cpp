#include "ParentAccountScreen.h"
#include "ParentScene.h"
namespace Glasgow
{

ParentAccountScreen::ParentAccountScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	//fill references
	hasAccountRoot		= find("hasAccountRoot");
	noAccountRoot		= find("noAccountRoot");

	emailLabel			= hasAccountRoot->findLabel("emailLabel");
	errorPrompt			= hasAccountRoot->findLabel("errorPrompt");

	registerButton		= noAccountRoot->findButton("registerButton");
	registerAgainButton = hasAccountRoot->findButton("registerButton");

	setupInitialState();
}

ParentAccountScreen::~ParentAccountScreen()
{
	printf("ParentAccountScreen::~ParentAccountScreen\n");
}

void ParentAccountScreen::setupInitialState()
{
	//check if the device is registered
	errorPrompt->setVisible(false);
	emailLabel->setVisible(false);
	hasAccountRoot->setVisible(false);
	noAccountRoot->setVisible(false);
	registerAgainButton->setVisible(false);
	hasAccountRoot->findLabel("text2")->setVisible(false);
}

void ParentAccountScreen::loadGameObjectState(GameObjectState state)
{
	printf("ParentAccountScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ParentAccountScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(objectHighlight != null)
	{

		if(objectHighlight == registerButton || objectHighlight == registerAgainButton)
		{
			if(ConnectionManager::getInstance()->isOnline())
			{
				parentScene->pushScreen(ParentScene::PS_REGISTER_SCREEN);
				return true;
			}
			else
			{
				DialogOptions options;
				options.title 	= "Register LeapTV";
				options.prompt  = "Registering requires an internet connection.  Go to Settings, then Network to set up an internet connection.";
				options.buttonA = DialogButtonOption("OK", "Ok");
				parentScene->showModalDialog(DialogTypeInternetConnectionRequired, options);
				return true;
			}
		}
	}

	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ParentAccountScreen::cameraArrived()
{

	parentScene->showHelpButton(1,false);

	//check if the device is registered
	bool registered = SystemSettings::getIsActivated();
	printf("ParentAccountScreen::device registered: %s\n", registered ? "true" : "false");

	if(registered)
	{
		hasAccountRoot->setVisible(true);
		noAccountRoot->setVisible(false);
		emailLabel->setVisible(true);
		String emailStr = SystemSettings::getRegistrationEmail();
		if(emailStr.isEmpty())
		{
			emailLabel->setVisible(false);
			errorPrompt->recursiveFadeIn();
			hasAccountRoot->findLabel("text1")->setVisible(false);
		}
		else
		{
			emailLabel->setText(emailStr);
		}
		parentScene->showBackButton(0.75f,true);
	}
	else
	{
		hasAccountRoot->setVisible(false);
		noAccountRoot->setVisible(true);
		parentScene->showBackButton(0.75f,false);
	}
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void ParentAccountScreen::cameraLeaving()
{
	super::cameraLeaving();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ParentAccountScreen::getArrivalSelection()
{
	if(registerButton->isVisibleHierarchy())
	{
		return registerButton;

	}
	else if(registerAgainButton->isVisibleHierarchy())
	{
		return registerAgainButton;
	}

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ParentAccountScreen::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(registerButton);
	trackingList->addElement(registerAgainButton);
}

} /* namespace Glasgow */

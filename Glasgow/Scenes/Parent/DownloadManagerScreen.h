#ifndef DOWNLOADMANAGERSCREEN_H_
#define DOWNLOADMANAGERSCREEN_H_

#include "ParentScreen.h"

namespace Glasgow
{

class DownloadManagerScreen : public ParentScreen
{
	CLASSEXTENDS(DownloadManagerScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();



	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	bool handlePressAction(GameObject*& objectHighlight);


	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );
	DownloadManagerScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~DownloadManagerScreen();
	/// <summary>
	///
	/// </summary>
	void handlePackageDownloadProgressed( PackageInfo* info, int percent);
	/// <summary>
	/// Handle package changed
	/// </summary>
	void handlePackageChanged(tPackageStatus newStatus, PackageInfo* info );

private:
	/// <summary>
	/// Sets the pause/resume button visibility
	/// </summary>
	void setButtonDisplay(bool paused);
	void populateDownloadUI();
	void populateDownloadArray();

	TextLabel* headerLabel;
	TextLabel* noDownloadsLabel;
	ObjectArray downloadItems;
	ButtonObject* pauseAllButton;
	ButtonObject* resumeAllButton;
};

}
#endif

#include "EditProfilesScreen.h"
#include "ParentScene.h"
#include "SIEngine/Include/SIScenes.h"
namespace Glasgow
{

EditProfilesScreen::EditProfilesScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	currentStep				= CreationStepName;
	currentUser 			= null;
	birthDayRoot			= find("birthDayRoot");
	birthMonthRoot			= find("birthMonthRoot");
	birthYearRoot			= find("birthYearRoot");
	genderRoot				= find("genderRoot");
	gradeLevelRoot			= find("gradeLevelRoot");
	nameRoot				= find("nameRoot");
	confirmRoot				= find("confirmRoot");

	headerPrompt			= findLabel("header");
	nameErrorPrompt			= nameRoot->findLabel("errorPrompt");
	nameEnterPrompt			= nameRoot->findLabel("namePrompt");

	yearNumPad				= (GateLockObject*)birthYearRoot->find("numPad");
	keyboard 				= new KeyboardObject(scene, false);
	nameDoneButton			= nameRoot->findButton("done");
	keyboard->setMaxCharacters(Constants::System::userNameMaximumCharLimit);
	keyboard->setPosition( Vec3f::Down*25 + Vec3f::Forward*900);
	keyboard->setLeadingCaps(true);
	yearField				= birthYearRoot->find("birthField")->findLabel("codeText");
	nameFieldContainer		= nameRoot->find("nameField");
	nameField				= nameFieldContainer->findLabel("codeText");
	nameRoot->addChild(keyboard);
	keyboard->setCullingLayerRecursive(0);
	changing = false;
	isExiting = false;
	nameField->setBatched(false);
	yearField->setBatched(false);

	calender = new Calender();
	//scene->printSceneGraph(this,0);
	chosenDay = "01";


	setupInitialState();
}

EditProfilesScreen::~EditProfilesScreen()
{
	printf("EditProfilesScreen::~EditProfilesScreen()\n");
	calender->release();
	keyboard->release();
}

void EditProfilesScreen::setupInitialState()
{
	birthDayRoot->setVisible(false);
	birthMonthRoot->setVisible(false);
	birthYearRoot->setVisible(false);
	genderRoot->setVisible(false);
	gradeLevelRoot->setVisible(false);
	nameRoot->setVisible(false);
	confirmRoot->setVisible(false);
	nameErrorPrompt->setVisible(false);
	nameDoneButton->setVisible(false);

	nameRoot->setVisible(true);
	nameField->setText("");
	nameField->setVisible(true);
	keyboard->reset();
	keyboard->setLeadingCaps(true);
	keyboard->setText("");

	exitToChildTimer = 0;
	exitTimer = 0;
	currentUser = null;

	currentStep = CreationStepName;
	changing = false;

	setLocalizedGradeTextInChildren(gradeLevelRoot);
}


void EditProfilesScreen::loadGameObjectState(GameObjectState state)
{
	printf("EditProfilesScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// Animates to the next step and returns the new default selection
/// </summary>
GameObject* EditProfilesScreen::gotoNextSubStep()
{
	float lockOutTime = 1.35f;
	float waitTime = 0.75f;
	if( currentStep == CreationStepConfirm )
	{
		printf("Confirm -> Name\n");
		parentScene->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate In name
		//**********************************************************************

		confirmRoot->findButton("change")->bubbleOut();
		confirmRoot->findButton("confirm")->bubbleOut();

		confirmRoot->findLabel("confirmPrompt")->recursiveFadeOut();
		confirmRoot->findLabel("nameLabel")->recursiveFadeOut();
		confirmRoot->findLabel("genderLabel")->recursiveFadeOut();
		confirmRoot->findLabel("gradeLabel")->recursiveFadeOut();
		confirmRoot->findLabel("birthMonthLabel")->recursiveFadeOut();
		confirmRoot->findLabel("birthDayLabel")->recursiveFadeOut();
		confirmRoot->findLabel("birthYearLabel")->recursiveFadeOut();
		confirmRoot->findLabel("label1")->recursiveFadeOut();
		confirmRoot->findLabel("label2")->recursiveFadeOut();
		confirmRoot->findLabel("label3")->recursiveFadeOut();
		confirmRoot->findLabel("label4")->recursiveFadeOut();
		confirmRoot->findLabel("label5")->recursiveFadeOut();
		confirmRoot->findLabel("label6")->recursiveFadeOut();

		//**********************************************************************
		// Animate In name
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_FIRSTNAME"));
		headerPrompt->recursiveFadeIn();

		nameRoot->waitFor(waitTime);
		nameRoot->makeVisible(true);
		nameEnterPrompt->recursiveFadeIn(waitTime);
		keyboard->bubbleIn(waitTime);
		nameFieldContainer->bubbleIn(waitTime);

		String usernameText = keyboard->getText();
		if(validNameEntry(usernameText) == false)
		{ nameDoneButton->setVisible(false);}
		else
		{ nameDoneButton->bubbleIn(waitTime);}

		currentStep = CreationStepName;

		if( !changing )
		{
			nameField->setText("");
			keyboard->reset();
			keyboard->setLeadingCaps(true);
			return keyboard->getDefaultHighLight();
		}
		else
		{
			return nameRoot->findButton("done");
		}
	}
	else if ( currentStep == CreationStepName )
	{
		printf("Name -> Gender\n");
		parentScene->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Name
		//**********************************************************************
		keyboard->bubbleOut();
		nameFieldContainer->bubbleOut();
		nameDoneButton->bubbleOut();
		nameErrorPrompt->cancelActions();
		nameErrorPrompt->recursiveFadeOut();
		nameEnterPrompt->recursiveFadeOut();

		//**********************************************************************
		// Animate in Gender
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_GENDER"));
		headerPrompt->recursiveFadeIn();

		genderRoot->waitFor(waitTime);
		genderRoot->makeVisible(true);
		//Todo: localization
		genderRoot->findLabel("genderPrompt")->setText( String("Choose Boy or Girl for ") + chosenName );
		genderRoot->findLabel("genderPrompt")->recursiveFadeIn(waitTime);

		for ( int i = 0; i<genderRoot->numChildren(); i++ )
		{
			GameObject* child = genderRoot->childAtIndex(i);
			if( child != null && child->typeOf( ButtonObject::type()) )
			{	child->bubbleIn(waitTime);}
		}

		currentStep = CreationStepGender;

		if( !changing )
		{ return genderRoot->find("boy"); }
		else
		{ return genderRoot->find(chosenGender); }
	}
	else if ( currentStep == CreationStepGender )
	{
		printf("Gender -> Grade\n");
		parentScene->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out gender
		//**********************************************************************
		genderRoot->findLabel("genderPrompt")->recursiveFadeOut();
		for ( int i = 0; i<genderRoot->numChildren(); i++ )
		{
			GameObject* child = genderRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				child->cancelActions();
				if(child->name != chosenGender)
				{ child->bubbleOut(); }
			}
		}

		//**********************************************************************
		// Animate in GradeLevel
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_GRADE"));
		headerPrompt->recursiveFadeIn();

		gradeLevelRoot->waitFor(waitTime);
		gradeLevelRoot->makeVisible(true);
		gradeLevelRoot->findLabel("gradeLevelPrompt")->recursiveFadeIn(waitTime);
		//Todo:Localization
		gradeLevelRoot->findLabel("gradeLevelPrompt")->setText( String("Select current grade/level for ") + chosenName );

		for ( int i = 0; i<gradeLevelRoot->numChildren(); i++ )
		{
			GameObject* child = gradeLevelRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{ child->bubbleIn(waitTime); }
		}

		currentStep = CreationStepGrade;

		if( !changing )
		{ return gradeLevelRoot->find("gradePreschool"); }
		else
		{ return gradeLevelRoot->find(chosenGrade); }
	}
	else if (currentStep == CreationStepGrade)
	{
		printf("Grade -> Month\n");
		parentScene->lockOutInput(lockOutTime);
		//**********************************************************************
		// Animate Out Grade
		//**********************************************************************
		gradeLevelRoot->findLabel("gradeLevelPrompt")->recursiveFadeOut();

		for ( int i = 0; i<gradeLevelRoot->numChildren(); i++ )
		{
			GameObject* child = gradeLevelRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				child->cancelActions();
				if(child->name != chosenGrade)
				{ child->bubbleOut(); }
			}
		}

		//**********************************************************************
		// Animate in Month
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_BIRTHMONTH"));
		headerPrompt->recursiveFadeIn();

		birthMonthRoot->waitFor(waitTime);
		birthMonthRoot->makeVisible(true);
		birthMonthRoot->findLabel("birthMonthPrompt")->recursiveFadeIn(waitTime);
		//Todo:Localization
		birthMonthRoot->findLabel("birthMonthPrompt")->setText( String("Select birth month for ") + chosenName );

		for ( int i = 0; i<birthMonthRoot->numChildren(); i++ )
		{
			GameObject* child = birthMonthRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{ child->bubbleIn(waitTime); }

		}

		currentStep = CreationStepMonth;

		if( !changing )
		{ return birthMonthRoot->find("january");}
		else
		{ return birthMonthRoot->find(chosenMonth); }
	}
	else if ( currentStep == CreationStepMonth )
	{
		printf("Month -> Year\n");
		parentScene->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Month
		//**********************************************************************
		birthMonthRoot->findLabel("birthMonthPrompt")->recursiveFadeOut();

		for ( int i = 0; i<birthMonthRoot->numChildren(); i++ )
		{
			GameObject* child = birthMonthRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				child->cancelActions();
				if(child->name != chosenMonth)
				{ child->bubbleOut(); }
			}
		}

		//**********************************************************************
		// Animate in Year
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_BIRTHYEAR"));
		headerPrompt->recursiveFadeIn();

		yearField->setText("");
		chosenYear = "";
		birthYearRoot->waitFor(waitTime);
		birthYearRoot->makeVisible(true);
		birthYearRoot->findLabel("birthYearPrompt")->recursiveFadeIn(waitTime);
		//Todo:Localization
		birthYearRoot->findLabel("birthYearPrompt")->setText( String("Enter birth year for ") + chosenName );

		yearNumPad->bubbleIn(waitTime);
		birthYearRoot->find("birthField")->bubbleIn(waitTime);

		currentStep = CreationStepYear;

		return yearNumPad->find("1");
	}
	else if ( currentStep == CreationStepYear )
	{
		printf("Year -> Day\n");
		parentScene->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Year
		//**********************************************************************
		birthYearRoot->findLabel("birthYearPrompt")->recursiveFadeOut();
		yearNumPad->bubbleOut();
		birthYearRoot->find("birthField")->bubbleOut();

		//**********************************************************************
		// Animate in Day
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_BIRTHDAY"));
		headerPrompt->recursiveFadeIn();

		birthDayRoot->waitFor(waitTime);
		birthDayRoot->makeVisible(true);
		birthDayRoot->findLabel("birthDayPrompt")->recursiveFadeIn(waitTime);
		//Todo:localization
		birthDayRoot->findLabel("birthDayPrompt")->setText( String("Select birth day for ") + chosenName );

		int daysInMonth = MIN(birthDayRoot->getChildren()->getSize(),calender->daysInMonthYear(chosenMonth,chosenYear));
		for( int i = 0; i< 31; i++ )
		{
			ButtonObject* button = birthDayRoot->findButton(calender->dayStrForInt(i+1));
			if(i < daysInMonth)
			{ button->bubbleIn(waitTime);}
			else
			{ button->setVisible(false); }
		}

		currentStep = CreationStepDay;
		if( !changing )
		{ return birthDayRoot->find("01");}
		else
		{
			GameObject* go = birthDayRoot->find(chosenDay);
			if(go->isVisibleHierarchy())
			{return go;}
			return birthDayRoot->find("01");
		}
	}
	else if ( currentStep == CreationStepDay )
	{
		printf("Day -> Confirm\n");
		parentScene->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Day
		//**********************************************************************
		 birthDayRoot->findLabel("birthDayPrompt")->recursiveFadeOut();

		for ( int i = 0; i<birthDayRoot->numChildren(); i++ )
		{
			GameObject* child = birthDayRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				child->cancelActions();
				if(child->name != chosenDay)
				{ child->bubbleOut(); }
			}
		}

		//**********************************************************************
		// Animate in Confirm
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("EDITPROFILESCREEN_HEADER"));
		headerPrompt->recursiveFadeIn();

		confirmRoot->waitFor(waitTime);
		confirmRoot->makeVisible(true);
		confirmRoot->findLabel("nameLabel")->setText(chosenName);
		String genderStr = chosenGender == "boy" ? StringsManager::getInstance()->getLocalizedString("PROFILE_GENDER_MALE") : StringsManager::getInstance()->getLocalizedString("PROFILE_GENDER_FEMALE");
		confirmRoot->findLabel("genderLabel")->setText(genderStr);
		confirmRoot->findLabel("gradeLabel")->setText(getLocalizedStringFromGradeButtonName(chosenGrade));
		confirmRoot->findLabel("birthMonthLabel")->setText(calender->formattedMonthStr(chosenMonth));
		confirmRoot->findLabel("birthDayLabel")->setText(calender->formattedDayStr(chosenDay));
		confirmRoot->findLabel("birthYearLabel")->setText(chosenYear);

		confirmRoot->findLabel("nameLabel")->recursiveFadeIn(waitTime);
		confirmRoot->findLabel("genderLabel")->recursiveFadeIn(waitTime + 0.05f);
		confirmRoot->findLabel("gradeLabel")->recursiveFadeIn(waitTime + 0.1f);
		confirmRoot->findLabel("birthMonthLabel")->recursiveFadeIn(waitTime + 0.15f);
		confirmRoot->findLabel("birthDayLabel")->recursiveFadeIn(waitTime + 0.2f);
		confirmRoot->findLabel("birthYearLabel")->recursiveFadeIn(waitTime + 0.25f);

		confirmRoot->findLabel("confirmPrompt")->recursiveFadeIn(waitTime);

		confirmRoot->findLabel("label1")->recursiveFadeIn(waitTime);
		confirmRoot->findLabel("label2")->recursiveFadeIn(waitTime + 0.05f);
		confirmRoot->findLabel("label3")->recursiveFadeIn(waitTime + 0.1f);
		confirmRoot->findLabel("label4")->recursiveFadeIn(waitTime + 0.15f);
		confirmRoot->findLabel("label5")->recursiveFadeIn(waitTime + 0.2f);
		confirmRoot->findLabel("label6")->recursiveFadeIn(waitTime + 0.25f);

		confirmRoot->findButton("confirm")->bubbleIn(waitTime + 0.25f);
		confirmRoot->findButton("change")->bubbleIn(waitTime + 0.25f);

		currentStep = CreationStepConfirm;

		return confirmRoot->findButton("confirm");
	}
	return null;
}

/// <summary>
/// Takes the new user information and adds the Glasgow based profile
/// </summary>
void EditProfilesScreen::addNewUser()
{
	User* newUser = UsersManager::getInstance()->newUser(chosenName);
	String gender = chosenGender == "boy" ? Constants::System::genderMaleKey : Constants::System::genderFemaleKey;
	newUser->setGender(gender);
	newUser->setBirthDate(chosenDay,chosenMonth,chosenYear);
	newUser->setBirthDay(calender->getDayIntRepresentation(chosenDay));
	newUser->setBirthMonth(calender->getMonthIntRepresentation(chosenMonth));
	newUser->setBirthYear(chosenYear.toInt());
	GradeInfo* info = GradeInfo::getGradeInfo(chosenGrade);
	newUser->setGrade(info->leapGradeEnum);
	newUser->save();

	// The newly created user should be set as the last user so that it's selected when they enter the child scene
	printf("Setting last logged in user : %s\n", newUser->getFirstName().str());
	UsersManager::getInstance()->setCurrentUser(newUser);
	currentUser = newUser;
}

/// <summary>
/// returns the localized string for a given grad button name
/// </summary>
String EditProfilesScreen::getLocalizedStringFromGradeButtonName(String buttonName)
{
	GradeInfo* gradeInfo = GradeInfo::getGradeInfo(buttonName);
	return StringsManager::getInstance()->getLocalizedString(gradeInfo->localizationKey);
}

void EditProfilesScreen::setLocalizedGradeTextInChildren(GameObject* root)
{
	for( int i = 0; i<root->numChildren(); i++ )
	{
		GameObject* child = root->childAtIndex(i);

		if(child->typeOf(ButtonObject::type()))
		{
			TextLabel *label = child->findLabel("text");
			label->setText(getLocalizedStringFromGradeButtonName(child->name));
		}
	}
}

/// <summary>
/// saves user settings
/// </summary>
void EditProfilesScreen::saveSettings()
{
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool EditProfilesScreen::handlePressAction(GameObject*& objectHighlight)
{
	if ( objectHighlight != null )
	{
		if ( currentStep == CreationStepConfirm )
		{
			if( objectHighlight->name == "confirm" )
			{
				changing = false;
				parentScene->lockOutInput(2);
				confirmRoot->findButton("confirm")->setState(ButtonStateNormal);
				parentScene->removeSelection();
				addNewUser();
				exitTimer = 1;
				return false;
			}
			else if( objectHighlight->name == "change" )
			{
				changing = true;
				parentScene->setCurrentHighlight(gotoNextSubStep());
				return true;
			}
		}
		else if ( currentStep == CreationStepName )
		{
			if( objectHighlight == nameDoneButton )
			{
				String enteredName = keyboard->getText();
				printf("Entered Name == %s\n", enteredName.str());
				if(UsersManager::getInstance()->isUserNameUnique(enteredName) == false)
				{
					printf("Username == %s ,is already in use.\n", enteredName.str());
					nameErrorPrompt->cancelActions();
					nameEnterPrompt->recursiveFadeOut();
					nameErrorPrompt->recursiveFadeIn();
					nameErrorPrompt->recursiveFadeOut(2);
					nameEnterPrompt->recursiveFadeIn(2);
				}
				else
				{
					chosenName = enteredName;
					parentScene->setCurrentHighlight(gotoNextSubStep());
				}
				return true;
			}
			else if( keyboard->onButtonPressAction() )
			{
				parentScene->lockOutInput(0.2f);

				String usernameText = keyboard->getText();
				if (objectHighlight->name == "Backspace")
				{
					// Fade last character out takes 0.2f so we block for that long
					parentScene->playSound("Parent_Keystroke_Backspace");
					nameField->removeLastCharacter();
				}
				else
				{
					parentScene->playSound("Parent_Keystroke");
					nameField->setText(usernameText);
					nameField->fadeLastCharacterIn();
				}

				if(usernameText.equals(" "))
					keyboard->setText("");

				if(validNameEntry(usernameText) == false)
				{ nameDoneButton->setVisible(false); }
				else if(!nameDoneButton->isVisibleSelf())
				{	nameDoneButton->bubbleIn();}

				parentScene->setCurrentHighlight(keyboard->getDefaultHighLight());
			}
		}
		else if ( currentStep == CreationStepYear )
		{
			parentScene->lockOutInput(0.2f);
			String pinValue = objectHighlight->name;
			if( pinValue == "backspacebutton" )
			{
				if( chosenYear.length() > 0 )
				{
					chosenYear = chosenYear.substring(0, chosenYear.length()-1);
					parentScene->playSound("Parent_Keystroke_Backspace");
					yearField->removeLastCharacter();
				}
			}
			else
			{
				chosenYear += pinValue;
				yearField->setText(chosenYear);
				yearField->fadeLastCharacterIn();
				parentScene->playSound("Parent_Keystroke");

				if( chosenYear.length() == 4 )
				{
					parentScene->setCurrentHighlight(gotoNextSubStep());
					return true;
				}
			}
		}
		else if ( objectHighlight->typeOf(ButtonObject::type()) )
		{
			if ( currentStep == CreationStepGender )
			{
				chosenGender = objectHighlight->name;
				printf("Gender Chosen: %s\n", chosenGender.str());
			}
			else if ( currentStep == CreationStepMonth )
			{
				chosenMonth = objectHighlight->name;
				printf("Month Chosen: %s\n", chosenMonth.str());
			}
			else if ( currentStep == CreationStepGrade )
			{
				chosenGrade = objectHighlight->name;
				printf("Grade Chosen: %s\n", chosenMonth.str());
			}
			else if (currentStep == CreationStepDay)
			{
				chosenDay = objectHighlight->name;
				printf("Day Chosen: %s\n", chosenDay.str());
			}

			ButtonObject* oldSelection = (ButtonObject*)objectHighlight;
			parentScene->setCurrentHighlight(gotoNextSubStep());
			oldSelection->cancelActions();
			oldSelection->bubbleOut(0.3f);

			return true;
		}
	}
	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool EditProfilesScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	if ( currentStep == CreationStepName && keyboard->onSelectionChange(objectLeft, objectEntered) )
	{
		if( objectEntered != nameDoneButton )
		{
			currentHighlight = keyboard->getDefaultHighLight();
		}

		return true;
	}

	return false;

}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void EditProfilesScreen::cameraArrived()
{
	parentScene->showBackButton(0.75f);

	headerPrompt->setText(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_FIRSTNAME"));

	nameFieldContainer->bubbleIn();
	nameEnterPrompt->recursiveFadeIn();
	keyboard->bubbleIn();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* EditProfilesScreen::getArrivalSelection()
{
	return keyboard->getDefaultHighLight();
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void EditProfilesScreen::addHotSpots( ObjectArray* trackingList )
{
	if ( currentStep == CreationStepMonth )
	{
		printf("Adding buttons for month\n");

		for( int i = 0; i<12; i++ )
		{
			ButtonObject* button = birthMonthRoot->findButton(calender->monthStrForInt(i+1));
			printf("adding month button: %s\n", button->name.str());
			if( button != null )
			{ trackingList->addElement( button ); }
		}
	}
	else if ( currentStep == CreationStepDay )
	{
		printf("Adding buttons for day\n");

		for( int i = 0; i<31; i++ )
		{
			ButtonObject* button = birthDayRoot->findButton(calender->dayStrForInt(i+1));
			if( button != null )
			{ trackingList->addElement( button ); }
		}
	}
	else if ( currentStep == CreationStepConfirm )
	{
		trackingList->addElement(confirmRoot->findButton("confirm"));
		trackingList->addElement(confirmRoot->findButton("change"));
	}
	else if ( currentStep == CreationStepName )
	{
		keyboard->setupHotSpots();
		trackingList->addElement(nameDoneButton);
	}
	else if ( currentStep == CreationStepYear )
	{ yearNumPad->addHotSpots(trackingList); }
	else if ( currentStep == CreationStepGender )
	{
		trackingList->addElement( genderRoot->findButton("girl") );
		trackingList->addElement( genderRoot->findButton("boy") );
	}
	else if ( currentStep == CreationStepGrade )
	{
		printf("Adding Grades buttons\n");

		for(int i = 0; i < gradeLevelRoot->getChildren()->getSize(); ++i)
		{
			BaseObject* bo = gradeLevelRoot->childAtIndex(i);
			if(bo->typeOf(ButtonObject::type()))
			{
				trackingList->addElement((ButtonObject*)bo);
			}
		}
	}
}

/// <summary>
/// Custom update
/// </summary>
void EditProfilesScreen::update(float dt)
{

	if(exitTimer > 0)
	{
		exitTimer -= dt;
		if(exitTimer <= 0)
		{
			if(parentScene->newUserFromChildScene)
			{
				parentScene->lockOutInput(2);
				parentScene->screenDarken->setVisible(true);
				parentScene->screenDarken->setOpacity(0);
				parentScene->screenDarken->fadeTo(1, 1.5f);
				exitToChildTimer = 2;
			}
			else
				parentScene->popScreen();
		}
	}

	if( exitToChildTimer > 0 )
	{
		exitToChildTimer -= dt;
		if ( exitToChildTimer <= 0 )
		{
			SceneManager::getInstance()->launchChildSceneForUser(currentUser);
		}
	}

	super::update(dt);
}

/// <summary>
/// validates the user name field
/// </summary>
bool EditProfilesScreen::validNameEntry(String str)
{
	str.replace(" ", "");
	if(!str.isEmpty())
		return true;

	return false;
}

}

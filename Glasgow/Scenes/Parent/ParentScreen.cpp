#include "ParentScreen.h"

namespace Glasgow
{

ParentScreen::ParentScreen(GameObject* objectRoot, ParentScene* scene) : super()
{
	//*****************************************************************
	// Take over the entire scene graph of the replacement object
	//*****************************************************************
	for ( int i = 0; i<objectRoot->numChildren(); i++ )
	{
		GameObject * child = objectRoot->childAtIndex(i);
		child->addRef(); //so remove from parent doesn't refCount to 0
		child->removeFromParent();
		addChild(child);
		child->release();
		i--;
	}
	name = objectRoot->name;
	setScale(objectRoot->getScale());
	setPosition(objectRoot->getPosition());
	setRotation(objectRoot->getRotation());
	objectRoot->removeFromParent();
	parentScene = scene;
}

ParentScreen::~ParentScreen()
{ }

/// <summary>
/// If we want to handle analog changes
/// </summary>
void ParentScreen::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	//base

}

/// <summary>
///
/// </summary>
void ParentScreen::handlePackageDownloadProgressed( PackageInfo* info, int percent)
{


}

/// <summary>
/// When a package has changed state
/// </summary>
void ParentScreen::handlePackageChanged( tPackageStatus newStatus, PackageInfo* info)
{

	//base
}




/// <summary>
/// If we want to handle cancels
/// </summary>
void ParentScreen::handleDialogCanceled( DialogType dialogType)
{
	//base
}


/// <summary>
/// Network error occurred
/// </summary>
void ParentScreen::handleNetworkError(String networkName, String networkError)
{
	//base class
}

/// <summary>
/// Network connection made
/// </summary>
void ParentScreen::handleNetworkConnected( String networkName )
{
	//base class
}


/// <summary>
///
/// </summary>
void ParentScreen::handleDownloadError()
{
	//base class
}

/// <summary>
/// Download finished
/// </summary>
void ParentScreen::handleDonwloadFinished( String packageID )
{
	//base
}


/// <summary>
///
/// </summary>
void ParentScreen::handlePendingChanged()
{
	//base
}

///<summary>
/// Called when a package is deleted
///</summary>
void ParentScreen::handlePackageDeleted( PackageInfo* info )
{
	//base

}

/// <summary>
/// Download started
/// </summary>
void ParentScreen::handleDonwloadStarted( String packageID )
{
}

/// <summary>
/// Download progress
/// </summary>
void ParentScreen::handleDownloadProgress( String package, int downloadProgress)
{
	//base class
}

/// <summary>
/// Controller Sync Event
/// </summary>
void ParentScreen::handleControllerSync(bool success)
{
	//base class
}

/// <summary>
/// Controller Connected Event
/// </summary>
void ParentScreen::handleControllerConnected()
{
	//base class
}

/// <summary>
/// Controller Disconnected Event
/// </summary>
void ParentScreen::handleControllerDisconnected()
{
	//base class
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ParentScreen::handlePressAction(GameObject*& objectHighlight)
{ return false; }

/// <summary>
/// If we want to handle the press back ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ParentScreen::handlePressBack(GameObject*& objectHighlight)
{ return false; }

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ParentScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	if( objectEntered != null )
	{ ((ButtonObject*)objectEntered)->setState(ButtonStateActive); }

	if( objectLeft != null )
	{ ((ButtonObject*)objectLeft)->setState(ButtonStateNormal); }

	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ParentScreen::cameraArrived()
{ }

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void ParentScreen::cameraLeaving()
{ }

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ParentScreen::getArrivalSelection()
{ return NULL; }

/// <summary>
/// Input forwarding call to handle selection changes
/// </summary>
void ParentScreen::handleSelectionChange( GameObject* old, GameObject* newObj)
{ }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ParentScreen::addHotSpots( ObjectArray* trackingList )
{ }


} /* namespace Glasgow */

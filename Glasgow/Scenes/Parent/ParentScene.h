#ifndef ParentScene_H
#define ParentScene_H


#include "SIEngine/Core/Scene.h"
#include "SIEngine/Include/GlasgowControls.h"
#include "Glasgow/Scenes/Parent/ParentScreen.h"
#include "Glasgow/Scenes/Parent/ChangePinScreen.h"
#include "Glasgow/Scenes/Parent/ContentManagerScreen.h"
#include "Glasgow/Scenes/Parent/DownloadManagerScreen.h"
#include "Glasgow/Scenes/Parent/ProfilesScreen.h"
#include "Glasgow/Scenes/Parent/EditProfilesScreen.h"
#include "Glasgow/Scenes/Parent/GateLockScreen.h"
#include "Glasgow/Scenes/Parent/LandingScreen.h"
#include "Glasgow/Scenes/Parent/LocaleScreen.h"
#include "Glasgow/Scenes/Parent/PackageDetailsScreen.h"
#include "Glasgow/Scenes/Parent/PurchasedManagerScreen.h"
#include "Glasgow/Scenes/Parent/SettingsLandingScreen.h"
#include "Glasgow/Scenes/Parent/SystemSettingsScreen.h"
#include "Glasgow/Scenes/Parent/EthernetScreen.h"
#include "Glasgow/Scenes/Parent/WifiScreen.h"
#include "Glasgow/Scenes/Parent/ControllerScreen.h"
#include "Glasgow/Scenes/Parent/ParentAccountScreen.h"
#include "Glasgow/Scenes/Parent/RegisterScreen.h"
#include "Glasgow/Scenes/Parent/UpdateScreen.h"

#include "SIEngine/Core/Thread.h"

using namespace SICore;

namespace Glasgow
{
class ParentScene;

class PreloadTextureThread : public Thread
{
	CLASSEXTENDS(PreloadTextureThread,Thread);
public:
	PreloadTextureThread(ParentScene* scene_) : super() { scene = scene_; }
	~PreloadTextureThread() {}
	virtual void run();
private:
	ParentScene* scene;
};


class ParentScene : public Scene
{
	CLASSEXTENDS(ParentScene, Scene);
	ADD_TO_CLASS_MAP;

public:

	typedef enum ParentScreenNames
	{
		PS_SCREEN_INVALID = 0,
		PS_PIN_SCREEN,
		PS_CONTENT_MANAGER_SCREEN,
		PS_DOWNLOAD_MANAGER_SCREEN,
		PS_PROFILES_SCREEN,
		PS_EDIT_PROFILES_SCREEN,
		PS_GATE_LOCK_SCREEN,
		PS_LANDING_SCREEN,
		PS_LOCALE_SCREEN,
		PS_PACKAGE_DETAILS_SCREEN,
		PS_PURCHASED_MANAGER_SCREEN,
		PS_SETTINGS_LANDING_SCREEN,
		PS_SYSTEM_SETTINGS_SCREEN,
		PS_NETWORK_SCREEN,
		PS_CONTROLLER_SCREEN,
		PS_PARENT_ACCOUNT_SCREEN,
		PS_REGISTER_SCREEN,
		PS_UPDATE_SCREEN,
		PS_SCREEN_MAX
	} ParentScreenName;

	bool  			newUserFromChildScene;
	bool  			enterFromFWUpdate;
	bool			fwDownloadErrorOccured;
	SpriteObject*	screenDarken;

	ParentScene();
	virtual ~ParentScene();

	void setupForNewUser();

	/// <summary>
	/// Base scene feature, handling controller pairing
	/// </summary>
	virtual void onControllerSynced(HWController* controller, bool success);

	/// <summary>
	/// Base scene feature, handling controller connecting
	/// </summary>
	virtual void onControllerConnected(HWController* controller);

	/// <summary>
	/// Base scene feature, handling controller disconnecting
	/// </summary>
	virtual void onControllerDisconnected(HWController* controller);

	/// <summary>
	/// Download error'd out
	/// </summary>
	virtual void onDownloadError();

	/// <summary>
	/// Download finished
	/// </summary>
	virtual void onDonwloadFinished( String packageID );

	/// <summary>
	/// Download started
	/// </summary>
	virtual void onDonwloadStarted( String packageID );

	/// <summary>
	/// Download has progressed
	/// </summary>

	void onDonwloadProgressed( String packageID, int downloadProgress );

	virtual void onPackageChanged(tPackageStatus newStatus, PackageInfo* info );

	virtual void onPendingDownloadsChanged();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onButtonHint();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onButtonHome();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onButtonPressAction();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onButtonPressBack();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onSelectionChange( GameObject* oldSelection, GameObject* newSelection);

	/// <summary>onTimerExpired
	/// Custom handling of analog changes
	/// </summary>
	virtual void onAnalogChange(GameObject* objectLeft, GameObject* objectEntered );

	/// <summary>
	/// Base scene feature, tick
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// onDialogHandled
	/// </summary>
	virtual void onDialogHandled( DialogType type, DialogButtonOption option );


	/// <summary>
	/// When a dialog is canceled, meaning an option wasn't selected but it was dismissed.
	/// </summary>
	virtual void onDialogCanceled( DialogType dialogType);

	/// <summary>
	/// Child scene uses timers
	/// </summary>
	virtual void onTimerExpired( TimerEvent* timer );

	/// <summary>
	/// Network error occured
	/// </summary>
	void onNetworkError(String networkName, String networkError);

	/// <summary>
	/// Network connection made
	/// </summary>
	void onNetworkConnected( String networkName );

	/// <summary>
	/// Base scene feature, handling ethernet plugged in
	/// </summary>
	virtual void onEthernetPluggedIn();

	/// <summary>
	/// Base scene feature, handling ethernet unplugged
	/// </summary>
	virtual void onEthernetUnplugged();

	/// <summary>
	/// Base scene feature, show contextual back button
	/// </summary>
	void showBackButton(float delay = 0, bool isCurrentHighlight = false);

	/// <summary>
	/// Base scene feature, hide contextual back button
	/// </summary>
	void hideBackButton(float delay = 0);

	/// <summary>
	/// Base scene feature, show contextual help button
	/// </summary>
	void showHelpButton(float delay = 0, bool isCurrentHighlight = false);

	/// <summary>
	/// Base scene feature, hide contextual help button
	/// </summary>
	void hideHelpButton(float delay = 0);


	/// <summary>
	/// push a new screen to the view stack (screenname)
	/// </summary>
	void pushScreen(ParentScreenName screenName);

	/// <summary>
	/// pops the last screen from the view
	/// </summary>
	ParentScreen* popScreen();

	/// <summary>
	/// replaces the last screen from the view (screenname)
	/// </summary>
	void replaceScreen(ParentScreenName screenName);

	/// <summary>
	/// force exit to child scene
	/// </summary>
	void exitToChildScene();

	/// <summary>
	/// parent gate has been passed
	/// </summary>
	void parentGatePassed();

	/// <summary>
	/// Setup hotspots
	/// </summary>
	void setupHotSpots();

	/// <summary>
	///
	/// </summary>
	virtual void onPackageDownloadProgressed( PackageInfo* info, int percent);

	///<summary>
	/// returns screen being displayed
	///</summary>
	ParentScreen* getCurrentScreen();

	///<summary>
	/// Called when a package is deleted
	///</summary>
	virtual void onPackageDeleted( PackageInfo* info );


private:
	/// <summary>
	/// Fills in pointers and arrays of references post scene Load
	/// </summary>
	void fillReferences();


	/// <summary>
	/// Sets up all objects for the initial scene entry
	/// </summary>
	void setupInitialState();

	/// <summary>
	/// exit dialog or immediate exit dependant on the screen entered
	/// </summary>
	void requestExitSettings();

	/// <summary>
	/// push a new screen to the view stack
	/// </summary>
	void pushScreen( ParentScreen* newScreen );

	/// <summary>
	/// replaces the last screen from the view
	/// </summary>
	void replaceScreen(ParentScreen* newScreen);
//	virtual void saveSettings();

	/// <summary>
	/// References to objects
	/// </summary>
	SpriteObject*	background;

	ButtonObject*	prevButton;
	ButtonObject*	helpButton;

	float			backButtonTimer;
	bool			willHighlightBackButton;
	float			helpButtonTimer;
	bool			willHighlightHelpButton;
	float 			exitTimer;

	int				numEthernetChecks;
	ObjectArray		screenQueue;
	HashTable		screens;



	ParentScreen*					currentScreen;

	ChangePinScreen*				pinScreen;
	ContentManagerScreen*			contentScreen;
	DownloadManagerScreen*			downloadScreen;
	EditProfilesScreen*				editProfileScreen;
	ProfilesScreen*					profilesScreen;
	GateLockScreen*					gateLockScreen;
	LandingScreen*					landingScreen;
	LocaleScreen*					localeScreen;
	PackageDetailsScreen*			detailsScreen;
	PurchasedManagerScreen*			purchasedScreen;
	SettingsLandingScreen*			settingsLandingScreen;
	SystemSettingsScreen*			systemSettingsScreen;
	WifiScreen*						wifiScreen;
	EthernetScreen*					ethernetScreen;
	ControllerScreen*				controllerScreen;
	ParentAccountScreen*			parentAccountScreen;
	RegisterScreen*					registerScreen;
	UpdateScreen*					updateScreen;

};

}

#endif /* PARENTSCENE_H_ */

#include "RegisterScreen.h"
#include "ParentScene.h"

namespace Glasgow
{

RegisterScreen::RegisterScreen(GameObject* objectRoot, ParentScene* scene) : super(objectRoot, scene)
{
	printf("RegisterScreen::RegisterScreen\n");

	activationRoot 	   		= find("activationRoot");
	activatedRoot  	   		= find("activatedRoot");

	activateCodeLabel		= activationRoot->find("activationCode")->findLabel("codeText");

	createNetError();

	setupInitialState();
}

RegisterScreen::~RegisterScreen()
{
	printf("RegisterScreen::~RegisterScreen()\n");
	SAFE_RELEASE(netError);
	disconnect(PackageManager::Instance(), SIGNAL(DeviceUpdated()), this, SLOT(onDeviceUpdated()));
}

void RegisterScreen::setupInitialState()
{
	attempts 		 		= 0;
	netErrorActive 			= false;
	setupTimeOut 			= -1;
	activationCodeTimeOut	= -1;
	processed = true;

	activatedRoot->setVisible(false);
	netError->setVisible(false);
}

void RegisterScreen::loadGameObjectState(GameObjectState state)
{
	printf("RegisterScreen::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// Helper call to instantiate a netError object to handle reconnect / skip flow
/// </summary>
void RegisterScreen::createNetError()
{
	printf("RegisterScreen::createNetError()\n");
	netError = (NetErrorObject*)Scene::instantiate("NetErrorObject"); //ref1
	find("netErrorContainer")->addChild(netError);//ref 2 ::~ -> ref 1
	netError->setVisible(false);
	netError->setPosition(Vec3f::Zero);
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
void RegisterScreen::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	if( dialogType == DialogTypeActivationFail )
	{
		if( option.id == "tryagain" )
		{
			startActivationRequest();
			parentScene->removeSelection();
		}
		else
		{
			parentScene->removeSelection();
		}
	}

	super::handleDialogClosed(dialogType,option);
}

void RegisterScreen::handleDialogCanceled( DialogType dialogType )
{
	if( dialogType == DialogTypeActivationFail )
	{
		if( attempts < 3 )
		{ startActivationRequest(); }

		parentScene->removeSelection();
	}
	else
	{
		parentScene->removeSelection();
	}

	super::handleDialogCanceled(dialogType);
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool RegisterScreen::handlePressAction(GameObject*& objectHighlight)
{
	if(objectHighlight != null && objectHighlight->name  == "reconnectButton")
	{
		parentScene->pushScreen(ParentScene::PS_NETWORK_SCREEN);
		return true;
	}
	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool RegisterScreen::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void RegisterScreen::cameraArrived()
{
	parentScene->showHelpButton(0.1f,false);
	parentScene->showBackButton(0.25f,true);

	RioPkgManager* managerInstance = PackageManager::Instance();
	managerInstance->UpdateProfiles(true);
	connect(managerInstance, SIGNAL(DeviceUpdated()), this, SLOT(onDeviceUpdated()));
	startActivationRequest();
}


/// <summary>
/// Activation code succeeded
/// </summary>
void RegisterScreen::doActivationRequestFinished()
{
	if( !processed )
	{
		String activationCode = activationRequest.value().toStdString().c_str();
		printf("ACTIVATION REQUEST IS FINISHED: %s\n", activationCode.str());
		activationCodeTimeOut = 0;

		if(activationCode.isEmpty())
		{									//Unavailable
			activateCodeLabel->setText("Unavailable"); //Requesting

			if( !netErrorActive )
			{
				// Only show the try again dialog on the first two attempts
				if ( attempts < 3 )
				{
					// TODO: Localize Strings
					if( attempts == 1 )
					{
						DialogOptions options;
						options.title   = "Activation Request Error";
						options.prompt  = "There was an issue creating your activation code.  Please try again.";
						options.buttonA = DialogButtonOption("Try Again", "tryagain");
						parentScene->showModalDialog(DialogTypeActivationFail, options);
						processed = true;
						return;
					}
					else
					{
						DialogOptions options;
						options.title 	= "Error";
						options.prompt  = "We were unable to get a Registration Code. Please try again.";
						options.buttonA = DialogButtonOption("OK", "ok");
						options.ignoreOldTrackedObjects = true;
						parentScene->showModalDialog(DialogTypeActivationFail, options);
						processed = true;
						return;
					}
				}
			}

			parentScene->showHelpButton(0.1f, true);
			processed = true;
			return;
		}

		if( !netErrorActive )
		{
			activateCodeLabel->fadeOut(0);
			activateCodeLabel->changeTo(activationCode.str());
			activateCodeLabel->fadeIn(0);

			//start setup time out
			setupTimeOut = Constants::Time::activationTimeOut;
		}
		parentScene->setupHotSpots();
		processed = true;
	}
}

/// <summary>
/// Activation code timed out
/// </summary>
void RegisterScreen::doActivationTimeOut()
{

	activationCodeTimeOut = 0;
	if( !activatedRoot->isVisibleHierarchy() && !processed)
	{
		DialogOptions options;
		options.title 	= "Error";
		options.prompt  = "We were unable to get a Registration Code. Please try again.";

		if( !ConnectionManager::getInstance()->leapFrogServersResponded )
		{
			options.title 	= "Error";
			options.prompt  = "We are unable to connect to LeapFrog servers. Please check your Internet connection or try again later.";
			attempts = 100;
		}


		options.buttonA = DialogButtonOption("OK", "ok");
		options.ignoreOldTrackedObjects = true;
		parentScene->showModalDialog(DialogTypeActivationFail, options);
		processed = true;
	}
}

/// <summary>
/// Activation code failed
/// </summary>
void RegisterScreen::doActivationFail()
{
	if( !processed )
	{
		printf("ACTIVATION REQUEST ERROR: %s\n", activationRequest.error().message().toStdString().c_str());
		activationCodeTimeOut = 0;

		//only allow 2 attempts
		if( !activatedRoot->isVisibleHierarchy())
		{
			if ( attempts < 3 && !netErrorActive)
			{
				activateCodeLabel->setText("Unavailable");

				if( attempts == 1 )
				{
					DialogOptions options;
					options.title 	= "Activation Request Error";
					options.prompt  = activationRequest.error().message().toStdString().c_str();
					options.buttonA = DialogButtonOption("Try Again", "tryagain");
					options.ignoreOldTrackedObjects = true;
					parentScene->showModalDialog(DialogTypeActivationFail , options);
				}
				else
				{
					DialogOptions options;
					options.title 	= "Error";
					options.prompt  = "We were unable to get a Registration Code. Try again later.";
					options.buttonA = DialogButtonOption("OK", "ok");
					options.ignoreOldTrackedObjects = true;
					parentScene->showModalDialog(DialogTypeActivationFail , options);
				}
			}
			else
			{ showNetworkError("Check Connection", "The request to get your registration code has timed out."); }
		}

		parentScene->setupHotSpots();
		processed = true;
	}
}

/// <summary>
/// Updates activation request
/// </summary>
void RegisterScreen::updateActivationRequest(float dt )
{
	if( activationCodeTimeOut > 0 )
	{
		activationCodeTimeOut -= dt;
		if( activationRequest.isFinished() )
		{
			if( activationRequest.isError() || !activationRequest.isValid())
			{ doActivationFail(); }
			else
			{ doActivationRequestFinished(); }
		}
		else if ( activationCodeTimeOut <= 0 )
		{ doActivationTimeOut(); }
	}
}

/// <summary>
/// Network error occured
/// </summary>
void RegisterScreen::handleNetworkError(String networkName, String networkError)
{
	printf("RegisterScreen::handleNetworkError ==> %s\n", networkError.str());
	//No need to care if network goes down while an activation request has been receieved
	if( !activatedRoot->isVisibleHierarchy())
	{ showNetworkError("Check Connection", networkError); }

	parentScene->setupHotSpots();
}

/// <summary>
/// Network connection made
/// </summary>
void RegisterScreen::handleNetworkConnected( String networkName )
{
	printf("RegisterScreen::handleNetworkConnected()\n");
	netError->animateOut();
	netErrorActive = false;

	if( activatedRoot->isVisibleHierarchy())
	{
		activatedRoot->find("welcomeHeader")->bubbleIn(0.5f);
		activatedRoot->find("freeGameIcon")->bubbleIn(0.65f);
		activatedRoot->find("welcomeText1")->setVisible(false);
		activatedRoot->find("welcomeText2")->bubbleIn(0.75f);
	}
	else
	{
		activationRoot->find("activationCode")->bubbleIn(0);
		activationRoot->find("computerText")->bubbleIn(0);
		activationRoot->find("header")->bubbleIn(0);
		activationRoot->find("enterCodeText")->bubbleIn(0);
		activationRoot->find("urlText")->bubbleIn(0);
		if(parentScene->getCurrentScreen() == this)
		{ startActivationRequest();}
	}

	parentScene->setupHotSpots();
}

/// <summary>
/// Animation transition into a networking error
/// </summary>
void RegisterScreen::showNetworkError( String header, String error )
{
	setupTimeOut = 0;
	activationCodeTimeOut = 0;

	activationRoot->find("activationCode")->bubbleOut(0);
	activationRoot->find("computerText")->bubbleOut(0);
	activationRoot->find("header")->bubbleOut(0);
	activationRoot->find("enterCodeText")->bubbleOut(0);
	//activationRoot->find("skipButton")->bubbleOut(0);
	activationRoot->find("urlText")->bubbleOut(0);

	activatedRoot->find("freeGameIcon")->bubbleOut(0);
	activatedRoot->find("welcomeHeader")->fadeOut(0);
	activatedRoot->find("welcomeText1")->fadeOut(0);
	activatedRoot->find("welcomeText2")->fadeOut(0);

	netError->setVisible(true);
	netError->displayNetworkError(header, error);
	netError->skipButton->setVisible(false);

	parentScene->setupHotSpots();

	if( !parentScene->isShowingModalDialog() )
	{ parentScene->setCurrentHighlight(netError->reconnectButton); }
	netErrorActive = true;
}

/// <summary>
/// Overridden so we can poll the activationRequest
/// </summary>
void RegisterScreen::update(float dt)
{
	updateActivationRequest(dt);
	super::update(dt);
}

/// <summary>
/// Start activation request
/// </summary>
void RegisterScreen::startActivationRequest()
{
	if( attempts >= 2 )
		return;

	printf("RegisterScreen::startActivationRequest()\n");
	processed = false;
	attempts++;

	// TODO: Localize strings
	activateCodeLabel->setText("Requesting");
	activationRequest = PackageManager::Instance()->GetActivationCode();
	activationCodeTimeOut = Constants::Time::registrationCodeTimeOut;
}

/// <summary>
/// Remove network error
/// </summary>
void RegisterScreen::hideNetworkError()
{
	if( netError->isVisibleHierarchy())
	{ netError->animateOut(); }

	netErrorActive = false;
	parentScene->setupHotSpots();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* RegisterScreen::getArrivalSelection()
{
	if( attempts > 2 )
	{ return parentScene->getWorldRoot()->findButton("helpButton"); }

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void RegisterScreen::addHotSpots( ObjectArray* trackingList )
{
	if ( netErrorActive )
	{ netError->addHotSpots(trackingList); }
}


/// <summary>
/// We have a call that the device was updated via the activation step !!!
/// </summary>
void RegisterScreen::onDeviceUpdated()
{
	printf("=======================================\n");
	printf("=======================================\n");
	printf("ACTIVATION SUCCEEDED !!! \n");
	printf("=======================================\n");
	printf("=======================================\n");
	SystemSettings::setIsActivated(true);
	hideNetworkError();

	activationRoot->find("activationCode")->bubbleOut(0);
	activationRoot->find("computerText")->bubbleOut(0);
	activationRoot->find("header")->bubbleOut(0);
	activationRoot->find("enterCodeText")->bubbleOut(0);
	activationRoot->find("urlText")->bubbleOut(0);
	activatedRoot->setVisible(true);

	Color c = activatedRoot->findLabel("welcomeHeader")->getTextColor();
	c.a = 1;
	activatedRoot->findLabel("welcomeHeader")->setTextColor(c);

	c = activatedRoot->findLabel("welcomeText1")->getTextColor();
	c.a = 1;
	activatedRoot->findLabel("welcomeText1")->setTextColor(c);

	c = activatedRoot->findLabel("welcomeText2")->getTextColor();
	c.a = 1;
	activatedRoot->findLabel("welcomeText2")->setTextColor(c);

	activatedRoot->find("welcomeHeader")->bubbleIn(0.5f);
	activatedRoot->find("freeGameIcon")->bubbleIn(0.65f);
	activatedRoot->find("welcomeText1")->setVisible(false);
	activatedRoot->find("welcomeText2")->bubbleIn(0.75f);

	parentScene->showBackButton(0.1f,true);
	netErrorActive = false;
}

}

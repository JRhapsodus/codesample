#ifndef PARENTACCOUNTSCREEN_H_
#define PARENTACCOUNTSCREEN_H_

#include "ParentScreen.h"

namespace Glasgow
{

class ParentAccountScreen : public ParentScreen
{
	CLASSEXTENDS(ParentAccountScreen, ParentScreen);
	ADD_TO_CLASS_MAP;

public:
	ParentAccountScreen(GameObject* objectRoot, ParentScene* scene);
	virtual ~ParentAccountScreen();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();

	//***************************
	// scenegraph objects
	//***************************
	GameObject*		hasAccountRoot;
	GameObject*		noAccountRoot;

	TextLabel*		emailLabel;
	TextLabel*		errorPrompt;

	ButtonObject*	registerButton;
	ButtonObject*	registerAgainButton;
	//***************************
};

} /* namespace Glasgow */

#endif /* PARENTACCOUNTSCREEN_H_ */

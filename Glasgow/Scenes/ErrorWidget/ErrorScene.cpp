#include "ErrorScene.h"
#include "SIEngine/Include/SIRendering.h"
#include "Glasgow/GlasgowApp.h"

namespace Glasgow
{

ErrorScene::ErrorScene() : super("Layouts/ErrorScene.dat", "ErrorSceneStrings")
{
	printf("ErrorScene::ErrorScene()\n");
	setupHotSpots();
	autoCorrect = false;
	autoClose = 0;
	currentHighlight = findQtButton("home");
	QVariantMap parameters = GlasgowApp::appInstance->getLaunchParameters();

	if ( parameters.contains("ErrorID") )
	{ errorID = (ErrorID)String(parameters.value("ErrorID").toString().toStdString().c_str()).toInt(); }
	if ( parameters.contains("PromptTitle") )
	{ titleOverride = parameters.value("PromptTitle").toString().toStdString().c_str(); }
	if ( parameters.contains("PromptDescription") )
	{ descriptionOverride = parameters.value("PromptDescription").toString().toStdString().c_str(); }
	if ( parameters.contains("CustomGraphic") )
	{ customErrorPath = parameters.value("CustomGraphic").toString().toStdString().c_str(); }
	if( parameters.contains("AutoClose") )
	{ autoClose = (float)String(parameters.value("AutoClose").toString().toStdString().c_str()).toInt(); }
	if( parameters.contains("AutoSolve") )
	{ autoCorrect = String(parameters.value("AutoSolve").toString().toStdString().c_str()).toInt() == 0 ? false : true; }

	hideAll();

	//********************************************************************************************************
	// Create Top Bar
	//********************************************************************************************************
	TRelease<QtRectObject> tBar( new QtRectObject( Color( 0, 0, 0, 0.8f), Size( 1280, 720 ) ));
	tBar->name = "tBar";
	worldRoot->addChild(tBar);
	tBar->setPosition( Vec3f( 0, 0, 5000 ) );
	printf("End ErrorScene::ErrorScene()");

	GlasgowApp::mainState->writeUIReadyFlag();
	loaded = true;
}

ErrorScene::~ErrorScene()
{ printf("ErrorScene::~ErrorScene()\n"); }

/// <summary>
/// Over ridden to auto close
/// </summary>
void ErrorScene::update(float dt)
{
	if( autoClose > 0 )
	{
		autoClose -= dt;
		if ( autoClose < 0 )
		{ closeWidget("Response", "AutoCloseTimer"); }
	}

	super::update(dt);
}

/// <summary>
/// Over ridden to auto close for Controller group
/// </summary>
void ErrorScene::onControllerConnected(HWController* controller)
{
	printf("ErrorScene::onControllerConnected\n");

	if( autoCorrect && errorID == ErrorIDController )
	{ closeWidget("Response", "ControllerConnected"); }

	super::onControllerConnected(controller);
}

/// <summary>
/// Helper function to hide all on scene graph
/// </summary>
void ErrorScene::hideAll()
{
	GameObject* errorRoot = worldRoot->find("Errors");
	GameObject* toTurnOn  = NULL;

	for ( int i = 0; i<errorRoot->numChildren(); i++ )
	{ errorRoot->childAtIndex(i)->setVisible(false); }

	printf("Setting up ErrorWidget for ErrorID: %d\n", errorID);

	if( errorID == ErrorIDCamera )
	{ toTurnOn = errorRoot->find("Camera"); }
	else if( errorID == ErrorIDController)
	{
		toTurnOn = errorRoot->find("Controller");
		worldRoot->find("home")->setVisible(false);
	}
	else if( errorID == ErrorIDCartridge )
	{ toTurnOn = errorRoot->find("Cartridge"); }
	else if( errorID == ErrorIDNetwork )
	{ toTurnOn = errorRoot->find("Network"); }
	else if ( errorID == ErrorIDSystem )
	{ toTurnOn = errorRoot->find("System"); }
	else if ( errorID == ErrorIDContent )
	{ toTurnOn = errorRoot->find("Content"); }
	else if ( errorID == ErrorIDPetPlay )
	{ toTurnOn = errorRoot->find("PetPlay"); }
	else if ( !customErrorPath.isEmpty() )
	{ toTurnOn = errorRoot->find("Custom"); }

	if( toTurnOn != null )
	{
		toTurnOn->setVisible(true);

		if ( !titleOverride.isEmpty() )
		{
			printf("TITLE OVERRIDE: %s\n", titleOverride.str());
			((QtTextObject*)toTurnOn->find("title"))->setText(titleOverride);
		}

		if ( !descriptionOverride.isEmpty() )
		{
			printf("DESCRIPTION OVERRIDE: %s\n", descriptionOverride.str());
			((QtTextObject*)toTurnOn->find("description"))->setText(descriptionOverride);
		}
	}

	printf("END\n");
}

/// <summary>
/// Needed for auto solve
/// </summary>
void ErrorScene::onCameraPluggedIn()
{
	printf("ErrorScene::onCameraPluggedIn()\n");
	if( autoCorrect && errorID == ErrorIDCamera )
	{ closeWidget("Response", "CameraPluggedIn"); }
}

/// <summary>
/// Auto correct
/// </summary>
void ErrorScene::onNetworkConnected( String networkName )
{
	printf("ErrorScene::onNetworkConnected()\n");
	if( autoCorrect && errorID == ErrorIDNetwork )
	{ closeWidget("Response", "NetworkConnected"); }
}

/// <summary>
/// Helper function to cast a qtbuttonobject
/// </summary>
QtButtonObject* ErrorScene::findQtButton(String name)
{
	GameObject* goRet = (QtButtonObject*)worldRoot->find(name);
	if( goRet != null )
		return (QtButtonObject*)goRet;
	return NULL;
}

/// <summary>
/// Close widget with an option shut down reason
/// </summary>
void ErrorScene::closeWidget(String parameterName, String parameterResponse)
{
	QVariantMap responseMap;
	responseMap.insert(parameterName.str(), parameterResponse.str());
	GlasgowApp::appInstance->mainState->quit(responseMap);
}

/// <summary>
/// Perhaps app should just exit on button press?
/// </summary>
void ErrorScene::onButtonHome()
{
	closeWidget("Response", "ButtonPressHome");
	super::onButtonPressAction();
}

/// <summary>
/// Perhaps app should just exit on button press?
/// </summary>
void ErrorScene::onButtonPressAction()
{
	closeWidget("Response", "ButtonPressAction");
	super::onButtonPressAction();
}

/// <summary>
/// Perhaps app should just exit on button press?
/// </summary>
void ErrorScene::onButtonPressBack()
{
	closeWidget("Response", "ButtonPressBack");
	super::onButtonPressBack();
}

/// <summary>
/// Returns the object representing by a no selection -> selected state chnage
/// </summary>
GameObject* ErrorScene::getDefaultHighLight()
{ return findQtButton("home"); }

/// <summary>
/// Sets up hot spots -- Ugly implementation but brute fast quick, only like 5 buttons anyways
/// </summary>
void ErrorScene::setupHotSpots()
{
	trackedObjects->removeAllElements();
	trackedObjects->addElement( findQtButton("home"));
}

/// <summary>
/// Over ridden to deal with the render chain of qtrenderer, normally we have scene graph objects for this
/// </summary>
void ErrorScene::postRender()
{
	QtRenderer::getInstance()->swapBuffers();
	super::postRender();
}

}

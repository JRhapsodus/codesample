#ifndef ERRORSCENE_H_
#define ERRORSCENE_H_

#include "SIEngine/Core/Scene.h"
#include "SIEngine/Core/User.h"
#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Utilities/Constants.h"
#include "SIEngine/Objects/QtButtonObject.h"
#include "SIEngine/Objects/QtTextObject.h"
#include "Glasgow/Renderables/QtRectObject.h"

using namespace SICore;
using namespace SIUtils;

namespace Glasgow
{

class ErrorScene : public Scene
{
	CLASSEXTENDS(ErrorScene, Scene);
public:
	ErrorScene();
	virtual ~ErrorScene();

	/// <summary>
	/// Perhaps app should just exit on button press?
	/// </summary>
	virtual void onButtonPressAction();

	/// <summary>
	/// Perhaps app should just exit on button press?
	/// </summary>
	virtual void onButtonPressBack();

	/// <summary>
	/// Sets up hot spots -- Ugly implementation but brute fast quick, only like 5 buttons anyways
	/// </summary>
	virtual void setupHotSpots();

	/// <summary>
	/// Returns the object representing by a no selection -> selected state chnage
	/// </summary>
	virtual GameObject* getDefaultHighLight();

	/// <summary>
	/// Over ridden to deal with the render chain of qtrenderer, normally we have scene graph objects for this
	/// </summary>
	virtual void postRender();

	/// <summary>
	/// Over ridden to auto close
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Needed for auto solve
	/// </summary>
	virtual void onCameraPluggedIn();

	/// <summary>
	/// Over ridden to auto close for network connection
	/// </summary>
	virtual void onNetworkConnected( String networkName );

	/// <summary>
	/// Over ridden to auto close for Controller group
	/// </summary>
	virtual void onControllerConnected(HWController* controller);

	/// <summary>
	/// On button home
	/// </summary>
	virtual void onButtonHome();

protected:
	/// <summary>
	/// Close widget with an option shut down reason
	/// </summary>
	void closeWidget(String parameterName, String parameterResponse);

	/// <summary>
	/// Helper function to cast a qtbuttonobject
	/// </summary>
	QtButtonObject* findQtButton(String name);

	/// <summary>
	/// Helper function to hide all root error types
	/// </summary>
	void hideAll();

private:
	ErrorID errorID;
	String titleOverride;
	String descriptionOverride;
	String customErrorPath;
	bool	autoCorrect;
	float	autoClose;
};

}
#endif

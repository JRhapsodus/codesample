#include "VideoWidgetScene.h"
#include "Glasgow/GlasgowApp.h"
#include "SIEngine/Include/SIGeometry.h"
#include <stdio.h>

using namespace LogData::Lightning;
using namespace LTM;
namespace Glasgow
{

VideoWidgetScene::VideoWidgetScene() : super("Layouts/VideoWidgetScene.dat", "VideoWidgetSceneStrings")
{
	printf("VideoWidgetScene::VideoWidgetScene\n");

	//Defualt to play
	nextMovieTimer			= 0;
	currentVideoIndex 		= -1;
	replayTimer				= 0;
	isPaused 				= false;
	useClosedCaption 		= false;
	showUI 					= false;
	isPlaying				= false;
	analogMaxDistance 		= 1000;
	fastForward				= 0;
	rewind					= 0;
	oneSecondTimer 			= 0;
	analogTrackOffScreen 	= true;
	resumeTimer				= 0;
	watchedEnough			= false;
	playingLastFrame = false;
	fillReferences();
	setupHotSpots();
	setupDefaultState();
	GlasgowApp::mainState->writeUIReadyFlag();


	lastUser = UsersManager::getInstance()->getUser(SystemSettings::getLastUser());
	if( lastUser != null )
	{
		playerLog = PlayerLog::Instance(lastUser->getPlayerSLot());

		CActivityStart activityStart((char*)"WV", null, null);
		playerLog->Append(*(dynamic_cast<CLogData*>(&activityStart)));
	}
}

VideoWidgetScene::~VideoWidgetScene()
{
	printf("VideoWidgetScene::~VideoWidgetScene()\n");
}

/// <summary>
/// Helper cast find
/// </summary>
QtSpriteObject* VideoWidgetScene::findQtSprite(String name )
{
	GameObject* goRet = (QtSpriteObject*)worldRoot->find(name);
	if( goRet != null )
		return (QtSpriteObject*)goRet;

	goRet = (QtSpriteObject*)topPanel->find(name);
	if( goRet != null )
		return (QtSpriteObject*)goRet;

	goRet = (QtSpriteObject*)bottomPanel->find(name);
	if( goRet != null )
		return (QtSpriteObject*)goRet;

	//exception
	return NULL;


}

/// <summary>
/// Helper cast find
/// </summary>
QtButtonObject* VideoWidgetScene::findQtButton(String name)
{
	GameObject* goRet = (QtButtonObject*)worldRoot->find(name);
	if( goRet != null )
		return (QtButtonObject*)goRet;

	goRet = (QtButtonObject*)topPanel->find(name);
	if( goRet != null )
		return (QtButtonObject*)goRet;

	goRet = (QtButtonObject*)bottomPanel->find(name);
	if( goRet != null )
		return (QtButtonObject*)goRet;

	//exception
	return NULL;
}

/// <summary>
/// Fill references
/// </summary>
void VideoWidgetScene::fillReferences()
{
	//******************************************************************************************
	// Fill Video list of information based on what was passed into the widget
	//******************************************************************************************
	QVariantMap parameters = GlasgowApp::appInstance->getLaunchParameters();
	String videoIndex = parameters.value("VideoStart").toString().toStdString().c_str();
	if( !videoIndex.isEmpty() )
	{
		currentVideoIndex = videoIndex.toInt();
		printf("VIDEO INDEX WAS PASSED IN AS %d\n", currentVideoIndex );
	}

	videoPackageID 			= parameters.value("PackageID").toString().toStdString().c_str();
	rootVideoPackagePath 	= parameters.value("AppDirPath").toString().toStdString().c_str();

	for ( int i = 0; i<100; i++ )
	{
		String videoNameKey = String("VideoName"+String(i));
		String videoFileKey = String("Video"+String(i));
		String videoName = parameters.value(videoNameKey.str()).toString().toStdString().c_str();
		String videoPath = parameters.value(videoFileKey.str()).toString().toStdString().c_str();

		if( !videoPath.isEmpty() )
		{
			TRelease<VideoInfo> newInfo( new VideoInfo(videoPath, videoName) );
			videos.addElement(newInfo);
		}
	}

	GameObject* captionRoot = worldRoot->find("closedCaptions");
	if( captionRoot == null )
		ThrowException::deserializeException();

	//8 is max in CaptionData
	for( int i = 0;i<8; i++ )
	{
		closedCaptionsLabels.addElement(captionRoot->find( String("captions") + String(i) ));
		((QtTextObject*)closedCaptionsLabels[i])->setText("");
		((QtTextObject*)closedCaptionsLabels[i])->setVisible(false);
	}

	//********************************************************************************************************
	// Play video must be called to create video surfaces & textures, should be named setupVideoForPlay
	//********************************************************************************************************
	topPanel 			= worldRoot->find("topPanel");
	bottomPanel 		= worldRoot->find("bottomPanel");
	statusLabel 		= (QtTextObject*)bottomPanel->find("statusLabel");
	currentTimeLabel	= (QtTextObject*)bottomPanel->find("currentTimeLabel");
	totalTimeLabel		= (QtTextObject*)bottomPanel->find("totalTimeLabel");
	videoNameLabel 		= (QtTextObject*)topPanel->find("videNameLabel");
	backLabel			= (QtTextObject*)topPanel->find("home")->find("text");
	backLabel->setText("Back");


	if( currentVideoIndex >= 0 )
	{
		playVideo( currentVideoIndex );

		//********************************************************************************************************
		// Add Video View Frame
		//********************************************************************************************************
		addViewFrame( QtRenderer::getInstance()->getVideoRect() );

		//********************************************************************************************************
		// Create Top Bar
		//********************************************************************************************************
		topBar = new QtRectObject( Color( 0, 0, 0, 0.5f), Size( 1280, 150 ) );
		topBar->name = "topBar";
		worldRoot->addChild(topBar);
		topBar->setPosition( Vec3f( 0, 0, 1000 ) );
		topBar->release();

		//********************************************************************************************************
		// Create Bottom Bar
		//********************************************************************************************************
		bottomBar = new QtRectObject( Color( 0, 0, 0, 0.5f), Size( 1280, 150 ) );
		bottomBar->name = "bottomBar";
		worldRoot->addChild(bottomBar);

		bottomBar->setPosition( Vec3f( 0, 720-150, 0 ) );
		bottomBar->release();

		//********************************************************************************************************
		// Create Top Bar
		//********************************************************************************************************
		TRelease<QtRectObject> tBar( new QtRectObject( Color( 0, 0, 0, 0), Size( 1280, 720/2 ) ));
		tBar->name = "tBar";
		worldRoot->addChild(tBar);
		tBar->setPosition( Vec3f( 0, 0, 5000 ) );

		TRelease<QtRectObject> rectCenter( new QtRectObject( Color::BlackClear, Size(1280, 720/2) ) );
		rectCenter->name = "ccccccc";
		worldRoot->addChild(rectCenter);
		rectCenter->setPosition( Vec3f(0, 720/2, 4000) );
	}

}

/// <summary>
/// Setup default state
/// </summary>
void VideoWidgetScene::setupDefaultState()
{
	findQtButton("pause")->setVisible(true);
	findQtButton("closedCaptionEnabled")->setVisible(false);
	topPanel->setPosition( Vec3f( 0, -500, 0 ) );
	bottomPanel->setPosition( Vec3f( 0, 500, 0 ) );

	topBar->setPosition( Vec3f( 0, -500, 1000 ) );
	bottomBar->setPosition( Vec3f( 0, 900, bottomBar->getPosition().z ) );
}

/// <summary>
/// Adds a view frame
/// </summary>
void VideoWidgetScene::addViewFrame(Rect frame)
{
	//dont bother with full screen videos
	if( frame.size_.width_ != 1280 || frame.size_.height_ != 720)
	{
		printf("Creating addview Frame\n");
		frame.debugLog();

		TRelease<QtRectObject> rectTop( new QtRectObject( Color::BlackOpaque, Size(1280, frame.origin_.y) ) );
		TRelease<QtRectObject> rectRight( new QtRectObject( Color::BlackOpaque, Size(frame.origin_.x, 720) ) );
		TRelease<QtRectObject> rectBottom( new QtRectObject( Color::BlackOpaque, Size(1280, frame.origin_.y) ) );
		TRelease<QtRectObject> rectLeft( new QtRectObject( Color::BlackOpaque, Size(frame.origin_.x, 720) ) );

		rectTop->name = "tttttt";
		rectRight->name = "rrrrrr";
		rectBottom->name = "bbbbb";
		rectLeft->name = "lllll";


		worldRoot->addChild(rectTop);
		worldRoot->addChild(rectRight);
		worldRoot->addChild(rectBottom);
		worldRoot->addChild(rectLeft);

		rectTop->setPosition( Vec3f(0, 0, 3000) );
		rectRight->setPosition( Vec3f(frame.origin_.x+frame.size_.width_, 0, 3000) );
		rectBottom->setPosition( Vec3f(0, frame.origin_.y+frame.size_.height_, 3000) );
		rectLeft->setPosition( Vec3f(0, 0, 3000) );

		viewFrames.addElement(rectTop);
		viewFrames.addElement(rectRight);
		viewFrames.addElement(rectBottom);
		viewFrames.addElement(rectLeft);

	}
}

/// <summary>
/// Starts playback of a video
/// </summary>
void VideoWidgetScene::playVideo( int index )
{
	VideoInfo* videoInfo = (VideoInfo*)videos.elementAt(index);
	currentVideo = videoInfo;
	QtRenderer::getInstance()->createVideoHandle( currentVideo );

	//********************************************************************************
	// Get Closed caption data from video
	//********************************************************************************
	S64 offset = videoInfo->sccOffset;
	printf("Closed Caption File %s with Offset %d\n", videoInfo->sccFile.str(), offset);

	noClosedCaptionFile = false;

	if( !videoInfo->sccFile.isEmpty() )
	{
		ccMgr.setOffset(offset);
		ccMgr.init(videoInfo->sccFile.str());
	}
	else
	{
		noClosedCaptionFile 	= true;
		useClosedCaption 		= false;
		findQtButton("closedCaptionDisabled")->setVisible(false);
		findQtButton("closedCaptionEnabled")->setVisible(false);
	}

	videoLength = videoInfo->videoTime*videoInfo->frameRate;
	videoNameLabel->setText(videoInfo->videoTitle);

	//********************************************************************************
	// Start playing
	//********************************************************************************
	if( currentVideo->videoHandle != 0 )
	{
		if (videoInfo->audioFile.isEmpty() )
		{
			videoHandle = videoManager.StartVideo(currentVideo->videoFile.str(), &currentVideo->videoSurface );
			if( videoHandle == 0 )
			{
				currentVideo->videoFile.replace(".OGG", ".ogg");
				videoHandle = videoManager.StartVideo(currentVideo->videoFile.str(), &currentVideo->videoSurface );
			}
		}
		else
		{
			videoHandle = videoManager.StartVideo(currentVideo->videoFile.str(), currentVideo->audioFile.str(), &currentVideo->videoSurface );

			if( videoHandle == 0 )
			{
				currentVideo->videoFile.replace(".OGG", ".ogg");
				currentVideo->audioFile.replace(".OGG", ".ogg");
				videoHandle = videoManager.StartVideo(currentVideo->videoFile.str(), &currentVideo->videoSurface );
			}
		}
	}

	isPlaying = true;
}

/// <summary>
/// Sets up hot spots -- Ugly implementation but brute fast quick, only like 5 buttons anyways
/// </summary>
void VideoWidgetScene::setupHotSpots()
{
	trackedObjects->removeAllElements();

	if( showUI )
	{
		//Track a different button if the video is paused
		if ( isPaused )
		{ trackedObjects->addElement( findQtButton("play")); }
		else
		{ trackedObjects->addElement( findQtButton("pause")); }

		//If closed caption feature is available for this video
		if( !noClosedCaptionFile )
		{
			if( useClosedCaption )
			{ trackedObjects->addElement( findQtButton("closedCaptionEnabled")); }
			else
			{ trackedObjects->addElement( findQtButton("closedCaptionDisabled")); }
		}

		//Always available
		trackedObjects->addElement( findQtButton("home"));
		trackedObjects->addElement(findQtButton("replay"));

		trackedObjects->addElement(findQtButton("fastForward"));
		trackedObjects->addElement(findQtButton("rewind"));
	}
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void VideoWidgetScene::onSelectionChange( GameObject* old, GameObject* newObj)
{
	if( old != null && newObj != null )
	{
		playSound("Base_Highlight");
		printf("Selection changed from %s to %s \n", old->name.str(), newObj->name.str() );
	}
	else if ( old != null && newObj == null )
	{  printf("Selection changed from %s to nothing. \n", old->name.str() ); }
	else if ( old == null && newObj != null )
	{  printf("Selection changed from nothing to %s. \n", newObj->name.str() ); }


	if( old != null && newObj != null )
	{
		((QtButtonObject*)old)->setState(QtButtonStateNormal);
		((QtButtonObject*)newObj)->setState(QtButtonStateActive);
		setupHotSpots();
	}
}

/// <summary>
/// Position closed captions
/// </summary>
void VideoWidgetScene::positionClosedCaptions()
{
	if( noClosedCaptionFile )
	{ return; }

	Rect videoRect = QtRenderer::getInstance()->getVideoRect();
	for(int i=0; i<closedCaptionsLabels.getSize(); ++i)
	{
		QtTextObject* lineLabel = (QtTextObject*)closedCaptionsLabels[i];

		//This logic can be collapsed
		if( captionData == null )
		{
			lineLabel->setVisible(false);
			lineLabel->setText("");
		}
		else
		{
			if( i<= captionData->currentLine && useClosedCaption )
			{ lineLabel->setVisible(true); }
			else
			{ lineLabel->setVisible(false); }
		}
	}

	if( captionData != null )
	{
		int line = 0;
		for(int i=captionData->currentLine; i>=0; i--)
		{
			QtTextObject* lineLabel = (QtTextObject*)closedCaptionsLabels[i];
			Rect bounds = QtRenderer::getInstance()->getStringBounds(captionData->text[i].c_str());

			float startY  = videoRect.origin_.y + videoRect.size_.height_ * 0.9f;
			float offsetY = bounds.size_.height_ * line;
			lineLabel->setText(captionData->text[i].c_str());
			lineLabel->setPosition( Vec3f( 640, startY - offsetY, lineLabel->getPosition().z ) );
			lineLabel->setQtColor( captionData->color[i]);
			lineLabel->isUnderlined = captionData->bUnderlined[i];
			line++;
		}
	}
}


/// <summary>
/// Update's closed captions
/// </summary>
void VideoWidgetScene::updateClosedCaptions( float dt )
{
	if( !noClosedCaptionFile )
	{ captionData = ccMgr.update(videoTime.time); }

	positionClosedCaptions();
}


/// <summary>
/// Slides in UI
/// </summary>
void VideoWidgetScene::slideUI( bool visible )
{
	printf("slideUI\n");
	showUI = visible;

	if( visible )
	{
		topBar->moveTo( Vec3f(0,0,1000), 0.5f );
		bottomBar->moveTo( Vec3f(0,720 - bottomBar->getWorldBoundingBox().getHeight(), bottomBar->getPosition().z), 0.5f );

		topPanel->moveTo( Vec3f(0,0,0), 0.5f );
		bottomPanel->moveTo( Vec3f(0,0,0), 0.5f );

		if( currentHighlight != null )
		{ ((QtButtonObject*)currentHighlight)->setState(QtButtonStateNormal); }
		currentHighlight = null;
	}
	else
	{
		topBar->moveTo( Vec3f(0,-500, 1000), 0.5f );
		bottomBar->moveTo( Vec3f(0, 900, bottomBar->getPosition().z), 0.5f );

		topPanel->moveTo( Vec3f(0,-200, 0), 0.5f );
		bottomPanel->moveTo( Vec3f(0,200, 0), 0.5f );

		findQtButton("home")->setState(QtButtonStateNormal);
		findQtButton("closedCaptionDisabled")->setState(QtButtonStateNormal);
		findQtButton("closedCaptionEnabled")->setState(QtButtonStateNormal);
		findQtButton("fastForward")->setState(QtButtonStateNormal);
		findQtButton("pause")->setState(QtButtonStateNormal);
		findQtButton("play")->setState(QtButtonStateNormal);
		findQtButton("rewind")->setState(QtButtonStateNormal);
		findQtButton("replay")->setState(QtButtonStateNormal);

	}
}


/// <summary>
/// Pause video
/// </summary>
void VideoWidgetScene::resumeVideo()
{
	printf("Resuming Video\n");
	resumeTimer = 0.5f;
	findQtButton("play")->setVisible(false);
	findQtButton("pause")->setVisible(true);
	findQtButton("pause")->setState(QtButtonStateNormal);
	isPaused = false;
}


/// <summary>
/// Pause video
/// </summary>
void VideoWidgetScene::pauseVideo()
{
	printf("Pausing Video\n");
	fastForward = 0;
	oneSecondTimer = 0;
	videoManager.PauseVideo(videoHandle);
	findQtButton("pause")->setVisible(false);
	findQtButton("play")->setVisible(true);
	findQtButton("play")->setState(QtButtonStateActive);
	isPaused = true;
	setupHotSpots();
}

/// <summary>
/// Update progress bar of video
/// </summary>
void VideoWidgetScene::updateFill( float dt )
{
	float dp = (float)videoTime.frame/(float)videoLength;
	if( dp < 0.01f ) dp = 0.01f;
	if( dp > 1 ) dp = 1;

	U16 curhr = (dp*currentVideo->videoTime)/3600;
	U16 curmin = ((int)((dp*currentVideo->videoTime)/60))%60;
	U16 cursec = ((int)(dp*currentVideo->videoTime))%60;

	QtSpriteObject* progressBar = findQtSprite("middleProgress");
	progressBar->setClipRectBasedOnPercent(dp);
	Box3f screenBounds = progressBar->getScreenBoundingBox(NULL);

	GameObject* progressRight = findQtSprite("rightProgress");
	progressRight->setPosition( Vec3f( screenBounds.getRight(), progressRight->getPosition().y, progressRight->getPosition().z ) );

	if( isPaused )
	{ statusLabel->setText("Paused"); }
	else if ( nextMovieTimer > 0 )
	{
		int timeLeft = (int)nextMovieTimer;
		VideoInfo* nextVideo = (VideoInfo*)videos.elementAt(currentVideoIndex+1);
		if( nextVideo != null )
		{ statusLabel->setText(String("Playing ") + nextVideo->videoTitle.str() + String(" in ") + String(timeLeft)); }
	}
	else if ( !isPlaying )
	{
		statusLabel->setText("Finished");

		if( replayTimer < 0 )
		{
			cleanUpAfterVideo(currentVideo);
			worldRoot->setVisible(false);

			if( lastUser != null )
			{
				CActivityExit activityExit(0, CActivityExit::kReasonComplete);
				playerLog->Append(*(dynamic_cast<CLogData*>(&activityExit)));
			}

			QVariantMap responseMap;
			responseMap.insert("KeyboardText", "I entered my name");
			GlasgowApp::appInstance->mainState->quit(responseMap);
			return;
		}
	}
	else if  ( fastForward == 2 )
	{ statusLabel->setText("Fast Forward >>"); }
	else if  ( fastForward == -2 )
	{ statusLabel->setText("<< Rewind"); }
	else
	{ statusLabel->setText(""); }

	char timeString[10];
	sprintf(timeString, "%02d:%02d:%02d", curhr, curmin, cursec);
	currentTimeLabel->setText(timeString);

	U16 hr = currentVideo->videoTime/3600;
	U16 min = ((int)(currentVideo->videoTime/60))%60;
	U16 sec = ((int)currentVideo->videoTime%60);
	sprintf(timeString, "%02d:%02d:%02d", hr, min, sec);
	totalTimeLabel->setText(timeString);
}

/// <summary>
/// Returns the object representing by a no selection -> selected state chnage
/// </summary>
GameObject* VideoWidgetScene::getDefaultHighLight()
{
	if( !showUI && currentVideo != null && videoManager.IsVideoPlaying(videoHandle))
	{
		slideUI(true);
		currentHighlight = findQtButton("pause");
		findQtButton("pause")->setState(QtButtonStateActive);
		return findQtButton("pause");
	}

	return NULL;
}

/// <summary>
/// Over ridden to deal with the render chain of qtrenderer, normally we have scene graph objects for this
/// </summary>
void VideoWidgetScene::postRender()
{
	updateClosedCaptions(0);

	if( oneSecondTimer == 0 )
	{ updateFill(0); }
	QtRenderer::getInstance()->swapBuffers();
	super::postRender();
}

/// <summary>
/// Call's leap required api when a video is halfway through
/// </summary>
void VideoWidgetScene::logTitleStart()
{
	if( lastUser != null )
	{
		CMetaInfo metaInfo(rootVideoPackagePath.str());
		CTitleStart titleStart(
				metaInfo.GetProductId(),
				const_cast<char *>(metaInfo.GetPartNumber().c_str()),
				const_cast<char *>(metaInfo.GetPackageVersion().c_str()), 1);
		playerLog->Append(*(dynamic_cast<CLogData*>(&titleStart)));
	}
}

/// <summary>
/// Call's leap required api when a video is halfway through
/// </summary>
void VideoWidgetScene::logVideoWatched()
{
	if( lastUser != null )
	{
		CCorrectAnswer caLog(1, NULL, NULL);
		playerLog->Append(*(dynamic_cast<CLogData*>(&caLog)));
		watchedEnough = true;
	}
}

/// <summary>
/// Over ridden so we can ask for video manager
/// </summary>
void VideoWidgetScene::update(float dt )
{
	if( videoHandle != 0 )
	{
		//************************************************************************************************************
		// If video is currently in play mode,  we could have an active fast forward
		//************************************************************************************************************
		if( videoManager.IsVideoPlaying( videoHandle ) )
		{
			videoManager.GetVideoTime(videoHandle, &videoTime);
			playingLastFrame = true;

			if( !watchedEnough )
			{
				float percentage = (float)videoTime.frame/(float)videoLength;
				if ( videoTime.time >= 0.5f )
				{ logVideoWatched(); }
			}

			if( fastForward > 0  )
			{
				if( oneSecondTimer >= 0 )
				{
					oneSecondTimer-= dt;
					if( oneSecondTimer < 0 )
					{
						oneSecondTimer = 0.5f;
						jumpForward(0.05f);
						updateFill(0);
					}
				}
			}
			else if ( fastForward < 0 )
			{
				if( oneSecondTimer >= 0 )
				{
					oneSecondTimer-= dt;
					if( oneSecondTimer < 0 )
					{
						oneSecondTimer = 0.5f;
						jumpForward(-0.05f);
						updateFill(0);
					}
				}
			}
		}

		//************************************************************************************************************
		// Video is paused, our resume is on a timer due to UI animations
		//************************************************************************************************************
		if( videoManager.IsVideoPaused(videoHandle) && resumeTimer > 0 )
		{
			resumeTimer -= dt;
			if( resumeTimer <= 0 )
			{
				videoManager.ResumeVideo(videoHandle);
			}
		}

		//************************************************************************************************************
		// We are not playing, we could have stopped legitimately if we were playing on the last frame
		// Otherwise we are just ticking in a stopped state in which case we should move to the next video if bundled
		//************************************************************************************************************
		if( !videoManager.IsVideoPlaying(videoHandle) )
		{
			if ( playingLastFrame )
			{
				//Turn off playing or fast forwarding
				isPlaying = false;
				fastForward = 0;

				//Slide in UI
				if( !showUI )
				{ slideUI(true); }

				//Set the timer to start next video if there is another one
				if( currentVideoIndex+1 < videos.getSize() )
				{ nextMovieTimer = 8; }

				currentHighlight = findQtButton("home");
				findQtButton("home")->setState(QtButtonStateActive);
				setupHotSpots();

				//Call Stop and null out video handle
				videoManager.StopVideo(videoHandle);
				videoHandle = 0;

				//video has stopped
				playingLastFrame = false;
			}
		}
	}

	if( replayTimer > 0 )
	{
		replayTimer -= dt;
		if( replayTimer <= 0 )
		{
			playVideo(currentVideoIndex);
		}
	}


	if( nextMovieTimer > 0 )
	{
		nextMovieTimer -= dt;
		if( nextMovieTimer <= 0 )
		{ playNextMovie(); }
	}

	super::update(dt);
}

/// <summary>
/// Deletes view frame, and contexts from qtrenderer
/// </summary>
void VideoWidgetScene::cleanUpAfterVideo(VideoInfo* info)
{
	printf("VideoWidgetScene::cleanUpAfterVideo\n");
	if( info == null )
		return;

	videoManager.StopVideo(videoHandle);
	videoHandle = 0;
	for( int i = 0;i<viewFrames.getSize(); i++ )
	{
		GameObject* remove = (GameObject*)viewFrames[i];
		remove->removeFromParent();
	}

	viewFrames.removeAllElements();
	QtRenderer::getInstance()->cleanupVideoSurface(info);

}

/// <summary>
/// Call to play next movie
/// </summary>
void VideoWidgetScene::playNextMovie()
{
	slideUI(false);
	videoManager.StopVideo(videoHandle);
	currentVideoIndex++;
	cleanUpAfterVideo( currentVideo );
	playVideo(currentVideoIndex);
	addViewFrame( QtRenderer::getInstance()->getVideoRect() );
	watchedEnough = false;
}

/// <summary>
/// Fast forwards or rewinds a given percent [-1,0,1]
/// </summary>
void VideoWidgetScene::jumpForward(float percent)
{
	//Complete replay

	float dp = (float)videoTime.frame/(float)videoLength;
	dp += percent;

	if( dp < 0 )
	{ dp = 0; }
	if( dp > 1 )
	{ dp = 1; }

	int frameBefore = videoTime.frame;
	videoTime.frame = (int)videoLength * dp;

	if ( frameBefore <= videoTime.frame )
	{ videoTime.frame++; }

	videoManager.SeekVideoKeyFrame( videoHandle, &videoTime, true);

	if( dp <= 0.05f || videoTime.frame <= 0 )
	{
		videoTime.frame = 0;
		fastForward = 0;
		oneSecondTimer = 0;
	}
	else if( dp >= 0.95f || videoTime.frame >= videoLength )
	{
		videoTime.frame = videoLength;
		fastForward = 0;
		oneSecondTimer = 0;
	}

	replayTimer = 0;
	nextMovieTimer = 0;
}

void VideoWidgetScene::onButtonPressBack()
{
	nextMovieTimer = 0;
	fastForward = 0;

	if ( showUI )
	{
		slideUI(false);
		setupHotSpots();
	}
	else
	{
		pauseVideo();
		slideUI(true);
		setupHotSpots();
		currentHighlight = findQtButton("play");
		findQtButton("play")->setState(QtButtonStateActive);
	}

	super::onButtonPressBack();
}

/// <summary>
/// A button on controller was pressed, to reduce size of function
/// each state has its own delegate function for handling
/// </summary>
void VideoWidgetScene::onButtonPressAction()
{
	printf("Button Pressed\n");
	if( !showUI )
	{
		pauseVideo();
		slideUI(true);
		setupHotSpots();
		currentHighlight = findQtButton("play");
		return;
	}

	if( currentHighlight != null && currentHighlight->name == "pause" )
	{
		pauseVideo();
		setupHotSpots();
		currentHighlight = findQtButton("play");
		return;
	}
	else if( currentHighlight != null && currentHighlight->name == "rewind" )
	{
		if( videoManager.IsVideoPaused( videoHandle ) )
		{
			printf("rewind pushed\n");
			jumpForward( -0.1f );
			updateFill(0);
			return;
		}
		else
		{
			if( fastForward == -2 )
			{
				printf("Active rewind stopped\n");
				fastForward = 0;
				oneSecondTimer = 0;
			}
			else
			{
				printf("Active rewind set to 2x\n");
				fastForward = -2;
			}
		}
	}
	else if( currentHighlight != null && currentHighlight->name == "fastForward" )
	{
		if( videoManager.IsVideoPaused( videoHandle ) )
		{
			printf("fastForward pushed\n");
			jumpForward( 0.1f );
			updateFill(0);
			return;
		}
		else
		{
			if( fastForward == 2 )
			{
				printf("Active fastForward stopped\n");
				fastForward = 0;
				oneSecondTimer = 0;
			}
			else
			{
				printf("Active fastForward set to 2x\n");
				fastForward = 2;
			}
		}
	}
	else if( currentHighlight != null && currentHighlight->name == "play" )
	{
		resumeVideo();
		if( showUI )
		{ slideUI(false); }

		setupHotSpots();
		currentHighlight = null;
		return;
	}
	else if( currentHighlight != null && currentHighlight->name == "replay" )
	{
		videoManager.StopVideo(videoHandle);
		cleanUpAfterVideo( currentVideo );
		playVideo(currentVideoIndex);
		nextMovieTimer = 0;
		slideUI(false);
		setupHotSpots();
		currentHighlight = null;
		return;
	}
	//enable closed captions
	else if( currentHighlight != null && currentHighlight->name == "closedCaptionDisabled" )
	{
		useClosedCaption = true;

		printf("Turning closed captions on\n");
		findQtButton("closedCaptionEnabled")->setVisible(true);
		findQtButton("closedCaptionDisabled")->setVisible(false);
	}
	//disable closed captions
	else if( currentHighlight != null && currentHighlight->name == "closedCaptionEnabled" )
	{
		useClosedCaption = false;
		printf("Turning closed captions off\n");
		findQtButton("closedCaptionEnabled")->setVisible(false);
		findQtButton("closedCaptionDisabled")->setVisible(true);
	}
	else if( currentHighlight != null && currentHighlight->name == "home" )
	{
		cleanUpAfterVideo(currentVideo);
		worldRoot->setVisible(false);

		if( lastUser != null )
		{
			CActivityExit activityExit(0, CActivityExit::kReasonComplete);
			playerLog->Append(*(dynamic_cast<CLogData*>(&activityExit)));
		}

		QVariantMap responseMap;
		responseMap.insert("KeyboardText", "I entered my name");
		GlasgowApp::appInstance->mainState->quit(responseMap);
	}

	super::onButtonPressAction();
}



}

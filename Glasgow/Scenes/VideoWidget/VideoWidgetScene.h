#ifndef VIDEOWIDGETSCENE_H_
#define VIDEOWIDGETSCENE_H_

#include "SIEngine/Core/Scene.h"
#include "SIEngine/Core/User.h"
#include "SIEngine/Include/SIControls.h"
#include "Glasgow/Data/ClosedCaption.h"
#include "SIEngine/Objects/QtButtonObject.h"
#include "SIEngine/Objects/QtTextObject.h"
#include "Glasgow/Renderables/QtRectObject.h"
#include <LF/AudioMPI.h>
#include <VideoMPI.h>
#include <MultiProfile/PlayerLog.h>
namespace Glasgow
{

class VideoWidgetScene : public Scene
{
	CLASSEXTENDS( VideoWidgetScene, Scene );

public:
	VideoWidgetScene();
	virtual ~VideoWidgetScene();

	/// <summary>
	/// Handle analog changes
	/// </summary>
	virtual void onSelectionChange( GameObject* old, GameObject* newObj);

	/// <summary>
	/// A button on controller was pressed, to reduce size of function
	/// each state has its own delegate function for handling
	/// </summary>
	virtual void onButtonPressAction();
	virtual void onButtonPressBack();

	/// <summary>
	/// Starts playback of a video
	/// </summary>
	void playVideo( int index );

	/// <summary>
	/// Over ridden so we can ask for video manager
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Over ridden to deal with the render chain of qtrenderer, normally we have scene graph objects for this
	/// </summary>
	virtual void postRender();

	/// <summary>
	/// Sets up hot spots
	/// </summary>
	void setupHotSpots();

	/// <summary>
	/// Returns the object representing by a no selection -> selected state chnage
	/// </summary>
	virtual GameObject* getDefaultHighLight();

	/// <summary>
	/// Adds a view frame
	/// </summary>
	void addViewFrame(Rect frame);

	/// <summary>
	/// Update's closed captions
	/// </summary>
	void updateClosedCaptions( float dt );

	/// <summary>
	/// Update progress bar of video
	/// </summary>
	void updateFill( float dt );

	/// <summary>
	/// Slides in UI
	/// </summary>
	void slideUI( bool visible );

	/// <summary>
	/// Pause video
	/// </summary>
	void pauseVideo();

	/// <summary>
	/// Pause video
	/// </summary>
	void resumeVideo();

	/// <summary>
	/// Position closed captions
	/// </summary>
	void positionClosedCaptions();

	/// <summary>
	/// Call's leap required api when a video is halfway through
	/// </summary>
	void logTitleStart();

	/// <summary>
	/// Call's leap required api when a video is halfway through
	/// </summary>
	void logVideoWatched();

	/// <summary>
	/// Fills in all scene graph references
	/// </summary>
	void fillReferences();

	/// <summary>
	/// Sets widget in default launch state
	/// </summary>
	void setupDefaultState();

	/// <summary>
	/// Fast forwards or rewinds a given percent [-1,0,1]
	/// </summary>
	void jumpForward(float percent);

	/// <summary>
	/// Deletes view frame, and contexts from qtrenderer
	/// </summary>
	void cleanUpAfterVideo(VideoInfo* info);

	/// <summary>
	/// Call to play next movie
	/// </summary>
	void playNextMovie();

	/// <summary>
	/// Helper cast find
	/// </summary>
	QtSpriteObject* findQtSprite(String name );

	/// <summary>
	/// Helper cast find
	/// </summary>
	QtButtonObject* findQtButton(String name);

private:
	ClosedCaption ccMgr;
	ClosedCaption::CaptionData *captionData;

	String			videoPackageID;
	float 			resumeTimer;
	QtRectObject*	topBar;
	QtRectObject*	bottomBar;
	VideoInfo*		currentVideo;
	String			rootVideoPackagePath;
	ObjectArray 	videos;
	tVideoTime 		videoTime;
	tVideoHndl		videoHandle;
	CVideoMPI		videoManager;
	ObjectArray		viewFrames;
	bool			watchedEnough;

	//THESE SHOULD BE ENUMS!!!!!!!!
	float 			replayTimer;
	float 			nextMovieTimer;
	float			videoLength;
	int 			currentVideoIndex;
	bool 			playingLastFrame;
	bool			isPlaying;
	bool 			useClosedCaption;
	bool			noClosedCaptionFile;
	bool			isPaused;
	bool			showUI;
	int 			lastCCPosY;
	float 			fastForward;
	float 			oneSecondTimer;
	float 			rewind;

	ObjectArray     closedCaptionsLabels;
	QtTextObject*	statusLabel;
	QtTextObject*	currentTimeLabel;
	QtTextObject*	totalTimeLabel;
	QtTextObject*	videoNameLabel;
	QtTextObject*	backLabel;


	GameObject* 	topPanel;
	GameObject*		bottomPanel;
	PlayerLog*		playerLog;
	User* 			lastUser;
};

}
#endif

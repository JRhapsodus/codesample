#include "SongsPage.h"

namespace Glasgow
{

SongsPage::SongsPage(GameObject* rootObject) : super(rootObject)
{
	audioSlider = (SliderObject*)find("audioSlider");
	//audioSlider->setupSlider(SliderObject::SliderTypeAudio);

}

SongsPage::~SongsPage()
{
	printf("SongsPage::~SongsPage\n");
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool SongsPage::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	//*****************************************************************
	// Forward to slider, since the slider can move on analog change
	// we might need to update the selections, don't allow outside scene
	//*****************************************************************
	if ( audioSlider->onAnalogChange( objectLeft, objectEntered ) )
	{
		currentHighlight = audioSlider->getSelectedItem();
		if( currentHighlight != NULL && currentHighlight->typeOf(ButtonObject::type()) )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
		return true;
	}
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void SongsPage::addHotSpots( ObjectArray* trackedObjects )
{
	trackedObjects->addElement(findButton("backButton"));
	audioSlider->addObjectsToTracking( trackedObjects );
}

/// <summary>
/// Handle hub feature
/// </summary>
void SongsPage::cameraArrived()
{
	audioSlider->setPosition( Vec3f(0,-15,1) );
	audioSlider->animateInitialEntry();
	audioSlider->setOpacity(0);
	audioSlider->waitFor(1.8f);
	audioSlider->fadeTo(1, 0.25f);
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void SongsPage::cameraLeaving()
{
	audioSlider->cancelActions();
	audioSlider->moveBy(Vec3f(0, 400, -200), 1.55f);
	audioSlider->moveBy(Vec3f(0, -400, 200), 0.01f);
	super::cameraLeaving();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* SongsPage::getArrivalSelection()
{ return audioSlider->getSelectedItem(); }

/// <summary>
/// Convenience function that returns the appropriate slider's selected Item casted
/// </summary>
AudioItem* SongsPage::getSelectedAudio()
{ return (AudioItem*)audioSlider->getSelectedItem(); }


} /* namespace Glasgow */

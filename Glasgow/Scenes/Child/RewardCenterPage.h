#ifndef REWARDCENTERPAGE_H_
#define REWARDCENTERPAGE_H_

#include "ChildPage.h"

namespace Glasgow
{

class RewardCenterPage : public ChildPage
{
	CLASSEXTENDS(RewardCenterPage, ChildPage);
public:
	RewardCenterPage(GameObject* rootObject);
	virtual ~RewardCenterPage();

	/// <summary>
	/// Add our tracked items to the scene list
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackedList );

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Sets a random encouragement in the label
	/// </summary>
	void setRandomEncouragement(String userName);

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

private:
	GameObject*				rewardCenterTray;
	String					encouragements[10];
};

} /* namespace Glasgow */
#endif /* REWARDCENTERPAGE_H_ */

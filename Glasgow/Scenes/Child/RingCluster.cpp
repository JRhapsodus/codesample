#include "RingCluster.h"

namespace Glasgow
{

RingCluster::RingCluster(int id, Color rim ) : super()
{
	rimColor = rim;
	clusterID = id;
	distanceFromCenter = 0;
}

RingCluster::~RingCluster()
{
	printf("RingCluster::~RingCluster()\n");
}


/// <summary>
/// Returns the content item representing the package id
/// </summary>
ContentItem* RingCluster::getPackageID( String packageID )
{
	for ( int i = 0 ;i<contentItems.getSize(); i++ )
	{
		ContentItem* item = (ContentItem*)contentItems[i];
		PackageInfo* info = item->getPackageInfo();

		if( info->packageID.equals(packageID) )
		{ return item; }
	}

	return null;
}

/// <summary>
/// Returns true if this cluster has the package Id
/// </summary>
bool RingCluster::hasPackageID(String packageID)
{ return (getPackageID(packageID)!=null); }

/// <summary>
/// Staggers out content items
/// </summary>
void RingCluster::staggerOutForLaunch(float delay)
{
	float waitTime = delay;
	for( int i = 0; i<contentItems.getSize(); i++ )
	{
		((ContentItem*)contentItems[i])->cancelActions();
		((ContentItem*)contentItems[i])->waitFor(waitTime);
		((ContentItem*)contentItems[i])->scaleTo(Vec3f::Zero, 0.25f);
		waitTime += 0.1f;
	}
}

/// <summary>
/// Staggers in content items
/// </summary>
void RingCluster::staggerInForCancelLaunch()
{
	for( int i = 0; i<contentItems.getSize(); i++ )
	{
		((ContentItem*)contentItems[i])->cancelActions();
		((ContentItem*)contentItems[i])->bubbleIn(0);
	}
}

/// <summary>
/// Staggers in content items
/// </summary>
void RingCluster::staggerInContentRings(float delay)
{
	float waitTime = delay;
	for( int i = 0; i<contentItems.getSize(); i++ )
	{
		((ContentItem*)contentItems[i])->setRingColor(rimColor);
		((ContentItem*)contentItems[i])->bubbleIn(waitTime);
		waitTime += 0.1f;
	}
}

/// <summary>
/// Removes any cartridge items from the list
/// </summary>
void RingCluster::removeCartridgeItems()
{
	for( int i = 0; i<contentItems.getSize(); i++ )
	{
		ContentItem* contentItem = (ContentItem*)contentItems[i];
		if( contentItem->getPackageInfo()->isCartridge() )
		{
			contentItem->removeFromParent();
			contentItems.removeElementAt(i);
			break;
		}
	}

	setNaturalPosition( distanceFromCenter );
}

/// <summary>
/// Staggers out content items
/// </summary>
void RingCluster::staggerOutContentRings(float delay)
{
	float waitTime = delay;
	for( int i = 0; i<contentItems.getSize(); i++ )
	{
		((ContentItem*)contentItems[i])->bubbleOut(waitTime);
		waitTime += 0.1f;
	}
}


void RingCluster::changeColor( Color rim)
{
	rimColor = rim;
	for( int i = 0; i<contentItems.getSize(); i++ )
	{ ((ContentItem*)contentItems[i])->changeRingColor(rim); }
}

/// <summary>
/// Returns if the item is in this cluster
/// </summary>
bool RingCluster::contains(ContentItem* item)
{
	for( int i = 0; i<contentItems.getSize(); i++ )
	{
		if( contentItems[i] == item )
		{ return true; }
	}

	return false;
}


/// <summary>
/// Adds our items to the tracking list
/// </summary>
void RingCluster::addItemsToTracking( ObjectArray* trackedList )
{
	for( int i = 0; i<contentItems.getSize(); i++ )
	{ trackedList->addElement( getItem(i)); }
}


/// <summary>
/// Clears items from out data struct
/// </summary>
void RingCluster::clearItems()
{ contentItems.removeAllElements(); }

/// <summary>
/// Custom update to always face the camera
/// </summary>
void RingCluster::update(float dt )
{
	//***************************************************************
	// Lookat the backward's direction torwards the camera
	//***************************************************************
	Vec3f targetLocation = Vec3f::Back;
	Vec3f localTarget = ((GameObject*)parent)->transform.getWorldToLocal()*targetLocation;
	Vec3f localUp = ((GameObject*)parent)->transform.getWorldToLocal()*Vec3f::Up;
	Quaternion quat = Quaternion::lookRotation(localTarget, localUp);
	quat *= Quaternion::angleAxis(180, Vec3f::Up);
	transform.setRotation(quat);

	super::update(dt);

	//value from 0 -> a
	Vec3f worldPos = getWorldPosition();
	float value = ((worldPos.z+300)/1200.0f);
	value = (value<0)?0:value;
	value = (value>1)?1:value;

	static float maxDisplacementX = 300;
	static float maxDisplacementY = 350;
	float displacementX = (-1 * (fabs(2*value - 1) -1)) * maxDisplacementX;
	float displacementY = maxDisplacementY*value*value;

	//right side
	Vec3f offset = Vec3f::Zero;

	if ( worldPos.x > 0 )
	{ offset = (Vec3f::Right * displacementX) + (Vec3f::Up*displacementY); }
	//left side
	else
	{ offset = (Vec3f::Left * displacementX) + (Vec3f::Up*displacementY); }

	if( getParent() != null )
	{
		Vec3f localOffset = getParent()->transform.getWorldToLocal() * offset;
		setPosition( originalPos + localOffset );

		updateContentItemDisplacement(dt, (-1 * (fabs(2*value - 1) -1)));
	}
}

/// <summary>
/// Updates the relative position of the rings around rotation, value represents the position
/// along the ring that the items sit, 0 being up front, 0.5 to being on the sides and 0 being far back
/// </summary>
void RingCluster::updateContentItemDisplacement(float dt, float value)
{
	int items = getNumItems();

	if( items == 1 )
	{
		//do nothing
	}
	else if ( items == 2 )
	{
		if( getItem(0) == null || getItem(1) == null )
			return;

		//id 0 goes forward to right, id 1 goes left and back
		Vec3f worldPos = getWorldPosition();
		Vec3f maxDisplacent1 = Vec3f( 10, 0, 50 );
		Vec3f maxDisplacent2 = Vec3f( -10, 0, -50 );

		if ( worldPos.x > 0 )
		{
			maxDisplacent1 = Vec3f( 10, 0, -50 );
			maxDisplacent2 = Vec3f( -10, 0, 50 );
		}

		Vec3f currentDisplacement1 = maxDisplacent1 * value;
		Vec3f currentDisplacement2 = maxDisplacent2 * value;

		Vec3f localOffset1 = transform.getWorldToLocal() * currentDisplacement1;
		Vec3f localOffset2 = transform.getWorldToLocal() * currentDisplacement2;

		getItem(0)->clusterOffset = localOffset1;
		getItem(1)->clusterOffset = localOffset2;

	}
	else if ( items == 3 )
	{
		if( getItem(0) == null || getItem(1) == null || getItem(2) == null)
			return;

		//id 0 goes down, 1 goes forward nad left, 2 goes back and right
		Vec3f worldPos = getWorldPosition();
		Vec3f maxDisplacent1 = Vec3f( 0, -15, 0 );
		Vec3f maxDisplacent2 = Vec3f( 120, 0, -75 );
		Vec3f maxDisplacent3 = Vec3f( -120, 0, 75 );

		if ( worldPos.x > 0 )
		{
			maxDisplacent1 = Vec3f( 0, -15, 0 );
			maxDisplacent2 = Vec3f( 120, 0, 75 );
			maxDisplacent3 = Vec3f( -120, 0, -75 );
		}

		Vec3f currentDisplacement1 = maxDisplacent1 * value;
		Vec3f currentDisplacement2 = maxDisplacent2 * value;
		Vec3f currentDisplacement3 = maxDisplacent3 * value;

		Vec3f localOffset1 = transform.getWorldToLocal() * currentDisplacement1;
		Vec3f localOffset2 = transform.getWorldToLocal() * currentDisplacement2;
		Vec3f localOffset3 = transform.getWorldToLocal() * currentDisplacement3;

		getItem(0)->clusterOffset = localOffset1;
		getItem(1)->clusterOffset = localOffset2;
		getItem(2)->clusterOffset = localOffset3;
	}
}

/// <summary>
/// Returns a content Item if this cluster has one representing the cartridge
/// otherwise it will return null
/// </summary>
ContentItem* RingCluster::getCartridgeItem()
{
	for ( int i = 0 ;i<contentItems.getSize(); i++ )
	{
		ContentItem* item = (ContentItem*)contentItems[i];
		PackageInfo* info = item->getPackageInfo();
		if( info != null && info->isCartridge() )
		{ return item; }
	}

	return null;
}


/// <summary>
/// Convenience cast call
/// </summary>
ContentItem* RingCluster::getItem( int index )
{ return (ContentItem*)contentItems[index]; }


/// <summary>
/// Get number of items
/// </summary>
ContentItem* RingCluster::getRightItem()
{
	int items = getNumItems();
	if ( items == 1 )
	{ return getItem(0); }
	else if ( items == 2 )
	{ return getItem(1); }
	else if ( items == 3 )
	{ return getItem(2); }
}

/// <summary>
/// Get number of items
/// </summary>
ContentItem* RingCluster::getMiddleItem()
{
	if (contentItems.getSize() > 0 )
	{ return getItem(0); }
	else
		return null;
}

/// <summary>
/// Get number of items
/// </summary>
ContentItem* RingCluster::getLeftItem()
{
	int items = getNumItems();
	if ( items == 1 )
	{ return getItem(0); }
	else if ( items == 2 )
	{ return getItem(0); }
	else if ( items == 3 )
	{ return getItem(1); }
}



/// <summary>
/// Tells the ring page how to layout its content items, should this just be dynamic via ::update?
/// and base the positions based on the world front Z of the ring ?
/// </summary>
void RingCluster::setNaturalPosition( int distance )
{
	distanceFromCenter = distance;
	int itemCount = contentItems.getSize();

	if( itemCount == 0 )
		return;

	Vec3f worldPos = getWorldPosition();
	float value = ((worldPos.z+300)/1200.0f);
	value = (value<0)?0:value;
	value = (value>1)?1:value;
	updateContentItemDisplacement(0, (-1 * (fabs(2*value - 1) -1)));

	//************************************************************************************************************************
	// This ring is moving into the center! position elements as if to be displayed on screen
	//************************************************************************************************************************
	if( distance == 0 )
	{
		if( itemCount == 1 )
		{
			getItem(0)->setPosition( Vec3f(0, 30, 0));
		}
		else if ( itemCount == 2 )
		{
			getItem(0)->setPosition( Vec3f(-110, 30, 0));
			getItem(1)->setPosition( Vec3f(110, 30, 0));
		}
		else
		{
			getItem(0)->setPosition( Vec3f(0, 100, 0));
			getItem(1)->setPosition( Vec3f(-175, 0, 0));
			getItem(2)->setPosition( Vec3f(175, 0, 0));
		}
	}
	//************************************************************************************************************************
	// This is either to the left or right of the center ring, so lets specially position these for 'beauty'
	//************************************************************************************************************************
	else if( distance == 1 )
	{
		if( itemCount == 1 )
		{
			getItem(0)->setPosition( Vec3f(0, 30, 0));
		}
		else if ( itemCount == 2 )
		{
			getItem(0)->setPosition( Vec3f(-110, 30, 0) );
			getItem(1)->setPosition( Vec3f(110, 30, 0) );
		}
		else
		{
			getItem(0)->setPosition( Vec3f(0, 100, 0) );
			getItem(1)->setPosition( Vec3f(-175, 0, 0) );
			getItem(2)->setPosition( Vec3f(175, 0, 0) );
		}
	}
	//************************************************************************************************************************
	// Cluster is off the distance 'in the peripheal' so items should move higher up and z depth around for fun
	//************************************************************************************************************************
	else
	{
		if( itemCount == 1 )
		{
			getItem(0)->setPosition( Vec3f(0, 30, 0) );
		}
		else if ( itemCount == 2 )
		{
			getItem(0)->setPosition( Vec3f(-110, 30, 0) );
			getItem(1)->setPosition( Vec3f(110, 30, 0) );
		}
		else
		{
			getItem(0)->setPosition( Vec3f(0, 100, 0) );
			getItem(1)->setPosition( Vec3f(-175, 0, 0) );
			getItem(2)->setPosition( Vec3f(175, 0, 0) );
		}
	}
}

/// <summary>
/// Tells the ring page how to layout its content items, should this just be dynamic via ::update?
/// and base the positions based on the world front Z of the ring ?
/// </summary>
void RingCluster::moveNaturalPosition( int distance, float anim )
{
	distanceFromCenter = distance;
	int itemCount = contentItems.getSize();

	if( itemCount == 0 )
		return;

	Vec3f worldPos = getWorldPosition();
	float value = ((worldPos.z+300)/1200.0f);
	value = (value<0)?0:value;
	value = (value>1)?1:value;
	updateContentItemDisplacement(0, (-1 * (fabs(2*value - 1) -1)));

	//************************************************************************************************************************
	// This ring is moving into the center! position elements as if to be displayed on screen
	//************************************************************************************************************************
	if( distance == 0 )
	{
		if( itemCount == 1 )
		{
			getItem(0)->moveTo( Vec3f(0, 30, 0), anim);
		}
		else if ( itemCount == 2 )
		{
			getItem(0)->moveTo( Vec3f(-110, 30, 0), anim);
			getItem(1)->moveTo( Vec3f(110, 30, 0), anim);
		}
		else
		{
			getItem(0)->moveTo( Vec3f(0, 100, 0), anim);
			getItem(1)->moveTo( Vec3f(-175, 0, 0), anim);
			getItem(2)->moveTo( Vec3f(175, 0, 0), anim);
		}
	}
	//************************************************************************************************************************
	// This is either to the left or right of the center ring, so lets specially position these for 'beauty'
	//************************************************************************************************************************
	else if( distance == 1 )
	{
		if( itemCount == 1 )
		{
			getItem(0)->moveTo( Vec3f(0, 30, 0), anim);
		}
		else if ( itemCount == 2 )
		{
			getItem(0)->moveTo( Vec3f(-110, 30, 0), anim );
			getItem(1)->moveTo( Vec3f(110, 30, 0), anim );
		}
		else
		{
			getItem(0)->moveTo( Vec3f(0, 100, 0), anim );
			getItem(1)->moveTo( Vec3f(-175, 0, 0), anim );
			getItem(2)->moveTo( Vec3f(175, 0, 0), anim );
		}
	}
	//************************************************************************************************************************
	// Cluster is off the distance 'in the peripheal' so items should move higher up and z depth around for fun
	//************************************************************************************************************************
	else
	{
		if( itemCount == 1 )
		{
			getItem(0)->moveTo( Vec3f(0, 30, 0), anim );
		}
		else if ( itemCount == 2 )
		{
			getItem(0)->moveTo( Vec3f(-110, 30, 0), anim );
			getItem(1)->moveTo( Vec3f(110, 30, 0), anim );
		}
		else
		{
			getItem(0)->moveTo( Vec3f(0, 100, 0), anim );
			getItem(1)->moveTo( Vec3f(-175, 0, 0), anim );
			getItem(2)->moveTo( Vec3f(175, 0, 0), anim );
		}
	}
}

/// <summary>
/// Get number of items
/// </summary>
int RingCluster::getNumItems()
{ return contentItems.getSize(); }

/// <summary>
/// Adds a content item to this ring page
/// </summary>
void RingCluster::addContentItem( ContentItem* item )
{
	printf("RingCluster::addContentItem\n");
	contentItems.addElement(item); //addRefs
	addChild( item ); //addrefs2
	item->setPosition( Vec3f::Zero );
}


} /* namespace Glasgow */

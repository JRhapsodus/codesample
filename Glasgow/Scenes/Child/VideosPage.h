#ifndef VIDEOSPAGE_H_
#define VIDEOSPAGE_H_

#include "ChildPage.h"

namespace Glasgow
{

class VideosPage : public ChildPage
{
	CLASSEXTENDS(VideosPage, ChildPage);

public:
	VideosPage(GameObject* objectRoot);
	virtual ~VideosPage();

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& highlight);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Custom response to pressaction
	/// </summary>
	virtual bool handlePressAction(GameObject*& currentHighlight);

	/// <summary>
	/// Notification for camera arriving
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	void setPackageInfo(PackageInfo* info);

	int getVideoIndex();
private:
	PackageInfo*			packageInfo;
	SliderObject*			gallerySlider;
	SpriteObject*			galleryDarken;
	SpriteObject*			galleryFrame;
	TextLabel*				videoNameLabel;
	TextLabel*				episodeNameLabel;
};

}
#endif

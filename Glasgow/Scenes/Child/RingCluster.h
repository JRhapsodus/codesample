#ifndef RINGPAGE_H_
#define RINGPAGE_H_

#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Include/SICollections.h"

namespace Glasgow
{

class RingCluster : public GameObject
{
	CLASSEXTENDS(RingCluster, GameObject);
	ADD_TO_CLASS_MAP;
public:
	RingCluster(int id, Color rim);
	virtual ~RingCluster();

	/// <summary>
	/// Adds our items to the tracking list
	/// </summary>
	void addItemsToTracking( ObjectArray* trackedList );

	/// <summary>
	/// Adds a content item to this ring page
	/// </summary>
	void addContentItem( ContentItem* item );

	/// <summary>
	/// Tells the ring page how to layout its content items, should this just be dynamic via ::update?
	/// and base the positions based on the world front Z of the ring ?
	/// </summary>
	void moveNaturalPosition( int distance, float anim );

	/// <summary>
	/// Tells the ring page how to layout its content items
	/// </summary>
	void setNaturalPosition( int distance );

	/// <summary>
	/// Staggers in content items
	/// </summary>
	void staggerInContentRings(float delay);

	/// <summary>
	/// Staggers out content items
	/// </summary>
	void staggerOutForLaunch(float delay);

	/// <summary>
	/// Staggers in content items
	/// </summary>
	void staggerInForCancelLaunch();

	/// <summary>
	/// Returns if the item is in this cluster
	/// </summary>
	bool contains(ContentItem* item);

	/// <summary>
	/// Staggers out content items
	/// </summary>
	void staggerOutContentRings(float delay);

	/// <summary>
	/// Changes the color of the rim
	/// </summary>
	void changeColor( Color rim);

	/// <summary>
	/// Clears items from out data struct
	/// </summary>
	void clearItems();

	/// <summary>
	/// Removes any cartridge items from the list
	/// </summary>
	void removeCartridgeItems();

	/// <summary>
	/// Custom override
	/// </summary>
	virtual void update(float dt );

	/// <summary>
	/// Convenience cast call
	/// </summary>
	ContentItem* getItem(int index);

	/// <summary>
	/// Returns a content Item if this cluster has one representing the cartridge
	/// otherwise it will return null
	/// </summary>
	ContentItem* getCartridgeItem();

	/// <summary>
	/// Get number of items
	/// </summary>
	int getNumItems();

	/// <summary>
	/// Get number of items
	/// </summary>
	ContentItem* getRightItem();

	/// <summary>
	/// Get number of items
	/// </summary>
	ContentItem* getLeftItem();

	/// <summary>
	/// Only exists for 1 and 3 sized clusters
	/// </summary>
	ContentItem* getMiddleItem();

	/// <summary>
	/// Returns true if this cluster has the package Id
	/// </summary>
	bool hasPackageID(String packageID);

	/// <summary>
	/// Returns the content item representing the package id
	/// </summary>
	ContentItem* getPackageID( String packageID );

	/// <summary>
	/// Updates the relative position of the rings around rotation, value represents the position
	/// along the ring that the items sit, 0 being up front, 1 being far back
	/// </summary>
	void updateContentItemDisplacement(float dt, float value);
private:
	/// <summary>
	/// A ring page only has at most 3 content items, in the case of two, there is
	/// only a left and right. In the case of 1 we use the middle.
	/// </summary>
	ObjectArray contentItems;
	int distanceFromCenter;
	Color rimColor;
	int clusterID;
};

}
#endif

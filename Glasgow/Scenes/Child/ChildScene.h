#ifndef CHILDSCENE_H
#define CHILDSCENE_H

#include "SIEngine/Core/Scene.h"
#include "SIEngine/Core/User.h"
#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Managers/SystemSettings.h"
#include "Glasgow/Scenes/Child/GalleryPage.h"
#include "Glasgow/Scenes/Child/RewardCenterPage.h"
#include "Glasgow/Scenes/Child/LoginPage.h"
#include "Glasgow/Scenes/Child/SongsPage.h"
#include "Glasgow/Scenes/Child/ProfilePage.h"
#include "Glasgow/Scenes/Child/HubPage.h"
#include "Glasgow/Scenes/Child/TutorialPage.h"
#include "Glasgow/Scenes/Child/VideosPage.h"
#include "Glasgow/Scenes/Child/WristStrapPage.h"
#include "SIEngine/Core/Thread.h"
LF_USING_BRIO_NAMESPACE()
using namespace LTM;

namespace Glasgow
{
class HubPage;
class ChildScene;

class PreloadChildTextureThread : public Thread
{
	CLASSEXTENDS(PreloadChildTextureThread,Thread);
public:
	PreloadChildTextureThread(ChildScene* scene_) : super()
	{ scene = scene_; }
	~PreloadChildTextureThread() {}
	virtual void run();
private:
	ChildScene* scene;
};


class ChildScene: public Scene
{
	CLASSEXTENDS(ChildScene, Scene);
	ADD_TO_CLASS_MAP;
public:

	typedef enum RingPages
	{
		RingProfileChooser,
		RingAudio,
		RingCenter,
		RingProfiles,
		RingFarRight,
		RingFarLeft,
		RingGallery,
		RingRewardCenter
	} RingPage;

	ChildScene();
	virtual ~ChildScene();

	/// <summary> 
	/// Sets the current user via user pointer
	/// </summary> 
	void setUser(User *user);

	/// <summary>
	/// Sets the current user via the avatarLogin icon
	/// </summary>
	void setUser(AvatarItem* selected);

	/// <summary>
	/// Respond to controller disconnects
	/// </summary>
	virtual void onControllerDisconnected( HWController* controller );

	/// <summary>
	/// Respond to action button presses
	/// </summary>
	virtual void onButtonPressAction();

	/// <summary>
	/// Respond to back button presses
	/// </summary>
	virtual void onButtonPressBack();

	virtual void onButtonHint();

	/// <summary>
	/// Debug button
	/// </summary>
	virtual void onButtonHome();

	/// <summary>
	/// Sets which objects should be tracked for given states of childscene
	/// </summary>
	virtual void setupHotSpots();

	/// <summary>
	/// Sometimes a scene will want to check for any analog event as opposed to being
	/// told about the post processed candidacy
	/// </summary>
	virtual bool onAnalogStickEvent(AnalogStickEvent *event);

	/// <summary>
	/// Custom handling of analog changes
	/// </summary>
	virtual void onAnalogChange(GameObject* objectLeft, GameObject* objectEntered );

	/// <summary>
	/// Custom handling of selection changes
	/// </summary>
	virtual void onSelectionChange( GameObject* old, GameObject* newObj);

	/// <summary>
	/// Child scene uses timers
	/// </summary>
	virtual void onTimerExpired( TimerEvent* timer );

	/// <summary>
	/// Returns the object representing by a no selection -> selected state chnage
	/// </summary>
	virtual GameObject* getDefaultHighLight();

	/// <summary>
	/// Called when cartridge has been inserted, this should arrive before a ready message
	/// as the files may not have been mounted yet
	/// </summary>
	virtual void onCartridgeInserted();

	/// <summary>
	/// Called when cartridge is ready to load and play
	/// </summary>
	virtual void onCartridgeReady();

	/// <summary>
	/// Cartridge removal
	/// </summary>
	virtual void onCartridgeRemoved();

	/// <summary>
	/// Update timers
	/// </summary>
	void updateTimers(float dt );

	/// <summary>
	/// Custom update loop
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Update loop for spawning particles
	/// </summary>
	void updateParticles(float dt);

	/// <summary>
	/// Sets the scene up for initial login state
	/// </summary>
	void setupForProfileSelection();

	/// <summary>
	/// Sets the scene up in the case a user is already 'logged in'
	/// </summary>
	void setupForSpecificUser( User * user);

	/// <summary>
	/// Sets the scene up in for the first time the user enters the scene and instructs them
	/// on how to use the wrist strap.
	/// </summary>
	void setupForWristStrap();

	/// <summary>
	/// Launches a content item
	/// </summary>
	void launchContentItem(ContentItem* item );

	/// <summary>
	/// Animates to parent scene
	/// </summary>
	virtual void prepareLaunchParentScene(String entryName);

	/// <summary>
	/// Launches error widget
	/// </summary>
	virtual void prepareLaunchVideoWidget(int startingInfo);

	/// <summary>
	/// Logs in using avatar item
	/// </summary>
	void loginUsingAvatar( AvatarItem* user);

	/// <summary>
	/// Hard wired launch mechanism for cartridge inserts
	/// </summary>
	void doCartridgeLaunch();

	/// <summary>
	/// Particle Burst for fun
	/// </summary>
	void doCartBurst();

	/// <summary> 
	/// Child scene logic state: Current page tracks where the camera is within the scene
	/// </summary> 
	ChildPage* currentPage;

	/// <summary>
	/// Child scene logic state: Current page tracks where the camera was within the scene
	/// </summary>
	ChildPage* previousPage;

	/// <summary>
	/// Active child page
	/// </summary>
	ChildPage* 					activePage;

	/// <summary>
	/// Changes theme
	/// </summary>
	void changeToNewTheme(String newThemeName);
private:
	/// <summary>
	/// Helper function that moves the two cameras a given direction
	/// </summary>
	void moveCamerasBy( Vec3f direction );

	/// <summary>
	/// Helper function that moves the two cameras a given direction
	/// </summary>
	void moveCamerasTo( Vec3f uiCamPosition, Vec3f bgCamPosition );

	/// <summary>
	/// Sets a random encouragement string in the reward center
	/// </summary>
	void setRandomEncouragement();

	/// <summary>
	/// Fills our references
	/// </summary>
	void fillReferences();

	/// <summary>
	/// Plays the song for the given AudioItem
	/// </summary>
	void playSong(AudioItem* item);

	/// <summary>
	/// Moves camera to show the gallery
	/// </summary>
	void moveCameraToPage( ChildPage* page );

	/// <summary>
	/// Moves camera to switch users
	/// </summary>
	void moveCameraToSwitchUsers();

	/// <summary>
	/// Sets up all the default visibility status' and positions of dynamic objects on scene creation
	/// </summary>
	void setupInitialState();

	/// <summary>
	/// Animates the sliders to change position in the profile page
	/// </summary>
	void switchSliderToAvatar();

	/// <summary>
	/// Animates the sliders to change position in the profile page
	/// </summary>
	void switchSliderToTheme();

	/// <summary>
	/// Fades the label in on the Profiles page to show the choose avatar
	/// </summary>
	void fadeToAvatarLabel(float time);

	/// <summary>
	/// Fades the label in on the Profiles page to show the choose theme
	/// </summary>
	void fadeToThemeLabel(float time);

	/// <summary>
	/// Sets the main hub's avatar icon using the prefab name
	/// </summary>
	void setAvatarIcon(String avatarPrefabName);

	/// <summary>
	/// Sets the theme based on previous login
	/// </summary>
	void setLastUsedTheme();

	/// <summary>
	/// Clones and copies the Avatar icon in the same position for the same scene graph location
	/// </summary>
	AvatarItem* reCreateAvatarItem(AvatarItem* toClone);

	/// <summary>
	/// Animation helper to dim screen
	/// </summary>
	void darkenScreen();

	/// <summary>
	/// Returns the color for a given theme
	/// </summary>
	Color getThemeColor();

	/// <summary>
	/// Does the actual launch
	/// </summary>
	void doLaunch();

	/// <summary>
	/// Notification that the app has returned back to us from a suspend state
	/// </summary>
	void onReturnToApp();

	/// <summary>
	/// Creates a single object to be used for dynamic re-loading of cartridges
	/// </summary>
	void createCartridgeItem();

	/// <summary>
	/// Returns name of the music file to be played iwth a theme
	/// This should be restructured into a ThemeInfo class I think
	/// </summary>
	String getThemeMusic(String themeName);

	/// <summary>
	/// Adds cartridge content item to scene graph
	/// </summary>
	void addInCartridgeContentItem();


	void setupLaunchState();
	/// <summary> 
	/// Object references
	/// </summary> 
	int							videoIndexLaunch;
	int							errorCodeLaunch;
	bool						firstLaunchOfUI;
	PackageInfo* 				cartridgeInfo;
	GameObject*					preCartridgeSelection;
	User*						currentUser;
	ContentItem*				cartridgeItem;
	CameraObject* 				contentCamera;
	CameraObject* 				bgCamera;
	SpriteObject* 				screenDarken;
	SpriteObject* 				sceneChangeDarken;
	SpriteObject*				backGround;
	SpriteObject*				backGroundSwap;
	VideosPage*					videosPage;
	RewardCenterPage* 			rewardCenterPage;
	ProfilePage*				profilesPage;
	GalleryPage*				galleryPage;
	TutorialPage*				tutorialPage;
	HubPage*					mainHub;
	SongsPage*					songsPage;
	LoginPage*					profileChooserPage;
	WristStrapPage*				wristStrapPage;
	PackageInfo*				itemToLaunch;
	float						launchTimer;
	float 						prepareTimer;
	float						cartBurstTimer;
	float 						hintTimer;
};

}
#endif /* TESTSCENE_H_ */

#include "RewardCenterPage.h"

namespace Glasgow
{

RewardCenterPage::RewardCenterPage(GameObject* rootObject) : super(rootObject)
{
	rewardCenterTray 	= find("storeTray");

	encouragements[0] = "Great Job ";
	encouragements[1] = "Well Done ";
	encouragements[2] = "Way To Go ";
	encouragements[3] = "The Amazing ";
	encouragements[4] = "Good Work ";
	encouragements[5] = "Amazing ";
	encouragements[6] = "Excellent ";
	encouragements[7] = "Magnificent ";
	encouragements[8] = "Resplendent ";
	encouragements[9] = "Exalted ";

}

/// <summary>
/// Add our tracked items to the scene list
/// </summary>
void RewardCenterPage::addHotSpots( ObjectArray* trackedList )
{ trackedList->addElement(findButton("backButton")); }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void RewardCenterPage::cameraArrived()
{
	User* user = UsersManager::getInstance()->getCurrentUser();
	if( user != null )
	{
		setRandomEncouragement(user->getDisplayName());
		int badges = (int)user->GetBadges();

		if( badges == 1 )
		{
			findLabel("badgeCountLabel")->setText( String(badges) );
			findLabel("badgesText")->setText( "BADGE" );
		}
		else
		{
			findLabel("badgeCountLabel")->setText( String(badges) );
			findLabel("badgesText")->setText( "BADGES" );
		}

		findLabel("tokenCountLabel")->setText( String((int)user->GetTokens()) );
	}
	else
	{
		setRandomEncouragement("");
		findLabel("badgeCountLabel")->setText( "0" );
		findLabel("tokenCountLabel")->setText( "0" );
	}

	rewardCenterTray->moveBy(Vec3f::Down * 200, 0.01f);
	rewardCenterTray->waitFor(1.5f);
	rewardCenterTray->moveBy(Vec3f::Up * 200, 0.5f);
}


/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void RewardCenterPage::cameraLeaving()
{
	rewardCenterTray->moveBy(Vec3f::Down * 200, 1.5f);
	rewardCenterTray->waitFor(0.25f);
	rewardCenterTray->moveBy(Vec3f::Up * 200, 0.01f);
	super::cameraLeaving();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* RewardCenterPage::getArrivalSelection()
{ return findButton("backButton"); }


void RewardCenterPage::setRandomEncouragement(String userName)
{
	findLabel("welcomeLabel")->setText(userName);
}

RewardCenterPage::~RewardCenterPage()
{
	printf("RewardCenterPage::~RewardCenterPage\n");
}

} /* namespace Glasgow */

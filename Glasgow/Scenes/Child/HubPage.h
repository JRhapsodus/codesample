#ifndef HUBPAGE_H_
#define HUBPAGE_H_

#include "SIEngine/Include/LFIncludes.h"

#include "ChildPage.h"
#include "Glasgow/Scenes/Child/RingCluster.h"
#include "SIEngine/Include/SIScenes.h"
#include "Glasgow/Data/PackageInfo.h"

namespace Glasgow
{
class ChildScene;

class HubPage : public ChildPage
{
	CLASSEXTENDS(HubPage, ChildPage);
public:
	HubPage(GameObject* rootObject);
	virtual ~HubPage();

	/// <summary>
	/// Forwarding call from childscene
	/// </summary>
	virtual void handleCartridgeReady();
	void handleReturnToApp();
	/// <summary>
	/// Animation plug for all pages to prepare for a cartridge launch
	/// Note: This animation has to be canceled at any point along its progress
	/// </summary>
	virtual void prepareForCartridgeLaunch();

	/// <summary>
	/// Cartridge removed
	/// </summary>
	virtual void handleCartridgeRemoved();

	/// <summary>
	/// Cancels the animation for cartridge launch
	/// </summary>
	virtual void cancelCartridgeLaunch();

	/// <summary>
	/// Returns which direction the camera goes to when going to this page
	/// </summary>
	virtual Vec3f getCameraPlacement();

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& highlight);

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Called when a new active user has effectively loggedin
	/// </summary>
	virtual void handleUserSet(User* user);

	/// <summary>
	/// Rotates the center ring effectively bringing the next RingPage into focus
	/// </summary>
	void rotateRingRight();

	/// <summary>
	/// Rotates the center ring effectively bringing the next RingPage into focus
	/// </summary>
	void rotateRingLeft();

	/// <summary>
	/// Rotates the center ring back to landing
	/// </summary>
	void rotateToLanding();

	/// <summary>
	/// Animates all pages and rings coming in
	/// </summary>
	void staggerInContentRings();

	/// <summary>
	/// Animates all pages and rings going out
	/// </summary>
	void staggerOutContentRings();

	/// <summary>
	/// Displays Indicators of location
	/// </summary>
	void displayIndicators(float delay);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Do not allow cameraleaving from ChildPage to destroy us, but do nothing
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Sets avatar icon
	/// </summary>
	void setAvatarIcon(String avatarPrefabName);

	/// <summary>
	/// Animates all pages and rings coming in
	/// </summary>
	void animateAvatarItemIntoTray(AvatarItem* selected);

	/// <summary>
	/// Fills the hub with ring pages and content items
	/// </summary>
	void fillHub();

	/// <summary>
	/// Animates all pages and rings coming in
	/// Different than calling slideTray with 0 because that requires the use of actions
	/// and objects not on the scene graph do not get called Update to progress the actions
	/// </summary>
	void setTrayDown();

	/// <summary>
	/// Animate main hub tray sliding down
	/// </summary>
	void slideTrayDown(float time);

	/// <summary>
	/// Animate main hub tray sliding up
	/// </summary>
	void slideTrayUp(float time);

	/// <summary>
	/// Replaces the main avatar
	/// </summary>
	void replaceAvatar(AvatarItem* newAvatar);

	/// <summary>
	/// Spawns a particle effect
	/// </summary>
	void doStarBurst();

	/// <summary>
	/// Returns the color for a given theme
	/// </summary>
	Color getThemeColor();

	/// <summary>
	/// Retuurns the cluster that is front focused
	/// </summary>
	RingCluster* getCurrentCluster();

	/// <summary>
	/// Position all the clusters around the ring
	/// </summary>
	void positionClusters();

	void moveClusters(int except);

	/// <summary>
	/// Called when the theme changes
	/// </summary>
	void onThemeChange();

	/// <summary>
	/// Animation for when an item is launched
	/// </summary>
	void animateForItemLaunch(ContentItem* toLaunch);

	/// <summary>
	/// Animation for when an item is launched
	/// </summary>
	void cancelAnimateForItemLaunch();


	/// <summary>
	/// Animating from return from item launch
	/// </summary>
	void animateFromItemLaunch();

	/// <summary>
	/// Gets the cluster at the index provided
	/// </summary>
	RingCluster* getCluster(int index);

	/// <summary>
	/// Scans and finds an unfilled cluster in even/odd order front to back
	/// </summary>
	RingCluster* findAvailableCluster();

	/// <summary>
	/// Call this to query Package Manager and fill a list of Packagedata
	/// </summary>
	void fillAppsFromDataBase();

private:

	/// <summary>
	/// Helper function for creating content items with the various settings
	/// </summary>
	ContentItem* makeNewContentItem( String prefabName, PackageInfo* info );

	/// <summary>
	/// Helper function for creating a new cluster for content items to go into
	/// </summary>
	void makeNewCluster();

	Vec3f 					originalLaunchPosition;
	/// <summary>
	/// Left / right navigation on ring pages
	/// </summary>
	GameObject*				locker;
	GameObject*				profileIcon;

	ContentItem*			itemLaunched;

	ButtonObject*			parentSettingsButton;
	ButtonObject*			rewardCenterButton;
	SpriteObject* 			toRightButton;
	SpriteObject* 			toLeftButton;
	SpriteObject* 			toLeftIcon;
	SpriteObject* 			toRightIcon;
	GameObject*				rootTray;
	GameObject* 			centerRing;
	ObjectArray 			ringPages;
	ObjectArray 			indicators;
	ChildScene*				currentScene;
	ObjectArray* 			packages;

	ContentItem*			lastContentItemPlayed;
	/// <summary>
	/// Page 0 always represents the middle -default ring, where 1 represents the one to the right
	/// </summary>
	int 					currentPage;

	/// <summary>
	/// Polar coordinate delta between each page
	/// </summary>
	float					anglesPerCluster;

	/// <summary>
	/// Used to calculate the positions of the clusters along the ring
	/// </summary>
	float 					ringRadius;


};

} /* namespace Glasgow */
#endif /* HUBPAGE_H_ */

#ifndef GALLERYPAGE_H_
#define GALLERYPAGE_H_

#include "ChildPage.h"

namespace Glasgow
{

class GalleryPage : public ChildPage
{
	CLASSEXTENDS(GalleryPage, ChildPage);
public:
	GalleryPage(GameObject* objectRoot);
	virtual ~GalleryPage();

	/// <summary>
	/// Called when a new active user has effectively loggedin
	/// </summary>
	virtual void handleUserSet(User* user);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& highlight);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Returns the selected game snap
	/// </summary>
	GalleryItem* getSelectedGameSnap();

	/// <summary>
	/// Custom response to pressaction
	/// </summary>
	virtual bool handlePressAction(GameObject*& currentHighlight);

	/// <summary>
	/// Notification for camera arriving
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();
private:
	SliderObject*			gallerySlider;
	SpriteObject*			galleryDarken;
	bool 					galleryItemIsFullScreen;
	SpriteObject*			galleryFrame;
};

} /* namespace Glasgow */
#endif /* GALLERYPAGE_H_ */

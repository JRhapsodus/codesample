#include "TutorialPage.h"
#include "SIEngine/Managers/TimerManager.h"
#include "SIEngine/Include/SIScenes.h"
#include "SIEngine/Include/SIActions.h"
#include "SIEngine/Include/SIMaterials.h"
#include "Glasgow/Particles/Particle.h"

namespace Glasgow
{

TutorialPage::TutorialPage(GameObject* objectRoot) : super(objectRoot)
{
	welcomeLabel		= findLabel("headerLabel");
	avatarSlider		= (SliderObject*)find("avatarSlider");
	themeSlider			= (SliderObject*)find("themeSlider");
	toolTipRoot			= find("TutorialToolTip");
	toolTipWand			= toolTipRoot->findLabel("wandLabel");
	toolTipAnalog		= toolTipRoot->findLabel("joystickLabel");
	chooseThemeLabel	= findLabel("themeLabel");
	chooseAvatarLabel	= findLabel("avatarLabel");

	controllerWand[0]	= toolTipRoot->findSprite("controller1");
	controllerWand[1]	= toolTipRoot->findSprite("controller2");
	controllerWand[2]	= toolTipRoot->findSprite("controller3");
	controllerWand[3]	= toolTipRoot->findSprite("controller4");
	controllerDefault	= toolTipRoot->findButton("controllerButton");

	setupInitialState();
}

TutorialPage::~TutorialPage()
{
	printf("TutorialPage::~TutorialPage\n");
}

void TutorialPage::setupInitialState()
{
	toolTipRoot->find("controllerToClose")->setVisible(false);
	controllerWand[0]->setVisible(false);
	controllerWand[1]->setVisible(false);
	controllerWand[2]->setVisible(false);
	controllerWand[3]->setVisible(false);
	controllerDefault->setVisible(false);
	controllerDefault->setScaleOnActive(0.02f);
	themeChosen = false;
	switchTimer = 1;
	controllerIndex = 0;
}


void TutorialPage::loadGameObjectState(GameObjectState state)
{
	printf("TutorialPage::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// Overr ridden for tutorial to get any analog stick event
/// </summary>
bool TutorialPage::handleAnalogStickEvent(AnalogStickEvent *event)
{
	Scene* childScene = SceneManager::getInstance()->getScene();
	if( !toolTipRoot->hasActions() && childScene->getCurrentHighlight() == controllerDefault )
	{
		childScene->setCurrentHighlight(themeSlider->getSelectedItem());
		toolTipRoot->moveBy( Vec3f::Down*400, 1.0f);
		toolTipRoot->makeVisible(false);
		doControllerParticles();
	}
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool TutorialPage::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	if( objectLeft == controllerDefault )
	{
		toolTipRoot->moveBy( Vec3f::Down*400, 1.0f);
		toolTipRoot->makeVisible(false);
		doControllerParticles();
	}

	if( themeChosen )
	{
		if ( avatarSlider->onAnalogChange( objectLeft, objectEntered ) )
		{
			SceneManager::getInstance()->getScene()->playSound("Base_MenuScroll");
			currentHighlight = avatarSlider->getSelectedItem();
			if( currentHighlight != NULL && currentHighlight->typeOf(ButtonObject::type()) )
			{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
			return true;
		}
	}
	else
	{
		if ( themeSlider->onAnalogChange( objectLeft, objectEntered ) )
		{
			SceneManager::getInstance()->getScene()->playSound("Base_MenuScroll");

			currentHighlight = themeSlider->getSelectedItem();
			if( currentHighlight != NULL && currentHighlight->typeOf(ButtonObject::type()) )
			{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }

			ThemeItem* newThemeItem = (ThemeItem*)currentHighlight;
			ChildScene* scene = (ChildScene*)SceneManager::getInstance()->getScene();
			scene->changeToNewTheme( newThemeItem->getThemeName() );
			return true;
		}
	}
}



/// <summary>
/// Custom response to pressaction
/// </summary>
void TutorialPage::doControllerParticles()
{
	for( int i = 0; i<50; i++ )
	{
		TRelease<SpriteData> spriteData( new SpriteData("starParticle"));
		TRelease<Particle> particle( new Particle( spriteData) );

		particle->lifeSpan = 1+randomFloat(0, 3);
		particle->gravity = Vec3f::Up*60;

		float randomX = -10 + randomFloat(0, 20);
		float randomY = 5 - randomFloat(0, 20);
		float randomZ = -15 + randomFloat(0, 15);

		particle->rotationRate = (int) (-500 + randomFloat(0, 1) * 1000);
		particle->velocity = Vec3f(randomX, randomY, randomZ)*10;
		particle->startColor = Color( 1, 1, 1 ,1 );
		particle->endColor = Color( 1, 1, 1 , 0  );
		particle->setSize( Size( 25, 25 ) );
		particle->setScale( Vec3f::One * 0.3f );
		particle->setMaterial(TRelease<TintMaterial>(new TintMaterial(particle)));
		particle->fadeTo(0, particle->lifeSpan);
		addChild(particle);

		particle->setRotation( Quaternion::angleAxis( randomFloat(0,360), Vec3f::Forward) );
		float offX = -100 + 200*randomFloat(0,1);
		float offY = -50 + 100*randomFloat(0,1);
		particle->setPosition( controllerWand[0]->getPosition() + Vec3f(offX, offY, 0) );
	}
}

/// <summary>
/// Custom response to pressaction
/// </summary>
void TutorialPage::doWelcomeParticles()
{
	printf("Welcome Particles Triggered\n");
	SceneManager::getInstance()->getScene()->playSound("Base_AvatarSelectionStars");

	for( int i = 0; i<50; i++ )
	{
		TRelease<SpriteData> spriteData( new SpriteData("starParticle"));
		TRelease<Particle> particle( new Particle( spriteData) );

		particle->lifeSpan = 1+randomFloat(0, 3);
		particle->gravity = Vec3f::Up*60;

		float randomX = -10 + randomFloat(0, 20);
		float randomY = 5 - randomFloat(0, 20);
		float randomZ = -15 + randomFloat(0, 15);

		particle->rotationRate = (int) (-500 + randomFloat(0, 1) * 1000);
		particle->velocity = Vec3f(randomX, randomY, randomZ)*10;
		particle->startColor = Color( 1, 1, 1 ,1 );
		particle->endColor = Color( 1, 1, 1 , 0  );
		particle->setSize( Size( 25, 25 ) );
		particle->setScale( Vec3f::One * 0.3f );
		particle->setMaterial(TRelease<TintMaterial>(new TintMaterial(particle)));
		particle->fadeTo(0, particle->lifeSpan);
		addChild(particle);

		particle->setRotation( Quaternion::angleAxis( randomFloat(0,360), Vec3f::Forward) );
		float offX = -100 + 200*randomFloat(0,1);
		float offY = -50 + 100*randomFloat(0,1);
		particle->setPosition(welcomeLabel->getPosition() + Vec3f(offX, offY, 0) );
	}
}

/// <summary>
/// Add our tracked items to the scene list
/// </summary>
void TutorialPage::addHotSpots( ObjectArray* trackedList )
{
	if( themeChosen )
	{ avatarSlider->addObjectsToTracking(trackedList); }
	else
	{ themeSlider->addObjectsToTracking(trackedList); }
}

/// <summary>
/// Returns which direction the camera goes to when going to this page
/// </summary>
Vec3f TutorialPage::getCameraPlacement()
{ return Vec3f::Zero; }

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* TutorialPage::getArrivalSelection()
{ return controllerDefault; }

/// <summary>
/// Recreates avatar
/// </summary>
AvatarItem* TutorialPage::reCreateAvatarItem(AvatarItem* toClone)
{
	Vec3f pos = toClone->getPosition();
	Vec3f scale = toClone->getScale();
	Quaternion rot = toClone->getRotation();
	GameObject* parent = toClone->getParent();

	AvatarItem* avatar = (AvatarItem*) Scene::instantiate(toClone->getPrefabCloneName());
	avatar->originalScale = scale;
	avatar->setupAsLoginAvatar(toClone->getUserName());
	avatar->setPosition(pos);
	avatar->setScale(scale);
	avatar->setRotation(rot);
	parent->addChild(avatar);
	avatar->release();

	toClone->setVisible(false);
	return avatar;
}


/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void TutorialPage::cameraArrived()
{
	SceneManager::getInstance()->getScene()->playSound("HTP_Idle_RecustomizeLater");

	toolTipRoot->setVisible(false);
	avatarSlider->setupSlider(SliderObject::SliderTypeAvatar);
	themeSlider->setupSlider(SliderObject::SliderTypeTheme);
	avatarSlider->setVisible(false);
	themeSlider->setVisible(false);
	chooseThemeLabel->setVisible(false);
	chooseAvatarLabel->setVisible(false);

	float waitTime = 0;
	Vec3f currentPosition = welcomeLabel->getPosition();
	welcomeLabel->setPosition( currentPosition + Vec3f(0, 200, -300 ) );
	welcomeLabel->waitFor( 0.4f );
	waitTime += 0.4f;
	welcomeLabel->moveTo( currentPosition, 1.5f );
	waitTime += 1.5f;

	TimerManager::getInstance()->addTimer("WelcomeBurst", waitTime, 0 );

	welcomeLabel->waitFor( 0.5f );
	waitTime += 0.5f;
	welcomeLabel->moveTo(Vec3f::Zero, 0.5f);
	welcomeLabel->makeVisible(false);
	waitTime += 1.5f;

	toolTipRoot->moveBy( Vec3f::Down*100, 0);
	toolTipRoot->waitFor(waitTime);
	toolTipRoot->makeVisible(true);
	toolTipRoot->moveBy( Vec3f::Up*100, 1.00);
	waitTime+= 1.0;

	chooseThemeLabel->fadeIn(waitTime);
	TimerManager::getInstance()->addTimer("PlayAnalogSelect", waitTime, 0);

	controllerWand[0]->setVisible(false);
	controllerWand[1]->setVisible(false);
	controllerWand[2]->setVisible(false);
	controllerWand[3]->setVisible(false);

	controllerWand[0]->waitFor(waitTime);
	controllerWand[0]->makeVisible(true);
	controllerWand[0]->waitFor(0.5f);
	controllerWand[0]->setVisible(false);


	controllerDefault->waitFor(waitTime + 2);
	toolTipWand->setVisible(false);

	themeSlider->moveBy( Vec3f( 0, 200, -300 ), 0);
	themeSlider->waitFor(waitTime);
	themeSlider->makeVisible(true);
	themeSlider->staggerElementsIn();
	themeSlider->moveBy( Vec3f( 0, -200, 300 ), 1.0f);


	SceneManager::getInstance()->getScene()->lockOutInput(waitTime);


	super::cameraArrived();
}

void TutorialPage::update(float dt)
{
	if( !controllerWand[0]->hasActions() )
	{
		if ( switchTimer > 0 )
		{
			switchTimer -= dt;
			if( switchTimer <= 0 )
			{
				switchTimer = 1;
				controllerWand[controllerIndex]->setVisible(false);
				controllerIndex = (controllerIndex+1)%4;
				controllerWand[controllerIndex]->setVisible(true);
			}
		}
	}

	super::update(dt);
}

/// <summary>
/// Custom response to pressaction
/// </summary>
bool TutorialPage::handlePressAction(GameObject*& currentHighlight)
{
	if( !themeChosen )
	{
		if( currentHighlight != NULL && currentHighlight->typeOf(ThemeItem::type()) )
		{
			themeChosen = true;

			chooseThemeLabel->fadeOut(0);

			CameraObject* bgCamera = SceneManager::getInstance()->getScene()->getCamera(0);
			CameraObject* contentCamera = SceneManager::getInstance()->getScene()->getCamera(1);
			((ButtonObject*)currentHighlight)->setState(ButtonStateNormal);

			float wait = 1.0f;
			chooseAvatarLabel->fadeIn(wait);

			avatarSlider->moveBy( Vec3f( 0, 200, -300 ), 0);
			avatarSlider->waitFor(wait);
			avatarSlider->makeVisible(true);
			avatarSlider->staggerElementsIn();
			avatarSlider->moveBy( Vec3f( 0, -200, 300 ), 1.0f);

			themeSlider->moveBy( Vec3f( 0, 200, -300 ), 1.0f);
			themeSlider->makeVisible(false);

			currentHighlight = avatarSlider->getSelectedItem();
			((ButtonObject*)currentHighlight)->setState(ButtonStateActive);

			contentCamera->waitFor(wait);
			contentCamera->moveBy( Vec3f::Forward*100, 1.5f);

			bgCamera->waitFor(wait);
			bgCamera->moveBy( Vec3f::Forward*50, 1.5f);

			SceneManager::getInstance()->getScene()->playSound("Base_Select");
			SceneManager::getInstance()->getScene()->playSound("HTP_Avatar_alt00");
			return true;
		}
	}

	return false;
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void TutorialPage::cameraLeaving()
{
	avatarSlider->moveBy( Vec3f( 0, 200, -300 ), 0);
	toolTipRoot->moveBy( Vec3f::Down*100, 1.0f);
	toolTipRoot->moveBy( Vec3f::Back*100, 0.1f);
	toolTipRoot->moveBy( Vec3f::Up*100, 0.1f);
	AvatarItem* avatar = (AvatarItem*)avatarSlider->getSelectedItem();
	avatar->setState(ButtonStateNormal);

	super::cameraLeaving();
}


} /* namespace Glasgow */

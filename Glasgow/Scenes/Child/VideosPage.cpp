#include "VideosPage.h"
#include "Glasgow/Scenes/SceneManager.h"
namespace Glasgow
{

VideosPage::VideosPage(GameObject* objectRoot) : super(objectRoot)
{
	gallerySlider		= (SliderObject*)find("gallerySlider");
	galleryDarken		= findSprite("photoDarken");
	galleryFrame		= findSprite("photoFrame");
	galleryDarken->setOpacity(0);
	galleryFrame->setVisible(false);
	videoNameLabel 		= findLabel("videoNameLabel");
	episodeNameLabel	= findLabel("episodeNameLabel");

	videoNameLabel->setPosition( videoNameLabel->getPosition() + Vec3f::Down * 60);
	episodeNameLabel->setPosition(videoNameLabel->getPosition() + Vec3f::Down * 205);
	gallerySlider->setScale( Vec3f::One * 0.8f);
}


VideosPage::~VideosPage()
{
	printf("VideosPage::~VideosPage\n");

}

void VideosPage::setPackageInfo(PackageInfo* info)
{
	printf("Package Info set for Videospage with %d videos\n", info->videos.getSize() );
	packageInfo = info;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void VideosPage::addHotSpots( ObjectArray* trackedObjects )
{
	trackedObjects->addElement(findButton("backButton"));
	gallerySlider->addObjectsToTracking( trackedObjects );
}

int VideosPage::getVideoIndex()
{
	return gallerySlider->getSelectedIndex();
}

/// <summary>
/// Custom response to pressaction
/// </summary>
bool VideosPage::handlePressAction(GameObject*& currentHighlight)
{
	if( currentHighlight != NULL )
	{
		SceneManager::getInstance()->getScene()->playSound("Base_LaunchApp");
		printf("GalleryPage::handlePressAction(GameObject*& currentHighlight) currentHighlight == %s\n", currentHighlight->name.str());
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool VideosPage::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	//*****************************************************************
	// Forward to slider, since the slider can move on analog change
	// we might need to update the selections, don't allow outside scene
	//*****************************************************************
	if ( gallerySlider->onAnalogChange( objectLeft, objectEntered ) )
	{
		currentHighlight = gallerySlider->getSelectedItem();
		if( currentHighlight != NULL && currentHighlight->typeOf(ButtonObject::type()) )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }

		if( currentHighlight != NULL && currentHighlight->typeOf(GalleryItem::type()) )
		{

			SceneManager::getInstance()->getScene()->playSound("Base_Highlight");

			VideoInfo* info = ((GalleryItem*)currentHighlight)->getVideoInfo();
			Color c = Color::fromHex(info->fontColor);
			c.a = 0;
			episodeNameLabel->cancelActions();
			episodeNameLabel->colorTo(c, 0.25f );
			episodeNameLabel->changeTo(info->videoTitle);
			c.a = 1;
			episodeNameLabel->colorTo(c, 0.25f );
		}

		return true;
	}
}

/// <summary>
/// Called when the camera moves to this page
/// </summary>
void VideosPage::cameraArrived()
{
	if( packageInfo != null )
	{
		gallerySlider->setupSlider(SliderObject::SliderTypeVideo, &packageInfo->videos);
		gallerySlider->setPosition( Vec3f(0,-35,1) );
		gallerySlider->animateInitialEntry();
		galleryFrame->setOpacity(0);
		galleryFrame->waitFor(1.8f);
		galleryFrame->fadeTo(1, 0.25f);

		SpriteObject* backing = findSprite("bundleBacking");
		VideoInfo* videoInfo = (VideoInfo*)packageInfo->videos.elementAt(0);
		videoNameLabel->setTextColor(Color::fromHex(videoInfo->fontColor));
		videoNameLabel->setText(videoInfo->bundleTitle);
		episodeNameLabel->setText(videoInfo->videoTitle);

		if( !videoInfo->backGroundImage.isEmpty() )
		{
			String backgroundImage = packageInfo->appDirPath +  videoInfo->backGroundImage;
			if( AssetLibrary::fileExists(backgroundImage) )
			{
				backing->getSpriteData()->setTexture(backgroundImage);
				backing->setOpacity(0);
				backing->waitFor(1.5f);
				backing->fadeTo(1, 0.5f);
				backing->setScale( Vec3f::One*0.66f);
			}
			else
			{ backing->setVisible(false); }
		}
		else
		{ backing->setVisible(false); }
	}
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* VideosPage::getArrivalSelection()
{
	GalleryItem* item = (GalleryItem*)gallerySlider->getSelectedItem();
	VideoInfo* info = item->getVideoInfo();
	videoNameLabel->setText(info->bundleTitle);

	Color c = Color::fromHex(info->fontColor);
	c.a = 0;
	episodeNameLabel->colorTo(c, 0.25f );
	episodeNameLabel->changeTo(info->videoTitle);
	c.a = 1;
	episodeNameLabel->colorTo(c, 0.25f );

	return gallerySlider->getSelectedItem();
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void VideosPage::cameraLeaving()
{
	SpriteObject* backing = findSprite("bundleBacking");
	backing->fadeTo(0, 0.5f);
	galleryFrame->fadeTo(0, 0.15f);
	gallerySlider->moveBy(Vec3f(0, 400, -200), 1.55f);
	gallerySlider->moveBy(Vec3f(0, -400, 200), 0.01f);
	super::cameraLeaving();
}

}

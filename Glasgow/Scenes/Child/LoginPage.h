#ifndef LOGINPAGE_H_
#define LOGINPAGE_H_

#include "ChildPage.h"


namespace Glasgow
{

class LoginPage : public ChildPage
{
CLASSEXTENDS(LoginPage, ChildPage)
	;
public:
	LoginPage( GameObject* rootObject );
	virtual ~LoginPage();

	/// <summary>
	/// Overridden to snoop for analog changes
	/// </summary>
	virtual bool handleAnlogChange( GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight );

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Custom animation for leaving for tutorial
	/// </summary>
	void leaveForTutorial();

	/// <summary>
	/// Add our tracked items to the scene list
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackedList );

	/// <summary>
	/// sets the last user that logged in as the default selected in initial login
	/// </summary>
	AvatarItem* getLastUserSelected();

	/// <summary>
	/// Positions the logins on screen
	/// </summary>
	void positionAvatarLogins();

	/// <summary>
	/// Creates an avatar login per user
	/// </summary>
	void createLoginIcons();

	/// <summary>
	/// Custom animation transition
	/// </summary>
	void animateForSwitchUsers();

	/// <summary>
	/// Override to take care of custom timer events
	/// </summary>
	void update( float dt );

	/// <summary>
	/// Helper function to clone.
	/// Really this is why we made an ICloneable on gameobject, but this is
	/// not able to due to the fact we must modify the old parents scene graph
	/// </summary>
	AvatarItem* reCreateAvatarItem( AvatarItem* toClone );



	/// <summary>
	/// Redo avatar items
	/// </summary>
	void remakeAvatarItems();




	/// <summary>
	/// Bubble out items animation
	/// </summary>
	void bubbleOutItems();

	/// <summary>
	/// Animation plug for all pages to prepare for a cartridge launch
	/// Note: This animation has to be canceled at any point along its progress
	/// </summary>
	virtual void prepareForCartridgeLaunch();

	/// <summary>
	/// Cartridge removed prior to the actual launch call
	/// </summary>
	virtual void cancelCartridgeLaunch();

	/// <summary>
	/// Bubble in items animation
	/// </summary>
	void bubbleInItems( float delay );

	void setUserLabel(String name );

	bool itemsHaveActions();

private:
	AvatarItem* selectedAvatar;
	ObjectArray avatarLogins;
	TextLabel* userNameLabel;
	float autoLoginTimer;
	float preSwitchThemeTimer;
};

} /* namespace Glasgow */
#endif /* LOGINPAGE_H_ */

#include "GalleryPage.h"

namespace Glasgow
{

GalleryPage::GalleryPage(GameObject* objectRoot) : super(objectRoot)
{
	galleryItemIsFullScreen = false;
	gallerySlider		= (SliderObject*)find("gallerySlider");
	galleryDarken		= findSprite("photoDarken");
	galleryFrame		= findSprite("photoFrame");

	//gallerySlider->setupSlider(SliderObject::SliderTypeGallery);

	galleryDarken->setOpacity(0);
	galleryFrame->setVisible(false);
}

/// <summary>
/// Convenience function for casting purposes
/// </summary>
GalleryItem* GalleryPage::getSelectedGameSnap()
{ return (GalleryItem*)gallerySlider->getSelectedItem(); }

/// <summary>
/// Called when a new active user has effectively loggedin
/// </summary>
void GalleryPage::handleUserSet(User* user)
{
	TextLabel* text = findLabel("userLabel");
	text->setText(user->getDisplayName());
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void GalleryPage::addHotSpots( ObjectArray* trackedObjects )
{
	if ( galleryItemIsFullScreen )
	{
		trackedObjects->addElement(gallerySlider->getSelectedItem());
	}
	else
	{
		trackedObjects->addElement(findButton("backButton"));
		trackedObjects->addElement(findButton("trashButton"));
		gallerySlider->addObjectsToTracking( trackedObjects );
	}
}

/// <summary>
/// Custom response to pressaction
/// </summary>
bool GalleryPage::handlePressAction(GameObject*& currentHighlight)
{
	if( currentHighlight != NULL )
	{
		printf("GalleryPage::handlePressAction(GameObject*& currentHighlight) currentHighlight == %s\n", currentHighlight->name.str());

		if ( currentHighlight->typeOf( GalleryItem::type() ) )
		{
			if( galleryItemIsFullScreen )
			{
				galleryDarken->fadeTo(0, 0.25f);
				galleryFrame->waitFor(0.25f);
				GalleryItem* item = (GalleryItem*)currentHighlight;
				item->scaleTo( item->originalScale, 0.25f );
				item->setState(ButtonStateActive);
			}
			else
			{
				galleryFrame->fadeTo(0, 0.15f);
				galleryDarken->fadeTo(0.8f, 0.25f);
				GalleryItem* item = (GalleryItem*)currentHighlight;
				item->scaleTo( item->originalScale * 2.75f, 0.25f );
				item->setState(ButtonStateNormal);
			}

			galleryItemIsFullScreen = !galleryItemIsFullScreen;
			return true;
		}
		else if ( currentHighlight->name == "trashButton" )
		{
			printf("Handling trash button\n");

			if( currentHighlight->typeOf( ButtonObject::type() ) )
			{ ((ButtonObject*)currentHighlight)->setState(ButtonStateNormal); }

			if( gallerySlider->getSelectedItem() != NULL )
			{
				printf("Removing game snap \n");
				gallerySlider->removeItem( gallerySlider->getSelectedItem() );
				currentHighlight = gallerySlider->getSelectedItem();

				if( currentHighlight != NULL && currentHighlight->typeOf( ButtonObject::type()) )
				{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
			}

			return true;
		}
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool GalleryPage::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	//*****************************************************************
	// Forward to slider, since the slider can move on analog change
	// we might need to update the selections, don't allow outside scene
	//*****************************************************************
	if ( gallerySlider->onAnalogChange( objectLeft, objectEntered ) )
	{
		currentHighlight = gallerySlider->getSelectedItem();
		if( currentHighlight != NULL && currentHighlight->typeOf(ButtonObject::type()) )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
		return true;
	}
}

/// <summary>
/// Called when the camera moves to this page
/// </summary>
void GalleryPage::cameraArrived()
{
	gallerySlider->setPosition( Vec3f(0,-15,1) );
	gallerySlider->animateInitialEntry();
	galleryFrame->setOpacity(0);
	galleryFrame->waitFor(1.8f);
	galleryFrame->fadeTo(1, 0.25f);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* GalleryPage::getArrivalSelection()
{ return getSelectedGameSnap(); }

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void GalleryPage::cameraLeaving()
{
	galleryFrame->fadeTo(0, 0.15f);
	gallerySlider->moveBy(Vec3f(0, 400, -200), 1.55f);
	gallerySlider->moveBy(Vec3f(0, -400, 200), 0.01f);

	super::cameraLeaving();
}

GalleryPage::~GalleryPage()
{
	printf("GalleryPage::~GalleryPage\n");
}

} /* namespace Glasgow */

#ifndef CHILDPAGE_H_
#define CHILDPAGE_H_

#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Include/SICollections.h"

namespace Glasgow
{

class ChildPage : public GameObject
{
	CLASSEXTENDS(ChildPage, GameObject);
public:
	ChildPage(GameObject* objectRoot);
	virtual ~ChildPage();


	/// <summary>
	/// Called when a new active user has effectively loggedin
	/// </summary>
	virtual void handleUserSet(User* user);

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Returns which direction the camera goes to when going to this page
	/// </summary>
	virtual Vec3f getCameraPlacement();

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Inpuit forwarding call to handle selection changes
	/// </summary>
	virtual void handleSelectionChange( GameObject* old, GameObject* newObj);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Animation plug for all pages to prepare for a cartridge launch
	/// Note: This animation has to be canceled at any point along its progress
	/// </summary>
	virtual void prepareForCartridgeLaunch();

	/// <summary>
	/// Cancels the animation for cartridge launch
	/// </summary>
	virtual void cancelCartridgeLaunch();

protected:
	User* currentUser;
};

}
#endif

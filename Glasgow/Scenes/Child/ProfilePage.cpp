#include "ProfilePage.h"
#include "Glasgow/Scenes/SceneManager.h"
namespace Glasgow
{

ProfilePage::ProfilePage(GameObject* rootObject) : super(rootObject)
{
	avatarChooserSlider = (SliderObject*)find("avatarSlider");
	themeChooserSlider 	= (SliderObject*)find("themeSlider");
	lastProfilesSlider  = avatarChooserSlider;

	avatarChooserSlider->setupSlider(SliderObject::SliderTypeAvatar);
	themeChooserSlider->setupSlider(SliderObject::SliderTypeTheme);
	themeChooserSlider->setItemsAsInactive();

	//findButton("namePlateButton")->keepBackgroundOn = true;
	namePlate = findButton("namePlateButton");
	namePlate->setScale(Vec3f::One*0.75f);

	//Last minute gm fix for bad selection user experience
	for( int i = 0;i<namePlate->numChildren(); i++ )
	{
		GameObject* child = namePlate->childAtIndex(i);
		if( child->typeOf(SpriteObject::type()) )
		{ ((SpriteObject*)child)->setOpacity(0); }
		else
		{ namePlate->childAtIndex(i)->setVisible(false); }
	}
	activeSlider = lastProfilesSlider;
}

/// <summary>
/// Called when a new active user has effectively loggedin
/// </summary>
void ProfilePage::handleUserSet(User* user)
{
	TextLabel* text = findButton("namePlateButton")->findLabel("text");
	text->setText(user->getDisplayName());
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void ProfilePage::animateToSwitchUsers()
{
	findButton("switchUsersButton")->setState(ButtonStateNormal);

	find("avatarLabel")->scaleTo(Vec3f::Zero, 0.45f);
	find("backButton")->scaleTo(Vec3f::Zero, 0.45f);
	find("switchUsersButton")->scaleTo(Vec3f::Zero, 0.45f);

	avatarChooserSlider->animateOutOnTop();
	themeChooserSlider->animateOutOnBottom();

	waitFor(1.55f);
	makeVisible(false);
	kill();
}

/// <summary>
/// Add our tracked items to the scene list
/// </summary>
void ProfilePage::addHotSpots( ObjectArray* trackedList )
{
	trackedList->addElement(find("backButton"));
	trackedList->addElement(find("switchUsersButton"));
	trackedList->addElement(find("namePlateButton"));

	if( activeSlider != NULL )
	{ activeSlider->addObjectsToTracking( trackedList ); }

	if( activeSlider == avatarChooserSlider )
	{ trackedList->addElement(themeChooserSlider->getSelectedItem()); }
	else
	{ trackedList->addElement(avatarChooserSlider->getSelectedItem()); }

	//debug
	//SceneManager::getInstance()->getScene()->debugLogTrackedObjects();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ProfilePage::getArrivalSelection()
{ return activeSlider->getSelectedItem(); }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ProfilePage::cameraArrived()
{
	find("avatarLabel")->scaleToOriginal(0.05f);
	find("backButton")->scaleToOriginal(0.05f);
	//find("namePlateButton")->scaleToOriginal(0.05f);
	find("switchUsersButton")->scaleToOriginal(0.05f);

	User* user = UsersManager::getInstance()->getCurrentUser();
	String avatarName = "AvatarItem15";
	String themeName = "UnderWater";
	if( user != null )
	{
		avatarName = user->getAvatar();
		themeName = user->getTheme();
	}

	avatarChooserSlider->setSelected(avatarName);
	themeChooserSlider->setSelected(themeName);

	themeChooserSlider->setPosition(Vec3f(0, -80, 0));
	themeChooserSlider->animateInitialEntry();

	avatarChooserSlider->setPosition(Vec3f(0, 40, 0));
	avatarChooserSlider->animateInitialEntry();

	themeChooserSlider->setItemsAsInactive();

	activeSlider = avatarChooserSlider;
	activeSlider->setItemsAsActive();

	fadeToAvatarLabel(1.5f);
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void ProfilePage::cameraLeaving()
{
	avatarChooserSlider->animateOutOnTop();
	themeChooserSlider->animateOutOnBottom();
	super::cameraLeaving();
}

/// <summary>
/// Animates the sliders to change position in the profile page
/// </summary>
void ProfilePage::switchSliderToAvatar()
{
	lastProfilesSlider = avatarChooserSlider;
	themeChooserSlider->setItemsAsInactive();
	avatarChooserSlider->setItemsAsActive();
	fadeToAvatarLabel(0.35f);
	activeSlider = avatarChooserSlider;
	getSelectedAvatar()->setState(ButtonStateActive);
}

/// <summary>
/// Animates the sliders to change position in the profile page
/// </summary>
void ProfilePage::switchSliderToTheme()
{
	lastProfilesSlider = themeChooserSlider;
	avatarChooserSlider->setItemsAsInactive();
	themeChooserSlider->setItemsAsActive();
	fadeToThemeLabel(0.35f);
	activeSlider = themeChooserSlider;
	getSelectedTheme()->setState(ButtonStateActive);
}

/// <summary>
/// Fades the label in on the Profiles page to show the choose avatar
/// </summary>
void ProfilePage::fadeToAvatarLabel(float waitTime)
{
	findLabel("themeLabel")->setVisible(false);
	findLabel("avatarLabel")->setVisible(false);
}

/// <summary>
/// Fades the label in on the Profiles page to show the choose theme
/// </summary>
void ProfilePage::fadeToThemeLabel(float waitTime)
{
	findLabel("themeLabel")->setVisible(false);
	findLabel("avatarLabel")->setVisible(false);
}

/// <summary>
/// Convenience function that returns the appropriate slider's selected Item casted
/// </summary>
AvatarItem* ProfilePage::getSelectedAvatar()
{ return (AvatarItem*)avatarChooserSlider->getSelectedItem(); }

/// <summary>
/// Convenience function that returns the appropriate slider's selected Item casted
/// </summary>
ThemeItem* ProfilePage::getSelectedTheme()
{ return (ThemeItem*)themeChooserSlider->getSelectedItem(); }

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ProfilePage::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight )
{
	//*****************************************************************
	// Forward to slider, since the slider can move on analog change
	// we might need to update the selections, don't allow outside scene
	//*****************************************************************
	if( activeSlider != NULL )
	{
		if ( activeSlider->onAnalogChange( objectLeft, objectEntered ) )
		{
			SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
			currentHighlight = activeSlider->getSelectedItem();
			if( currentHighlight != NULL && currentHighlight->typeOf(ButtonObject::type()) )
			{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
			return true;
		}
		else if ( objectLeft == activeSlider->getSelectedItem())
		{
			activeSlider->setItemsAsInactive();
			lastProfilesSlider = activeSlider;
			SliderObject* other = avatarChooserSlider;

			if( activeSlider == avatarChooserSlider && objectEntered == themeChooserSlider->getSelectedItem() )
			{
				printf("Switch slider to theme\n");
				activeSlider = themeChooserSlider;
				activeSlider->setItemsAsActive();
				currentHighlight = activeSlider->getSelectedItem();
				SceneManager::getInstance()->getScene()->playSound("Base_Highlight");
				((ButtonObject*)currentHighlight)->setState(ButtonStateActive);
				return true;
			}
			else if( activeSlider == themeChooserSlider && objectEntered == avatarChooserSlider->getSelectedItem() )
			{
				printf("Switch slider to avatar\n");
				SceneManager::getInstance()->getScene()->playSound("Base_Highlight");
				activeSlider = avatarChooserSlider;
				activeSlider->setItemsAsActive();
				currentHighlight = activeSlider->getSelectedItem();
				((ButtonObject*)currentHighlight)->setState(ButtonStateActive);
				return true;
			}
			else
			{
				if( objectLeft == avatarChooserSlider->getSelectedItem() && objectEntered == namePlate )
				{
					printf("No active slider\n");
					SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
					currentHighlight = findButton("backButton");
					((ButtonObject*)currentHighlight)->setState(ButtonStateActive);
					activeSlider = null;
					return true;
				}
				else if( objectLeft == avatarChooserSlider->getSelectedItem() && objectEntered == findButton("switchUsersButton") )
				{
					printf("No active slider\n");
					SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
					currentHighlight = objectEntered;
					((ButtonObject*)avatarChooserSlider->getSelectedItem())->setState(ButtonStateNormal);
					((ButtonObject*)objectEntered)->setState(ButtonStateActive);
					activeSlider = null;
					return true;
				}
			}
		}
	}
	else if ( objectEntered != null && (objectEntered->name == "SliderLeft" || objectEntered->name == "SliderRight" ) )
	{
		if( currentHighlight != null )
		{ ((ButtonObject*)currentHighlight)->setState( ButtonStateNormal ); }
		SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
		activeSlider = lastProfilesSlider;
		activeSlider->setItemsAsActive();
		currentHighlight = activeSlider->getSelectedItem();
		((ButtonObject*)currentHighlight)->setState( ButtonStateActive );
		return true;
	}
	else if ( objectLeft== find("backButton") && objectEntered == namePlate )
	{
		SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
		((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
		currentHighlight = findButton("switchUsersButton");
		((ButtonObject*)currentHighlight)->setState(ButtonStateActive);
		return true;
	}
	else if ( objectLeft== find("switchUsersButton") && objectEntered == namePlate )
	{
		SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
		((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
		currentHighlight = findButton("backButton");
		((ButtonObject*)currentHighlight)->setState(ButtonStateActive);
		return true;
	}
	else if ( objectEntered != null && objectEntered->typeOf(AvatarItem::type()))
	{
		if( objectLeft != null )
		{ ((ButtonObject*)objectLeft)->setState(ButtonStateNormal); }
		((AvatarItem*)objectEntered)->setState(ButtonStateActive);
		SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
		currentHighlight = avatarChooserSlider->getSelectedItem();
		avatarChooserSlider->setItemsAsActive();
		activeSlider = avatarChooserSlider;
		return true;
	}
	else if ( objectEntered != null && objectEntered->typeOf(ThemeItem::type()) )
	{
		if( objectLeft != null )
		{ ((ButtonObject*)objectLeft)->setState(ButtonStateNormal); }
		((ThemeItem*)objectEntered)->setState(ButtonStateActive);
		SceneManager::getInstance()->getScene()->playSound("ProfCustom_Scroll");
		currentHighlight = themeChooserSlider->getSelectedItem();
		themeChooserSlider->setItemsAsActive();
		activeSlider = themeChooserSlider;
		return true;
	}

	//*****************************************************************
	// Normally we dont over ride the base scene logic by directly assigning
	//but here we dont want a selection change to be triggered and still
	//mantain the logically correct options
	//*****************************************************************
	return false;
}


ProfilePage::~ProfilePage()
{
	printf("ProfilePage::~ProfilePage\n");
}

} /* namespace Glasgow */

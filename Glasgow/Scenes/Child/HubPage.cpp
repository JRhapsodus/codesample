#include "HubPage.h"
#include "SIEngine/Include/SIActions.h"
#include "SIEngine/Include/SIMaterials.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIRendering.h"
#include "Glasgow/Particles/Particle.h"
#include "Glasgow/Particles/ParticleEmitterObject.h"

namespace Glasgow
{

HubPage::HubPage(GameObject* rootObject) : super(rootObject)
{
	lastContentItemPlayed = null;
	packages = null;
	centerRing 			= find("Center");
	toLeftButton 		= findSprite("RotateLeftButton");
	toRightButton 		= findSprite("RotateRightButton");
	toLeftIcon 			= findSprite("arrowLeft");
	toRightIcon 		= findSprite("arrowRight");
	rootTray 			= find("Tray");
	locker				= rootTray->find("locker");
	profileIcon			= locker->find("profileIcon");
	parentSettingsButton= rootTray->findButton("parentSettings");
	rewardCenterButton  = rootTray->findButton("rewardCenterButton");
	toLeftButton->setOpacity(0);
	toRightButton->setOpacity(0);
	toLeftIcon->setOpacity(0);
	toRightIcon->setOpacity(0);

	parentSettingsButton->scaleOnActiveAmount *= 3;
	rewardCenterButton->scaleOnActiveAmount *= 3;
	currentPage = 0;
	ringRadius = 600;
	anglesPerCluster = 0;

	GameObject* indicatorRoot = find("Indicators");
	for( int i = 0; i<50; i++ )
	{
		GameObject* indicator = indicatorRoot->find(String("Indicator") + (i+1) );
		if( indicator != NULL )
		{
			indicator->tag = i;
			indicators.addElement(indicator);
			((GameObject*)indicators[i])->setVisible(false);
		}
	}


	printf("Root Tray position\n");
	rootTray->getPosition().debugLog();
	currentScene = NULL;
}


HubPage::~HubPage()
{
	ringPages.removeAllElements();
	SAFE_RELEASE(packages);
}

/// <summary>
/// Custom response to pressaction
/// </summary>
bool HubPage::handlePressAction(GameObject*& currentHighlight)
{
	printf( "HubPage::handlePressAction %s \n", currentHighlight->name.str() );

	if( currentScene == NULL )
	{
		currentScene = (ChildScene*)SceneManager::getInstance()->getScene();
		printf( "CurrentScene is set: %s \n", currentScene );
	}

	if( currentScene->isWandEnabled() )
	{
		CameraObject* bgCamera = currentScene->getCamera(0);
		CameraObject* contentCamera = currentScene->getCamera(1);

		if ( currentHighlight->name == "RotateLeftButton" )
		{
			rotateRingLeft();
			((ButtonObject*)currentHighlight)->setState(ButtonStateNormal);
			currentHighlight = null;
			toLeftIcon->fadeTo(0, 0.25f);
			currentScene->lockOutInput(0.5f);
			return true;
		}
		else if ( currentHighlight->name == "RotateRightButton" )
		{
			rotateRingRight();
			((ButtonObject*)currentHighlight)->setState(ButtonStateNormal);
			currentHighlight = null;
			toRightIcon->fadeTo(0, 0.25f);
			currentScene->lockOutInput(0.5f);
			return true;
		}
	}

	return false;
}

/// <summary>
/// Cancels the animation for cartridge launch
/// </summary>
void HubPage::cancelCartridgeLaunch()
{
	rootTray->cancelActions();
	slideTrayUp(0.25f);

	for( int i = 0; i<ringPages.getSize();i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		cluster->staggerInForCancelLaunch();
	}
}

/// <summary>
/// Retuurns the cluster that is front focused
/// </summary>
RingCluster* HubPage::getCurrentCluster()
{
	if( currentPage < ringPages.getSize() )
	{ return ((RingCluster*)ringPages[currentPage]); }


	return null;
}

/// <summary>
/// Cartridge removed
/// </summary>
void HubPage::handleReturnToApp()
{
	printf("HubPage::handleCartridgeRemoved()\n");
	cancelAnimateForItemLaunch();
	moveClusters(0);
}


/// <summary>
/// Cartridge removed
/// </summary>
void HubPage::handleCartridgeRemoved()
{
	printf("HubPage::handleCartridgeRemoved()\n");
	cancelAnimateForItemLaunch();

	for ( int i = 0; i<ringPages.getSize(); i++ )
	{
		RingCluster* newCluster = (RingCluster*)ringPages.elementAt(i);
		newCluster->cancelActions();
	}

	for( int i = 0; i<ringPages.getSize();i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		cluster->removeCartridgeItems();
		if( cluster->getNumItems() == 0 )
		{
			cluster->waitFor(2);
			cluster->kill();
			ringPages.removeElementAt(i);
			currentPage--;
			if( currentPage < 0 )
				currentPage = 0;

			anglesPerCluster = 360.0f/(float)(ringPages.getSize());
			centerRing->cancelActions();
			centerRing->rotateTo(Quaternion::angleAxis(anglesPerCluster*currentPage, Vec3f::Up), 0.25f);
		}
	}

	moveClusters(0);
}

/// <summary>
/// Forwarding call from childscene
/// </summary>
void HubPage::handleCartridgeReady()
{
	printf("HubPage::handleCartridgeReady()\n");
	//**********************************************************
	// In case a rogue timer triggered twice (?) or wasn't
	// stopped at the appropriate time, lets see if we already
	// have an item in the scene graph representing a cart item
	//**********************************************************
	for ( int i = 0; i<ringPages.getSize(); i++ )
	{
		RingCluster* newCluster = (RingCluster*)ringPages.elementAt(i);
		ContentItem* cartItem = newCluster->getCartridgeItem();

		if( cartItem != null )
		{ return; }
	}

	//**********************************************************
	// Cancel any actions / animations that are mid run
	//**********************************************************
	centerRing->cancelActions();
	for ( int i = 0; i<ringPages.getSize(); i++ )
	{
		RingCluster* newCluster = (RingCluster*)ringPages.elementAt(i);
		newCluster->cancelActions();
	}

	//**********************************************************
	// Grab the information about the package for the cartridge
	// and create a new cluster
	//**********************************************************
	PackageInfo* cartridgeInfo = ContentManager::getInstance()->getCartridgePackage();
	if ( cartridgeInfo != null )
	{
		makeNewCluster();
		RingCluster* newCluster = (RingCluster*)ringPages.lastElement();

		//**********************************************************
		// Preload the texture, so open gl doesn't load on scene
		// graph update, ie: treat as a synchronized block
		//**********************************************************
		TextureManager::getInstance()->loadPng( cartridgeInfo->iconPath );
		ContentItem* contentItem = makeNewContentItem("PetPlanetContentItem", cartridgeInfo);
		newCluster->addContentItem(contentItem);

		//**********************************************************
		// Reset angles due to new cluster and default page
		//**********************************************************
		printf("Angle per cluster: %f\n", anglesPerCluster);
		positionClusters();
		contentItem->waitFor(0.5f);
		contentItem->bubbleIn(0.25f);
		contentItem->changeRingColorFast(getThemeColor());
		currentPage = ringPages.getSize()-1;
		centerRing->rotateTo(Quaternion::angleAxis(anglesPerCluster*currentPage, Vec3f::Up), 0.25f);
	}
}

/// <summary>
/// Animation plug for all pages to prepare for a cartridge launch
/// Note: This animation has to be canceled at any point along its progress
/// </summary>
void HubPage::prepareForCartridgeLaunch()
{
	printf("HubPage::prepareForCartridgeLaunch()\n");
	rootTray->cancelActions();
	slideTrayDown(0.5f);
	float delay = 0.15f;
	for( int i = 0; i<ringPages.getSize();i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		cluster->cancelActions();
		cluster->staggerOutForLaunch(delay);
		cluster->waitFor(3*delay);

		if( i%2 == 0 )
		{ delay += 0.15f; }
	}
}

/// <summary>
/// Animates all pages and rings going out
/// </summary>
void HubPage::staggerOutContentRings()
{
	float delay = 0.15f;
	for( int i = 0; i<ringPages.getSize();i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		cluster->staggerOutContentRings(delay);
		cluster->waitFor(3*delay);
		cluster->kill();

		if( i%2 == 0 )
		{ delay += 0.15f; }
	}

	ringPages.removeAllElements();
	centerRing->waitFor(delay);
	centerRing->rotateTo(Quaternion::identity, 0.01f);
}

/// <summary>
/// Do not allow cameraleaving from ChildPage to destroy us, but do nothing
/// </summary>
void HubPage::cameraLeaving()
{ }

/// <summary>
/// Called when a new active user has effectively loggedin
/// </summary>
void HubPage::handleUserSet(User* user)
{
	TextLabel* userName = rootTray->findLabel("profileName");
	TextLabel* userNameShadow = rootTray->findLabel("profileShadow");
	userName->setText(user->getDisplayName());
	userNameShadow->setText(user->getDisplayName());
}

/// <summary>
/// Gets the cluster at the index provided
/// </summary>
RingCluster* HubPage::getCluster(int index)
{ return (RingCluster*)ringPages[index]; }

/// <summary>
/// Animating from return from item launch
/// </summary>
void HubPage::animateFromItemLaunch()
{
	if( itemLaunched != null )
	{
		slideTrayUp(2);
		itemLaunched->cancelActions();
		itemLaunched->moveTo(originalLaunchPosition, 0.5f);
		itemLaunched->setState(ButtonStateActive);

		float delay = 0.15f;
		for( int i = 0; i<ringPages.getSize();i++ )
		{
			RingCluster* cluster = (RingCluster*)ringPages[i];
			printf("Stagger In %d\n", i);
			cluster->staggerInContentRings(delay);

			if( i%2 == 0 )
			{ delay += 0.15f; }
		}
	}
}

/// <summary>
/// Animation for when an item is launched
/// </summary>
void HubPage::cancelAnimateForItemLaunch()
{
	rootTray->cancelActions();
	slideTrayUp(0.5f);

	for( int i = 0; i<ringPages.getSize();i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		cluster->staggerInForCancelLaunch();
		if( cluster->contains(itemLaunched) )
		{
			itemLaunched->cancelActions();
			itemLaunched->moveTo(originalLaunchPosition, 0.25f);
		}
	}

	itemLaunched = null;
}


/// <summary>
/// Animation for when an item is launched
/// </summary>
void HubPage::animateForItemLaunch(ContentItem* toLaunch)
{
	currentScene->lockOutInput(5.0f);

	itemLaunched = toLaunch;
	currentScene->playSound("Base_LaunchApp");
	slideTrayDown(2);

	float delay = 0.15f;
	for( int i = 0; i<ringPages.getSize();i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		printf("Stagger out %d\n", i);
		cluster->staggerOutContentRings(delay);
		if( cluster->contains(toLaunch) )
		{
			originalLaunchPosition = toLaunch->getPosition();
			toLaunch->cancelActions();
			toLaunch->waitFor(0.25);
			toLaunch->moveTo( Vec3f::Zero, 0.5f );
			toLaunch->scaleBy( Vec3f::One*1.25f, 0.5 );
			toLaunch->waitFor(5);
		}
		else
		{ cluster->waitFor(3*delay); }

		if( i%2 == 0 )
		{ delay += 0.15f; }
	}
}

/// <summary>
/// Helper function for creating a new cluster for content items to go into
/// </summary>
void HubPage::makeNewCluster()
{
	RingCluster* cluster = new RingCluster( ringPages.getSize(), getThemeColor() );
	centerRing->addChild(cluster);
	ringPages.addElement(cluster);
}

/// <summary>
/// Returns the color for a given theme
/// </summary>
Color HubPage::getThemeColor()
{
	String themeName = "UnderWater";

	if( currentUser != null )
	{ themeName = currentUser->getTheme(); }

	if( themeName.equalsIgnoreCase("UnderWater") )
	{ return Color( 0/255.0f, 191.0f/255.0f, 255.0f/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Fruit") )
	{ return Color( 253.0f/255.0f, 17/255.0f, 172.0f/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Cave") )
	{ return Color( 198.0f/255.0f, 255.0f/255.0f, 0/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Space") )
	{ return Color( 108.0f/255.0f, 0/255.0f, 255/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Waterfall") )
	{ return Color( 0/255.0f, 200.0f/255.0f, 184.0f/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Wildwest") )
	{ return Color( 198.0f/255.0f, 255.0f/255.0f, 0/255.0f, 1 ); }

	return Color::Blue;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void HubPage::cameraArrived()
{
	displayIndicators(2.0f);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* HubPage::getArrivalSelection()
{
	RingCluster* currentCluster = getCurrentCluster();
	if( lastContentItemPlayed != null )
	{
		ContentItem* returnItem = lastContentItemPlayed;
		lastContentItemPlayed = null;
		return returnItem;
	}

	return currentCluster->getMiddleItem();
}

/// <summary>
/// Displays Indicators of location
/// </summary>
void HubPage::displayIndicators(float delay)
{
	 int totalPages = ringPages.getSize();
	 float nextX = 0;
	 for( int i = 0; i<totalPages; i++ )
	 {
		GameObject* indicator = (GameObject*)indicators[i];
		GameObject* indicatorOn = indicator->find("on");
		GameObject* indicatorOff = indicator->find("off");

		indicator->cancelActions();
		indicatorOn->cancelActions();
		indicatorOff->cancelActions();

		float farLeft = -25*((int)totalPages/2);
		indicator->setPosition( Vec3f( farLeft + 25*i, 340, -500) );
		indicator->setVisible(true);

		indicatorOn->waitFor(delay);
		indicatorOff->waitFor(delay);

		indicatorOff->makeVisible(true);
		indicatorOff->setOpacity(1);
		indicatorOn->makeVisible(true);
		indicatorOn->setOpacity(0);

		if( i == currentPage )
		{ indicatorOn->fadeTo(1, 0.25f); }

		indicatorOn->waitFor(3 - 0.25f);
		indicatorOn->fadeTo(0, 0.25f);
		indicatorOff->waitFor(3);
		indicatorOff->fadeTo(0, 0.25f);
	 }
}

/// <summary>
/// Call this to query Package Manager and fill a list of Packagedata
/// </summary>
void HubPage::fillAppsFromDataBase()
{
	SAFE_RELEASE(packages);
	currentUser = UsersManager::getInstance()->getCurrentUser();
	packages = ContentManager::getInstance()->getPlayablePackages();

	//************************************************************************************************************
	//Do not process packages that are hideable if the current user has sneak peeks turned off
	//************************************************************************************************************
	for( int i = 0; i<packages->getSize(); i++ )
	{
		PackageInfo* info = (PackageInfo*)packages->elementAt(i);
		if( !currentUser->getSneakPeeksEnabled() && info->hideableType )
		{  packages->removeElementAt(i); i--; }
	}
}

/// <summary>
/// Scans and finds an unfilled cluster in even/odd order front to back
/// </summary>
RingCluster* HubPage::findAvailableCluster()
{
	int numClusters = ringPages.getSize();

	//***************************************************************
	// First 3 rings are special cased
	//***************************************************************
	if ( numClusters == 1 )
	{ return getCluster(0); }

	RingCluster *min = NULL;
	for( int i = 0; i<numClusters; i++ )
	{
		if ( min == NULL )
		{ min = getCluster(i); }
		else
		{
			RingCluster* test = getCluster(i);
			if( test->getNumItems() < min->getNumItems() )
			{ min = test; }
		}
	}

	return min;
}

/// <summary>
/// Fills the hub with ring pages and content items
/// </summary>
void HubPage::fillHub()
{
	printf("Filling hub \n");
	fillAppsFromDataBase();

	centerRing->setPosition( Vec3f(0, 0, 300));
	centerRing->setRotation(Quaternion::identity);

	//************************************************************************************************************
	// We know there is a minimum of 1 ring cluster
	//************************************************************************************************************
	makeNewCluster(); //id 0

	//************************************************************************************************************
	// Now lets fill the clusters with the appropriate number of content items
	//************************************************************************************************************
	//getCluster(0)->addContentItem(TRelease<ContentItem>(makeNewContentItem("FlappyContentItem", null)));

	//************************************************************************************************************
	// Calculate and make the required number of clusters
	//************************************************************************************************************
	int totalItems = packages->getSize() + 1;
	int numClusters = (int)(totalItems/3);
	numClusters = (totalItems % 3 > 0) ? numClusters+1 : numClusters;
	printf("Total Packages: %d Placed into %d clusters\n", totalItems, numClusters);

	//************************************************************************************************************
	//we already created the first by default
	//************************************************************************************************************
	for( int i = 1; i<numClusters; i++ )
	{ makeNewCluster(); }

	//************************************************************************************************************
	// Now add the content items to the firstAvailableCluster, this algorithm needs to be deterministic
	//************************************************************************************************************
	printf("There are %d packages\n", packages->getSize());
	for( int i = 0; i<packages->getSize(); i++ )
	{
		PackageInfo* info = (PackageInfo*)packages->elementAt(i);
		TRelease<ContentItem> contentItem( makeNewContentItem("PetPlanetContentItem", info));

		//************************************************************************************************************
		// If the package is a cartridge, then create its own dedicated cluster, otherwise get the first available
		//************************************************************************************************************
		if( info->isCartridge() )
		{
			makeNewCluster();
			RingCluster* clusterToAdd = (RingCluster*)ringPages.lastElement();
			clusterToAdd->addContentItem(contentItem);
		}
		else
		{
			RingCluster* clusterToAdd = findAvailableCluster();
			clusterToAdd->addContentItem(contentItem);
		}
	}

	//************************************************************************************************************
	// Position ring clusters
	//************************************************************************************************************
	int pageWithLastGame = -1;
	printf("Looking for last game\n");
	String lastGame = SystemSettings::getLastGame();
	for( int i = 0; i<ringPages.getSize(); i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		ContentItem* item = cluster->getPackageID(lastGame);
		if( item != null )
		{
			pageWithLastGame = i;
			lastContentItemPlayed = item;
			SystemSettings::setLastGame("");
			break;
		}
	}

	printf("Last Game is in cluster %d\n", pageWithLastGame);
	anglesPerCluster = 360.0f/(float)(ringPages.getSize());
	printf("Angle per cluster: %f\n", anglesPerCluster);
	positionClusters();
	displayIndicators(2.0f);
	staggerInContentRings();

	if( pageWithLastGame != -1 )
	{
		currentPage = pageWithLastGame;
		centerRing->setRotation(Quaternion::angleAxis(anglesPerCluster*pageWithLastGame, Vec3f::Up));
	}
	else
	{
		currentPage = 0;
		centerRing->setRotation(Quaternion::identity);
	}
}

/// <summary>
/// Called when the theme changes
/// </summary>c
void HubPage::onThemeChange()
{
	for( int i = 0; i<ringPages.getSize(); i ++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		cluster->changeColor(getThemeColor());
	}
}

/// <summary>
/// Helper function for creating content items with the various settings
/// </summary>
ContentItem* HubPage::makeNewContentItem( String prefabName, PackageInfo* info )
{
	printf("HubPage::makeNewContentItem\n");
	String iconPath = "";
	String appName  = "Default Filler";
	if ( info != null )
	{
		iconPath = info->iconPath;
		appName  = info->appName;
	}

	ContentItem* contentItem = (ContentItem*)Scene::instantiate(prefabName);
	contentItem->setContentType(ContentApp);

	if( prefabName == "FlappyContentItem" )
	{ appName = "Flappy Frog"; }

	if( info->isVideoBundle )
	{ contentItem->setBannerText( ((VideoInfo*)info->videos.elementAt(0))->bundleTitle );
	}
	else
	{ contentItem->setBannerText(appName); }
	contentItem->setPackageInfo(info);

	if( info->isCartridge() )
	{ printf("Testing for cartridge icon path existence!!!\n"); }

	if( AssetLibrary::fileExists(iconPath) )
	{
		if( info->isCartridge() )
		{ printf("Loading cartridge %s!!!\n", iconPath.str()); }
		contentItem->setBackGroundImage(iconPath);
	}

	Vec3f scale = contentItem->originalScale * 0.9f;
	contentItem->setScale(scale);
	contentItem->originalScale = scale;
	return contentItem;
}

#define DEG_TO_RAD 0.0174532925

/// <summary>
/// Position all the clusters around the ring
/// </summary>
void HubPage::positionClusters()
{
	printf("HubPage::positionClusters()\n");
	float currentPolarAngle = -90;
	anglesPerCluster = 360.0f/(float)(ringPages.getSize());

	for( int i = 0; i<ringPages.getSize(); i++ )
	{
		RingCluster* cluster = getCluster(i);

		if( cluster == null )
		{
			printf("Cluster is null\n");
		}

		cluster->setPosition(Vec3f(cosf(currentPolarAngle*DEG_TO_RAD)*ringRadius, 0, sinf(currentPolarAngle*DEG_TO_RAD)*ringRadius));
		cluster->originalPos = cluster->getPosition();
		currentPolarAngle += anglesPerCluster;
		cluster->setNaturalPosition(i);
	}
}

/// <summary>
/// Position all the clusters around the ring
/// </summary>
void HubPage::moveClusters(int except)
{
	float currentPolarAngle = -90;
	anglesPerCluster = 360.0f/(float)(ringPages.getSize());

	for( int i = 0; i<ringPages.getSize(); i++ )
	{
		RingCluster* cluster = getCluster(i);
		Vec3f position = Vec3f(cosf(currentPolarAngle*DEG_TO_RAD)*ringRadius, 0, sinf(currentPolarAngle*DEG_TO_RAD)*ringRadius);
		cluster->setPosition(position);
		cluster->originalPos = position;
		currentPolarAngle += anglesPerCluster;
		cluster->moveNaturalPosition(i, 0.01f);
	}
}

/// <summary>
/// Replaces the main avatar
/// </summary>
void HubPage::replaceAvatar(AvatarItem* avatarToClone)
{
	avatarToClone->cancelActionsAsComplete();
	avatarToClone->setPosition( Vec3f(0,0,50) );

	profileIcon->getWorldPosition().debugLog();
	rootTray->getWorldPosition().debugLog();
	currentScene->playSound("Home_ChangeCharacter_alt");
	avatarToClone->getParent()->getWorldPosition().debugLog();
	AvatarItem* currentAvatar = (AvatarItem*)profileIcon;

	printf("Replacing %s avatar using %s \n", currentAvatar->name.str(), avatarToClone->name.str());
	Vec3f oldScale = currentAvatar->originalScale;
	currentAvatar->setVisible(false);

	String prefabName = avatarToClone->getPrefabCloneName();

	if( currentUser != NULL )
	{
		printf("Saving user %s \n", currentUser->getDisplayName().str());
		currentUser->setAvatar(prefabName);
		currentUser->save();
	}

	currentAvatar->removeFromParent();
	avatarToClone->setVisible(false);
	avatarToClone->waitFor(2);
	avatarToClone->makeVisible(true);

	Vec3f pos = avatarToClone->getPosition();
	Vec3f scale = avatarToClone->getScale();
	Quaternion rot = avatarToClone->getRotation();
	AvatarItem* newAvatarIcon = (AvatarItem*)Scene::instantiate(prefabName);
	newAvatarIcon->originalScale = oldScale;
	newAvatarIcon->name = "profileIcon";
	newAvatarIcon->setPosition(pos);
	newAvatarIcon->setScale(scale);
	newAvatarIcon->setRotation(rot);
	avatarToClone->parent->addChild(newAvatarIcon);
	newAvatarIcon->release();
	profileIcon = newAvatarIcon;

	locker->setRotation(Quaternion::identity);
	locker->adoptChild(newAvatarIcon);
	locker->rotateTo(Quaternion::angleAxis(180, Vec3f::Right), 1.5f);



	printf("Old scale: \n");
	oldScale.debugLog();
	locker->getWorldPosition().debugLog();

	newAvatarIcon->scaleTo(oldScale, 1.5f);
	TimerManager::addTimer("Starburst", 1.45f, 0 );
	printf("Done replacing avatar\n");
}

/// <summary>
/// Spawns a particle effect
/// </summary>
void HubPage::doStarBurst()
{
	SceneManager::getInstance()->getScene()->playSound("Base_AvatarSelectionStars");

	for( int i = 0; i<50; i++ )
	{
		TRelease<SpriteData> spriteData( new SpriteData("starParticle"));
		TRelease<Particle> particle( new Particle( spriteData) );

		particle->lifeSpan = 1+randomFloat(0, 3);
		particle->gravity = Vec3f::Up*60;

		float randomX = -10 + randomFloat(0, 20);
		float randomY = 5 - randomFloat(0, 20);
		float randomZ = -15 + randomFloat(0, 15);

		particle->rotationRate = (int) (-500 + randomFloat(0, 1) * 1000);
		particle->velocity = Vec3f(randomX, randomY, randomZ)*10;
		particle->startColor = Color( 1, 1, 1 ,1 );
		particle->endColor = Color( 1, 1, 1 , 0  );
		particle->setSize( Size( 25, 25 ) );
		particle->setScale( Vec3f::One );
		particle->setMaterial(TRelease<TintMaterial>(new TintMaterial(particle)));
		particle->fadeTo(0, particle->lifeSpan);
		locker->addChild(particle);

		AvatarItem* currentAvatar = (AvatarItem*)profileIcon;
		particle->setRotation(currentAvatar->getRotation());
		particle->setPosition(currentAvatar->getPosition());
	}
}

/// <summary>
/// Animates all pages and rings coming in
/// Different than calling slideTray with 0 because that requires the use of actions
/// and objects not on the scene graph do not get called Update to progress the actions
/// </summary>
void HubPage::setTrayDown()
{ rootTray->setPosition( Vec3f(0, -262, -146) + Vec3f::Down*800); }

/// <summary>
/// Animates all pages and rings coming in
/// </summary>
void HubPage::animateAvatarItemIntoTray(AvatarItem* selected)
{
	selected->fadeNameOut();

	locker->setRotation(Quaternion::identity);
	TRelease<MultiAction> multi(new MultiAction(selected));

	// We use the Unity data here since changing the avatars can significantly change the transform data
	// that this new profileIcon should be Set to due to the locker rotating different icons into place.
	// as such we don't know if the locker has the perfect transform set at the time of this action
	multi->addAction( TRelease<MoveToAction>(new MoveToAction(selected, Vec3f(-1, -100, 324), 1.0f) ));
	multi->addAction( TRelease<ScaleToAction>(new ScaleToAction(selected, Vec3f::One, 1.0f) ));
	profileIcon->removeFromParent();

	selected->scaleOnActiveAmount *= 4;
	selected->name = "profileIcon";
	locker->adoptChild(selected);
	selected->originalScale =  Vec3f::One;
	selected->waitFor(0.5f);
	selected->queueAction(multi);
	profileIcon = selected;
	rootTray->waitFor(0.5);
	slideTrayUp(1.0f);
}

/// <summary>
/// Returns which direction the camera goes to when going to this page
/// </summary>
Vec3f HubPage::getCameraPlacement()
{ return Vec3f::Forward; }

/// <summary>
/// Add our tracked items to the scene list
/// </summary>
void HubPage::addHotSpots( ObjectArray* trackedList )
{
	printf("HubPage::addHotSpots\n");
	trackedList->addElement(toLeftButton);
	trackedList->addElement(toRightButton);
	trackedList->addElement(profileIcon);
	trackedList->addElement(rootTray->findButton("parentSettings"));
	trackedList->addElement(rootTray->findButton("rewardCenterButton"));

	if( getCluster(currentPage) != NULL )
	{ getCluster(currentPage)->addItemsToTracking(trackedList); }

	super::addHotSpots(trackedList);
}

/// <summary>
/// Sets the main hub's avatar icon using the prefab name
/// </summary>
void HubPage::setAvatarIcon(String avatarPrefabName)
{
	profileIcon->removeFromParent();

	AvatarItem* avatar = (AvatarItem*) Scene::instantiate(avatarPrefabName);
	avatar->originalScale = Vec3f::One;
	avatar->name = "profileIcon";
	avatar->setPosition( Vec3f(-1, -100, 324));
	avatar->setScale(Vec3f::One);
	avatar->setRotation(Quaternion::identity);
	locker->addChild(avatar);
	avatar->release();
	profileIcon = avatar;
}

/// <summary>
/// Animate main hub tray sliding down
/// </summary>
void HubPage::slideTrayDown(float time)
{
	rootTray->moveTo(Vec3f(0, -262, -146) + Vec3f::Down*800, time);
}

/// <summary>
/// Animate main hub tray sliding up
/// </summary>
void HubPage::slideTrayUp(float time)
{
	rootTray->moveTo(Vec3f(0, -262, -146), time);
}

/// <summary>
/// Animates all pages and rings coming in
/// </summary>
void HubPage::staggerInContentRings()
{
	float delay = 1.0f;
	for( int i = 0; i<ringPages.getSize();i++ )
	{
		RingCluster* cluster = (RingCluster*)ringPages[i];
		cluster->waitFor(delay);
		cluster->staggerInContentRings(delay);

		if( i%2 == 0 )
		{ delay -= 0.25f; }
	}
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool HubPage::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& highlight)
{
	//*****************************************************************
	// Turn on scroller icons if on the far edges
	//*****************************************************************
	currentScene = (ChildScene*)SceneManager::getInstance()->getScene();
	CameraObject* bgCamera = currentScene->getCamera(0);
	CameraObject* contentCamera = currentScene->getCamera(1);
	bool wandEnabled = currentScene->isWandEnabled();

	SceneManager::getInstance()->getScene()->playSound("Base_Highlight");

	//*****************************************************************
	// This is an override of the custom selection orders to provide
	// a specific goto for usability
 	//*****************************************************************
	RingCluster* activeCluster = getCluster(currentPage);
	if ( activeCluster != null )
	{
		ContentItem* left = activeCluster->getLeftItem();
		ContentItem* right = activeCluster->getRightItem();

		int numItems = activeCluster->getNumItems();

		if( numItems == 3)
		{
			ContentItem* center = activeCluster->getItem(0);
			if( center!= null && objectLeft != null && objectEntered != null && left != null && right != null )
			{
				if( objectLeft == left && objectEntered == right )
				{
					((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
					highlight = center;
					((ButtonObject*)highlight)->setState(ButtonStateActive);
					return true;
				}

				if( objectLeft == right && objectEntered == left )
				{
					((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
					highlight = center;
					((ButtonObject*)highlight)->setState(ButtonStateActive);
					return true;
				}

				if( objectLeft == center && objectEntered == toRightButton )
				{
					((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
					highlight = right;
					((ButtonObject*)highlight)->setState(ButtonStateActive);
					return true;
				}

				if( objectLeft == center && objectEntered == toLeftButton )
				{
					((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
					highlight = left;
					((ButtonObject*)highlight)->setState(ButtonStateActive);
					return true;
				}

			}
		}

		//goto center rather than edge
		if( objectEntered == parentSettingsButton && objectLeft == left && left != null )
		{
			((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
			highlight = profileIcon;
			((ButtonObject*)highlight)->setState(ButtonStateActive);
			return true;
		}
		else if( objectEntered == parentSettingsButton && objectLeft == right && right != null )
		{
			((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
			highlight = profileIcon;
			((ButtonObject*)highlight)->setState(ButtonStateActive);
			return true;
		}

		//goto center rather than edge
		if( objectEntered == rewardCenterButton && objectLeft == left && left != null )
		{
			((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
			highlight = profileIcon;
			((ButtonObject*)highlight)->setState(ButtonStateActive);
			return true;
		}
		else if( objectEntered == rewardCenterButton && objectLeft == right && right != null )
		{
			((ButtonObject*)objectLeft)->setState(ButtonStateNormal);
			highlight = profileIcon;
			((ButtonObject*)highlight)->setState(ButtonStateActive);
			return true;
		}
	}

	if (objectEntered == toLeftButton)
	{
		if( objectLeft != null )
		{ ((ButtonObject*)objectLeft)->setState(ButtonStateNormal); }
		rotateRingLeft();
		highlight = ((RingCluster*)ringPages[currentPage])->getRightItem();
		((ButtonObject*)highlight)->setState(ButtonStateActive);

		return true;
	}
	else if (objectEntered == toRightButton)
	{
		if( objectLeft != null )
		{ ((ButtonObject*)objectLeft)->setState(ButtonStateNormal); }

		rotateRingRight();

		highlight = ((RingCluster*)ringPages[currentPage])->getLeftItem();
		((ButtonObject*)highlight)->setState(ButtonStateActive);

		return true;
	}


	return false;
}

/// <summary>
/// Rotates the center ring effectively bringing the next RingPage into focus
/// </summary>
void HubPage::rotateRingRight()
{
	currentScene->lockOutInput(1.05f);
	centerRing->rotateBy(Quaternion::angleAxis(anglesPerCluster, Vec3f::Up), 1.5f);

	CameraObject* bgCamera = currentScene->getCamera(0);
	CameraObject* contentCamera = currentScene->getCamera(1);

	currentScene->playSound("Base_RightScroll");

	contentCamera->rotateTo(Vec3f(0, 2.5f, 0), 0.75f);
	bgCamera->rotateTo(Vec3f(0, 1.5f, 0), 0.75f);
	contentCamera->rotateTo(Quaternion::identity, 0.75f);
	bgCamera->rotateTo(Quaternion::identity, 0.75f);

	currentPage = (currentPage+1)%ringPages.getSize();
	displayIndicators(0.25f);
	printf("Rotating to cluster %d\n", currentPage);
}

/// <summary>
/// Rotates the center ring effectively bringing the next RingPage into focus
/// </summary>
void HubPage::rotateRingLeft()
{

	currentScene->lockOutInput(1.05f);

	centerRing->rotateBy(Quaternion::angleAxis(-anglesPerCluster, Vec3f::Up), 1.5f);
	currentPage--;
	if( currentPage < 0 )
	{ currentPage = ringPages.getSize()-1; }

	currentScene->playSound("Base_LeftScroll");

	CameraObject* bgCamera = currentScene->getCamera(0);
	CameraObject* contentCamera = currentScene->getCamera(1);
	contentCamera->rotateTo(Vec3f(0, -2.5f, 0), 0.75f);
	bgCamera->rotateTo(Vec3f(0, -1.5f, 0), 0.75f);
	contentCamera->rotateTo(Quaternion::identity, 0.75f);
	bgCamera->rotateTo(Quaternion::identity, 0.75f);

	displayIndicators(0.25f);
	printf("Rotating to cluster %d\n", currentPage);
}

/// <summary>
/// Rotates the center ring effectively bringing the next RingPage into focus
/// </summary>
void HubPage::rotateToLanding()
{
	if(currentPage == 0)
		return;

	currentScene->lockOutInput(1.05f);

	centerRing->rotateTo( Quaternion::identity , 1.5f );
	currentPage = 0;

	currentScene->playSound("Base_LeftScroll");

	CameraObject* bgCamera = currentScene->getCamera(0);
	CameraObject* contentCamera = currentScene->getCamera(1);
	contentCamera->rotateTo(Vec3f(0, -2.5f, 0), 0.75f);
	bgCamera->rotateTo(Vec3f(0, -1.5f, 0), 0.75f);
	contentCamera->rotateTo(Quaternion::identity, 0.75f);
	bgCamera->rotateTo(Quaternion::identity, 0.75f);

	displayIndicators(0.25f);
	printf("Rotating to cluster %d\n", currentPage);
}


} /* namespace Glasgow */

#include "Glasgow/GlasgowApp.h"
#include "ChildScene.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIObjects.h"
#include "SIEngine/Include/SIRendering.h"
#include "SIEngine/Include/SIActions.h"
#include "SIEngine/Include/SIMaterials.h"
#include "SIEngine/Include/GlasgowControls.h"
#include "Glasgow/Scenes/SceneManager.h"
#include "Glasgow/Renderables/WandObject.h"
#include "Glasgow/Particles/Particle.h"
#include <sys/stat.h>

using namespace SICore;
using namespace SICollections;

namespace Glasgow
{

static float spawnTimer = 10;
static int particleID = 0;

ChildScene::~ChildScene()
{
	printf("ChildScene::~ChildScene\n");
	SAFE_RELEASE(tutorialPage);
	SAFE_RELEASE(profilesPage);
	SAFE_RELEASE(profileChooserPage);
	SAFE_RELEASE(rewardCenterPage);
	SAFE_RELEASE(songsPage);
	SAFE_RELEASE(galleryPage);
	SAFE_RELEASE(videosPage);
	SAFE_RELEASE(cartridgeItem);
	SAFE_RELEASE(wristStrapPage);

	audio_mpi.StopAllAudio();
}

void PreloadChildTextureThread::run()
{
	TextureManager::getInstance()->loadPng("childScreenAtlas");
	TextureManager::getInstance()->loadPng("avatarAtlas");
	TextureManager::getInstance()->loadPng("avatarAtlas2");
	TextureManager::getInstance()->loadPng("prefabAtlas");
	TextureManager::getInstance()->loadPng("bannerAtlas");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-40");
}

/// <summary>
/// Child scene creation
/// </summary>
ChildScene::ChildScene() : super("Layouts/ChildScene.dat", "ChildSceneStrings")
{
	PreloadChildTextureThread pcT(this);
	pcT.start();
	GlasgowApp::getRenderer()->clearBuffers();
	firstLaunchOfUI = !SystemSettings::getIsWristStrapDone();
	previousPage = NULL;
	currentPage = NULL;
	activePage = NULL;
	itemToLaunch = NULL;
	preCartridgeSelection = NULL;
	cartridgeInfo = NULL;
	videoIndexLaunch = -1;
	analogMaxDistance = 2.0f;
	errorCodeLaunch = -1;
	launchTimer = 0;
	prepareTimer = 0;
	cartBurstTimer = 0;
	hintTimer = 0;

	//***********************************************************************************
	// Over ride the last user on the very first launch of the UI since package manager
	// doesn't write the lastUser when it downloads all the new profiles to the device
	//***********************************************************************************
	if( firstLaunchOfUI )
	{
		ObjectArray* users = UsersManager::getInstance()->getUsers();
		if( users->getSize() > 0 )
		{ SystemSettings::setLastUser( ((User*)users->elementAt(0))->getDisplayName() ); }
	}

	fillReferences();
	setupInitialState();
	setLastUsedTheme();
	GlasgowApp::mainState->writeUIReadyFlag();
	setupLaunchState();
}

void ChildScene::setupLaunchState()
{
	lockOutInput(2.0f);

	printf("ChildScene::setupLaunchState()\n");
	if( requestedState.from == AppIDParentSettings && requestedState.loadToState == AppStateNone )
	{
		printf("Launch request coming from parent scene to last state of app\n");
		String lastUserName = lastStateOnExit.data1;
		User* oldUser = UsersManager::getInstance()->getUser(lastUserName);
		if( oldUser != null && !oldUser->needsTutorial() && !oldUser->isGuest() )
		{
			User* lastUser = UsersManager::getInstance()->getUser(SystemSettings::getLastUser());
			if( lastUser != null )
			{
				setupForSpecificUser( lastUser );
				return;
			}
		}
	}

	if ( lastStateOnExit.exitReason == ExitReasonCartridge || lastStateOnExit.exitReason == ExitReasonContentItem )
	{
		printf("We last left due to a content item launch\n");
		User* lastUser = UsersManager::getInstance()->getUser(SystemSettings::getLastUser());
		if( lastUser != null && !lastUser->needsTutorial())
		{
			setupForSpecificUser( lastUser );
			return;
		}
		else
		{
			setLastUsedTheme();

			bgCamera->setPosition( Vec3f(0,0,1300));
			sceneChangeDarken->setOpacity(1);
			sceneChangeDarken->waitFor(0.5f);
			sceneChangeDarken->fadeTo(0, 2.0f);
			screenDarken->fadeTo(0.5f, 2.0f);

			if(SystemSettings::getIsWristStrapDone())
			{ setupForProfileSelection(); }
			else
			{ setupForWristStrap(); }
		}
	}
	else
	{
		setLastUsedTheme();

		bgCamera->setPosition( Vec3f(0,0,1300));
		sceneChangeDarken->setOpacity(1);
		sceneChangeDarken->waitFor(0.5f);
		sceneChangeDarken->fadeTo(0, 2.0f);
		screenDarken->fadeTo(0.5f, 2.0f);

		if(SystemSettings::getIsWristStrapDone())
		{ setupForProfileSelection(); }
		else
		{ setupForWristStrap(); }

	}
}


/// <summary>
/// Sets up all the default visibility status' and positions of dynamic objects on scene creation
/// </summary>
void ChildScene::setupInitialState()
{
	contentCamera->setPosition(Vec3f(0, 0, -1200));
	bgCamera->clearColor = Color(0,0,0,1);
}

/// <summary>
/// Sets the scene up in the case a user is already 'logged in'
/// </summary>
void ChildScene::setupForSpecificUser( User * user)
{
	audio_mpi.StopAllAudio();

	printf("User profile: %s\n", user->getDisplayName().str());
	setUser(user);
	setLastUsedTheme();

	worldRoot->addChild(mainHub);
	mainHub->setAvatarIcon(currentUser->getAvatar());
	mainHub->fillHub();

	screenDarken->setOpacity(1);
	sceneChangeDarken->setOpacity(1);
	sceneChangeDarken->waitFor(0.5f);
	sceneChangeDarken->fadeTo(0, 2.0f);
	screenDarken->fadeTo(0, 2.0f);

	previousPage = mainHub;
	currentPage = mainHub;
	setupHotSpots();

	currentHighlight = mainHub->getArrivalSelection();
	((ButtonObject*)currentHighlight)->setState(ButtonStateActive);

	contentCamera->setPosition(Vec3f(0, 0, -750));
	bgCamera->setPosition( Vec3f(0,0,1385));
}

/// <summary>
/// Sets the theme based on previous login
/// </summary>
void ChildScene::setLastUsedTheme()
{
	User* lastUser = UsersManager::getInstance()->getUser(SystemSettings::getLastUser());
	String avatarName = "AvatarItem15";
	String themeName = "UnderWater";
	if( lastUser != null )
	{ themeName = lastUser->getTheme(); }

	SystemSettings::setTheme(themeName);
	playMusic( getThemeMusic( themeName ));
}

/// <summary>
/// Creates a single object to be used for dynamic re-loading of cartridges
/// </summary>
void ChildScene::createCartridgeItem()
{
	String iconPath = "";
	String appName  = "";
	cartridgeItem = (ContentItem*)Scene::instantiate("PetPlanetContentItem");//ref 1
	cartridgeItem->setContentType(ContentApp);
	cartridgeItem->setBannerText(appName);
	Vec3f scale = cartridgeItem->originalScale * 0.9f;
	cartridgeItem->setScale(scale);
	cartridgeItem->originalScale = scale;
}

/// <summary>
/// Update loop for spawning particles
/// </summary>
void ChildScene::updateParticles(float dt)
{
	/*
	 * TODO
	 *
	 * Come back later when product has time to stabalize the particle system
	 *
	 *
	String currentTheme = SceneManager::getInstance()->getTheme();
	if( currentTheme.equalsIgnoreCase("SPACE") && spawnTimer < 0 )
	{
		TRelease<SpriteData> spriteData( new SpriteData("sparkle_element0012"));
		TRelease<Particle> particle( new Particle( spriteData) );

		particle->lifeSpan = randomFloat3(0.8f, 1.5f);
		particle->gravity = Vec3f::Zero;

		float randomX = -1 + randomFloat(0, 2);
		float randomY = -1 + randomFloat(0, 2);
		float randomZ = -1 + randomFloat(0, 2);

		particle->rotationRate = 0;// (int) (-500 + randomFloat(0, 1) * 1000);
		particle->velocity = Vec3f::Zero;

		particle->startColor = Color(1,1,1,1 );
		particle->endColor = Color( 1,1,1,0 );

		float size = randomFloat(0.2f, 0.8f);
		particle->setSize( Size( 120*size, 120*size ) );
		particle->setScale( Vec3f::Zero );
		particle->scaleTo(Vec3f::One, 0.1f);

		particle->fadeTo(0, particle->lifeSpan);
		particle->name = String("Particle") + particleID;
		particleID++;

		worldRoot->addChild(particle);
		particle->setPosition( backGround->getPosition() + Vec3f( randomX*2048, randomY*1024, -100 ) );
		spawnTimer = 0.15f;
	}*/

	spawnTimer -= dt;
}

/// <summary>
/// Update timers
/// </summary>
void ChildScene::updateTimers(float dt )
{
	//***************************************************************************
	// Launch timer
	//***************************************************************************
	if( launchTimer > 0 )
	{
		launchTimer -= dt;
		if( launchTimer <= 0 )
		{ doCartridgeLaunch(); }
	}

	//***************************************************************************
	// Animation sequence prior to launch
	//***************************************************************************
	if ( prepareTimer > 0 )
	{
		prepareTimer -= dt;
		if( prepareTimer <= 0 )
		{
			lockOutInput(0.5f);
			mainHub->handleCartridgeReady();

			if( currentPage == mainHub )
			{
				RingCluster* cluster = mainHub->getCurrentCluster();
				ContentItem* centerItem = cluster->getMiddleItem();
				setCurrentHighlight( centerItem );
			}

			setupHotSpots();
			cartBurstTimer = 1.0f;
		}
	}

	if( cartBurstTimer > 0  )
	{
		cartBurstTimer -= dt;
		if( cartBurstTimer <= 0 )
		{ doCartBurst(); }
	}

	if(hintTimer > 0)
	{
		hintTimer -= dt;
	}
}

/// <summary>
/// The call made to add the cartridge content item to the scene graph
/// </summary>
void ChildScene::addInCartridgeContentItem()
{
	printf("ChildScene::addInCartridgeContentItem()\n");
	if( cartridgeInfo != null && cartridgeItem != null )
	{
		cartridgeItem->setPackageInfo(cartridgeInfo);

		if( cartridgeItem->getParent() == null )
		{ worldRoot->addChild(cartridgeItem); }

		if( AssetLibrary::fileExists(cartridgeInfo->iconPath))
		{ cartridgeItem->setBackGroundImage(cartridgeInfo->iconPath); }

		cartridgeItem->cancelActions();
		cartridgeItem->setBannerText(cartridgeInfo->appName);
		cartridgeItem->showBanner();
		cartridgeItem->setPosition(Vec3f( 0, 0, -300));
		cartridgeItem->setRingColor(getThemeColor());
		cartridgeItem->setScale(Vec3f::Zero);
		cartridgeItem->scaleTo(Vec3f::One, 0.25f);
		cartridgeItem->scaleTo(Vec3f::One*1.25f, 1.5f);

		sceneChangeDarken->cancelActions();
		sceneChangeDarken->waitFor(1.5f);
		sceneChangeDarken->fadeTo(1, 0.5f);
		launchTimer = 2; //The call to launching the game is in 2 seconds!
		lockOutInput(3.0f);
	}
}

/// <summary>
/// Returns the color for a given theme
/// </summary>
Color ChildScene::getThemeColor()
{
	String themeName = "UnderWater";

	if( currentUser != null )
	{ themeName = currentUser->getTheme(); }

	if( themeName.equalsIgnoreCase("UnderWater") )
	{ return Color( 0/255.0f, 191.0f/255.0f, 255.0f/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Fruit") )
	{ return Color( 253.0f/255.0f, 17/255.0f, 172.0f/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Cave") )
	{ return Color( 198.0f/255.0f, 255.0f/255.0f, 0/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Space") )
	{ return Color( 108.0f/255.0f, 0/255.0f, 255/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Waterfall") )
	{ return Color( 0/255.0f, 200.0f/255.0f, 184.0f/255.0f, 1 ); }
	else if( themeName.equalsIgnoreCase("Wildwest") )
	{ return Color( 198.0f/255.0f, 255.0f/255.0f, 0/255.0f, 1 ); }

	return Color::Blue;
}

/// <summary>
/// Space particles
/// </summary>
void ChildScene::update(float dt )
{
	updateParticles(dt);
	updateTimers(dt);
	super::update(dt);
}

/// <summary>
/// Sets the scene up for initial login state
/// </summary>
void ChildScene::setupForProfileSelection()
{
	tutorialPage->saveOriginalStateRecursive();
	printf("Setup for Profile Selection\n");
	if( profileChooserPage->getParent() == NULL )
	{ worldRoot->addChild(profileChooserPage); }

	screenDarken->triggerSound("Home_SignIn");
	AvatarItem* lastUser = profileChooserPage->getLastUserSelected();
	currentHighlight = lastUser;

	if( lastUser != null )
	{
		lastUser->setState(ButtonStateActive);
		profileChooserPage->setUserLabel(lastUser->getUserName());
	}

	mainHub->setTrayDown();
	previousPage = mainHub;
	currentPage = profileChooserPage;

	currentPage->moveTo(Vec3f(0, 0, -970.0f), 1.5f);
	setupHotSpots();
}

void ChildScene::setupForWristStrap()
{
	lockOutInput(10);
	printf("Setup for Wrist Strap \n");

	worldRoot->addChild(wristStrapPage);

	// Move the profile chooser page offscreen
	profileChooserPage->setPosition(Vec3f(0, 300, -970.0f));

	previousPage = mainHub;
	currentPage = wristStrapPage;
	wristStrapPage->cameraArrived();
	setupHotSpots();

	currentHighlight = wristStrapPage->getArrivalSelection();
	((ButtonObject*)currentHighlight)->setState(ButtonStateActive);
	((ButtonObject*)currentHighlight)->cancelActions();
	((ButtonObject*)currentHighlight)->bubbleIn(10);
}

/// <summary>
/// Fills our references
/// </summary>
void ChildScene::fillReferences()
{
	currentUser = UsersManager::getInstance()->getUser(SystemSettings::getLastUser());
	bgCamera 			= getCamera(0);
	contentCamera 		= getCamera(1);
	screenDarken 		= (SpriteObject*)findObject("profileDarken");
	sceneChangeDarken 	= (SpriteObject*)findObject("DepthCamUI/worldDarken");
	backGround			= worldRoot->findSprite("Background");
	backGroundSwap		= NULL;

	//*****************************************************************************************************
	// Mantain reference since these objects will pop on and off the scene graph for the zsort pruning
	//*****************************************************************************************************
	profilesPage 		= new ProfilePage(findObject("ProfileRoot"));
	profileChooserPage 	= new LoginPage(findObject("ProfileChooser"));
	rewardCenterPage 	= new RewardCenterPage(findObject("RewardCenter"));
	songsPage 			= new SongsPage(findObject("AudioRoot"));
	galleryPage 		= new GalleryPage(findObject("GalleryRoot"));
	mainHub				= new HubPage(findObject("RingRoot"));
	tutorialPage 		= new TutorialPage(findObject("TutorialTheme"));
	videosPage			= new VideosPage(findObject("VideoRoot"));
	wristStrapPage		= new WristStrapPage(findObject("WristStrapRoot"));


}

/// <summary>
/// Particle Burst for fun
/// </summary>
void ChildScene::doCartBurst()
{
	for( int i = 0; i<50; i++ )
	{
		TRelease<SpriteData> spriteData( new SpriteData("starParticle"));
		TRelease<Particle> particle( new Particle( spriteData) );
		particle->lifeSpan = 1+randomFloat(0, 2);
		particle->gravity = Vec3f::Up*60;
		float randomX = -20 + randomFloat(0, 40);
		float randomY = 10 - randomFloat(0, 40);
		float randomZ = -30 + randomFloat(0, 60);
		particle->rotationRate = (int) (-500 + randomFloat(0, 1) * 1000);
		particle->velocity = Vec3f(randomX, randomY, randomZ)*10;
		particle->startColor = Color( 1, 1, 1 ,1 );
		particle->endColor = Color( 1, 1, 1 , 0  );
		particle->setSize( Size( 25, 25 ) );
		particle->setScale( Vec3f::One );
		particle->setMaterial(TRelease<TintMaterial>(new TintMaterial(particle)));
		particle->fadeTo(0, particle->lifeSpan);
		worldRoot->addChild(particle);
		particle->setPosition(Vec3f( 0, 75, -300));
	}
}

/// <summary>
/// Animation helper to dim screen
/// </summary>
void ChildScene::darkenScreen()
{
	screenDarken->waitFor(1);
	screenDarken->fadeTo(0.8f, 1);
}

/// <summary>
/// Moves camera to switch users
/// </summary>
void ChildScene::moveCameraToSwitchUsers()
{
	profileChooserPage->remakeAvatarItems();
	if(currentPage == profileChooserPage)
		return;

	currentHighlight = profileChooserPage->getLastUserSelected();

	playVo("Home_SignIn");
	mainHub->staggerOutContentRings();
	mainHub->setTrayDown();
	profilesPage->animateToSwitchUsers();

	if( profileChooserPage->getParent() == NULL )
	{ worldRoot->addChild(profileChooserPage); }

	profileChooserPage->animateForSwitchUsers();

	currentPage = profileChooserPage;
	setupHotSpots();
	printf("animateForSwitchUsers2()\n");
}

/// <summary>
/// Moves camera to show the gallery
/// </summary>
void ChildScene::moveCameraToPage( ChildPage* page )
{
	if( page == NULL )
	{ ThrowException::exception("Page is NULL \n"); }

	printf("Moving to ChildScene state %s\n", page->name.str());

	if( currentPage == videosPage && page == mainHub )
	{ playMusic(getThemeMusic(SystemSettings::getTheme())); }

	if(page == profilesPage)
	{ playSound("ProfCustom_Launch"); }
	else if( page == mainHub && currentPage == profilesPage)
	{ playSound("ProfCustom_Exit"); }
	else
	{ playSound("Base_Select"); }

	removeSelection();

	if( page->getParent() == NULL )
	{ worldRoot->addChild( page ); }
	page->setVisible(true);

	currentPage->cameraLeaving();
	previousPage = currentPage;
	currentPage = page;
	page->setVisible(true);
	page->cameraArrived();
	moveCamerasBy( currentPage->getCameraPlacement() );

	currentHighlight = page->getArrivalSelection();
	if( currentHighlight != NULL )
	{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }

	lockOutInput(2);
	setupHotSpots();
}

/// <summary>
/// Helper function that moves the two cameras a given direction
/// </summary>
void ChildScene::moveCamerasTo( Vec3f uiCamPosition, Vec3f bgCamPosition )
{
	bgCamera->moveTo(bgCamPosition, 2);
	contentCamera->moveTo(uiCamPosition, 2);
}

/// <summary>
/// Helper function that moves the two cameras a given direction
/// </summary>
void ChildScene::moveCamerasBy( Vec3f direction )
{
	float darkenTimer = 1;
	if( prepareTimer > 0 )
		darkenTimer /= 2;

	if( direction.z < 0 )
	{
		screenDarken->waitFor(darkenTimer);
		screenDarken->fadeTo(0.8f, darkenTimer);
	}
	else if( direction.z > 0 )
	{
		darkenTimer /= 2;
		screenDarken->waitFor(darkenTimer);
		screenDarken->fadeTo(0, darkenTimer);
	}

	float cameraMoveTimer = 2;
	if( prepareTimer > 0 )
	{ cameraMoveTimer = 0.75f;	 }

	bgCamera->moveBy(direction * 85, cameraMoveTimer);
	contentCamera->moveBy(direction * 450, cameraMoveTimer);
}

/// <summary>
/// Notification that the app has returned back to us from a suspend state
/// </summary>
void ChildScene::onReturnToApp()
{
	super::onReturnToApp();
	printf("ChildScene::onReturnToApp()\n");

	lockOutInput(0);
	if( lastStateOnExit.exitReason == ExitReasonErrorWidget )
	{
		printf("Returning from error widget\n");
		sceneChangeDarken->fadeTo(0, 1);
		system("fbctrl /dev/fb1 set blank 0");
		return;
	}
	else if( lastStateOnExit.exitReason == ExitReasonVideoBundle )
	{
		printf("Returning from video bundle\n");

		if(itemToLaunch->videos.getSize() <= 1 )
		{

			//heeeeeeeeeeee hhhhhhhhaaaaaawwwwwwww
			mainHub->animateFromItemLaunch();

			RingCluster* cluster = mainHub->getCurrentCluster();
			ContentItem* centerItem = cluster->getMiddleItem();
			if( centerItem != null )
			{ setCurrentHighlight( centerItem ); }
		}
		else
		{
			((GalleryItem*)currentHighlight)->setState(ButtonStateActive);
		}

		sceneChangeDarken->fadeTo(0, 1);
		system("fbctrl /dev/fb1 set blank 0");
	}
	else
	{

		sceneChangeDarken->cancelActions();
		sceneChangeDarken->fadeTo(0, 0.25f);
		system("fbctrl /dev/fb1 set blank 0");
	}
}

/// <summary>
/// Sometimes a scene will want to check for any analog event as opposed to being
/// told about the post processed candidacy
/// </summary>
bool ChildScene::onAnalogStickEvent(AnalogStickEvent *event)
{
	//custom over ride for tutorial
	if( !super::onAnalogStickEvent(event) && currentPage == tutorialPage )
	{ tutorialPage->handleAnalogStickEvent(event); }
}



/// <summary>
/// Child scene actions
/// </summary>
void ChildScene::setUser(User *user)
{
	printf("ChildScene::setUser\n");
	currentUser = user;
	if(currentUser == NULL )
	{ currentUser = User::guest(); }

	//Update all Labels
	profilesPage->handleUserSet(currentUser);
	mainHub->handleUserSet(currentUser);
	galleryPage->handleUserSet(currentUser);

	if( SystemSettings::getTheme() != currentUser->getTheme() )
	{ changeToNewTheme( currentUser->getTheme() ); }

	printf("Setting last logged in user : %s\n", currentUser->getFirstName().str());
	UsersManager::getInstance()->setCurrentUser(currentUser);
}

/// <summary>
/// Child scene uses timers
/// </summary>
void ChildScene::onTimerExpired( TimerEvent* timer )
{
	if( timer->id == "LaunchParentForNewUser" )
	{ SceneManager::getInstance()->launchParentSceneForNewUser(); }
	else if( timer->id == "LaunchParent" )
	{ SceneManager::getInstance()->launchParentSettings(); }
	else if( timer->id == "Starburst" )
	{ mainHub->doStarBurst(); }
	else if( timer->id == "WelcomeBurst" )
	{ tutorialPage->doWelcomeParticles(); }
	else if( timer->id == "PlayAnalogSelect" )
	{ playVo("HTP_AnalogTheme_select"); }
	else if ( timer->id == "DoItemLaunch")
	{ doLaunch(); }
	else if ( timer->id == "DoCartLaunch" )
	{ doCartridgeLaunch(); }

	super::onTimerExpired(timer);
}

/// <summary>
/// Custom handling of selection changes
/// </summary>
void ChildScene::onSelectionChange(GameObject* old, GameObject* newObj)
{
	super::onSelectionChange(old, newObj);

	if( old != null && newObj != null )
	{  printf("Selection changed from %s to %s \n", old->name.str(), newObj->name.str() ); }
	else if ( old != null && newObj == null )
	{  printf("Selection changed from %s to nothing. \n", old->name.str() ); }
	else if ( old == null && newObj != null )
	{  printf("Selection changed from nothing to %s. \n", newObj->name.str() ); }

	if(old != null &&  old->typeOf( ButtonObject::type()) )
	{ ((ButtonObject*)old)->setState(ButtonStateNormal); }

	if(newObj != null &&  newObj->typeOf( ButtonObject::type()) )
	{ ((ButtonObject*)newObj)->setState(ButtonStateActive); }

	if( currentPage != null )
	{ currentPage->handleSelectionChange(old, newObj); }
}

/// <summary>
/// Returns name of the music file to be played iwth a theme
/// This should be restructured into a ThemeInfo class I think
/// </summary>
String ChildScene::getThemeMusic(String themeName)
{
	if( themeName.equalsIgnoreCase("UnderWater") )
	{ return "Base_ThemeMusic01"; }
	else if( themeName.equalsIgnoreCase("Castle") )
	{  return "Base_ThemeMusic04"; }
	else if( themeName.equalsIgnoreCase("Cave") )
	{  return "Base_ThemeMusic04"; }
	else if( themeName.equalsIgnoreCase("WildWest") )
	{  return "Base_ThemeMusic03"; }
	else if( themeName.equalsIgnoreCase("Space") )
	{ return "Base_ThemeMusic02"; }
	else if( themeName.equalsIgnoreCase("Fruit") )
	{ return "Base_ThemeMusic05"; }
	else
	{ return "Base_ThemeMusic01"; }
}

/// <summary>
/// Change UI to new Theme
/// </summary>
void ChildScene::changeToNewTheme(String newThemeName)
{
	if( newThemeName.equals(SystemSettings::getTheme()) )
	{ return; }
	else
	{
		playMusic(getThemeMusic(newThemeName));
		mainHub->onThemeChange();

		String texture = AssetLibrary::getInstallDir() + String("Shared/Textures/Themes/")+newThemeName+"/themeAtlas";
		TextureManager::getInstance()->loadPng(texture);
		TRelease<SpriteData> data(new SpriteData(TRelease<SpriteFrame>(new SpriteFrame()),texture));

		//****************************************************************************************
		// Fade out old background
		//****************************************************************************************
		SpriteObject* fadeOut = backGround;
		if( backGroundSwap != NULL )
		{
			printf("Fading out background swap\n");
			fadeOut = backGroundSwap;
		}

		fadeOut->fadeTo(0, 2.0f);
		fadeOut->kill();

		//****************************************************************************************
		// Fade in new background
		//****************************************************************************************
		TRelease<SpriteObject> obj(new SpriteObject(data));
		obj->setSize(fadeOut->getSize());
		obj->setScale(fadeOut->getScale());
		obj->setPosition(fadeOut->getPosition() + Vec3f::Forward);
		obj->setCullingLayer( bgCamera->getCullingMask());
		worldRoot->addChild(obj);
		backGroundSwap = obj;

		SystemSettings::setTheme(newThemeName);
	}
}

/// <summary>
/// Returns the object representing by a no selection -> selected state chnage
/// </summary>
GameObject* ChildScene::getDefaultHighLight()
{
	if(currentPage == wristStrapPage)
	{
		return wristStrapPage->getArrivalSelection();
	}
	return super::getDefaultHighLight();
}

/// <summary>
/// Plays the song for the given AudioItem
/// </summary>
void ChildScene::playSong(AudioItem* item)
{
	String audioName =item->getAudioName();
	item->setState(ButtonStateNormal);
	audio_mpi.StopAllAudio();
	playMusic(audioName.str());
	moveCameraToPage(mainHub);
}

/// <summary>
/// Does the actual launch
/// </summary>
void ChildScene::doCartridgeLaunch()
{
	//Launch cartridge application
	SceneManager::getInstance()->launchCartridgeApp(ContentManager::getInstance()->getCartridgePackage(), AppStateCartridge);
}

/// <summary>
/// Does the actual launch
/// </summary>
void ChildScene::doLaunch()
{
	printf("Doing the actual launch\n");
	audio_mpi.StopAllAudio();

	if( itemToLaunch != null )
	{
		struct stat buffer;
		//****************************************************************************************
		// Video Bundle
		//****************************************************************************************
		if( itemToLaunch->isVideoBundle )
		{
			QVariantMap parameterMap;
			parameterMap.insert("VideoStart", String(videoIndexLaunch).str() );
			for( int i = 0; i<itemToLaunch->videos.getSize(); i++ )
			{
				VideoInfo* videoInfo = (VideoInfo*)itemToLaunch->videos[i];
				String keyName = String("Video")+String(i);
				String keyName2 = String("VideoName")+String(i);
				parameterMap.insert(keyName.str(), videoInfo->baseDir.str() );
				parameterMap.insert(keyName2.str(), videoInfo->videoTitle.str() );
				parameterMap.insert("PackageID", itemToLaunch->packageID.str());
				parameterMap.insert("AppDirPath", itemToLaunch->appDirPath.str());
			}

			SceneManager::getInstance()->launchVideoWidget(parameterMap, AppStateVideoBundle);
		}
		else if ( itemToLaunch->isCartridge() )
		{ SceneManager::getInstance()->launchCartridgeApp(itemToLaunch, AppStateCartridge);	 }
		else
		{ SceneManager::getInstance()->launchContentItem(itemToLaunch, AppStateContent); }
	}
}

/// <summary>
/// Ready is called when the cartridge has valid access to the file system mounted by the cartridge
/// </summary>
void ChildScene::onCartridgeReady()
{
	playSound("Base_Select");

	//ignore
	if( currentPage == profileChooserPage )
	{ return; }

	prepareTimer = 1;
	super::onCartridgeReady();
}

/// <summary>
/// Scene::onCartridgeRemoved()
/// </summary>
void ChildScene::onCartridgeRemoved()
{
	printf("ChildScene::onCartridgeRemoved()\n");
	lockOutInput(1.0f);

	//Cancel launch timer
	playSound("Base_Back");
	prepareTimer = 0;
	mainHub->handleCartridgeRemoved();
	cartBurstTimer = 0;

	if( currentPage == mainHub )
	{
		RingCluster* cluster = mainHub->getCurrentCluster();
		ContentItem* centerItem = cluster->getMiddleItem();

		if( centerItem != null )
		{ setCurrentHighlight( centerItem ); }

		if( itemToLaunch != null && itemToLaunch->isCartridge() )
		{
			cartBurstTimer = 0;
			Timer* launchTimer = TimerManager::getInstance()->getTimer("DoItemLaunch");
			if( launchTimer!= null && !launchTimer->isDone() )
			{
				TimerManager::getInstance()->cancelTimer("DoItemLaunch");
				sceneChangeDarken->cancelActions();
				sceneChangeDarken->fadeTo(0, 0.25f);
			}
		}
	}

	setupHotSpots();
	super::onCartridgeRemoved();
}

/// <summary>
/// Scene::onCartridgeInserted()
/// </summary>
void ChildScene::onCartridgeInserted()
{
	printf("ChildScene::onCartridgeInserted()\n");
	playSound("Base_Select");

	if( currentPage == profileChooserPage )
	{
		//ignore
		return;
	}

	if( prepareTimer <= 0 )
	{
		preCartridgeSelection = currentHighlight;
		prepareTimer = 2.0f;

		if( currentHighlight != null && currentHighlight->typeOf(ButtonObject::type()))
		{
			currentHighlight->cancelActions();
			((ButtonObject*)currentHighlight)->setState(ButtonStateNormal);
		}

		if( currentPage != mainHub )
		{
			moveCameraToPage(mainHub);
		}

		removeSelection();
		lockOutInput(3);
	}

	super::onCartridgeInserted();
}

/// <summary>
/// Respond to controller disconnects
/// </summary>
void ChildScene::onControllerDisconnected( HWController* controller )
{
	printf("ChildScene::onControllerDisconnected\n");
	super::onControllerDisconnected(controller);

	//no more controllers controlling UI, error widget
	if( !hasAConnectedController() )
	{
		// TODO: Localize strings
		SceneManager::getInstance()->launchErrorWidget(ErrorIDController, true, 0, "", "", "" );
	}
}


/// <summary>
/// Launches a content item
/// </summary>
void ChildScene::prepareLaunchVideoWidget(int startingInfo)
{
	videoIndexLaunch = startingInfo;

	//************************************************************************************
	// We have a few internal apps within the HUB, check for those else call launch!
	//************************************************************************************
	TimerManager::getInstance()->addTimer("DoItemLaunch", 4, 0);
	if( currentHighlight->typeOf(GalleryItem::type()))
	{ ((GalleryItem*)currentHighlight)->setState(ButtonStateNormal); }
	sceneChangeDarken->fadeTo(1, 1);
}

/// <summary>
/// Launches a content item
/// </summary>
void ChildScene::launchContentItem(ContentItem* item )
{
	printf("ChildScene::launchContentItem(ContentItem* item )\n");
	//************************************************************************************
	// Even though there is a contentType in packagemanager, the packageInfo does a
	// more strict interpretation based on the files that are at the package itself.
	//************************************************************************************
	/*
	 *
	 *
	if ( item->getPackageInfo() != null && item->getPackageInfo()->isPetPlayWorld())
	{
		if( !SystemSettings::getIsActivated() )
		{
			SceneManager::getInstance()->launchErrorWidget(ErrorIDPetPlay, false, 0, "", "", "" );
			return;
		}
	}
	*/

	if( item->getPackageInfo()!= null && item->getPackageInfo()->isVideoBundle && item->getPackageInfo()->videos.getSize() == 1 )
	{
		itemToLaunch = item->getPackageInfo();
		videosPage->setPackageInfo(item->getPackageInfo());
		prepareLaunchVideoWidget(0);
		mainHub->animateForItemLaunch( item );
	}
	else if( item->contentType == ContentApp )
	{
		if( item->getAppName() == "Game Snaps" )
		{ moveCameraToPage(galleryPage); }
		else if( item->getAppName() == "Songs" )
		{ moveCameraToPage(songsPage); }
		else
		{
			//Bundled videos go directly to the choose video screen
			if( item->getPackageInfo() != null && item->getPackageInfo()->isVideoBundle )
			{
				stopAllMusic();
				itemToLaunch = item->getPackageInfo();
				videosPage->setPackageInfo(item->getPackageInfo());
				moveCameraToPage(videosPage);
			}
			//Single video packages launch directly
			else
			{
				TimerManager::getInstance()->addTimer("DoItemLaunch", 4, 0);
				itemToLaunch = item->getPackageInfo();
				mainHub->animateForItemLaunch( item );
				cartBurstTimer = 0.75f;
				sceneChangeDarken->waitFor(2);
				sceneChangeDarken->fadeTo(1, 1);
			}
		}
	}
}

/// <summary>
/// Animates to parent scene
/// </summary>
void ChildScene::prepareLaunchParentScene(String entryName)
{
	lockOutInput(2.0f);
	playSound("Base_Select");
	sceneChangeDarken->fadeTo(1, 2.0f);
	TimerManager::addTimer(entryName, 2.0f, 0);
	removeSelection();
}

/// <summary>
/// Logs in using avatar item
/// </summary>
void ChildScene::loginUsingAvatar( AvatarItem* avatarItem)
{
	playSound("Base_Select");

	if( avatarItem->getUserName() == "New User" )
	{
		prepareLaunchParentScene("LaunchParentForNewUser");
	}
	else if ( avatarItem->getPrefabCloneName() == "AvatarItemNotChosen" )
	{
		printf("Avatar Item Not Chosen!! GotoTutorial!\n");
		setUser(avatarItem);
		removeSelection();
		profileChooserPage->leaveForTutorial();

		if( tutorialPage->getParent() == NULL )
		{ worldRoot->addChild( tutorialPage ); }

		printf("Reset\n");
		tutorialPage->loadOriginalStateRecursive();

		contentCamera->moveTo( contentCamera->originalState.originalTransform.getPosition(), 1.0f);

		tutorialPage->setVisible(true);
		tutorialPage->cameraArrived();
		currentHighlight = tutorialPage->getArrivalSelection();

		if( currentHighlight != NULL )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }

		currentPage = tutorialPage;
		lockOutInput(2);
		setupHotSpots();
		playSound("SFX_select");

		return;
	}

	else if( currentPage == tutorialPage )
	{
		playSound("Base_Select");

		printf("Logging in for the first time using avatar %s\n", avatarItem->name.str());
		AvatarItem* selected = profileChooserPage->reCreateAvatarItem(avatarItem);
		currentUser->setAvatar(selected->getPrefabCloneName());
		currentUser->setTheme(SystemSettings::getTheme());
		currentUser->save();


		removeSelection();
		if( mainHub->getParent() == NULL )
		{ worldRoot->addChild(mainHub); }

		tutorialPage->cameraLeaving();
		bgCamera->waitFor(0.4f);
		contentCamera->waitFor(0.4f);
		screenDarken->waitFor(0.25f);
		screenDarken->fadeTo(0, 0.4f);

		moveCamerasTo( Vec3f(0,0,-750),  Vec3f(0,0, 1385) );
		currentPage = mainHub;
		lockOutInput(2.5f);

		//Fill hub with the proper content for the user
		mainHub->animateAvatarItemIntoTray(selected);
		mainHub->fillHub();

		currentHighlight = mainHub->getCluster(0)->getItem(0);
		if( currentHighlight != null )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
		setupHotSpots();
	}
	else
	{
		printf("Logging in using Avatar %s\n", avatarItem->name.str());
		AvatarItem* selected = profileChooserPage->reCreateAvatarItem( (AvatarItem*)currentHighlight );
		selected->setupAsLoginAvatar(((AvatarItem*)currentHighlight)->getUserName());

		setUser(selected);
		removeSelection();

		if( mainHub->getParent() == NULL )
		{ worldRoot->addChild(mainHub); }

		profileChooserPage->bubbleOutItems();
		profileChooserPage->waitFor(3);
		profileChooserPage->kill();

		bgCamera->waitFor(0.4f);
		contentCamera->waitFor(0.4f);
		moveCamerasBy(Vec3f::Forward);

		playVo("Home_SelectGame");

		currentPage = mainHub;
		lockOutInput(2.5f);

		//Fill hub with the proper content for the user
		mainHub->animateAvatarItemIntoTray(selected);
		mainHub->fillHub();
		setupHotSpots();

		currentHighlight = mainHub->getArrivalSelection();
		if ( currentHighlight != null )
		{ ((ButtonObject*)currentHighlight)->setState(ButtonStateActive); }
	}
}

/// <summary>
/// Sets the current user via the avatarLogin icon
/// </summary>
void ChildScene::setUser(AvatarItem* selected)
{
	User *whichUser = NULL;
	ObjectArray* users = UsersManager::getInstance()->getUsers();
	for( int i = 0; i<users->getSize(); i++ )
	{
		User* user = (User*)users->elementAt(i);
		if( user->getFirstName().equalsIgnoreCase(selected->getUserName()))
		{ whichUser = user; }
	}

	currentUser = whichUser;
	setUser(whichUser);
}

/// <summary>
/// Debug button
/// </summary>
void ChildScene::onButtonHome()
{
	printf( "ChildScene::onButtonHome\n");

	if( currentPage == tutorialPage )
	{ return; }

	if(currentPage == wristStrapPage)
	{
		currentPage->cameraLeaving();
		setupForProfileSelection();
	}
	else if(currentPage == mainHub)
	{
		mainHub->rotateToLanding();
	}
	else if(currentPage != profileChooserPage)
	{
		moveCameraToPage(mainHub);
	}

	super::onButtonHome();
}

/// <summary>
/// Respond to action button presses
/// </summary>
void ChildScene::onButtonPressAction()
{
	super::onButtonPressAction();

	if( wandEnabled )
	{
		if ( wand->getLockOutTimer() > 0 )
		{ return; }
	}
	//************************************************************************
	// If the page has handled this click
	//************************************************************************
	if( currentHighlight != NULL )
	{
		if( currentPage != NULL && currentPage->handlePressAction(currentHighlight) )
		{
			lockOutInput(0.5f);
			setupHotSpots();
		}
		else
		{
			if( currentHighlight->name == "parentSettings")
			{ return prepareLaunchParentScene("LaunchParent"); }
			else if( currentHighlight->name == "backButton")
			{
				if(currentPage == wristStrapPage)
				{
					currentPage->cameraLeaving();
					setupForProfileSelection();
					return;
				}
				else{ return moveCameraToPage(mainHub); }
			}
			else if( currentHighlight->name == "profileIcon")
			{ return moveCameraToPage(profilesPage); }
			else if( currentHighlight->name == "rewardCenterButton")
			{ return moveCameraToPage(rewardCenterPage); }
			else if( currentHighlight->name == "switchUsersButton")
			{ return moveCameraToSwitchUsers(); }
			else if ( currentPage->typeOf(TutorialPage::type()) && currentHighlight->typeOf(AvatarItem::type()))
			{ return loginUsingAvatar((AvatarItem*)currentHighlight); }
			else if ( currentPage->typeOf(LoginPage::type()) && currentHighlight->typeOf(AvatarItem::type()))
			{ return loginUsingAvatar((AvatarItem*)currentHighlight); }
			else if( currentHighlight->typeOf(AudioItem::type()) )
			{ return playSong( (AudioItem*)currentHighlight ); }
			else if( currentPage->typeOf(VideosPage::type()) && currentHighlight->typeOf(GalleryItem::type()) )
			{
				prepareLaunchVideoWidget(((VideosPage*)currentPage)->getVideoIndex());
			}
			else if( currentPage->typeOf(ProfilePage::type()) && currentHighlight->typeOf(ThemeItem::type()) )
			{
				String newThemeName = ((ThemeItem*)currentHighlight)->getThemeName();
				currentUser->setTheme(newThemeName);
				currentUser->save();
				changeToNewTheme(newThemeName);
				return moveCameraToPage(mainHub);
			}
			else if ( currentPage->typeOf(ProfilePage::type()) && currentHighlight->typeOf(AvatarItem::type()))
			{
				mainHub->replaceAvatar( ((ProfilePage*)currentPage)->getSelectedAvatar() );
				return moveCameraToPage(mainHub);
			}
			else if ( currentHighlight->typeOf(ContentItem::type()) )
			{ return launchContentItem( (ContentItem*)currentHighlight ); }
		}
	}
}

/// <summary>
/// Respond to back button presses
/// </summary>
void ChildScene::onButtonPressBack()
{
	super::onButtonPressBack();
}

void ChildScene::onButtonHint()
{
	// Prevent kids from spamming the hint button
	if(hintTimer > 0)
	{
		return;
	}

	//************************************************************************
	// If the page has handled this click
	//************************************************************************
	if( currentHighlight != NULL )
	{
		if(currentPage->typeOf(HubPage::type()))
		{
			if( currentHighlight->name == "parentSettings")
			{ playVo("Home_Hint_Parent"); }
			else if( currentHighlight->name == "profileIcon")
			{ playVo("Home_Hint_Profile"); }
			else if( currentHighlight->name == "rewardCenterButton")
			{ playVo("Home_Hint_Rewards"); }
			else if ( currentHighlight->typeOf(ContentItem::type()) )
			{ playVo("Home_LaunchContent"); }
			else
			{ playVo("Home_Hint"); }
		}
		else if( currentPage->typeOf(ProfilePage::type()))
		{
			if(currentHighlight->typeOf(ThemeItem::type()))
			{ playVo("Profile_Hint_world"); }
			else if ( currentHighlight->typeOf(AvatarItem::type()))
			{ playVo("HTP_SelectAvatar"); }
			else if(currentHighlight->name == "switchUsersButton")
			{ playVo("Profile_Hint_SwitchUser"); }
			else if(currentHighlight->name == "backButton")
			{ playVo("Base_Exit"); }
			else{ playVo("Profile_Hint"); }
		}
		else if( currentPage->typeOf(TutorialPage::type()))
		{
			if(currentHighlight->typeOf(ThemeItem::type()))
			{ playVo("Profile_Hint_world"); }
			else if ( currentHighlight->typeOf(AvatarItem::type()))
			{ playVo("HTP_SelectAvatar"); }
		}
		else if( currentPage->typeOf(RewardCenterPage::type()))
		{ playVo("Rewards_Hint"); }

		hintTimer = 1;
	}

	super::onButtonHint();
}

/// <summary>
/// Custom handling of analog changes
/// </summary>
void ChildScene::onAnalogChange(GameObject* objectLeft, GameObject* objectEntered)
{
	if( selectionLockOut > 0 )
	{ return; }

	if( currentPage != null && currentPage->handleAnlogChange(objectLeft, objectEntered, currentHighlight) )
	{ setupHotSpots(); }
	else
	{ super::onAnalogChange(objectLeft, objectEntered); }
}

/// <summary>
/// Sets the required hot spots for the given ChildScene state
/// </summary>
void ChildScene::setupHotSpots()
{
	trackedObjects->removeAllElements();

	if( currentPage != NULL )
	{
		currentPage->addHotSpots(trackedObjects);
	}
}

}

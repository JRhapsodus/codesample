#ifndef PROFILEPAGE_H_
#define PROFILEPAGE_H_

#include "ChildPage.h"

namespace Glasgow
{

class ProfilePage : public ChildPage
{
	CLASSEXTENDS(ProfilePage, ChildPage);
public:
	ProfilePage(GameObject* rootObject);
	virtual ~ProfilePage();

	/// <summary>
	/// Animates the sliders to change position in the profile page
	/// </summary>
	void switchSliderToAvatar();

	/// <summary>
	/// Called when a new active user has effectively loggedin
	/// </summary>
	virtual void handleUserSet(User* user);

	/// <summary>
	/// Animates the sliders to change position in the profile page
	/// </summary>
	void switchSliderToTheme();

	/// <summary>
	/// Fades the label in on the Profiles page to show the choose avatar
	/// </summary>
	void fadeToAvatarLabel(float waitTime);

	/// <summary>
	/// Fades the label in on the Profiles page to show the choose theme
	/// </summary>
	void fadeToThemeLabel(float waitTime);

	/// <summary>
	/// Convenience function that returns the appropriate slider's selected Item casted
	/// </summary>
	ThemeItem* getSelectedTheme();

	/// <summary>
	/// Convenience function that returns the appropriate slider's selected Item casted
	/// </summary>
	AvatarItem* getSelectedAvatar();

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& highlight);

	/// <summary>
	/// Add our tracked items to the scene list
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackedList );

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void animateToSwitchUsers();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

private:
	SliderObject*				avatarChooserSlider;
	SliderObject*				themeChooserSlider;
	SliderObject*				lastProfilesSlider;
	SliderObject*				activeSlider;

	ButtonObject*				namePlate;
};

} /* namespace Glasgow */
#endif /* PROFILEPAGE_H_ */

#include "WristStrapPage.h"
#include "Glasgow/Scenes/SceneManager.h"

namespace Glasgow
{

WristStrapPage::WristStrapPage(GameObject* rootObject) : super(rootObject)
{
	instructionsIcon1 = findSprite("instructionsIcon1");
	instructionsIcon2 = findSprite("instructionsIcon2");
	headerRoot = find("headerRoot");
	cautionRoot = find("cautionRoot");
	backButton = findButton("backButton");

	setVisibleRecursive(false);

	backButton->setState(ButtonStateActive);
}

WristStrapPage::~WristStrapPage()
{

}

/// <summary>
/// Add our tracked items to the scene list
/// </summary>
void WristStrapPage::addHotSpots( ObjectArray* trackedList )
{
	trackedList->addElement(backButton);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* WristStrapPage::getArrivalSelection()
{
	return backButton;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void WristStrapPage::cameraArrived()
{
	setVisibleRecursive(true);

	// Hide all of our ui elements

	headerRoot->setPosition(Vec3f(0, 300, 0));
	cautionRoot->setPosition(Vec3f(0, 100, 0));
	cautionRoot->recursiveFadeOut(0);
	instructionsIcon1->setPosition(Vec3f(-450, 0, 12));
	instructionsIcon2->setPosition(Vec3f(450, 0, 12));
	backButton->setScale(Vec3f(0, 0, 0));

	float waitTime = 1.5f;

	headerRoot->waitFor(waitTime);
	headerRoot->moveTo(Vec3f(0, 0, 0), 1);

	waitTime += 0.5f;

	cautionRoot->recursiveFadeIn(waitTime);
	cautionRoot->waitFor(waitTime+1.0f);
	cautionRoot->moveTo(Vec3f(0, 0, 0), 1);

	waitTime += 1.5f;

	instructionsIcon1->waitFor(waitTime);
	instructionsIcon1->moveTo(Vec3f(-139, 0, 81), 1);
	instructionsIcon2->waitFor(waitTime);
	instructionsIcon2->moveTo(Vec3f(147, 0, 81), 1);

	waitTime += 0.5f;

	backButton->bubbleIn(waitTime);
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void WristStrapPage::cameraLeaving()
{
	super::cameraLeaving();

	SystemSettings::setIsWristStrapDone(true);
	recursiveFadeOut(0);
}


} /* namespace Glasgow */

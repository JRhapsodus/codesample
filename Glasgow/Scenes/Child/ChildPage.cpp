#include "ChildPage.h"

namespace Glasgow
{

ChildPage::ChildPage(GameObject* objectRoot)
{
	for ( int i = 0; i<objectRoot->numChildren(); i++ )
	{
		GameObject * child = objectRoot->childAtIndex(i);
		child->addRef(); //so remove from parent doesn't refCount to 0
		child->removeFromParent();
		addChild(child);
		child->release();
		i--;
	}

	setScale(objectRoot->getScale());
	setPosition(objectRoot->getPosition());
	setRotation(objectRoot->getRotation());
	name = objectRoot->name;

	currentUser = UsersManager::getInstance()->getCurrentUser();
}

ChildPage::~ChildPage()
{ }

/// <summary>
/// Animation plug for all pages to prepare for a cartridge launch
/// Note: This animation has to be canceled at any point along its progress
/// </summary>
void ChildPage::prepareForCartridgeLaunch()
{ }

/// <summary>
/// Cancels the animation for cartridge launch
/// </summary>
void ChildPage::cancelCartridgeLaunch()
{ }

/// <summary>
/// Called when a new active user has effectively loggedin
/// </summary>
void ChildPage::handleUserSet(User* user)
{ }

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ChildPage::handlePressAction(GameObject*& objectHighlight)
{ return false;  }

/// <summary>
/// Returns which direction the camera goes to when going to this page
/// </summary>
Vec3f ChildPage::getCameraPlacement()
{ return Vec3f::Back; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ChildPage::cameraArrived()
{ }

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void ChildPage::cameraLeaving()
{
	waitFor(2);
	kill();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ChildPage::getArrivalSelection()
{ return NULL; }

/// <summary>
/// Inpuit forwarding call to handle selection changes
/// Pass the pointer for current Highlight as the pages are really sub screens
/// of the scene and they might want to handle auto 'reselection' such as sliders
/// </summary>
void ChildPage::handleSelectionChange( GameObject* old, GameObject* newObj)
{ }

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ChildPage::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ChildPage::addHotSpots( ObjectArray* trackingList )
{ }

} /* namespace Glasgow */

#ifndef SONGSPAGE_H_
#define SONGSPAGE_H_

#include "ChildPage.h"

namespace Glasgow
{

class SongsPage : public ChildPage
{
	CLASSEXTENDS(SongsPage, ChildPage);
public:
	SongsPage(GameObject* rootObject);
	virtual ~SongsPage();

	/// <summary>
	/// Convenience function that returns the appropriate slider's selected Item casted
	/// </summary>
	AudioItem* getSelectedAudio();


	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& highlight);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Handle hub feature
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

private:
	SliderObject*				audioSlider;
};

}
#endif

#ifndef TUTORIALPAGE_H_
#define TUTORIALPAGE_H_

#include "ChildPage.h"

namespace Glasgow
{

class TutorialPage : public ChildPage
{
	CLASSEXTENDS(TutorialPage, ChildPage);
public:
	TutorialPage(GameObject* objectRoot);
	virtual ~TutorialPage();

	/// <summary>
	/// Overr ridden for tutorial to get any analog stick event
	/// </summary>
	bool handleAnalogStickEvent(AnalogStickEvent *event);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& highlight);

	/// <summary>
	/// Add our tracked items to the scene list
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackedList );

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Returns which direction the camera goes to when going to this page
	/// </summary>
	virtual Vec3f getCameraPlacement();

	/// <summary>
	/// Custom response to pressaction
	/// </summary>
	virtual bool handlePressAction(GameObject*& currentHighlight);

	/// <summary>
	/// Custom response to pressaction
	/// </summary>
	void doWelcomeParticles();

	void doControllerParticles();

	virtual void loadGameObjectState(GameObjectState state);


	void setupInitialState();
	virtual void update(float dt);
	/// <summary>
	/// Recreates avatar
	/// </summary>
	AvatarItem* reCreateAvatarItem(AvatarItem* toClone);
private:
	bool themeChosen;
	TextLabel* chooseAvatarLabel;
	TextLabel* welcomeLabel;
	TextLabel* chooseThemeLabel;

	TextLabel* toolTipWand;
	TextLabel* toolTipAnalog;

	int controllerIndex ;
	float switchTimer;
	SliderObject* avatarSlider;
	SliderObject* themeSlider;
	SpriteObject* controllerWand[5];
	ButtonObject* controllerDefault;
	GameObject* toolTipRoot;
};

} /* namespace Glasgow */
#endif /* TUTORIALPAGE_H_ */

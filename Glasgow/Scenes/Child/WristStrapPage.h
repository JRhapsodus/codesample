#ifndef WRISTSTRAPPAGE_H_
#define WRISTSTRAPPAGE_H_

#include "ChildPage.h"

namespace Glasgow
{

class WristStrapPage : public ChildPage
{
	CLASSEXTENDS(WristStrapPage, ChildPage);
public:
	WristStrapPage(GameObject *rootObject);
	virtual ~WristStrapPage();

	/// <summary>
	/// Add our tracked items to the scene list
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackedList );

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

private:
	SpriteObject* 	instructionsIcon1;
	SpriteObject* 	instructionsIcon2;
	GameObject* 	headerRoot;
	GameObject*		cautionRoot;
	ButtonObject* 	backButton;
};

} /* namespace Glasgow */
#endif /* WRISTSTRAPPAGE_H_ */

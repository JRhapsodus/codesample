#include "LoginPage.h"
#include "SIEngine/Include/SIScenes.h"

namespace Glasgow
{
int waitTimeForLogin = 14;
LoginPage::LoginPage( GameObject* rootObject ) : super(rootObject)
{
	createLoginIcons();
	positionAvatarLogins();

	userNameLabel = findLabel("userLabel");
	selectedAvatar = NULL;
	autoLoginTimer = -1;
	preSwitchThemeTimer = -1;
}

LoginPage::~LoginPage()
{
	printf("LoginPage::~LoginPage\n");
}

void LoginPage::setUserLabel(String name )
{
	userNameLabel->setText(name);
}

/// <summary>
/// Custom animation for leaving for tutorial
/// </summary>
void LoginPage::leaveForTutorial()
{
	userNameLabel->colorTo( Color(1,1,1,0), 0.5f );
	autoLoginTimer = -1;
	preSwitchThemeTimer = -1;
	bubbleOutItems();
}

/// <summary>
/// Redo avatar items
/// </summary>
void LoginPage::remakeAvatarItems()
{

	for( int i = 0; i<avatarLogins.getSize(); i++ )
	{
		((AvatarItem*)avatarLogins[i])->removeFromParent();
	}

	avatarLogins.removeAllElements();
	createLoginIcons();
	positionAvatarLogins();
}

/// <summary>
/// Positions the logins on screen
/// </summary>
void LoginPage::positionAvatarLogins()
{
	int nUsers = avatarLogins.getSize();
	if ( nUsers == 2 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-80, 0, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(80, 0, 0));
	}
	else if ( nUsers == 3 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-120, 0, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(0, 0, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(120, 0, 0));
	}
	else if ( nUsers == 4 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-80, 50, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(80, 50, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(-80, -50, 0));
		((AvatarItem*) avatarLogins[3])->setPosition(Vec3f(80, -50, 0));
	}
	else if ( nUsers == 5 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-120, 50, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(0, 50, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(120, 50, 0));
		((AvatarItem*) avatarLogins[3])->setPosition(Vec3f(-60, -50, 0));
		((AvatarItem*) avatarLogins[4])->setPosition(Vec3f(60, -50, 0));
	}
	else if ( nUsers == 6 )
	{
		((AvatarItem*) avatarLogins[0])->setPosition(Vec3f(-120, 50, 0));
		((AvatarItem*) avatarLogins[1])->setPosition(Vec3f(0, 50, 0));
		((AvatarItem*) avatarLogins[2])->setPosition(Vec3f(120, 50, 0));
		((AvatarItem*) avatarLogins[3])->setPosition(Vec3f(-120, -50, 0));
		((AvatarItem*) avatarLogins[4])->setPosition(Vec3f(0, -50, 0));
		((AvatarItem*) avatarLogins[5])->setPosition(Vec3f(120, -50, 0));
	}
}

bool LoginPage::itemsHaveActions()
{
	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		if( ((AvatarItem*)avatarLogins[i])->hasActions() )
			return true;
	}

	return false;

}


/// <summary>
/// Custom animation transition
/// </summary>
void LoginPage::animateForSwitchUsers()
{
	waitFor(0.5f);
	makeVisible(true);

	float time = 1;
	float timeInc = 0.1f;
	find("parentSettings")->waitFor(time);
	find("parentSettings")->makeVisible(true);
	find("parentSettings")->setScale(Vec3f::Zero);
	find("parentSettings")->scaleTo(find("parentSettings")->originalScale, 0.25f);
	time += timeInc;
	bubbleInItems(time);
	preSwitchThemeTimer = 4+time;

	if( selectedAvatar->getItemType() != AvatarItem::ItemUserAsNewUser )
	{ autoLoginTimer = waitTimeForLogin; }
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void LoginPage::cameraArrived()
{
	userNameLabel->setText("");
}

/// <summary>
/// Add our tracked items to the scene list
/// </summary>
void LoginPage::addHotSpots( ObjectArray* trackedList )
{
	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		trackedList->addElement(avatarLogins[i]);
	}

	trackedList->addElement(findButton("parentSettings"));
}

/// <summary>
/// Recreates avatar
/// </summary>
AvatarItem* LoginPage::reCreateAvatarItem( AvatarItem* toClone )
{
	Vec3f pos = toClone->getPosition();
	Vec3f scale = toClone->getScale();
	Quaternion rot = toClone->getRotation();
	GameObject* parent = toClone->getParent();

	AvatarItem* avatar = (AvatarItem*) Scene::instantiate(toClone->getPrefabCloneName());
	avatar->originalScale = scale;
	avatar->setupAsLoginAvatar(toClone->getUserName());
	avatar->setPosition(pos);
	avatar->setScale(scale);
	avatar->setRotation(rot);
	parent->addChild(avatar);
	avatar->release();

	toClone->setVisible(false);
	return avatar;
}

/// <summary>
/// Cartridge removed prior to the actual launch call
/// </summary>
void LoginPage::cancelCartridgeLaunch()
{
	autoLoginTimer = -1;
	preSwitchThemeTimer = -1;
	float time = 0.5f;

	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		GameObject* scaleOut = (GameObject*) avatarLogins[i];
		scaleOut->waitFor(time);
		scaleOut->makeVisible(true);
		scaleOut->setScale(Vec3f::Zero);
		scaleOut->scaleTo(scaleOut->originalScale, 0.25f);
		time += 0.1f;
	}

	userNameLabel->colorTo(Color(1,1,1,1), 0.15f);
	findButton("parentSettings")->scaleTo(findButton("parentSettings")->originalScale, 0.15f);
}

/// <summary>
/// Animation plug for all pages to prepare for a cartridge launch
/// Note: This animation has to be canceled at any point along its progress
/// </summary>
void LoginPage::prepareForCartridgeLaunch()
{
	autoLoginTimer = -1;
	preSwitchThemeTimer = -1;
	float time = 0.05f;
	float timeInc = 0.05f;


	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		GameObject* scaleOut = (GameObject*) avatarLogins[i];
		scaleOut->waitFor(time);
		scaleOut->scaleTo(Vec3f::Zero, 0.15f);
		time += timeInc;
	}

	userNameLabel->colorTo(Color(1,1,1,0), 0.15f);
	findButton("parentSettings")->scaleTo(Vec3f::Zero, 0.15f);
}

/// <summary>
/// Bubble out items animation
/// </summary>
void LoginPage::bubbleOutItems()
{
	printf("Bubbling Out Items\n");
	autoLoginTimer = -1;
	preSwitchThemeTimer = -1;
	float time = 0.1f;
	float timeInc = 0.15f;

	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		GameObject* scaleOut = (GameObject*) avatarLogins[i];
		scaleOut->waitFor(time);
		scaleOut->scaleTo(Vec3f::Zero, 0.25f);
		time += timeInc;
	}

	findButton("parentSettings")->waitFor(time);
	findButton("parentSettings")->scaleTo(Vec3f::Zero, 0.25f);
}

/// <summary>
/// Bubble in items animation
/// </summary>
void LoginPage::bubbleInItems( float time )
{
	autoLoginTimer = -1;
	preSwitchThemeTimer = -1;

	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		GameObject* scaleOut = (GameObject*) avatarLogins[i];
		scaleOut->waitFor(time);
		scaleOut->makeVisible(true);
		scaleOut->setScale(Vec3f::Zero);
		scaleOut->scaleTo(scaleOut->originalScale, 0.25f);
		time += 0.1f;
	}
}

/// <summary>
/// Override to take care of custom timer events
/// </summary>
void LoginPage::update( float dt )
{
	if( preSwitchThemeTimer > 0 )
	{
		preSwitchThemeTimer -= dt;
		if( preSwitchThemeTimer <= 0 )
		{
			printf("Preswitch themes\n");
			User* user = selectedAvatar->getUser();
			ChildScene* childScene = (ChildScene*)SceneManager::getInstance()->getScene();
			if( user != NULL )
			{ childScene->changeToNewTheme(user->getTheme()); }
			else if ( selectedAvatar->getItemType() == AvatarItem::ItemUserAsGuest )
			{ childScene->changeToNewTheme("UnderWater"); }
		}
	}

	if( autoLoginTimer > 0 && !itemsHaveActions())
	{
		autoLoginTimer -= dt;
		float dp = 0.5f + (autoLoginTimer/2.0f)*0.5f;
		if( dp > 1.0f )
			dp = 1.0f;

		for ( int i = 0; i < avatarLogins.getSize(); i++ )
		{
			AvatarItem* user = (AvatarItem*) avatarLogins[i];
			if( user != selectedAvatar )
			{ user->setScale( user->originalScale * dp ); }
		}

		//"Auto Click"
		if( autoLoginTimer <= 0 )
		{ SceneManager::getInstance()->getScene()->onButtonPressAction(); }

	}
	super::update(dt);

}

/// <summary>
/// Creates an avatar login per user
/// </summary>
void LoginPage::createLoginIcons()
{
	//****************************************************************************************************
	// Add in user icons
	//****************************************************************************************************
	ObjectArray* users = UsersManager::getInstance()->getUsers();
	for ( int i = 0; i < users->getSize(); i++ )
	{
		User* user = (User*) users->elementAt(i);
		TRelease<AvatarItem> userItem((AvatarItem*)Scene::instantiate(user->getAvatar()));
		addChild(userItem);
		userItem->setUser(user);
		userItem->setupAsLoginAvatar(user->getFirstName());
		avatarLogins.addElement(userItem);
	}

	if ( avatarLogins.getSize() < 5 )
	{
		TRelease<AvatarItem> newUser((AvatarItem*) Scene::instantiate("AvatarItemNewUser"));
		addChild(newUser);
		newUser->setupAsLoginAvatar("New User");
		avatarLogins.addElement(newUser);
	}

	if ( avatarLogins.getSize() < 6 )
	{
		//Add guest button
		TRelease<AvatarItem> guestUser((AvatarItem*) Scene::instantiate("AvatarItemGuest"));
		addChild(guestUser);
		guestUser->setupAsLoginAvatar(Constants::UI::guestProfileName);
		avatarLogins.addElement(guestUser);
	}
}

/// <summary>
/// Overridden to snoop for analog changes
/// </summary>
bool LoginPage::handleAnlogChange( GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight )
{
	if( objectEntered != NULL && objectEntered->typeOf(AvatarItem::type()) )
	{
		if( selectedAvatar != objectEntered )
		{
			for ( int i = 0; i < avatarLogins.getSize(); i++ )
			{
				AvatarItem* user = (AvatarItem*) avatarLogins[i];
				user->scaleTo( user->originalScale, 0.25f );
			}

			preSwitchThemeTimer = 4;
			selectedAvatar = (AvatarItem*)objectEntered;

			userNameLabel->colorTo( Color( 1,1,1,0), 0.25f );
			userNameLabel->changeTo( selectedAvatar->getUserName() );
			userNameLabel->colorTo( Color( 1,1,1,1), 0.25f );

			SceneManager::getInstance()->getScene()->playSound("Base_Highlight");

			if( selectedAvatar->getItemType() != AvatarItem::ItemUserAsNewUser )
			{ autoLoginTimer = waitTimeForLogin; }
			else
			{
				printf("Do Not Auto Login\n");
				autoLoginTimer = -1;
			}
		}
		else if ( objectLeft != null && objectLeft->name == "parentSettings")
		{
			userNameLabel->colorTo( Color( 1,1,1,0), 0.25f );
			userNameLabel->changeTo( selectedAvatar->getUserName() );
			userNameLabel->colorTo( Color( 1,1,1,1), 0.25f );
		}
	}
	else
	{
		for ( int i = 0; i < avatarLogins.getSize(); i++ )
		{
			AvatarItem* user = (AvatarItem*) avatarLogins[i];
			user->scaleTo( user->originalScale, 0.25f );
		}

		printf("Do Not Auto Login\n");
		autoLoginTimer = -1;
		preSwitchThemeTimer = -1;

		if( objectEntered != null && objectEntered->name == "parentSettings" )
		{
			userNameLabel->colorTo( Color( 1,1,1,0), 0.25f );
			userNameLabel->changeTo( "Parent Settings" );
			userNameLabel->colorTo( Color( 1,1,1,1), 0.25f );
		}
	}

	return false;
}

/// <summary>
/// sets the last user that logged in as the default selected in initial login
/// </summary>
AvatarItem* LoginPage::getLastUserSelected()
{
	AvatarItem* guestLogin = null;
	String lastUser = SystemSettings::getLastUser();
	printf("Last logged in user: %s\n", lastUser.str());
	if( lastUser.isEmpty() )
	{ lastUser = Constants::UI::guestProfileName; }

	for ( int i = 0; i < avatarLogins.getSize(); i++ )
	{
		AvatarItem* user = (AvatarItem*) avatarLogins[i];
		if ( user->getUserName().equalsIgnoreCase(lastUser) )
		{
			printf("Found avatar item for %s \n", user->getUserName().str());
			user->setState(ButtonStateActive);
			preSwitchThemeTimer = 4;
			selectedAvatar = user;

			userNameLabel->setTextColor( Color( 1,1,1,0) );
			userNameLabel->changeTo( selectedAvatar->getUserName() );
			userNameLabel->colorTo( Color( 1,1,1,1), 0.25f );

			if( selectedAvatar->getItemType() != AvatarItem::ItemUserAsNewUser )
			{ autoLoginTimer = waitTimeForLogin; }
			return user;
		}
		else if ( user->isGuest() || lastUser.isEmpty())
		{
			guestLogin = user;
		}
	}

	printf("No user found that matches the last login Size[%d]\n", avatarLogins.getSize());
	if( guestLogin == null )
	{
		for ( int i = 0; i < avatarLogins.getSize(); i++ )
		{
			AvatarItem* user = (AvatarItem*) avatarLogins[i];
			if( user->isGuest() )
			{
				printf("Found a guest user\n");
				guestLogin = user;
				break;
			}
		}
	}


	return guestLogin;

}

} /* namespace Glasgow */

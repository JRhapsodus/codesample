#ifndef LOCKCREATIONSTEP_H_
#define LOCKCREATIONSTEP_H_

#include "OutOfBoxStep.h"
#include "SIEngine/Include/GlasgowControls.h"

namespace Glasgow
{

class LockCreationStep : public OutOfBoxStep
{
	CLASSEXTENDS(LockCreationStep, OutOfBoxStep);
public:
	LockCreationStep(GameObject * replacementRoot, OutOfBoxScene* scene);
	virtual ~LockCreationStep();

	/// <summary>
	/// saves user settings
	/// </summary>
	virtual void saveSettings();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Custom update
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Pin entered
	/// </summary>
	void pinEntered();



private:
	bool 				confirm;
	String 				enteredPin;
	String 				confirmPin;
	GateLockObject* 	numPad;
	SpriteObject*		pinBacking;
	SpriteObject*		confirmPinBacking;
	SpriteObject*		finalPinBacking;
	TextLabel* 			entryPrompt;
	TextLabel* 			errorPrompt;
	TextLabel* 			confirmPrompt;
	TextLabel*			finishedPrompt;
	TextLabel*			finalPinLabel;
	TextLabel*			pinLabel;
	TextLabel*			confirmPinLabel;
};

}
#endif

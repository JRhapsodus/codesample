#include "Glasgow/GlasgowApp.h"
#include "OutOfBoxScene.h"
#include "SIEngine/Include/SIScenes.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIActions.h"
#include "SIEngine/Include/SIRendering.h"
#include "Glasgow/Renderables/WandObject.h"
#include "Glasgow/Controls/RollOverButton.h"
#include "Glasgow/Controls/WifiButton.h"
#include "Glasgow/Renderables/VideoObject.h"

namespace Glasgow
{

OutOfBoxScene::~OutOfBoxScene()
{
	printf("OutOfBoxScene::~OutOfBoxScene()\n");
	SAFE_RELEASE(welcomeStep);
	SAFE_RELEASE(cameraStep);
	SAFE_RELEASE(localeStep);
	SAFE_RELEASE(chooseWifi);
	SAFE_RELEASE(downloadStep);
	SAFE_RELEASE(ethernetStep);
	SAFE_RELEASE(createPinStep);
	SAFE_RELEASE(createProfileStep);
	SAFE_RELEASE(activationStep);
	SAFE_RELEASE(verifyAgeStep);
	SAFE_RELEASE(finalStep);
	steps.clear();

	audio_mpi.StopAllAudio();
}

OutOfBoxScene::OutOfBoxScene() :
		super("Layouts/OutOfBoxScene.dat", "OutOfBoxSceneStrings")
{
	printf("OutOfBoxScene::OutOfBoxScene\n");
	printf("Preload known textures\n");

	TextureManager::getInstance()->loadPng("oobeAtlas");
	TextureManager::getInstance()->loadPng("prefabAtlas");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/MontserratBold-LF-60");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/MontserratBold-LF-24");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-22");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-60");
	TextureManager::getInstance()->loadPng("Shared/Textures/Fonts/Montserrat-LF-24");

	analogMaxDistance = 2.0f;
	getCamera(0)->setupForOrthographic(true);
	enableNextStepTimer = 0;
	exitTimer = 0;
	unplugTimer = 3;
	numEthernetChecks = 0;
	fillReferences();
	setupInitialState();

	playMusic("Base_OOBEmusic");
	currentHighlight = NULL;
	currentStep = NULL;

	overRideStep = null;

	String lastOOBEStep = SystemSettings::getLastOOBEStep();
	if ( lastOOBEStep.isEmpty() )
	{ lastOOBEStep = "welcomeStep"; }

	//If we somehow ended up on registrations but are already registered lets skip to update step
	if( lastOOBEStep.equalsIgnoreCase("activationStep") && SystemSettings::getIsActivated() )
	{ lastOOBEStep = "updateStep"; }


	if( lastOOBEStep.equalsIgnoreCase("chooseWifiStep") || lastOOBEStep.equalsIgnoreCase("ethernetStep") )
	{
		if( ConnectionManager::getInstance()->hasEthernet() )
		{ lastOOBEStep = "ethernetStep"; }
		else
		{ lastOOBEStep = "chooseWifiStep"; }
	}


	BaseObject* step = steps[lastOOBEStep];
	moveToStep( null, (OutOfBoxStep*) step);
	GlasgowApp::mainState->writeUIReadyFlag();
	loaded = true;
}

//***************************************************************************
// Fill in references
//***************************************************************************
void OutOfBoxScene::fillReferences()
{
	printf("Filling references. OutOfBoxScene\n");

	background = worldRoot->findSprite("Background");
	stepsRoot = findObject("Steps");

	if ( stepsRoot == null )
		ThrowException::deserializeException();

	welcomeStep = new WelcomeStep(stepsRoot->find("welcomeStep"), this);
	cameraStep = new CameraStep(stepsRoot->find("cameraStep"), this);
	localeStep = new LocaleStep(stepsRoot->find("localeStep"), this);
	chooseWifi = new ChooseWifiStep(stepsRoot->find("chooseWifiStep"), this);
	downloadStep = new DownloadStep(stepsRoot->find("updateStep"), this);
	ethernetStep = new EthernetStep(stepsRoot->find("ethernetStep"), this);
	createPinStep = new LockCreationStep(stepsRoot->find("createPinStep"), this);
	createProfileStep = new ProfileCreationStep(stepsRoot->find("createProfileStep"), this);
	activationStep = new ActivationStep(stepsRoot->find("activationStep"), this);
	verifyAgeStep = new AgeGateStep(stepsRoot->find("ageVerificationStep"), this);
	finalStep = new FinalStep(stepsRoot->find("finalStep"), this);

	chooseWifi->saveOriginalStateRecursive();

	steps[welcomeStep->name] = welcomeStep;
	steps[cameraStep->name] = cameraStep;
	steps[localeStep->name] = localeStep;
	steps[chooseWifi->name] = chooseWifi;
	steps[downloadStep->name] = downloadStep;
	steps[ethernetStep->name] = ethernetStep;
	steps[createPinStep->name] = createPinStep;
	steps[createProfileStep->name] = createProfileStep;
	steps[activationStep->name] = activationStep;
	steps[finalStep->name] = finalStep;
	steps[verifyAgeStep->name] = verifyAgeStep;

	stepIndicator = findObject("stepInfoRoot/stepIndicator");
	nextButton = worldRoot->findButton("nextButton");
	prevButton = worldRoot->findButton("backButton");
	helpButton = worldRoot->findButton("helpButton");
	helpRollOver = worldRoot->findLabel("helpLabel");
	screenDarken = worldRoot->findSprite("screenDarken");

}

/// <summary>
/// Child scene uses timers
/// </summary>
void OutOfBoxScene::onTimerExpired( TimerEvent* timer )
{

	if ( timer->id == "ToChild" )
	{
		exitTimer = 1;
		return;
	}

	else if ( timer->id == "ScanRequest" )
	{
		chooseWifi->doScan();
		setupHotSpots();

		currentHighlight = chooseWifi->selectedWifiButton();
		if ( currentHighlight != null )
		{
			((ButtonObject*) currentHighlight)->setState(ButtonStateActive);
		}
	}

	else if ( timer->id == "WifiConnectTimeout" )
	{
		ConnectionManager::getInstance()->wifiTimeoutExpired();
	}

	else if(timer->id == "HideSynchError")
	{
		welcomeStep->hideSynchError();
		welcomeStep->showSynchInstructions();
	}

}

/// <summary>
/// Enables the next step
/// </summary>
void OutOfBoxScene::enableNextStep( float delay )
{
	if( delay <= 0 )
	{ ThrowException::invalidApiException(); }

	printf("OutOfBoxScene::enableNextStep( float delay )\n");
	playSound("Base_TransformSuccess");
	enableNextStepTimer = delay;
	setupHotSpots();
}

/// <summary>
/// Base scene feature, handling camera
/// </summary>
void OutOfBoxScene::onCameraPluggedIn()
{
	if ( currentStep == cameraStep )
		cameraStep->handleCameraConnected();
	super::onCameraPluggedIn();
}

/// <summary>
/// Base scene feature, handling camera
/// </summary>
void OutOfBoxScene::onCameraUnplugged()
{
	if ( currentStep == cameraStep )
		cameraStep->handleCameraDisconnected();
	super::onCameraUnplugged();
}

///<summary>
/// Hides the next step
/// </summary>
void OutOfBoxScene::hideNextStep()
{
	nextButton->bubbleOut(0);
	nextButton->setState(ButtonStateNormal);
}

/// <summary>
/// Enables the help button
/// </summary>
void OutOfBoxScene::enableHelpButton()
{
	if ( !helpButton->isVisibleHierarchy() )
		helpButton->bubbleIn(1.5f);
}

///<summary>
/// Hides the help button
/// </summary>
void OutOfBoxScene::hideHelpButton()
{
	if ( helpButton->isVisibleHierarchy() )
	{
		helpButton->bubbleOut(0);
		helpRollOver->bubbleOut(0);
	}
}

void OutOfBoxScene::onPendingDownloadsChanged()
{
	if( currentStep == activationStep )
	{
		activationStep->handlePendingChanged();
		downloadStep->handlePendingChanged();

	}
}

/// <summary>
/// Download has progressed
/// </summary>
void OutOfBoxScene::onDonwloadProgressed( String packageID, int downloadProgress )
{
	downloadStep->handleDownloadProgress(packageID, downloadProgress);
	activationStep->handleDownloadProgress(packageID, downloadProgress);
}

//***************************************************************************
// Lets save our O(n) qsort by limiting the scene graph to the current pages
//***************************************************************************
void OutOfBoxScene::setupInitialState()
{
	worldRoot->find("backButton")->setVisible(false);
	helpRollOver->setVisible(false);
	helpButton->setVisible(false);
	nextButton->setVisible(false);
	prevButton->setVisible(false);
	helpRollOver->makeVisible(false);
	screenDarken->waitFor(1.0f);
	screenDarken->fadeTo(0, 0.5f);
	screenDarken->makeVisible(false);
}

//**********************************************************************************************
// Move indicator left
//**********************************************************************************************
void OutOfBoxScene::moveIndicatorLeft()
{
	stepIndicator->fadeTo(0, 0.15f);
	stepIndicator->moveBy(Vec3f::Left * 149, 0.1f);
	stepIndicator->waitFor(1.05f);
	stepIndicator->fadeTo(1, 0.35f);
}

//**********************************************************************************************
// Move indicator right
//**********************************************************************************************
void OutOfBoxScene::moveIndicatorRight()
{
	stepIndicator->fadeTo(0, 0.15f);
	stepIndicator->moveBy(Vec3f::Right * 149, 0.1f);
	stepIndicator->waitFor(1.05f);
	stepIndicator->fadeTo(1, 0.35f);
}

/// <summary>
/// Helper Animation functions
/// </summary>
void OutOfBoxScene::moveToIndicatorStep( OutOfBoxParentStep step )
{
	int stepNum = Max(Min((int)step,(int)OutOfBoxParentStepPlayAndLearn), (int )OutOfBoxParentStepPowerUp);
	printf("OutOfBoxScene::moveToIndicatorStep %d\n", stepNum);

	Vec3f targetPos = Vec3f(-298 + 149 * (stepNum - 1), 280, -96);
	Vec3f toTargetPos = targetPos - stepIndicator->getPosition();

	float waitTime = 0;
	if ( toTargetPos.length() > 0 )
	{
		stepIndicator->fadeTo(0, 0.15f);
		stepIndicator->waitFor(1);
		stepIndicator->moveTo(targetPos);
		stepIndicator->fadeTo(1, 0.35f);
		waitTime += 1.25f;
	}

	for ( int i = (int) OutOfBoxParentStepPowerUp; i < (int) OutOfBoxParentStepMax; ++i )
	{
		TextLabel* stepLabel = worldRoot->find("stepInfoRoot")->findLabel(String("step") + i + String("Text"));
		if ( i == stepNum )
		{
			stepLabel->waitFor(waitTime);
			stepLabel->colorTo(Color(58 / 255.0f, 118 / 255.0f, 3 / 255.0f, 1), 0.25f);
		}
		else
		{
			stepLabel->colorTo(Color(1, 1, 1, 1), 0.25f);
		}
	}
}

///<summary>
/// returns step being displayed
///</summary>
OutOfBoxStep* OutOfBoxScene::getCurrentStep()
{ return currentStep;}

/// <summary>
/// get the step parent associated with the step
/// </summary>
OutOfBoxParentStep OutOfBoxScene::getParentStepForStep( OutOfBoxStep* step )
{
	if ( step == null )
	{
		OutOfBoxParentStepMax;
	}
	if ( step == welcomeStep )
	{
		return OutOfBoxParentStepPowerUp;
	}
	else if ( step == localeStep || step == ethernetStep || step == chooseWifi )
	{
		return OutOfBoxParentStepSetUp;
	}
	else if ( step == activationStep || step == verifyAgeStep || step == createPinStep || step == createProfileStep )
	{
		return OutOfBoxParentStepRegister;
	}
	else if ( step == downloadStep )
	{
		return OutOfBoxParentStepDownload;
	}
	else if ( step == cameraStep || step == finalStep )
	{
		return OutOfBoxParentStepPlayAndLearn;
	}

	return OutOfBoxParentStepInvalid;
}

/// <summary>
/// get the next step
/// </summary>
OutOfBoxStep* OutOfBoxScene::getNextStep( OutOfBoxStep* step )
{
	//If Over ride step was set, go back to the proper step
	if( overRideStep != null )
	{
		OutOfBoxStep* nextStep = overRideStep;
		overRideStep = null;
		return nextStep;
	}


	if ( step == welcomeStep )
	{
		return localeStep;
	}
	else if ( step == localeStep )
	{
		if ( ConnectionManager::getInstance()->hasEthernet() )
		{
			return ethernetStep;
		}
		else
		{
			return chooseWifi;
		}
	}
	else if ( step == ethernetStep || step == chooseWifi )
	{
		return activationStep;
	}
	else if ( step == verifyAgeStep )
	{
		return createPinStep;
	}
	else if ( step == createPinStep )
	{
		return createProfileStep;
	}
	else if ( step == createProfileStep )
	{
		printf(
				"Going to next step from Create Profile skip flow, testing existence of pending or ready firmware update\n");
		if ( ContentManager::getInstance()->getFirmwareStatus() == FirmwareNotAvailabe )
		{
			printf("No firmware available, goto camera step\n");
			return cameraStep;
		}
		else
		{
			printf("Firmware update pending or ready to install, go to update screen\n");
			return downloadStep;
		}
	}
	else if ( step == downloadStep )
	{
		return cameraStep;
	}
	else if ( step == activationStep )
	{
		printf("Going to next step from Activation, testing existence of pending or ready firmware update\n");
		if ( ContentManager::getInstance()->getFirmwareStatus() == FirmwareNotAvailabe )
		{
			printf("No firmware available\n");
			return cameraStep;
		}

		printf("Firmware update pending or ready to install, go to update screen\n");
		return downloadStep;
	}
	else if ( step == cameraStep )
	{
		return finalStep;
	}

	return null;
}

/// <summary>
/// Download finished
/// </summary>
void OutOfBoxScene::onDonwloadFinished( String packageID )
{
	downloadStep->handleDonwloadFinished(packageID);
	activationStep->handleDonwloadFinished(packageID);
}

/// <summary>
/// Download started
/// </summary>
void OutOfBoxScene::onDonwloadStarted( String packageID )
{
	downloadStep->handleDonwloadStarted(packageID);
	activationStep->handleDonwloadStarted(packageID);
}

/// <summary>
/// Animation functions
/// </summary>
void OutOfBoxScene::moveToStep( OutOfBoxStep* lastStep, OutOfBoxStep* newStep )
{
	printf("OutOfBoxScene::moveToStep %s\n", newStep->name.str());
	enableNextStepTimer = 0;
	removeSelection();
	preDialogSelection = null;

	if( currentModal != null )
	{
		currentModal->cancel();
		currentModal = null;
	}

	OutOfBoxParentStep parentStep = getParentStepForStep(newStep);
	moveToIndicatorStep(parentStep);
	playSound("Base_RightScroll");

	nextButton->cancelActionsAsComplete();
	prevButton->cancelActionsAsComplete();
	nextButton->scaleTo(Vec3f::Zero, 0.25f);
	prevButton->scaleTo(Vec3f::Zero, 0.25f);
	nextButton->makeVisible(false);

	SystemSettings::setOOBEStep(newStep->name);
	stepsRoot->addChild(newStep);
	newStep->refreshLocalizedStringsRecursive();
	//just calling this for wifi since you can't return to any other step
	if(newStep == chooseWifi)
	{ newStep->loadOriginalStateRecursive();}

	if ( lastStep != null )
	{
		lockOutInput(2);
		lastStep->moveToScaleTo(Vec3f::Left * 1280, Vec3f::Zero, 2);
		lastStep->kill();

		newStep->setPosition(Vec3f(1280, 0, 0));
		newStep->setScale(Vec3f::Zero);
		newStep->moveToScaleTo(Vec3f::Zero, Vec3f::One, 2);
	}



	newStep->cameraArrived();

	currentStep = newStep;
	currentModal = null; //we should not be progressing steps with a dialog visible
	//really the dialog modal should be null by this time but base layer was over ridden in this scene

	setCurrentHighlight(newStep->getArrivalSelection());
	setupHotSpots();
}

/// <summary>
/// Setup hotspots
/// </summary>
void OutOfBoxScene::setupHotSpots()
{
	printf("OutOfBoxScene::setupHotSpots()\n");
	if ( currentModal == null )
	{
		trackedObjects->removeAllElements();

		if( currentModal != null )
		{ currentModal->addHotSpots( trackedObjects); }

		if ( currentStep != null )
		{ currentStep->addHotSpots(trackedObjects); }

		trackedObjects->addElement(nextButton);
		trackedObjects->addElement(prevButton);
		trackedObjects->addElement(helpButton);
	}
	else
	{
		currentModal->addHotSpots(trackedObjects);
	}
}

/// <summary>
/// skips the registration process
/// </summary>
void OutOfBoxScene::skipActivation()
{
	//clear override step
	overRideStep = null;
	moveToStep(currentStep, (OutOfBoxStep*)verifyAgeStep);
}

/// <summary>
/// Goes to the next step
/// </summary>
void OutOfBoxScene::gotoNextStep()
{
	printf("gotoNextStep()\n");

	if ( currentStep == null )
	{
		printf("gotoNextStep:: current step is null");
		return;
	}

	playSound("Base_Select");
	currentStep->saveSettings();
	OutOfBoxStep* nextStep = getNextStep(currentStep);

	if ( nextStep == null )
	{
		screenDarken->setVisible(true);
		screenDarken->setOpacity(0);
		screenDarken->fadeTo(1, 1.5f);
		TimerManager::getInstance()->addTimer("ToChild", 1.5f, 0);
		nextButton->bubbleOut(0);
		nextButton->setState(ButtonStateNormal);
		return;
	}

	printf("GotoNextStep: %s\n", nextStep->name.str());
	OutOfBoxParentStep currentParentStep = getParentStepForStep(currentStep);
	OutOfBoxParentStep nextParentStep = getParentStepForStep(nextStep);

	moveToStep(currentStep, nextStep);
}

/// <summary>
/// onDialogHandled
/// </summary>
void OutOfBoxScene::onDialogHandled( DialogType dialogType, DialogButtonOption option )
{
	printf("Processing dialog with option id [%s]\n", option.id.str());

	if( dialogType == DialogTypeDownloadError )
	{
		if( option.id == "latersskaters" )
		{
			OutOfBoxStep* nextStep = cameraStep;
			moveToStep(currentStep, nextStep);
			return;
		}
		else
		{ currentStep->handleDialogClosed(dialogType, option); }
	}
	else if ( dialogType == DialogTypeAreYouSure )
	{
		if( option.id == "skip" )
		{ return skipActivation(); }
		else
		{ currentStep->handleDialogClosed(dialogType, option); }
	}
	else if( dialogType == DialogTypeActivationFail )
	{
		if( option.id == "skip" )
		{ return skipActivation(); }
		else
		{ currentStep->handleDialogClosed(dialogType, option); }
	}
	else if ( dialogType == DialogTypeHelp )
	{
		if( option.id == "skip" )
		{
			if( currentModal != null )
			{
				currentModal->cancel();
				currentModal = null;
			}

			DialogOptions options;
			options.title 	= "Are you sure?";
			options.prompt  = "Are you sure you want to skip this step? To unlock the Pet Play World learning game you need to complete registration.";
			options.buttonA = DialogButtonOption("Skip", "skip");
			options.buttonB = DialogButtonOption("Don't Skip", "no");
			showModalDialog(DialogTypeAreYouSure, options);
			return;
		}
		else
		{ currentStep->handleDialogClosed(dialogType, option); }
	}
	else if ( dialogType == DialogTypeSkipWifi || dialogType == DialogTypeSkipEthernet || dialogType == DialogTypeSkipActivation )
	{
		if ( option.id == "Yes" )
		{ return skipActivation(); }
		else
		{ currentStep->handleDialogClosed(dialogType, option); }
	}
	else
	{ currentStep->handleDialogClosed(dialogType, option); }

	super::onDialogHandled(dialogType,option);
}

void OutOfBoxScene::onDialogCanceled( DialogType dialogType)
{
	currentStep->handleDialogCanceled(dialogType);
	super::onDialogCanceled(dialogType);
}

void OutOfBoxScene::onEthernetPluggedIn()
{
	if ( currentStep == chooseWifi )
	{
		OutOfBoxStep* nextStep = ethernetStep;
		moveToStep(currentStep, nextStep);
	}
	else
	{ super::onEthernetPluggedIn(); }
}

void OutOfBoxScene::onEthernetUnplugged()
{

	if ( currentStep == ethernetStep )
	{
		OutOfBoxStep* nextStep = chooseWifi;
		moveToStep(currentStep, nextStep);
	}
	else
	{ super::onEthernetUnplugged(); }
}

/// <summary>
/// Base scene feature, handling controller pairing
/// </summary>
void OutOfBoxScene::onControllerSynced( HWController* controller, bool success )
{
	if ( currentStep != null && currentStep->typeOf(WelcomeStep::type()) )
	{
		currentStep->handleControllerSync(success);
	}

	super::onControllerSynced(controller, success);
}

/// <summary>
/// Base scene feature, handling controller connecting
/// </summary>
void OutOfBoxScene::onControllerConnected( HWController* controller )
{
	if ( currentStep != null && currentStep->typeOf(WelcomeStep::type()) )
	{
		currentStep->handleControllerConnected();
	}

	super::onControllerConnected(controller);
}

/// <summary>
/// Base scene feature, handling controller disconnecting
/// </summary>
void OutOfBoxScene::onControllerDisconnected( HWController* controller )
{
	if ( currentStep != null && currentStep->typeOf(WelcomeStep::type()) )
	{
		currentStep->handleControllerDisconnected();
	}

	super::onControllerDisconnected(controller);
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void OutOfBoxScene::onButtonHint()
{
	printf(" OutOfBoxScene::onButtonHint\n");
	if ( currentModal != null || currentStep == null || !helpButton->isVisibleHierarchy() )
		return super::onButtonHint();

	if ( currentStep != null )
	{
		playSound("Content_Hint");
		lockOutInput(0.5f);

		String title = "Help";
		String prompt = "For issues with your LeapTV, visit leapfrog.com/support/leaptv";
		String option = "Ok";

		//show modal dialog based on step
		if ( currentStep == ethernetStep || currentStep == chooseWifi )
		{
			title = StringsManager::getInstance()->getLocalizedString("OOBE_HELP_NETWORK_HEADER");
			prompt = StringsManager::getInstance()->getLocalizedString("OOBE_HELP_NETWORK_TEXT");
			option = StringsManager::getInstance()->getLocalizedString("OOBE_HELP_NETWORK_BUTTON");
		}
		else if ( currentStep == activationStep || currentStep == createProfileStep )
		{
			title = StringsManager::getInstance()->getLocalizedString("OOBE_HELP_REGISTRATIONCODE_HEADER");
			prompt = StringsManager::getInstance()->getLocalizedString("OOBE_HELP_REGISTRATIONCODE_TEXT");
			option = StringsManager::getInstance()->getLocalizedString("OOBE_HELP_REGISTRATIONCODE_BUTTON");
			printf("Title is %s prompt is %s option is %s", title.str(), prompt.str(), option.str());
		}

		DialogOptions options;
		options.title 	= title;
		options.prompt  = prompt;
		options.buttonA = DialogButtonOption(option, "Ok");

		//activation step
		if( currentStep == activationStep )
		{
			//only skip if not activation
			if( !SystemSettings::getIsActivated() )
			{
				options.buttonB = DialogButtonOption("Skip Registration", "skip");
			}
		}

		showModalDialog(DialogTypeHelp, options);
	}

	super::onButtonHint();
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void OutOfBoxScene::onButtonPressAction()
{
	//pass to dialogs first
	if ( currentModal != null )
	{ return super::onButtonPressAction(); }

	//lets process
	if ( currentHighlight != null && currentHighlight->name == "skipButton" && currentHighlight->isVisibleHierarchy() )
	{
		String title = "Skip";
		String content = "Do you want to skip?";
		String confirm = "OK";
		String cancel = "Don't skip";
		lockOutInput(0.8f);

		if ( currentStep == activationStep )
		{
			playSound("Content_BadgeAlert");
			title = StringsManager::getInstance()->getLocalizedString("STEPS_ACTIVATIONSTEP_DIALOG_SKIP_HEADER");
			content = StringsManager::getInstance()->getLocalizedString("STEPS_ACTIVATIONSTEP_DIALOG_SKIP_CONTENT");
			confirm = StringsManager::getInstance()->getLocalizedString("STEPS_ACTIVATIONSTEP_DIALOG_SKIP_SKIPBUTTON");
			cancel = StringsManager::getInstance()->getLocalizedString("STEPS_ACTIVATIONSTEP_DIALOG_SKIP_DONTSKIPBUTTON");

			DialogOptions options;
			options.title 	= title;
			options.prompt  = content;
			options.buttonA = DialogButtonOption(confirm, "Yes");
			options.buttonB = DialogButtonOption(cancel, "Cancel");
			showModalDialog(DialogTypeSkipActivation, options);
		}
		else if( currentStep == chooseWifi)
		{
			DialogOptions options;
			options.title 	= "Are you sure?";
			options.prompt  = "Are you sure you want to skip this step? To unlock the Pet Play World learning game you need to complete registration.";
			options.buttonA = DialogButtonOption("Skip", "Yes");
			options.buttonB = DialogButtonOption("Don't Skip", "No");
			showModalDialog(DialogTypeSkipWifi, options);
		}
		else
		{
			DialogOptions options;
			options.title 	= "Are you sure?";
			options.prompt  = "Are you sure you want to skip this step? To unlock the Pet Play World learning game you need to complete registration.";
			options.buttonA = DialogButtonOption("Skip", "Yes");
			options.buttonB = DialogButtonOption("Don't Skip", "No");
			showModalDialog(DialogTypeSkipEthernet, options);
		}
		return;
	}
	else if ( currentHighlight != null && currentHighlight->name == "helpButton" && currentHighlight->isVisibleHierarchy() )
	{
		onButtonHint();
		return;
	}
	else if ( currentHighlight != null && currentHighlight->name == "reconnectButton" && currentHighlight->isVisibleHierarchy() )
	{
		playSound("Base_Select");
		currentStep->saveSettings();
		overRideStep = currentStep;

		OutOfBoxStep* nextStep = chooseWifi;

		if ( ConnectionManager::getInstance()->hasEthernet() )
		{ nextStep = ethernetStep; }

		printf("GotoNextStep: %s\n", nextStep->name.str());
		OutOfBoxParentStep currentParentStep = getParentStepForStep(currentStep);
		OutOfBoxParentStep nextParentStep = getParentStepForStep(nextStep);
		moveToStep(currentStep, nextStep);

		return;
	}
	else if ( currentHighlight != null && currentHighlight->name == "nextButton" && currentHighlight->isVisibleHierarchy() )
	{
		gotoNextStep();
		return;
	}
	else if ( currentStep != null && currentStep->handlePressAction(currentHighlight) )
	{
		playSound("Parent_Keystroke_Backspace");
		setupHotSpots();
		return;
	}

	super::onButtonPressAction();
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void OutOfBoxScene::onButtonPressBack()
{
	if ( currentModal != null )
	{ currentModal->cancel(); }

	super::onButtonPressBack();
}

/// <summary>
/// Network error occured
/// </summary>
void OutOfBoxScene::onNetworkError(String networkName, String networkError )
{
	printf("OutOfBoxScene::onNetworkError [%s] msg[%s]\n", networkName.str(), networkError.str());
	if ( currentStep != null )
	{
		currentStep->handleNetworkError(networkName, networkError);

		if ( currentStep == chooseWifi )
		{
			currentHighlight = currentStep->getArrivalSelection();
			if ( currentHighlight != NULL )
			{
				((ButtonObject*) currentHighlight)->setState(ButtonStateActive);
			}
		}
	}
}

/// <summary>
/// Network connection made
/// </summary>
void OutOfBoxScene::onNetworkConnected( String networkName )
{
	printf("OutOfBoxScene::onNetworkConnected for %s\n", networkName.str());
	if ( currentStep == chooseWifi || currentStep == ethernetStep )
	{ currentStep->handleNetworkConnected(networkName); }

	//allow activation and download step to go ahead and handle this when off the scene graph
	activationStep->handleNetworkConnected(networkName);
	downloadStep->handleNetworkConnected(networkName);
}

/// <summary>
/// Download error'd out
/// </summary>
void OutOfBoxScene::onDownloadError()
{
	if( currentStep == downloadStep )
	{
		downloadStep->handleDownloadError();
	}

	super::onDownloadError();
}

static float updateProgress = 5;
static int testProgress = 0;
/// <summary>
/// Base scene feature, tick
/// </summary>
void OutOfBoxScene::update( float dt )
{
	unplugTimer -= dt;

	if ( updateProgress > 0 )
	{
		updateProgress -= dt;
		if ( updateProgress < 0 )
		{
			testProgress++;
			onDonwloadProgressed("TESTPACKAGE", testProgress);
		}
	}

	if ( enableNextStepTimer > 0 )
	{
		enableNextStepTimer -= dt;
		if ( enableNextStepTimer <= 0 )
		{
			printf("Actually taking over current highlight\n");
			nextButton->bubbleIn(0);
			setupHotSpots();

			//Don't overr ide selection if dialog is up
			if( currentModal == null )
			{ setCurrentHighlight(nextButton); }
			else
			{ preDialogSelection = nextButton; }
		}
	}

	if (exitTimer > 0 )
	{
		exitTimer -= dt;
		if(exitTimer <= 0)
		{ SceneManager::getInstance()->launchChildScene(Constants::System::appID); }
	}

	super::update(dt);
}

/// <summary>
/// Custom handling of analog changes
/// </summary>
void OutOfBoxScene::onAnalogChange( GameObject* objectLeft, GameObject* objectEntered )
{
	if ( currentModal != null )
	{
		return super::onAnalogChange(objectLeft, objectEntered);
	}

	if ( selectionLockOut > 0 )
		return;

	if ( currentStep != null && currentStep->handleAnlogChange(objectLeft, objectEntered, currentHighlight) )
	{
		setupHotSpots();
	}
	else
	{
		super::onAnalogChange(objectLeft, objectEntered);
	}
}

/// <summary>
/// Base scene feature, handling input
/// </summary>
void OutOfBoxScene::onSelectionChange( GameObject* old, GameObject* newObj )
{
	if ( old != null && newObj != null )
	{
		playSound("Parent_Highlight");
		printf("Selection changed from %s to %s \n", old->name.str(), newObj->name.str());
	}
	else if ( old != null && newObj == null )
	{
		printf("Selection changed from %s to nothing. \n", old->name.str());
	}
	else if ( old == null && newObj != null )
	{
		printf("Selection changed from nothing to %s. \n", newObj->name.str());
	}

	if ( old != null && old->typeOf(ButtonObject::type()) )
	{
		((ButtonObject*) old)->setState(ButtonStateNormal);

		if ( old->name == "helpButton" )
		{
			helpRollOver->bubbleOut(0);
		}
	}

	if ( newObj != null && newObj->typeOf(ButtonObject::type()) )
	{
		((ButtonObject*) newObj)->setState(ButtonStateActive);
		if ( newObj->name == "helpButton" )
		{
			helpRollOver->bubbleIn();
		}
	}

	if ( currentModal == null )
	{
		if ( currentStep != null )
		{
			currentStep->handleSelectionChange(old, newObj);
		}
	}

	super::onSelectionChange(old, newObj);

}

}

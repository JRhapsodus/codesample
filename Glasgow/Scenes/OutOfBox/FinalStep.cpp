#include "FinalStep.h"
#include "OutOfBoxScene.h"

namespace Glasgow
{

FinalStep::FinalStep(GameObject * replacementRoot, OutOfBoxScene* scene) : super( replacementRoot, scene)
{


}

FinalStep::~FinalStep()
{
	printf("FinalStep::~FinalStep()\n");
}

void FinalStep::update(float dt)
{

//	SpriteObject* rays = findSprite("rays");
//	Quaternion newRotation = rays->getRotation()*Quaternion::angleAxis(10*dt, Vec3f::Forward);
//	rays->setRotation(newRotation);


	super::update(dt);
}


/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void FinalStep::cameraArrived()
{
	outOfBox->hideHelpButton();
	SystemSettings::setInitialSetupDone(true);
	outOfBox->enableNextStep(2);

}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* FinalStep::getArrivalSelection()
{
	outOfBox->getWorldRoot()->findButton("nextButton")->setState(ButtonStateActive);
	return outOfBox->getWorldRoot()->findButton("nextButton");
}

}

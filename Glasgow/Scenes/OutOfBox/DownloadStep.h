#ifndef DOWNLOADSTEP_H_
#define DOWNLOADSTEP_H_

#include "OutOfBoxStep.h"
#include "SIEngine/Include/GlasgowControls.h"
namespace Glasgow
{

class DownloadStep : public OutOfBoxStep
{
	CLASSEXTENDS(DownloadStep, OutOfBoxStep);

public:
	DownloadStep(GameObject* replacementRoot, OutOfBoxScene* scene);
	virtual ~DownloadStep();

	/// <summary>
	/// If we want to handle the press action ourselves, usually welcomeHeaderLabelmeans the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	///
	/// </summary>
	virtual void handleDownloadError();

	/// <summary>
	///
	/// </summary>
	virtual void handlePendingChanged();

	/// <summary>
	/// Tests the status for the requirement of a firmware update and update
	/// the required UI that may be needed
	/// </summary>
	void checkFirmwareUpdate();

	/// <summary>
	/// Needed for firmware dialog prompt
	/// </summary>
	virtual void handleDialogClosed( DialogType dialogType, DialogButtonOption option );

	/// <summary>
	/// Over ridden for timer based polling in ContentManager
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Animation transition into a networking error
	/// </summary>
	void showNetworkError( String header, String error );

	/// <summary>
	/// Remove network error
	/// </summary>
	void hideNetworkError();


	/// <summary>
	/// Animation helper
	/// </summary>
	void showFinished();

	/// <summary>
	/// Network error occured
	/// </summary>
	virtual void handleNetworkError(String networkName, String networkError);

	/// <summary>
	/// Network connection made
	/// </summary>
	virtual void handleNetworkConnected( String networkName );

	/// <summary>
	/// Download progress
	/// </summary>
	virtual void handleDownloadProgress( String package, int downloadProgress);

	/// <summary>
	/// Download finished
	/// </summary>
	virtual void handleDonwloadFinished( String packageID );

	/// <summary>
	/// Download started
	/// </summary>
	virtual void handleDonwloadStarted( String packageID );

private:
	static float dlFinishedTimer;
	static float dlProgressTest;
	static float oneSecondTimer;

	/// <summary>
	/// Called before showing progress bar
	/// </summary>
	void firmwareTestUpdate(float dt);

	/// <summary>
	/// Test function for simulating with the package manager interface
	/// </summary>
	void testProgressBarUpdate(float dt);

	/// <summary>
	/// Called before showing progress bar
	/// </summary>
	void calculateDownloadInfo();

	/// <summary>
	/// Create netError and progressBar
	/// </summary>
	void createObjects();

	float	readyTimer;
	bool	errorShown;
	float 	retestTimer;
	float 	killTimer;
	float 	rebootTimer;

	GameObject* 		systemUpdatedRoot;
	GameObject* 		systemUpdatingRoot;
	TextLabel*			countdownLabel;
	ButtonObject* 		updateButton;
	NetErrorObject* 	netError;
	FirmwareProgressBar*  progressBar;
	QDBusPendingReply<QStringList> pendingRequest;
	RioPkgManager* packageManager;
};

}
#endif

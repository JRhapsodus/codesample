#ifndef AGEGATESTEP_H_
#define AGEGATESTEP_H_

#include "OutOfBoxStep.h"
#include "SIEngine/Include/GlasgowControls.h"


namespace Glasgow
{

class AgeGateStep : public OutOfBoxStep
{
	CLASSEXTENDS(AgeGateStep, OutOfBoxStep);
public:
	AgeGateStep(GameObject * replacementRoot, OutOfBoxScene* scene);
	virtual ~AgeGateStep();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Custom update
	/// </summary>
	virtual void update(float dt);


	void yearEntered();
private:
	String 				enteredYear;
	GateLockObject* 	numPad;
	TextLabel* 			entryPrompt;
	TextLabel* 			errorPrompt;
	TextLabel*			yearField;
};

}
#endif

#ifndef WELCOMESTEP_H_
#define WELCOMESTEP_H_

#include "OutOfBoxStep.h"

namespace Glasgow
{

class WelcomeStep : public OutOfBoxStep
{
	CLASSEXTENDS(WelcomeStep, OutOfBoxStep);

public:
	WelcomeStep(GameObject* replacementRoot, OutOfBoxScene* scene);
	virtual ~WelcomeStep();

	/// <summary>
	/// saves user settings
	/// </summary>
	virtual void saveSettings();

	/// <summary>
	/// If we want to handle the press action ourselves, usually welcomeHeaderLabelmeans the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Controller Sync Event
	/// </summary>
	virtual void handleControllerSync(bool success);

	/// <summary>
	/// Controller Connected Event
	/// </summary>
	virtual void handleControllerConnected();

	/// <summary>
	/// Controller Disconnected Event
	/// </summary>
	virtual void handleControllerDisconnected();

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	///<summary>
	/// Successful pairing of controller
	/// </summary>
	void pairingComplete();

	///<summary>
	/// Animation helper call
	/// </summary>
	void animateInController(float delay);

	/// <summary>
	/// Animation helper call
	/// </summary>
	void animateToNext();

	void showSynchError();
	void hideSynchError();

	void showSynchInstructions();
	void hideSynchInstructions();

private:
	bool hasPressedA;
	bool hasPairedController;
	bool isShowingError;

	SpriteObject* 	connectStep1Icon;
	SpriteObject* 	connectStep2Icon;
	TextLabel*		connectSubHeaderLabel;
	SpriteObject* 	controllerIcon;
	SpriteObject* 	gameIcon;
	TextLabel*	  	welcomeHeaderLabel;
	TextLabel*	  	welcomePressLabel;
	TextLabel*	  	welcomeSubHeaderLabel;
	TextLabel*	 	welcomePromptLabel;

	TextLabel*		errorPrompt;
	SpriteObject*	errorIcon;

};

}
#endif

#include "OutOfBoxStep.h"

namespace Glasgow
{

OutOfBoxStep::OutOfBoxStep(GameObject* objectRoot, OutOfBoxScene* scene) : super()
{
	//*****************************************************************
	// Take over the entire scene graph of the replacement object
	//*****************************************************************
	for ( int i = 0; i<objectRoot->numChildren(); i++ )
	{
		GameObject * child = objectRoot->childAtIndex(i);
		child->addRef(); //so remove from parent doesn't refCount to 0
		child->removeFromParent();
		addChild(child);
		child->release();
		i--;
	}
	name = objectRoot->name;
	setScale(objectRoot->getScale());
	setPosition(objectRoot->getPosition());
	setRotation(objectRoot->getRotation());
	objectRoot->removeFromParent();
	outOfBox = scene;
}

OutOfBoxStep::~OutOfBoxStep()
{ }

/// <summary>
/// If we want to handle analog changes
/// </summary>
void OutOfBoxStep::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	//base class
}

void OutOfBoxStep::handleDialogCanceled( DialogType dialogType )
{
	//base class
}

/// <summary>
/// If we want to be told about camera connection status
/// </summary>
void OutOfBoxStep::handleCameraDisconnected()
{
	//base class
}



/// <summary>
/// If we want to be told about camera connection status
/// </summary>
void OutOfBoxStep::handleCameraConnected()
{
	//base class
}


/// <summary>
///
/// </summary>
void OutOfBoxStep::handleDownloadError()
{
	//base class
}

/// <summary>
/// Download finished
/// </summary>
void OutOfBoxStep::handleDonwloadFinished( String packageID )
{
	//base
}


/// <summary>
///
/// </summary>
void OutOfBoxStep::handlePendingChanged()
{
	//base
}

/// <summary>
/// Download started
/// </summary>
void OutOfBoxStep::handleDonwloadStarted( String packageID )
{
}

/// <summary>
/// Network connection made
/// </summary>
void OutOfBoxStep::handleNetworkConnected( String networkName )
{
	//base class
}

/// <summary>
/// Network error occurred
/// </summary>
void OutOfBoxStep::handleNetworkError(String networkName, String networkError)
{
	//base class
}

void OutOfBoxStep::saveSettings()
{
	//base implementation
}

/// <summary>
/// Controller Sync Event
/// </summary>
void OutOfBoxStep::handleControllerSync(bool success)
{
	//base class
}

/// <summary>
/// Controller Connected Event
/// </summary>
void OutOfBoxStep::handleControllerConnected()
{
	//base class
}

/// <summary>
/// Controller Disconnected Event
/// </summary>
void OutOfBoxStep::handleControllerDisconnected()
{
	//base class
}

/// <summary>
/// Download progress
/// </summary>
void OutOfBoxStep::handleDownloadProgress( String package, int downloadProgress)
{
	//base class
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool OutOfBoxStep::handlePressAction(GameObject*& objectHighlight)
{ return false; }

/// <summary>
/// If we want to handle the press back ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool OutOfBoxStep::handlePressBack(GameObject*& objectHighlight)
{ return false; }

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool OutOfBoxStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	if( objectEntered != null )
	{ ((ButtonObject*)objectEntered)->setState(ButtonStateActive); }

	if( objectLeft != null )
	{ ((ButtonObject*)objectLeft)->setState(ButtonStateNormal); }

	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void OutOfBoxStep::cameraArrived()
{ }

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void OutOfBoxStep::cameraLeaving()
{ }

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* OutOfBoxStep::getArrivalSelection()
{ return NULL; }

/// <summary>
/// Input forwarding call to handle selection changes
/// </summary>
void OutOfBoxStep::handleSelectionChange( GameObject* old, GameObject* newObj)
{ }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void OutOfBoxStep::addHotSpots( ObjectArray* trackingList )
{ }


} /* namespace Glasgow */

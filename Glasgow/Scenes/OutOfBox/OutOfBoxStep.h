#ifndef OOOBESTEP_H_
#define OOOBESTEP_H_

#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Include/SIActions.h"

namespace Glasgow
{

class OutOfBoxScene;

class OutOfBoxStep : public GameObject
{
	CLASSEXTENDS(OutOfBoxStep, GameObject);
public:
	OutOfBoxStep(GameObject* objectRoot, OutOfBoxScene* scene);
	virtual ~OutOfBoxStep();

	/// <summary>
	/// Controller Sync Event
	/// </summary>
	virtual void handleControllerSync(bool success);

	/// <summary>
	/// Controller Connected Event
	/// </summary>
	virtual void handleControllerConnected();

	/// <summary>
	/// Controller Disconnected Event
	/// </summary>
	virtual void handleControllerDisconnected();

	/// <summary>
	/// If we want to be told about camera connection status
	/// </summary>
	virtual void handleCameraDisconnected();

	/// <summary>
	/// If we want to be told about camera connection status
	/// </summary>
	virtual void handleCameraConnected();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle the press back ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressBack(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual void handleDialogClosed( DialogType dialogType, DialogButtonOption option );

	/// <summary>
	/// When a dialog dismisses without making a selection.
	/// </summary>
	virtual void handleDialogCanceled( DialogType dialogType );

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Inpuit forwarding call to handle selection changes
	/// </summary>
	virtual void handleSelectionChange( GameObject* old, GameObject* newObj);

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Network error occured
	/// </summary>
	virtual void handleNetworkError(String networkName, String networkError);

	/// <summary>
	/// Network connection made
	/// </summary>
	virtual void handleNetworkConnected( String networkName );

	/// <summary>
	/// Download finished
	/// </summary>
	virtual void handleDonwloadFinished( String packageID );

	/// <summary>
	///
	/// </summary>
	virtual void handlePendingChanged();

	/// <summary>
	/// Download started
	/// </summary>
	virtual void handleDonwloadStarted( String packageID );


	/// <summary>
	///
	/// </summary>
	virtual void handleDownloadError();

	/// <summary>
	/// Download progress
	/// </summary>
	virtual void handleDownloadProgress( String package, int downloadProgress);

	/// <summary>
	/// All oobe steps must save their settings when leaving screen
	/// </summary>
	virtual void saveSettings();

protected:
	OutOfBoxScene* outOfBox;
};

}
#endif

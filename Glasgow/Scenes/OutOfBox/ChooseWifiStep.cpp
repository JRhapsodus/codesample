#include "ChooseWifiStep.h"
#include "OutOfBoxScene.h"
namespace Glasgow
{

ChooseWifiStep::ChooseWifiStep(GameObject* objectRoot, OutOfBoxScene* scene) : super(objectRoot, scene)
{
	connectingRoot		= find("connectionStatus");
	passwordRoot		= find("passwordRoot");
	wifiList 			= find("wifiList");

	ethernetPrompt		= findLabel("ethernetPrompt");
	connectingPrompt	= findLabel("connectingPrompt");
	entryPrompt			= findLabel("entryPrompt");
	connectedPrompt		= findLabel("connectedPrompt");
	errorPrompt			= findLabel("errorPrompt");
	scanButton			= findButton("scanButton");
	skipButton			= findButton("skipButton");

	wifiSlider		 	= (VerticalSlider*)wifiList->find("verticalSlider");
	noNetworksPrompt	= wifiList->findLabel("noNetworksFound");
	scanningPrompt		= wifiList->findLabel("scanning");

	macAddress			= entryPrompt->findLabel("macAddress");

	backButton			= passwordRoot->findButton("backButton");
	passwordField		= passwordRoot->find("passwordField")->findLabel("inputEntry");
	connectButton		= passwordRoot->findButton("connectButton");

	connectedStrength1  = connectingRoot->findSprite("wifi1");
	connectedStrength2  = connectingRoot->findSprite("wifi2");
	connectedStrength3  = connectingRoot->findSprite("wifi3");
	connectedStrength4  = connectingRoot->findSprite("wifi4");
	transmitting  		= connectingRoot->findSprite("wifiAnimation");

	systemIcon 			= findSprite("icon");
	firmwareStatusLabel = findLabel("firmwareStatusLabel");
	keyboard			= new KeyboardObject((Scene*)scene, true);

	passwordField->setBatched(false);
	passwordRoot->setVisible(false);
	passwordRoot->addChild(keyboard);
	keyboard->release();
	passwordField->setText("");

	keyboard->setPosition( Vec3f( 100, -75, -100 ) );
	keyboard->setText("");
	keyboard->setMaxCharacters(Constants::System::wifiPasswordMaximumCharLimit);
	systemIcon->setVisible(false);
	firmwareStatusLabel->setVisible(false);

	setupInitialState();
}

ChooseWifiStep::~ChooseWifiStep()
{
	printf("ChooseWifiStep::~ChooseWifiStep()\n");
	wifiSlider->removeAll();
}

void ChooseWifiStep::setupInitialState()
{
	attemptingConnect	= false;
	connected 			= false;
	networkError 		= false;
	firstScan 			= true;
	keyboardPresent 	= false;
	connectingTimer 	= -1;

	backButton->setVisible(false);
	passwordRoot->setVisible(false);
	passwordField->setText("");

	keyboard->setVisible(false);
	keyboard->reset();
	keyboard->setPosition( Vec3f( 75, -75, -100 ) );
	keyboard->setText("");
	keyboard->setMaxCharacters(Constants::System::wifiPasswordMaximumCharLimit);

	wifiSlider->removeAll();
	wifiList->setPosition(Vec3f(0, -85, wifiList->getPosition().z));
}

void ChooseWifiStep::loadGameObjectState(GameObjectState state)
{
	printf("ChooseWifiStep::loadGameObjectState\n");
	super::loadGameObjectState(state);
	setupInitialState();
}

/// <summary>
/// Custom update
/// </summary>
void ChooseWifiStep::update(float dt)
{
	if( connectingTimer > 0 )
	{
		connectingTimer -= dt;
		if( connectingTimer <= 0 )
		{
			String passwordEntered = keyboard->getText();
			networkError = false;
			ConnectionManager::getInstance()->connectToWifiNetwork( currentNetworkName, passwordEntered );
		}
	}

	super::update(dt);
}

/// <summary>
/// Network error occured
/// </summary>
void ChooseWifiStep::handleNetworkError(String networkName, String errorMessage)
{
	if( keyboardPresent )
		return;

	if(!currentNetworkName.equalsIgnoreCase(networkName))
		return;

	if( transmitting->isVisibleSelf() )
	{
		outOfBox->lockOutInput(0);
		attemptingConnect = false;
		outOfBox->playSound("Base_WiFiDisconnected");
		networkError = true;
		transmitting->setVisible(false);
		connectedStrength1->setVisible(true);

		errorPrompt->recursiveFadeIn(0.25f );
		TextLabel* errorLabel = errorPrompt->findLabel("errorLabel");
		errorLabel->setText(errorMessage);
		connectingPrompt->recursiveFadeOut(0.25f );
		entryPrompt->recursiveFadeOut(0.25f);

		scanButton->setScale( Vec3f::Zero );
		scanButton->makeVisible(true);
		scanButton->scaleTo( scanButton->originalScale, 0.25f );
		scanButton->setState(ButtonStateNormal);

		skipButton->setScale( Vec3f::Zero );
		skipButton->makeVisible(true);
		skipButton->scaleTo( skipButton->originalScale, 0.25f );
		skipButton->setState(ButtonStateNormal);

		if( !outOfBox->isShowingModalDialog() )
		{
			GameObject* selection = selectedWifiButton();
			if(selection)
				outOfBox->setCurrentHighlight(selection);
			else
				outOfBox->setCurrentHighlight(scanButton);
		}
	}
}


/// <summary>
/// Network connection made
/// </summary>
void ChooseWifiStep::handleNetworkConnected( String networkName )
{
	printf("ChooseWifiStep::handleNetworkConnected\n");
	if( keyboardPresent )
		return;

	if( transmitting->isVisibleSelf() )
	{
		outOfBox->lockOutInput(0);
		attemptingConnect = false;
		outOfBox->playSound("Base_TransformSuccess");
		wifiList->scaleTo(Vec3f::Zero, 0.5f);
		scanButton->scaleTo(Vec3f::Zero, 0.5f);
		skipButton->scaleTo(Vec3f::Zero, 0.5f);
		connectingRoot->waitFor(0.5f);
		connectingRoot->moveTo(Vec3f(-300, -52, 120), 0.5f);
		transmitting->setVisible(false);
		connectedStrength4->setVisible(true);
		entryPrompt->recursiveFadeOut();
		connectingPrompt->recursiveFadeOut();

	    connectedPrompt->findLabel("macAddress")->setText(SystemSettings::getWifiMacAddress());
	    connectedPrompt->recursiveFadeIn(1);
		outOfBox->enableNextStep(1.5f);
		connected = true;

		printf("Wifi connected to online lets ask Package Manager if there is a firmware update\n");
		if( ContentManager::getInstance()->getFirmwareStatus() != FirmwareNotAvailabe )
		{
			printf("Firmware update available\n");
			systemIcon->bubbleIn(1);
			firmwareStatusLabel->fadeIn(1);
		}
		else
		{
			systemIcon->setVisible(false);
			firmwareStatusLabel->setVisible(false);
			printf("No Firmware update at this time\n");
		}
	}
}


/// <summary>
/// Setup display for scan
/// </summary>
void ChooseWifiStep::setDisplayForScan()
{
	if( firstScan )
	{
		entryPrompt->setVisible(false);
		connectingRoot->setVisible(false);
		connectedPrompt->setVisible(false);
		connectedStrength1->setVisible(false);
		connectedStrength2->setVisible(false);
		connectedStrength3->setVisible(false);
		connectedStrength4->setVisible(false);
		transmitting->setVisible(false);
		connectingPrompt->setVisible(false);
		errorPrompt->setVisible(false);
		ethernetPrompt->setVisible(false);
		noNetworksPrompt->setVisible(false);
		scanButton->setVisible(false);
		skipButton->setVisible(false);
		backButton->setVisible(false);
		connectedPrompt->setVisible(false);
		scanningPrompt->setVisible(true);

		macAddress->setText(SystemSettings::getWifiMacAddress());
		outOfBox->lockOutInput(2);
		TimerManager::getInstance()->addTimer("ScanRequest", 2, 0 );
	}
	else
	{
		scanButton->cancelActions();
		skipButton->cancelActions();

		scanButton->scaleTo(Vec3f::Zero, 0.25f);
		skipButton->scaleTo(Vec3f::Zero, 0.25f);
		connectingRoot->setVisible(false);

		entryPrompt->recursiveFadeOut();
		noNetworksPrompt->recursiveFadeOut();
		connectingPrompt->recursiveFadeOut();
		errorPrompt->recursiveFadeOut();

		scanningPrompt->recursiveFadeIn(0.5f);
		outOfBox->lockOutInput(2);
		TimerManager::getInstance()->addTimer("ScanRequest", 2, 0 );
	}
}

/// <summary>
/// Initiates a scan request
/// </summary>
void ChooseWifiStep::doScan()
{
	printf("ChooseWifiStep::doScan()\n");
	ConnectionManager::getInstance()->scanForWifiNetworks();
	onScanFinished();
}

/// <summary>
/// Returns selected wifibutton from slider
/// </summary>
WifiButton* ChooseWifiStep::selectedWifiButton()
{ return (WifiButton*)wifiSlider->getSelectedItem(); }


/// <summary>
/// When scan completes
/// </summary>
void ChooseWifiStep::onScanFinished()
{
	firstScan = false;

	wifiList->moveTo( Vec3f(-225, -85, wifiList->getPosition().z), 0.5f );

	TRelease<ObjectArray> networks( ConnectionManager::getInstance()->getAllNetworks() );

	//Found networks
	if( networks->getSize() > 0 )
	{
		for ( int i = 0; i<networks->getSize(); i++ )
		{
			NetworkInfo* networkConnection = (NetworkInfo*)networks->elementAt(i);

			if(!networkConnection->isValidWifi())
				continue;

			TRelease<WifiButton> wifiButton( (WifiButton*)Scene::instantiate("WifiButton") );
			wifiButton->name = networkConnection->networkName;
			wifiSlider->addItem( wifiButton );
			wifiButton->setNetworkName( networkConnection->networkName );

			// 0 -> 25
			if( networkConnection->strength > 0 && networkConnection->strength < 26 )
			{ wifiButton->setWifiStrength(  WifiStrengthWeakest ); }
			// 26-50
			else if ( networkConnection->strength < 51 )
			{ wifiButton->setWifiStrength(  WifiStrengthWeak ); }
			//51 - 75
			else if ( networkConnection->strength < 76 )
			{ wifiButton->setWifiStrength(  WifiStrengthStrong ); }
			// 76-> 100
			else
			{ wifiButton->setWifiStrength(  WifiStrengthStrongest ); }



			wifiButton->setPosition( Vec3f::Zero );
		}

		wifiSlider->positionFirstElements();

		scanningPrompt->recursiveFadeOut();
		entryPrompt->recursiveFadeIn(0.5f);
	}
	else
	{
		noNetworksPrompt->recursiveFadeIn(0.5f);
		scanningPrompt->recursiveFadeOut();
		entryPrompt->recursiveFadeIn(0.5f );
	}

	scanButton->setScale( Vec3f::Zero );
	scanButton->setVisible(true);
	scanButton->waitFor(0.5f);
	scanButton->scaleTo( scanButton->originalScale, 0.25f );

	skipButton->setScale( Vec3f::Zero );
	skipButton->setVisible(true);
	skipButton->waitFor(0.5f);
	skipButton->scaleTo( skipButton->originalScale, 0.25f );
}


/// <summary>
/// Tries to attempt a wifi connection
/// </summary>
bool ChooseWifiStep::collectPassword(WifiButton* wifiButton)
{
	currentNetworkName = wifiButton->name;

	if( ConnectionManager::getInstance()->isServiceOnline(currentNetworkName) )
	{
		wifiList->scaleTo(Vec3f::Zero, 0.5f);
		scanButton->scaleTo(Vec3f::Zero, 0.5f);
		skipButton->scaleTo(Vec3f::Zero, 0.5f);
		connectingRoot->waitFor(0.5f);
		connectingRoot->makeVisible(true);
		connectingRoot->moveTo(Vec3f(-300, -52, 120), 0.5f);
		transmitting->setVisible(false);
		connectedStrength4->setVisible(true);

		entryPrompt->recursiveFadeOut();
		errorPrompt->recursiveFadeOut();
		connectingPrompt->recursiveFadeOut();


		String macAddress = SystemSettings::getWifiMacAddress();
		if( ContentManager::getInstance()->getFirmwareStatus() != FirmwareNotAvailabe )
		{
			printf("Firmware update available\n");
			systemIcon->bubbleIn(1);
			firmwareStatusLabel->fadeIn(1);
		}

	    connectedPrompt->findLabel("macAddress")->setText(macAddress);
	    connectedPrompt->recursiveFadeIn(1);
		outOfBox->enableNextStep(1.5f);
		connected = true;
		lastNetworkName = currentNetworkName;
		return false;
	}
	else if(ConnectionManager::getInstance()->isNetworkSecure(currentNetworkName) == false)
	{
		attemptConnection(currentNetworkName);
		lastNetworkName = currentNetworkName;
		return false;
	}
	else
	{
		keyboardPresent = true;
		connectButton->setVisible(false);
		passwordRoot->setPosition( Vec3f::Right*1280 );
		passwordRoot->setVisible(true);
		passwordRoot->moveBy( Vec3f::Left*1280, 1 );
		backButton->bubbleIn(1);

		keyboard->setVisible(true);

		//For password re-entry lets not reset it completely
		//so users can see what was typed and change accordingly???
		if( lastNetworkName != currentNetworkName )
		{
			keyboard->reset();
			keyboard->setText("");
			passwordField->setText("");
		}

		outOfBox->hideNextStep();

		connectingRoot->moveBy( Vec3f::Left*1280, 1 );
		errorPrompt->moveBy( Vec3f::Left*1280, 1 );
		connectingPrompt->moveBy( Vec3f::Left*1280, 1 );

		wifiList->moveBy( Vec3f::Left*1280, 1 );
		noNetworksPrompt->moveBy( Vec3f::Left*1280, 1 );
		scanningPrompt->moveBy( Vec3f::Left*1280, 1 );
		ethernetPrompt->moveBy( Vec3f::Left*1280, 1 );
		entryPrompt->moveBy( Vec3f::Left*1280, 1 );
		scanButton->moveBy( Vec3f::Left*1280, 1 );
		skipButton->moveBy( Vec3f::Left*1280, 1 );

		TextLabel* networkNameLabel = passwordRoot->findLabel("enterPasswordText")->findLabel("networkName");
		networkNameLabel->setText(wifiButton->name);
		lastNetworkName = currentNetworkName;
		return true;
	}
}

///<summary>
///animation helper
///</summary>
void ChooseWifiStep::cancelCollectPassword()
{
	if(keyboardPresent)
	{
		outOfBox->lockOutInput(1);

		keyboardPresent = false;
		passwordRoot->moveBy( Vec3f::Right*1280, 1 );
		passwordRoot->makeVisible(false);

		keyboard->setText("");
		passwordField->setText("");

		backButton->setState(ButtonStateNormal);
		backButton->bubbleOut(0);

		connectingRoot->moveBy( Vec3f::Right*1280, 1 );
		errorPrompt->moveBy( Vec3f::Right*1280, 1 );
		connectingPrompt->moveBy( Vec3f::Right*1280, 1 );

		wifiList->moveBy( Vec3f::Right*1280, 1 );
		noNetworksPrompt->moveBy( Vec3f::Right*1280, 1 );
		scanningPrompt->moveBy( Vec3f::Right*1280, 1 );
		ethernetPrompt->moveBy( Vec3f::Right*1280, 1 );
		entryPrompt->moveBy( Vec3f::Right*1280, 1 );
		scanButton->moveBy( Vec3f::Right*1280, 1 );
		skipButton->moveBy( Vec3f::Right*1280, 1 );

		if( !outOfBox->isShowingModalDialog() )
		{
			GameObject* selection = selectedWifiButton();
			if(selection)
				outOfBox->setCurrentHighlight(selection);
			else
				outOfBox->setCurrentHighlight(scanButton);
		}
	}

}

/// <summary>
/// Tries to attempt a wifi connection
/// </summary>
void ChooseWifiStep::attemptConnection(String networkName)
{
	attemptingConnect = true;

	if( keyboardPresent )
	{
		connectButton->setState(ButtonStateNormal);
		keyboardPresent = false;
		passwordRoot->moveBy( Vec3f::Right*1280, 1 );
		passwordRoot->makeVisible(false);
		wifiList->moveBy( Vec3f::Right*1280, 1 );
		noNetworksPrompt->moveBy( Vec3f::Right*1280, 1 );
		scanningPrompt->moveBy( Vec3f::Right*1280, 1 );
		ethernetPrompt->moveBy( Vec3f::Right*1280, 1 );
		entryPrompt->moveBy( Vec3f::Right*1280, 1 );

		backButton->bubbleOut(0);

		errorPrompt->moveBy( Vec3f::Right*1280, 1 );
		errorPrompt->setVisible(false);

		scanButton->moveBy( Vec3f::Right*1280, 1 );
		skipButton->moveBy( Vec3f::Right*1280, 1 );
	}

	connectingPrompt->setVisible(true);
	connectingPrompt->findLabel("labelNetworkName")->setText(networkName);

	connectingPrompt->recursiveFadeIn(0.5f);

	connectingRoot->setPosition( Vec3f( 140, 0, 120 ) );
	connectingPrompt->setPosition( Vec3f( 265, 40, -260 ) );
	connectingRoot->setScale( Vec3f::Zero );
	connectingRoot->waitFor(0.5f);
	connectingRoot->makeVisible(true);
	connectingRoot->scaleTo(connectingRoot->originalScale, 0.5f );
	transmitting->setVisible(true);
	transmitting->playAnimation("Transmitting");

	skipButton->setVisible(false);

	entryPrompt->setVisible(false);
	scanButton->setVisible(false);

	outOfBox->setupHotSpots();

	connectingTimer = 1.5f;

}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ChooseWifiStep::handlePressAction(GameObject*& objectHighlight)
{
	if ( objectHighlight != null && !attemptingConnect)
	{
		if( objectHighlight == scanButton )
		{
			outOfBox->lockOutInput(1);
			scanButton->bubbleOut();
			wifiSlider->prepareForScan();
			setDisplayForScan();
			objectHighlight = null;
			return true;
		}
		else if ( objectHighlight == connectButton )
		{
			outOfBox->removeSelection();
			outOfBox->lockOutInput(Constants::Time::wifiInputTimer);
			attemptConnection(currentNetworkName);
			return true;
		}
		else if ( objectHighlight->typeOf(WifiButton::type()) )
		{
			((WifiButton*)objectHighlight)->setState(ButtonStateNormal);

			outOfBox->lockOutInput(1);
			if( collectPassword((WifiButton*)objectHighlight) )
			{  outOfBox->setCurrentHighlight(keyboard->getDefaultHighLight()); }

			return true;
		}
		else if (objectHighlight == backButton)
		{
			outOfBox->lockOutInput(1);
			cancelCollectPassword();
		}
		else if ( keyboard->onButtonPressAction() )
		{
			outOfBox->lockOutInput(0.2f);
			String txt = keyboard->getText();
			if(txt.length() > Constants::UI::wifiPasswordDisplayLimit)
				txt = txt.substring(txt.length()-Constants::UI::wifiPasswordDisplayLimit,txt.length());

			if(txt.length() == 0)
				connectButton->setVisible(false);
			else if(!connectButton->isVisibleSelf())
				connectButton->bubbleIn();
			outOfBox->setupHotSpots();

			if (objectHighlight->name == "Backspace")
			{
				// Fade last character out takes 0.2f so we block for that long
				passwordField->removeLastCharacter();
				passwordField->changeTo(txt);
				outOfBox->playSound("Parent_Keystroke_Backspace");
			}
			else
			{
				outOfBox->playSound("Parent_Keystroke");
				passwordField->setText(txt);
				passwordField->fadeLastCharacterIn();
				objectHighlight = keyboard->getDefaultHighLight();
			}
		}
	}

	return false;
}



/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ChooseWifiStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{

	if( !connected )
	{
		if ( keyboardPresent && keyboard->onSelectionChange(objectLeft, objectEntered) )
		{
			if( objectEntered != connectButton )
			{
				outOfBox->playSound("Base_Highlight");
				currentHighlight = keyboard->getDefaultHighLight();
			}

			return true;
		}
		if ( !keyboardPresent && wifiSlider->onAnalogChange(objectLeft, objectEntered ) )
		{
			outOfBox->playSound("Base_MenuScroll");
			currentHighlight = wifiSlider->getSelectedItem();
			return true;
		}
	}

	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ChooseWifiStep::cameraArrived()
{
	printf("ChooseWifiStep::cameraArrived()\n");
	outOfBox->enableHelpButton();

	wifiSlider->prepareForScan();
	setDisplayForScan();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ChooseWifiStep::getArrivalSelection()
{
	if( networkError )
	{ return wifiSlider->getSelectedItem(); }

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ChooseWifiStep::addHotSpots( ObjectArray* trackingList )
{
	if( !connected )
	{
		wifiSlider->addObjectsToTracking(trackingList);

		if(scanButton->isVisibleHierarchy())
			trackingList->addElement(scanButton);
		if(skipButton->isVisibleHierarchy())
			trackingList->addElement(skipButton);
		if(backButton->isVisibleHierarchy())
			trackingList->addElement(backButton);
		if(connectButton->isVisibleHierarchy())
			trackingList->addElement(connectButton);
	}

	if( keyboardPresent )
	{ keyboard->setupHotSpots(); }
}


}

#include "LockCreationStep.h"
#include "OutOfBoxScene.h"

namespace Glasgow
{

LockCreationStep::LockCreationStep(GameObject * replacementRoot, OutOfBoxScene* scene) : super(replacementRoot, scene)
{
	confirm = false;
	numPad					= (GateLockObject*)find("numPad");
	confirmPrompt			= findLabel("confirmPrompt");
	entryPrompt				= findLabel("entryPrompt");
	finishedPrompt			= findLabel("finishPrompt");
	finalPinLabel 			= findLabel("finalPin");
	errorPrompt				= findLabel("errorPrompt");
	finalPinBacking			= findSprite("backing");
	pinBacking				= findSprite("pinField");
	confirmPinBacking		= findSprite("confirmPinField");
	pinLabel 				= pinBacking->findLabel("pinText");
	confirmPinLabel			= confirmPinBacking->findLabel("confirmPinText");

	finalPinBacking->setVisible(false);
	pinLabel->setBatched(false);
}

LockCreationStep::~LockCreationStep()
{
	printf("LockCreationStep::~LockCreationStep()\n");
}

void LockCreationStep::saveSettings()
{
	printf("LockCreationStep::saveSettings");

}
/// <summary>
/// Pin entered
/// </summary>
void LockCreationStep::pinEntered()
{
	printf("Pin entered:  %s =confirm= %s\n", enteredPin.str(), confirmPin.str() );

	if( confirm )
	{

		if( enteredPin == confirmPin )
		{
			//Success case!
			SystemSettings::setPin(enteredPin);
			outOfBox->lockOutInput(0.5f);

			finalPinBacking->bubbleIn(0.5f);
			entryPrompt->recursiveFadeOut();
			confirmPrompt->recursiveFadeOut();
			errorPrompt->cancelActions();
			errorPrompt->recursiveFadeOut();

			finishedPrompt->recursiveFadeIn(0.5f);
			finalPinLabel->recursiveFadeIn(0.5f);
			finalPinLabel->setText(confirmPin);

			numPad->scaleTo(Vec3f::Zero, 0.25f);
			pinBacking->bubbleOut(0);
			confirmPinBacking->bubbleOut(0);

			outOfBox->enableNextStep(0.5f);
		}
		else
		{
			//error case
			confirm = false;
			enteredPin = "";
			confirmPin = "";

			pinLabel->setText("");
			confirmPinLabel->setText("");

			outOfBox->lockOutInput(1.5f);
			errorPrompt->cancelActions();

			entryPrompt->setVisible(false);
			confirmPrompt->setVisible(false);
			errorPrompt->recursiveFadeIn(0.5f);
			entryPrompt->recursiveFadeIn(0.5f);

			pinBacking->bubbleOut(0);
			pinBacking->waitFor(0.25f);
			pinBacking->moveBy(Vec3f::Down*95,0.01f);
			pinBacking->bubbleIn(0.1f);
			confirmPinBacking->bubbleOut(0);

			errorPrompt->recursiveFadeOut(4);
		}
	}
	else
	{
		confirm = true;

		errorPrompt->cancelActions();
		errorPrompt->setVisible(false);

		outOfBox->lockOutInput(1.25f);
		entryPrompt->recursiveFadeOut();
		pinBacking->waitFor(0.5f);
		pinBacking->moveBy(Vec3f::Up*95,0.75f);
		confirmPinBacking->bubbleIn(1.25f);
		confirmPrompt->recursiveFadeIn(1.25f);
	}
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool LockCreationStep::handlePressAction(GameObject*& objectHighlight)
{

	if ( objectHighlight != null && objectHighlight->descendantOf(numPad) )
	{
		outOfBox->lockOutInput(0.18f);

		String pinValue = objectHighlight->name;
		if( pinValue == "backspacebutton" )
		{
			if( !confirm )
			{
				if( enteredPin.length() > 0 )
				{
					enteredPin = enteredPin.substring(0, enteredPin.length()-1);
					outOfBox->playSound("Parent_Keystroke_Backspace");
					pinLabel->removeLastCharacter();
				}
			}
			else
			{
				if( confirmPin.length() > 0 )
				{
					confirmPin = confirmPin.substring(0, confirmPin.length()-1);
					outOfBox->playSound("Parent_Keystroke_Backspace");
					confirmPinLabel->removeLastCharacter();
				}
			}
		}
		else
		{
			if( confirm )
			{
				outOfBox->playSound("Parent_Keystroke");
				confirmPin += pinValue;
				int len = confirmPin.length();
				String encryptedPin = "";
				while(len--)
				{ encryptedPin.append(Constants::UI::encryptionDisplayPattern); }

				confirmPinLabel->setText(encryptedPin);
				confirmPinLabel->fadeLastCharacterIn();
				if( confirmPin.length() == 4 )
				{ pinEntered(); }
			}
			else
			{
				outOfBox->playSound("Parent_Keystroke");
				enteredPin += pinValue;
				int len = enteredPin.length();
				String encryptedPin = "";
				while(len--)
				{ encryptedPin.append(Constants::UI::encryptionDisplayPattern); }

				pinLabel->setText(encryptedPin);
				pinLabel->fadeLastCharacterIn();
				if( enteredPin.length() == 4 )
				{ pinEntered(); }
			}
		}

	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool LockCreationStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void LockCreationStep::cameraArrived()
{
	outOfBox->hideHelpButton();

	errorPrompt->setVisible(false);
	entryPrompt->setVisible(false);
	confirmPrompt->setVisible(false);
	finishedPrompt->setVisible(false);
	finalPinLabel->setVisible(false);
	confirmPinBacking->setVisible(false);
	confirmPinLabel->setText("");
	entryPrompt->recursiveFadeIn(1.5f);
	pinLabel->setText("");
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* LockCreationStep::getArrivalSelection()
{ return numPad->find("1"); }

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void LockCreationStep::addHotSpots( ObjectArray* trackingList )
{
	if( confirmPin.length()>0 && confirmPin == enteredPin )
	{}
	else
	{
		numPad->addHotSpots(trackingList);
	}
}

/// <summary>
/// Custom update
/// </summary>
void LockCreationStep::update(float dt)
{
	super::update(dt);
}



}

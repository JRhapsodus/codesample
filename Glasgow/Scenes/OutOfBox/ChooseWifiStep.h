#ifndef CHOOSEWIFISTEP_H_
#define CHOOSEWIFISTEP_H_

#include "OutOfBoxStep.h"
#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Include/SIActions.h"
#include "SIEngine/Include/GlasgowControls.h"


namespace Glasgow
{

class ChooseWifiStep : public OutOfBoxStep
{
	 CLASSEXTENDS(ChooseWifiStep, OutOfBoxStep);

public:
	ChooseWifiStep(GameObject* objectRoot, OutOfBoxScene* scene);
	virtual ~ChooseWifiStep();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Initiates a scan request
	/// </summary>
	void doScan();

	/// <summary>
	/// When scan completes
	/// </summary>
	void onScanFinished();

	/// <summary>
	/// Setup display for scan
	/// </summary>
	void setDisplayForScan();

	/// <summary>
	/// Collect password
	/// </summary>
	bool collectPassword(WifiButton* wifiButton);

	/// <summary>
	/// Tries to attempt a wifi connection
	/// </summary>
	void attemptConnection(String networkName);

	///<summary>
	///animation helper
	///</summary>
	void cancelCollectPassword();

	/// <summary>
	/// Network error occured
	/// </summary>
	void handleNetworkError(String networkName, String networkError);

	/// <summary>
	/// Network connection made
	/// </summary>
	void handleNetworkConnected( String networkName );

	/// <summary>
	/// Custom update
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// Returns selected wifibutton from slider
	/// </summary>
	WifiButton* selectedWifiButton();

	virtual void loadGameObjectState(GameObjectState state);

	void setupInitialState();
private:
	GameObject* wifiList;
	VerticalSlider* wifiSlider;
	bool keyboardPresent;
	float connectingTimer;
	String currentNetworkName;
	String lastNetworkName;
	bool networkError;
	bool connected;
	bool attemptingConnect;

	TextLabel* 		noNetworksPrompt;
	TextLabel* 		scanningPrompt;
	TextLabel* 		ethernetPrompt;
	TextLabel* 		entryPrompt;
	TextLabel* 		errorPrompt;
	TextLabel* 		connectingPrompt;
	TextLabel* 		connectedPrompt;
	TextLabel*		macAddress;

	GameObject* 	connectingRoot;
	GameObject* 	passwordRoot;

	SpriteObject* 	transmitting;
	SpriteObject* 	connectedStrength1;
	SpriteObject* 	connectedStrength2;
	SpriteObject* 	connectedStrength3;
	SpriteObject* 	connectedStrength4;
	TextLabel*  	passwordField;
	KeyboardObject* keyboard;

	ButtonObject* 	scanButton;
	ButtonObject* 	skipButton;
	ButtonObject* 	connectButton;
	ButtonObject*	backButton;

	SpriteObject*	systemIcon;
	TextLabel*		firmwareStatusLabel;

	bool firstScan;
};

}
#endif

#include "CameraStep.h"
#include "OutOfBoxScene.h"

namespace Glasgow
{

CameraStep::CameraStep(GameObject* root, OutOfBoxScene* scene) : super(root, scene)
{
	attempts = 0;
	cameraMPI  = null;
	cameraFeed = null;
	errorPresent = false;
	startCameraTimer = 0;

	leftGrouping  	= find("leftgrouping");
	rightGrouping 	= find("rightgrouping");
	cameraErrorRoot = find("cameraError");
	cameraErrorRoot->setVisible(false);
}

CameraStep::~CameraStep()
{
	if( cameraMPI != null )
		delete cameraMPI;
	printf("CameraStep::~CameraStep()\n");
}

/// <summary>
/// Show error
/// </summary>
void CameraStep::showError()
{
	cameraErrorRoot->findLabel("description")->fadeIn(0.2f);
	cameraErrorRoot->findLabel("header")->fadeIn(0.45f);
	cameraErrorRoot->findLabel("icon")->bubbleIn(0.5f);
	findLabel("cameraWorking")->fadeOut(0);
	leftGrouping->bubbleOut(0);
	rightGrouping->bubbleOut(0);
	errorPresent = true;
	cameraErrorRoot->setVisible(true);
	startCameraTimer = 0;
	outOfBox->enableNextStep(1.0f);
}

/// <summary>
/// Hide error
/// </summary>
void CameraStep::hideError()
{
	if( errorPresent)
	{
		cameraErrorRoot->findLabel("header")->fadeOut(0.2f);
		cameraErrorRoot->findLabel("description")->fadeOut(0.45f);
		cameraErrorRoot->findLabel("icon")->bubbleOut(0.5f);

		findLabel("cameraWorking")->fadeIn(0.5f);
		leftGrouping->bubbleIn( 0.6f );
		rightGrouping->bubbleIn( 0.6f );

		errorPresent = false;
	}
}

/// <summary>
/// If we want to be told about camera connection status
/// </summary>
void CameraStep::handleCameraDisconnected()
{
	printf("CameraStep::handleCameraDisconnected()\n");
	removeCameraFeed();

	if ( !errorPresent )
	{
		showError();
	}
}

/// <summary>
/// If we want to be told about camera connection status
/// </summary>
void CameraStep::handleCameraConnected()
{
	printf("CameraStep::handleCameraConnected()\n");
	startCameraTimer = 2;
}

/// <summary>
/// Remove camera feed object
/// </summary>
void CameraStep::removeCameraFeed()
{
	if( cameraFeed != null )
	{
		cameraFeed->removeFromParent();
		cameraFeed = null;
	}
}

/// <summary>
/// If we want to be told about camera connection status
/// </summary>
void CameraStep::startCameraFeed()
{
	createMPIInstance();

	//create a new camera feed object
	printf("Testing cameraMPI->IsCameraPresent\n");
	if( cameraMPI->IsCameraPresent() )
	{
		printf("Camera is present\n");

		if( errorPresent )
		{
			hideError();
		}

		removeCameraFeed();

		cameraFeed = new CameraFeedObject();
	    cameraFeed->setPosition( Vec3f( 0, -94, 225 ) );
	    cameraFeed->setCullingLayer( outOfBox->getCamera(0)->getCullingMask() );
	    cameraFeed->setScale( Vec3f::One* 0.69f);
	    cameraFeed->name = "cameraFeedObject";
	    rightGrouping->addChild(cameraFeed);
	    cameraFeed->release();

	    if ( cameraFeed->startFeed() )
	    {
	    	printf("CAMERA FEED STARTED\n");
	    	if(rightGrouping->getPosition() == Vec3f::Zero)
	    		outOfBox->enableNextStep(0.5f);
	    }
	    else
	    {
	    	printf("CAMERA FEED FAILED\n");
	    	startCameraTimer = 3;
	    }
	}
	else
	{
		delete cameraMPI;
		cameraMPI = null;
		printf("Camera is not present!\n");
		showError();
	}
}

/// <summary>
/// Due to the underlying camera module cleanup, it is necessary than when
/// the camera is hot plugged, we create a new interface to the camera MPI
/// </summary>
void CameraStep::createMPIInstance()
{
	if( cameraMPI != null )
		delete cameraMPI;

	cameraMPI = new CCameraMPI();
	attempts++;
	printf("Attempt count %d\n", attempts);
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void CameraStep::cameraArrived()
{
	printf("CameraStep::cameraArrived!\n");
	createMPIInstance();

	//create a new camera feed object
	printf("Testing for camera's pressence!\n");
	if( cameraMPI->IsCameraPresent() )
	{
		cameraErrorRoot->setVisible(false);
		outOfBox->enableHelpButton();
		outOfBox->enableNextStep(6.0f);

		rightGrouping->setPosition( rightGrouping->getPosition() + Vec3f::Right*1200 );
		leftGrouping->setPosition( Vec3f::Right*1280);
		leftGrouping->waitFor(1.5f);
		leftGrouping->moveTo( Vec3f(445, 0, 0), 1.0f );
		leftGrouping->waitFor(1.5f);
		leftGrouping->moveTo(Vec3f::Zero, 1.0f);
		rightGrouping->waitFor(4.0f);
		rightGrouping->moveTo(Vec3f::Zero, 1.0f);
		startCameraTimer = 2.5f;
	}
	else
	{
		delete cameraMPI;
		cameraMPI = null;
		find("cameraWorking")->setVisible(false);
		cameraErrorRoot->setVisible(true);
		rightGrouping->setVisible(false);
		leftGrouping->setVisible(false);
		errorPresent = true;
		outOfBox->enableNextStep(1.0f);
	}
}

/// <summary>
/// Update
/// </summary>
void CameraStep::update(float dt)
{
	if( startCameraTimer > 0  )
	{
		startCameraTimer -= dt;
		if( startCameraTimer <= 0 )
		{
			startCameraFeed();
		}
	}

	super::update(dt);
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void CameraStep::cameraLeaving()
{


}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* CameraStep::getArrivalSelection()
{
	outOfBox->getWorldRoot()->findButton("nextButton")->setState(ButtonStateActive);
	return outOfBox->getWorldRoot()->findButton("nextButton");
}


} /* namespace Glasgow */

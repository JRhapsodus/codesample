#include "ProfileCreationStep.h"
#include "OutOfBoxScene.h"

namespace Glasgow
{

ProfileCreationStep::ProfileCreationStep(GameObject* objectRoot, OutOfBoxScene* scene) : super(objectRoot, scene)
{
	currentStep				= CreationStepStart;
	birthDayRoot			= find("birthDayRoot");
	birthMonthRoot			= find("birthMonthRoot");
	birthYearRoot			= find("birthYearRoot");
	genderRoot				= find("genderRoot");
	gradeLevelRoot			= find("gradeLevelRoot");
	nameRoot				= find("nameRoot");
	confirmRoot				= find("confirmRoot");
	usersRoot				= find("usersRoot");

	arrivalPrompt			= findLabel("arrivalPrompt");
	createdPrompt			= findLabel("createdPrompt");
	headerPrompt			= findLabel("header");
	nameErrorPrompt			= nameRoot->findLabel("errorPrompt");

	createProfileButton		= findButton("createProfileButton");

	yearNumPad				= (GateLockObject*)birthYearRoot->find("numPad");
	keyboard 				= new KeyboardObject(scene, false);
	nameDoneButton			= nameRoot->findButton("done");
	keyboard->setMaxCharacters(Constants::System::userNameMaximumCharLimit);
	keyboard->setPosition( Vec3f::Down*75);
	keyboard->setLeadingCaps(true);
	yearField				= birthYearRoot->find("birthField")->findLabel("codeText");
	nameFieldContainer		= nameRoot->find("nameField");
	nameField				= nameFieldContainer->findLabel("codeText");
	nameRoot->addChild(keyboard);
	keyboard->release();
	changing = false;
	nameField->setBatched(false);
	yearField->setBatched(false);
	calender = new Calender();

	createProfileButtonOriginalPosition = createProfileButton->getPosition();

	chosenDay = "01";
}

ProfileCreationStep::~ProfileCreationStep()
{
	printf("ProfileCreationStep::~ProfileCreationStep()\n");
	calender->release();
}


/// <summary>
/// Animates to the next step and returns the new default selection
/// </summary>
GameObject* ProfileCreationStep::gotoNextSubStep()
{
	float lockOutTime = 1.35f;
	float waitTime = 0.75f;
	if( currentStep == CreationStepStart )
	{
		printf("Start -> Name\n");
		outOfBox->lockOutInput(lockOutTime);

		if( confirmRoot->isVisibleSelf() )
		{
			confirmRoot->findButton("change")->bubbleOut();
			confirmRoot->findButton("confirm")->bubbleOut();

			confirmRoot->findLabel("confirmPrompt")->recursiveFadeOut();
			confirmRoot->findLabel("nameLabel")->recursiveFadeOut();
			confirmRoot->findLabel("genderLabel")->recursiveFadeOut();
			confirmRoot->findLabel("gradeLabel")->recursiveFadeOut();
			confirmRoot->findLabel("birthMonthLabel")->recursiveFadeOut();
			confirmRoot->findLabel("birthDayLabel")->recursiveFadeOut();
			confirmRoot->findLabel("birthYearLabel")->recursiveFadeOut();
			confirmRoot->findLabel("label1")->recursiveFadeOut();
			confirmRoot->findLabel("label2")->recursiveFadeOut();
			confirmRoot->findLabel("label3")->recursiveFadeOut();
			confirmRoot->findLabel("label4")->recursiveFadeOut();
			confirmRoot->findLabel("label5")->recursiveFadeOut();
			confirmRoot->findLabel("label6")->recursiveFadeOut();
		}

		//**********************************************************************
		// Animate Out start
		//**********************************************************************
		((ButtonObject*)createProfileButton)->setState(ButtonStateNormal);
		createProfileButton->bubbleOut();
		arrivalPrompt->recursiveFadeOut();
		createdPrompt->recursiveFadeOut();
		fadeOutUsers(0);

		outOfBox->hideNextStep();
		//.25s passed

		//**********************************************************************
		// Animate In name
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_FIRSTNAME"));
		headerPrompt->recursiveFadeIn();

		nameRoot->waitFor(waitTime);
		nameRoot->makeVisible(true);
		keyboard->bubbleIn(waitTime);
		nameFieldContainer->bubbleIn(waitTime);

		String usernameText = keyboard->getText();
		if(validNameEntry(usernameText) == false)
		{ nameDoneButton->setVisible(false);}
		else
		{ nameDoneButton->bubbleIn(waitTime);}

		currentStep = CreationStepName;

		if( !changing )
		{
			nameField->setText("");
			keyboard->reset();
			keyboard->setLeadingCaps(true);
			return keyboard->getDefaultHighLight();
		}
		else
		{
			return nameRoot->findButton("done");
		}
	}
	else if ( currentStep == CreationStepName )
	{
		printf("Name -> Gender\n");
		outOfBox->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Name
		//**********************************************************************
		keyboard->bubbleOut();
		nameFieldContainer->bubbleOut();
		nameDoneButton->bubbleOut();
		nameErrorPrompt->cancelActions();
		nameErrorPrompt->recursiveFadeOut();

		//**********************************************************************
		// Animate in Gender
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_GENDER"));
		headerPrompt->recursiveFadeIn();

		genderRoot->waitFor(waitTime);
		genderRoot->makeVisible(true);
		//TODO: localization
		genderRoot->findLabel("genderPrompt")->setText( String("Choose Boy or Girl for ") + chosenName );
		genderRoot->findLabel("genderPrompt")->recursiveFadeIn(waitTime);

		for ( int i = 0; i<genderRoot->numChildren(); i++ )
		{
			GameObject* child = genderRoot->childAtIndex(i);
			if( child != null && child->typeOf( ButtonObject::type()) )
			{	child->bubbleIn(waitTime);}
		}

		currentStep = CreationStepGender;

		if( !changing )
		{ return genderRoot->find("boy"); }
		else
		{ return genderRoot->find(chosenGender); }
	}
	else if ( currentStep == CreationStepGender )
	{
		printf("Gender -> Grade\n");
		outOfBox->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out gender
		//**********************************************************************
		genderRoot->findLabel("genderPrompt")->recursiveFadeOut();
		for ( int i = 0; i<genderRoot->numChildren(); i++ )
		{
			GameObject* child = genderRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				if(child->name != chosenGender)
					child->cancelActionsAsComplete();
				child->bubbleOut();
			}
		}

		//**********************************************************************
		// Animate in GradeLevel
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_GRADE"));
		headerPrompt->recursiveFadeIn();

		setLocalizedGradeTextInChildren(gradeLevelRoot);
		gradeLevelRoot->waitFor(waitTime);
		gradeLevelRoot->makeVisible(true);
		gradeLevelRoot->findLabel("gradeLevelPrompt")->recursiveFadeIn(waitTime);
		//Todo:Localization
		gradeLevelRoot->findLabel("gradeLevelPrompt")->setText( String("Select current grade/level for ") + chosenName );

		for ( int i = 0; i<gradeLevelRoot->numChildren(); i++ )
		{
			GameObject* child = gradeLevelRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{ child->bubbleIn(waitTime); }
		}

		currentStep = CreationStepGrade;

		if( !changing )
		{ return gradeLevelRoot->find("gradePreschool"); }
		else
		{ return gradeLevelRoot->find(chosenGrade); }
	}
	else if (currentStep == CreationStepGrade)
	{
		printf("Grade -> Month\n");
		outOfBox->lockOutInput(lockOutTime);
		//**********************************************************************
		// Animate Out Grade
		//**********************************************************************
		gradeLevelRoot->findLabel("gradeLevelPrompt")->recursiveFadeOut();

		for ( int i = 0; i<gradeLevelRoot->numChildren(); i++ )
		{
			GameObject* child = gradeLevelRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				if(child->name != chosenGrade)
					child->cancelActionsAsComplete();
				child->bubbleOut();
			}
		}

		//**********************************************************************
		// Animate in Month
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_BIRTHMONTH"));
		headerPrompt->recursiveFadeIn();

		birthMonthRoot->waitFor(waitTime);
		birthMonthRoot->makeVisible(true);
		birthMonthRoot->findLabel("birthMonthPrompt")->recursiveFadeIn(waitTime);
		//Todo:Localization
		birthMonthRoot->findLabel("birthMonthPrompt")->setText( String("Select birth month for ") + chosenName );

		for ( int i = 0; i<birthMonthRoot->numChildren(); i++ )
		{
			GameObject* child = birthMonthRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{ child->bubbleIn(waitTime); }

		}

		currentStep = CreationStepMonth;

		if( !changing )
		{ return birthMonthRoot->find("january");}
		else
		{ return birthMonthRoot->find(chosenMonth); }
	}
	else if ( currentStep == CreationStepMonth )
	{
		printf("Month -> Year\n");
		outOfBox->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Month
		//**********************************************************************
		birthMonthRoot->findLabel("birthMonthPrompt")->recursiveFadeOut();

		for ( int i = 0; i<birthMonthRoot->numChildren(); i++ )
		{
			GameObject* child = birthMonthRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				if(child->name != chosenMonth)
					child->cancelActionsAsComplete();
				child->bubbleOut();
			}
		}

		//**********************************************************************
		// Animate in Year
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_BIRTHYEAR"));
		headerPrompt->recursiveFadeIn();

		yearField->setText("");
		chosenYear = "";
		birthYearRoot->waitFor(waitTime);
		birthYearRoot->makeVisible(true);
		birthYearRoot->findLabel("birthYearPrompt")->recursiveFadeIn(waitTime);
		//Todo:Localization
		birthYearRoot->findLabel("birthYearPrompt")->setText( String("Enter birth year for ") + chosenName );

		yearNumPad->bubbleIn(waitTime);
		birthYearRoot->find("birthField")->bubbleIn(waitTime);

		currentStep = CreationStepYear;

		return yearNumPad->find("1");
	}
	else if ( currentStep == CreationStepYear )
	{
		printf("Year -> Day\n");
		outOfBox->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Year
		//**********************************************************************
		birthYearRoot->findLabel("birthYearPrompt")->recursiveFadeOut();
		yearNumPad->bubbleOut();
		birthYearRoot->find("birthField")->bubbleOut();

		//**********************************************************************
		// Animate in Day
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER_BIRTHDAY"));
		headerPrompt->recursiveFadeIn();

		birthDayRoot->waitFor(waitTime);
		birthDayRoot->makeVisible(true);
		birthDayRoot->findLabel("birthDayPrompt")->recursiveFadeIn(waitTime);
		//Todo:localization
		birthDayRoot->findLabel("birthDayPrompt")->setText( String("Select birth day for ") + chosenName );

		int daysInMonth = MIN(birthDayRoot->getChildren()->getSize(),calender->daysInMonthYear(chosenMonth,chosenYear));
		for( int i = 0; i< 31; i++ )
		{
			ButtonObject* button = birthDayRoot->findButton(calender->dayStrForInt(i+1));
			if(i < daysInMonth)
			{ button->bubbleIn(waitTime);}
			else
			{ button->setVisible(false); }
		}

		currentStep = CreationStepDay;
		if( !changing )
		{ return birthDayRoot->find("01");}
		else
		{
			GameObject* go = birthDayRoot->find(chosenDay);
			if(go->isVisibleHierarchy())
			{return go;}
			return birthDayRoot->find("01");
		}
	}
	else if ( currentStep == CreationStepDay )
	{
		printf("Day -> Confirm\n");
		outOfBox->lockOutInput(lockOutTime);

		//**********************************************************************
		// Animate Out Day
		//**********************************************************************
		 birthDayRoot->findLabel("birthDayPrompt")->recursiveFadeOut();

		for ( int i = 0; i<birthDayRoot->numChildren(); i++ )
		{
			GameObject* child = birthDayRoot->childAtIndex(i);
			if( child->typeOf( ButtonObject::type() ) )
			{
				if(child->name != chosenDay)
					child->cancelActionsAsComplete();
				child->bubbleOut();
			}
		}

		//**********************************************************************
		// Animate in Confirm
		//**********************************************************************
		headerPrompt->recursiveFadeOut();
		headerPrompt->waitFor(0.25f);
		headerPrompt->changeTo(StringsManager::getInstance()->getLocalizedString("STEPS_CREATEPROFILESTEP_HEADER"));
		headerPrompt->recursiveFadeIn();

		confirmRoot->waitFor(waitTime);
		confirmRoot->makeVisible(true);
		confirmRoot->findLabel("nameLabel")->setText(chosenName);
		String genderStr = chosenGender == "boy" ? StringsManager::getInstance()->getLocalizedString("PROFILE_GENDER_MALE") : StringsManager::getInstance()->getLocalizedString("PROFILE_GENDER_FEMALE");
		confirmRoot->findLabel("genderLabel")->setText(genderStr);
		confirmRoot->findLabel("gradeLabel")->setText(getLocalizedStringFromGradeButtonName(chosenGrade));
		confirmRoot->findLabel("birthMonthLabel")->setText(calender->formattedMonthStr(chosenMonth));
		confirmRoot->findLabel("birthDayLabel")->setText(calender->formattedDayStr(chosenDay));
		confirmRoot->findLabel("birthYearLabel")->setText(chosenYear);

		confirmRoot->findLabel("nameLabel")->recursiveFadeIn(waitTime);
		confirmRoot->findLabel("genderLabel")->recursiveFadeIn(waitTime + 0.05f);
		confirmRoot->findLabel("gradeLabel")->recursiveFadeIn(waitTime + 0.1f);
		confirmRoot->findLabel("birthMonthLabel")->recursiveFadeIn(waitTime + 0.15f);
		confirmRoot->findLabel("birthDayLabel")->recursiveFadeIn(waitTime + 0.2f);
		confirmRoot->findLabel("birthYearLabel")->recursiveFadeIn(waitTime + 0.25f);

		confirmRoot->findLabel("confirmPrompt")->recursiveFadeIn(waitTime);

		confirmRoot->findLabel("label1")->recursiveFadeIn(waitTime);
		confirmRoot->findLabel("label2")->recursiveFadeIn(waitTime + 0.05f);
		confirmRoot->findLabel("label3")->recursiveFadeIn(waitTime + 0.1f);
		confirmRoot->findLabel("label4")->recursiveFadeIn(waitTime + 0.15f);
		confirmRoot->findLabel("label5")->recursiveFadeIn(waitTime + 0.2f);
		confirmRoot->findLabel("label6")->recursiveFadeIn(waitTime + 0.25f);

		confirmRoot->findButton("confirm")->bubbleIn(waitTime + 0.25f);
		confirmRoot->findButton("change")->bubbleIn(waitTime + 0.25f);

		currentStep = CreationStepConfirm;

		return confirmRoot->findButton("confirm");
	}
	else if ( currentStep == CreationStepConfirm )
	{
		printf("Confirm -> Start\n");
		outOfBox->lockOutInput(lockOutTime);

		confirmRoot->findButton("change")->bubbleOut();
		confirmRoot->findButton("confirm")->bubbleOut();

		confirmRoot->findLabel("confirmPrompt")->recursiveFadeOut();
		confirmRoot->findLabel("nameLabel")->recursiveFadeOut();
		confirmRoot->findLabel("genderLabel")->recursiveFadeOut();
		confirmRoot->findLabel("gradeLabel")->recursiveFadeOut();
		confirmRoot->findLabel("birthMonthLabel")->recursiveFadeOut();
		confirmRoot->findLabel("birthDayLabel")->recursiveFadeOut();
		confirmRoot->findLabel("birthYearLabel")->recursiveFadeOut();
		confirmRoot->findLabel("label1")->recursiveFadeOut();
		confirmRoot->findLabel("label2")->recursiveFadeOut();
		confirmRoot->findLabel("label3")->recursiveFadeOut();
		confirmRoot->findLabel("label4")->recursiveFadeOut();
		confirmRoot->findLabel("label5")->recursiveFadeOut();
		confirmRoot->findLabel("label6")->recursiveFadeOut();

		//**********************************************************************
		// Animate In Entry Point
		//**********************************************************************

		keyboard->reset();
		keyboard->setLeadingCaps(true);

		populateProfilePrompts();
		fadeInUsers(waitTime);
		createdPrompt->recursiveFadeIn(waitTime);

		int numUsers = UsersManager::getInstance()->getUsers()->getSize();
		if(numUsers == usersRoot->getChildren()->getSize())
		{
			createProfileButton->setVisible(false);
		}
		else
		{
			createProfileButton->setPosition(createProfileButtonOriginalPosition + Vec3f::Down * Constants::UI::addProfileButtonYOffset * numUsers);
			createProfileButton->bubbleIn(waitTime);
		}

		currentStep = CreationStepStart;

		outOfBox->enableNextStep(waitTime + 0.25f);
		return outOfBox->getWorldRoot()->findButton("nextButton");
	}
	return null;
}


/// <summary>
/// Takes the new user information and adds the Glasgow based profile
/// </summary>
void ProfileCreationStep::addNewUser()
{
	User* newUser = UsersManager::getInstance()->newUser(chosenName);
	String gender = chosenGender == "boy" ? Constants::System::genderMaleKey : Constants::System::genderFemaleKey;
	newUser->setGender(gender);
	newUser->setBirthDate(chosenDay,chosenMonth,chosenYear);
	newUser->setBirthDay(calender->getDayIntRepresentation(chosenDay));
	newUser->setBirthMonth(calender->getMonthIntRepresentation(chosenMonth));
	newUser->setBirthYear(chosenYear.toInt());
	GradeInfo* info = GradeInfo::getGradeInfo(chosenGrade);
	newUser->setGrade(info->leapGradeEnum);
	newUser->save();

	// The newly created user should be set as the last user so that it's selected when they enter the child scene
	printf("Setting last logged in user : %s\n", newUser->getFirstName().str());
	UsersManager::getInstance()->setCurrentUser(newUser);
}

/// <summary>
/// saves user settings
/// </summary>
void ProfileCreationStep::saveSettings()
{
}

/// <summary>
/// populates the users on arrival
/// </summary>
void ProfileCreationStep::populateProfilePrompts()
{
	ObjectArray* users = UsersManager::getInstance()->getUsers();
	ObjectArray* children = usersRoot->getChildren();
	for(int i = 0; i < children->getSize(); ++i)
	{
		if(i < users->getSize())
		{
			User* user = (User*)users->elementAt(i);
			((TextLabel*)children->elementAt(i))->setText(user->getDisplayName());
		}
		else
		{
			((TextLabel*)children->elementAt(i))->setText("");
		}
	}
}

/// <summary>
/// returns the localized string for a given grad button name
/// </summary>
String ProfileCreationStep::getLocalizedStringFromGradeButtonName(String buttonName)
{
	GradeInfo* gradeInfo = GradeInfo::getGradeInfo(buttonName);
	return StringsManager::getInstance()->getLocalizedString(gradeInfo->localizationKey);
}

void ProfileCreationStep::setLocalizedGradeTextInChildren(GameObject* root)
{
	for( int i = 0; i<root->numChildren(); i++ )
	{
		GameObject* child = root->childAtIndex(i);

		if(child->typeOf(ButtonObject::type()))
		{
			TextLabel *label = child->findLabel("text");
			label->setText(getLocalizedStringFromGradeButtonName(child->name));
		}
	}
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ProfileCreationStep::handlePressAction(GameObject*& objectHighlight)
{
	if ( objectHighlight != null )
	{
		if ( currentStep == CreationStepStart && objectHighlight == createProfileButton)
		{
			outOfBox->setCurrentHighlight(gotoNextSubStep());
			return true;
		}
		else if( currentStep == CreationStepConfirm )
		{
			if( objectHighlight->name == "confirm" )
			{
				changing = false;
				addNewUser();
				outOfBox->setCurrentHighlight(gotoNextSubStep());
				return true;
			}
			else if( objectHighlight->name == "change" )
			{
				//trick the step into going back through
				changing = true;
				currentStep = CreationStepStart;
				outOfBox->setCurrentHighlight(gotoNextSubStep());
				return true;
			}
		}
		else if ( currentStep == CreationStepName )
		{
			if( objectHighlight == nameDoneButton )
			{
				String enteredName = keyboard->getText();
				printf("Entered Name == %s\n", enteredName.str());
				if(UsersManager::getInstance()->isUserNameUnique(enteredName) == false)
				{
					printf("User_name == %s ,is already in use.\n", enteredName.str());
					nameErrorPrompt->cancelActions();
					nameErrorPrompt->recursiveFadeIn();
					nameErrorPrompt->recursiveFadeOut(2);
				}
				else
				{
					chosenName = enteredName;
					outOfBox->setCurrentHighlight(gotoNextSubStep());
				}
				return true;
			}
			else if( keyboard->onButtonPressAction() )
			{
				//block out input
				outOfBox->lockOutInput(0.2f);

				String usernameText = keyboard->getText();
				if (objectHighlight->name == "Backspace")
				{
					outOfBox->playSound("Parent_Keystroke_Backspace");
					nameField->removeLastCharacter();
				}
				else
				{
					outOfBox->playSound("Parent_Keystroke");
					nameField->setText(usernameText);
					nameField->fadeLastCharacterIn();
				}

				if(usernameText.equals(" "))
					keyboard->setText("");

				if(validNameEntry(usernameText) == false)
				{
					nameDoneButton->setVisible(false);
				}
				else if(!nameDoneButton->isVisibleSelf())
				{
					nameDoneButton->bubbleIn();
				}

				outOfBox->setCurrentHighlight(keyboard->getDefaultHighLight());
			}
		}
		else if ( currentStep == CreationStepYear )
		{
			outOfBox->lockOutInput(0.2f);

			String pinValue = objectHighlight->name;
			if( pinValue == "backspacebutton" )
			{
				if( chosenYear.length() > 0 )
				{
					chosenYear = chosenYear.substring(0, chosenYear.length()-1);
					outOfBox->playSound("Parent_Keystroke_Backspace");
					yearField->removeLastCharacter();
				}
			}
			else
			{
				if( chosenYear.length() < 4 )
				{
					chosenYear += pinValue;
					yearField->setText(chosenYear);
					yearField->fadeLastCharacterIn();
					outOfBox->playSound("Parent_Keystroke");

					if( chosenYear.length() == 4 )
					{
						outOfBox->setCurrentHighlight(gotoNextSubStep());
						return true;
					}
				}
			}
		}
		else if ( objectHighlight->typeOf(ButtonObject::type()) )
		{
			if ( currentStep == CreationStepGender )
			{
				chosenGender = objectHighlight->name;
				printf("Gender Chosen: %s\n", chosenGender.str());
			}
			else if ( currentStep == CreationStepMonth )
			{
				chosenMonth = objectHighlight->name;
				printf("Month Chosen: %s\n", chosenMonth.str());
			}
			else if ( currentStep == CreationStepGrade )
			{
				chosenGrade = objectHighlight->name;
				printf("Grade Chosen: %s\n", chosenMonth.str());
			}
			else if (currentStep == CreationStepDay)
			{
				chosenDay = objectHighlight->name;
				printf("Day Chosen: %s\n", chosenDay.str());
			}

			outOfBox->setCurrentHighlight(gotoNextSubStep());
			return true;
		}
	}




	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ProfileCreationStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	if ( currentStep == CreationStepName && keyboard->onSelectionChange(objectLeft, objectEntered) )
	{
		if( objectEntered != nameDoneButton )
		{
			currentHighlight = keyboard->getDefaultHighLight();
		}

		return true;
	}

	return false;

}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ProfileCreationStep::cameraArrived()
{
	outOfBox->hideHelpButton();

	birthDayRoot->setVisible(false);
	birthMonthRoot->setVisible(false);
	birthYearRoot->setVisible(false);
	genderRoot->setVisible(false);
	gradeLevelRoot->setVisible(false);
	nameRoot->setVisible(false);
	nameFieldContainer->setVisible(false);
	createdPrompt->setVisible(false);
	arrivalPrompt->setVisible(false);
	confirmRoot->setVisible(false);
	nameErrorPrompt->setVisible(false);
	keyboard->reset();
	keyboard->setLeadingCaps(true);
	keyboard->setVisible(false);

	populateProfilePrompts();
	fadeInUsers(0.5f);

	int numUsers = UsersManager::getInstance()->getUsers()->getSize();
	if(numUsers == usersRoot->getChildren()->getSize())
	{
		createdPrompt->recursiveFadeIn(1);
		createProfileButton->setVisible(false);
		outOfBox->enableNextStep(1);
	}
	else
	{
		arrivalPrompt->recursiveFadeIn(0.5f);
		createProfileButton->setPosition(createProfileButtonOriginalPosition + Vec3f::Down * Constants::UI::addProfileButtonYOffset * numUsers);
		createProfileButton->bubbleIn(0.5f);
		if(numUsers > 0)
			outOfBox->enableNextStep(1);
	}
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ProfileCreationStep::getArrivalSelection()
{
	if( currentStep == CreationStepStart )
	{
		if(UsersManager::getInstance()->getUsers()->getSize() > 0)
		{
			return outOfBox->getWorldRoot()->findButton("nextButton");
		}
		else
		{
			return createProfileButton;
		}
	}
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ProfileCreationStep::addHotSpots( ObjectArray* trackingList )
{
	trackingList->addElement(createProfileButton);

	if ( currentStep == CreationStepMonth )
	{
		printf("Adding buttons for month\n");

		for( int i = 0; i<12; i++ )
		{
			ButtonObject* button = birthMonthRoot->findButton(calender->monthStrForInt(i+1));
			if( button != null )
			{ trackingList->addElement( button ); }
		}
	}
	else if ( currentStep == CreationStepDay )
		{
			printf("Adding buttons for day\n");


			for( int i = 0; i<31; i++ )
			{
				ButtonObject* button = birthDayRoot->findButton(calender->dayStrForInt(i+1));
				if( button != null )
				{ trackingList->addElement( button ); }
			}
		}
	else if ( currentStep == CreationStepConfirm )
	{
		trackingList->addElement(confirmRoot->findButton("confirm"));
		trackingList->addElement(confirmRoot->findButton("change"));
	}
	else if ( currentStep == CreationStepName )
	{
		keyboard->setupHotSpots();
		trackingList->addElement(nameDoneButton);
	}
	else if ( currentStep == CreationStepYear )
	{ yearNumPad->addHotSpots(trackingList); }
	else if ( currentStep == CreationStepGender )
	{
		trackingList->addElement( genderRoot->findButton("girl") );
		trackingList->addElement( genderRoot->findButton("boy") );
	}
	else if ( currentStep == CreationStepGrade )
	{
		printf("Adding Grades buttons\n");

		for(int i = 0; i < gradeLevelRoot->getChildren()->getSize(); ++i)
		{
			BaseObject* bo = gradeLevelRoot->childAtIndex(i);
			if(bo->typeOf(ButtonObject::type()))
			{
				trackingList->addElement((ButtonObject*)bo);
			}
		}
	}
}

/// <summary>
/// Custom update
/// </summary>
void ProfileCreationStep::update(float dt)
{
	super::update(dt);
}

void ProfileCreationStep::fadeOutUsers(float delay)
{
	usersRoot->recursiveFadeOut(delay);
	usersRoot->waitFor(delay + 0.25f);
	usersRoot->makeVisible(false);
}

void ProfileCreationStep::fadeInUsers(float delay)
{
	usersRoot->makeVisible(true);
	usersRoot->recursiveFadeIn(delay);
}

/// <summary>
/// validates the user name field
/// </summary>
bool ProfileCreationStep::validNameEntry(String str)
{
	str.replace(" ", "");
	if(!str.isEmpty())
		return true;

	return false;
}

}

#ifndef CAMERASTEP_H_
#define CAMERASTEP_H_

#include "OutOfBoxStep.h"
#include "Glasgow/Renderables/CameraFeedObject.h"

namespace Glasgow
{

class CameraStep : public OutOfBoxStep
{
	CLASSEXTENDS(CameraStep, OutOfBoxStep);
public:
	CameraStep(GameObject * replacementRoot, OutOfBoxScene* scene);
	virtual ~CameraStep();

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// If we want to be told about camera connection status
	/// </summary>
	virtual void handleCameraDisconnected();

	/// <summary>
	/// If we want to be told about camera connection status
	/// </summary>
	virtual void handleCameraConnected();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Remove camera feed object
	/// </summary>
	void removeCameraFeed();

	/// <summary>
	/// Shows the error screen for Check camera
	/// </summary>
	void showError();

	/// <summary>
	/// Hides the error screen for Check camera
	/// </summary>
	void hideError();

	/// <summary>
	/// Due to the underlying camera module cleanup, it is necessary than when
	/// the camera is hot plugged, we create a new interface to the camera MPI
	/// </summary>
	void createMPIInstance();

	/// <summary>
	/// Update
	/// </summary>
	virtual void update(float dt);
private:

	void startCameraFeed();
	bool errorPresent;
	float 	startCameraTimer;
	CCameraMPI*		cameraMPI;
	int attempts;
	CameraFeedObject* cameraFeed;
	GameObject* leftGrouping;
	GameObject* rightGrouping;
	GameObject* cameraErrorRoot;
};

}
#endif

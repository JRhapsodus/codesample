#include "DownloadStep.h"
#include "OutOfBoxScene.h"
#include <sys/reboot.h>
#include <signal.h>

namespace Glasgow
{
float DownloadStep::dlFinishedTimer = 20;
float DownloadStep::dlProgressTest = 0;
float DownloadStep::oneSecondTimer = 1;

DownloadStep::DownloadStep(GameObject* root, OutOfBoxScene* scene) : super(root, scene)
{
	retestTimer = 0;
	systemUpdatedRoot 	 = find("completeRoot");
	systemUpdatingRoot	 = find("updatingRoot");
	updateButton		 = systemUpdatingRoot->findButton("updateButton");
	countdownLabel 		 = systemUpdatingRoot->findLabel("updatePromptCountdown");

	rebootTimer = 0;
	killTimer = 0;
	errorShown				= false;
	countdownLabel->setVisible(false);
	readyTimer				= 0;
	createObjects();
}

DownloadStep::~DownloadStep()
{
	printf("DownloadStep::~DownloadStep()\n");
	SAFE_RELEASE(netError);
	SAFE_RELEASE(progressBar);
}

/// <summary>
/// Create netError and progressBar
/// </summary>
void DownloadStep::createObjects()
{
	netError = (NetErrorObject*)Scene::instantiate(NetErrorObject::type()); //ref1
	find("netErrorContainer")->addChild(netError);//ref 2 ::~ -> ref 1
	netError->setVisible(false);
	netError->setPosition(Vec3f::Zero);

	progressBar = (FirmwareProgressBar*)Scene::instantiate(FirmwareProgressBar::type());

	find("updatingRoot")->find("progressBarContainer")->addChild(progressBar);
	progressBar->setVisible(false);
	progressBar->setPosition(Vec3f::Zero);
	progressBar->setCullingLayerRecursive(outOfBox->getCamera(0)->getCullingMask());
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void DownloadStep::cameraArrived()
{
	printf("DownloadStep::cameraArrived!\n");
	outOfBox->enableHelpButton();
	packageManager = PackageManager::Instance();

	//post firmware update
	if( AssetLibrary::fileExists(Constants::Paths::flagFirmwareInstalled) )
	{
		printf("%s exists!\n", Constants::Paths::flagFirmwareInstalled.str());
		AssetLibrary::removeFile(Constants::Paths::flagFirmwareInstalled);
		String firmwareVersion = SystemSettings::getFirmwareVersion();

		outOfBox->enableNextStep(1.0f);
		systemUpdatedRoot->setVisible(true);
		systemUpdatingRoot->setVisible(false);

		progressBar->setVisible(false);
		// TODO: Localize strings
		systemUpdatedRoot->findLabel("firmwareVersion")->setText(SystemSettings::getFirmwareVersion());
	}
	else
	{
		printf("[%s] does not exist!\n", Constants::Paths::flagFirmwareInstalled.str());
		checkFirmwareUpdate();
	}
}

/// <summary>
/// Over ridden for timer based polling in ContentManager
/// </summary>
void DownloadStep::update(float dt)
{
	if( killTimer > 0 )
	{
		killTimer -= dt;
		if( killTimer <= 0 )
		{
			printf("Rebooting due to firmware update !\n");
			sync();
			::kill(1, 15);
		}
	}

	if( rebootTimer > 0 )
	{
		int secondsLeft = (int)rebootTimer;

		//TODO Localization
		if ( secondsLeft > 1 )
		{ countdownLabel->setText(String("Your LeapTV will automatically restart in ") + String(secondsLeft) + String(" seconds.")); }
		else
		// TODO: Localize strings
		{ countdownLabel->setText(String("Your LeapTV will automatically restart in 1 second.")); }

		rebootTimer -= dt;
		if ( rebootTimer <= 0 )
		{
			killTimer = 1;
			outOfBox->screenDarken->setVisible(true);
			outOfBox->screenDarken->setOpacity(0);
			outOfBox->screenDarken->fadeTo(1, 0.5f);
		}
	}

	//Enable to test the progress bar interface
	//testProgressBarUpdate(dt);
	firmwareTestUpdate(dt);
	super::update(dt);
}

/// <summary>
/// In case package manager discovers new downloads the progress bar must be 'reset' with the new data
/// </summary>
void DownloadStep::handlePendingChanged()
{
	if( progressBar->isVisibleHierarchy() )
	{
		FirmwareStatus status = ContentManager::getInstance()->getFirmwareStatus();
		if ( status == FirmwarePending )
		{
			progressBar->setDisplay(ContentManager::getInstance()->getDownloadsRemaining());
			readyTimer = 0;
		}
		else if ( status == FirmwareReady )
		{
			readyTimer = 2;
		}

	}
}

/// <summary>
/// Called before showing progress bar
/// </summary>
void DownloadStep::firmwareTestUpdate(float dt)
{
	if( readyTimer > 0 )
	{
		readyTimer-= dt;
		if( readyTimer< 0 )
		{
			printf("Asking package manager if it is ready\n");
			if( ContentManager::getInstance()->getFirmwareStatus() == FirmwareReady )
			{
				printf("Package manager says it is now ready\n");
				rebootTimer = 20;
				countdownLabel->fadeIn(0.25f);
				outOfBox->setCurrentHighlight(updateButton);
				updateButton->setState(ButtonStateActive);
				updateButton->cancelActions();
				updateButton->bubbleIn(0.5f);
			}
			else
			{
				printf("Firmware packages are downloaded but package manager says it is not ready!!??\n");
				readyTimer = 2;
			}
		}
	}
}

/// <summary>
/// Test function for simulating with the package manager interface
/// </summary>
void DownloadStep::testProgressBarUpdate(float dt)
{
	if( !errorShown )
	{
		if( dlFinishedTimer > 0 )
		{
			dlFinishedTimer -= dt;
			if( dlFinishedTimer <= 0 )
			{
				handleDonwloadFinished("MOO");
				handleDonwloadStarted("MOO2");
				dlFinishedTimer= 30;
			}
		}

		if( oneSecondTimer > 0 )
		{
			oneSecondTimer -= dt;
			if( oneSecondTimer <= 0 )
			{
				oneSecondTimer = 1;
				dlProgressTest = 1- (dlFinishedTimer/30);
				dlProgressTest = (dlProgressTest>1)?1:dlProgressTest;
				handleDownloadProgress("SOME PACKAGE", (int)(100*dlProgressTest));
			}
		}
	}
}

/// <summary>
/// Animation transition into a networking error
/// </summary>
void DownloadStep::showNetworkError( String header, String error )
{
	printf("DownloadStep::showNetworkError => [%s]", error.str());

	systemUpdatingRoot->find("icon")->cancelActions();
	progressBar->cancelActions();
	systemUpdatingRoot->findLabel("downloadPrompt")->cancelActions();
	systemUpdatingRoot->findLabel("updateHeader")->cancelActions();
	systemUpdatingRoot->find("updatePrompt")->cancelActions();

	systemUpdatingRoot->find("icon")->bubbleOut(0);
	progressBar->bubbleOut(0);
	systemUpdatingRoot->findLabel("downloadPrompt")->fadeOut(0);
	systemUpdatingRoot->findLabel("updateHeader")->fadeOut(0);
	netError->setVisible(true);
	netError->displayNetworkError(header, error);
	outOfBox->setupHotSpots();
	outOfBox->setCurrentHighlight(netError->reconnectButton);
	errorShown = true;
}

/// <summary>
/// Remove network error
/// </summary>
void DownloadStep::hideNetworkError()
{
	if (errorShown)
	{
		netError->animateOut();
		progressBar->bubbleIn(0.5f);
		systemUpdatingRoot->find("icon")->bubbleIn(0.5f);
		systemUpdatingRoot->findLabel("downloadPrompt")->fadeIn(0.5f);
		systemUpdatingRoot->findLabel("updateHeader")->fadeIn(0.5f);
		errorShown = false;
	}

	outOfBox->setupHotSpots();
}

/// <summary>
/// Network error occured, thrown by connman
/// </summary>
void DownloadStep::handleNetworkError(String networkName, String networkError)
{
	printf("DownloadStep::handleNetworkError ==> %s\n", networkError.str());

	//No need to care if network goes down when system is ready to update
	if( progressBar->isVisibleHierarchy() )
	{ showNetworkError("Check Connection", networkError); }

	outOfBox->setupHotSpots();
}

/// <summary>
/// Thrown by package-manager
/// </summary>
void DownloadStep::handleDownloadError()
{
	// what to do ?!
	//No need to care if network goes down when system is ready to update
	if( isVisibleHierarchy() )
	{
		DialogOptions options;
		options.title   = "Check Connection";
		options.prompt  = "Download could not complete, you can update your LeapTV later in Parent Settings.";

		if( !ConnectionManager::getInstance()->leapFrogServersResponded )
		{
			options.title 	= "Error";
			options.prompt  = "We are unable to connect to LeapFrog servers. Please check your Internet connection and try again later in Parent Settings.";
		}

		options.buttonA = DialogButtonOption("Update Later", "latersskaters");
		outOfBox->showModalDialog(DialogTypeDownloadError, options);
		return;
	}

	outOfBox->setupHotSpots();
	super::handleDownloadError();
}


/// <summary>
/// Network connection made
/// </summary>
void DownloadStep::handleNetworkConnected( String networkName )
{
	if(errorShown)
	{
		hideNetworkError();
	}

	calculateDownloadInfo();
}

/// <summary>
/// Download of the current package has changed [0,100]
/// </summary>
void DownloadStep::handleDownloadProgress( String package, int downloadProgress)
{
	progressBar->handleDownloadProgress(package, downloadProgress);
}

/// <summary>
/// Tests the status for the requirement of a firmware update and update
/// the required UI that may be needed
/// </summary>
void DownloadStep::checkFirmwareUpdate()
{
	printf("Checking to see if firmware update is ready, pending or none\n");

	if ( ContentManager::getInstance()->getFirmwareStatus() == FirmwareReady )
	{
		printf("Firmware is ready to install!\n");
		systemUpdatedRoot->setVisible(false);
		systemUpdatingRoot->setVisible(true);
		countdownLabel->fadeIn(0.25f);
		systemUpdatingRoot->find("downloadPrompt")->setVisible(false);
		systemUpdatingRoot->find("progressBarContainer")->setVisible(false);
		updateButton->bubbleIn(0.5f);
		rebootTimer = 20;
	}
	else if ( ContentManager::getInstance()->getFirmwareStatus() == FirmwarePending )
	{
		calculateDownloadInfo();
		progressBar->setVisible(true);
		systemUpdatingRoot->find("updatePrompt")->setVisible(false);
		systemUpdatedRoot->setVisible(false);
		updateButton->setVisible(false);
	}
	else
	{
		printf("No firmware is needed to update\n");
		outOfBox->enableNextStep(2.0f);
		progressBar->setVisible(false);
		updateButton->setVisible(false);
		// TODO: Localize strings
		systemUpdatingRoot->findLabel("downloadPrompt")->setText("Your LeapTV is already up to date!\n");
		Vec3f pos = systemUpdatingRoot->findLabel("downloadPrompt")->getPosition();
		pos.x = 0;
		systemUpdatingRoot->findLabel("downloadPrompt")->setPosition(pos);
		systemUpdatingRoot->find("progressBarContainer")->setVisible(false);
		systemUpdatingRoot->find("updatePrompt")->setVisible(false);
		systemUpdatedRoot->setVisible(false);
	}
}

/// <summary>
/// Called before showing progress bar
/// </summary>
void DownloadStep::calculateDownloadInfo()
{
	progressBar->setDisplay( ContentManager::getInstance()->getDownloadsRemaining() );
}

/// <summary>
/// Animation helper
/// </summary>
void DownloadStep::showFinished()
{
	progressBar->bubbleOut(0);
	systemUpdatingRoot->find("downloadPrompt")->fadeOut(0);
	systemUpdatingRoot->find("updatePrompt")->fadeIn(0.25f);

	printf("DownloadStep::showFinished()\n");
	printf("Ask package manager if it is ready since we are finished with the download\n");
	if( ContentManager::getInstance()->getFirmwareStatus() == FirmwareReady )
	{
		printf("Package manager says it is ready\n");
		countdownLabel->fadeIn(0.25f);
		outOfBox->setCurrentHighlight(updateButton);
		updateButton->setState(ButtonStateActive);
		updateButton->cancelActions();
		updateButton->bubbleIn(0.5f);

		rebootTimer = 20;
	}
	else
	{
		printf("Firmware packages are downloaded but package manager says it is not ready!!??\n");
		readyTimer = 2;
	}
}

/// <summary>
/// Download finished
/// </summary>
void DownloadStep::handleDonwloadFinished( String packageID )
{
	if (!progressBar->getIsFinished() )
	{
		progressBar->handleDonwloadFinished(packageID);
		if( progressBar->getIsFinished() )
		{ showFinished(); }
	}

	printf("DownloadStep::handleDonwloadFinished for %s\n", packageID.str());
}

/// <summary>
/// Download started
/// </summary>
void DownloadStep::handleDonwloadStarted( String packageID )
{
	if (!progressBar->getIsFinished() )
	{ progressBar->handleDonwloadStarted(packageID); }
}

/// <summary>
/// Needed for firmware dialog prompt
/// </summary>
void DownloadStep::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	printf("DownloadStep::handleDialogClosed\n");
	super::handleDialogClosed(dialogType, option);
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void DownloadStep::cameraLeaving()
{
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void DownloadStep::addHotSpots( ObjectArray* trackingList )
{
	printf("DownloadStep::addHotSpots\n");

	if( updateButton->isVisibleHierarchy() )
	{ trackingList->addElement(updateButton); }

	if( netError->isVisibleHierarchy() )
	{ netError->addHotSpots(trackingList); }

	super::addHotSpots(trackingList);
}

/// <summary>
/// If we want to handle the press action ourselves, usually welcomeHeaderLabelmeans the
/// action click is self contained within the page
/// </summary>
bool DownloadStep::handlePressAction(GameObject*& objectHighlight)
{
	if( objectHighlight == updateButton && updateButton->isVisibleHierarchy() )
	{
		killTimer = 1;
		outOfBox->screenDarken->setVisible(true);
		outOfBox->screenDarken->setOpacity(0);
		outOfBox->screenDarken->fadeTo(1, 0.5f);
	}
	return super::handlePressAction(objectHighlight);
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* DownloadStep::getArrivalSelection()
{
	printf("DownloadStep::getArrivalSelection()\n");

	if( systemUpdatingRoot->isVisibleHierarchy() )
	{ return updateButton; }

	if( systemUpdatedRoot->isVisibleHierarchy() )
	{ return outOfBox->getWorldRoot()->findButton("nextButton"); }

	return null;
}


} /* namespace Glasgow */

#include "AgeGateStep.h"
#include "OutOfBoxScene.h"

#include <time.h>
#include <iostream>

namespace Glasgow
{

AgeGateStep::AgeGateStep(GameObject * replacementRoot, OutOfBoxScene* scene) : super(replacementRoot, scene)
{
	numPad 		= (GateLockObject*)find("numPad");
	yearField	= find("birthField")->findLabel("codeText");
	entryPrompt	= findLabel("enterPrompt");
	errorPrompt = findLabel("errorPrompt");

	yearField->setBatched(false);
}

AgeGateStep::~AgeGateStep()
{
	printf("AgeGateStep::~AgeGateStep()\n");
}


/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool AgeGateStep::handlePressAction(GameObject*& objectHighlight)
{
	if ( objectHighlight != null && objectHighlight->descendantOf(numPad) )
	{
		outOfBox->lockOutInput(0.2f);
		String pinValue = objectHighlight->name;
		if( pinValue == "backspacebutton" )
		{
			if( enteredYear.length() > 0 )
			{
				enteredYear = enteredYear.substring(0, enteredYear.length()-1);
				outOfBox->playSound("Parent_Keystroke_Backspace");
				yearField->removeLastCharacter();
			}
		}
		else
		{
			outOfBox->playSound("Parent_Keystroke");
			enteredYear += pinValue;
			yearField->setText(enteredYear);
			yearField->fadeLastCharacterIn();
			if( enteredYear.length() == 4 )
			{ yearEntered(); }
		}
	}
	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool AgeGateStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

void AgeGateStep::yearEntered()
{
	time_t t = time(NULL);
	tm* timePtr = localtime(&t);
	int years = 1900 + timePtr->tm_year;

	printf("Entered year: %s Age: %d\n", enteredYear.str(),(years - enteredYear.toInt()));

	int age = years - enteredYear.toInt();
	if ( age < 18 )
	{
		errorPrompt->recursiveFadeIn();
		errorPrompt->recursiveFadeOut(2.0f);
		yearField->waitFor(1.0f);
		yearField->changeTo("");
		enteredYear = "";
	}
	else
	{
		outOfBox->gotoNextStep();
	}
}


/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void AgeGateStep::cameraArrived()
{
	outOfBox->enableHelpButton();
	errorPrompt->setVisible(false);
	entryPrompt->setVisible(false);
	entryPrompt->recursiveFadeIn(1.5f);
	yearField->setText("");
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* AgeGateStep::getArrivalSelection()
{
	return numPad->findButton("1");
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void AgeGateStep::addHotSpots( ObjectArray* trackingList )
{
	if( enteredYear.length() == 4  )
	{

	}
	else
	{
		numPad->addHotSpots(trackingList);
	}
}

/// <summary>
/// Custom update
/// </summary>
void AgeGateStep::update(float dt)
{ super::update(dt); }

}

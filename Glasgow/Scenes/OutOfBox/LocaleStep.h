#ifndef LOCALESTEP_H_
#define LOCALESTEP_H_

#include "OutOfBoxStep.h"
#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Include/SIActions.h"

namespace Glasgow
{

class LocaleStep : public OutOfBoxStep
{
	CLASSEXTENDS(LocaleStep, OutOfBoxStep);

public:
	LocaleStep(GameObject* objectRoot, OutOfBoxScene* scene);
	virtual ~LocaleStep();

	/// <summary>
	/// saves user settings
	/// </summary>
	virtual void saveSettings();

	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called when the camera is moving away from this page
	/// </summary>
	virtual void cameraLeaving();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );
private:
	/// <summary>
	/// Deselects all checkboxes on screen
	/// </summary>
	void deselectAllCheckBoxes();

	/// <summary>
	/// Returns the selected locale
	/// </summary>
	String getLocaleName();

	/// <summary>
	/// Returns the button for the locale
	/// </summary>
	CheckButton* getLocaleButton(SystemLocale locale);

	CheckButton* 	selectedLocale;
	SystemLocale chosenLocale;
};

}
#endif

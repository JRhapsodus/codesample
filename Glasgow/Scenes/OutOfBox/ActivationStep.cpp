#include "ActivationStep.h"
#include "OutOfBoxScene.h"

namespace Glasgow
{

ActivationStep::ActivationStep(GameObject * replacementRoot, OutOfBoxScene* scene) : super(replacementRoot, scene)
{
	printf("ActivationStep::ActivationStep\n");
	activationRoot 	   = find("activationRoot");
	activatedRoot  	   = find("activatedRoot");

	glasgowIcon 		= activationRoot->findSprite("icon");
	firmwareStatusLabel = activationRoot->findLabel("firmwareUpdateLabel");
	activateCodeLabel  = activationRoot->find("activationCode")->findLabel("codeText");
	attempts 		   = 0;
	skipButton 		   = activationRoot->findButton("skipButton");
	netErrorActive = false;
	setupTimeOut 			 = -1;
	activationCodeTimeOut	 = -1;
	activatedRoot->setVisible(false);
	skipButton->setVisible(false);
	processed = true;
	glasgowIcon->setVisible(false);
	firmwareStatusLabel->setVisible(false);

	createNetError();
}

ActivationStep::~ActivationStep()
{
	printf("ActivationStep::~ActivationStep()\n");
	SAFE_RELEASE(netError);
	disconnect(PackageManager::Instance(), SIGNAL(DeviceUpdated()), this, SLOT(onDeviceUpdated()));
}

/// <summary>
/// Helper call to instantiate a netError object to handle reconnect / skip flow
/// </summary>
void ActivationStep::createNetError()
{
	printf("ActivationStep::createNetError()\n");
	netError = (NetErrorObject*)Scene::instantiate("NetErrorObject"); //ref1
	find("netErrorContainer")->addChild(netError);//ref 2 ::~ -> ref 1
	netError->setVisible(false);
	netError->setPosition(Vec3f::Zero);

	progressBar = (FirmwareProgressBar*)Scene::instantiate(FirmwareProgressBar::type());
	activationRoot->addChild(progressBar);
	progressBar->setVisible(false);
	progressBar->setPosition(Vec3f(0, -206, 0));
	progressBar->setCullingLayerRecursive(outOfBox->getCamera(0)->getCullingMask());
	progressBar->setScale( Vec3f::One*0.8f);
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
void ActivationStep::handleDialogClosed( DialogType dialogType, DialogButtonOption option )
{
	if( dialogType == DialogTypeActivationFail )
	{
		if( option.id == "tryagain" )
		{
			startActivationRequest();
			skipButton->setVisible(false);
			outOfBox->removeSelection();
		}
		else
		{
			outOfBox->removeSelection();
		}
	}

	super::handleDialogClosed(dialogType,option);
}

void ActivationStep::handleDialogCanceled( DialogType dialogType )
{
	if( dialogType == DialogTypeActivationFail )
	{
		if( attempts < 3 )
		{ startActivationRequest(); }

		skipButton->setVisible(false);
		outOfBox->removeSelection();
	}
	else
	{
		outOfBox->removeSelection();
	}

	super::handleDialogCanceled(dialogType);
}

/// <summary>
/// Download progress
/// </summary>
void ActivationStep::handleDownloadProgress( String package, int downloadProgress)
{
	if( !progressBar->getIsFinished() )
	{ progressBar->handleDownloadProgress(package, downloadProgress); }
}

/// <summary>
/// Download finished
/// </summary>
void ActivationStep::handleDonwloadFinished( String packageID )
{
	if( !progressBar->getIsFinished() )
	{
		progressBar->handleDonwloadFinished(packageID);
		if( progressBar->getIsFinished() )
		{
			progressBar->setVisible(false);
			glasgowIcon->bubbleIn(0.5f);
			firmwareStatusLabel->fadeIn(0.5f);
		}
	}
}


/// <summary>
/// Download started
/// </summary>
void ActivationStep::handleDonwloadStarted( String packageID )
{
	if( !progressBar->getIsFinished() )
	{ progressBar->handleDonwloadStarted(packageID); }
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool ActivationStep::handlePressAction(GameObject*& objectHighlight)
{ return false; }

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool ActivationStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void ActivationStep::cameraArrived()
{
	outOfBox->enableHelpButton();

	printf("Asking Package Manager for Firmware status\n");
	attempts = 0;
	FirmwareStatus status = ContentManager::getInstance()->getFirmwareStatus();
	if ( status == FirmwareReady )
	{
//		printf("Firmware is ready to FirmwareReady!\n");
		glasgowIcon->bubbleIn(0.5f);
		firmwareStatusLabel->fadeIn(0.5f);
		progressBar->setVisible(false);
	}
	else if ( status == FirmwarePending )
	{
//		printf("Firmware is FirmwarePending!\n");
		glasgowIcon->setVisible(false);
		firmwareStatusLabel->setVisible(false);
		progressBar->setDisplay(ContentManager::getInstance()->getDownloadsRemaining());
		progressBar->setVisible(true);
	}
	else
	{
//		printf("No firmware is needed to update\n");
		progressBar->setVisible(false);
		firmwareStatusLabel->setVisible(false);
		glasgowIcon->setVisible(false);
	}


	RioPkgManager* managerInstance = PackageManager::Instance();
	managerInstance->UpdateProfiles(true);
	connect(managerInstance, SIGNAL(DeviceUpdated()), this, SLOT(onDeviceUpdated()));
	startActivationRequest();
}

/// <summary>
///
/// </summary>
void ActivationStep::handlePendingChanged()
{
	super::handlePendingChanged();
	if( activationRoot->isVisibleHierarchy() )
	{
		FirmwareStatus status = ContentManager::getInstance()->getFirmwareStatus();
		if ( status == FirmwareReady )
		{
			glasgowIcon->bubbleIn(0.5f);
			firmwareStatusLabel->fadeIn(0.5f);
			progressBar->setVisible(false);
		}
		else if ( status == FirmwarePending )
		{
			printf("Show progress bar\n");
			glasgowIcon->setVisible(false);
			firmwareStatusLabel->setVisible(false);
			progressBar->setVisible(true);
			progressBar->setScale(Vec3f::One*0.8f);
			progressBar->setDisplay(ContentManager::getInstance()->getDownloadsRemaining());
		}
	}
}

/// <summary>progressBar->
/// Activation code succeeded
/// </summary>
void ActivationStep::doActivationRequestFinished()
{
	if( !processed )
	{
		String activationCode = activationRequest.value().toStdString().c_str();
		printf("ACTIVATION REQUEST IS FINISHED: %s\n", activationCode.str());
		activationCodeTimeOut = 0;

		if(activationCode.isEmpty())
		{									//Unavailable
			activateCodeLabel->setText("Unavailable"); //Requesting

			if( !netErrorActive )
			{
				// Only show the try again dialog on the first two attempts
				if ( attempts < 3 )
				{
					// TODO: Localize Strings
					if( attempts == 1 )
					{
						DialogOptions options;
						options.title   = "Activation Request Error";
						options.prompt  = "There was an issue creating your activation code.  Please try again.";
						options.buttonA = DialogButtonOption("Try Again", "tryagain");
						outOfBox->showModalDialog(DialogTypeActivationFail, options);
						processed = true;
						return;
					}
					else
					{
						DialogOptions options;
						options.title 	= "Error";
						options.prompt  = "We were unable to get a Registration Code. You can skip this step by selecting the \"Hint\" button on the registration screen, then select \"Skip Registration\".";
						options.buttonA = DialogButtonOption("OK", "ok");
						options.ignoreOldTrackedObjects = true;
						outOfBox->showModalDialog(DialogTypeActivationFail, options);
						processed = true;
						return;
					}
				}
			}

			ButtonObject *helpButton = outOfBox->getWorldRoot()->findButton("helpButton");
			outOfBox->setCurrentHighlight(helpButton);
			processed = true;
			return;
		}

		if( !netErrorActive )
		{
			activateCodeLabel->fadeOut(0);
			activateCodeLabel->changeTo(activationCode.str());
			activateCodeLabel->fadeIn(0);

			//start setup time out
			setupTimeOut = Constants::Time::activationTimeOut;
		}
		outOfBox->setupHotSpots();
		processed = true;
	}
}

/// <summary>
/// Activation code timed out
/// </summary>
void ActivationStep::doActivationTimeOut()
{

	activationCodeTimeOut = 0;
	if( !activatedRoot->isVisibleHierarchy() && !processed)
	{
		DialogOptions options;
		options.title 	= "Error";
		options.prompt  = "We were unable to get a Registration Code. You can skip this step by selecting the \"Hint\" button on the registration screen, then select \"Skip Registration\".";

		if( !ConnectionManager::getInstance()->leapFrogServersResponded )
		{
			options.title 	= "Error";
			options.prompt  = "The LeapFrog servers are currently unavailable. You can skip this step by selecting the \"Hint\" button on the registration screen, then select \"Skip Registration\".";
			attempts = 100;
		}

		options.buttonA = DialogButtonOption("OK", "ok");
		options.ignoreOldTrackedObjects = true;
		outOfBox->showModalDialog(DialogTypeActivationFail, options);
		processed = true;
	}
}

/// <summary>
/// Activation code failed
/// </summary>
void ActivationStep::doActivationFail()
{
	if( !processed )
	{
		printf("ACTIVATION REQUEST ERROR: %s\n", activationRequest.error().message().toStdString().c_str());
		activationCodeTimeOut = 0;

		//only allow 2 attempts
		if( !activatedRoot->isVisibleHierarchy())
		{
			if ( attempts < 3 && !netErrorActive)
			{
				activateCodeLabel->setText("Unavailable");

				if( attempts == 1 )
				{
					DialogOptions options;
					options.title 	= "Activation Request Error";
					options.prompt  = activationRequest.error().message().toStdString().c_str();
					options.buttonA = DialogButtonOption("Try Again", "tryagain");
					options.ignoreOldTrackedObjects = true;
					outOfBox->showModalDialog(DialogTypeActivationFail , options);
				}
				else
				{
					DialogOptions options;
					options.title 	= "Error";
					options.prompt  = "We were unable to get a Registration Code. You can skip this step by selecting the \"Hint\" button on the registration screen, then select \"Skip Registration\".";
					options.buttonA = DialogButtonOption("OK", "ok");
					options.ignoreOldTrackedObjects = true;
					outOfBox->showModalDialog(DialogTypeActivationFail , options);
				}
			}
			else
			{ showNetworkError("Check Connection", "The request to get your registration code has timed out."); }
		}

		outOfBox->setupHotSpots();
		processed = true;
	}
}

/// <summary>
/// Updates activation request
/// </summary>
void ActivationStep::updateActivationRequest(float dt )
{
	if( activationCodeTimeOut > 0 )
	{
		activationCodeTimeOut -= dt;
		if( activationRequest.isFinished() )
		{
			if( activationRequest.isError() || !activationRequest.isValid())
			{ doActivationFail(); }
			else
			{ doActivationRequestFinished(); }
		}
		else if ( activationCodeTimeOut <= 0 )
		{ doActivationTimeOut(); }
	}
}

/// <summary>
/// Network error occured
/// </summary>
void ActivationStep::handleNetworkError(String networkName, String networkError)
{
	printf("ActivationStep::handleNetworkError ==> %s\n", networkError.str());
	//No need to care if network goes down while an activation request has been receieved
	if( !activatedRoot->isVisibleHierarchy())
	{ showNetworkError("Check Connection", networkError); }

	outOfBox->setupHotSpots();
}

/// <summary>
/// Network connection made
/// </summary>
void ActivationStep::handleNetworkConnected( String networkName )
{
	printf("ActivationStep::handleNetworkConnected()\n");
	netError->animateOut();
	netErrorActive = false;

	if( activatedRoot->isVisibleHierarchy())
	{
		activatedRoot->find("welcomeHeader")->bubbleIn(0.5f);
		activatedRoot->find("freeGameIcon")->bubbleIn(0.65f);
		activatedRoot->find("welcomeText1")->setVisible(false);
		activatedRoot->find("welcomeText2")->bubbleIn(0.75f);
	}
	else
	{
		activationRoot->find("activationCode")->bubbleIn(0);
		activationRoot->find("computerText")->bubbleIn(0);
		activationRoot->find("controllerWorkingText")->bubbleIn(0);
		activationRoot->find("enterCodeText")->bubbleIn(0);
		activationRoot->find("urlText")->bubbleIn(0);
		if(outOfBox->getCurrentStep() == this)
		{ startActivationRequest();}

		FirmwareStatus status = ContentManager::getInstance()->getFirmwareStatus();
		if ( status == FirmwareReady )
		{ progressBar->setVisible(false); }
		else if ( status == FirmwarePending )
		{
			progressBar->setDisplay(ContentManager::getInstance()->getDownloadsRemaining());
			progressBar->setVisible(true);
			progressBar->bubbleIn(0);
		}
		else
		{ progressBar->setVisible(false); }
	}

	outOfBox->setupHotSpots();
}

/// <summary>
/// Animation transition into a networking error
/// </summary>
void ActivationStep::showNetworkError( String header, String error )
{
	setupTimeOut = 0;
	activationCodeTimeOut = 0;

	activationRoot->find("activationCode")->bubbleOut(0);
	activationRoot->find("computerText")->bubbleOut(0);
	activationRoot->find("controllerWorkingText")->bubbleOut(0);
	activationRoot->find("enterCodeText")->bubbleOut(0);
	//activationRoot->find("skipButton")->bubbleOut(0);
	activationRoot->find("urlText")->bubbleOut(0);
	progressBar->bubbleOut(0);

	activatedRoot->find("freeGameIcon")->bubbleOut(0);
	activatedRoot->find("welcomeHeader")->fadeOut(0);
	activatedRoot->find("welcomeText1")->fadeOut(0);
	activatedRoot->find("welcomeText2")->fadeOut(0);

	netError->setVisible(true);
	netError->displayNetworkError(header, error);
	netError->skipButton->setVisible(false);

	outOfBox->setupHotSpots();

	if( !outOfBox->isShowingModalDialog() )
	{ outOfBox->setCurrentHighlight(netError->reconnectButton); }
	netErrorActive = true;
}

/// <summary>
/// Overridden so we can poll the activationRequest
/// </summary>
void ActivationStep::update(float dt)
{
	updateActivationRequest(dt);
	super::update(dt);
}

/// <summary>
/// Start activation request
/// </summary>
void ActivationStep::startActivationRequest()
{
	if( attempts >= 2 )
		return;

	printf("ActivationStep::startActivationRequest()\n");
	processed = false;
	attempts++;

	// TODO: Localize strings
	activateCodeLabel->setText("Requesting");
	activationRequest = PackageManager::Instance()->GetActivationCode();
	activationCodeTimeOut = Constants::Time::registrationCodeTimeOut;
}

/// <summary>
/// Remove network error
/// </summary>
void ActivationStep::hideNetworkError()
{
	if( netError->isVisibleHierarchy())
	{ netError->animateOut(); }

	netErrorActive = false;
	outOfBox->setupHotSpots();
}

/// <summary>
/// We have a call that the device was updated via the activation step !!!
/// </summary>
void ActivationStep::onDeviceUpdated()
{
	printf("=======================================\n");
	printf("=======================================\n");
	printf("ACTIVATION SUCCEEDED !!! \n");
	printf("=======================================\n");
	printf("=======================================\n");
	SystemSettings::setOOBEStep("cameraStep");
	SystemSettings::setIsActivated(true);
	hideNetworkError();

	progressBar->cancelActions();
	progressBar->setVisible(false);
	activationRoot->find("activationCode")->bubbleOut(0);
	activationRoot->find("computerText")->bubbleOut(0);
	activationRoot->find("controllerWorkingText")->bubbleOut(0);
	activationRoot->find("enterCodeText")->bubbleOut(0);
	activationRoot->find("urlText")->bubbleOut(0);
	glasgowIcon->bubbleOut(0);
	firmwareStatusLabel->fadeOut(0);
	activatedRoot->setVisible(true);

	Color c = activatedRoot->findLabel("welcomeHeader")->getTextColor();
	c.a = 1;
	activatedRoot->findLabel("welcomeHeader")->setTextColor(c);

	c = activatedRoot->findLabel("welcomeText1")->getTextColor();
	c.a = 1;
	activatedRoot->findLabel("welcomeText1")->setTextColor(c);

	c = activatedRoot->findLabel("welcomeText2")->getTextColor();
	c.a = 1;
	activatedRoot->findLabel("welcomeText2")->setTextColor(c);

	activatedRoot->find("welcomeHeader")->bubbleIn(0.5f);
	activatedRoot->find("freeGameIcon")->bubbleIn(0.65f);
	activatedRoot->find("welcomeText1")->setVisible(false);
	activatedRoot->find("welcomeText2")->bubbleIn(0.75f);
	outOfBox->enableNextStep(1);
	netErrorActive = false;
}


/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* ActivationStep::getArrivalSelection()
{
	if( attempts > 2 )
	{ return outOfBox->getWorldRoot()->findButton("helpButton"); }

	return null;
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void ActivationStep::addHotSpots( ObjectArray* trackingList )
{
	if ( netErrorActive )
	{ netError->addHotSpots(trackingList); }
}

}

#ifndef FINALSTEP_H_
#define FINALSTEP_H_

#include "OutOfBoxStep.h"

namespace Glasgow
{

class FinalStep : public OutOfBoxStep
{
	CLASSEXTENDS(FinalStep, OutOfBoxStep);
public:

	FinalStep(GameObject * replacementRoot, OutOfBoxScene* scene);
	virtual ~FinalStep();

	virtual void update(float dt);
	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();
};

}
#endif

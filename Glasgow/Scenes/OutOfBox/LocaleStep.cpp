#include "LocaleStep.h"
#include "OutOfBoxScene.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"
namespace Glasgow
{

LocaleStep::LocaleStep(GameObject* objectRoot, OutOfBoxScene* scene) : super( objectRoot, scene )
{}

LocaleStep::~LocaleStep()
{
	printf("LocaleStep::~LocaleStep()\n");
}

/// <summary>
/// saves user settings
/// </summary>
void LocaleStep::saveSettings()
{
	SystemSettings::setSystemLocale(chosenLocale);
}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void LocaleStep::addHotSpots( ObjectArray* trackingList )
{
	printf("LocaleStep::addHotSpots\n");
	trackingList->addElement( find("localeAustralia") );
	trackingList->addElement( find("localeCanada") );
	trackingList->addElement( find("localeIreland") );
	trackingList->addElement( find("localeNewZealand") );
	trackingList->addElement( find("localeOther") );
	trackingList->addElement( find("localeUK") );
	trackingList->addElement( find("localeUnitedStates") );
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool LocaleStep::handlePressAction(GameObject*& objectHighlight)
{
	if( objectHighlight != null && objectHighlight->typeOf(CheckButton::type()) )
	{
		deselectAllCheckBoxes();

		selectedLocale = (CheckButton*)objectHighlight;
		selectedLocale->setChecked(true);
		selectedLocale->setState(ButtonStateSelected);
		String locale =  objectHighlight->name.str();

		if(locale.equals("localeAustralia"))
		{ chosenLocale = SystemLocaleAustralia;}
		else if(locale.equals("localeCanada"))
		{ chosenLocale = SystemLocaleCanada;}
		else if(locale.equals("localeIreland"))
		{ chosenLocale = SystemLocaleIreland;}
		else if(locale.equals("localeNewZealand"))
		{ chosenLocale = SystemLocaleNewZealand;}
		else if(locale.equals("localeUK"))
		{ chosenLocale = SystemLocaleUnitedKingdom;}
		else if(locale.equals("localeUnitedStates"))
		{ chosenLocale = SystemLocaleUnitedStates;}
		else
		{ chosenLocale = SystemLocaleDefault;}

		StringsManager::getInstance()->changeSystemLocale(chosenLocale);
		SystemSettings::setSystemLocale(chosenLocale);
		outOfBox->getWorldRoot()->refreshLocalizedStringsRecursive();
		outOfBox->enableNextStep(0.25f);
	}
}

/// <summary>
/// Deselects all checkboxes on screen
/// </summary>
void LocaleStep::deselectAllCheckBoxes()
{
	ObjectArray* children = getChildren();
	for(int i = 0; i < children->getSize(); ++i)
	{
		BaseObject* obj = children->elementAt(i);
		if(obj->typeOf(CheckButton::type()))
		{
			((CheckButton*)obj)->setChecked(false);
			((CheckButton*)obj)->setState(ButtonStateNormal);
		}
	}
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool LocaleStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{
	return super::handleAnlogChange(objectLeft, objectEntered, currentHighlight);
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void LocaleStep::cameraArrived()
{
	printf("Arrived at localeStep\n");

	outOfBox->hideHelpButton();

	deselectAllCheckBoxes();
	chosenLocale = SystemSettings::getSystemLocale();
	selectedLocale = getLocaleButton(chosenLocale);
	if(selectedLocale != null)
	{
		selectedLocale->setState(ButtonStateActive);
		selectedLocale->setChecked(true);
	}
}

/// <summary>
/// Returns the selected locale
/// </summary>
String LocaleStep::getLocaleName()
{ return selectedLocale->name; }

/// <summary>
/// Returns the button for the locale
/// </summary>
CheckButton* LocaleStep::getLocaleButton(SystemLocale locale)
{
	switch(locale)
	{
		case SystemLocaleAustralia: 	return (CheckButton*)findButton("localeAustralia");		break;
		case SystemLocaleCanada:		return (CheckButton*)findButton("localeCanada");		break;
		case SystemLocaleIreland:		return (CheckButton*)findButton("localeIreland");		break;
		case SystemLocaleNewZealand:	return (CheckButton*)findButton("localeNewZealand");	break;
		case SystemLocaleUnitedKingdom:	return (CheckButton*)findButton("localeUK");			break;
		case SystemLocaleUnitedStates:	return (CheckButton*)findButton("localeUnitedStates");	break;
		case SystemLocaleDefault:		return (CheckButton*)findButton("localeOther");			break;
	}

	return null;
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void LocaleStep::cameraLeaving()
{
	//nothing special
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* LocaleStep::getArrivalSelection()
{ return selectedLocale; }


}

#include "WelcomeStep.h"
#include "OutOfBoxScene.h"
namespace Glasgow
{

WelcomeStep::WelcomeStep( GameObject* root, OutOfBoxScene* scene) : super(root, scene)
{
	connectStep1Icon		= findSprite("connectStep1");
	connectStep2Icon		= findSprite("connectStep2");
	connectSubHeaderLabel	= findLabel("connectSubHeader");
	controllerIcon 			= findSprite("controller2Illustration");
	gameIcon 				= findSprite("freeGameIcon");
	welcomeHeaderLabel		= findLabel("header");
	welcomeSubHeaderLabel	= findLabel("welcomeSubHeader");
	welcomePressLabel		= findLabel("lightBlinksText");
	welcomePromptLabel		= findLabel("welcomeText");

	errorPrompt 			= findLabel("errorPrompt");
	errorPrompt->setVisible(false);
	errorIcon				= findSprite("errorIcon");
	errorIcon->setVisible(false);

	hasPressedA 			= false;
	hasPairedController 	= false;
	isShowingError			= false;
}

WelcomeStep::~WelcomeStep()
{
	printf("WelcomeStep::~WelcomeStep()\n");
}

/// <summary>
/// saves user settings
/// </summary>
void WelcomeStep::saveSettings()
{
	//override if needed
}

/// <summary>
/// pairing of controller success
/// </summary>
void WelcomeStep::pairingComplete()
{
	printf("Pairing Complete\n");

	if(isShowingError)
	{
		TimerManager::cancelTimer("HideSynchError");
		hideSynchError();
	}

	hasPairedController = true;

	connectStep1Icon->moveBy( Vec3f::Left*800, 1);
	connectStep2Icon->moveBy( Vec3f::Right*800, 1);
	connectStep1Icon->makeVisible(false);
	connectStep2Icon->makeVisible(false);

	animateInController(1);
}

///<summary>
/// Animation helper call
/// </summary>
void WelcomeStep::animateInController(float delay)
{
	welcomePressLabel->waitFor(delay);
	welcomePressLabel->makeVisible(true);
	Color pressTextColor = welcomePressLabel->getTextColor();
	pressTextColor.a = 0;
	welcomePressLabel->setTextColor( pressTextColor );
	pressTextColor.a = 1;
	welcomePressLabel->colorTo( pressTextColor, 1 );

	delay += 0.5f;
	controllerIcon->waitFor(delay);
	controllerIcon->makeVisible(true);
	controllerIcon->setOpacity(0);
	controllerIcon->fadeTo(1, 1);
}

/// <summary>
/// Animation helper call
/// </summary>
void WelcomeStep::animateToNext()
{
	controllerIcon->moveBy( Vec3f::Left * 900, 1 );
	welcomePressLabel->moveBy( Vec3f::Right * 900, 1 );
	welcomePressLabel->makeVisible(false);
	controllerIcon->makeVisible(false);

	connectSubHeaderLabel->colorTo( Color( 1,1,1,0), 1);
	welcomePromptLabel->moveBy( Vec3f::Right*800, 0.01);
	welcomePromptLabel->makeVisible(true);
	welcomePromptLabel->waitFor(1);
	welcomePromptLabel->moveBy( Vec3f::Left*800, 1);

	gameIcon->moveBy( Vec3f::Left*600, 0.01f);
	gameIcon->makeVisible(true);
	gameIcon->waitFor(1);
	gameIcon->moveBy( Vec3f::Right*600, 1);

	outOfBox->enableNextStep( 2.5f );
}

/// <summary>
/// handle controller pairing
/// </summary>
void WelcomeStep::handleControllerSync(bool success)
{
	printf("WelcomeStep::handleControllerSync  -- %B", success);

	// If the controller failed to synch then show the error message
	if(!success)
	{
		// Show the synch error if we're not already showing it and if the welcome intro has finished playing
		// Also don't show if they already have a synched controller.
		if(!isShowingError && !connectStep1Icon->hasActions() && !hasPairedController)
		{
			showSynchError();
			hideSynchInstructions();
			// Dismiss the error with a timeout
			TimerManager::addTimer("HideSynchError", 10, 0);
		}
	}
}

/// <summary>
/// Controller Connected Event
/// </summary>
void WelcomeStep::handleControllerConnected()
{
	printf("WelcomeStep::handleControllerConnected\n");
	if(!hasPairedController && !connectStep1Icon->hasActions())
	{
		pairingComplete();
	}
}

/// <summary>
/// Controller Disconnected Event
/// </summary>
void WelcomeStep::handleControllerDisconnected()
{
	printf("WelcomeStep::handleControllerDisconnected");
}

/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool WelcomeStep::handlePressAction(GameObject*& objectHighlight)
{
	if (!hasPairedController && !connectStep1Icon->hasActions())
	{
		//user pressed, so we're connected
		pairingComplete();
		return true;
	}

	if ( !hasPressedA && !welcomePressLabel->hasActions() && welcomePressLabel->isVisibleHierarchy() )
	{
		hasPressedA = true;
		animateToNext();
		return true;
	}

	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool WelcomeStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{ return false; }

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void WelcomeStep::cameraArrived()
{
	printf("WelcomeStep::cameraArrived()\n");
	outOfBox->hideHelpButton();

	controllerIcon->setVisible(false);
	gameIcon->setVisible(false);
	welcomeHeaderLabel->setVisible(false);
	welcomePressLabel->setVisible(false);
	welcomeSubHeaderLabel->setVisible(false);
	welcomePromptLabel->setVisible(false);
	connectSubHeaderLabel->setVisible(false);
	connectStep1Icon->setVisible(false);
	connectStep2Icon->setVisible(false);


	welcomeHeaderLabel->setVisible(true);
	Vec3f originalPos = welcomeHeaderLabel->getPosition();
	welcomeHeaderLabel->setPosition( Vec3f(0, -500, -250 ));
	Color textColor = welcomeHeaderLabel->getTextColor();
	textColor.a = 0;
	welcomeHeaderLabel->setTextColor( textColor );
	textColor.a = 1;
	TRelease<MultiAction> multi( new MultiAction(welcomeHeaderLabel));
	multi->addAction( TRelease<ColorToAction>( new ColorToAction(welcomeHeaderLabel, textColor, 0.5f, EaseFunctions::cubicEaseInOut)) );
	multi->addAction( TRelease<MoveToAction>( new MoveToAction(welcomeHeaderLabel, Vec3f(0, 0, 0), 0.5f ) ));

	welcomeHeaderLabel->waitFor(1);
	welcomeHeaderLabel->queueAction( multi );
	welcomeHeaderLabel->waitFor(2);
	welcomeHeaderLabel->moveTo( originalPos, 1);

	Vec3f promptPosition = welcomeSubHeaderLabel->getPosition();
	welcomeSubHeaderLabel->setPosition( Vec3f( promptPosition.x, 0, promptPosition.z) );
	welcomeSubHeaderLabel->waitFor(5.0f);
	welcomeSubHeaderLabel->makeVisible(true);
	welcomeSubHeaderLabel->setTextColor( Color( 1,1,1,0));
	welcomeSubHeaderLabel->colorTo( Color(1,1,1,1), 0.5f );
	welcomeSubHeaderLabel->moveTo( promptPosition, 0.5f);

	if(!outOfBox->hasASyncedController())
	{
		welcomeSubHeaderLabel->waitFor(2.0f);
		welcomeSubHeaderLabel->colorTo(Color(1,1,1,0),1);

		float waitTime = 9.0f;

		connectSubHeaderLabel->waitFor(waitTime);
		connectSubHeaderLabel->makeVisible(true);
		connectSubHeaderLabel->setTextColor(Color(1,1,1,0));
		connectSubHeaderLabel->colorTo(Color(1,1,1,1),1);

		connectStep1Icon->moveBy( Vec3f::Left*800, 0.01);
		connectStep1Icon->waitFor(waitTime);
		connectStep1Icon->makeVisible(true);
		connectStep1Icon->moveBy( Vec3f::Right*800, 1);
		connectStep2Icon->moveBy( Vec3f::Right*800, 0.01);
		connectStep2Icon->waitFor(waitTime);
		connectStep2Icon->makeVisible(true);
		connectStep2Icon->moveBy( Vec3f::Left*800, 1);
	}
	else
	{
		hasPairedController = true;
		animateInController(7.0f);
	}
}

/// <summary>
/// Called when the camera is moving away from this page
/// </summary>
void WelcomeStep::cameraLeaving()
{

}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* WelcomeStep::getArrivalSelection()
{ return null; }


void WelcomeStep::showSynchError()
{
	isShowingError = true;

	errorPrompt->fadeIn(0);
	errorIcon->fadeIn(0);
}

void WelcomeStep::hideSynchError()
{
	errorPrompt->fadeOut(0);
	errorIcon->fadeOut(0);

	isShowingError = false;
}

void WelcomeStep::showSynchInstructions()
{
	connectSubHeaderLabel->fadeIn(0);
	welcomeHeaderLabel->fadeIn(0);

	connectStep1Icon->moveTo(Vec3f(-282, -105, 0), 0.5f);
	connectStep2Icon->moveTo(Vec3f(290, -105, 0), 0.5f);
}

void WelcomeStep::hideSynchInstructions()
{
	connectSubHeaderLabel->fadeOut(0);
	welcomeHeaderLabel->fadeOut(0);

	connectStep1Icon->moveTo(Vec3f(-1000, -105, 0), 0.5f);
	connectStep2Icon->moveTo(Vec3f(1000, -105, 0), 0.5f);
}

}

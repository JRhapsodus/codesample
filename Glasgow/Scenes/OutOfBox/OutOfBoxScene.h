#ifndef OUTOFBOX_H
#define OUTOFBOX_H


#include "SIEngine/Core/Scene.h"
#include "SIEngine/Include/GlasgowControls.h"

#include "Glasgow/Scenes/OutOfBox/OutOfBoxStep.h"
#include "Glasgow/Scenes/OutOfBox/WelcomeStep.h"
#include "Glasgow/Scenes/OutOfBox/DownloadStep.h"
#include "Glasgow/Scenes/OutOfBox/CameraStep.h"
#include "Glasgow/Scenes/OutOfBox/LocaleStep.h"
#include "Glasgow/Scenes/OutOfBox/ChooseWifiStep.h"
#include "Glasgow/Scenes/OutOfBox/LockCreationStep.h"
#include "Glasgow/Scenes/OutOfBox/EthernetStep.h"
#include "Glasgow/Scenes/OutOfBox/ProfileCreationStep.h"
#include "Glasgow/Scenes/OutOfBox/ActivationStep.h"
#include "Glasgow/Scenes/OutOfBox/FinalStep.h"
#include "Glasgow/Scenes/OutOfBox/AgeGateStep.h"

using namespace SICore;

namespace Glasgow
{

typedef enum OutOfBoxParentSteps
{
	OutOfBoxParentStepInvalid,
	OutOfBoxParentStepPowerUp,
	OutOfBoxParentStepSetUp,
	OutOfBoxParentStepRegister,
	OutOfBoxParentStepDownload,
	OutOfBoxParentStepPlayAndLearn,
	OutOfBoxParentStepMax
} OutOfBoxParentStep;

class OutOfBoxScene : public Scene
{
	CLASSEXTENDS(OutOfBoxScene, Scene);

public:
	OutOfBoxScene();
	virtual ~OutOfBoxScene();

	SpriteObject*	screenDarken;

	/// <summary>
	/// Base scene feature, handling camera
	/// </summary>
	virtual void onCameraPluggedIn();

	/// <summary>
	/// Base scene feature, handling camera
	/// </summary>
	virtual void onCameraUnplugged();

	/// <summary> 
	/// Base scene feature, handling controller pairing
	/// </summary>
	virtual void onControllerSynced(HWController* controller, bool success);

	/// <summary>
	/// Base scene feature, handling controller connecting
	/// </summary>
	virtual void onControllerConnected(HWController* controller);

	/// <summary>
	/// Base scene feature, handling controller disconnecting
	/// </summary>
	virtual void onControllerDisconnected(HWController* controller);

	/// <summary>
	/// Download error'd out
	/// </summary>
	virtual void onDownloadError();

	/// <summary>
	/// Download finished
	/// </summary>
	virtual void onDonwloadFinished( String packageID );

	/// <summary>
	/// Download started
	/// </summary>
	virtual void onDonwloadStarted( String packageID );

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onButtonHint();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary> 
	virtual void onButtonPressAction();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onButtonPressBack();

	/// <summary>
	/// Base scene feature, handling input
	/// </summary>
	virtual void onSelectionChange( GameObject* oldSelection, GameObject* newSelection);

	/// <summary>onTimerExpired
	/// Custom handling of analog changes
	/// </summary>
	virtual void onAnalogChange(GameObject* objectLeft, GameObject* objectEntered );

	/// <summary>
	/// Base scene feature, tick
	/// </summary>
	virtual void update(float dt);

	/// <summary>
	/// onDialogHandled
	/// </summary>
	virtual void onDialogHandled( DialogType dialogType, DialogButtonOption option );

	/// <summary>
	/// When a dialog is canceled, meaning an option wasn't selected but it was dismissed.
	/// </summary>
	virtual void onDialogCanceled( DialogType dialogType);

	/// <summary>
	/// Setup hotspots
	/// </summary>
	void setupHotSpots();

	/// <summary>
	/// Download has progressed
	/// </summary>

	void onDonwloadProgressed( String packageID, int downloadProgress );


	virtual void onPendingDownloadsChanged();


	///<summary>
	/// returns step being displayed
	///</summary>
	OutOfBoxStep* getCurrentStep();

	/// <summary>
	/// Goes to the next step
	/// </summary>
	void gotoNextStep();

	/// <summary>
	/// skips the registration process
	/// </summary>
	void skipActivation();

	/// <summary>
	/// Enables the next step
	/// </summary>
	void enableNextStep(float delay);

	///<summary>
	/// Hides the next step
	/// </summary>
	void hideNextStep();

	/// <summary>
	/// Enables the help button
	/// </summary>
	void enableHelpButton();

	///<summary>
	/// Hides the help button
	/// </summary>
	void hideHelpButton();

	/// <summary>
	/// Child scene uses timers
	/// </summary>
	virtual void onTimerExpired( TimerEvent* timer );

	/// <summary>OutOfBoxParentStepMax
	/// Network error occured
	/// </summary>
	void onNetworkError(String networkName, String networkError);

	/// <summary>
	/// Network connection made
	/// </summary>
	void onNetworkConnected( String networkName );

	/// <summary>
	/// Base scene feature, handling ethernet plugged in
	/// </summary>
	virtual void onEthernetPluggedIn();

	/// <summary>
	/// Base scene feature, handling ethernet unplugged
	/// </summary>
	virtual void onEthernetUnplugged();

private:
	/// <summary> 
	/// Fills in pointers and arrays of references post scene Load
	/// </summary>
	void fillReferences();

	/// <summary>
	/// Sets up all objects for the initial scene entry
	/// </summary>
	void setupInitialState();

	/// <summary>
	/// Called by the steps when the step is complete
	/// </summary>
	void onStepComplete(OutOfBoxStep* step );

	/// <summary>
	/// Animation functions
	/// </summary> 
	void moveToStep( OutOfBoxStep* lastStep, OutOfBoxStep* newStep);

	/// <summary>
	/// Helper Animation functions
	/// </summary>
	void moveIndicatorLeft();

	/// <summary>
	/// Helper Animation functions
	/// </summary>
	void moveIndicatorRight();

	/// <summary>
	/// Helper Animation functions
	/// </summary>
	void moveToIndicatorStep(OutOfBoxParentStep step);

	/// <summary>
	/// get the parent step associated with the step
	/// </summary>
	OutOfBoxParentStep getParentStepForStep(OutOfBoxStep* step);

	/// <summary>
	/// get the next step
	/// </summary>
	OutOfBoxStep* getNextStep(OutOfBoxStep* step);

	/// <summary> 
	/// References to objects
	/// </summary> 
	SpriteObject*	background;

	TextLabel*		helpRollOver;
	GameObject* 	stepIndicator;
	GameObject*		stepsRoot;
	ButtonObject*	nextButton;
	ButtonObject*	prevButton;
	ButtonObject*	helpButton;

	float unplugTimer;
	int numEthernetChecks;
	float exitTimer;
	float enableNextStepTimer;

	FinalStep*				finalStep;
	AgeGateStep*			verifyAgeStep;
	ActivationStep*			activationStep;
	LockCreationStep* 		createPinStep;
	DownloadStep*			downloadStep;
	EthernetStep*   		ethernetStep;
	LocaleStep* 			localeStep;
	OutOfBoxStep*			currentStep;
	WelcomeStep* 			welcomeStep;
	CameraStep*				cameraStep;
	ChooseWifiStep* 		chooseWifi;
	ProfileCreationStep*	createProfileStep;
	HashTable				steps;

	OutOfBoxStep*			overRideStep;

};

}

#endif /* TESTSCENE_H_ */

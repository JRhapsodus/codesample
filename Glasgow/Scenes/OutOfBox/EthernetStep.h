#ifndef ETHERNETSTEP_H_
#define ETHERNETSTEP_H_

#include "OutOfBoxStep.h"

namespace Glasgow
{

class EthernetStep : public OutOfBoxStep
{
	CLASSEXTENDS(EthernetStep, OutOfBoxStep);
public:
	EthernetStep(GameObject * replacementRoot, OutOfBoxScene* scene);
	virtual ~EthernetStep();


	/// <summary>
	/// If we want to handle the press action ourselves, usually means the
	/// action click is self contained within the page
	/// </summary>
	virtual bool handlePressAction(GameObject*& objectHighlight);

	/// <summary>
	/// If we want to handle analog changes
	/// </summary>
	virtual bool handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight);

	/// <summary>
	/// Called when the camera is moving to this page
	/// </summary>
	virtual void cameraArrived();

	/// <summary>
	/// Called to get the object to be selected on camera arrival
	/// </summary>
	virtual GameObject* getArrivalSelection();

	/// <summary>
	/// Adds the page objects to the hot spots
	/// </summary>
	virtual void addHotSpots( ObjectArray* trackingList );

	/// <summary>
	/// Network error occured
	/// </summary>
	void handleNetworkError(String networkName, String networkError);

	/// <summary>
	/// Network connection made
	/// </summary>
	void handleNetworkConnected( String networkName );

	/// <summary>
	/// Custom update
	/// </summary>
	virtual void update(float dt);


	/// <summary>
	/// Asks connection manager for information regarding the test
	/// </summary>
	void testConnection();

private:
	ButtonObject* 	skipButton;
	TextLabel* 		ipAddress;
	TextLabel*		macAddress;

	TextLabel*		connectedPrompt;
	TextLabel*		connectingPrompt;
	TextLabel*		errorPrompt;

	SpriteObject*	connectionSprite;
	SpriteObject*	disconnectedSprite;
	SpriteObject*	systemIcon;
	TextLabel*		firmwareStatusLabel;

	float testConnectionTimer;

};

}
#endif

#include "EthernetStep.h"
#include "OutOfBoxScene.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"

namespace Glasgow
{

EthernetStep::EthernetStep(GameObject * replacementRoot, OutOfBoxScene* scene) : super(replacementRoot, scene)
{
	testConnectionTimer = -1;
	skipButton					= findButton("skipButton");
	ipAddress					= findLabel("ipAddress");
	macAddress					= findLabel("macAddress");
	connectedPrompt				= findLabel("connectedPrompt");
	connectingPrompt			= findLabel("connectingPrompt");
	errorPrompt					= findLabel("errorPrompt");
	connectionSprite			= findSprite("connectionStatus");
	disconnectedSprite			= findSprite("connectionFailed");

	systemIcon = findSprite("icon");
	firmwareStatusLabel = findLabel("firmwareStatusLabel");

	systemIcon->setVisible(false);
	firmwareStatusLabel->setVisible(false);
}

EthernetStep::~EthernetStep()
{
	printf("EthernetStep::~EthernetStep()\n");
}


/// <summary>
/// If we want to handle the press action ourselves, usually means the
/// action click is self contained within the page
/// </summary>
bool EthernetStep::handlePressAction(GameObject*& objectHighlight)
{
	return false;
}

/// <summary>
/// If we want to handle analog changes
/// </summary>
bool EthernetStep::handleAnlogChange(GameObject* objectLeft, GameObject* objectEntered, GameObject*& currentHighlight)
{

	return false;
}

/// <summary>
/// Called when the camera is moving to this page
/// </summary>
void EthernetStep::cameraArrived()
{
	outOfBox->enableHelpButton();
	skipButton->setVisible(false);
	ipAddress->setVisible(false);
	macAddress->setVisible(false);
	connectedPrompt->setVisible(false);
	connectingPrompt->recursiveFadeIn(1);
	errorPrompt->setVisible(false);
	connectionSprite->setVisible(false);
	connectionSprite->waitFor(1);
	connectionSprite->makeVisible(true);
	connectionSprite->scaleTo( connectionSprite->originalScale, 0.25f);
	disconnectedSprite->setVisible(false);

	systemIcon->setVisible(false);
	firmwareStatusLabel->setVisible(false);

	testConnectionTimer = 3;
	ConnectionManager::getInstance()->connectToEthernet();
}

/// <summary>
/// Called to get the object to be selected on camera arrival
/// </summary>
GameObject* EthernetStep::getArrivalSelection()
{
	return null;

}

/// <summary>
/// Adds the page objects to the hot spots
/// </summary>
void EthernetStep::addHotSpots( ObjectArray* trackingList )
{

	trackingList->addElement(skipButton);


}

/// <summary>
/// Network error occured
/// </summary>
void EthernetStep::handleNetworkError(String networkName, String networkError)
{



}
/// <summary>
/// Network connection made
/// </summary>
void EthernetStep::handleNetworkConnected( String networkName )
{
	outOfBox->playSound("Base_TransformSuccess");



}

/// <summary>
/// Asks connection manager for information regarding the test
/// </summary>
void EthernetStep::testConnection()
{
	bool isOnline = ConnectionManager::getInstance()->isServiceOnline("Wired");
	String ip = ConnectionManager::getInstance()->getIp("Wired");

	printf("EthernetStep::testConnection() => Online [%d] ipAddress [%s]\n", (int)isOnline, ip.str());

	if( isOnline )
	{
		outOfBox->playSound("Base_TransformSuccess");
		// TODO: Localize
	    macAddress->setText(String("MAC Address: ") + SystemSettings::getEthernetMacAddress());
	    ipAddress->setText(String("IP Address: ") + ip);

	    ipAddress->recursiveFadeIn(0.25f);
	    macAddress->recursiveFadeIn(0.25f);
	    connectedPrompt->recursiveFadeIn(0.25f);

	    connectingPrompt->recursiveFadeOut();
		outOfBox->enableNextStep(1);

		if( ContentManager::getInstance()->getFirmwareStatus() != FirmwareNotAvailabe )
		{
			systemIcon->bubbleIn(1);
			firmwareStatusLabel->fadeIn(1);
		}
	}
	else
	{
		outOfBox->playSound("Base_WiFiDisconnected");
		errorPrompt->recursiveFadeIn(0.25f);
		connectingPrompt->recursiveFadeOut();

		connectionSprite->setVisible(false);
		disconnectedSprite->setVisible(true);

		skipButton->setVisible(true);

		skipButton->setScale(Vec3f::Zero);

		skipButton->waitFor(0.25f);
		skipButton->scaleTo( skipButton->originalScale, 0.25f );
	}
}


/// <summary>
/// Custom update
/// </summary>
void EthernetStep::update(float dt)
{
	if( testConnectionTimer > 0 )
	{
		testConnectionTimer -= dt;
		if( testConnectionTimer <= 0 )
		{ testConnection(); }
	}

	super::update(dt);
}


}

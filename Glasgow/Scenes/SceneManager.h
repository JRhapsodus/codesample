#ifndef INCLUDE_SCENEMANAGER_H_
#define INCLUDE_SCENEMANAGER_H_

#include <App.h>
#include <AppInterface.h>
#include <AppManager.h>
#include <LTM.h>
#include <CSystemData.h>
#include <KernelMPI.h>
#include <ButtonMPI.h>
#include <DisplayMPI.h>
#include <iostream>
#include <stdio.h>
#include <ButtonEventQueue.h>
#include <ButtonTypes.h>
#include <BrioOpenGLConfig.h>
#include <EventListener.h>
#include <EventMPI.h>
#include <ButtonEventQueue.h>
#include <TouchEventQueue.h>
#include <Hardware/HWAnalogStickMPI.h>
#include <AccelerometerMPI.h>
#include <sys/time.h>
#include <GLES2/gl2.h>
#include <Hardware/HWController.h>
#include <Hardware/HWControllerMPI.h>
#include <Vision/VNVisionMPI.h>
#include <Vision/VNWand.h>
#include <Vision/VNWandTracker.h>

#include "SIEngine/Include/SICore.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"

LF_USING_BRIO_NAMESPACE();

namespace Glasgow
{
/// <summary>
/// Scene manager, watches over the loading of the main scene
/// In the past we could have multiple scenes per application instance
/// </summary>
class SceneManager : public BaseObject
{
	CLASSEXTENDS(SceneManager, BaseObject);
public:
	/// <summary>
	/// Singleton
	/// </summary>
	static SceneManager* getInstance();

	/// <summary>
	/// Loads the named scene as passed in via main
	/// </summary>
	Scene* loadScene(String sceneName);

	/// <summary>
	/// Returns the currently loaded scene
	/// </summary>
	Scene* getScene();

	/// <summary>
	/// Launches Error widget
	/// </summary>
	void launchErrorWidget(ErrorID errorCode, bool autoSolve, int autoClose, String titleOverRide, String descOverRide, String cutomGraphic);

	/// <summary>
	/// Launches Video widget
	/// </summary>
	void launchVideoWidget( QVariantMap params, AppState launchAppState );

	/// <summary>
	/// Launches Cartridge based application
	/// </summary>
	void launchCartridgeApp( PackageInfo* params, AppState launchAppState);

	/// <summary>
	/// Launches parent settings application with a particular entry value
	/// </summary>
	void launchParentSettings();

	/// <summary>
	/// Launches parent settings application for new user
	/// </summary>
	void launchParentSceneForNewUser();

	/// <summary>
	/// Launches parent settings application for new user
	/// </summary>
	void launchChildSceneForUser(User* user);

	/// <summary>
	/// Launches child scene regularly
	/// </summary>
	void launchChildScene(AppID fromApp);

	/// <summary>
	/// Launches Content package based application
	/// </summary>
	void launchContentItem( PackageInfo* itemToLaunch, AppState launchAppState);

private:
	/// <summary>
	/// Launches Cartridge based application
	/// </summary>
	void launchWidget(AppID widgetID, QVariantMap params, AppState launchAppState );

	SceneManager();
	~SceneManager();
	static SceneManager* scinstance;

	Scene *currentScene;
};

}
#endif

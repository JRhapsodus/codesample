#include "MultiProfileScene.h"
#include "SIEngine/Include/SIRendering.h"
#include "Glasgow/GlasgowApp.h"

namespace Glasgow
{

MultiProfileScene::MultiProfileScene() : super("TODO","TODO")
{
	setupHotSpots();
	currentHighlight = findQtButton("home");
	GlasgowApp::mainState->writeUIReadyFlag();
	loaded = true;
}

MultiProfileScene::~MultiProfileScene()
{

}

/// <summary>
/// Helper function to cast a qtbuttonobject
/// </summary>
QtButtonObject* MultiProfileScene::findQtButton(String name)
{
	GameObject* goRet = (QtButtonObject*)worldRoot->find(name);
	if( goRet != null )
		return (QtButtonObject*)goRet;
	return NULL;
}

/// <summary>
/// Perhaps app should just exit on button press?
/// </summary>
void MultiProfileScene::onButtonPressAction()
{
	//since home is autoselected this should be the only thing we cazn do really
	QVariantMap responseMap;
	responseMap.insert("SomeResponse", "Error Widget says hope you solved it!");
	GlasgowApp::appInstance->mainState->quit(responseMap);
	super::onButtonPressAction();
}

/// <summary>
/// Perhaps app should just exit on button press?
/// </summary>
void MultiProfileScene::onButtonPressBack()
{

}

/// <summary>
/// Really the error widget has no selections possible
/// </summary>
void MultiProfileScene::onSelectionChange(GameObject* oldSelection, GameObject* newSelection)
{
	super::onSelectionChange(oldSelection, newSelection);
}

/// <summary>
/// Returns the object representing by a no selection -> selected state chnage
/// </summary>
GameObject* MultiProfileScene::getDefaultHighLight()
{
	return findQtButton("home");
}

/// <summary>
/// Sets up hot spots -- Ugly implementation but brute fast quick, only like 5 buttons anyways
/// </summary>
void MultiProfileScene::setupHotSpots()
{
	trackedObjects->removeAllElements();
}

/// <summary>
/// Over ridden to deal with the render chain of qtrenderer, normally we have scene graph objects for this
/// </summary>
void MultiProfileScene::postRender()
{
	QtRenderer::getInstance()->swapBuffers();
	super::postRender();
}

}

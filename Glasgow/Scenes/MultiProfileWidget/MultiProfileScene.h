#ifndef MULTIPROFILESCENE_H_
#define MULTIPROFILESCENE_H_

#include "SIEngine/Core/Scene.h"
#include "SIEngine/Core/User.h"
#include "SIEngine/Include/SIControls.h"
#include "SIEngine/Objects/QtButtonObject.h"
#include "SIEngine/Objects/QtTextObject.h"
#include "Glasgow/Renderables/QtRectObject.h"

using namespace SICore;

namespace Glasgow
{
class MultiProfileScene : public Scene
{
	CLASSEXTENDS(MultiProfileScene, Scene);
public:
	MultiProfileScene();
	virtual ~MultiProfileScene();

	/// <summary>
	/// Perhaps app should just exit on button press?
	/// </summary>
	virtual void onButtonPressAction();

	/// <summary>
	/// Perhaps app should just exit on button press?
	/// </summary>
	virtual void onButtonPressBack();

	/// <summary>
	/// Really the error widget has no selections possible
	/// </summary>
	virtual void onSelectionChange(GameObject* oldSelection, GameObject* newSelection);

	/// <summary>
	/// Sets up hot spots -- Ugly implementation but brute fast quick, only like 5 buttons anyways
	/// </summary>
	virtual void setupHotSpots();

	/// <summary>
	/// Returns the object representing by a no selection -> selected state chnage
	/// </summary>
	virtual GameObject* getDefaultHighLight();

	/// <summary>
	/// Over ridden to deal with the render chain of qtrenderer, normally we have scene graph objects for this
	/// </summary>
	virtual void postRender();

	/// <summary>
	/// Helper function to cast a qtbuttonobject
	/// </summary>
	QtButtonObject* findQtButton(String name);

private:


};

}
#endif

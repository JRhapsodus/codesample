#include "FlappyScene.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIUtils.h"
#include "Glasgow/GlasgowApp.h"
namespace Glasgow
{

FlappyScene::FlappyScene() : super("Layouts/FlappyScene.dat", "FlappySceneStrings")
{
	getCamera(0)->setupForOrthographic(true);
	distanceTraveled = 0;
	winningBackGround = worldRoot->findSprite("winner");
	startButton = worldRoot->findButton("startButton");
	flappy = worldRoot->find("frogRoot");

	trackedObjects->addElement(startButton);
	startButton->setState(ButtonStateActive);
	currentHighlight = startButton;
	attempts = 0;

	score = worldRoot->findLabel("highScore");
	scoreShadow = worldRoot->findLabel("highScoreShadow");

	bgRoot = worldRoot->find("Background");
	fgRoot = worldRoot->find("Foreground");
	pipeRoot = fgRoot->find("Pipes");
	winningBackGround->setVisible(false);
	flappy->setVisible(false);
	gameStarted = false;
	velocity = Vec3f::Up * 200;

	for( int i = 1; i<11; i++ )
	{
		bgTiles.addElement(bgRoot->find(String("Bg") + i));
		GameObject* tile = (GameObject*)bgTiles[i-1];

		tile->setPosition( Vec3f(-969, 36.72461, 0) + ((Vec3f::Right * 360) *i) + ((Vec3f::Forward * 0.1f) *i));
	}

	for( int i = 1; i<21; i++ )
	{
		fgTiles.addElement(fgRoot->find(String("Bg") + i));

		GameObject* tile = (GameObject*)fgTiles[i-1];
		tile->setPosition( Vec3f(-926, 0, 0) + ((Vec3f::Right * 425) *i));
	}

	pipeToClone = pipeRoot->find("Pipe1");
	pipeToClone->setVisible(false);
	originalPosition = flappy->getPosition();
	spawnTimer = 1;
	playMusic("BreakFree");
	GlasgowApp::mainState->writeUIReadyFlag();
	loaded = true;
}

GameObject* FlappyScene::copyPipe( GameObject* pipeRoot )
{
	SpriteObject* top = pipeRoot->findSprite("pipesUp");
	SpriteObject* bottom = pipeRoot->findSprite("pipesDown");

	SpriteData* dataTop = top->getSpriteData();
	SpriteData* dataBottom = bottom->getSpriteData();
	SpriteFrame* frameTop = dataTop->getCurrentFrame();
	SpriteFrame* frameBottom = dataBottom->getCurrentFrame();

	TRelease<SpriteFrame> spriteFrameT( new SpriteFrame(frameTop->getUV(0), frameTop->getUV(1), frameTop->getUV(2), frameTop->getUV(3)) );
	TRelease<SpriteData> spriteDataT( new SpriteData(spriteFrameT, dataTop->getTextureName()) );
	TRelease<SpriteObject> spriteT( new SpriteObject(spriteDataT) );
	TRelease<SpriteFrame> spriteFrameB( new SpriteFrame(frameBottom->getUV(0), frameBottom->getUV(1), frameBottom->getUV(2), frameBottom->getUV(3)) );
	TRelease<SpriteData> spriteDataB( new SpriteData(spriteFrameB, dataBottom->getTextureName()) );
	TRelease<SpriteObject> spriteB( new SpriteObject(spriteDataB) );

	spriteT->setSize(top->getSize());
	spriteB->setSize(bottom->getSize());
	spriteT->setPosition(top->getPosition() + Vec3f::Up*26);
	spriteT->setScale(top->getScale());
	spriteT->name = "pipesUp";
	spriteB->setPosition(bottom->getPosition());
	spriteB->setScale(bottom->getScale());
	spriteB->name = "pipesDown";

	GameObject* newPipe = new GameObject();
	newPipe->addChild(spriteT);
	newPipe->addChild(spriteB);
	newPipe->name = "PipeCopy";
	newPipe->setCullingLayerRecursive(9);

	return newPipe;
}

void FlappyScene::onLost()
{
	gameStarted = false;
	winningBackGround->setVisible(true);
	winningBackGround->setOpacity(0);
	winningBackGround->waitFor(1.5f);
	winningBackGround->fadeTo(1, 1);
	startButton->waitFor(1.5f);
	startButton->setVisible(true);
	startButton->setState(ButtonStateActive);
	startButton->scaleTo( startButton->originalScale, 0.25f );
	startButton->setState(ButtonStateActive);
	currentHighlight = startButton;
	clickLockOut = 4;
}


FlappyScene::~FlappyScene()
{ }

/// <summary>
/// A button on controller was pressed, to reduce size of function
/// each state has its own delegate function for handling
/// </summary>
void FlappyScene::onButtonPressAction()
{
	if( currentHighlight!= null && currentHighlight->name == "startButton")
	{
		if( attempts > 0 )
		{
			winningBackGround->setVisible(false);
			flappy->setRotation(Quaternion::identity);
			flappy->setPosition(originalPosition);
			velocity = Vec3f::Up * 165;

			for( int i = 0; i<10; i++ )
			{
				GameObject* tile = (GameObject*)bgTiles[i];
				tile->setPosition( Vec3f(-969, 36.72461, 0) + ((Vec3f::Right * 360) *i) + ((Vec3f::Forward * 0.1f) *i));
			}

			for( int i = 0; i<20; i++ )
			{
				GameObject* tile = (GameObject*)fgTiles[i];
				tile->setPosition( Vec3f(-926, 0, 0) + ((Vec3f::Right * 425) *i));
			}

			for( int i = 0; i<pipes.getSize(); i++ )
			{
				((GameObject*)pipes[i])->removeFromParent();
			}

			pipes.removeAllElements();
			distanceTraveled = 0;
			spawnTimer = 1;
			attempts++;
			gameStarted = true;
			removeSelection();
			startButton->setState(ButtonStateNormal);
			startButton->scaleTo(Vec3f::Zero, 0.25f);
			startButton->makeVisible(false);
			playSound("Base_PowerOn");
		}
		else
		{
			flappy->setVisible(true);
			startButton->setState(ButtonStateNormal);
			startButton->scaleTo(Vec3f::Zero, 0.25f);
			startButton->makeVisible(false);
			removeSelection();
			distanceTraveled = 0;
			gameStarted = true;
			spawnTimer = 1;
			attempts++;
			playSound("Base_PowerOn");
		}
	}
	else
	{
		playSound("Content_AnswerWrong");
		velocity = Vec3f::Up * 400;
	}

	clickLockOut = 0;
}


void FlappyScene::updateBackGround( float dt )
{
	distanceTraveled += 210*dt;
	score->setText(String("High Score: ") + String( (int)(distanceTraveled/200) ) );
	scoreShadow->setText(String("High Score: ") + String( (int)(distanceTraveled/200) ) );


	for( int i = 0; i<bgTiles.getSize(); i++ )
	{
		GameObject* tile = (GameObject*)bgTiles[i];
		tile->setPosition( tile->getPosition() + Vec3f::Left*25*dt );
	}

	for( int i = 0; i<fgTiles.getSize(); i++ )
	{
		GameObject* tile = (GameObject*)fgTiles[i];
		tile->setPosition( tile->getPosition() + Vec3f::Left*210*dt );
	}

	for( int i = 0; i<pipes.getSize(); i++ )
	{
		GameObject* tile = (GameObject*)pipes[i];
		tile->setPosition( tile->getPosition() + Vec3f::Left*210*dt );
		if( tile->getPosition().x < -1500 )
		{
			tile->removeFromParent();
			pipes.removeElement(tile);
			i--;
		}
	}
}

bool FlappyScene::checkCollisions()
{
	Box3f frogBounds = flappy->getWorldBoundingBox();
	frogBounds._width -= 8;
	frogBounds._height -= 9;

	if( frogBounds.getBottom() < -720/2 + 150 )
	{
		onLost();
		return true;
	}

	if( frogBounds.getTop() > 720/2+100 )
	{
		onLost();
		return true;
	}


	for( int i = 0; i<pipes.getSize(); i++ )
	{
		GameObject* pipe = (GameObject*)pipes[i];
		SpriteObject* topPipe = pipe->findSprite("pipesUp");
		SpriteObject* bottomPipe = pipe->findSprite("pipesDown");

		Box3f topBounds = topPipe->getWorldBoundingBox();
		Box3f bottomBounds = bottomPipe->getWorldBoundingBox();

		Vec3f uL = Vec3f( frogBounds.getLeft(), frogBounds.getTop(), 0);
		Vec3f uR = Vec3f( frogBounds.getRight(), frogBounds.getTop(), 0);
		Vec3f bL = Vec3f( frogBounds.getLeft(), frogBounds.getBottom(), 0);
		Vec3f bR = Vec3f( frogBounds.getRight(), frogBounds.getBottom(), 0);

		if( topBounds.containsIgnoreDepth( uL) || topBounds.containsIgnoreDepth(uR) || topBounds.containsIgnoreDepth( bL) || topBounds.containsIgnoreDepth(bR) )
		{
			onLost();
			return true;
		}

		if( bottomBounds.containsIgnoreDepth( uL) || bottomBounds.containsIgnoreDepth(uR) || bottomBounds.containsIgnoreDepth( bL) || bottomBounds.containsIgnoreDepth(bR) )
		{
			onLost();
			return true;
		}
	}

	return false;
}


void FlappyScene::spawnPipe()
{
	spawnTimer = 1;
	TRelease<GameObject> newPipe( copyPipe(pipeToClone) );
	pipeRoot->addChild(newPipe);

	float rf = randomFloat(0, 1);

	newPipe->setPosition( pipeToClone->getPosition() + Vec3f::Right*distanceTraveled + Vec3f(0, -100 + 325 * rf, 0 ) );
	pipes.addElement( newPipe );


}

void FlappyScene::update(float dt)
{
	if( flappy->isVisibleHierarchy() && gameStarted )
	{
		if( spawnTimer > 0 )
		{
			spawnTimer -= dt;
			if( spawnTimer <= 0 )
			{
				spawnPipe();
			}
		}

		bool hit = checkCollisions();

		if( !hit )
		{
			//scroll background
			updateBackGround(dt);

			//add gravity
			flappy->setPosition( flappy->getPosition() + velocity*dt );
			velocity += Vec3f::Down*1000 * dt;

			//set rotation
			Quaternion currentRot = flappy->getRotation();
			flappy->setRotation( Quaternion::nlerp(currentRot, destRot, 0.25f, false) );
			destRot = Quaternion::angleAxis( velocity.y/15, Vec3f::Forward);
		}
	}

	super::update(dt);
}

/// <summary>
/// Respond to input
/// </summary>
void FlappyScene::onButtonPressBack()
{
	GlasgowApp::appInstance->exit(0);
}


} /* namespace Glasgow */

#ifndef FLAPPYSCENE_H_
#define FLAPPYSCENE_H_

#include "SIEngine/Include/SICollections.h"
#include "SIEngine/Core/Scene.h"
#include "SIEngine/Core/User.h"
#include "SIEngine/Include/GlasgowControls.h"


namespace Glasgow
{

class FlappyScene : public Scene
{
	CLASSEXTENDS(FlappyScene, Scene);
public:
	FlappyScene();
	virtual ~FlappyScene();

	/// <summary>
	/// A button on controller was pressed, to reduce size of function
	/// each state has its own delegate function for handling
	/// </summary>
	virtual void onButtonPressAction();

	/// <summary>
	/// Respond to input
	/// </summary>
	virtual void onButtonPressBack();

	void onLost();
	bool checkCollisions();
	void spawnPipe();
	GameObject* copyPipe( GameObject* pipeRoot );
	void updateBackGround( float dt );
	virtual void update(float dt);

private:
	Vec3f velocity;
	bool gameStarted;

	ObjectArray pipes;
	ObjectArray bgTiles;
	ObjectArray fgTiles;

	int attempts;

	Vec3f originalPosition;

	SpriteObject* winningBackGround;
	ButtonObject* startButton;
	GameObject* flappy;
	TextLabel* score;
	TextLabel* scoreShadow;

	GameObject* pipeToClone;
	Quaternion destRot;
	GameObject* pipeRoot;
	GameObject* bgRoot;
	GameObject* fgRoot;


	float distanceTraveled;
	float spawnTimer;

};

} /* namespace Glasgow */
#endif /* FLAPPYSCENE_H_ */

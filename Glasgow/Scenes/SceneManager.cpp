#include "SIEngine/Include/SIScenes.h"
#include "SIEngine/Include/SIRendering.h"
#include "SIEngine/Include/SIUtils.h"
#include "SIEngine/Include/SIManagers.h"
#include "Glasgow/GlasgowApp.h"

namespace Glasgow
{

SceneManager* SceneManager::scinstance = NULL;

SceneManager::SceneManager() : BaseObject()
{ currentScene = NULL; }

SceneManager::~SceneManager()
{
	printf("SceneManager::~SceneManager\n");
	SAFE_RELEASE(currentScene);
}

SceneManager* SceneManager::getInstance()
{
	if (!scinstance)
	{ scinstance = new SceneManager(); }
	return scinstance;
}

/// <summary>
/// Loads the named scene as passed in via main
/// </summary>
Scene* SceneManager::loadScene(String sceneName)
{
	StringsManager::getInstance()->loadStrings("Help_Strings");
	StringsManager::getInstance()->loadStrings("Dynamic_Strings");
	Scene* newScene = NULL;

	if (sceneName == "ParentScene")
	{ newScene = new ParentScene();	}
	else if (sceneName == "ChildScene")
	{ newScene = new ChildScene(); }
	else if (sceneName == "OutOfBoxScene")
	{ newScene = new OutOfBoxScene(); }
	else if (sceneName == "FlappyScene")
	{ newScene = new FlappyScene(); }
	else if (sceneName == "ErrorScene")
	{ newScene = new ErrorScene(); }
	else if (sceneName == "VideoScene")
	{ newScene = new VideoWidgetScene(); }
	else
	{ ThrowException::exception("Unrecognized Scene Name"); }

	SAFE_RELEASE(currentScene);
	currentScene = newScene;
	return currentScene;
}

/// <summary>
/// Launches Video widget
/// </summary>
void SceneManager::launchVideoWidget( QVariantMap params, AppState currentAppState )
{ launchWidget(AppIDVideoWidget, params, currentAppState); }


/// <summary>
/// Launches widget
/// </summary>
void SceneManager::launchErrorWidget(ErrorID errorCode, bool autoSolve, int autoClose, String titleOverRide, String descOverRide, String cutomGraphic)
{
	if( currentScene == null )
		return;

	currentScene->lockOutInput(4);

	QVariantMap parameterMap;
	if( autoSolve )
	{ parameterMap.insert("AutoSolve", "1" ); }

	if( autoClose > 0 )
	{ parameterMap.insert("AutoClose", String(autoClose).str()); }

	if( !titleOverRide.isEmpty() )
	{ parameterMap.insert("PromptTitle", titleOverRide.str() ); }

	if( !descOverRide.isEmpty() )
	{ parameterMap.insert("PromptDescription", descOverRide.str() );}

	if( !cutomGraphic.isEmpty() )
	{ parameterMap.insert("CustomGraphic", cutomGraphic.str() ); }

	parameterMap.insert("ErrorID", String((int)errorCode).str() );

	launchWidget(AppIDErrorWidget, parameterMap, AppStateNone);
}

/// <summary>
/// Launches Content package based application
/// </summary>
void SceneManager::launchContentItem( PackageInfo* itemToLaunch, AppState currentAppState)
{
	currentScene->lockOutInput(4);

	printf("===================================================================================\n");
	printf("LAUNCHING CONTENT ITEM: %s located at [%s]\n", itemToLaunch->appName.str(), itemToLaunch->appSoPath.str());
	printf("===================================================================================\n");

	//Call entering game
	RioPkgManager* packageManager = PackageManager::Instance();
	packageManager->EnteringGame(itemToLaunch->packageID.str(), UsersManager::getInstance()->getCurrentUser()->getPlayerSLot(), false);

	//Save smashing's last exit launch state and launch paramaeters
	LaunchParameters launchParams = LaunchParameters(AppIDGlasgowUI, AppIDExternal, AppStateNone);
	LaunchAppState launchState = LaunchAppState(ExitReasonContentItem, currentAppState, itemToLaunch->packageID, itemToLaunch->appSoPath, launchParams);
	SystemSettings::saveLaunchState( launchState );
	SystemSettings::setLastGame(itemToLaunch->packageID);

	//Call Package Manager
	QVariantMap parameterMap;
	if( !itemToLaunch->isAppQt )
	{ GlasgowApp::appInstance->LaunchApp( itemToLaunch->appSoPath.str(), parameterMap, kLaunchApp_Brio, kLaunchAction_Exit ); }
	else
	{ GlasgowApp::appInstance->LaunchApp( itemToLaunch->appSoPath.str(), parameterMap, kLaunchApp_Qt, kLaunchAction_Exit ); }
}


/// <summary>
/// Launches Cartridge based application
/// </summary>
void SceneManager::launchWidget( AppID widgetID, QVariantMap params, AppState currentAppState )
{
	currentScene->lockOutInput(4);

	String widgetPath;
	ExitReason exitReason = ExitReasonNone;
	if( widgetID == AppIDVideoWidget )
	{
		exitReason = ExitReasonVideoBundle;
		widgetPath = AssetLibrary::getVideoWidgetPath();
	}
	else
	{
		exitReason = ExitReasonErrorWidget;
		widgetPath = AssetLibrary::getErrorWidgetPath();
	}

	RioPkgManager* packageManager = PackageManager::Instance();
	packageManager->EnteringGame("Widget", UsersManager::getInstance()->getCurrentUser()->getPlayerSLot(), false);

	printf("===================================================================================\n");
	printf(" LAUNCHING WIDGET %s!\n", widgetPath.str());
	printf("===================================================================================\n");
	LaunchParameters launchParams = LaunchParameters(AppIDGlasgowUI, widgetID, AppStateNone);
	LaunchAppState launchState = LaunchAppState(exitReason, currentAppState, widgetPath, "", launchParams);
	SystemSettings::saveLaunchState( launchState );
	GlasgowApp::appInstance->LaunchApp(widgetPath.str(), params, kLaunchApp_Qt, kLaunchAction_Suspend );
}

/// <summary>
/// Launches Cartridge based application
/// </summary>
void SceneManager::launchCartridgeApp( PackageInfo* itemToLaunch, AppState currentAppState )
{
	currentScene->lockOutInput(4);

	printf("===================================================================================\n");
	printf(" LAUNCHING APP ON THE CARTRIDGE!\n");
	printf("===================================================================================\n");

	QVariantMap parameterMap;

	//Call entering game
	RioPkgManager* packageManager = PackageManager::Instance();
	packageManager->EnteringGame(itemToLaunch->packageID.str(), UsersManager::getInstance()->getCurrentUser()->getPlayerSLot(), true);

	//Launch paramaters for smashing
	LaunchParameters launchParams = LaunchParameters(AppIDGlasgowUI, AppIDExternal, AppStateNone);
	LaunchAppState launchState = LaunchAppState( ExitReasonCartridge, currentAppState, itemToLaunch->packageID, "", launchParams);
	SystemSettings::saveLaunchState(launchState);

	SystemSettings::setLastGame(itemToLaunch->packageID);

	//Do the actual launch
	if( itemToLaunch->isAppQt )
	{ GlasgowApp::appInstance->LaunchApp( itemToLaunch->appSoPath.str(), parameterMap, kLaunchApp_Qt, kLaunchAction_Exit );  }
	else
	{ GlasgowApp::appInstance->LaunchApp( itemToLaunch->appSoPath.str(), parameterMap, kLaunchApp_Brio, kLaunchAction_Exit ); }
}


/// <summary>
/// Returns the currently loaded scene
/// </summary>
Scene* SceneManager::getScene()
{ return currentScene; }

/// <summary>
/// Launches parent settings application for new user
/// </summary>
void SceneManager::launchParentSceneForNewUser()
{
	LaunchParameters launchParams = LaunchParameters(AppIDGlasgowUI, AppIDParentSettings, AppStateNewUser);
	LaunchAppState launchState = LaunchAppState( ExitReasonParentSettings, AppStateSignIn, "", "", launchParams);
	SystemSettings::saveLaunchState(launchState);

	QVariantMap parameterMap;
	GlasgowApp::appInstance->LaunchApp( AssetLibrary::getParentSettingsPath().str(), parameterMap, kLaunchApp_Qt, kLaunchAction_Exit );
}

/// <summary>
/// Launches parent settings application with a particular entry value
/// </summary>
void SceneManager::launchParentSettings()
{
	User* currentUser = UsersManager::getInstance()->getCurrentUser();
	String lastUser = "";
	if( currentUser != null )
		lastUser = currentUser->getDisplayName();

	LaunchParameters launchParams = LaunchParameters(AppIDGlasgowUI, AppIDParentSettings, AppStateParentLanding);
	LaunchAppState launchState = LaunchAppState( ExitReasonParentSettings, AppStateSignIn, lastUser, "", launchParams);
	SystemSettings::saveLaunchState(launchState);

	QVariantMap parameterMap;
	GlasgowApp::appInstance->LaunchApp( AssetLibrary::getParentSettingsPath().str(), parameterMap, kLaunchApp_Qt, kLaunchAction_Exit );
}

/// <summary>
/// Launches parent settings application for new user
/// </summary>
void SceneManager::launchChildSceneForUser(User* user)
{
	LaunchParameters launchParams = LaunchParameters(AppIDParentSettings, AppIDGlasgowUI, AppStateSignIn);
	LaunchAppState launchState = LaunchAppState( ExitReasonParentSettingsNewUser, AppStateNone, user->getDisplayName(), "", launchParams);
	SystemSettings::saveLaunchState(launchState);

	QVariantMap parameterMap;
	GlasgowApp::appInstance->LaunchApp( AssetLibrary::getGlasgowUIPath().str(), parameterMap, kLaunchApp_Qt, kLaunchAction_Exit );
}

/// <summary>
/// Launches child scene regularly
/// </summary>
void SceneManager::launchChildScene(AppID fromApp)
{
	LaunchParameters launchParams = LaunchParameters(fromApp, AppIDGlasgowUI, AppStateNone);
	LaunchAppState launchState = LaunchAppState( ExitReasonParentSettings, AppStateNone, "", "", launchParams);
	SystemSettings::saveLaunchState(launchState);

	QVariantMap parameterMap;
	GlasgowApp::appInstance->LaunchApp( AssetLibrary::getGlasgowUIPath().str(), parameterMap, kLaunchApp_Qt, kLaunchAction_Exit );
}


}

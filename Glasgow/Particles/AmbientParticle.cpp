#include "AmbientParticle.h"
#include "SIEngine/Include/SIGeometry.h"
#include "Glasgow/Scenes/SceneManager.h"
#include "AmbientParticleObject.h"

namespace Glasgow
{

AmbientParticle::AmbientParticle(AmbientParticleObject *emitter_, SpriteData *spriteData) : SpriteObject(spriteData)
{
	velocity = Vec3f(0,0,0);
	emitter = emitter_;

	float s = randomFloat(30, 300);
	setSize(Size(s,s));
}

AmbientParticle::~AmbientParticle() { }

Vec2f AmbientParticle::isOffscreen()
{
	ObjectArray *cameras = SceneManager::getInstance()->getScene()->getCameras();
	CameraObject* contentCamera = (CameraObject*)cameras->lastElement();
	Box3f b = getScreenBoundingBox(contentCamera);

	Vec2f offscreenDirection = Vec2f(0,0);

	if (b.getX() < -1 - b.getWidth()) offscreenDirection.x = 1;
	else if (b.getX() > 1) offscreenDirection.x = -1;

	if (b.getY() < -1 - b.getHeight()) offscreenDirection.y = -1;
	else if (b.getY() > 1) offscreenDirection.y = 1;

	return offscreenDirection;
}

void AmbientParticle::update(float dt) {

	Vec3f currentPosition = transform.getPosition();

	// Calculate velocity relative to delta time
	Vec3f vel = Vec3f(velocity.x * dt, velocity.y * dt, 0);

	velocity += emitter->acceleration;

	// Adjust new position by the current velocity & gravity.
	Vec3f newPosition = Vec3f(currentPosition.x + vel.x,
							  currentPosition.y + vel.y,
							  currentPosition.z + vel.z);

	setPosition(newPosition);

	Vec2f offscreenDirection = isOffscreen();
	if (offscreenDirection.x != 0 || offscreenDirection.y != 0) {
		Vec3f position = transform.getPosition();
		int dx, dy;

		if (offscreenDirection.x == 0) dx = randomFloat(-640, 640);
		else dx = position.x * offscreenDirection.x;

		if (offscreenDirection.y == 0) dy = randomFloat(-360, 360);
		else dy = position.y * offscreenDirection.y;


		Vec3f newPosition = Vec3f(dx, dy, position.z);

		printf("Particle went offscreen %2.2f, %2.2f - New particle at %2.2f, %2.2f\n",
				(double)position.x, (double)position.y, (double)newPosition.x, (double)newPosition.y);
		emitter->spawnParticle(newPosition, velocity);
		removeFromParent();
	}

	super::update(dt);
}

void AmbientParticle::render() { super::render(); }

}

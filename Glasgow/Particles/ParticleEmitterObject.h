#ifndef INCLUDE_PARTICLEEMITTEROBJECT_H_
#define INCLUDE_PARTICLEEMITTEROBJECT_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class ParticleEmitterObject : public GameObject
{
	CLASSEXTENDS(ParticleEmitterObject, GameObject);
	ADD_TO_CLASS_MAP;
public:
	ParticleEmitterObject(SpriteData *spriteData);
	virtual ~ParticleEmitterObject();

	bool	particlesTrackEmitter;
	Vec3f 	gravity;
	float 	lifetime;
	int 	maxParticles;
	float 	emissionRate;
	bool 	emitting;

	Vec3f	initialVelocity;
	Vec3f	velocityVariance;

	void update( float dt );

private:
	SpriteData *spriteData;
	float remainder;
	ObjectArray *particles;

};


}
#endif

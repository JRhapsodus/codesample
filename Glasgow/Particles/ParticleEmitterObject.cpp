#include "ParticleEmitterObject.h"
#include "Particle.h"

using namespace SIGeometry;

namespace Glasgow
{

ParticleEmitterObject::ParticleEmitterObject(SpriteData *spriteData_)
{
	particlesTrackEmitter = true;
	particles = new ObjectArray();
	emissionRate = 10;
	emitting = false;
	maxParticles = 100;
	lifetime = 1;
	remainder = 0;
	gravity = Vec3f(0, -9.81, 0);
	spriteData = spriteData_;
	spriteData->addRef();
}

ParticleEmitterObject::~ParticleEmitterObject()
{
	SAFE_RELEASE(spriteData);
	SAFE_RELEASE(particles);
}

void ParticleEmitterObject::update( float dt )
{
	if (emitting == true) {
		int noToEmit = floorf((emissionRate * dt) + remainder);
		int currentParticles = particles->getSize();

		if (noToEmit + currentParticles > maxParticles)
		{ noToEmit = maxParticles - currentParticles; }

		remainder = (emissionRate * dt) - noToEmit;

		for (int i = 0; i < noToEmit; i++)
		{
			Particle *p = new Particle(spriteData);

			p->setSize(Size(31,30));
			p->velocity = Vec3f(initialVelocity.x + randomFloat(-velocityVariance.x, velocityVariance.x),
								initialVelocity.y + randomFloat(-velocityVariance.y, velocityVariance.y),
								initialVelocity.z + randomFloat(-velocityVariance.z, velocityVariance.z));

			if (particlesTrackEmitter == false) {
				parent->addChild(p);
			} else {
				addChild(p);
			}
			particles->addElement(p);
			p->release();
			p->waitFor((lifetime / 4.0f) * 3);
			p->fadeTo(0, lifetime / 4.0f);
		}
	}
	super::update(dt);
	ObjectArray *dead = new ObjectArray();
	for (int i = 0; i < particles->getSize(); i++) {
		BaseObject *o = particles->elementAt(i);
		if (o->typeOf(Particle::type())) {
			Particle *p = (Particle *)o;
			if (p->age >= lifetime) {
				dead->addElement(p);
			}
		}
	}
	for (int i = 0; i < dead->getSize(); i++) {
		Particle *p = (Particle *) dead->elementAt(i);
		if (p == NULL) continue;
		GameObject *parent = (GameObject *)p->parent;
		parent->removeChild(p);
		particles->removeElement(dead->elementAt(i));
	}
	dead->removeAllElements();
	dead->release();
}

}

#include "AmbientParticleObject.h"
#include "AmbientParticle.h"
#include "SIEngine/Geometry/Mathematics.h"

namespace Glasgow
{


AmbientParticleObject::AmbientParticleObject(SpriteData *spriteData_, int numberOfParticles) {
	spriteData = spriteData_;
	for (int i = 0; i < numberOfParticles; i++)
	{
		int screenWidth = 1280;
		int screenHeight = 720;
		int zPosition = transform.getPosition().z;
		spawnParticle(Vec3f(randomFloat(0, screenWidth) - (screenWidth / 2.0f),
						  randomFloat(0, screenHeight) - (screenHeight / 2.0f),
						  zPosition), Vec3f(0,0,0));
	}
}

AmbientParticleObject::~AmbientParticleObject() {}


void AmbientParticleObject::spawnParticle(Vec3f position, Vec3f velocity) {
	AmbientParticle *p = new AmbientParticle(this, spriteData);
	addChild(p);
	p->setPosition(position);
	//p->SetOpacity(0.0f);
	//p->FadeTo(1.0f, 10.0f);
	p->velocity = velocity;
	p->release();
}

void AmbientParticleObject::update( float dt ) {
	super::update(dt);
}

}

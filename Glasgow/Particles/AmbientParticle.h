#ifndef AMBIENTPARTICLE_H_
#define AMBIENTPARTICLE_H_

#include "SIEngine/Objects/SpriteObject.h"

namespace Glasgow
{


class AmbientParticleObject;

class AmbientParticle: public SpriteObject {
	CLASSEXTENDS(AmbientParticle, SpriteObject);
	ADD_TO_CLASS_MAP;
public:
	AmbientParticle(AmbientParticleObject *emitter, SpriteData *spriteData);
	virtual ~AmbientParticle();

	AmbientParticleObject *emitter;

	Vec3f velocity;

	virtual void update(float dt);
	virtual void render();

private:
	Vec2f isOffscreen();
};

}

#endif /* AMBIENTPARTICLE_H_ */

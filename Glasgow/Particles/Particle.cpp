#include "Particle.h"
#include "ParticleEmitterObject.h"
#include "SIEngine/Include/SIUtils.h"

namespace Glasgow
{

Particle::Particle( SpriteData *spriteData ) : SpriteObject(spriteData)
{
	rotationRate = 0;
	lifeSpan = 0;
	age = 0;
	velocity = Vec3f::Zero;
}

Particle::~Particle()
{ }

void Particle::update(float dt)
{
	age += dt;
	float dp = age/lifeSpan;
	if( dp > 1 )
	{
		//dead particle now
		removeFromParent();
		return super::update(dt);
	}

	if ( dp < 0 )
	{ dp = 0; }

	setColor( startColor.lerp( endColor, dp ));

	Vec3f position = transform.getPosition();
	position += velocity * dt;
	velocity += gravity * dt;

	setRotation( getRotation() * Quaternion::angleAxis( rotationRate * dt, Vec3f::Forward));

	setPosition(position);

	super::update(dt);
}

}

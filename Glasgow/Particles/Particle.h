#ifndef PARTICLE_H_
#define PARTICLE_H_

#include "SIEngine/Include/SIDeserialize.h"

namespace Glasgow
{

class Particle: public SpriteObject
{
	CLASSEXTENDS(Particle, SpriteObject);
	ADD_TO_CLASS_MAP;

public:
	Particle(SpriteData *spriteData);
	virtual ~Particle();

	/// <summary> 
	/// Physics properties of a "particle"
	/// </summary> 
	Vec3f gravity;
	Vec3f velocity;
	Color startColor;
	Color endColor;
	float rotationRate;
	float lifeSpan;
	float age;

	virtual void update(float dt);
};

}
#endif /* PARTICLE_H_ */

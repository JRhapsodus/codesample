#ifndef AMBIENTPARTICLEOBJECT_H_
#define AMBIENTPARTICLEOBJECT_H_

#include "SIEngine/Include/SIObjects.h"

namespace Glasgow
{

class AmbientParticleObject : public GameObject
{
	CLASSEXTENDS(AmbientParticleObject, GameObject);
	ADD_TO_CLASS_MAP;
public:
	AmbientParticleObject(SpriteData *spriteData, int numberOfParticles);
	virtual ~AmbientParticleObject();

	void spawnParticle(Vec3f position, Vec3f velocity);
	void update( float dt );
	Vec3f acceleration;
private:
	SpriteData *spriteData;
};

}
#endif /* AMBIENTPARTICLEOBJECT_H_ */

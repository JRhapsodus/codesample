#ifndef GRAPHICS_OPENGLMOVINGSPRITE_INCLUDE_CoreState_H_
#define GRAPHICS_OPENGLMOVINGSPRITE_INCLUDE_CoreState_H_

#include "SIEngine/Include/LFIncludes.h"
#include "SIEngine/Include/SIScenes.h"
#include "SIEngine/Include/SIRendering.h"
#include "SIEngine/Include/SIGeometry.h"
#include "SIEngine/Include/SIInput.h"
#include "SIEngine/Include/SIManagers.h"
#include "SIEngine/Include/SIUtils.h"
#include "GlasgowApp.h"
#include <USBDeviceMPI.h>
#include <USBDeviceTypes.h>

using namespace LF::Hardware;
namespace Glasgow
{

class GlasgowApp;

/*
 *  CoreState class.
 *
 * The only state of the OpenGLMovingSprite app.  This class is responsible
 * for creating the touch button, handling touch and button events and
 * playing the audio file in response to the trigger of a touch event.
 */
class MainState : public QObject, public CGameStateBase, public IEventListener
{
	Q_OBJECT
public:

    MainState(String scene, bool qtRender);
    ~MainState();

    /// <summary> 
    /// Life cycle
    /// </summary> 
    virtual void Enter(CStateData* userData);

    /// <summary>
    /// Main Game Loop
    /// </summary>
    virtual void Update(CGameStateHandler* sh);

    /// <summary>
    /// Life cycle
    /// </summary>
    virtual void Suspend();

    /// <summary>
    /// Life cycle
    /// </summary>
    virtual void Resume();

    /// <summary>
    /// Life cycle
    /// </summary>
    virtual void Exit();

    /// <summary>
    /// Wrapper call to do a clean app shut down
    /// </summary>
    void quit(QVariantMap response);

    BaseRenderer* getRenderer();

protected:
    /// <summary> 
    /// Scenes
    /// </summary> 
    SceneManager* 						sceneManager;

    /// <summary> 
    /// Debug
    /// </summary> 
    float 								frameTimes[10];
    int									frameIndex;

    /// <summary> 
    /// Rendering
    /// </summary> 
    BaseRenderer* 						renderer;
    timespec                            previous_render_time_;

    /// <summary> 
    /// Input
    /// </summary> 
    uint								keyStates;
    uint								lastKeyStates;
    Vec2f								cursor;
    float								cursorStrength;
    Vec3f								accelerometer;
	bool								firstRender;
    bool 								globalPause;
	int									screen_width_;
	int									screen_height_;
	float								analogRepeatTimer;
	float								cameraUpdateTimer;
	bool 								visionGate;
	bool								isQtRenderer;

	/// <summary> 
	/// Leap Frog MPIS
	/// </summary>
    CUSBDeviceMPI						usbMPI_;
	CDisplayMPI 						displayManager;
	CCartridgeMPI						cartridge_mpi;
	CButtonMPI							key_event_mpi;
	CEventMPI 							event_mpi;
	HWControllerMPI 					controllerMpi;


	tVideoSurf 							videoDisplaySurface;
	VNVisionMPI*   						visionMPI;
	tDisplayHandle 						visionDisplayHandle;
	VNAlgorithm*						visionAlgorithm;

 public:
	static GlasgowApp*					appInstance;

    /// <summary>
    /// </summary>
	bool	 controllerSyncInProgress;



	/// <summary>
	/// Forwarding call into FW MPI
	/// </summary>
	void					 			getAllControllers(std::vector<HWController*>&);

    /// <summary>
    /// Wraps the controllerMpi call due to the lack of a controller manager
    /// </summary>
	HWController*					 	getController(int controllerID);

	/// <summary>
	/// IEventListener* protocol, WARNING: SOME EVENTS CAN COME IN THREADED!!!!
	/// The right architecture here is to wrap each Leap message, into a queue that
	/// gets processed on the update thread. No feature in this system needs the fidelity
	/// of threaded real time, so 60 frames per second should be enough speed
	/// </summary>
	virtual tEventStatus Notify( const IEventMessage &msgIn );

    /// <summary>
    /// Required by leap to signal when to clear the device loading video's frame buffer
    /// it should be set to black and thus our screen darken should kick in
    /// </summary>
    void writeUIReadyFlag();

 protected:
	String defaultScene;
	static U32 g_Buttons[];
    void doInputUpdate(float dt);
    void dispatchButtonPress(int keystate, int buttonMask);
    void dispatchButtonReleases(int keystate, int buttonMask);
    void dispatchEvent(Event *event);

    /// <summary>
    /// I would probably prefer a ControllerManager for this but since this
    /// class is the Input dispatcher, it is usefull to listen to the messages. But still,
    /// Maybe at some point a different logic owner can handle the lights, modes, etc...
    /// </summary>
    void startVision();
    void trackWand(HWController* controller);
    void turnControllerLEDOn( HWController* controller);
    void turnControllerLEDOff( HWController* controller);

    /// <summary>
    /// Sub delegation function that processes input if it applies to the controller
    /// subsystem. else return false to signify the message did not apply to controller
    /// </summary>
    bool processControllerMessage(const IEventMessage &msgIn);

    /// <summary>
    /// Sub delegation function that processes input if it applies to the camera
    /// subsystem. else return false to signify the message did not apply to controller
    /// </summary>
    bool processCameraMessage(const IEventMessage &msgIn);

    /// <summary>
    /// Sub delegation function that processes input if it applies to the Cartridge
    /// subsystem. else return false to signify the message did not apply to controller
    /// </summary>
    bool processCatridgeMessage(const IEventMessage &msgIn);

    /// <summary>
    /// Sub delegation function that processes input if it applies to any system
    /// subsystem. else return false to signify the message did not apply to controller
    /// </summary>
    bool processSystemMessage(const IEventMessage &msgIn);

    int masterControllerID;
 private:
    /// <summary>
    /// 60fps cap
    /// </summary>
    float 	timeSinceLastRender;

    /// <summary>
    /// This is useful for internal state logic
    /// </summary>
    bool 	firstUpdate;

    /// <summary>
    /// Creates the renderer
    /// </summary>
    void 	createRenderer();

    /// <summary>
    /// Creates all singeltons & managers used by the app
    /// </summary>
    void 	createSingeltons();

    /// <summary>
    /// Bool to let the app finish a full tick before doing the actual quit.
    /// This is to ensure half the scene graph isn't destroyed while we are
    /// are rendering or updating
    /// </summary>
    int 	quitPosted;

    /// <summary>
    /// To run the engine 1 frame at a time, this bool lets a full 'update' occur on the scene graph
    /// </summary>
    bool 	doNextFrame;

    bool postCartReady;
    bool postCartRemoved;

    /// <summary>
    /// Fixed time step to use, in the case that variable frame rates are not acceptable
    /// Jeston: NOT RECOMMEND, learn to multiply by delatTime. Useful for frame stepping though
    /// </summary>
    double 	fixedTimeStep;

    /// <summary>
    /// This bool is to dispatch down on the update thread. We shouldn't have bools for each
    /// message type but a more generalized event data struct being pushed onto a priority stack
    /// </summary>
    bool 	dispatchSyncPush;

    /// <summary>
    /// For threading
    /// </summary>
    bool dispatchCameraConnect;

    /// <summary>
    /// For threading
    /// </summary>
    bool dispatchCameraDisconnect;

    /// <summary>
    /// Prints out the frames per second
    /// </summary>
    void printFPS();

    /// <summary>
    /// Registers the listeners into Leap's MPIs
    /// </summary>
    void registerEventListeners(void);

    /// <summary>
    /// Clean up
    /// </summary>
    void unregisterEventListeners(void);

    void connectDefaultController();
};

}

#endif  /// GRAPHICS_OPENGLMOVINGSPRITE_INCLUDE_CoreState_H_

